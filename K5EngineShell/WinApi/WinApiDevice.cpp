#include "WinApiDevice.h"
//#include "Globals.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TWinApiDevice *SystemEventWinApiDevicePtr = NULL;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
LRESULT CALLBACK WinApiDeviceProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return SystemEventWinApiDevicePtr->ProcessSystemEvent(hwnd,uMsg,wParam,lParam);
}
///----------------------------------------------------------------------------------------------//
TWinApiDevice::TWinApiDevice():TBaseDevice(),Hwnd(NULL), Bpp(32), CursorOnWindow(false)
{
//-----> ��������� �������� �������� ���� � ������ �� ���� ����������
	LimitThreadAffinityToCurrentProc();
//-----> ��������� ����������� ������� �� ������ � ���������� ���������
	LARGE_INTEGER qwTicksPerSec = {0};

	if(QueryPerformanceFrequency(&qwTicksPerSec) != 0){ WinApiTimer = new TWinApiQPFTimer(); }
	else                                              { WinApiTimer = new TWinApiTGTTimer(); }

    ActiveFlag = true;
	#ifdef TIME_RANGE_TEST
		TFolderWorker FolderWorker;
		TestTimeRangesLog.SetFileName(FolderWorker.GetAppPath()+L"TimeRangesLog.txt");
	#endif
}
///----------------------------------------------------------------------------------------------//
TWinApiDevice::~TWinApiDevice()
{
	if(Hwnd!=NULL){
		DestroyWindow(Hwnd);
	}

	#ifdef TIME_RANGE_TEST
		int Size = TestTimeRanges.size();
		for(int i=0;i<Size;i++){
			wstring Mess(IntToWStr(i)+L": "+FloatToWStr(TestTimeRanges[i]*1000.0f));
			TestTimeRangesLog(Mess);
		}
	#endif
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::MinimizeWindow()
{
	if(Hwnd == NULL){return;}
	if(FullScreen)  {return;}

	SendMessage(Hwnd,WM_SYSCOMMAND,SC_MINIMIZE,0);
	//ShowWindow(Hwnd,SW_MINIMIZE );
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::RestoreWindow()
{
	if(Hwnd == NULL){return;}
	if(FullScreen)  {return;}

	SendMessage(Hwnd,WM_SYSCOMMAND,SC_RESTORE,0);
}
///----------------------------------------------------------------------------------------------//
TPoint TWinApiDevice::GetCursor() const
{
	return CursorPos;
}
///----------------------------------------------------------------------------------------------//
TPoint TWinApiDevice::GetGlobalCursor() const
{
	POINT Pos;
	GetCursorPos(&Pos);
	return TPoint((float)Pos.x, (float)Pos.y);
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::SetBitPerPixel(const int &Value)
{
	Bpp = Value;
}
///----------------------------------------------------------------------------------------------//
int  TWinApiDevice::GetBitPerPixel() const
{
	return Bpp;
}
///----------------------------------------------------------------------------------------------//
void  TWinApiDevice::LimitThreadAffinityToCurrentProc()
{
//----> ��������� ��������� �� ������� �������
    HANDLE hCurProcess = GetCurrentProcess();

    DWORD_PTR dwProcAffinityMask = 0;
    DWORD_PTR dwSysAffinityMask  = 0;
//----> ��������� ����� ������������ ���� ��������� ��� ����� ����������
//----> ������ ����� ������� ��� ��� ������ ��� ����� ������������ �������� ��� ����
//----> �� ������� ����� ��������� ������ �������. ���� �� ������� ����������� �� ����������
//----> �������� ����, �� �������� ���� ����� ����������� �� �����, ��� � ���� ������� �������� �
//----> �� ������ ������ ��������.
    if(GetProcessAffinityMask(hCurProcess, &dwProcAffinityMask,
							  &dwSysAffinityMask) != 0 && dwProcAffinityMask)
	{
        DWORD_PTR dwAffinityMask = (dwProcAffinityMask & ((~dwProcAffinityMask) + 1));
//----> �������� �������� ���� ����������
        HANDLE hCurThread = GetCurrentThread();
        if(INVALID_HANDLE_VALUE != hCurThread){
            SetThreadAffinityMask(hCurThread, dwAffinityMask);
            CloseHandle(hCurThread);
        }
    }
    CloseHandle(hCurProcess);
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::ToProcessEvent()
{
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::AddTimerEvent(const float &Time)
{
	TEvent Event;
	Event			= EN_EVENTTRUE;
	Event.Type		= EN_TIMER;

	Event.Timer.Sec	= Time;
	Event.Timer.Ms 	= Time*1000.0f;

	EventDeque->Push(Event);
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::ProcessTimeDeque()
{
	if(!WinApiTimer->IsActive()){return;}

	float Timer(WinApiTimer->GetElapsedTime());

	#ifdef TIME_RANGE_TEST
		TestTimeRanges.push_back(Timer);
		if(TestTimeRanges.size()>10000){
			TestTimeRanges.clear();
		}
	#endif

	if(TimerEventDelay <= 0.0f){
		AddTimerEvent(Timer);
	}
	else{
		TimerEventDelayCounter += Timer*1000.0f;

		if(TimerEventDelayCounter >= TimerEventDelay){
			TimerEventDelayCounter = 0.0f;
			AddTimerEvent(TimerEventDelay/1000.0f);
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::ProcessWinApiMessage(const MSG &Msg)
{
	TEvent Event;
	switch(Msg.message){
		default:break;

		case WM_PAINT:{
			Event = EN_EVENTTRUE;
			Event.Type = EN_SYSTEM;
			Event.System.Type = EN_PAINT;
		}break;

		case WM_MOUSEMOVE:{
			MouseEventConvertor.MouseMove(Msg);
		}break;

		case WM_LBUTTONUP:{
			MouseEventConvertor.MouseClick(Msg);
		}break;

		case WM_LBUTTONDOWN:{
			MouseEventConvertor.MouseClick(Msg);
		}break;

		case WM_RBUTTONUP:{
			MouseEventConvertor.MouseClick(Msg);
		}break;

		case WM_RBUTTONDOWN:{
			MouseEventConvertor.MouseClick(Msg);
		}break;

		case WM_MOUSEWHEEL:{
			MouseEventConvertor.MouseClick(Msg);
		}break;

		case WM_KEYDOWN:{
			KeyboardEventConvertor.MessType(Msg);
		}break;

		case WM_CHAR:{
			KeyboardEventConvertor.MessChar(Msg);
		}break;

		case WM_KEYUP:{
			KeyboardEventConvertor.MessType(Msg);
		}break;

		case WM_QUIT:{
			Event = EN_EVENTTRUE;
			Event.Type = EN_SYSTEM;
			Event.System.Type = EN_QUIT;
		}break;
	}

	if(MouseEventConvertor.IsReady()){
		Event = MouseEventConvertor.GetResult();
		CursorPos.Set((float)Event.Mouse.X, (float)Event.Mouse.Y);
	}

	if(KeyboardEventConvertor.IsReady()){
		Event = KeyboardEventConvertor.GetResult();
	}

	if(Event == true){ EventDeque->Push(Event); }

    TranslateMessage(&Msg);
    DispatchMessage (&Msg);
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::ProcessEventDeque()
{
    MSG  Msg;
    if(ActiveFlag){
        while(PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE) != 0){
            ProcessWinApiMessage(Msg);
        }
    }
    else if(GetMessage(&Msg, NULL, 0, 0) != 0){
		ProcessWinApiMessage(Msg);
    }
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::CheckCursorPos()
{
	if(FullScreen){return;}

	WINDOWINFO Info;
	GetWindowInfo(Hwnd, &Info);

	POINT Pos;
	GetCursorPos(&Pos);

	if(Pos.x >= Info.rcClient.left && Pos.x <= Info.rcClient.right &&
	   Pos.y >= Info.rcClient.top  && Pos.y <= Info.rcClient.bottom)
	{
		if(!CursorOnWindow){
			CursorOnWindow = true;

			TEvent Event;
			Event = true;
			Event.Type = EN_MOUSE;
			Event.Mouse.Type = EN_MOUSEWINDOWIN;
			EventDeque->Push(Event);
		}
	}
	else{
		if(CursorOnWindow){
			CursorOnWindow = false;

			TEvent Event;
			Event = true;
			Event.Type = EN_MOUSE;
			Event.Mouse.Type = EN_MOUSEWINDOWOUT;
			EventDeque->Push(Event);
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::ProcessEvent()
{
	if(EventDeque!=NULL){
		CheckCursorPos   ();
		ProcessEventDeque();
		ProcessTimeDeque ();
	}

	ToProcessEvent();
}
///----------------------------------------------------------------------------------------------//
LRESULT TWinApiDevice::ProcessSystemEvent(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
		case WM_DISPLAYCHANGE:{
			TEvent Event;

			Event             = EN_EVENTTRUE;
			Event.Type        = EN_SYSTEM;
			Event.System.Type = EN_DISPLAYCHANGE;

			EventDeque->Push(Event);
		}break;

		case WM_MOVE:{
		}break;

        case WM_CLOSE:	{PostQuitMessage(0);}break;
		case WM_DESTROY:{PostQuitMessage(0);}break;

		case WM_ACTIVATE:{
			TEvent Event;
			Event = EN_EVENTTRUE;
			Event.Type = EN_SYSTEM;

			if(LOWORD(wParam) == WA_ACTIVE || LOWORD(wParam) == WA_CLICKACTIVE) {
				Event.System.Type = EN_ACTIVATE;
				ActiveFlag = true;
			}

			if(LOWORD(wParam) == WA_INACTIVE){
				Event.System.Type = EN_DEACTIVATE;
				ActiveFlag = false;
			}
			EventDeque->Push(Event);
		}break;

        case WM_SYSCOMMAND:{
            switch(wParam){

                case SC_MINIMIZE:{
                    TEvent Event;
                    Event = EN_EVENTTRUE;
                    Event.Type = EN_SYSTEM;
                    Event.System.Type = EN_MINIMIZE;
                    EventDeque->Push(Event);

                    return DefWindowProc(hwnd, uMsg, wParam, lParam);
                }break;

                case SC_RESTORE:{
                    TEvent Event;
                    Event = EN_EVENTTRUE;
                    Event.Type = EN_SYSTEM;
                    Event.System.Type = EN_RESTORE;
                    EventDeque->Push(Event);

                    return DefWindowProc(hwnd, uMsg, wParam, lParam);
                }break;
                default:return DefWindowProc(hwnd, uMsg, wParam, lParam);
            }
        }break;

        default: return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
//--------->

    return 0;
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::ActivateTimer()
{
	this->WinApiTimer->Start();
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::DeActivateTimer()
{
	this->WinApiTimer->Stop();
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::ResetTimer()
{
	this->WinApiTimer->Reset();
}
///----------------------------------------------------------------------------------------------//
void TWinApiDevice::SetInstance(const HINSTANCE &Val)
{
	Instance = Val;
}
///----------------------------------------------------------------------------------------------//
HWND TWinApiDevice::GetHwnd()
{
	return Hwnd;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
