#include "WinApiQPFTimer.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TWinApiQPFTimer::TWinApiQPFTimer()
{
   this->bTimerStopped     = false;
   this->bTimeIsBroken     = false;
   this->llQPFTicksPerSec  = 0;
   this->llLastElapsedTime = 0;
   this->llBaseTime        = 0;
   this->llStopTime        = 0;

   LARGE_INTEGER qwTicksPerSec = {0};
   QueryPerformanceFrequency(&qwTicksPerSec);
   this->llQPFTicksPerSec = qwTicksPerSec.QuadPart;

   LARGE_INTEGER qwTime = this->GetAdjustedCurrentTime();

   this->llBaseTime = qwTime.QuadPart;
}
///----------------------------------------------------------------------------------------------//
TWinApiQPFTimer::~TWinApiQPFTimer()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
LARGE_INTEGER TWinApiQPFTimer::GetAdjustedCurrentTime()
{
    LARGE_INTEGER qwTime;
    QueryPerformanceCounter(&qwTime);
    return qwTime;
}
///----------------------------------------------------------------------------------------------//
float TWinApiQPFTimer::GetElapsedTime()
{
    LARGE_INTEGER qwTime = this->GetAdjustedCurrentTime();

    double fElapsedTime = (float)((double)(qwTime.QuadPart - this->llLastElapsedTime)/(double) this->llQPFTicksPerSec );
    this->llLastElapsedTime = qwTime.QuadPart;

//-----> �� ����� �������� ������������� �������� ������� �������, ��-�� ���� ���
//-----> ������� ���� ������ ����������� �� ������ ���� ���������� ��� �������� �������
//-----> � ����������������� �����.
	if(fElapsedTime < 0.0f) {
		fElapsedTime        = 0.0f;
		this->bTimeIsBroken = true;
	}

    return (float)fElapsedTime;
}
///----------------------------------------------------------------------------------------------//
void TWinApiQPFTimer::Start()
{

	LARGE_INTEGER qwTime = {0};
    QueryPerformanceCounter(&qwTime);

    if(this->bTimerStopped)
        this->llBaseTime += qwTime.QuadPart - this->llStopTime;

    this->llStopTime        = 0;
    this->llLastElapsedTime = qwTime.QuadPart;
    this->bTimerStopped     = false;

}
///----------------------------------------------------------------------------------------------//
void TWinApiQPFTimer::Stop()
{
    if(!this->bTimerStopped){
        LARGE_INTEGER qwTime = {0};
        QueryPerformanceCounter(&qwTime);

        this->llStopTime        = qwTime.QuadPart;
        this->llLastElapsedTime = qwTime.QuadPart;
        this->bTimerStopped     = true;
    }

}
///----------------------------------------------------------------------------------------------//
void TWinApiQPFTimer::Reset()
{
	LARGE_INTEGER qwTime = this->GetAdjustedCurrentTime();

    this->llBaseTime		= qwTime.QuadPart;
    this->llLastElapsedTime = qwTime.QuadPart;
    this->llStopTime        = 0;
    this->bTimerStopped     = false;
}
///----------------------------------------------------------------------------------------------//
double TWinApiQPFTimer::GetApplicationRunTime()
{
	LARGE_INTEGER qwTime = this->GetAdjustedCurrentTime();
    double fAppTime = (double)(qwTime.QuadPart - this->llBaseTime) / (double)this->llQPFTicksPerSec;

    return fAppTime;
}
///----------------------------------------------------------------------------------------------//
bool TWinApiQPFTimer::IsActive()
{
	return(!this->bTimerStopped);
}
///----------------------------------------------------------------------------------------------//
bool TWinApiQPFTimer::IsBroken()
{
	return(this->bTimeIsBroken);
}
///----------------------------------------------------------------------------------------------//







