#include "WinApiTGTTimer.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TWinApiTGTTimer::TWinApiTGTTimer()
{
    this->bTimerStopped     = false;
	this->uiPeriod			= 1;
	this->dwStopTime        = 0;
	this->dwLastElapsedTime = 0;
	this->dwBaseTime        = 0;
//----> ������������� ���������� ������� ������ 1���.
	timeBeginPeriod(this->uiPeriod);
}
///----------------------------------------------------------------------------------------------//
TWinApiTGTTimer::~TWinApiTGTTimer()
{
	timeEndPeriod(this->uiPeriod);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
DWORD TWinApiTGTTimer::GetAdjustedCurrentTime()
{
	return(timeGetTime());
}
///----------------------------------------------------------------------------------------------//
float TWinApiTGTTimer::GetElapsedTime()
{
	DWORD dwTime = this->GetAdjustedCurrentTime();

	double fElapsedTime = (float)((double)(dwTime - this->dwLastElapsedTime) * 0.001);
	this->dwLastElapsedTime = dwTime;

//-----> �� ����� �������� ������������� �������� ������� �������, ��-�� ���� ���
//-----> ������� ���� ������ ����������� �� ������ ���� ���������� ��� �������� �������
//-----> � ����������������� �����.
    if(fElapsedTime < 0.0f) fElapsedTime = 0.0f;

    return (float)fElapsedTime;
}
///----------------------------------------------------------------------------------------------//
void TWinApiTGTTimer::Start()
{
	DWORD dwTime = this->GetAdjustedCurrentTime();

    if(this->bTimerStopped)
        this->dwBaseTime += dwTime - this->dwStopTime;

    this->dwStopTime        = 0;
    this->dwLastElapsedTime = dwTime;
    this->bTimerStopped     = false;

}
///----------------------------------------------------------------------------------------------//
void TWinApiTGTTimer::Stop()
{
    if(!this->bTimerStopped){
		DWORD dwTime = this->GetAdjustedCurrentTime();

        this->dwStopTime        = dwTime;
        this->dwLastElapsedTime = dwTime;
        this->bTimerStopped     = true;
    }
}
///----------------------------------------------------------------------------------------------//
void TWinApiTGTTimer::Reset()
{
	DWORD dwTime = this->GetAdjustedCurrentTime();

    this->dwBaseTime		= dwTime;
    this->dwLastElapsedTime = dwTime;
    this->dwStopTime        = 0;
    this->bTimerStopped     = false;
}
///----------------------------------------------------------------------------------------------//
double TWinApiTGTTimer::GetApplicationRunTime()
{
	DWORD dwTime = this->GetAdjustedCurrentTime();

    double fAppTime = (double)(dwTime - this->dwBaseTime) * 0.001;

    return fAppTime;

}
///----------------------------------------------------------------------------------------------//
bool TWinApiTGTTimer::IsActive()
{
	return(!this->bTimerStopped);
}
///----------------------------------------------------------------------------------------------//
bool TWinApiTGTTimer::IsBroken()
{
	return false;
}
///----------------------------------------------------------------------------------------------//







