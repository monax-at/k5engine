///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef WINAPIDEVICE_H_INCLUDED
#define WINAPIDEVICE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../Core/BaseDevice.h"
#include "WinApiMouseEventConvertor.h"
#include "WinApiKeyboardEventConvertor.h"
#include "WinApiTimer.h"
#include "WinApiQPFTimer.h"
#include "WinApiTGTTimer.h"

#ifdef TIME_RANGE_TEST
	#include <LibTools.h>

	#include <vector>
	using std::vector;
#endif
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
LRESULT CALLBACK WinApiDeviceProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

class TWinApiDevice:public TBaseDevice
{
	protected:
		#ifdef TIME_RANGE_TEST
			TLogWorker TestTimeRangesLog;
			vector<double> TestTimeRanges;
		#endif

		HINSTANCE 	Instance;
		HWND 		Hwnd;
		int 		Bpp;

		TWinApiMouseEventConvertor		MouseEventConvertor;
		TWinApiKeyboardEventConvertor 	KeyboardEventConvertor;
//----> �������� �������� �������� ���� � ���� ����������
		void LimitThreadAffinityToCurrentProc();
//----> �������� ������
		TWinApiTimer *WinApiTimer;

		TPoint CursorPos;
		bool     CursorOnWindow;
	protected:
		virtual void ToProcessEvent();

		inline void AddTimerEvent(const float &Time);
		inline void ProcessTimeDeque();
		inline void ProcessWinApiMessage(const MSG &Msg);
		inline void ProcessEventDeque();
		inline void CheckCursorPos();
	public:
		TWinApiDevice();
		virtual ~TWinApiDevice();

		void MinimizeWindow();
		void RestoreWindow();

		TPoint GetCursor()       const;
		TPoint GetGlobalCursor() const;

		void SetBitPerPixel(const int &Value);
		int  GetBitPerPixel() const;

		void ProcessEvent();
		LRESULT ProcessSystemEvent(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

		void ActivateTimer();
		void DeActivateTimer();
		void ResetTimer();

		void SetInstance(const HINSTANCE &Val);

		HWND GetHwnd();
};

extern TWinApiDevice *SystemEventWinApiDevicePtr;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // WINAPIGLDEVICE_H_INCLUDED
