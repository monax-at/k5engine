#include "WinApiMouseEventConvertor.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TWinApiMouseEventConvertor::TWinApiMouseEventConvertor()
			:TWinApiBaseEventConvertor(),hwnd(NULL),Width(0),Height(0)
{
}
///----------------------------------------------------------------------------------------------//
TWinApiMouseEventConvertor::~TWinApiMouseEventConvertor()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TWinApiMouseEventConvertor::SetWinParam(const HWND &Value,
											 const int &iWidth, const int iHeight)
{
	hwnd   = Value;
	Width  = iWidth;
	Height = iHeight;
}
///----------------------------------------------------------------------------------------------//
void TWinApiMouseEventConvertor::MouseMove(const MSG &msg)
{
	ReadyFlag = true;

	Event = EN_EVENTTRUE;
	Event.Type = EN_MOUSE;
	Event.Mouse.Type   = EN_MOUSEMOTION;
	Event.Mouse.Button = EN_NONE;

	// в данном случае координаты считаются от нижнего левого угла экрана
	/*
	Event.Mouse.Set(LOWORD(msg.lParam), Height - HIWORD(msg.lParam));
	*/

	// в данном случае координаты считаются от верхнего левого угла экрана
	Event.Mouse.Set(LOWORD(msg.lParam), HIWORD(msg.lParam));
}
///----------------------------------------------------------------------------------------------//
void TWinApiMouseEventConvertor::MouseClick(const MSG &msg)
{
	ReadyFlag = true;

	Event = EN_EVENTTRUE;
	Event.Type = EN_MOUSE;

	// в данном случае координаты считаются от нижнего левого угла экрана
	/*
	Event.Mouse.Set(LOWORD(msg.lParam), Height - HIWORD(msg.lParam));
	*/

	// в данном случае координаты считаются от верхнего левого угла экрана
	Event.Mouse.Set(LOWORD(msg.lParam), HIWORD(msg.lParam));

	switch(msg.message){
		case WM_LBUTTONUP:{
			Event.Mouse.Type   = EN_MOUSEBUTTONUP;
			Event.Mouse.Button = EN_MOUSEBUTTONLEFT;
		}break;

		case WM_LBUTTONDOWN:{
			Event.Mouse.Type   = EN_MOUSEBUTTONDOWN;
			Event.Mouse.Button = EN_MOUSEBUTTONLEFT;
		}break;

		case WM_RBUTTONUP:{
			Event.Mouse.Type   = EN_MOUSEBUTTONUP;
			Event.Mouse.Button = EN_MOUSEBUTTONRIGHT;
		}break;

		case WM_RBUTTONDOWN:{
			Event.Mouse.Type   = EN_MOUSEBUTTONDOWN;
			Event.Mouse.Button = EN_MOUSEBUTTONRIGHT;
		}break;

		case WM_MOUSEWHEEL:{
			if(GET_WHEEL_DELTA_WPARAM(msg.wParam) > 0){ Event.Mouse.Type = EN_MOUSEWHEELUP  ; }
			else                                      { Event.Mouse.Type = EN_MOUSEWHEELDOWN; }
		}break;

		default:break;
	}
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
