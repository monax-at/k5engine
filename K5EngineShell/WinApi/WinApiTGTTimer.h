///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
/// Authors:
///	Sergey Kalenik [monax.at@gmail.com]
///	Maxim Kolesnikov [mpkmaximus@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef WINAPITGTTIMER_H_INCLUDED
#define WINAPITGTTIMER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include <Windows.h>
#include "WinApiTimer.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TWinApiTGTTimer : public TWinApiTimer
{
	protected:
		UINT  uiPeriod;
		bool  bTimerStopped;
		DWORD dwStopTime;
		DWORD dwLastElapsedTime;
		DWORD dwBaseTime;

		DWORD inline GetAdjustedCurrentTime();
	public:
		TWinApiTGTTimer();
		~TWinApiTGTTimer();

	    void Start();
	    void Stop();
		void Reset();
		inline bool IsActive();
		inline bool IsBroken();
		inline float GetElapsedTime();
		inline double GetApplicationRunTime();
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // WINAPITGTTIMER_H_INCLUDED
