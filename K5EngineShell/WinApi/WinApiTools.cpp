#include "WinApiTools.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TWinApiScreenTool::TWinApiScreenTool()
{
}
///----------------------------------------------------------------------------------------------//
TWinApiScreenTool::~TWinApiScreenTool()
{

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TWinApiScreenTool::GetCurDisplayResolution()
{
	HDC HDCScreen =  GetDC(NULL);

	CurDisplayResolution(	(float)GetDeviceCaps(HDCScreen,  HORZRES),
							(float)GetDeviceCaps(HDCScreen,  VERTRES));

	ReleaseDC(NULL, HDCScreen);
}
///----------------------------------------------------------------------------------------------//
TPoint TWinApiScreenTool::GetDisplayResolution()
{
	GetCurDisplayResolution();
	return CurDisplayResolution;
}
///----------------------------------------------------------------------------------------------//
int TWinApiScreenTool::GetDisplayResolutionWidth()
{
	GetCurDisplayResolution();
	return (int)CurDisplayResolution.GetX();
}
///----------------------------------------------------------------------------------------------//
int TWinApiScreenTool::GetDisplayResolutionHeight()
{
	GetCurDisplayResolution();
	return (int)CurDisplayResolution.GetY();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

