#include "WinApiKeyboardEventConvertor.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TWinApiKeyboardEventConvertor::TWinApiKeyboardEventConvertor()
		:TWinApiBaseEventConvertor()
{
}
///----------------------------------------------------------------------------------------------//
TWinApiKeyboardEventConvertor::~TWinApiKeyboardEventConvertor()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TWinApiKeyboardEventConvertor::MessType(const	MSG &msg)
{
	Event 	   = EN_EVENTTRUE;
	Event.Type = EN_KEYBOARD;

	Event.Keyboard.Type = EN_NONE;
	Event.Keyboard.Key  = EN_NONE;

	ReadyFlag = true;

	switch(msg.message){
		case WM_KEYDOWN:{
			// здесь ReadyFlag ставится в false для того, что бы
			// сообщение было дообновлено функцией MessChar.
			ReadyFlag = false;
			Event.Keyboard.Type = EN_KEYDOWN;
		}break;
		case WM_KEYUP  :{
			Event.Keyboard.Type = EN_KEYUP;
		}break;
	}

	switch(msg.wParam){
		case VK_CONTROL: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_CTRL;
		} break;

		case VK_LMENU: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_LALT;
		} break;

		case VK_RMENU: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_RALT;
		} break;

		case VK_SHIFT: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_SHIFT;
		} break;

		case VK_ESCAPE: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_ESCAPE;
		} break;

		case VK_SPACE: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_SPACE;
		} break;

		case VK_RETURN: { // Enter
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_ENTER;
		} break;

		case VK_UP: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_UP;
		} break;

		case VK_DOWN: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_DOWN;
		} break;

		case VK_LEFT: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_LEFT;
		} break;

		case VK_RIGHT: {
			if(ReadyFlag == false){ReadyFlag = true;}
			Event.Keyboard.Key = EN_KEY_RIGHT;
		} break;

		case VK_BACK: {
			Event.Keyboard.Key = EN_KEY_BACK;
		} break;

		case VK_TAB: {
			Event.Keyboard.Key = EN_KEY_TAB;
		} break;

		case VK_ADD: 	  {Event.Keyboard.Key = EN_KEY_PLUS; } break;
		case VK_SUBTRACT: {Event.Keyboard.Key = EN_KEY_MINUS;} break;

		case VK_NUMPAD0: { Event.Keyboard.Key = EN_KEY_0; } break;
		case VK_NUMPAD1: { Event.Keyboard.Key = EN_KEY_1; } break;
		case VK_NUMPAD2: { Event.Keyboard.Key = EN_KEY_2; } break;
		case VK_NUMPAD3: { Event.Keyboard.Key = EN_KEY_3; } break;
		case VK_NUMPAD4: { Event.Keyboard.Key = EN_KEY_4; } break;
		case VK_NUMPAD5: { Event.Keyboard.Key = EN_KEY_5; } break;
		case VK_NUMPAD6: { Event.Keyboard.Key = EN_KEY_6; } break;
		case VK_NUMPAD7: { Event.Keyboard.Key = EN_KEY_7; } break;
		case VK_NUMPAD8: { Event.Keyboard.Key = EN_KEY_8; } break;
		case VK_NUMPAD9: { Event.Keyboard.Key = EN_KEY_9; } break;

		case 0xC0/*VK_OEM_3*/ 	  :{Event.Keyboard.Key = EN_KEY_MINUS;		} break;
		case 0xBD/*VK_OEM_MINUS*/ :{Event.Keyboard.Key = EN_KEY_MINUS;		} break;
		case 0xBB/*VK_OEM_PLUS*/  :{Event.Keyboard.Key = EN_KEY_PLUS; 		} break;
		case 0xDB/*VK_OEM_4 [*/	  :{Event.Keyboard.Key = EN_KEY_LBKT; 		} break;
		case 0xDD/*VK_OEM_6 [*/	  :{Event.Keyboard.Key = EN_KEY_RBKT; 		} break;
		case 0xBA/*VK_OEM_1 [*/	  :{Event.Keyboard.Key = EN_KEY_SEMICOLON; 	} break;
		case 0xDE/*VK_OEM_7 [*/	  :{Event.Keyboard.Key = EN_KEY_DQUOTE; 	} break;
		case 0xBC/*VK_OEM_COMMA*/ :{Event.Keyboard.Key = EN_KEY_COMMA; 		} break;
		case 0xBE/*VK_OEM_PERIOD*/:{Event.Keyboard.Key = EN_KEY_PERIOD; 	} break;
		case 0xBF/*VK_OEM_2*/	  :{Event.Keyboard.Key = EN_KEY_SLASH; 		} break;

		case 48: { Event.Keyboard.Key = EN_KEY_0; } break;
		case 49: { Event.Keyboard.Key = EN_KEY_1; } break;
		case 50: { Event.Keyboard.Key = EN_KEY_2; } break;
		case 51: { Event.Keyboard.Key = EN_KEY_3; } break;
		case 52: { Event.Keyboard.Key = EN_KEY_4; } break;
		case 53: { Event.Keyboard.Key = EN_KEY_5; } break;
		case 54: { Event.Keyboard.Key = EN_KEY_6; } break;
		case 55: { Event.Keyboard.Key = EN_KEY_7; } break;
		case 56: { Event.Keyboard.Key = EN_KEY_8; } break;
		case 57: { Event.Keyboard.Key = EN_KEY_9; } break;

		case 65: { Event.Keyboard.Key = EN_KEY_A; } break;
		case 66: { Event.Keyboard.Key = EN_KEY_B; } break;
		case 67: { Event.Keyboard.Key = EN_KEY_C; } break;
		case 68: { Event.Keyboard.Key = EN_KEY_D; } break;
		case 69: { Event.Keyboard.Key = EN_KEY_E; } break;
		case 70: { Event.Keyboard.Key = EN_KEY_F; } break;
		case 71: { Event.Keyboard.Key = EN_KEY_G; } break;
		case 72: { Event.Keyboard.Key = EN_KEY_H; } break;
		case 73: { Event.Keyboard.Key = EN_KEY_I; } break;
		case 74: { Event.Keyboard.Key = EN_KEY_J; } break;
		case 75: { Event.Keyboard.Key = EN_KEY_K; } break;
		case 76: { Event.Keyboard.Key = EN_KEY_L; } break;
		case 77: { Event.Keyboard.Key = EN_KEY_M; } break;
		case 78: { Event.Keyboard.Key = EN_KEY_N; } break;
		case 79: { Event.Keyboard.Key = EN_KEY_O; } break;
		case 80: { Event.Keyboard.Key = EN_KEY_P; } break;
		case 81: { Event.Keyboard.Key = EN_KEY_Q; } break;
		case 82: { Event.Keyboard.Key = EN_KEY_R; } break;
		case 83: { Event.Keyboard.Key = EN_KEY_S; } break;
		case 84: { Event.Keyboard.Key = EN_KEY_T; } break;
		case 85: { Event.Keyboard.Key = EN_KEY_U; } break;
		case 86: { Event.Keyboard.Key = EN_KEY_V; } break;
		case 87: { Event.Keyboard.Key = EN_KEY_W; } break;
		case 88: { Event.Keyboard.Key = EN_KEY_X; } break;
		case 89: { Event.Keyboard.Key = EN_KEY_Y; } break;
		case 90: { Event.Keyboard.Key = EN_KEY_Z; } break;
	}
}
///----------------------------------------------------------------------------------------------//
void TWinApiKeyboardEventConvertor::MessChar(const	MSG &msg)
{
	if(ReadyFlag == true){return;}

	wchar_t Character = msg.wParam;
	Event.Keyboard.Symbol = Character;

	ReadyFlag = true;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
