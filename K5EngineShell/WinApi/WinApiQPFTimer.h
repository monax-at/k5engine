///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
/// Authors:
///		Sergey Kalenik [monax.at@gmail.com]
///		Maxim Kolesnikov [mpkmaximus@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef WINAPIQPFTIMER_H_INCLUDED
#define WINAPIQPFTIMER_H_INCLUDED

#include <windows.h>
#include "WinApiTimer.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TWinApiQPFTimer : public TWinApiTimer
{
	protected:
		bool     bTimeIsBroken;
		bool     bTimerStopped;
		LONGLONG llStopTime;
		LONGLONG llQPFTicksPerSec;
		LONGLONG llLastElapsedTime;
		LONGLONG llBaseTime;

		LARGE_INTEGER inline GetAdjustedCurrentTime();
	public:
		TWinApiQPFTimer();
		~TWinApiQPFTimer();

	    void Start();
	    void Stop();
		void Reset();
		inline bool IsActive();
		inline bool IsBroken();
		inline float GetElapsedTime();
		inline double GetApplicationRunTime();
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // WINAPIQPFTIMER_H_INCLUDED
