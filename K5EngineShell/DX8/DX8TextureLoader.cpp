#include "DX8TextureLoader.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDX8TextureLoader::TDX8TextureLoader():TBaseTextureLoader()
{
	Exception.SetPrefix(L"TDX8TextureLoader: ");
}
///----------------------------------------------------------------------------------------------//
TDX8TextureLoader::~TDX8TextureLoader()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
IDirect3DTexture8* TDX8TextureLoader::GetDX8Texture(const wstring &FileName)
{
	IDirect3DTexture8 *TexturePtr(NULL);

	wstring RealFileName;
	if(UseEngineFilePathFormat){RealFileName = (BuildPathStr(FileName));}
	else					   {RealFileName = (FileName);}

	wchar_t* wcFileName = const_cast<wchar_t*>(RealFileName.c_str());

	Exception( 	D3DXCreateTextureFromFile(GPointer_D3DDevice8,wcFileName,&TexturePtr)
				== D3D_OK, L"in GetDX8Texture(...) Can't load texture:"+FileName);

	return TexturePtr;
}
///----------------------------------------------------------------------------------------------//
pTTexture TDX8TextureLoader::Run(	const wstring &FileName, const int &PosX,
									const int &PosY, const int &Width,
									const int &Height, const wstring &TexName)
{
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTTexture TDX8TextureLoader::Run(const wstring &FileName,const wstring &TexName)
{
	Exception(FileName.length() > 0, L"Run -> Invalid texture file name");

	return new TDX8Texture(TexName,GetDX8Texture(FileName));
}
///----------------------------------------------------------------------------------------------//
pTTexture TDX8TextureLoader::Mask(const wstring &FileName,const wstring &TexName)
{
	pTTexture Texture(Run(FileName,TexName));
	pTTexture Mask(Texture->CreateCutMask(TexName));
	Texture->Del();
	delete Texture;
	return Mask;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

