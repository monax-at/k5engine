#include "DX8Texture.h"
#include "../../LibMath/MathFunction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
const int Bpp = 4;
///----------------------------------------------------------------------------------------------//
TDX8Texture::TDX8Texture(const wstring &TextName, IDirect3DTexture8 *pDX8Texture): TTexture()
{
	Init();

    D3DSURFACE_DESC d3Srf;
	Exception(pDX8Texture->GetLevelDesc(0, &d3Srf) == D3D_OK,
			  L"Can't get surface description: " + TextName);

	Exception(d3Srf.Format == D3DFMT_A8R8G8B8 ,
			  L"Wrong texture pixel format. Support only  D3DFMT_A8R8G8B8. Texture: " + TextName);

	// ��������� �� ��������
	TextPointer  = pDX8Texture;

	Name   = TextName;
	Width  = d3Srf.Width;
	Height = d3Srf.Height;
}
///----------------------------------------------------------------------------------------------//
TDX8Texture::TDX8Texture(const wstring &TextName, const int &WidthVal, const int &HeightVal,
						 unsigned char *TexRawData, const bool &IsNativRawData): TTexture()
{
    Init();

    Exception(	GPointer_D3DDevice8->CreateTexture( WidthVal, HeightVal, 1,
				0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &TextPointer) == D3D_OK ,
				L"Can't create texture: "+TextName);

	if(TexRawData != NULL){
		IDirect3DSurface8 *TextSurf = NULL;
		unsigned char *pByteDes     = NULL;

		if(IsNativRawData == false){
			pByteDes = new unsigned char[HeightVal * WidthVal * Bpp];
			WriteToTexture(pByteDes, TexRawData, WidthVal ,HeightVal ,WidthVal * Bpp);
		}else pByteDes = TexRawData;

		Exception(TextPointer->GetSurfaceLevel(0,&TextSurf) == D3D_OK,
				  L"Can't get surface from texture:" + TextName);

		unsigned int Pitch = WidthVal * Bpp;
		Rect TextRect(0, 0, WidthVal, HeightVal);

		Exception( 	D3DXLoadSurfaceFromMemory(TextSurf, NULL, NULL, pByteDes, D3DFMT_A8R8G8B8,
					Pitch, NULL, TextRect.GetRectPtr(), D3DX_FILTER_NONE, 0) == D3D_OK,
					L"Can't load surface from memory");

        TextSurf->Release();

        delete[] pByteDes;
	}

	Name   = TextName;
	Width  = WidthVal;
	Height = HeightVal;
}
///----------------------------------------------------------------------------------------------//
TDX8Texture::~TDX8Texture()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDX8Texture::Init()
{
	Exception.SetPrefix(L"TDX8Texture: ");
	TextPointer = NULL;
}
///----------------------------------------------------------------------------------------------//
IDirect3DTexture8 *TDX8Texture::GetPointer() const
{
    return this->TextPointer;
}
///----------------------------------------------------------------------------------------------//
void TDX8Texture::ToRun()
{
    if(TextPointer == NULL){return;}
	GPointer_D3DDevice8->SetTexture(0, TextPointer);
}
///----------------------------------------------------------------------------------------------//
void TDX8Texture::ToStop()
{
    if(TextPointer == NULL){return;}
	GPointer_D3DDevice8->SetTexture(0, 0);
}
///----------------------------------------------------------------------------------------------//
void TDX8Texture::ToDel()
{
	if(TextPointer == NULL){return;}
	TextPointer->Release();
	TextPointer = NULL;
}
///----------------------------------------------------------------------------------------------//
TTexture *TDX8Texture::ToCreateMask(const wstring &MaskName)
{
	int Size = Width * Height * Bpp;
	unsigned char *pByteSrc = NULL;
	unsigned char *pByteDst = NULL;
///------->
    D3DLOCKED_RECT lrSource;
    TextPointer->LockRect(0, &lrSource, 0, 0);
    pByteSrc = ( unsigned char * )lrSource.pBits;
    pByteDst = new unsigned char[Size]; /// pByteDst ����� ������������� ����������

	for(int iCount = 0; iCount < Size; iCount = iCount + Bpp){
		pByteDst[iCount]     = 0xff;
		pByteDst[iCount + 1] = 0xff;
		pByteDst[iCount + 2] = 0xff;
		pByteDst[iCount + 3] = pByteSrc[iCount + 3];
	}
    TextPointer->UnlockRect(0);

	TDX8Texture *DXTexture = new TDX8Texture(MaskName, Width, Height, pByteDst);

	delete[] pByteDst;

    return DXTexture;
}
///----------------------------------------------------------------------------------------------//
/// ������� �������� ���������� ����� ��� ��������� ����������� ��������
///----------------------------------------------------------------------------------------------//
TTexture *TDX8Texture::ToCreateCutMask(const wstring &MaskName)
{

    D3DLOCKED_RECT lrSource;
	TextPointer->LockRect( 0 , &lrSource , NULL , 0 );
	unsigned int Size = Width * Height * Bpp;

    unsigned char *pByteSrc = (unsigned char *)lrSource.pBits;
	unsigned char *pByteDst = new unsigned char[Size];

    for(unsigned int iCount = 0; iCount < Size; iCount = iCount + Bpp){
		unsigned char Val = 0;
		if(pByteSrc[iCount + 3] > 0){Val = 0xff;}
        for(int i=0;i<4;i++){pByteDst[iCount + i] = Val;}
    }

    TextPointer->UnlockRect(0);

	TDX8Texture *DXTexture = new TDX8Texture(	MaskName, Width, Height, pByteDst, true);

	delete[] pByteDst;

	return DXTexture;
}
///----------------------------------------------------------------------------------------------//
TTexture* TDX8Texture::ToClone(const wstring &TextName)
{

    D3DLOCKED_RECT lrSource;
    TextPointer->LockRect(0, &lrSource, 0, 0);
    unsigned char *pByteSrc = ( unsigned char * )lrSource.pBits;

	TDX8Texture *DXTexture = new TDX8Texture(TextName, Width, Height, pByteSrc, true);

    TextPointer->UnlockRect(0);

    return DXTexture;
}
///----------------------------------------------------------------------------------------------//
TTexture* TDX8Texture::ToCopyRect(const wstring &TextName, const int &RectWidth,const int &RectHeight,
							      const int &X,const int &Y, const bool &UseDeg2TexSizeFlag)

{
    D3DLOCKED_RECT lrText;
	unsigned char *pByteDst	 = NULL;
	unsigned char *pByteSrc  = NULL;
	int NewRectWidth         = RectWidth;
	int NewRectHeight        = RectHeight;
///------------------>
	if(UseDeg2TexSizeFlag){
		NewRectWidth = NextDeg2(NewRectWidth);
		NewRectHeight = NextDeg2(NewRectHeight);
	}

	int Size = NewRectWidth * NewRectHeight * Bpp;
	pByteDst = new unsigned char[Size];
	memset(pByteDst, 0, Size);
///------------------>
	TextPointer->LockRect( 0 , &lrText , NULL , 0 );
	pByteSrc = (unsigned char *)lrText.pBits;
	pByteSrc = pByteSrc + (lrText.Pitch * Y) + X * Bpp;

	ReadFromTexture(pByteDst ,pByteSrc, NewRectWidth, NewRectHeight, lrText.Pitch);

	TextPointer->UnlockRect(0);

	pTDX8Texture NewTexture = new TDX8Texture( TextName, NewRectWidth, NewRectHeight, pByteDst);
	delete[] pByteDst;

	return NewTexture;
}
///----------------------------------------------------------------------------------------------//
void TDX8Texture::ReadFromTexture(unsigned char *DstVal, unsigned char *SrcVal, const int &WidthVal,
								  const int &HeightVal,const int &PitchVal)
{
	unsigned char *pByteSta = NULL;
	unsigned char *pByteDst = DstVal;
	unsigned char *pByteSrc = SrcVal;
///--------->
	for(int iY = 0; iY < HeightVal; iY++){
		pByteSta = pByteSrc;
		for(int iX = 0; iX < WidthVal; iX++){
			pByteDst[0] = pByteSrc[2];
			pByteDst[1] = pByteSrc[1];
			pByteDst[2] = pByteSrc[0];
			pByteDst[3] = pByteSrc[3];
///--------->
			pByteDst = pByteDst + 4;
			pByteSrc = pByteSrc + 4;
		}
		pByteSrc = pByteSta + PitchVal;
	}
}
///----------------------------------------------------------------------------------------------//
void TDX8Texture::WriteToTexture(unsigned char *DstVal, unsigned char *SrcVal, const int &WidthVal,
							const int &HeightVal, const int &PitchVal,const enTexWriteMode &ModeVal)
{
	unsigned char *pByteSta = NULL;
	unsigned char *pByteDst = DstVal;
	unsigned char *pByteSrc = SrcVal;
///--------->
	for(int iY = 0; iY < HeightVal; iY++){
		pByteSta = pByteDst;
		for(int iX = 0; iX < WidthVal ; iX++){
			switch(ModeVal){
				case EN_TWM_Normal:{
								pByteDst[0] = pByteSrc[2];
								pByteDst[1] = pByteSrc[1];
								pByteDst[2] = pByteSrc[0];
								pByteDst[3] = pByteSrc[3];
				}break;
				case EN_TWM_And:{
								pByteDst[0] = pByteDst[0] & pByteSrc[2];
								pByteDst[1] = pByteDst[1] & pByteSrc[1];
								pByteDst[2] = pByteDst[2] & pByteSrc[0];
								pByteDst[3] = pByteDst[3] & pByteSrc[3];
				}break;

				case EN_TWM_Or :{
								pByteDst[0] = pByteDst[0] | pByteSrc[2];
								pByteDst[1] = pByteDst[1] | pByteSrc[1];
								pByteDst[2] = pByteDst[2] | pByteSrc[0];
								pByteDst[3] = pByteDst[3] | pByteSrc[3];
				}break;

				default: { Exception(L"Unknown write mode!"); }break;
			}

			pByteDst = pByteDst + 4;
			pByteSrc = pByteSrc + 4;
		}
		pByteDst = (pByteSta + PitchVal);
	}
}
///----------------------------------------------------------------------------------------------//
pTTextureRawData TDX8Texture::ToGetCopyRawData(const int &WidthVal, const int &HeightVal,
											   const int &XVal, const int &YVal)
{
    D3DLOCKED_RECT lrText;

    D3DSURFACE_DESC d3Srf;
	if(TextPointer->GetLevelDesc(0, &d3Srf) != D3D_OK){
		Exception(L"Can't get surface description: " + Name);
	}
	unsigned char *pByteDst		  = NULL;
	unsigned char *pByteSrc 	  = NULL;
///------------------>
	TextPointer->LockRect( 0 , &lrText , NULL , D3DLOCK_READONLY || D3DLOCK_DISCARD );

	pByteDst = new unsigned char[WidthVal * HeightVal * Bpp];
	pByteSrc = (unsigned char *)lrText.pBits;

	pByteSrc = pByteSrc + (lrText.Pitch * YVal) + XVal * 4;
	ReadFromTexture(pByteDst, pByteSrc, WidthVal, HeightVal, lrText.Pitch);

	TextPointer->UnlockRect(0);
///------------------>
    pTTextureRawData TextureRawData = new TTextureRawData(WidthVal, HeightVal, pByteDst);
	return TextureRawData;
}
///----------------------------------------------------------------------------------------------//
void TDX8Texture::ToWriteRawData(const pTTextureRawData &RawDataVal, const int &XVal,
								 const int &YVal, const enTexWriteMode &ModeVal)
{
    D3DLOCKED_RECT lrText;
	unsigned char *pByteDst	= NULL;
	unsigned char *pByteSrc = NULL;
///------------------>
	TextPointer->LockRect( 0 , &lrText , NULL , 0 );

	pByteSrc = RawDataVal->TextureData;
	pByteDst = (unsigned char *)lrText.pBits;

	pByteDst = pByteDst + (lrText.Pitch * YVal) + XVal  * 4;
	WriteToTexture(pByteDst, pByteSrc, RawDataVal->Width, RawDataVal->Height, lrText.Pitch, ModeVal);

	TextPointer->UnlockRect(0);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
