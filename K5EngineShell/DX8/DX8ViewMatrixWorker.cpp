#include "DX8ViewMatrixWorker.h"
#include "../../LibMath/MathFunction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDX8ViewMatrixWorker::TDX8ViewMatrixWorker():TBaseViewMatrixWorker()
{
}
///----------------------------------------------------------------------------------------------//
TDX8ViewMatrixWorker::~TDX8ViewMatrixWorker()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDX8ViewMatrixWorker::Run()
{
	GPointer_D3DDevice8->GetTransform(D3DTS_VIEW,&MatrView);

	D3DXMATRIX DXTransform;
	D3DXMatrixIdentity(&DXTransform);

	for(int i=0;i<4;i++){ for(int j=0;j<4;j++){DXTransform.m[i][j] = Transform.a[j][i];}}

	D3DXMATRIX ModelView(DXTransform * MatrView);
	GPointer_D3DDevice8->SetTransform(D3DTS_VIEW,&ModelView);
}
///----------------------------------------------------------------------------------------------//
void TDX8ViewMatrixWorker::Stop()
{
	GPointer_D3DDevice8->SetTransform(D3DTS_VIEW,&MatrView);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
