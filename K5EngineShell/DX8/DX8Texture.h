///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
/// Authors:
///		Sergey Kalenik    [monax.at@gmail.com]
///		Maxim Kolesnikov [mpkmaximus@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
/// ����������: �������� ����������� ������ ���� ������ ����� ������� R8G8B8A8.
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef TDX8TEXTURE_H
#define TDX8TEXTURE_H
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "DXTypes.h"
#include "DXTOPalet.h"

#include <limits>

using std::numeric_limits;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDX8Texture: public TTexture
{
    private:
        IDirect3DTexture8 *TextPointer;

		inline void Init();
		inline void ReadFromTexture(unsigned char *DstVal, unsigned char *SrcVal,
									const int &WidthVal, const int &HeightVal,const int &PitchVal);

		inline void WriteToTexture(unsigned char *DstVal, unsigned char *SrcVal, const int &WidthVal,
				const int &HeightVal,const int &PitchVal, const enTexWriteMode &ModeVal=EN_TWM_Normal);
    protected:
        void ToRun();
        void ToStop();
        void ToDel();
///---->
		TTexture *ToCreateMask   (const wstring &MaskName);
		TTexture *ToCreateCutMask(const wstring &MaskName);
///---->
		TTexture *ToClone(const wstring &TextName);
///---->
		TTexture* ToCopyRect(const wstring &TextName, const int &RectWidth,const int &RectHeight,
							 const int &X,const int &Y, const bool &UseDeg2TexSizeFlag);
///---->
		pTTextureRawData ToGetCopyRawData(const int &WidthVal, const int &HeightVal,
										  const int &XVal, const int &YVal);

		void ToWriteRawData(const pTTextureRawData &RawDataVal, const int &XVal, const int &YVal,
							const enTexWriteMode &ModeVal);

    public:
		TDX8Texture(const wstring &TextName, IDirect3DTexture8 *pDX8Texture);
        TDX8Texture(const wstring &TextName, const int &WidthVal, const int &HeightVal,
					unsigned char *TexRawData = NULL, const bool &IsNativRawData = false);

        virtual ~TDX8Texture();
///---->
        IDirect3DTexture8 *GetPointer()  const;
};

typedef TDX8Texture* pTDX8Texture;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // TDX8TEXTURE_H
