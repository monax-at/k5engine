#include "WinApiDX8Device.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TWinApiDX8Device::TWinApiDX8Device():TWinApiDevice(),
			Exception(L"TWinApiDX8Device: "), Format(D3DFMT_UNKNOWN), LostDeviceFlag(false)
{
}
///----------------------------------------------------------------------------------------------//
TWinApiDX8Device::~TWinApiDX8Device()
{
	DisableDirectX();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
_D3DTEXTUREFILTERTYPE TWinApiDX8Device::GetD3D8TextureFilter(const enTextureFilter &Filter)
{
	switch(Filter){
		default:break;
		case EN_TF_Unknown	:return D3DTEXF_NONE; 	break;
		case EN_TF_None		:return D3DTEXF_NONE; 	break;
		case EN_TF_Point	:return D3DTEXF_POINT; 	break;
		case EN_TF_Linear	:return D3DTEXF_LINEAR; break;
	}
	return D3DTEXF_NONE;
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::ToSetTextureFilter()
{
	if(GPointer_D3DDevice8 == NULL){return;}

	Exception( 	GPointer_D3DDevice8->SetTextureStageState(0, D3DTSS_MINFILTER,
				GetD3D8TextureFilter(MinTextureFilter)) == D3D_OK,
				L"SetTextureStageState(0, D3DTSS_MINFILTER, ... ) corrupt");

	Exception( 	GPointer_D3DDevice8->SetTextureStageState(0, D3DTSS_MAGFILTER,
				GetD3D8TextureFilter(MagTextureFilter)) == D3D_OK,
				L"SetTextureStageState(0, D3DTSS_MAGFILTER, ... ) corrupt");
}
///----------------------------------------------------------------------------------------------//
bool TWinApiDX8Device::CheckD3DFormat(const D3DFORMAT &D3DFormat)
{
	if (GPointer_D3D8->CheckDeviceType( D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,
										D3DFormat,D3DFormat,false) == D3D_OK)
	{
		Format = D3DFormat;
		return true;
	}
	return false;
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::CreateWinWindow()
{
	WNDCLASSEX wcex;

	HINSTANCE Instance = GetModuleHandle(NULL);
	Exception(Instance!=NULL,L"in CreateWinWindow() cant't get Instance");

	// register window class
	wcex.cbSize		   = sizeof(WNDCLASSEX);
	wcex.style		   = CS_OWNDC;
	wcex.lpfnWndProc   = WinApiDeviceProc;
	wcex.cbClsExtra	   = 0;
	wcex.cbWndExtra	   = 0;
	wcex.hInstance	   = Instance;
	wcex.hIcon		   = LoadIcon(NULL, IDI_APPLICATION);
	wcex.hCursor	   = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName  = NULL;
	wcex.lpszClassName = L"WinApiWindowClass";
	wcex.hIconSm	   = LoadIcon(NULL, IDI_APPLICATION);;

	Exception(RegisterClassEx(&wcex)!=0,L"in CreateWinWindow() RegisterClassEx() fail");

	DWORD Style(0);
	switch(WindowStyle){
		case EN_DWS_Default:{
			Style = WS_OVERLAPPED | WS_CAPTION | WS_VISIBLE | WS_SYSMENU |
					WS_MINIMIZEBOX;
		}break;

		case EN_DWS_BorderOff:{
			Style = WS_POPUP | WS_VISIBLE;
		}break;
	}

	RECT WinRect;
	WinRect.left 	= 0;
	WinRect.right 	= Width;
	WinRect.top 	= 0;
	WinRect.bottom 	= Height;

	AdjustWindowRect(&WinRect,Style,false);

	int X = (GetSystemMetrics(SM_CXSCREEN) - Width ) / 2;
	int Y = (GetSystemMetrics(SM_CYSCREEN) - Height) / 2;

	Hwnd = CreateWindowEx(0,
						  L"WinApiWindowClass",
						  Caption.c_str(),
						  Style,
						  X,Y,
						  WinRect.right  - WinRect.left,
						  WinRect.bottom - WinRect.top,
						  NULL, NULL, NULL, NULL);

	ShowWindow(Hwnd, SW_SHOW);

	MouseEventConvertor.SetWinParam(Hwnd,Width,Height);
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::EnableDirectX()
{
	GPointer_D3D8 = Direct3DCreate8(D3D_SDK_VERSION);
	Exception(GPointer_D3D8!=NULL,	L"in EnableDirectX() initialization fail, "
									L"GPointer_D3D8 is NULL");

	ZeroMemory(&D3dpp, sizeof(D3dpp));

	#ifdef GET_ADAPTER_INFO
/*
		D3DADAPTER_IDENTIFIER8 D3dai;
		GPointer_D3D8->GetAdapterIdentifier(D3DADAPTER_DEFAULT, D3DENUM_NO_WHQL_LEVEL, &D3dai);
		TLog Log(L"log.txt");
		Log.WriteFrm("D3D Driver: %s",D3dai.Driver);
		Log.WriteFrm("Description: %s",D3dai.Description);
		Log.WriteFrm("Version: %d.%d.%d.%d",
							HIWORD(D3dai.DriverVersion.HighPart),
							LOWORD(D3dai.DriverVersion.HighPart),
							HIWORD(D3dai.DriverVersion.LowPart),
							LOWORD(D3dai.DriverVersion.LowPart));
*/
	#endif


	if( !FullScreen ){
		D3dpp.Windowed = true;

		if(EnableVSync){
		}

		D3DDISPLAYMODE D3ddm;
		if(FAILED(GPointer_D3D8->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &D3ddm))){
			Exception(L"in EnableDirectX() can`t get display mode D3ddm ");
		}
		Format = D3ddm.Format;
	}
	else{
		D3dpp.Windowed = false;
		D3dpp.FullScreen_RefreshRateInHz	 = D3DPRESENT_RATE_DEFAULT;

		if(EnableVSync){ D3dpp.FullScreen_PresentationInterval = D3DPRESENT_INTERVAL_ONE; }
		else		   { D3dpp.FullScreen_PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE; }

		switch(ColorBits){
			case 16:{
		 		if(CheckD3DFormat(D3DFMT_A4R4G4B4) == true){break;}
				if(CheckD3DFormat(D3DFMT_X4R4G4B4) == true){break;}
				if(CheckD3DFormat(D3DFMT_A1R5G5B5) == true){break;}
				if(CheckD3DFormat(D3DFMT_X1R5G5B5) == true){break;}
				if(CheckD3DFormat(D3DFMT_R5G6B5)   == true){break;}
			}break;

			case 32:{
				if(CheckD3DFormat(D3DFMT_A8R8G8B8) == true){break;}
				if(CheckD3DFormat(D3DFMT_X8R8G8B8) == true){break;}
			}break;
		}
	}

	Exception(Format != D3DFMT_UNKNOWN, L"in EnableDirectX() unknown Format");

	if(FullScreen)	{ D3dpp.SwapEffect = D3DSWAPEFFECT_FLIP; }
	else			{ D3dpp.SwapEffect = D3DSWAPEFFECT_COPY; }

	D3dpp.BackBufferFormat 	= Format;
	D3dpp.BackBufferWidth	= Width;
	D3dpp.BackBufferHeight	= Height;
	D3dpp.BackBufferCount	= 1;
	D3dpp.MultiSampleType   = D3DMULTISAMPLE_NONE;
	D3dpp.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
	D3dpp.EnableAutoDepthStencil = false;
	D3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;

	D3DCAPS8 DevCaps;
	DWORD dwVertexProcessing;
	GPointer_D3D8->GetDeviceCaps(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,&DevCaps);

	if((DevCaps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) == D3DDEVCAPS_HWTRANSFORMANDLIGHT) {
		dwVertexProcessing = D3DCREATE_HARDWARE_VERTEXPROCESSING;
	}
	else{
		dwVertexProcessing = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}

	HRESULT  Res = GPointer_D3D8->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
				   Hwnd, dwVertexProcessing | D3DCREATE_MULTITHREADED, &D3dpp, &GPointer_D3DDevice8);

	switch(Res){
		case D3D_OK:{}break;

		case D3DERR_INVALIDCALL :{
			Exception(L"in EnableDirectX() can`t create device: D3DERR_INVALIDCALL");
		}break;

		case D3DERR_NOTAVAILABLE :{
			Exception(L"in EnableDirectX() can`t create device: D3DERR_NOTAVAILABLE");
		}break;

		case D3DERR_OUTOFVIDEOMEMORY  :{
			Exception(L"in EnableDirectX() can`t create device: D3DERR_OUTOFVIDEOMEMORY");
		}break;
	}
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::ConfigureDirectX()
{
	HRESULT Res;

	Res = GPointer_D3DDevice8->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
	Exception(Res == D3D_OK, L"SetRenderState(D3DRS_CULLMODE, D3DCULL_CW) corrupt");

	Res = GPointer_D3DDevice8->SetRenderState(D3DRS_LIGHTING, false);
	Exception(Res == D3D_OK, L"SetRenderState(D3DRS_LIGHTING, false) corrupt");

	Res = GPointer_D3DDevice8->SetRenderState( D3DRS_ZENABLE, true );
	Exception(Res == D3D_OK, L"SetRenderState( D3DRS_ZENABLE, true ) corrupt");

	Res = GPointer_D3DDevice8->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	Exception(Res == D3D_OK, L"SetRenderState(D3DRS_ALPHABLENDENABLE, true) corrupt");

	Res = GPointer_D3DDevice8->SetRenderState(D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA);
	Exception(Res == D3D_OK, L"SetRenderState(D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA) corrupt");

	Res = GPointer_D3DDevice8->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	Exception(Res == D3D_OK, L"SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA) corrupt");

	Res = GPointer_D3DDevice8->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	Exception(Res == D3D_OK, L"SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE) corrupt");

	Res = GPointer_D3DDevice8->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );
	Exception(Res == D3D_OK, L"SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE ) corrupt");

	Res = GPointer_D3DDevice8->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE );
	Exception(Res == D3D_OK, L"SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE ) corrupt");

	// установка фильтрации текстур
	ToSetTextureFilter();

	D3DVIEWPORT8 Vp;
	Vp.X	  = 0;
	Vp.Y	  = 0;
	Vp.Width  = Width;
	Vp.Height = Height;

	Vp.MinZ   = 0.0f;
	Vp.MaxZ   = 1.0f;

	Res = GPointer_D3DDevice8->SetViewport(&Vp);
	Exception(Res == D3D_OK, L"SetViewport(&Vp) corrupt");

	D3DXMATRIX MatView;
	D3DXMatrixIdentity(&MatView);
	Res = GPointer_D3DDevice8->SetTransform(D3DTS_VIEW, &MatView);
	Exception(Res == D3D_OK, L"SetTransform(D3DTS_VIEW, &MatView) corrupt");

	D3DXMATRIX MatWorld;
	D3DXMatrixIdentity(&MatWorld);
	Res = GPointer_D3DDevice8->SetTransform(D3DTS_WORLD, &MatWorld);
	Exception(Res == D3D_OK, L"SetTransform(D3DTS_WORLD, &MatWorld) corrupt");

	D3DXMATRIX Orto;
	D3DXMatrixIdentity(&Orto);
	D3DXMatrixOrthoOffCenterRH(&Orto,0.0f,(float)Width,(float)Height,0.0f,0.0f,-100.0f);
	Res = GPointer_D3DDevice8->SetTransform(D3DTS_PROJECTION, &Orto);
	Exception(Res == D3D_OK, L"SetTransform(D3DTS_PROJECTION, &Orto) corrupt");
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::DisableDirectX()
{
	if(GPointer_D3DDevice8 != NULL){
		GPointer_D3DDevice8->Release();
		GPointer_D3DDevice8 = NULL;
	}

	if(GPointer_D3D8 != NULL){
		GPointer_D3D8->Release();
		GPointer_D3D8 = NULL;
	}
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::ResetDxDevice()
{
	while(GPointer_D3DDevice8->Reset(&D3dpp) != D3D_OK);

	ConfigureDirectX();
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::ToProcessEvent()
{
	// проверка потери устройства
	HRESULT Res(GPointer_D3DDevice8->TestCooperativeLevel());
	if(Res == D3D_OK){return;}

	TEvent Event;
	Event = true;
	Event.Type = EN_SYSTEM;

	if(Res == D3DERR_DEVICELOST && LostDeviceFlag == false){
		LostDeviceFlag = true;
		Event.System.Type = EN_DEVICELOST;

		if(EventDeque!=NULL){EventDeque->Push(Event);}
	}

	if(Res == D3DERR_DEVICENOTRESET && LostDeviceFlag == true){

		ResetDxDevice();

		LostDeviceFlag = false;
		Event.System.Type = EN_DEVICERESTORE;

		if(EventDeque!=NULL){EventDeque->Push(Event);}
	}
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::Create(	const int &iWidth,const int &iHeight,
								const bool &bFullScreen)
{
	Width      = iWidth;
	Height     = iHeight;
	FullScreen = bFullScreen;

	SystemEventWinApiDevicePtr = this;

	CreateWinWindow ();
	EnableDirectX   ();
	ConfigureDirectX();

	SetDrawCursor(DrawCursor);
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::ResetDevice()
{
	ResetDxDevice();
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::ClearSceneColor()
{
	GPointer_D3DDevice8->Clear(0, NULL, D3DCLEAR_TARGET,
				D3DCOLOR_COLORVALUE(ClearColor.Get(0), ClearColor.Get(1),
									ClearColor.Get(2), ClearColor.Get(3)), 1.0f, 0);

	GPointer_D3DDevice8->BeginScene();
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::SwapSceneBuffers()
{
	GPointer_D3DDevice8->EndScene();
	GPointer_D3DDevice8->Present(NULL, NULL, NULL, NULL);
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::ResizeWindow(const int &Width, const int &Height)
{
	D3dpp.BackBufferWidth  = Width;
	D3dpp.BackBufferHeight = Height;

	ResetDxDevice();

	::SetWindowPos(Hwnd, NULL, 0, 0, Width, Height, SWP_NOZORDER | SWP_NOMOVE);
}
///----------------------------------------------------------------------------------------------//
void TWinApiDX8Device::SetDrawCursor(const bool &Val)
{
	DrawCursor = Val;
	if(Hwnd == NULL){return;}

	ShowCursor(DrawCursor);
	GPointer_D3DDevice8->ShowCursor(Val);
}
///----------------------------------------------------------------------------------------------//
pTBaseTextureLoader TWinApiDX8Device::CreateTextureLoader()
{
	return new TDX8TextureLoader;
}
///----------------------------------------------------------------------------------------------//
pTBaseTextureCreater	TWinApiDX8Device::CreateTextureCreater()
{
	return new TDX8TextureCreater;
}
///----------------------------------------------------------------------------------------------//
pTBaseScreenPixelReader TWinApiDX8Device::CreateScreenPixelReader()
{
	return new TDX8ScreenPixelReader;
}
///----------------------------------------------------------------------------------------------//
pTBaseViewMatrixWorker TWinApiDX8Device::CreateViewMatrixWorker()
{
	return new TDX8ViewMatrixWorker;
}
///----------------------------------------------------------------------------------------------//
pTBaseVertexData TWinApiDX8Device::CreateSpriteVertexData()
{
	return new TDX8SpriteVertexData;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
