///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
/// Authors:
///     Sergey Kalenik    [monax.at@gmail.com]
///     Maxim Kolesnikov [mpkmaximus@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DX8TEXTURELOADER_H
#define DX8TEXTURELOADER_H
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../LibTools/LibTools.h"
#include "DX8Texture.h"

#include <string>
using std::wstring;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDX8TextureLoader: public TBaseTextureLoader
{
	protected:
		IDirect3DTexture8* GetDX8Texture(const wstring &FileName);
	public:
		TDX8TextureLoader();
		virtual ~TDX8TextureLoader();

		pTTexture Run(	const wstring &FileName, const int &PosX, const int &PosY,
						const int &Width, const int &Height,const wstring &TexName=L"");

		pTTexture Run (const wstring &FileName, const wstring &TexName=L"");
		pTTexture Mask(const wstring &FileName,const wstring &TexName=L"");
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DX8TEXTURELOADER_H
