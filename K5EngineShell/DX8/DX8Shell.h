///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DX8SHELL_H_INCLUDED
#define DX8SHELL_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "DX8Camera.h"
#include "DX8ScreenPixelReader.h"
#include "DX8SpriteBankViewerVBO.h"
#include "DX8SpriteViewerVBO.h"
#include "DX8SpriteVertexDataPool.h"
#include "DX8Texture.h"
#include "DX8TextureLoader.h"
#include "DX8TextViewerVBO.h"
#include "DXTOPalet.h"
#include "DXTypes.h"
#include "FTDX8GlyphCreator.h"
#include "WinApiDX8Device.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DX8SHELL_H_INCLUDED
