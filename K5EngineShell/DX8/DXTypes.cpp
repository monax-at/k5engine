#include "DXTypes.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
Rect::Rect()
{
    Left    = 0;
    Top     = 0;
    Width   = 0;
    Height  = 0;
    StartLeft = 0;
    StartTop  = 0;
}
///----------------------------------------------------------------------------------------------//
Rect::Rect(int iLeft, int iTop, int iWidth, int iHeight)
{
    this->Left   = iLeft;
    this->Top    = iTop;
    this->Width  = iWidth;
    this->Height = iHeight;

}
//---------------------------------------------------------------------------------------
const RECT* Rect::GetRectPtr()
{
    r.left   = Left;
    r.top    = Top;
    r.right  = Left+Width;
    r.bottom = Top+Height;
    return &r;
}
///----------------------------------------------------------------------------------------------//
void Rect::operator /= (int Right)
{
    Left /= Right;
    Top  /= Right;
    Height/= Right;
    Width /= Right;

}
///----------------------------------------------------------------------------------------------//
void Rect::operator *= (int Right)
{
    Left *= Right;
    Top  *= Right;
    Height *= Right;
    Width  *= Right;
}
///----------------------------------------------------------------------------------------------//
D3DXVECTOR2 Rect::Center(void)
{
    return D3DXVECTOR2(float(Width/2), float(Height/2));
}
///----------------------------------------------------------------------------------------------//
void Rect::SetSize(int pLeft, int pTop, int pWidth, int pHeight)
{
    Left   = pLeft;
    Top    = pTop;
    Width  = pWidth;
    Height = pHeight;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

