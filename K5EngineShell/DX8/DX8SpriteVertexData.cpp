#include "DX8SpriteVertexData.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDX8SpriteVertexData::TDX8SpriteVertexData():TSpriteVertexData(),
											 VertexBuffer(NULL)
{
	Exception.SetPrefix(L"TDX8SpriteVertexData: ");
}
///----------------------------------------------------------------------------------------------//
TDX8SpriteVertexData::~TDX8SpriteVertexData()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
DWORD TDX8SpriteVertexData::GenDXColor(const TColor &Val)
{
	return D3DCOLOR_COLORVALUE(	Val.GetRed (), Val.GetGreen(), Val.GetBlue(), Val.GetAlpha() );
}
///----------------------------------------------------------------------------------------------//
void TDX8SpriteVertexData::GenMeshData(float *fVert)
{
	TPoint Point;

	// первый треугольник
	Point = Mesh->Get(2);
	fVert[0] = Point.GetX();	fVert[1] = Point.GetY();

	Point = Mesh->Get(1);
	fVert[2] = Point.GetX();	fVert[3] = Point.GetY();

	Point = Mesh->Get(3);
	fVert[4] = Point.GetX();	fVert[5] = Point.GetY();

	// второй треугольник
	Point = Mesh->Get(0);
	fVert[6] = Point.GetX();	fVert[7] = Point.GetY();

	Point = Mesh->Get(3);
	fVert[8] = Point.GetX();	fVert[9] = Point.GetY();

	Point = Mesh->Get(1);
	fVert[10] = Point.GetX();	fVert[11] = Point.GetY();
}
///----------------------------------------------------------------------------------------------//
void TDX8SpriteVertexData::GenColorData(DWORD *dwColor)
{
	for(int i=0;i<6;i++){ dwColor[i] = GenDXColor(*Color); }

	for(int i=0;i<4;i++){
		if(UsePointColor[i]){
			TColor ColorVal(PointColor[i]);
			switch(i){
				case 0:{ dwColor[3] =  GenDXColor(ColorVal); }break;

				case 1:{
					dwColor[1] =  GenDXColor(ColorVal);
					dwColor[5] =  GenDXColor(ColorVal);
				}break;

				case 2:{ dwColor[0] =  GenDXColor(ColorVal); }break;

				case 3:{
					dwColor[2] =  GenDXColor(ColorVal);
					dwColor[4] =  GenDXColor(ColorVal);
				}break;
			}
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TDX8SpriteVertexData::GenTexData(float *fText)
{
	if(Texture->IsInit() && Texture->GetCoordinatesState() == EN_TCS_Floating){
		Texture->BuildCoordinatesByMesh(Mesh);
	}

	TPoint *Point(NULL);
/*
	float MaxX(0.0f);
	float MaxY(0.0f);
*/
	enTextureUseState UseState(Texture->GetUseState());
	if(UseState == EN_TUS_Repeat){
/*
		MaxX = Texture->GetRepeatX();
		MaxY = Texture->GetRepeatY();
*/
	}
	else{
/*
		float WidthCof (1.0f/Texture->GetWidth());
		float HeightCof(1.0f/Texture->GetHeight());
*/
/*
		MaxX = WidthCof *(Texture->GetRectX() + Texture->GetRectWidth());
		MaxY = HeightCof*(Texture->GetRectY() + Texture->GetRectHeight());
*/
	}

	// добавление пол пикселя для обеспечения отображения "пиксель-в-пиксель"
	float TextureShiftX = (1.0f/(Texture->GetWidth()  *2.0f));
	float TextureShiftY = (1.0f/(Texture->GetHeight() *2.0f));

	if(Texture->GetMirrorX()){ TextureShiftX *= -1;}
	if(Texture->GetMirrorY()){ TextureShiftY *= -1;}

	// первый треугольник
	Point = Texture->GetPointPtr(2);
	fText[0] = Point->GetX() + TextureShiftX;
	fText[1] = Point->GetY() + TextureShiftY;

	Point = Texture->GetPointPtr(1);
	fText[2] = Point->GetX() + TextureShiftX;
	fText[3] = Point->GetY() + TextureShiftY;

	Point = Texture->GetPointPtr(3);
	fText[4] = Point->GetX() + TextureShiftX;
	fText[5] = Point->GetY() + TextureShiftY;

	// второй треугольник
	Point = Texture->GetPointPtr(0);
	fText[6] = Point->GetX() + TextureShiftX;
	fText[7] = Point->GetY() + TextureShiftY;

	Point = Texture->GetPointPtr(3);
	fText[8] = Point->GetX() + TextureShiftX;
	fText[9] = Point->GetY() + TextureShiftY;

	Point = Texture->GetPointPtr(1);
	fText[10] = Point->GetX() + TextureShiftX;
	fText[11] = Point->GetY() + TextureShiftY;
}
///----------------------------------------------------------------------------------------------//
void TDX8SpriteVertexData::Create()
{
	try{
		HRESULT Res = GPointer_D3DDevice8->CreateVertexBuffer(
								6*sizeof(TexturedSpriteVertex),
								D3DUSAGE_WRITEONLY, D3DFVF_TEXTUREDSPRITEEVERT,
								D3DPOOL_MANAGED, &VertexBuffer);

		switch(Res){
			case D3D_OK:{}break;
			case D3DERR_INVALIDCALL :{ 		Exception(L"in ToInit() Failed CreateVertexBuffer() for VertexBuffer: D3DERR_INVALIDCALL");
			}break;
			case D3DERR_OUTOFVIDEOMEMORY:{	Exception(L"in ToInit() Failed CreateVertexBuffer() for VertexBuffer: D3DERR_OUTOFVIDEOMEMORY");
			}break;
			case E_OUTOFMEMORY:{ 			Exception(L"in ToInit() Failed CreateVertexBuffer() for VertexBuffer: E_OUTOFMEMORY");
			}break;
		}
	}
	catch(TException &Ex){
		if(VertexBuffer!=NULL){ VertexBuffer->Release(); }
		throw Ex;
	}
}
///----------------------------------------------------------------------------------------------//
void TDX8SpriteVertexData::Delete()
{
	if(VertexBuffer != NULL){ VertexBuffer->Release(); }
	VertexBuffer = NULL;
}
///----------------------------------------------------------------------------------------------//
void TDX8SpriteVertexData::Update()
{
	if(!CheckNeedUpdate()){return;}

	IdActive = Id;

	// 2*6 вершины, 2*6 текстурные координаты
    float  fVert  [12];
    DWORD  dwColor[6];
    float  fText  [12];

	GenMeshData (fVert);
	GenColorData(dwColor);
	GenTexData  (fText);

	TexturedSpriteVertex* Vertices;

	if(FAILED(VertexBuffer->Lock(0, 6*sizeof(TexturedSpriteVertex),
                            (BYTE **)&Vertices, 0)))
	{
		Exception(L"in Update() Failed Lock(): D3DERR_INVALIDCALL");
	}

	for(int i=0;i<6;i++){
		// вершины
		Vertices[i].X = fVert[i*2];
		Vertices[i].Y = fVert[i*2+1];
		Vertices[i].Z = 0;

		// цвет
		Vertices[i].Color = dwColor[i];

		// текстурные координаты
		Vertices[i].tu = fText[i*2];
		Vertices[i].tv = fText[i*2+1];
	}

	if(FAILED(VertexBuffer->Unlock())){
		Exception(L"in Update() Failed Unlock(): D3DERR_INVALIDCALL");
	}
}
///----------------------------------------------------------------------------------------------//
void TDX8SpriteVertexData::Run()
{
	Update();

	GPointer_D3DDevice8->SetStreamSource(0,VertexBuffer,sizeof(TexturedSpriteVertex));
	GPointer_D3DDevice8->SetVertexShader(D3DFVF_TEXTUREDSPRITEEVERT);
	GPointer_D3DDevice8->DrawPrimitive(D3DPT_TRIANGLELIST,0,2);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//


