///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "DXTOPalet.h"
///----------------------------------------------------------------------------------------------//
TDXPalet::TDXPalet():TPalet(),Exception(L"TDXPalet"),DXPalet(D3DFMT_UNKNOWN)
{
}
///----------------------------------------------------------------------------------------------//
TDXPalet::~TDXPalet()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
D3DFORMAT TDXPalet::PLTODX(enPaletType PaletFrmt)
{
    switch(PaletFrmt){
        case EN_PL_RGB       : return(D3DFMT_X8R8G8B8);
        case EN_PL_RGBA      : return(D3DFMT_A8R8G8B8);
        case EN_PL_GRAYSCALE : return(D3DFMT_L8);
        case EN_PL_NOTSET : this->Exception(L"Palet type not set, can't convert to DirectX format");
        default : this->Exception(L"Unknow palet type, can't convert to DirectX format");
    }
    return(D3DFMT_UNKNOWN);
}
///----------------------------------------------------------------------------------------------//
enPaletType TDXPalet::DXTOPL(D3DFORMAT TextFrmt)
{
    switch(TextFrmt){
        case D3DFMT_X8R8G8B8: return(EN_PL_RGB);
        case D3DFMT_A8R8G8B8: return(EN_PL_RGBA);
        case D3DFMT_L8      : return(EN_PL_GRAYSCALE);
        default:
            this->Exception(L" Not support DirectX pixel format, can't convert to palet format");
    }
    return(EN_PL_NOTSET);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
