///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DX8SPRITEVERTEXDATA_H_INCLUDED
#define DX8SPRITEVERTEXDATA_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../Core/Core.h"
#include "DX8Includes.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDX8SpriteVertexData:public TSpriteVertexData
{
	protected:
		IDirect3DVertexBuffer8* VertexBuffer;
	protected:
		DWORD GenDXColor(const TColor &Val);
		void GenMeshData (float *fVert);
		void GenColorData(DWORD *dwColor);
		void GenTexData  (float *fText);
	public:
		TDX8SpriteVertexData();
		virtual ~TDX8SpriteVertexData();

		void Create();
		void Delete();
		void Update();
		void Run();
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DXSPRITEVERTEXDATA_H_INCLUDED
