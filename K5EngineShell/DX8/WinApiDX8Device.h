///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef WINAPIDX8DEVICE_H_INCLUDED
#define WINAPIDX8DEVICE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include <windows.h>

#include "DX8Includes.h"

#include "../WinApi/WinApiDevice.h"

#include "DX8TextureLoader.h"
#include "DX8TextureCreater.h"
#include "DX8ScreenPixelReader.h"
#include "DX8ViewMatrixWorker.h"
#include "DX8SpriteVertexData.h"

#include "../../LibTools/Tools.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TWinApiDX8Device: public TWinApiDevice
{
	private:
		TExceptionGenerator Exception;
		D3DFORMAT Format;

		bool UseVBOFlag;

		HDC 	hDC;
		HGLRC	hRC;
		PIXELFORMATDESCRIPTOR pfd;
        D3DPRESENT_PARAMETERS D3dpp;

        bool LostDeviceFlag;
	private:
		_D3DTEXTUREFILTERTYPE GetD3D8TextureFilter(const enTextureFilter &Filter);
		void ToSetTextureFilter();

		inline bool CheckD3DFormat(const D3DFORMAT &D3DFormat);

		inline void CreateWinWindow ();
		inline void EnableDirectX   ();
		inline void ConfigureDirectX();
		inline void DisableDirectX  ();
		inline void ResetDxDevice   ();
		void ToProcessEvent         ();
	public:
		TWinApiDX8Device();
		virtual ~TWinApiDX8Device();

		void Create(const int &iWidth,const int &iHeight,
							const bool &bFullScreen = false);

		void ResetDevice();

		void ClearSceneColor();
		void SwapSceneBuffers();

		void SetDrawCursor(const bool &Val);

		void ResizeWindow(const int &Width, const int &Height);

		pTBaseTextureLoader		CreateTextureLoader();
		pTBaseTextureCreater    CreateTextureCreater();

		pTBaseScreenPixelReader CreateScreenPixelReader();
		pTBaseViewMatrixWorker 	CreateViewMatrixWorker();
		pTBaseVertexData        CreateSpriteVertexData();
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // WINAPIDX8DEVICE_H_INCLUDED
