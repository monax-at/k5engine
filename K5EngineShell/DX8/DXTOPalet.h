///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
/// Authors:
///		Sergey Kalenik    [monax.at@gmail.com]
///		Maxim Kolesnikov [mpkmaximus@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DXTOPALET_H_INCLUDED
#define DXTOPALET_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../Core/Core.h"
#include "DX8Includes.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDXPalet : public TPalet
{
	protected:
		TExceptionGenerator Exception;
		D3DFORMAT DXPalet;
	public:
		TDXPalet();
		virtual ~TDXPalet();

		inline D3DFORMAT GetDX(){return(this->DXPalet);};
		inline void SetDXPLByPL(enPaletType PLFormat){
			this->PLPalet = PLFormat;
			this->DXPalet = this->PLTODX(PLFormat);
			};

        enPaletType DXTOPL(D3DFORMAT TextFrmt);
        D3DFORMAT   PLTODX(enPaletType PaletFrmt);

};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DXTOPALET_H_INCLUDED
