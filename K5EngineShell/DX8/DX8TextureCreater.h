///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
/// Authors:
///		Maxim Kolesnikov [mpkmaximus@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DX8TEXTURECREATER_H_INCLUDED
#define DX8TEXTURECREATER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../LibTools/LibTools.h"
#include "../../Core/BaseTextureCreater.h"
#include "DX8Texture.h"

#include <string>
using std::wstring;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDX8TextureCreater : public TBaseTextureCreater
{
	private:
	protected:
	public:
		TDX8TextureCreater();
		virtual ~TDX8TextureCreater();

		pTTexture Run(const wstring &TextName, const int &WidthVal, const int &HeightVal,
					  unsigned char *TexRawData = NULL);
};

typedef TDX8TextureCreater *pTDX8TextureCreater;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DX8TEXTURECREATER_H_INCLUDED
