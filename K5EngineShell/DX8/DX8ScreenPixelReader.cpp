#include "DX8ScreenPixelReader.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDX8ScreenPixelReader::TDX8ScreenPixelReader():TBaseScreenPixelReader(),
						Exception(L"TDX8ScreenPixelReader: ")
{
}
///----------------------------------------------------------------------------------------------//
TDX8ScreenPixelReader::~TDX8ScreenPixelReader()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TColor TDX8ScreenPixelReader::ToRun(const float &X,const float &Y)
{

	D3DDEVICE_CREATION_PARAMETERS Dcp;
	Dcp.AdapterOrdinal = D3DADAPTER_DEFAULT;
	GPointer_D3DDevice8->GetCreationParameters(&Dcp);

	LPDIRECT3DSURFACE8 BufferSurface = NULL;

	HRESULT Result;
	Result = GPointer_D3DDevice8->GetBackBuffer(0,D3DBACKBUFFER_TYPE_MONO,&BufferSurface);
	Exception(Result==D3D_OK,L"in ToRun failed GetBackBuffer(...)");

	D3DLOCKED_RECT LockedRect;
	if (FAILED(BufferSurface->LockRect(&LockedRect, NULL, D3DLOCK_READONLY))){
		Exception(L"in ToRun() failed BufferSurface->LockRect(...)");
	}

	D3DSURFACE_DESC RectParams;
	if (FAILED(BufferSurface->GetDesc(&RectParams))){
		Exception(L"in ToRun() failed BufferSurface->GetDesc((...)");
	}

	int PosShift = 0;
	switch(RectParams.Format){
		case D3DFMT_R8G8B8	:{PosShift = 3;}break;
		case D3DFMT_A8R8G8B8:{PosShift = 4;}break;
		case D3DFMT_X8R8G8B8:{PosShift = 4;}break;
		default:{Exception(L"in ToRun(...) unknown RectParams.Format");}break;
	}
	BYTE* DataPointer = reinterpret_cast<BYTE*>(LockedRect.pBits);
	int Pos = PosShift*(int)((RectParams.Height - Y)*RectParams.Width + X);

	float ColorCof =1.0f/255.0f;
	float fColor[3] = {0.0f,0.0f,0.0f};

	fColor[0] = ColorCof*DataPointer[Pos+2];
	fColor[1] = ColorCof*DataPointer[Pos+1];
	fColor[2] = ColorCof*DataPointer[Pos+0];

	for(int i=0;i<3;i++){
		fColor[i] *= 100;
		float Fractional = FractionalPart(fColor[i]);
		fColor[i] = floor(fColor[i])/100;
		if(Fractional>=0.5f){
			fColor[i]+=0.01f;
		}
	}

	BufferSurface->UnlockRect();
	BufferSurface->Release();

	return TColor(fColor[0],fColor[1],fColor[2],1.0f);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
