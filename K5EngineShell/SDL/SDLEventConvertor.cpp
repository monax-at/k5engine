#include "SDLEventConvertor.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TSDLEventConvertor::TSDLEventConvertor()
{
}
///----------------------------------------------------------------------------------------------//
TSDLEventConvertor::~TSDLEventConvertor()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TSDLEventConvertor::SetKeyCode(const SDL_keysym &SDLKeysym,const pTEvent &Event)
{
	switch(SDLKeysym.sym){
		case SDLK_UP	:{ Event->Keyboard.Key = EN_KEY_UP; 	}break;
		case SDLK_DOWN	:{ Event->Keyboard.Key = EN_KEY_DOWN; 	}break;
		case SDLK_LEFT	:{ Event->Keyboard.Key = EN_KEY_LEFT; 	}break;
		case SDLK_RIGHT	:{ Event->Keyboard.Key = EN_KEY_RIGHT; 	}break;

		case SDLK_0	:{ Event->Keyboard.Key = EN_KEY_0; 	}break;
		case SDLK_1	:{ Event->Keyboard.Key = EN_KEY_1; 	}break;
		case SDLK_2	:{ Event->Keyboard.Key = EN_KEY_2; 	}break;
		case SDLK_3	:{ Event->Keyboard.Key = EN_KEY_3; 	}break;
		case SDLK_4	:{ Event->Keyboard.Key = EN_KEY_4; 	}break;
		case SDLK_5	:{ Event->Keyboard.Key = EN_KEY_5; 	}break;
		case SDLK_6	:{ Event->Keyboard.Key = EN_KEY_6; 	}break;
		case SDLK_7	:{ Event->Keyboard.Key = EN_KEY_7; 	}break;
		case SDLK_8	:{ Event->Keyboard.Key = EN_KEY_8; 	}break;
		case SDLK_9	:{ Event->Keyboard.Key = EN_KEY_9; 	}break;

		case SDLK_a	:{ Event->Keyboard.Key = EN_KEY_A; 	}break;
		case SDLK_b	:{ Event->Keyboard.Key = EN_KEY_B; 	}break;
		case SDLK_c	:{ Event->Keyboard.Key = EN_KEY_C; 	}break;
		case SDLK_d	:{ Event->Keyboard.Key = EN_KEY_D; 	}break;
		case SDLK_f	:{ Event->Keyboard.Key = EN_KEY_F; 	}break;
		case SDLK_g	:{ Event->Keyboard.Key = EN_KEY_G; 	}break;
		case SDLK_h	:{ Event->Keyboard.Key = EN_KEY_H; 	}break;
		case SDLK_i	:{ Event->Keyboard.Key = EN_KEY_I; 	}break;
		case SDLK_j	:{ Event->Keyboard.Key = EN_KEY_J; 	}break;
		case SDLK_k	:{ Event->Keyboard.Key = EN_KEY_K; 	}break;
		case SDLK_l	:{ Event->Keyboard.Key = EN_KEY_L; 	}break;
		case SDLK_m	:{ Event->Keyboard.Key = EN_KEY_M; 	}break;
		case SDLK_n	:{ Event->Keyboard.Key = EN_KEY_N; 	}break;
		case SDLK_o	:{ Event->Keyboard.Key = EN_KEY_O; 	}break;
		case SDLK_p	:{ Event->Keyboard.Key = EN_KEY_P; 	}break;
		case SDLK_q	:{ Event->Keyboard.Key = EN_KEY_Q; 	}break;
		case SDLK_r	:{ Event->Keyboard.Key = EN_KEY_R; 	}break;
		case SDLK_s	:{ Event->Keyboard.Key = EN_KEY_S; 	}break;
		case SDLK_t	:{ Event->Keyboard.Key = EN_KEY_T; 	}break;
		case SDLK_u	:{ Event->Keyboard.Key = EN_KEY_U; 	}break;
		case SDLK_v	:{ Event->Keyboard.Key = EN_KEY_V; 	}break;
		case SDLK_w	:{ Event->Keyboard.Key = EN_KEY_W; 	}break;
		case SDLK_x	:{ Event->Keyboard.Key = EN_KEY_X; 	}break;
		case SDLK_y	:{ Event->Keyboard.Key = EN_KEY_Y; 	}break;
		case SDLK_z	:{ Event->Keyboard.Key = EN_KEY_Z; 	}break;

		case SDLK_KP_ENTER	:{ Event->Keyboard.Key = EN_KEY_ENTER; 	}break;
		case SDLK_SPACE		:{ Event->Keyboard.Key = EN_KEY_SPACE; 	}break;
		case SDLK_ESCAPE	:{ Event->Keyboard.Key = EN_KEY_ESCAPE; }break;
		case SDLK_RCTRL		:{ Event->Keyboard.Key = EN_KEY_CTRL; 	}break;
		case SDLK_LCTRL		:{ Event->Keyboard.Key = EN_KEY_CTRL; 	}break;
		case SDLK_RALT		:{ Event->Keyboard.Key = EN_KEY_RALT; 	}break;
		case SDLK_LALT		:{ Event->Keyboard.Key = EN_KEY_LALT; 	}break;
		case SDLK_RSHIFT	:{ Event->Keyboard.Key = EN_KEY_SHIFT; 	}break;
		case SDLK_LSHIFT	:{ Event->Keyboard.Key = EN_KEY_SHIFT; 	}break;
		case SDLK_TAB		:{ Event->Keyboard.Key = EN_KEY_TAB; 	}break;

		default:break;
	}
}
///----------------------------------------------------------------------------------------------//
void TSDLEventConvertor::SetMouseBtn(const SDL_MouseButtonEvent &SDLButton,
									 const pTEvent &Event)
{
	switch(SDLButton.button){
		case SDL_BUTTON_LEFT	  :{Event->Mouse.Button = EN_MOUSEBUTTONLEFT;  }break;
		case SDL_BUTTON_MIDDLE	  :{Event->Mouse.Button = EN_MOUSEBUTTONMIDDLE;}break;
		case SDL_BUTTON_RIGHT	  :{Event->Mouse.Button = EN_MOUSEBUTTONRIGHT; }break;
		default:break;
	}

	Event->Mouse.Set(SDLButton.x, SDLButton.y);
}
///----------------------------------------------------------------------------------------------//
TEvent TSDLEventConvertor::operator()(const SDL_Event &SDLEvent)
{
	TEvent Event;
	Event = EN_EVENTTRUE;

	switch(SDLEvent.type){

		case SDL_ACTIVEEVENT:
		break;

		case SDL_KEYDOWN:
			Event.Type = EN_KEYBOARD;
			Event.Keyboard.Type = EN_KEYDOWN;
			SetKeyCode(SDLEvent.key.keysym,&Event);
		break;

		case SDL_KEYUP:
			Event.Type = EN_KEYBOARD;
			Event.Keyboard.Type = EN_KEYUP;
			SetKeyCode(SDLEvent.key.keysym,&Event);
		break;

		case SDL_MOUSEMOTION:
			Event.Type = EN_MOUSE;
			Event.Mouse.Type = EN_MOUSEMOTION;

			Event.Mouse.Set(SDLEvent.motion.x,SDLEvent.motion.y);
		break;

		case SDL_MOUSEBUTTONDOWN:
			Event.Type = EN_MOUSE;
			if(	SDLEvent.button.button != SDL_BUTTON_WHEELUP &&
				SDLEvent.button.button != SDL_BUTTON_WHEELDOWN)
			{
				Event.Mouse.Type = EN_MOUSEBUTTONDOWN;
				SetMouseBtn(SDLEvent.button, &Event);
			}
			else{
				if(SDLEvent.button.button == SDL_BUTTON_WHEELUP  ){
					Event.Mouse.Type = EN_MOUSEWHEELUP  ;
				}

				if(SDLEvent.button.button == SDL_BUTTON_WHEELDOWN){
					Event.Mouse.Type = EN_MOUSEWHEELDOWN;
				}
			}
		break;

		case SDL_MOUSEBUTTONUP:
			Event.Type = EN_MOUSE;
			Event.Mouse.Type = EN_MOUSEBUTTONUP;
			SetMouseBtn(SDLEvent.button,&Event);
		break;

		case SDL_QUIT:
			Event.Type = EN_SYSTEM;
			Event.System.Type = EN_QUIT;
		break;

		case SDL_SYSWMEVENT:
		break;

		case SDL_VIDEORESIZE:
		break;

		case SDL_VIDEOEXPOSE:
		break;
	}

	return Event;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
