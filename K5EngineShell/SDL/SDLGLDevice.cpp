#include "SDLGLDevice.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TSDLGLDevice::TSDLGLDevice():TBaseDevice(),	Screen(NULL), TimerCounter(0.0f)
{
	Exception.SetPrefix(L"TSDLGLDevice: ");
}
///----------------------------------------------------------------------------------------------//
TSDLGLDevice::~TSDLGLDevice()
{
	SDL_Quit();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
unsigned int TSDLGLDevice::GetGLTextureFilter(const enTextureFilter &Filter)
{
	switch(Filter)
	{
		default			 : return GL_NEAREST; break;
		case EN_TF_Linear: return GL_LINEAR ; break;
	}

	return GL_NEAREST;
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::ToSetTextureFilter()
{
/*
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
					GetGLTextureFilter(MinTextureFilter));

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,
					GetGLTextureFilter(MagTextureFilter));
*/
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::ToSetCaption()
{
	if(Screen != NULL){SDL_WM_SetCaption(WStrToStr(Caption).c_str(),NULL);}
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::SetGLStates()
{
	glViewport(0,0,Width,Height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0,Width,Height,0, 0, -100);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_CW);

	glFrontFace(GL_FRONT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER,0.0f);

	glShadeModel(GL_SMOOTH);

	glMatrixMode(GL_TEXTURE);
	glRotatef(180.0f,1.0f,0.0f,0.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	ToSetTextureFilter();
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::InitVBO()
{
	TGLExtensionWorker ExtWorker;
	ExtWorker.InitVBO();
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::MinimizeWindow()
{
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::RestoreWindow()
{
}
///----------------------------------------------------------------------------------------------//
TPoint TSDLGLDevice::GetCursor() const
{
	return CursorPos;
}
///----------------------------------------------------------------------------------------------//
TPoint TSDLGLDevice::GetGlobalCursor() const
{
	return TPoint();
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::Create(	const int &NewWidth,const int &NewHeight,
							const bool &NewFullScreen)
{
	Width = NewWidth;
	Height = NewHeight;

	Exception(	SDL_Init( SDL_INIT_VIDEO | SDL_INIT_TIMER ) >=0,
				L"in Create(...) SDL_Init crash" );

	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

	if(NewFullScreen){
		Screen = SDL_SetVideoMode(NewWidth, NewHeight, 32, SDL_OPENGL | SDL_FULLSCREEN);
	}
	else{
		Screen = SDL_SetVideoMode(NewWidth, NewHeight, 32, SDL_OPENGL);
	}
	Exception(Screen!=NULL, L"in Create(...) can`t SDL_SetVideoMode() ");

	SDL_ShowCursor(DrawCursor);
	SDL_WM_SetCaption(WStrToStr(Caption).c_str(),NULL);
	SDL_EnableUNICODE(1);
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,SDL_DEFAULT_REPEAT_INTERVAL);

	SetGLStates();
	InitVBO();

	DevInitWorker();
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::ClearSceneColor()
{
	glClearColor(	ClearColor.Get(0), ClearColor.Get(1),
					ClearColor.Get(2), ClearColor.Get(3));

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::SwapSceneBuffers()
{
	SDL_GL_SwapBuffers();
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::AddTimerEvent(const float &Time)
{
	TEvent Event;
	Event		    = EN_EVENTTRUE;
	Event.Type		= EN_TIMER;

	Event.Timer.Sec = Time/1000.0f;
	Event.Timer.Ms 	= Time;

	EventDeque->Push(Event);
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::ProcessEvent()
{
	// обработка событий системы
	SDL_Event SDLEvent;

	TSDLEventConvertor EventConvertor;

	while(SDL_PollEvent(&SDLEvent)){
		TEvent Event(EventConvertor(SDLEvent));

		if(Event.Type == EN_MOUSE){ CursorPos(Event.Mouse.X,Event.Mouse.Y);}

		if(Event.Type != EN_NONE){ EventDeque->Push(Event); }
	}

	// обработка таймера
	float Time((float)SDL_GetTicks());
	float DeltaTime(Time - TimerCounter);

	TimerCounter = Time;

	if(TimerEventDelay <= 0.0f){
		AddTimerEvent(DeltaTime);
	}
	else{
		TimerEventDelayCounter += DeltaTime;

		if(TimerEventDelayCounter >= TimerEventDelay){

			TimerEventDelayCounter = 0.0f;
			AddTimerEvent(TimerEventDelay);
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::SetDrawCursor(const bool &Val)
{
	DrawCursor = Val;
	if(Screen != NULL){SDL_ShowCursor(DrawCursor);}
}
///----------------------------------------------------------------------------------------------//
void TSDLGLDevice::ResetTimer()
{
	TimerCounter = (float)SDL_GetTicks();
}
///----------------------------------------------------------------------------------------------//
pTBaseTextureLoader TSDLGLDevice::CreateTextureLoader()
{
	return new TDevGLTextureLoader;
}
///----------------------------------------------------------------------------------------------//
pTBaseTextureCreater TSDLGLDevice::CreateTextureCreater()
{
	return new TGLTextureCreater;
}
///----------------------------------------------------------------------------------------------//
pTBaseScreenPixelReader TSDLGLDevice::CreateScreenPixelReader()
{
	return new TGLScreenPixelReader;
}
///----------------------------------------------------------------------------------------------//
pTBaseViewMatrixWorker TSDLGLDevice::CreateViewMatrixWorker()
{
	return new TGLViewMatrixWorker;
}
///----------------------------------------------------------------------------------------------//
pTBaseVertexData TSDLGLDevice::CreateSpriteVertexData()
{
	return new TGLSpriteVertexData;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

