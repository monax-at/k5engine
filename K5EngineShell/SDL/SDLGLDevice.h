///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef SDLGLDEVICE_H_INCLUDED
#define SDLGLDEVICE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "SDLEventConvertor.h"

#include "../OpenGL/GLIncludes.h"

#include "../OpenGL/GLExtensionWorker.h"
#include "../OpenGL/GLScreenPixelReader.h"
#include "../OpenGL/GLTextureCreater.h"
#include "../OpenGL/GLViewMatrixWorker.h"
#include "../OpenGL/GLSpriteVertexData.h"

#include "../DevIL/DevGLTextureLoader.h"
#include "../DevIL/DevInitWorker.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TSDLGLDevice:public TBaseDevice
{
	protected:
		SDL_Surface* Screen;

		TDevInitWorker DevInitWorker;

		float TimerCounter;

		TPoint CursorPos;
	protected:
		unsigned int GetGLTextureFilter(const enTextureFilter &Filter);
		void ToSetTextureFilter();

		void ToSetCaption();

		inline void SetGLStates();
		inline void InitVBO();
		inline void AddTimerEvent(const float &Time);
	public:
		TSDLGLDevice();
		virtual ~TSDLGLDevice();

		void MinimizeWindow();
		void RestoreWindow();

		TPoint GetCursor() const;
		TPoint GetGlobalCursor() const;

		void Create(const int &NewWidth,const int &NewHeight,
					const bool &NewFullScreen = false);

		void ClearSceneColor();
		void SwapSceneBuffers();
		void ProcessEvent();

		void SetDrawCursor(const bool &Val);

		void ResetTimer();

		pTBaseTextureLoader 	CreateTextureLoader();
		pTBaseTextureCreater    CreateTextureCreater();

		pTBaseScreenPixelReader CreateScreenPixelReader();
		pTBaseViewMatrixWorker 	CreateViewMatrixWorker();
		pTBaseVertexData        CreateSpriteVertexData();
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // SDLGLDEVICE_H_INCLUDED
