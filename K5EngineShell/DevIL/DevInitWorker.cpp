#include "DevInitWorker.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TDevInitWorker::TDevInitWorker()
{
	Exception.SetPrefix(L"TDevInitWorker: ");
}
///--------------------------------------------------------------------------------------//
TDevInitWorker::~TDevInitWorker()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TDevInitWorker::CheckVersion()
{
    if( ilGetInteger( IL_VERSION_NUM ) < IL_VERSION )
        Exception(L"Incorrect devil.dll version.");

    if( iluGetInteger( ILU_VERSION_NUM ) < ILU_VERSION )
        Exception(L"Incorrect ilu.dll version.");

    if( ilutGetInteger( ILUT_VERSION_NUM ) < ILUT_VERSION )
        Exception(L"Incorrect ilut.dll version.");
}
///--------------------------------------------------------------------------------------//
void TDevInitWorker::Init()
{
    ilInit();
    iluInit();
    ilutInit();

    ilutRenderer ( ILUT_OPENGL );
    ilSetInteger ( IL_KEEP_DXTC_DATA, IL_TRUE );
    ilutEnable   ( ILUT_GL_AUTODETECT_TEXTURE_TARGET );
    ilutEnable   ( ILUT_OPENGL_CONV );
    ilutEnable   ( ILUT_GL_USE_S3TC );

    CheckVersion();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TDevInitWorker::operator()()
{
	Init();
}
///--------------------------------------------------------------------------------------//
void TDevInitWorker::Run()
{
	Init();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
