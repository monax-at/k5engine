///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
/// Authors:
///		Sergey Kalenik    [monax.at@gmail.com]
///		Maxim Kolesnikov [mpkmaximus@gmail.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef ILTOPALET_H_INCLUDED
#define ILTOPALET_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Core/Core.h"
#include "../../LibTools/LibTools.h"
#include "DevILIncludes.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TILPalet : public TPalet
{
	private:
		TExceptionGenerator Exception;
	protected:
		ILenum  ILPalet;
	public:
		TILPalet():TPalet(),Exception(L"TDXPalet"){ILPalet = IL_TYPE_UNKNOWN;};

		inline ILenum GetIL(){return(this->ILPalet);};
		inline void SetILByPL(enPaletType PLFormat){
			this->PLPalet = PLFormat;
			this->ILPalet = PLTOIL(PLFormat);
			};
		inline void SetPLByIL(ILenum ILFormat){
			this->PLPalet = ILTOPL(ILFormat);
			this->ILPalet = ILFormat;
		};

        enPaletType ILTOPL(ILenum PaletFrmt);
        ILenum   PLTOIL(enPaletType PaletFrmt);

};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // ILTOPALET_H_INCLUDED
