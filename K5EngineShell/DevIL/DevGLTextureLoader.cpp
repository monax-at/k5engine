#include "DevGLTextureLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TDevGLTextureLoader::TDevGLTextureLoader():TBaseTextureLoader()
{
	Exception.SetPrefix(L"TDevGLTextureLoader :");
}
///--------------------------------------------------------------------------------------//
TDevGLTextureLoader::~TDevGLTextureLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTTexture TDevGLTextureLoader::Run(	const wstring &FileName,const int &PosX,
									const int &PosY, const int &CanvasWidth,
                                    const int &CanvasHeight,const wstring &TexName)
{
    return(NULL);
}
///--------------------------------------------------------------------------------------//
pTTexture TDevGLTextureLoader::Run(const wstring &FileName,const wstring &TexName)
{
	Exception(	FileName.length() > 0,
				L"in Run(FileName,TexName) Texture file name not set");

	//wchar_t *ILFileName = const_cast < wchar_t *> (FileName.c_str());
	string sFileName;
	if(UseEngineFilePathFormat){sFileName = (WStrToStr(BuildPathStr(FileName)));}
	else                       {sFileName = (WStrToStr(FileName));}

	char* ILFileName = const_cast<char*>(sFileName.c_str());

	GLuint TexId(ilutGLLoadImage(ILFileName));

	Exception(	ilGetError() == IL_NO_ERROR,
				L"in Run(FileName,TexName) Can't load texture: "+FileName);

	return new TGLTexture(TexName,TexId);
}
///--------------------------------------------------------------------------------------//
pTTexture TDevGLTextureLoader::Mask(const wstring &FileName,const wstring &TexName)
{
	pTTexture Texture(Run(FileName,TexName));
	pTTexture Mask(Texture->CreateCutMask(TexName));

	Texture->Del();
	delete Texture;
	return Mask;
}
///--------------------------------------------------------------------------------------//
void TDevGLTextureLoader::Run (	const pTTexture &Texture, const wstring &FileName,
					const wstring &TexName)
{
}
///--------------------------------------------------------------------------------------//
void TDevGLTextureLoader::Mask(	const pTTexture &Texture, const wstring &FileName,
					const wstring &TexName)
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
