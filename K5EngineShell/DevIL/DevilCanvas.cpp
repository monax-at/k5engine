#include "DevilCanvas.h"
#include "../../LibTools/LibTools.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TDevilCanvas::TDevilCanvas(const wstring &Name)
{
    this->Init();
///--------->
	string sFileName(WStrToStr(Name));
	char* ILFileName = const_cast<char*>(sFileName.c_str());

	Exception(	ilLoadImage(ILFileName) != IL_FALSE,
				L"Can't load images from file:"+Name);

	Palet.SetPLByIL(ilGetInteger( IL_IMAGE_FORMAT));

	Exception(Palet.CheckPL(EN_PL_NOTSET) != true,L"Not support pixel format, can't convert to palet format. Devil palet format:%i"+IntToWStr(ilGetInteger( IL_IMAGE_FORMAT)));
}
///--------------------------------------------------------------------------------------//
TDevilCanvas::TDevilCanvas(const pTTexture &TextureVal)
{
	Init();
///--------->
	int Width  = TextureVal->GetWidth();
	int Height = TextureVal->GetHeight();
	pTTextureRawData pTexRawData = TextureVal->GetCopyRawData(Width, Height);

    if(ilTexImage(Width, Height, 1, 4, IL_RGBA, IL_UNSIGNED_BYTE, pTexRawData->TextureData) != IL_TRUE)
            this->Exception(L"Can't create image");

	delete pTexRawData;
}
///--------------------------------------------------------------------------------------//
TDevilCanvas::TDevilCanvas(const int iWidht, const int iHeight, enPaletType PaletVal)
{
	Init();
///--------->
	Palet.SetILByPL(PaletVal);

    int Size = iWidht * iHeight *(int)Palet.GetPL();
    unsigned char *ImageData = new unsigned char[Size];
    memset(ImageData,0,Size);

    if(ilTexImage(iWidht, iHeight, 1, 4, IL_RGBA, IL_UNSIGNED_BYTE, ImageData) != IL_TRUE)
            this->Exception(L"Can't create image");

	delete[] ImageData;
}
///--------------------------------------------------------------------------------------//
TDevilCanvas::~TDevilCanvas()
{
    this->BindImage();
    ilDeleteImage(this->ImgId);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TDevilCanvas::Init()
{
	Exception.SetPrefix(L"TDevilCanvas: ");

    this->ImgId     = 0;
//-----> ��������� ����� �����������
    ilGenImages(1, &this->ImgId);
    ilBindImage(this->ImgId);
}
///--------------------------------------------------------------------------------------//
void TDevilCanvas::SetActive()
{
    this->BindImage();
}
///--------------------------------------------------------------------------------------//
void TDevilCanvas::BindImage()
{
    ilBindImage(this->ImgId);
    int iErrorCode = ilGetError();
    if( iErrorCode == IL_ILLEGAL_OPERATION){
        this->Exception(L"Can't bind image");
    }
}
///--------------------------------------------------------------------------------------//
int TDevilCanvas::GetWidth()
{
	BindImage();
	return ilGetInteger ( IL_IMAGE_WIDTH );
}
///--------------------------------------------------------------------------------------//
int TDevilCanvas::GetHeight()
{
	BindImage();
	return ilGetInteger ( IL_IMAGE_HEIGHT );
}
///--------------------------------------------------------------------------------------//
unsigned char *TDevilCanvas::GetImageData()
{
    this->BindImage();
    return (ilGetData());
}
///--------------------------------------------------------------------------------------//
void TDevilCanvas::SaveImage(const wstring &Name, ILuint FileType)
{
    this->BindImage();

	string sFileName(WStrToStr(Name));
	char* ILFileName = const_cast<char*>(sFileName.c_str());

	Exception(ilSave(FileType, ILFileName) == IL_TRUE,
				L"Can't vase image:"+Name);
}
///--------------------------------------------------------------------------------------//
void TDevilCanvas::SavePNGImage(const wstring &Name)
{
	SaveImage(Name, IL_PNG);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//

