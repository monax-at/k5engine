///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
/// Authors:
///		Sergey Kalenik    [monax.at@gmail.com]
///		Maxim Kolesnikov [mpkmaximus@gmail.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef DEVGLTEXTURELOADER_H_INCLUDED
#define DEVGLTEXTURELOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../OpenGL/GLTexture.h"

#include "DevILIncludes.h"
#include "ILTOPalet.h"

#include "../../LibTools/LibTools.h"

#include <string>
using std::wstring;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TDevGLTextureLoader: public TBaseTextureLoader
{
    public:
        TDevGLTextureLoader();
        virtual ~TDevGLTextureLoader();

        pTTexture Run(	const wstring &FileName, const int &PosX, const int &PosY,
						const int &Width, const int &Height,const wstring &TexName=L"");

        pTTexture Run(const wstring &FileName, const wstring &TexName=L"");
        pTTexture Mask(const wstring &FileName,const wstring &TexName=L"");

		void Run (	const pTTexture &Texture, const wstring &FileName,
					const wstring &TexName=L"");

		void Mask(	const pTTexture &Texture, const wstring &FileName,
					const wstring &TexName=L"");
};

typedef TDevGLTextureLoader* pTDevGLTextureLoader;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // DEVGLTEXTURELOADER_H_INCLUDED
