#pragma once
#ifndef TDEVILCANVASWORKER_H
#define TDEVILCANVASWORKER_H
///--------------------------------------------------------------------------------------//
#include "DevilCanvas.h"
///--------------------------------------------------------------------------------------//
class TDevilCanvasWorker
{
    protected:
        TExceptionGenerator Exception;
    public:
        TDevilCanvasWorker();
        virtual ~TDevilCanvasWorker();
///-----> Клонирование конвы
        TDevilCanvas *Clone(TDevilCanvas *Source);
///-----> Копирование конвы
        void Copy(TDevilCanvas *Source, TDevilCanvas *Destination, const int &PosX = 0, const int &PosY = 0);
///-----> Отимизация конвы
		void Optimization(TDevilCanvas *Source);
///-----> Переворачиваем изображение
		void Mirror(TDevilCanvas *Source);
///-----> Обрезание изображения по альфе и возвращает позицию обьекта на изображении
		TPoint CropByAlpha(const pTDevilCanvas &Source, const int &AlphaVal = 0,
						   const int &WidthShift = 0, const int &HeightShift = 0, const bool DivisibleTwo = true);

};
typedef TDevilCanvasWorker *pTDevilCanvasWorker;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//

#endif // TDEVILCANVASWORKER_H
