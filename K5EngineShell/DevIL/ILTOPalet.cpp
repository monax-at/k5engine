///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "ILTOPalet.h"
///--------------------------------------------------------------------------------------//
enPaletType TILPalet::ILTOPL(ILenum PaletFrmt)
{
    switch(PaletFrmt){
        case IL_RGB:       return(EN_PL_RGB);
        case IL_BGR:       return(EN_PL_RGB);
        case IL_RGBA:      return(EN_PL_RGBA);
        case IL_BGRA:      return(EN_PL_RGBA);
        case IL_LUMINANCE: return(EN_PL_GRAYSCALE);
        default:
            Exception(FormatWStr(L"Not support pixel format, can't convert to palet format. Devil palet format:%i",PaletFrmt));
    }
    return(EN_PL_NOTSET);
}
///--------------------------------------------------------------------------------------//
ILenum   TILPalet::PLTOIL(enPaletType PaletFrmt)
{
    switch(PaletFrmt){
        case EN_PL_RGB:       return(IL_RGB);
        case EN_PL_RGBA:      return(IL_RGBA);
        case EN_PL_GRAYSCALE: return(IL_LUMINANCE);
        default:
            Exception(L"Unknow palet type, can't convert to 'Devil' format");
    }
    return(0);
}
///--------------------------------------------------------------------------------------//
