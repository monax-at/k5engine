///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef DEVILSHELL_H_INCLUDED
#define DEVILSHELL_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "DevGLTextureLoader.h"
#include "DevilCanvas.h"
#include "DevilCanvasWorker.h"
#include "DevILIncludes.h"
#include "DevInitWorker.h"
#include "ILTOPalet.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // DEVILSHELL_H_INCLUDED
