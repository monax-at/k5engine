///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
/// Authors:
///		Sergey Kalenik    [monax.at@gmail.com]
///		Maxim Kolesnikov [mpkmaximus@gmail.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef TDEVILCANVAS_H
#define TDEVILCANVAS_H
///--------------------------------------------------------------------------------------//
#include "ILTOPalet.h"
#include "DevILIncludes.h"

#include <string>
using std::wstring;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
typedef struct
{
    unsigned char R;
    unsigned char G;
    unsigned char B;
    unsigned char A;
}TRGBAPixel, *pTRGBAPixel;
///--------------------------------------------------------------------------------------//
typedef struct
{
    unsigned char R;
    unsigned char G;
    unsigned char B;
}TRGBPixel, *pTRGBPixel;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TDevilCanvas
{
    private:
        TExceptionGenerator Exception;
///---->
        void Init();
        inline void BindImage();

        ILuint      ImgId;

        TILPalet    Palet;
    public:
    	TDevilCanvas();
        TDevilCanvas(const wstring &Name);
        TDevilCanvas(const pTTexture &TextureVal);
        TDevilCanvas(const int iWidht, const int iHeight, enPaletType PaletVal = EN_PL_RGBA);

        virtual ~TDevilCanvas();

        void SetActive();

        inline enPaletType GetPalet()  {return Palet.GetPL(); };
        inline ILuint      GetId()     {return ImgId;		  };
        int GetWidth();
        int GetHeight();


        unsigned char *GetImageData();
        void SaveImage(const wstring &Name, ILuint FileType);
        void SavePNGImage(const wstring &Name);
};
typedef TDevilCanvas *pTDevilCanvas;
///--------------------------------------------------------------------------------------//
#endif // TDEVILCANVAS_H
