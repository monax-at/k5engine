#include "DevilCanvasWorker.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TDevilCanvasWorker::TDevilCanvasWorker()
{
}
///--------------------------------------------------------------------------------------//
TDevilCanvasWorker::~TDevilCanvasWorker()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TDevilCanvas *TDevilCanvasWorker::Clone(TDevilCanvas *Source)
{
    TDevilCanvas *Canvas = new TDevilCanvas(Source->GetWidth(), Source->GetHeight(), Source->GetPalet());
    this->Copy(Source,Canvas);
    return(Canvas);
}
///--------------------------------------------------------------------------------------//
void TDevilCanvasWorker::Copy(TDevilCanvas *Source, TDevilCanvas *Destination,const int &PosX, const int &PosY )
{
    int SourWidth  = Source->GetWidth();
    int SourHeight = Source->GetHeight();
///----> ��������� �������� �� ����������� �����
    if(((PosX + SourWidth) > Destination->GetWidth()) || ((PosY + SourHeight) > Destination->GetHeight()))
        this->Exception(L"Can't copy canvas. Destination canvas is small.");
    if(Source->GetPalet() != Destination->GetPalet())
        this->Exception(L"Can't copy canvas. Source and destination canvas has different palet data");
///----> ������������� �������� �����������
    Destination->SetActive();
///----> ����������� �����
	ilDisable(IL_BLIT_BLEND);
    if(ilBlit(Source->GetId(),PosX, PosY, 0, 0, 0, 0,SourWidth, SourHeight, 1) == IL_FALSE){
		ilEnable(IL_BLIT_BLEND);
        this->Exception(L"Can't copy images invalid parameters.");
    }
	ilEnable(IL_BLIT_BLEND);
}
///--------------------------------------------------------------------------------------//
void TDevilCanvasWorker::Optimization(TDevilCanvas *Source)
{
	Exception(Source->GetPalet() == EN_PL_RGBA, L"Can't start optimization, bad palet");

    int Width  = Source->GetWidth();
    int Height = Source->GetHeight();

	pTRGBAPixel ImageData = (pTRGBAPixel)Source->GetImageData();
    for(int iY = 0 ;iY < Height; iY++){
        for(int iX = 0; iX < Width; iX++){
			unsigned long CurPos = ((iY * Width) + iX);
            if(!ImageData[CurPos].A){
				int Count = 0, R = 0, G = 0, B = 0;
				///----> ����� ������� ���� ���� ������� �����
				for(int iTY=-1; iTY<=1; iTY++){
					for(int iTX=-1; iTX<=1; iTX++){
						if((iY + iTY >= 0) && (iY  + iTY < Height)&&  (iX + iTX >= 0) && (iX + iTX < Width) &&  (ImageData[(iY + iTY) * Width + (iX + iTX)].A)){
							R += ImageData[(iY + iTY) * Width + (iX + iTX)].R;
							B += ImageData[(iY + iTY) * Width + (iX + iTX)].G;
							B += ImageData[(iY + iTY) * Width + (iX + iTX)].B;
							Count++;
						}
					}
				}
				///----> ����������� ���������� ���� �������
				if(Count > 0){
					ImageData[CurPos].R = (unsigned char)(R / Count);
					ImageData[CurPos].G = (unsigned char)(G / Count);
					ImageData[CurPos].B = (unsigned char)(B / Count);
				}

            }
        }
    }
}
///--------------------------------------------------------------------------------------//
void TDevilCanvasWorker::Mirror(TDevilCanvas *Source)
{
	Source->SetActive();
	iluFlipImage();
	//iluMirror();
}
///--------------------------------------------------------------------------------------//
TPoint TDevilCanvasWorker::CropByAlpha(const pTDevilCanvas &Source, const int &AlphaVal, const int &WidthShift, const int &HeightShift, const bool DivisibleTwo)
{
	Exception(Source->GetPalet() == EN_PL_RGBA, L"Can't resize by alpha, bad palet");

	int StartX = Source->GetWidth();
	int StartY = Source->GetHeight();
	int EndX   = 0;
	int EndY   = 0;

    int WidthSource  = Source->GetWidth();
    int HeightSource = Source->GetHeight();


    pTRGBAPixel ImageData = (pTRGBAPixel)Source->GetImageData();
    for(int iY = 0 ;iY < HeightSource; iY++){
        for(int iX = 0; iX < WidthSource; iX++){
///----> ����������� �����
            unsigned long CurPos = ((iY * WidthSource) + iX);
            if(ImageData[CurPos].A > AlphaVal){
///----> ������� ������� ����� ������� �� X
                if(StartX > iX) StartX = iX;
                if((EndX < iX) && (iX > StartX)) EndX = iX;
///----> ������� ������� ����� ������� �� X
                if(StartY > iY) StartY = iY;
                if((EndY < iY) && (iY > StartY)) EndY = iY;
            }
        }
    }
///----> ��������� ������ � ������
    int Width  = EndX - StartX;
    int Height = EndY - StartY;
///----> ������������� ����������� ����� ����������� ��� �� �������� �������� �����������
    if((EndX + 1) < WidthSource)  Width  = (EndX + 1) - StartX;
    if((EndX + 1) < HeightSource) Height = (EndY + 1) - StartY;
///----> ���� ��������� �������� �������� ���������� �������
    Width  = Width  + WidthShift;
    Height = Height + HeightShift;
///----> ��������� �������
	Exception(Width  < Source->GetWidth()  , L"Incorect width. Final width larger source width.");
	Exception(Height < Source->GetHeight() , L"Incorect height. Final height larger source height.");
///----> ����������� ������ � ������ ��� ��������� ������
	if(DivisibleTwo == true){
		if(Width  % 2 > 0.0f) Width  = Width  + 1;
		if(Height % 2 > 0.0f) Height = Height + 1;
	}
///----> �������� ������� �����������
	Source->SetActive();
	iluCrop(StartX, StartY, 0, Width, Height, 0);

	return TPoint((float)(StartX + (Width / 2)), (float)(StartY + (Height / 2)), 0.0f);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
