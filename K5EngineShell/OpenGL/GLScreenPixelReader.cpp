#include "GLScreenPixelReader.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TGLScreenPixelReader::TGLScreenPixelReader():TBaseScreenPixelReader()
{
}
///----------------------------------------------------------------------------------------------//
TGLScreenPixelReader::~TGLScreenPixelReader()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TColor TGLScreenPixelReader::ToRun(const float &X,const float &Y)
{
	float fColor[4] = {0.0f,0.0f,0.0f,0.0f};

	glReadPixels((int)X,(int)Y, 1, 1, GL_RGBA, GL_FLOAT, fColor);

	for(int i=0;i<4;i++){
		fColor[i] *= 100;
		float Fractional = FractionalPart(fColor[i]);
		fColor[i] = floor(fColor[i])/100;
		if(Fractional>=0.5f){
			fColor[i]+=0.01f;
		}
	}

	return TColor(fColor[0],fColor[1],fColor[2],1.0f);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
