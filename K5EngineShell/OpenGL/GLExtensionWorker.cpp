#include "GLExtensionWorker.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TGLExtensionWorker::TGLExtensionWorker()
{
}
///----------------------------------------------------------------------------------------------//
TGLExtensionWorker::~TGLExtensionWorker()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void* TGLExtensionWorker::GetProcAddress(const string &ProcName)
{
	#ifdef _WIN32
		return (void*)wglGetProcAddress(ProcName.c_str());
	#else
		return (void*)glXGetProcAddressARB((const GLubyte*)ProcName.c_str());
	#endif
}
///----------------------------------------------------------------------------------------------//
bool TGLExtensionWorker::InitVBO()
{
//	string Vendor 		= (char*)glGetString(GL_VENDOR);
//	string Version 		= (char*)glGetString(GL_VERSION);
//	string Renderer 	= (char*)glGetString(GL_RENDERER);
//	string Extensions 	= (char*)glGetString(GL_EXTENSIONS);


    glBindBufferARB				= (PFNGLBINDBUFFERARBPROC)           GetProcAddress("glBindBufferARB");
    glDeleteBuffersARB			= (PFNGLDELETEBUFFERSARBPROC)        GetProcAddress("glDeleteBuffersARB");
    glGenBuffersARB				= (PFNGLGENBUFFERSARBPROC)           GetProcAddress("glGenBuffersARB");
    glIsBufferARB				= (PFNGLISBUFFERARBPROC)             GetProcAddress("glIsBufferARB");
    glBufferDataARB				= (PFNGLBUFFERDATAARBPROC)           GetProcAddress("glBufferDataARB");
    glBufferSubDataARB			= (PFNGLBUFFERSUBDATAARBPROC)        GetProcAddress("glBufferSubDataARB");
    glGetBufferSubDataARB		= (PFNGLGETBUFFERSUBDATAARBPROC)     GetProcAddress("glGetBufferSubDataARB");
    glMapBufferARB				= (PFNGLMAPBUFFERARBPROC)            GetProcAddress("glMapBufferARB");
    glUnmapBufferARB			= (PFNGLUNMAPBUFFERARBPROC)          GetProcAddress("glUnmapBufferARB");
    glGetBufferParameterivARB	= (PFNGLGETBUFFERPARAMETERIVARBPROC) GetProcAddress("glGetBufferParameterivARB");
    glGetBufferPointervARB		= (PFNGLGETBUFFERPOINTERVARBPROC)    GetProcAddress("glGetBufferPointervARB");

    if(	glBindBufferARB 		== NULL || glDeleteBuffersARB == NULL ||
		glDeleteBuffersARB 		== NULL || glIsBufferARB == NULL ||
		glBufferDataARB 		== NULL || glBufferSubDataARB == NULL ||
		glGetBufferSubDataARB 	== NULL || glMapBufferARB == NULL ||
		glUnmapBufferARB 		== NULL || glGetBufferParameterivARB == NULL ||
		glGetBufferPointervARB 	== NULL)
    {
		return false;
    }
    return true;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
