#include "GLSpriteVertexData.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TGLSpriteVertexData::TGLSpriteVertexData():TSpriteVertexData(),VBOIndex(0)
{
	Exception.SetPrefix(L"TGLSpriteVertexData: ");
}
///----------------------------------------------------------------------------------------------//
TGLSpriteVertexData::~TGLSpriteVertexData()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TGLSpriteVertexData::SetPointColorData(float *fColor, int ArrShift, const TColor &ColorVal)
{
	for(int i=0;i<4;i++){ fColor[ArrShift + i] = ColorVal.Get(i); }
}
///----------------------------------------------------------------------------------------------//
void TGLSpriteVertexData::GenMeshData(float *fVert)
{
	TPoint Point;
	// первый треугольник
	Point = Mesh->Get(2);
	fVert[0] = Point.GetX();	fVert[1] = Point.GetY();

	Point = Mesh->Get(1);
	fVert[2] = Point.GetX();	fVert[3] = Point.GetY();

	Point = Mesh->Get(3);
	fVert[4] = Point.GetX();	fVert[5] = Point.GetY();

	// второй треугольник
	Point = Mesh->Get(0);
	fVert[6] = Point.GetX();	fVert[7] = Point.GetY();

	Point = Mesh->Get(3);
	fVert[8] = Point.GetX();	fVert[9] = Point.GetY();

	Point = Mesh->Get(1);
	fVert[10] = Point.GetX();	fVert[11] = Point.GetY();
}
///----------------------------------------------------------------------------------------------//
void TGLSpriteVertexData::GenColorData(float *fColor)
{
	for(int j=0;j<6;j++){
		for(int i=0;i<4;i++){ fColor[j*4+i] = Color->Get(i); }
	}

	for(int i=0;i<4;i++){
		if(UsePointColor[i]){
			TColor ColorVal(PointColor[i]);
			switch(i){
				case 0:{ SetPointColorData(fColor, 12, ColorVal); }break;

				case 1:{
					SetPointColorData(fColor,  4, ColorVal);
					SetPointColorData(fColor, 20, ColorVal);
				}break;

				case 2:{ SetPointColorData(fColor,  0, ColorVal); }break;

				case 3:{
					SetPointColorData(fColor,  8, ColorVal);
					SetPointColorData(fColor, 16, ColorVal);
				}break;
			}
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TGLSpriteVertexData::GenTexData(float *fText)
{
	if(Texture->IsInit() && Texture->GetCoordinatesState() == EN_TCS_Floating){
		Texture->BuildCoordinatesByMesh(Mesh);
	}

	TPoint *Point(NULL);
	// первый треугольник
	Point = Texture->GetPointPtr(2);
	fText[0] = Point->GetX();	fText[1] = Point->GetY();

	Point = Texture->GetPointPtr(1);
	fText[2] = Point->GetX();	fText[3] = Point->GetY();

	Point = Texture->GetPointPtr(3);
	fText[4] = Point->GetX();	fText[5] = Point->GetY();

	// второй треугольник
	Point = Texture->GetPointPtr(0);
	fText[6] = Point->GetX();	fText[7] = Point->GetY();

	Point = Texture->GetPointPtr(3);
	fText[8] = Point->GetX();	fText[9] = Point->GetY();

	Point = Texture->GetPointPtr(1);
	fText[10] = Point->GetX();	fText[11] = Point->GetY();
}
///----------------------------------------------------------------------------------------------//
void TGLSpriteVertexData::Create()
{
	glGenBuffersARB(1,&VBOIndex);
}
///----------------------------------------------------------------------------------------------//
void TGLSpriteVertexData::Delete()
{
	if(glIsBufferARB(VBOIndex) == GL_TRUE){ glDeleteBuffersARB(1,&VBOIndex); }
}
///----------------------------------------------------------------------------------------------//
void TGLSpriteVertexData::Update()
{
	if(!CheckNeedUpdate()){return;}

	IdActive = Id;

	float  fVert [12];
	float  fColor[24];
	float  fText [12];

	GenMeshData (fVert);
	GenColorData(fColor);
	GenTexData  (fText);

	float Data[48];
	for(int i=0; i< 12; i++){ Data[i    ]  = fVert[i];}
	for(int i=0; i< 24; i++){ Data[12 + i] = fColor[i];}
	for(int i=0; i< 12; i++){ Data[36 + i] = fText[i];}

	glBindBufferARB(GL_ARRAY_BUFFER, VBOIndex);
	glBufferDataARB(GL_ARRAY_BUFFER,sizeof(Data),Data,GL_STATIC_DRAW);
	glBindBufferARB(GL_ARRAY_BUFFER, 0);
}
///----------------------------------------------------------------------------------------------//
void TGLSpriteVertexData::Run()
{
	Update();

	glBindBufferARB(GL_ARRAY_BUFFER,VBOIndex);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glVertexPointer   (2, GL_FLOAT, 0, (void*)0 );
	glColorPointer    (4, GL_FLOAT, 0, (void*)(12*sizeof(float)));
	glTexCoordPointer (2, GL_FLOAT, 0, (void*)(36*sizeof(float)));

	glDrawArrays(GL_TRIANGLES,0,6);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);

	glBindBufferARB(GL_ARRAY_BUFFER,0);
}
///----------------------------------------------------------------------------------------------//
/*
void TGLSpriteVertexData::SetPointColor(const int &PointIndex, const TColor &PointColor)
{
	IdActive = Id;

	float  fVert[12];
	float  fColor[24];
	float  fText[12];

	GenBufferData(fVert, fColor, fText);

	switch(PointIndex){
		case 0:{ SetPointColorData(fColor, 12, PointColor);  }break;

		case 1:{
			SetPointColorData(fColor,  4, PointColor);
			SetPointColorData(fColor, 20, PointColor);
		}break;

		case 2:{ SetPointColorData(fColor,  0, PointColor); }break;

		case 3:{
			SetPointColorData(fColor,  8, PointColor);
			SetPointColorData(fColor, 16, PointColor);
		}break;
	}

	UpdateBufferData(fVert, fColor, fText);
}
*/
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//


