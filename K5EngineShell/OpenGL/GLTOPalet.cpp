#include "GLTOPalet.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TGLPalet::TGLPalet():TPalet(),Exception(L"TGLPalet"),GLPalet(0)
{
}
///----------------------------------------------------------------------------------------------//
TGLPalet::~TGLPalet()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
GLenum TGLPalet::GetGL() const
{
	return GLPalet;
};
///----------------------------------------------------------------------------------------------//
void TGLPalet::SetGLPLByPL(enPaletType PLFormat)
{
	PLPalet = PLFormat;
	GLPalet = PLTOGL(PLFormat);
};
///----------------------------------------------------------------------------------------------//
GLenum TGLPalet::PLTOGL(enPaletType PaletType)
{
    switch(PaletType){
        case EN_PL_RGB       : return(GL_RGB);
        case EN_PL_RGBA      : return(GL_RGBA);
        case EN_PL_GRAYSCALE : return(GL_LUMINANCE);
		case EN_PL_GRAYALPHA : return(GL_LUMINANCE_ALPHA);
        case EN_PL_NOTSET    : Exception(L"Palet type not set, can't convert to OpenGl format");
        default : Exception(L"Unknow palet type, can't convert to OpenGl format");
    }

    return(0);
}
///----------------------------------------------------------------------------------------------//
enPaletType TGLPalet::GLTOPL(GLenum Format)
{
    switch(Format){
        case GL_RGB            : return(EN_PL_RGB);
        case GL_RGB8           : return(EN_PL_RGB);
        case GL_RGBA           : return(EN_PL_RGBA);
        case GL_RGBA8          : return(EN_PL_RGBA);
        case GL_LUMINANCE      : return(EN_PL_GRAYSCALE);
		case GL_LUMINANCE_ALPHA: return(EN_PL_GRAYALPHA);
        default : Exception(L" Not support OpenGL pixel format, can't convert to palet format");
    }

    return(EN_PL_NOTSET);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
