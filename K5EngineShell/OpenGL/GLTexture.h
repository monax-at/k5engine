///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
/// Authors:
///		Sergey Kalenik    [monax.at@gmail.com]
///		Maxim Kolesnikov [mpkmaximus@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef GLTEXTURE_H_INCLUDED
#define GLTEXTURE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "GLTOPalet.h"

#include <limits>

using std::numeric_limits;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TGLTexture:public TTexture
{
	private:
        GLuint   IdTexture;

		inline void Init();
    	inline void BindTexture();

		inline void ReadFromTexture(unsigned char *DstVal, unsigned char *SrcVal,
									const int &WidthVal  , const int &HeightVal,const int &PitchVal);

		inline void WriteToTexture (unsigned char *DstVal, unsigned char *SrcVal,
									const int &WidthVal  , const int &HeightVal,const int &PitchVal);
    protected:
        void ToRun();
        void ToStop();
        void ToDel();
///---->
		TTexture *ToCreateMask   (const wstring &MaskName);
		TTexture *ToCreateCutMask(const wstring &MaskName);
///---->
		TTexture *ToClone(const wstring &TextName);
///---->
		TTexture* ToCopyRect(const wstring &TextName, const int &RectWidth,const int &RectHeight,
							 const int &X,const int &Y, const bool &UseDeg2TexSizeFlag);
///---->
		pTTextureRawData ToGetCopyRawData(const int &WidthVal, const int &HeightVal,
										  const int &XVal, const int &YVal);

		void ToWriteRawData(const pTTextureRawData &RawDataVal, const int &XVal, const int &YVal,
							const enTexWriteMode &ModeVal);
    public:
		TGLTexture( const wstring &TexName, GLuint IdVal);
        TGLTexture( const wstring &TextName, const int &WidthVal, const int &HeightVal,
					unsigned char *TexRawData = NULL, const bool &IsNativRawData = false);

        virtual ~TGLTexture();

        GLuint 	GetId()  const;
};
typedef TGLTexture* pTGLTexture;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // GLIMAGE_H_INCLUDED
