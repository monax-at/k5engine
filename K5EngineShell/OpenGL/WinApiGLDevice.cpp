#include "WinApiGLDevice.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TWinApiGLDevice::TWinApiGLDevice():TWinApiDevice(),UseVBOFlag(false)
{
}
///----------------------------------------------------------------------------------------------//
TWinApiGLDevice::~TWinApiGLDevice()
{
	if(Hwnd!=NULL){
		DisableOpenGL();
	}
	if(FullScreen){
		ChangeDisplaySettings(NULL, 0);
	}
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
unsigned int TWinApiGLDevice::GetGLTextureFilter(const enTextureFilter &Filter)
{
	switch(Filter)
	{
		default			 : return GL_NEAREST; break;
		case EN_TF_Linear: return GL_LINEAR ; break;
	}

	return GL_NEAREST;
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::ToSetTextureFilter()
{
	if(Hwnd == NULL){return;}

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
					GetGLTextureFilter(MinTextureFilter));

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,
					GetGLTextureFilter(MagTextureFilter));
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::CreateWinWindow()
{
	WNDCLASSEX wcex;

    HINSTANCE Instance = GetModuleHandle(NULL);
	Exception(Instance!=NULL,L"in CreateWinWindow() cant't get Instance");

    // register window class
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_OWNDC;
    wcex.lpfnWndProc = WinApiDeviceProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance  = Instance;
    wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = L"WinApiWindowClass";
    wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);;

	Exception(RegisterClassEx(&wcex)!=0,L"in CreateWinWindow() RegisterClassEx() fail");

	if(FullScreen == true){
		DEVMODE Dmode;
		memset(&Dmode, 0, sizeof(Dmode));
		Dmode.dmSize = sizeof(Dmode);
		Dmode.dmPelsWidth  = Width;
		Dmode.dmPelsHeight = Height;
		Dmode.dmBitsPerPel = Bpp;
		Dmode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

		if(ChangeDisplaySettings(&Dmode, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL){
			Exception(L"in CreateWinWindow() ChangeDisplaySettings() fail");
		}
	}

	DWORD Style(0);
	if(FullScreen){
		Style = WS_POPUP;
	}
	else{
		switch(WindowStyle){
			case EN_DWS_Default:{
				Style = WS_OVERLAPPED | WS_CAPTION | WS_VISIBLE | WS_SYSMENU |
						WS_MINIMIZEBOX;
			}break;

			case EN_DWS_BorderOff:{
				Style = WS_POPUP | WS_VISIBLE;
			}break;
		}
	}

    RECT WinRect;
	WinRect.left 	= 0;
	WinRect.right 	= Width;
	WinRect.top 	= 0;
	WinRect.bottom 	= Height;

    AdjustWindowRect(&WinRect,Style,false);

	int X = (GetSystemMetrics(SM_CXSCREEN) - Width) / 2;
	int Y = (GetSystemMetrics(SM_CYSCREEN) - Height) / 2;

    Hwnd = CreateWindowEx(0,
                          L"WinApiWindowClass",
                          Caption.c_str(),
                          Style,
                          X, Y,
                          WinRect.right  - WinRect.left,
                          WinRect.bottom - WinRect.top,
                          NULL, NULL, NULL, NULL);

    ShowWindow(Hwnd, SW_SHOW);

	MouseEventConvertor.SetWinParam(Hwnd,Width,Height);
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::SetVSync(const bool &Val)
{
	PFNWGLSWAPINTERVALEXTPROC wglSwapInterval = NULL;
	wglSwapInterval = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
	if( wglSwapInterval ){
		wglSwapInterval(Val);
	}
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::EnableOpenGL()
{
    int iFormat = 0;

    hDC = GetDC(Hwnd);

    ZeroMemory(&Pfd, sizeof(Pfd));

    // заполняем PIXELFORMATDESCRIPTOR
    Pfd.nSize = sizeof(Pfd);
    Pfd.nVersion = 1;
    Pfd.dwFlags = PFD_DRAW_TO_WINDOW |
                  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    Pfd.iPixelType = PFD_TYPE_RGBA;
    Pfd.cColorBits = 32;
	//Pfd.cDepthBits = 32;
    Pfd.iLayerType = PFD_MAIN_PLANE;

    // выбор оптимального формата пикселей
    iFormat = ChoosePixelFormat(hDC, &Pfd);
    Exception(iFormat!=0,L"in EnableOpenGL() can't shoose pixel format");

    // установка выбранного формата пикселей
    Exception(	(bool)SetPixelFormat(hDC, iFormat, &Pfd),
				L"in EnableOpenGL() can't set pixel format");


    hRC = wglCreateContext(hDC);

    wglMakeCurrent(hDC, hRC);
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::SetGLStates()
{
	glViewport(0,0,Width,Height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0,Width,Height,0,0,-100);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_CW);

	glFrontFace(GL_FRONT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER,0.0f);

	glShadeModel(GL_SMOOTH);

	glMatrixMode(GL_TEXTURE);
	glRotatef(180.0f,1.0f,0.0f,0.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	ToSetTextureFilter();
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::InitVBO()
{
	TGLExtensionWorker ExtWorker;
	UseVBOFlag = ExtWorker.InitVBO();
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::DisableOpenGL()
{
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(Hwnd, hDC);
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::Create(	const int &iWidth,const int &iHeight,
								const bool &bFullScreen)
{
	Width = iWidth;
	Height = iHeight;
	FullScreen = bFullScreen;

	SystemEventWinApiDevicePtr = this;

	CreateWinWindow();
	EnableOpenGL();
	SetGLStates();
	InitVBO();

	SetVSync(false);

	SetDrawCursor(DrawCursor);

	DevInitWorker();
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::ClearSceneColor()
{
	glClearColor(	ClearColor.Get(0), ClearColor.Get(1),
					ClearColor.Get(2), ClearColor.Get(3));

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::SwapSceneBuffers()
{
	SwapBuffers(hDC);
}
///----------------------------------------------------------------------------------------------//
void TWinApiGLDevice::SetDrawCursor(const bool &Val)
{
	DrawCursor = Val;
	if(Hwnd!=NULL){ShowCursor(DrawCursor);}
}
///----------------------------------------------------------------------------------------------//
pTBaseTextureLoader TWinApiGLDevice::CreateTextureLoader()
{
	return new TDevGLTextureLoader;
}
///----------------------------------------------------------------------------------------------//
pTBaseTextureCreater TWinApiGLDevice::CreateTextureCreater()
{
	return new TGLTextureCreater;
}
///----------------------------------------------------------------------------------------------//
pTBaseScreenPixelReader TWinApiGLDevice::CreateScreenPixelReader()
{
	return new TGLScreenPixelReader;
}
///----------------------------------------------------------------------------------------------//
pTBaseViewMatrixWorker TWinApiGLDevice::CreateViewMatrixWorker()
{
	return new TGLViewMatrixWorker;
}
///----------------------------------------------------------------------------------------------//
pTBaseVertexData TWinApiGLDevice::CreateSpriteVertexData()
{
	return new TGLSpriteVertexData;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
