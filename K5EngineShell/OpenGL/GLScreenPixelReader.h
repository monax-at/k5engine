///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef GLSCREENPIXELREADER_H_INCLUDED
#define GLSCREENPIXELREADER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "GLIncludes.h"
#include "../../Core/Core.h"
#include "../../LibMath/MathFunction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TGLScreenPixelReader:public TBaseScreenPixelReader
{
	protected:
		TColor ToRun(const float &X,const float &Y);
	public:
		TGLScreenPixelReader();
		~TGLScreenPixelReader();
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // GLSCREENPIXELREADER_H_INCLUDED
