#include "GLTexture.h"
#include "../../LibMath/MathFunction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
const int Bpp = 4;
///----------------------------------------------------------------------------------------------//
TGLTexture::TGLTexture(const wstring &TexName, GLuint IdVal):TTexture()
{
	Init();

	if(glIsTexture(IdVal) == GL_FALSE){return;}

	glBindTexture(GL_TEXTURE_2D,IdVal);

	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH , &Width);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &Height);

	Name      = TexName;
	IdTexture = IdVal;
}
///----------------------------------------------------------------------------------------------//
TGLTexture::TGLTexture(const wstring &TextName, const int &WidthVal, const int &HeightVal,
						unsigned char *TexRawData, const bool &IsNativRawData)
{
	Init();

	Name   = TextName;
	Width  = WidthVal;
	Height = HeightVal;

    glGenTextures ( 1, &IdTexture );
    glBindTexture ( GL_TEXTURE_2D, IdTexture );

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	if(TexRawData != NULL){
		glTexImage2D ( GL_TEXTURE_2D, 0, GL_RGBA,Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, TexRawData);
	}
	else{
		unsigned int   txSize      = Width * Height * Bpp;
		unsigned char *TextureData = new unsigned char[txSize];
		memset(TextureData,0,txSize);

		glTexImage2D ( GL_TEXTURE_2D, 0, GL_RGBA,Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, TextureData);
		delete[] TextureData;
	}

    glBindTexture( GL_TEXTURE_2D, 0 );
}
///----------------------------------------------------------------------------------------------//
TGLTexture::~TGLTexture()
{
}
///----------------------------------------------------------------------------------------------//
void TGLTexture::Init()
{
	Exception.SetPrefix(L"TGLTexture: ");
	IdTexture = 0;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TGLTexture::BindTexture()
{
	Exception(glIsTexture(IdTexture) == GL_TRUE,
			  L"Can't clone texture, attach by id to texture " + Name + L"is failure" );

	glBindTexture(GL_TEXTURE_2D,IdTexture);
}
///----------------------------------------------------------------------------------------------//
GLuint TGLTexture::GetId() const
{
	return IdTexture;
}
///----------------------------------------------------------------------------------------------//
void TGLTexture::ToRun()
{
    if(glIsTexture(IdTexture) == GL_FALSE){return;}
	glBindTexture(GL_TEXTURE_2D,IdTexture);
}
///----------------------------------------------------------------------------------------------//
void TGLTexture::ToStop()
{
	glBindTexture(GL_TEXTURE_2D,0);
}
///----------------------------------------------------------------------------------------------//
void TGLTexture::ToDel()
{
	if(glIsTexture(IdTexture) != GL_TRUE){return;}
	glDeleteTextures(1, &IdTexture);
	IdTexture = 0;
}
///----------------------------------------------------------------------------------------------//
TTexture *TGLTexture::ToCreateMask(const wstring &MaskName)
{
	BindTexture();

	int TextByteSize = Height * Width * Bpp;
	int MaskByteSize = Height * Width * Bpp;

	unsigned char *pByteSrc = new unsigned char[TextByteSize];
	unsigned char *pByteDst = new unsigned char[MaskByteSize];

//----> �������� �������� � ������ ����������
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pByteSrc);
//----> ��������� ����� ��������
	for(int iCount = 0; iCount < TextByteSize; iCount = iCount + 4){
		pByteDst[iCount]     = pByteSrc[iCount + 3];
		pByteDst[iCount + 1] = pByteSrc[iCount + 3];
		pByteDst[iCount + 2] = pByteSrc[iCount + 3];
		pByteDst[iCount + 3] = 0xFF;
	}
//----> ������� ����� ��������
	TGLTexture *GLTexture = new TGLTexture(MaskName, Width, Height, pByteDst);
	delete[] pByteSrc;
	delete[] pByteDst;

    return GLTexture;
}
///----------------------------------------------------------------------------------------------//
TTexture *TGLTexture::ToCreateCutMask(const wstring &MaskName)
{
	BindTexture();

	int TextByteSize = Height * Width * Bpp;
	int MaskByteSize = Height * Width * Bpp;

	unsigned char *pByteSrc = new unsigned char[TextByteSize];
	unsigned char *pByteDst = new unsigned char[MaskByteSize];
//----> �������� �������� � ������ ����������
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pByteSrc);

	unsigned char MinVal((numeric_limits<unsigned char>::min)());
	unsigned char MaxVal((numeric_limits<unsigned char>::max)());

	for(int iCount = 0; iCount < TextByteSize; iCount = iCount + 4){
		unsigned char Val(MinVal);
		if(pByteSrc[iCount + 3] > MinVal){Val = MaxVal;}
		for(int i=0;i<4;i++){pByteDst[iCount + i] = Val;}
	}

    TGLTexture *GLTexture = new TGLTexture(MaskName, Width, Height, pByteDst);
	delete[] pByteSrc;
	delete[] pByteDst;

	return GLTexture;
}
///----------------------------------------------------------------------------------------------//
TTexture* TGLTexture::ToClone(const wstring &TextName)
{
	BindTexture();

//---> ��������� ����������� ��������
	unsigned int  txSize  = Height * Width * Bpp;
    unsigned char *txData = new unsigned char[txSize];

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, txData);

    TGLTexture *GLTexture = new TGLTexture(TextName, Width, Height, txData);
    delete[] txData;

    return GLTexture;
}
///----------------------------------------------------------------------------------------------//
TTexture* TGLTexture::ToCopyRect(const wstring &TextName,const int &RectWidth,const int &RectHeight,
							     const int &X,const int &Y, const bool &UseDeg2TexSizeFlag)
{
//	unsigned char *NewData  = NULL;
	unsigned char *pByteSrc = NULL;
	unsigned char *pByteDst = NULL;
	int NewRectWidth  	    = RectWidth;
	int NewRectHeight       = RectHeight;
	int PitchSrc			= Width * Bpp;
///------------------>
	if(UseDeg2TexSizeFlag){
		NewRectWidth  = NextDeg2(NewRectWidth);
		NewRectHeight = NextDeg2(NewRectHeight);
	}
	int Size = NewRectWidth * NewRectHeight * Bpp;
	pByteDst = new unsigned char[Size];
	memset(pByteDst,0,Size);
///------------------>
	BindTexture();

	pByteSrc = new unsigned char[Width * Height * Bpp];
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pByteSrc);

	pByteSrc = pByteSrc + (PitchSrc * Y) + X * Bpp;
	ReadFromTexture(pByteDst, pByteSrc, NewRectWidth, NewRectHeight, PitchSrc);

	pTGLTexture NewTexture = new TGLTexture(TextName, NewRectWidth, NewRectHeight, pByteDst);
	delete[] pByteDst;

	return NewTexture;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TGLTexture::ReadFromTexture(unsigned char *DstVal, unsigned char *SrcVal,
								 const int &WidthVal, const int &HeightVal,const int &PitchVal)
{
	unsigned char *pByteSta = NULL;
	unsigned char *pByteDst = DstVal;
	unsigned char *pByteSrc = SrcVal;
///--------->
	for(int iY = 0; iY < HeightVal; iY++){
		pByteSta = pByteSrc;
		for(int iX = 0; iX < WidthVal; iX++){
			pByteDst[0] = pByteSrc[0];
			pByteDst[1] = pByteSrc[1];
			pByteDst[2] = pByteSrc[2];
			pByteDst[3] = pByteSrc[3];
///--------->
			pByteDst = pByteDst + 4;
			pByteSrc = pByteSrc + 4;
		}
		pByteSrc = pByteSta + PitchVal;
	}
}
///----------------------------------------------------------------------------------------------//
void TGLTexture::WriteToTexture(unsigned char *DstVal, unsigned char *SrcVal,
								const int &WidthVal, const int &HeightVal,const int &PitchVal)
{
	unsigned char *pByteSta = NULL;
	unsigned char *pByteDst = DstVal;
	unsigned char *pByteSrc = SrcVal;
///--------->
	for(int iY = 0; iY < HeightVal; iY++){
		pByteSta = pByteDst;
		for(int iX = 0; iX < WidthVal ; iX++){
			pByteDst[0] = pByteSrc[0];
			pByteDst[1] = pByteSrc[1];
			pByteDst[2] = pByteSrc[2];
			pByteDst[3] = pByteSrc[3];
///--------->
			pByteDst = pByteDst + 4;
			pByteSrc = pByteSrc + 4;
		}
		pByteDst = (pByteSta + PitchVal);
	}
}
///----------------------------------------------------------------------------------------------//
pTTextureRawData TGLTexture::ToGetCopyRawData(const int &WidthVal, const int &HeightVal,
											  const int &XVal, const int &YVal)
{
	BindTexture();

	int ByteSrcSize  = Height  * Width  * Bpp;
	int ByteDstSize  = HeightVal * WidthVal * Bpp;

	unsigned char *pByteSrc     = new unsigned char[ByteSrcSize];
	unsigned char *pByteDst     = new unsigned char[ByteDstSize];
//----> �������� �������� � ������ ����������
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pByteSrc);

	pByteSrc = pByteSrc + (Width * Bpp * YVal) + XVal * 4;
	ReadFromTexture(pByteDst, pByteSrc, WidthVal, HeightVal, WidthVal * 4);

    pTTextureRawData TextureRawData = new TTextureRawData(WidthVal, HeightVal, pByteDst);
	delete[] pByteSrc;

	return TextureRawData;
}
///----------------------------------------------------------------------------------------------//
void TGLTexture::ToWriteRawData(const pTTextureRawData &RawDataVal, const int &XVal,
								const int &YVal, const enTexWriteMode &ModeVal)
{
	BindTexture();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
