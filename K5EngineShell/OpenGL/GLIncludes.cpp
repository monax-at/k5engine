#include "GLIncludes.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
// VBO

PFNGLBINDBUFFERARBPROC                  glBindBufferARB 			= NULL;
PFNGLDELETEBUFFERSARBPROC               glDeleteBuffersARB 			= NULL;
PFNGLGENBUFFERSARBPROC                  glGenBuffersARB 			= NULL;
PFNGLISBUFFERARBPROC                    glIsBufferARB 				= NULL;
PFNGLBUFFERDATAARBPROC                  glBufferDataARB 			= NULL;
PFNGLBUFFERSUBDATAARBPROC               glBufferSubDataARB 			= NULL;
PFNGLGETBUFFERSUBDATAARBPROC            glGetBufferSubDataARB 		= NULL;
PFNGLMAPBUFFERARBPROC                   glMapBufferARB 				= NULL;
PFNGLUNMAPBUFFERARBPROC                 glUnmapBufferARB 			= NULL;
PFNGLGETBUFFERPARAMETERIVARBPROC        glGetBufferParameterivARB 	= NULL;
PFNGLGETBUFFERPOINTERVARBPROC           glGetBufferPointervARB 		= NULL;

/*
PFNGLACTIVETEXTUREARBPROC       		glActiveTextureARB       	= NULL;
PFNGLCLIENTACTIVETEXTUREARBPROC 		glClientActiveTextureARB 	= NULL;
PFNGLMULTITEXCOORD1FARBPROC     		glMultiTexCoord1f        	= NULL;
PFNGLMULTITEXCOORD1FVARBPROC    		glMultiTexCoord1fv       	= NULL;
PFNGLMULTITEXCOORD2FARBPROC     		glMultiTexCoord2f        	= NULL;
PFNGLMULTITEXCOORD2FVARBPROC    		glMultiTexCoord2fv       	= NULL;
*/
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//


