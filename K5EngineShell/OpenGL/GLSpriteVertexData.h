///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef GLSPRITEVERTEXDATA_H_INCLUDED
#define GLSPRITEVERTEXDATA_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../Core/Core.h"
#include "GLIncludes.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TGLSpriteVertexData:public TSpriteVertexData
{
	protected:
        GLuint VBOIndex;
	protected:
		void SetPointColorData(float *fColor, int ArrShift, const TColor &ColorVal);

		void GenMeshData (float *fVert);
		void GenColorData(float *fColor);
		void GenTexData  (float *fText);
	public:
		TGLSpriteVertexData();
		virtual ~TGLSpriteVertexData();

		void Create();
		void Delete();
		void Update();
		void Run();
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // GLSPRITEVERTEXDATA_H_INCLUDED
