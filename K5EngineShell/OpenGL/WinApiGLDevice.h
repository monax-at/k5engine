///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef WINAPIGLDEVICE_H_INCLUDED
#define WINAPIGLDEVICE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "GLIncludes.h"

#include "GLExtensionWorker.h"
#include "GLScreenPixelReader.h"
#include "GLViewMatrixWorker.h"
#include "GLTextureCreater.h"
#include "GLSpriteVertexData.h"

#include "../DevIL/DevGLTextureLoader.h"
#include "../DevIL/DevInitWorker.h"
#include "../WinApi/WinApiDevice.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TWinApiGLDevice: public TWinApiDevice
{
	protected:
		bool UseVBOFlag;

		HDC 	hDC;
		HGLRC	hRC;
		PIXELFORMATDESCRIPTOR Pfd;

		TDevInitWorker DevInitWorker;
	protected:
		unsigned int GetGLTextureFilter(const enTextureFilter &Filter);
		void ToSetTextureFilter();

		inline void CreateWinWindow();
		inline void SetVSync(const bool &Val);
		inline void EnableOpenGL();
		inline void SetGLStates();
		inline void InitVBO();
		inline void DisableOpenGL();
	public:
		TWinApiGLDevice();
		virtual ~TWinApiGLDevice();

		void Create(const int &iWidth,const int &iHeight,
					const bool &bFullScreen = false);

		void ClearSceneColor();
		void SwapSceneBuffers();

		void SetDrawCursor(const bool &Val);

		pTBaseTextureLoader 	CreateTextureLoader();
		pTBaseTextureCreater 	CreateTextureCreater();

		pTBaseScreenPixelReader CreateScreenPixelReader();
		pTBaseViewMatrixWorker 	CreateViewMatrixWorker();
		pTBaseVertexData        CreateSpriteVertexData();
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // WINAPIGLDEVICE_H_INCLUDED
