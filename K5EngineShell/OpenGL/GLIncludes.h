///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef GLINCLUDES_H_INCLUDED
#define GLINCLUDES_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#ifdef _WIN32
	#include <windows.h> // без этой .h-ки в видне не будут определяться из gl.h
#else
	#include <stdint.h>
	#define GLX_GLXEXT_LEGACY
#endif

// заголовочные файлы для работы с расширениями
	#include <GL/gl.h>
	#include <GL/glu.h>
	#include <GL/glext.h>

#ifdef _WIN32
	#include <GL/wglext.h>
#else
	#include <GL/glx.h>
#endif
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
// VBO
extern PFNGLBINDBUFFERARBPROC			glBindBufferARB				;
extern PFNGLDELETEBUFFERSARBPROC		glDeleteBuffersARB			;
extern PFNGLGENBUFFERSARBPROC			glGenBuffersARB				;
extern PFNGLISBUFFERARBPROC				glIsBufferARB				;
extern PFNGLBUFFERDATAARBPROC			glBufferDataARB				;
extern PFNGLBUFFERSUBDATAARBPROC		glBufferSubDataARB			;
extern PFNGLGETBUFFERSUBDATAARBPROC		glGetBufferSubDataARB		;
extern PFNGLMAPBUFFERARBPROC			glMapBufferARB				;
extern PFNGLUNMAPBUFFERARBPROC			glUnmapBufferARB			;
extern PFNGLGETBUFFERPARAMETERIVARBPROC	glGetBufferParameterivARB	;
extern PFNGLGETBUFFERPOINTERVARBPROC	glGetBufferPointervARB		;

// мультитекстурирование
/*
extern PFNGLACTIVETEXTUREARBPROC       glActiveTextureARB			;
extern PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTextureARB		;
extern PFNGLMULTITEXCOORD1FARBPROC     glMultiTexCoord1f       		;
extern PFNGLMULTITEXCOORD1FVARBPROC    glMultiTexCoord1fv      		;
extern PFNGLMULTITEXCOORD2FARBPROC     glMultiTexCoord2f       		;
extern PFNGLMULTITEXCOORD2FVARBPROC    glMultiTexCoord2fv      		;
*/
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // GLINCLUDES_H_INCLUDED
