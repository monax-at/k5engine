#include "DNSimplePathPoints.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TDNSimplePathPoints::TDNSimplePathPoints():Exception(L"TDNSimplePathPoints: ")
{
}
///----------------------------------------------------------------------------------------------//
TDNSimplePathPoints::~TDNSimplePathPoints()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDNSimplePathPoints::Init(const pTSimplePathPoints &Points , const pTDataNode &Node)
{
	TDNEngineDataConverter DC;

	int NodeSize(Node->GetSize());
	for(int i=0;i<NodeSize;i++){
		pTDataNode Point(Node->Get(i));
		if(Point->GetName() == L"Point"){ Points->Add(DC.ToPoint(Point)); }
	}
}
///----------------------------------------------------------------------------------------------//
void TDNSimplePathPoints::Init(const pTDataNode &Node, const pTSimplePathPoints &Points)
{
	Node->SetName(L"SimplePathPoints");

	TDNEngineDataConverter DC;

	int Size(Points->GetSize());
	for(int i=0;i<Size;i++){ Node->Add(DC.From(*(Points->Get(i)),L"Point")); }
}
///----------------------------------------------------------------------------------------------//
pTSimplePathPoints TDNSimplePathPoints::Run(const pTDataNode &Node)
{
	try{
		pTSimplePathPoints Points(new TSimplePathPoints);

		Init(Points,Node);

		return Points;
	}
	catch(TException &Ex){
		Exception(L"in Run(Node): " + Ex.GetMess());
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNSimplePathPoints::Run(const pTSimplePathPoints &Points)
{
	try{
		pTDataNode Node(new TDataNode);

		Init(Node, Points);

		return Node;
	}
	catch(TException &Ex){
		Exception(L"in Run(Node): " + Ex.GetMess());
	}

	return NULL;
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
