///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef K5PATHFINDING_LANDMAPCELLINFO_H_INCLUDED
#define K5PATHFINDING_LANDMAPCELLINFO_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "GridMapCell.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TGridMapCellInfo
{
	public:
		pTGridMapCell Cell;

		int StepCost;
		int Dist;

		TGridMapCellInfo* Parent;
	public:
		TGridMapCellInfo();
		TGridMapCellInfo(const TGridMapCellInfo &Val);
		~TGridMapCellInfo();

		void Set(const pTGridMapCell &CellVal, const int &StepCostVal,
				 const int &XLen,const int &YLen);

		int GetPathCost() const;
};

typedef TGridMapCellInfo* pTGridMapCellInfo;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // LANDMAPCELLINFO_H_INCLUDED
