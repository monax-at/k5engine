#include "SimplePathList.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TSimplePathList::TSimplePathList():Exception(L"TSimplePathList: ")
{
}
///----------------------------------------------------------------------------------------------//
TSimplePathList::~TSimplePathList()
{
	Clear();
}
///----------------------------------------------------------------------------------------------//
void TSimplePathList::Add(const pTSimplePath &Path)
{
	Paths.push_back(Path);
}
///----------------------------------------------------------------------------------------------//
pTSimplePath TSimplePathList::Get(const int &Index)
{
	int Size(Paths.size());
	Exception(Index >= 0 && Index < Size, L"in Get(Index) Id out of range");
	return Paths[Index];
}
///----------------------------------------------------------------------------------------------//
pTSimplePath TSimplePathList::Get(const pTPoint &Start, const pTPoint &Finish)
{
	int Size(Paths.size());
	for(int i=0;i<Size;i++){
		pTSimplePath Path(Paths[i]);
		if(Path->GetFirst() == Start && Path->GetLast() == Finish){return Path;}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
int TSimplePathList::GetSize()
{
	return Paths.size();
}
///----------------------------------------------------------------------------------------------//
void TSimplePathList::Clear()
{
	int Size(Paths.size());
	for(int i=0; i<Size; i++){ delete Paths[i]; }
	Paths.clear();
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
