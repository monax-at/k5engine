///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef SIMPLEPATHSMARTWALKER_H_INCLUDED
#define SIMPLEPATHSMARTWALKER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "SimplePathList.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TSimplePathSmartWalker:public TBaseAction
{
	protected:
		float            Speed;
		pTSimplePathList Paths;

		pTSimplePath CurrPath;
		int          CurrPathCursor;

		TActionPointIterator Iter;
	protected:
		void ToRun(const TEvent &Event);
		void ToStart();
		void ToStop();
	public:
		TSimplePathSmartWalker();
		virtual ~TSimplePathSmartWalker();

		void GoTo(const pTPoint &To, const float &SpeedVal);

		void SetPaths(const pTSimplePathList &Val);
		void SetPoint(const pTPoint &Val);

		void Set(const pTSimplePathList &PathsVal, const pTPoint &PointVal);

		pTActionPointIterator GetIterPtr();
};

typedef TSimplePathSmartWalker* pTSimplePathSmartWalker;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // SIMPLEPATHSMARTWALKER_H_INCLUDED
