///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef SIMPLEPATHLIST_H_INCLUDED
#define SIMPLEPATHLIST_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "SimplePath.h"
#include "SimplePathPoints.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TSimplePathList
{
	protected:
		TExceptionGenerator  Exception;
		vector<pTSimplePath> Paths;
	public:
		TSimplePathList();
		~TSimplePathList();

		void Add(const pTSimplePath &Path);

		pTSimplePath Get(const int &Index);
		pTSimplePath Get(const pTPoint &Start, const pTPoint &Finish);

		int GetSize();
		void Clear();
};

typedef TSimplePathList* pTSimplePathList;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // SIMPLEPATHLIST_H_INCLUDED
