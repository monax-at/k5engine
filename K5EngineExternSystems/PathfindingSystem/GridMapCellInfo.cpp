#include "GridMapCellInfo.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TGridMapCellInfo::TGridMapCellInfo():Cell(NULL),StepCost(0),Dist(0),Parent(NULL)
{
}
///----------------------------------------------------------------------------------------------//
TGridMapCellInfo::TGridMapCellInfo(const TGridMapCellInfo &Val)
{
	Cell     = Val.Cell;
	StepCost = Val.StepCost;
	Dist     = Val.Dist;
	Parent   = Val.Parent;
}
///----------------------------------------------------------------------------------------------//
TGridMapCellInfo::~TGridMapCellInfo()
{
}
///----------------------------------------------------------------------------------------------//
void TGridMapCellInfo::Set(	const pTGridMapCell &CellVal, const int &StepCostVal,
				 			const int &XLen,const int &YLen)
{
	Cell = CellVal;
	StepCost = StepCostVal;

	Dist = Abs(XLen) + abs(YLen);
}
///----------------------------------------------------------------------------------------------//
int TGridMapCellInfo::GetPathCost() const
{
	return Dist + StepCost;
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
