///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef K5PATHFINDING_SIMPLE_PATH_H_INCLUDED
#define K5PATHFINDING_SIMPLE_PATH_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TSimplePath
{
	protected:
		TExceptionGenerator Exception;
		vector<pTPoint>     Points;
	public:
		TSimplePath();
		virtual ~TSimplePath();

		void operator=(TSimplePath &Path);

		void Add(const pTPoint &Val);

		pTPoint Get(const int &Index) const;
		pTPoint GetFirst() const;
		pTPoint GetLast()  const;

		int GetSize() const;

		TVector2D GetDirVector(const int &StartIndex, const int &FinishIndex) const;

		void Reverse();
		void Clear();
};

typedef TSimplePath* pTSimplePath;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // K5PATHFINDING_SIMPLE_PATH_H_INCLUDED
