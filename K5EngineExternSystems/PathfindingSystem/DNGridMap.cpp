#include "DNGridMap.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TDNGridMap::TDNGridMap():Exception(L"TDNGridMap: ")
{
}
///----------------------------------------------------------------------------------------------//
TDNGridMap::~TDNGridMap()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDNGridMap::Init(const pTGridMap  &Map , const pTDataNode &Node)
{
	TDNEngineDataConverter DC;

	Map->Construct(Node->GetChildInt(L"width"), Node->GetChildInt(L"height"));

	if(Node->IsExist(L"Pos")){Map->SetPos(DC.ToPoint(Node->Get(L"Pos")));}

	pTDataNode CellsNode(Node->Get(L"Cells"));

	int CellsNodeSize(CellsNode->GetSize());
	for(int i=0;i<CellsNodeSize;i++){
		pTDataNode CellNode(CellsNode->Get(i));

		pTGridMapCell Cell(Map->GetCell(i));

		Cell->SetMapIndex(CellNode->GetChildInt(L"id_x"),CellNode->GetChildInt(L"id_y"));
		Cell->SetBlock   (CellNode->GetChildBool(L"block"));
		Cell->SetPathCost(CellNode->GetChildInt (L"path_cost"));

		if(CellNode->IsExist(L"Pos")){
			Cell->SetPos(DC.ToPoint(CellNode->Get(L"Pos")));
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TDNGridMap::Init(const pTDataNode &Node, const pTGridMap &Map)
{
	TDNEngineDataConverter DC;

	Node->SetName(L"K5PLandMap");

	int Width (Map->GetWidth());
	int Height(Map->GetHeight());

	Node->Add(Width , L"width");
	Node->Add(Height, L"height");

	Node->Add(DC.From(Map->GetPos(), L"Pos"));

	pTDataNode DNCells(Node->Add(new TDataNode));
	DNCells->SetName(L"Cells");

	for(int j=0;j<Height;j++){
		for(int i=0;i<Width;i++){
			pTGridMapCell Cell(Map->GetCell(i,j));

			pTDataNode DNCell(DNCells->Add(new TDataNode));
			DNCell->SetName(L"Cell");

			DNCell->Add(Cell->GetMapIndexX()  ,L"id_x");
			DNCell->Add(Cell->GetMapIndexY()  ,L"id_y");
			DNCell->Add(Cell->GetBlock()      ,L"block");
			DNCell->Add(Cell->GetPathCost()   ,L"path_cost");

			TPoint CellPos(Cell->GetPos());
			if(CellPos.GetX() != 0.0f || CellPos.GetY() != 0.0f || CellPos.GetZ() != 0.0f){
				DNCell->Add(DC.From(CellPos, L"Pos"));
			}
		}
	}
}
///----------------------------------------------------------------------------------------------//
pTGridMap TDNGridMap::Run(const pTDataNode &Node)
{
	try{
		pTGridMap Map(new TGridMap);

		Init(Map,Node);

		return Map;
	}
	catch(TException &Ex){
		Exception(L"in Run(Node): " + Ex.GetMess());
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNGridMap::Run(const pTGridMap &Map)
{
	try{
		pTDataNode Node(new TDataNode);

		Init(Node, Map);

		return Node;
	}
	catch(TException &Ex){
		Exception(L"in Run(Node): " + Ex.GetMess());
	}

	return NULL;
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

