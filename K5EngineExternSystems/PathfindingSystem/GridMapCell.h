///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef K5PATHFINDING_LANDMAPCELL_H_INCLUDED
#define K5PATHFINDING_LANDMAPCELL_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TGridMapCell
{
	public:
		pTPoint MapPos;
		TPoint  Pos;

		int     PathCost;
		bool    PathDirs[3][3]; // возможные направления прохода из ячейки. первый элемент -
							    // нижний левый (-1 -1). по умолчанию все true.
							    // позволяет иммитировать стенку или однонаправленный проход
		bool    Block;
		int     MapIndex[2];
	public:
		TGridMapCell();
		virtual ~TGridMapCell();

		void SetMapPos(const pTPoint &Val);
		pTPoint GetMapPos();

		void SetPos(const TPoint &Val);
		void SetPos(const float &x,const float &y);
		void SetPos(const float &x,const float &y,const float &z);
		TPoint GetPos() const;

		TPoint GetGlobalPos() const;

		void SetPathCost(const int &Val);
		int GetPathCost() const;

		void SetBlock(const bool &Val);
		bool GetBlock() const;

		void SetMapIndex(const int &x, const int &y);
		int  GetMapIndexX() const;
		int  GetMapIndexY() const;

		void SetPathDir(const int &IndexX, const int &IndexY, const bool &Val);
		bool GetPathDir(const int &IndexX, const int &IndexY) const;

		// проверка, можно ли пройти из этой ячейки в указанное направление
		// передаются значения от -1 до +1
		bool CheckPathDir(const int &ShiftX, const int &ShiftY) const;
};

typedef TGridMapCell* pTGridMapCell;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // TGridMapCELL_H_INCLUDED
