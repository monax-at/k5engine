#include "GridMap.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TGridMap::TGridMap():Exception(L"TGridMap: "),Width(0),Height(0)
{
}
///----------------------------------------------------------------------------------------------//
TGridMap::~TGridMap()
{
	Clear();
}
///----------------------------------------------------------------------------------------------//
TPoint TGridMap::GenCellPos(const int &IndexX, const int &IndexY) const
{
	TPoint CellPos(CellPosShift);
	CellPos.Mult(IndexX, IndexY, -(IndexY*Width + IndexX) );
	return CellPos;
}
///----------------------------------------------------------------------------------------------//
void TGridMap::UpdateCellPosShift()
{
	if(Width == 0 || Height == 0){return;}

	for(int i=0;i<Height;i++){
		for(int j=0;j<Width;j++){ Cells[i*Width + j]->SetPos(GenCellPos(j,i)); }
	}
}
///----------------------------------------------------------------------------------------------//
void TGridMap::Construct(const int &WidthVal, const int &HeightVal)
{
	Exception(WidthVal  > 0, L"in Construct(...) WidthVal <= 0");
	Exception(HeightVal > 0, L"in Construct(...) HeightVal <= 0");

	Width = WidthVal;
	Height = HeightVal;

	int Size(Width*Height);
	Cells.reserve(Size);

	for(int i=0;i<Height;i++){
		for(int j=0;j<Width;j++){
			pTGridMapCell Cell(new TGridMapCell);

			Cell->SetMapIndex(j,i);
			Cell->SetMapPos(&Pos);
			Cell->SetPos(GenCellPos(j,i));

			Cells.push_back(Cell);
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TGridMap::SetPos(const float &x,const float &y,const float &z)
{
	Pos(x,y,z);
}
///----------------------------------------------------------------------------------------------//
void TGridMap::SetPos(const TPoint &Val)
{
	Pos(Val);
}
///----------------------------------------------------------------------------------------------//
TPoint TGridMap::GetPos() const
{
	return Pos;
}
///----------------------------------------------------------------------------------------------//
void TGridMap::SetCellPosShift(const float &x,const float &y,const float &z)
{
	CellPosShift(x,y,z);
	UpdateCellPosShift();
}
///----------------------------------------------------------------------------------------------//
void TGridMap::SetCellPosShift(const TPoint &Val)
{
	CellPosShift(Val);
	UpdateCellPosShift();
}
///----------------------------------------------------------------------------------------------//
pTGridMapCell TGridMap::GetCell(const int &Index)
{
	return Cells[Index];
}
///----------------------------------------------------------------------------------------------//
pTGridMapCell TGridMap::GetCell(const int &WidthId,const int &HeightId)
{
	return Cells[HeightId*Width + WidthId];
}
///----------------------------------------------------------------------------------------------//
TPoint TGridMap::GetCellPos(const int &WidthId,const int &HeightId)
{
	return Cells[HeightId*Width + WidthId]->GetPos();
}
///----------------------------------------------------------------------------------------------//
TPoint TGridMap::GetCellGlobalPos(const int &WidthId,const int &HeightId)
{
	return Cells[HeightId*Width + WidthId]->GetGlobalPos();
}
///----------------------------------------------------------------------------------------------//
int TGridMap::GetHeight() const
{
	return Height;
}
///----------------------------------------------------------------------------------------------//
int TGridMap::GetWidth()  const
{
	return Width;
}
///----------------------------------------------------------------------------------------------//
int TGridMap::GetSize()   const
{
	return Width*Height;
}
///----------------------------------------------------------------------------------------------//
void TGridMap::Clear()
{
	Width  = 0;
	Height = 0;

	int Size(Cells.size());
	for(int i=0;i<Size;i++){ delete Cells[i]; }
	Cells.clear();
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

