#include "SimplePathWalker.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TSimplePathWalker::TSimplePathWalker():TBaseAction(), Speed(0.0f),
										Paths(NULL), CurrPath(NULL), CurrPathCursor(0)
{
}
///----------------------------------------------------------------------------------------------//
TSimplePathWalker::~TSimplePathWalker()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TSimplePathWalker::ToRun(const TEvent &Event)
{
	if(Event.Type != EN_TIMER || Event == false){return;}

	Iter.Run(Event);
	if(!Iter.GetActive()){
		CurrPathCursor++;
		int PathSize(CurrPath->GetSize());
		if(CurrPathCursor>=PathSize){
			Stop();
		}
		else{
			Iter.UsePosAndSpeed(*(CurrPath->Get(CurrPathCursor)), Speed);
			Iter.Start();
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TSimplePathWalker::ToStart()
{
	TExceptionGenerator Ex(L"TSimplePathWalker: ");
	Ex(Paths     !=NULL, L" Paths not set");
	Ex(Speed     > 0.0f, L" Speed not set");
	Ex(CurrPath != NULL, L" CurrPath not select, use UsePath(...) before Start()");

	Iter.UsePosAndSpeed(*(CurrPath->Get(CurrPathCursor)), Speed);
	Iter.Start();
}
///----------------------------------------------------------------------------------------------//
void TSimplePathWalker::UsePath(const pTPoint &From, const pTPoint &To)
{
	TExceptionGenerator Ex(L"TSimplePathWalker: ");
	Ex(Paths !=NULL, L" Paths not set");

	// первая точка пути та, на которой уже должен стоять объект
	CurrPathCursor = 0;
	CurrPath = Paths->Get(From, To);
}
///----------------------------------------------------------------------------------------------//
void TSimplePathWalker::SetPaths(const pTSimplePathList &Val)
{
	Paths = Val;
}
///----------------------------------------------------------------------------------------------//
void TSimplePathWalker::SetSpeed(const float &Val)
{
	Speed = Val;
}
///----------------------------------------------------------------------------------------------//
void TSimplePathWalker::SetPoint(const pTPoint &Val)
{
	Iter.Set(Val);
}
///----------------------------------------------------------------------------------------------//
void TSimplePathWalker::Set(const pTSimplePathList &PathsVal, const float &SpeedVal,
							const pTPoint &PointVal )
{
	Paths = PathsVal;
	Speed = SpeedVal;
	Iter.Set(PointVal);
}
///----------------------------------------------------------------------------------------------//
pTActionPointIterator TSimplePathWalker::GetIterPtr()
{
	return &Iter;
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
