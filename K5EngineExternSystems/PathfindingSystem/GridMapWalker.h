///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef K5PATHFINDING_LANDMAPWALKER_H_INCLUDED
#define K5PATHFINDING_LANDMAPWALKER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "GridMap.h"
#include "GridMapCellInfo.h"

#include <list>
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

using std::list;

class TGridMapWalker
{
	protected:
		TExceptionGenerator Exception;
		pTGridMap Map;
		bool DirOverview[3][3]; // ограничитель направления обхода ячеек

		pTGridMapCell Start;
		pTGridMapCell Finish;

		list<TGridMapCellInfo> PreparedCells;
		list<TGridMapCellInfo> ProcessedCells;
		bool*                  CheckCells;

		vector<pTGridMapCell> Path;
	protected:
		inline void PrepareCells(	TGridMapCellInfo &Info,
									const int &ShiftX, const int &ShiftY,
									const int &MapWidth, const int &MapHeight,
									bool* CheckCells);
	public:
		TGridMapWalker();
		~TGridMapWalker();

		void SetDirOverview(const int &X,const int &Y,const bool &Val);
		bool GetDirOverview(const int &X,const int &Y) const;

		bool FindPath(	const int &StartX, const int &StartY,
						const int &FinishX, const int &FinishY);

		void ClearTempLists();
		void ClearPath();

		void SetMap(const pTGridMap &Val);
		pTGridMap GetMap();

		int GetPathSize() const;
		pTGridMapCell GetPathCell(const int &Id);
};

typedef TGridMapWalker* pTGridMapWalker;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // K5PATHFINDING_LANDMAPWALKER_H_INCLUDED
