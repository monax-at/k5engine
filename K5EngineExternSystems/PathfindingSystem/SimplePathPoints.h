///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef K5PATHFINDING_SIMPLEPATHPOINTS_H_INCLUDED
#define K5PATHFINDING_SIMPLEPATHPOINTS_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TSimplePathPoints
{
	protected:
		TExceptionGenerator Exception;
		vector<pTPoint>    Points;
	public:
		TSimplePathPoints();
		virtual ~TSimplePathPoints();

		void operator=(TSimplePathPoints &Obj);

		void Add(const float &x,const float &y);
		void Add(const float &x,const float &y,const float &z);
		void Add(const TPoint &Val);

		void Del(const int &Index);
		void Del(const pTPoint &Val);

		pTPoint Get(const int &Index);
		pTPoint GetPointPtr(const TPoint &Val);

		int GetSize() const;

		void Clear();
};

typedef TSimplePathPoints* pTSimplePathPoints;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // K5PATHFINDING_SIMPLEPATHPOINTS_H_INCLUDED
