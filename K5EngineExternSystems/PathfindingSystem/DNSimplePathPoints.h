///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef K5PATHFINDING_TDNSIMPLEPATHPOINTS_H_INCLUDED
#define K5PATHFINDING_TDNSIMPLEPATHPOINTS_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "SimplePathPoints.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TDNSimplePathPoints
{
	protected:
		TExceptionGenerator	Exception;
	public:
		TDNSimplePathPoints();
		~TDNSimplePathPoints();

		void Init(const pTSimplePathPoints &Points, const pTDataNode &Node);
		void Init(const pTDataNode &Node, const pTSimplePathPoints &Points);

		pTSimplePathPoints Run(const pTDataNode &Node);
		pTDataNode Run(const pTSimplePathPoints &Points);
};

typedef TDNSimplePathPoints* pTDNSimplePathPoints;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // K5PATHFINDING_TDNSIMPLEPATHPOINTS_H_INCLUDED
