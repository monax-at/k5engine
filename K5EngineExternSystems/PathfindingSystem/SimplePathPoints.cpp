#include "SimplePathPoints.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

namespace K5Pathfinding
{

TSimplePathPoints::TSimplePathPoints()
{
}
///----------------------------------------------------------------------------------------------//
TSimplePathPoints::~TSimplePathPoints()
{
	Clear();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TSimplePathPoints::operator=(TSimplePathPoints &Obj)
{
	Clear();

	int Size(Obj.GetSize());
	for(int i=0;i<Size;i++){ Points.push_back(new TPoint(*Obj.Get(i))); }
}
///----------------------------------------------------------------------------------------------//
void TSimplePathPoints::Add(const float &x,const float &y)
{
	Points.push_back(new TPoint(x,y));
}
///----------------------------------------------------------------------------------------------//
void TSimplePathPoints::Add(const float &x,const float &y,const float &z)
{
	Points.push_back(new TPoint(x,y,z));
}
///----------------------------------------------------------------------------------------------//
void TSimplePathPoints::Add(const TPoint &Val)
{
	Points.push_back(new TPoint(Val));
}
///----------------------------------------------------------------------------------------------//
void TSimplePathPoints::Del(const int &Index)
{
	int Size(Points.size());
	Exception(Size > 0,L"in Del(Index) list is enpty");
	Exception(Index >= 0 && Index < Size,L"in Del(Index) Id out of range");

	delete Points[Index];

	Points.erase(Points.begin()+Index);
}
///----------------------------------------------------------------------------------------------//
void TSimplePathPoints::Del(const pTPoint &Val)
{
	int Size(Points.size());
	Exception(Size > 0,L"in Del(pTPoint) list is enpty");

	int DeleteId(-1);
	for(int i=0;i<Size;i++){
		if(Points[i] == Val){ DeleteId = i; break; }
	}

	Exception(DeleteId!=-1,L"in Del(pTPoint) point not exist");

	delete Points[DeleteId];

	Points.erase(Points.begin()+DeleteId);
}
///----------------------------------------------------------------------------------------------//
pTPoint TSimplePathPoints::Get(const int &Index)
{
	int Size(Points.size());
	Exception(Size > 0,L"in Get(Index) list is enpty");
	Exception(Index >= 0 && Index < Size,L"in Get(Index) Id out of range");

	return Points[Index];
}
///----------------------------------------------------------------------------------------------//
pTPoint TSimplePathPoints::GetPointPtr(const TPoint &Val)
{
	int Size(GetSize());
	for(int i=0;i<Size;i++){ if(*(Points[i]) == Val){return Points[i];} }

	return NULL;
}
///----------------------------------------------------------------------------------------------//
int TSimplePathPoints::GetSize() const
{
	return Points.size();
}
///----------------------------------------------------------------------------------------------//
void TSimplePathPoints::Clear()
{
	int Size(Points.size());
	for(int i=0; i<Size; i++){ delete Points[i]; }
	Points.clear();
}


}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
