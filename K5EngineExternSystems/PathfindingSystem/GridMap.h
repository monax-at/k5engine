///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef K5PATHFINDING_GRIDMAP_H_INCLUDED
#define K5PATHFINDING_GRIDMAP_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "GridMapCell.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TGridMap
{
	protected:
		TExceptionGenerator Exception;

		vector<pTGridMapCell> Cells;

		int Width;
		int Height;

		TPoint CellPosShift;
		TPoint Pos;
	protected:
		TPoint GenCellPos(const int &IndexX, const int &IndexY) const;
		void UpdateCellPosShift();
	public:
		TGridMap();
		virtual ~TGridMap();

		void Construct(const int &WidthVal, const int &HeightVal);

		void SetPos(const float &x,const float &y,const float &z);
		void SetPos(const TPoint &Val);
		TPoint GetPos() const;

		void SetCellPosShift(const float &x,const float &y,const float &z);
		void SetCellPosShift(const TPoint &Val);

		pTGridMapCell GetCell    (const int &Index);
		pTGridMapCell GetCell    (const int &WidthId,const int &HeightId);

		TPoint GetCellPos      (const int &WidthId,const int &HeightId);
		TPoint GetCellGlobalPos(const int &WidthId,const int &HeightId);

		int GetHeight() const;
		int GetWidth()  const;
		int GetSize()   const;

		void Clear();
};

typedef TGridMap* pTGridMap;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // LENDMAP_H_INCLUDED
