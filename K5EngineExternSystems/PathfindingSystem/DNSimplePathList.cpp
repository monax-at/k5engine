#include "DNSimplePathList.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TDNSimplePathList::TDNSimplePathList():Exception(L"TDNSimplePathList: "),Points(NULL)
{
}
///----------------------------------------------------------------------------------------------//
TDNSimplePathList::~TDNSimplePathList()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDNSimplePathList::Init(const pTSimplePathList  &List , const pTDataNode &Node)
{
	Exception(List   != NULL, L"in Init(List,Node) List not set");
	Exception(Points != NULL, L"in Init(List,Node) Points not set");

	TDNEngineDataConverter DC;

	int NodeSize(Node->GetSize());
	for(int i=0;i<NodeSize;i++){
		pTDataNode DNPath(Node->Get(i));
		if(DNPath->GetName() == L"Path"){

			pTSimplePath SimplePath(new TSimplePath);
			List->Add(SimplePath);

			int DNPathSize(DNPath->GetSize());
			for(int j=0;j<DNPathSize;j++){
				pTDataNode DNId(DNPath->Get(j));
				if(DNId->GetName() == L"Id"){
					SimplePath->Add(Points->Get(DNId->GetChildInt(L"val")));
				}
			}
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TDNSimplePathList::Init(const pTDataNode &Node, const pTSimplePathList   &List)
{
	Exception(List   != NULL, L"in Init(Node,List) List not set");
	Exception(Points != NULL, L"in Init(Node,List) Points not set");

	Node->SetName(L"SimplePathList");

	TDNEngineDataConverter DC;

	int Size(List->GetSize());
	for(int i=0;i<Size;i++){
		pTSimplePath Path(List->Get(i));
		pTDataNode DNPath(Node->Add(new TDataNode));
		DNPath->SetName(L"Path");

		int PathSize(Path->GetSize());
		for(int j=0;j<PathSize;j++){
			pTPoint Point(Path->Get(j));

			int PointsSize(Points->GetSize());
			for(int k=0;k<PointsSize;k++){
				if(Points->Get(k) == Point){
					pTDataNode DNId(DNPath->Add(new TDataNode));
					DNId->SetName(L"Id");
					DNId->Add(k,L"val");
					break;
				}
			}
		}
	}
}
///----------------------------------------------------------------------------------------------//
pTSimplePathList TDNSimplePathList::Run(const pTDataNode &Node)
{
	try{
		pTSimplePathList List(new TSimplePathList);

		Init(List,Node);

		return List;
	}
	catch(TException &Ex){
		Exception(L"in Run(Node): " + Ex.GetMess());
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNSimplePathList::Run(const pTSimplePathList &List)
{
	try{
		pTDataNode Node(new TDataNode);

		Init(Node, List);

		return Node;
	}
	catch(TException &Ex){
		Exception(L"in Run(Node): " + Ex.GetMess());
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
void TDNSimplePathList::SetPoints(const pTSimplePathPoints &Val)
{
	Points = Val;
}
///----------------------------------------------------------------------------------------------//
pTSimplePathPoints TDNSimplePathList::GetPoints()
{
	return Points;
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
