#include "SimplePathSmartWalker.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TSimplePathSmartWalker::TSimplePathSmartWalker():TBaseAction(), Speed(0.0f),
										Paths(NULL), CurrPath(NULL), CurrPathCursor(0)
{
}
///----------------------------------------------------------------------------------------------//
TSimplePathSmartWalker::~TSimplePathSmartWalker()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TSimplePathSmartWalker::ToRun(const TEvent &Event)
{
	if(Event.Type != EN_TIMER || Event == false){return;}

	// если на предыдущих итерациях не было перезапуска итератора, то останавливаем
	// ходилку и прерываем дальшейшее выполнение
	if(!Iter.GetActive()){
		Stop();
		return;
	}

	Iter.Run(Event);

	// далеее идёт выбор следующей точки, к которой надо двигатья, если текущая достигнута
	if(Iter.GetActive()){return;}

	CurrPathCursor++;
	if(CurrPath->GetSize() <= CurrPathCursor){
		Stop();
		return;
	}

	Iter.UsePosAndSpeed(*CurrPath->Get(CurrPathCursor), Speed);
	Iter.Start();
}
///----------------------------------------------------------------------------------------------//
void TSimplePathSmartWalker::ToStart()
{
	if(CurrPath == NULL){return;}

	Iter.Start();
}
///----------------------------------------------------------------------------------------------//
void TSimplePathSmartWalker::ToStop()
{
	CurrPath       = NULL;
	CurrPathCursor = 0;

	Iter.StopIsActive();
}
///----------------------------------------------------------------------------------------------//
void TSimplePathSmartWalker::GoTo(const pTPoint &To, const float &SpeedVal)
{
	TExceptionGenerator Ex(L"TSimplePathSmartWalker: ");
	Ex(Paths !=NULL      , L" Paths not set");
	Ex(SpeedVal > 0.0f   , L" Speed not set");
	Ex(Iter.Get() != NULL, L" Point not set");

	pTPoint Point(Iter.Get());
	if(GetDistance2D(*Point,*To) < Eps5){return;}

	CurrPath = NULL;

	int PathsSize(Paths->GetSize());
	for(int i=0;i<PathsSize;i++){
		pTSimplePath Path(Paths->Get(i));
		if(Path->GetLast() == To){
			if(CurrPath == NULL){
				CurrPath = Path;
			}
			else{
				if(GetDistance2D(*Path->GetFirst()    , *Point) <
				   GetDistance2D(*CurrPath->GetFirst(), *Point))
				{
					CurrPath = Path;
				}
			}
		}
	}

	if(CurrPath == NULL){return;}

	// если объект в данный момент находится на стартовой точке, то следующая, к которой
	// надо двигаться - вторая точка, в противном случае двигаемся к первой точке пути
	if(GetDistance2D(*Point,*CurrPath->GetFirst()) < Eps5) { CurrPathCursor = 1; }
	else                                                   { CurrPathCursor = 0; }

	if(CurrPath->GetSize() <= CurrPathCursor){return;}

	Speed = SpeedVal;
	Iter.UsePosAndSpeed(*CurrPath->Get(CurrPathCursor), Speed);
}
///----------------------------------------------------------------------------------------------//
void TSimplePathSmartWalker::SetPaths(const pTSimplePathList &Val)
{
	Paths = Val;
}
///----------------------------------------------------------------------------------------------//
void TSimplePathSmartWalker::SetPoint(const pTPoint &Val)
{
	Iter.Set(Val);
}
///----------------------------------------------------------------------------------------------//
void TSimplePathSmartWalker::Set(const pTSimplePathList &PathsVal,const pTPoint &PointVal)
{
	Paths = PathsVal;
	Iter.Set(PointVal);
}
///----------------------------------------------------------------------------------------------//
pTActionPointIterator TSimplePathSmartWalker::GetIterPtr()
{
	return &Iter;
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
