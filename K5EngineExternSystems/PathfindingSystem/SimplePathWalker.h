///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef K5PATHFINDING_SIMPLEPATHWALKER_H_INCLUDED
#define K5PATHFINDING_SIMPLEPATHWALKER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "SimplePathList.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TSimplePathWalker:public TBaseAction
{
	protected:
		float Speed;
		pTSimplePathList Paths;

		pTSimplePath CurrPath;
		int          CurrPathCursor;

		TActionPointIterator Iter;
	protected:
		void ToRun(const TEvent &Event);
		void ToStart();
	public:
		TSimplePathWalker();
		virtual ~TSimplePathWalker();

		void UsePath(const pTPoint &From, const pTPoint &To);

		void SetPaths(const pTSimplePathList &Val);
		void SetSpeed(const float &Val);
		void SetPoint(const pTPoint &Val);

		void Set     (const pTSimplePathList &PathsVal, const float &SpeedVal,
					  const pTPoint &PointVal);

		pTActionPointIterator GetIterPtr();
};

typedef TSimplePathWalker* pTSimplePathWalker;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // SIMPLEPATHWALKER_H_INCLUDED
