#include "GridMapWalker.h"

#include <stdexcept>
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
bool SortPreparedCells(const TGridMapCellInfo &A, const TGridMapCellInfo &B)
{
	return (A.Dist + A.StepCost) < (B.Dist + B.StepCost);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TGridMapWalker::TGridMapWalker():Exception(L"TGridMapWalker: "), Map(NULL),
											Start(NULL), Finish(NULL), CheckCells(NULL)
{
	for(int i=0;i<3;i++){ for(int j=0;j<3;j++){DirOverview[i][j] = true;} }
}
///----------------------------------------------------------------------------------------------//
TGridMapWalker::~TGridMapWalker()
{
}
///----------------------------------------------------------------------------------------------//
void TGridMapWalker::PrepareCells(	TGridMapCellInfo &Info,
									const int &ShiftX  , const int &ShiftY,
									const int &MapWidth, const int &MapHeight,
									bool* CheckCells )
{
	// выход, если проверяется центральная ячейка,
	if(ShiftX == 0 && ShiftY == 0){return;}

	// выход, если проверка этой ячейки отключена
	// +1 так как массив ограничителя двухмерный а индексы передаются из карты, и индекс
	// может быть -1, поэтому надо +1 что бы не было выхода за границы массива
	if(!DirOverview[ShiftY+1][ShiftX+1]){return;}

	int X(Info.Cell->GetMapIndexX() + ShiftX);
	int Y(Info.Cell->GetMapIndexY() + ShiftY);

	// выход, если проверка вышла за размеры карты
	if(X <0 || X > MapWidth  -1){return;}
	if(Y <0 || Y > MapHeight -1){return;}



	// выход, если такая ячейка уже была занесена в один из списков.
	// сделано через передаваемый указатель на массив флагов для того, что бы избежать
	// постоянного поиска по ячейкам, хранящимся в PreparedCells и ProcessedCells
	int CheckCellsShift(Y*MapWidth + X);
	if(CheckCells[CheckCellsShift]){return;}
	CheckCells[CheckCellsShift] = true;


	// обработка ячейки, установка параметров и занесение её в список проверяемых, если
	// она проходима для объекта
	pTGridMapCell Cell(Map->GetCell(X,Y));

	if(Cell->GetBlock()){return;}

	int PathCostCof(10);
	if( Abs(ShiftX) == 1 && Abs(ShiftY) == 1){ PathCostCof = 14; }

	TGridMapCellInfo NewInfo;

	NewInfo.Set(Cell, Cell->GetPathCost()*PathCostCof,
				(Finish->GetMapIndexX() - X)*10,(Finish->GetMapIndexY() - Y)*10);

	NewInfo.StepCost += Info.StepCost;
	NewInfo.Parent = &Info;

	PreparedCells.push_back(NewInfo);
}
///----------------------------------------------------------------------------------------------//
bool TGridMapWalker::FindPath(	const int &StartX, const int &StartY,
								const int &FinishX, const int &FinishY)
{
	Exception(Map != NULL, L" in FindPath(...) Map not set");

	ClearPath();

	Start  = Map->GetCell( StartX , StartY );
	Finish = Map->GetCell( FinishX, FinishY );

	TGridMapCellInfo CellInfo;
	CellInfo.Set(Start, 0, (FinishX - StartX)*10, (FinishY - StartY)*10);

	// добавление первой ячейки, которая будет обходиться при поиске пути
	PreparedCells.push_back(CellInfo);

	// размеры карты для проверки выхода за границу и для формирвоания массива флагов,
	// которые указывают, обходилась ли ячейка карты или нет
	int MapWidth (Map->GetWidth());
	int MapHeight(Map->GetHeight());

	int Size(MapHeight*MapWidth);

	CheckCells = new bool[Size];
	for(int i = 0; i < Size; i++){ CheckCells[i] = false; }
	CheckCells[StartY*MapWidth + StartX] = true;

	while(!PreparedCells.empty ()){

		list<TGridMapCellInfo>::iterator Itr = PreparedCells.begin();

		if( (*Itr).Cell != Finish){

			// подготовка ячеек к дальнейшей обработке
			for(int i=-1;i<2;i++){
				for(int j=-1;j<2;j++){
					PrepareCells((*Itr), i, j, MapWidth, MapHeight, CheckCells);
				}
			}

			// перекидывание текущей ячейки в обработанные
			ProcessedCells.splice(	ProcessedCells.end()  , PreparedCells,
									PreparedCells .begin(), ++Itr );

			PreparedCells.sort(SortPreparedCells);
		}
		else{
			// закидывание последней ячейки в обработанные
			ProcessedCells.splice(	ProcessedCells.end()  , PreparedCells,
									PreparedCells .begin(), ++Itr );

			// построение пути и выход
			list<TGridMapCellInfo>::iterator FItr = ProcessedCells.end();
			FItr--;

			TGridMapCellInfo Info(*FItr);
			pTGridMapCellInfo CellInfo(&Info);

			// получение вектора пути и переворачивание его, что бы был
			// от старта к финишу.
			vector<pTGridMapCell> TmpPath;

			while(CellInfo!=NULL){
				TmpPath.push_back(CellInfo->Cell);
				CellInfo = CellInfo->Parent;
			}

			for ( vector<pTGridMapCell>::reverse_iterator Itr = TmpPath.rbegin();
				  Itr < TmpPath.rend(); ++Itr )
			{
				Path.push_back(*Itr);
			}

			ClearTempLists();

			return true;
		}
	}

	ClearTempLists();

	return false;
}
///----------------------------------------------------------------------------------------------//
void TGridMapWalker::ClearTempLists()
{
	PreparedCells .clear();
	ProcessedCells.clear();

	delete[] CheckCells;
	CheckCells = NULL;
}
///----------------------------------------------------------------------------------------------//
void TGridMapWalker::ClearPath()
{
	Path.clear();
}
///----------------------------------------------------------------------------------------------//
void TGridMapWalker::SetDirOverview(const int &X,const int &Y,const bool &Val)
{
	Exception(X >=-1 && X<2, L"in SetDirOverview(...) X out of range: X >=0 && X<3" );
	Exception(Y >=-1 && Y<2, L"in SetDirOverview(...) Y out of range: Y >=0 && Y<3" );

	DirOverview[Y+1][X+1] = Val;
}
///----------------------------------------------------------------------------------------------//
bool TGridMapWalker::GetDirOverview(const int &X,const int &Y) const
{
	Exception(X >=-1 && X<2, L"in GetDirOverview(...) X out of range: X >=0 && X<3" );
	Exception(Y >=-1 && Y<2, L"in GetDirOverview(...) Y out of range: Y >=0 && Y<3" );

	return DirOverview[Y+1][X+1];
}
///----------------------------------------------------------------------------------------------//
void TGridMapWalker::SetMap(const pTGridMap &Val)
{
	Map = Val;
}
///----------------------------------------------------------------------------------------------//
pTGridMap TGridMapWalker::GetMap()
{
	return Map;
}
///----------------------------------------------------------------------------------------------//
int TGridMapWalker::GetPathSize() const
{
	return Path.size();
}
///----------------------------------------------------------------------------------------------//
pTGridMapCell TGridMapWalker::GetPathCell(const int &Id)
{
	try                          { return Path[Id]; }
	catch(std::out_of_range &oor){ Exception(L"in GetPathCell(Id) Id out of range"); }

	return NULL;
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
