#include "SimplePath.h"
#include <stdexcept>
#include <algorithm>
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TSimplePath::TSimplePath():Exception(L"TSimplePath: ")
{
}
///----------------------------------------------------------------------------------------------//
TSimplePath::~TSimplePath()
{
	Clear();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TSimplePath::operator=(TSimplePath &Path)
{
	Clear();

	int Size(Path.GetSize());
	for(int i=0;i<Size;i++){ Points.push_back(Path.Get(i)); }
}
///----------------------------------------------------------------------------------------------//
void TSimplePath::Add(const pTPoint &Val)
{
	Points.push_back(Val);
}
///----------------------------------------------------------------------------------------------//
pTPoint TSimplePath::Get(const int &Index) const
{
	try                          { return Points[Index]; }
	catch(std::out_of_range &oor){ Exception(L"in Get(Index) Index out of range"); }

	return pTPoint();
}
///----------------------------------------------------------------------------------------------//
pTPoint TSimplePath::GetFirst() const
{
	int Size(Points.size());
	Exception(Size>0, L"in GetFirst() points not add");
	return Points[0];
}
///----------------------------------------------------------------------------------------------//
pTPoint TSimplePath::GetLast()  const
{
	int Size(Points.size());
	Exception(Size>0, L"in GetLast() points not add");
	return Points[Size -1];
}
///----------------------------------------------------------------------------------------------//
int TSimplePath::GetSize() const
{
	return Points.size();
}
///----------------------------------------------------------------------------------------------//
TVector2D TSimplePath::GetDirVector(const int &StartIndex, const int &FinishIndex) const
{
	int Size(Points.size());
	Exception(Size>0, L"in GetDirVector() points not add");
	Exception(StartIndex >=0 && StartIndex <Size, L"in GetDirVector() StartIndex out of range");
	Exception(FinishIndex>=0 && FinishIndex<Size, L"in GetDirVector() FinishIndex out of range");
	Exception(StartIndex!=FinishIndex, L"in GetDirVector() StartIndex == FinishIndex");

	return TVector2D(*Points[StartIndex], *Points[FinishIndex]);
}
///----------------------------------------------------------------------------------------------//
void TSimplePath::Reverse()
{
	std::reverse(Points.begin(), Points.end());
}
///----------------------------------------------------------------------------------------------//
void TSimplePath::Clear()
{
	Points.clear();
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

