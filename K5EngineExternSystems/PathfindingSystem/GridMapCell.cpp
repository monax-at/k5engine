#include "GridMapCell.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

TGridMapCell::TGridMapCell():MapPos(NULL),PathCost(1),Block(false)
{
	MapIndex[0] = -1;
	MapIndex[1] = -1;

	for(int i=0;i<3;i++){ for(int j=0;j<3;j++){ PathDirs[i][j] = true; } }
}
///----------------------------------------------------------------------------------------------//
TGridMapCell::~TGridMapCell()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TGridMapCell::SetMapPos(const pTPoint &Val)
{
	MapPos = Val;
}
///----------------------------------------------------------------------------------------------//
pTPoint TGridMapCell::GetMapPos()
{
	return MapPos;
}
///----------------------------------------------------------------------------------------------//
void TGridMapCell::SetPos(const TPoint &Val)
{
	Pos = Val;
}
///----------------------------------------------------------------------------------------------//
void TGridMapCell::SetPos(const float &x,const float &y)
{
	Pos(x,y);
}
///----------------------------------------------------------------------------------------------//
void TGridMapCell::SetPos(const float &x,const float &y,const float &z)
{
	Pos(x,y,z);
}
///----------------------------------------------------------------------------------------------//
TPoint TGridMapCell::GetPos() const
{
	return Pos;
}
///----------------------------------------------------------------------------------------------//
TPoint TGridMapCell::GetGlobalPos() const
{
	if(MapPos == NULL){return Pos;}

	return *MapPos + Pos;
}
///----------------------------------------------------------------------------------------------//
void TGridMapCell::SetPathCost(const int &Val)
{
	PathCost = Val;
}
///----------------------------------------------------------------------------------------------//
int TGridMapCell::GetPathCost() const
{
	return PathCost;
}
///----------------------------------------------------------------------------------------------//
void TGridMapCell::SetBlock(const bool &Val)
{
	Block = Val;
}
///----------------------------------------------------------------------------------------------//
bool TGridMapCell::GetBlock() const
{
	return Block;
}
///----------------------------------------------------------------------------------------------//
void TGridMapCell::SetMapIndex(const int &x, const int &y)
{
	MapIndex[0] = x;
	MapIndex[1] = y;
}
///----------------------------------------------------------------------------------------------//
int TGridMapCell::GetMapIndexX() const
{
	return MapIndex[0];
}
///----------------------------------------------------------------------------------------------//
int TGridMapCell::GetMapIndexY() const
{
	return MapIndex[1];
}
///----------------------------------------------------------------------------------------------//
void TGridMapCell::SetPathDir(const int &IndexX, const int &IndexY, const bool &Val)
{
	TExceptionGenerator Ex(L"TGridMapCell: ");
	Ex(IndexX >=0 && IndexX <3, L"in SetPathDir(...) IndexX out of range: "+ IntToWStr(IndexX));
	Ex(IndexY >=0 && IndexY <3, L"in SetPathDir(...) IndexY out of range: "+ IntToWStr(IndexY));

	PathDirs[IndexX][IndexY] = Val;
}
///----------------------------------------------------------------------------------------------//
bool TGridMapCell::GetPathDir(const int &IndexX, const int &IndexY) const
{
	TExceptionGenerator Ex(L"TGridMapCell: ");
	Ex(IndexX >=0 && IndexX <3, L"in GetPathDir(...) IndexX out of range: "+ IntToWStr(IndexX));
	Ex(IndexY >=0 && IndexY <3, L"in GetPathDir(...) IndexY out of range: "+ IntToWStr(IndexY));

	return PathDirs[IndexX][IndexY];
}
///----------------------------------------------------------------------------------------------//
bool TGridMapCell::CheckPathDir(const int &ShiftX, const int &ShiftY) const
{
	TExceptionGenerator Ex(L"TGridMapCell: ");
	Ex(	ShiftX > -2 && ShiftX < 2, L"CheckPathDir(...) ShiftX out of range: " + IntToWStr(ShiftX));
	Ex(	ShiftY > -2 && ShiftY < 2, L"CheckPathDir(...) ShiftY out of range: " + IntToWStr(ShiftY));

	return PathDirs[ShiftX+1][ShiftY+1];
}

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
