///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef K5PATHFINDING_DNSIMPLEPATHLIST_H_INCLUDED
#define K5PATHFINDING_DNSIMPLEPATHLIST_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "SimplePathList.h"
#include "SimplePathPoints.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
namespace K5Pathfinding
{

class TDNSimplePathList
{
	protected:
		TExceptionGenerator	Exception;

		pTSimplePathPoints Points;
	public:
		TDNSimplePathList();
		~TDNSimplePathList();

		void Init(const pTSimplePathList &List, const pTDataNode &Node);
		void Init(const pTDataNode &Node, const pTSimplePathList &List);

		pTSimplePathList Run(const pTDataNode &Node);
		pTDataNode Run(const pTSimplePathList &List);

		void SetPoints(const pTSimplePathPoints &Val);
		pTSimplePathPoints GetPoints();
};

typedef TDNSimplePathList* pTDNSimplePathList;

}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // K5PATHFINDING_DNSIMPLEPATHLIST_H_INCLUDED
