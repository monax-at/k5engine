#include "BassDevice.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TBassDevice::TBassDevice():TBaseMusicDevice()
{
	Exception.SetPrefix(L"TBassDevice: ");
}
///--------------------------------------------------------------------------------------//
TBassDevice::~TBassDevice()
{
	BASS_Free();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TBassDevice::Create()
{
	TExceptionGenerator Ex(L"TBassDevice: ");

	Ex(HIWORD(BASS_GetVersion()) == BASSVERSION, L"in Create() BASSVERSION not correct" );
	Ex(BASS_Init(-1, 22050, BASS_DEVICE_3D , 0, NULL),L"BASS_Init(...) error" );
}
///--------------------------------------------------------------------------------------//
pTBaseMusicTrack TBassDevice::CreateMusicTrack()
{
	return new TBassMusicTrack;
}
///--------------------------------------------------------------------------------------//
pTBaseMusicTrackLoader TBassDevice::CreateMusicTrackLoader()
{
	return new TBassMusicTrackLoader;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
