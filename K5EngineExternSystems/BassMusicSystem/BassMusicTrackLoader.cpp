#include "BassMusicTrackLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TBassMusicTrackLoader::TBassMusicTrackLoader():TBaseMusicTrackLoader()
{
	Exception.SetPrefix(L"TBassMusicTrackLoader: ");
}
///--------------------------------------------------------------------------------------//
TBassMusicTrackLoader::~TBassMusicTrackLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseMusicTrack TBassMusicTrackLoader::Load(const wstring &File, const wstring &MusicName)
{
	HSTREAM Stream(BASS_StreamCreateFile(FALSE,WStrToStr(File).c_str(),0,0,0));
	Exception(Stream!=0,L"in Load() can't load music");

	pTBassMusicTrack Track(new TBassMusicTrack);
	Track->SetName(MusicName);
	Track->SetStream(Stream);

	return Track;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
