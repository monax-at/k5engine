#include "BassMusicTrack.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TBassMusicTrack::TBassMusicTrack():TBaseMusicTrack(),PlaySoundState(false),Stream(0)
{
}
///--------------------------------------------------------------------------------------//
TBassMusicTrack::~TBassMusicTrack()
{
	if(Stream!=0){BASS_StreamFree(Stream);}
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TBassMusicTrack::ToSetActive()
{
}
///--------------------------------------------------------------------------------------//
void TBassMusicTrack::ToSetRepeat()
{
	if(Stream == 0){return;}
}
///--------------------------------------------------------------------------------------//
void TBassMusicTrack::ToSetVolume()
{
	if(Stream == 0){return;}
}
///--------------------------------------------------------------------------------------//
void TBassMusicTrack::ToPlay()
{
	if(Stream == 0){return;}
	BASS_ChannelPlay( Stream, false );
}
///--------------------------------------------------------------------------------------//
void TBassMusicTrack::ToStop()
{
	if(Stream == 0){return;}
	BASS_ChannelPause(Stream);
}
///--------------------------------------------------------------------------------------//
void TBassMusicTrack::ToReset()
{
	if(Stream == 0){return;}
	BASS_ChannelStop(Stream);
}
///--------------------------------------------------------------------------------------//
bool TBassMusicTrack::ToIsPlaying()
{
	if(Stream == 0){return false;}

	switch( BASS_ChannelIsActive(Stream) ){

		case BASS_ACTIVE_STOPPED:{ return false; }break;
		case BASS_ACTIVE_PLAYING:{ return true;  }break;
		case BASS_ACTIVE_PAUSED	:{ return false; }break;
		case BASS_ACTIVE_STALLED:{ return false; }break;

		default:{ return false; }break;
	}

	return false;
}
///--------------------------------------------------------------------------------------//
void TBassMusicTrack::SetStream(const HSTREAM &Val)
{
	Stream = Val;
}
///--------------------------------------------------------------------------------------//
HSTREAM TBassMusicTrack::GetStream() const
{
	return Stream;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
