///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef BASSMUSICTRACK_H_INCLUDED
#define BASSMUSICTRACK_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"

#include <bass.h>
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TBassMusicTrack:public TBaseMusicTrack
{
	protected:
		bool PlaySoundState;
		HSTREAM Stream;
	protected:
		void ToSetActive();
		void ToSetRepeat();
		void ToSetVolume();

		void ToPlay();
		void ToStop();
		void ToReset();
		bool ToIsPlaying();
	public:
		TBassMusicTrack();
		virtual ~TBassMusicTrack();

		void SetStream(const HSAMPLE &Val);
		HSTREAM GetStream() const;
};

typedef TBassMusicTrack* pTBassMusicTrack;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // BASSMUSICTRACK_H_INCLUDED
