///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	�����: ������ �������. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef FTFONTFACE_H_INCLUDED
#define FTFONTFACE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OUTLINE_H

#include <string>
using std::wstring;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TFTFontFace
{
	private:
		wstring Name;
		bool    FaceInit;
		FT_Face Face;
	public:
		TFTFontFace();
		TFTFontFace(const wstring &NameVal,const FT_Face &FaceVal);
		~TFTFontFace();

		void SetName(const wstring &Val);
		wstring GetName() const;

		void SetFace(const FT_Face &Val);
		FT_Face GetFace();

		void Del();
};

typedef TFTFontFace* pTFTFontFace;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // FTFONTFACE_H_INCLUDED
