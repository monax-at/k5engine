#include "FTGlyphCreator.h"
#include "FTGlobals.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TFTGlyphCreator::TFTGlyphCreator():
							Exception(L"TFTGlyphCreator: "), HoriAdvance(0.0f), VertBearingX(0.0f),
							VertBearingY(0.0f), Device(NULL), FontFace(NULL), FontSize(16)
{
}
///----------------------------------------------------------------------------------------------//
TFTGlyphCreator::TFTGlyphCreator(	const pTBaseDevice &DevVal, const pTFTFontFace &FFace,
									const int &FSize):
							Exception(L"TFTGlyphCreator: "), HoriAdvance(0.0f), VertBearingX(0.0f),
							VertBearingY(0.0f), Device(DevVal), FontFace(FFace), FontSize(FSize)
{
}
///----------------------------------------------------------------------------------------------//
TFTGlyphCreator::~TFTGlyphCreator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TFTGlyphCreator::PrepareFTData(const wchar_t &Symbol)
{
	TFTFontFace *FTFontFace = dynamic_cast<TFTFontFace*>(FontFace);
	FTFace = FTFontFace->GetFace();

	FT_Error FTError;

	FTError = FT_Set_Char_Size(FTFace, 0,64*FontSize, 0, 0);
	Exception(FTError == 0,L"in PrepareFTData(...) FT_Set_Char_Size()");

	FTError = FT_Load_Char(FTFace, Symbol, FT_LOAD_RENDER);
	Exception(FTError == 0,L"in PrepareFTData(...) FT_Load_Char()");

	FTError = FT_Get_Glyph( FTFace->glyph, &FTGlyph);
	Exception(FTError == 0,L"in PrepareFTData(...) FT_Get_Glyph()");

	Exception(	FTGlyph->format == FT_GLYPH_FORMAT_BITMAP,
				L"in PrepareFTData(...) FTGlyph->format != FT_GLYPH_FORMAT_BITMAP");

	HoriAdvance  = (float)FTFace->glyph->metrics.horiAdvance /64.0f;
	VertBearingX = (float)FTFace->glyph->metrics.horiBearingX/64.0f;
	VertBearingY = (float)FTFace->glyph->metrics.horiBearingY/64.0f;

	FT_BitmapGlyph  FTGlyphBitmap = (FT_BitmapGlyph)FTGlyph;

	FTBitmap = FTGlyphBitmap->bitmap;
}
///----------------------------------------------------------------------------------------------//
pTGlyph TFTGlyphCreator::Run(const wchar_t &Symbol)
{
	Exception(Device   != NULL,	L"in Run(Symbol) Device   is NULL");
	Exception(FontFace != NULL,	L"in Run(Symbol) FontFace is NULL");
	Exception(FontSize > 0,		L"in Run(Symbol) FontSize is 0");

	PrepareFTData(Symbol);

	pTGlyph Glyph(new TGlyph);

	Glyph->SetSymbol(Symbol);
	Glyph->SetShifts(HoriAdvance, VertBearingX, VertBearingY);

	// параметры текстуры, которые будут в конце установлены
	int Width (0);
	int Height(0);
	unsigned char* TexData(NULL);

	// если генерируется символ без данных (например пробел), то будет создана
	// текстура минимального размера в 8 на 8
	if(FTBitmap.buffer == NULL){
		Glyph->SetGlyphSizes(8,8);

		Width  = 8;
		Height = 8;

		int Size = 64 * 4;
		TexData = new unsigned char[Size];
		memset(TexData,0,Size);
	}
	else{
		Glyph->SetGlyphSizes(FTBitmap.width, FTBitmap.rows);

		Width  = NextDeg2(FTBitmap.width + 2);
		Height = NextDeg2(FTBitmap.rows  + 2);

		int Size(Width*Height*2);

		unsigned char* ExpandedData = new unsigned char[Size];
		memset(ExpandedData,0,sizeof(unsigned char)*Size);

		// операции над данными в зависимости от типа рендера, фактически проводится перегон
		// данных из одного буфера в другой с последующей обработкой
		// TODO: посмотреть, можно ли упростить и оптимизировать реализацию
		switch(FTGenTextureType){
			case EN_FTGTT_DX8: {
				for(int j=1; j < Height -1; j++) {
					for(int i=1; i < Width -1; i++ ){
						int ibmp = i-1;
						int jbmp = j-1;
						if(ibmp < FTBitmap.width && jbmp <FTBitmap.rows){
							ExpandedData[2*(i + j*Width)]   =
							ExpandedData[2*(i + j*Width)+1] =
								FTBitmap.buffer[ibmp + FTBitmap.width*jbmp];
						}
					}
				}
			}break;

			case EN_FTGTT_GL: {
				for(int j=1; j < Height -1; j++) {
					for(int i=1; i < Width -1; i++ ){
						int ibmp = i-1;
						int jbmp = j-1;

						if(ibmp < FTBitmap.width && jbmp <FTBitmap.rows){
							ExpandedData[2*((Width -1 -i) + j*Width)]   =
							ExpandedData[2*((Width -1 -i) + j*Width)+1] =
									FTBitmap.buffer[ibmp + FTBitmap.width*jbmp];
						}
					}
				}

				// переворот текстуры глифа, фт генериует не в той последовательности, которая в
				// движке используется
				unsigned char* RotationData = new unsigned char[Size];
				for(int i = 0; i < Size; i++){ RotationData[Size -1 -i] = ExpandedData[i]; }
				for(int i = 0; i < Size; i++){ ExpandedData[i]          = RotationData[i]; }
				delete[] RotationData;
			}break;
		}
		//
		TexData = new unsigned char[Width * Height * 4];

		// перегоняем данные в RGBA политру
		int jCount = 0;
		for(int iCount=0; iCount < Size ; iCount = iCount + 2){
			TexData[jCount]     = ExpandedData[iCount];
			TexData[jCount + 1] = ExpandedData[iCount];
			TexData[jCount + 2] = ExpandedData[iCount];
			TexData[jCount + 3] = ExpandedData[iCount + 1];

			jCount = jCount + 4;
		}

		delete[] ExpandedData;
	}

	pTBaseTextureCreater TextureCreater(Device->CreateTextureCreater());

	pTTexture Texture(TextureCreater->Run(L"", Width, Height, TexData));
	Texture->Stop();
	Glyph  ->SetTexture(Texture);

	delete[] TexData;
	delete   TextureCreater;

	return Glyph;
}
///----------------------------------------------------------------------------------------------//
void TFTGlyphCreator::SetParams(const pTBaseDevice &DevVal, const pTFTFontFace &FFace,
								const int &FSize)
{
	Device   = DevVal;
	FontFace = FFace;
	FontSize = FSize;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
