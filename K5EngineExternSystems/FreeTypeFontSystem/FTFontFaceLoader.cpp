#include "FTFontFaceLoader.h"
#include "FTGlobals.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TFTFontFaceLoader::TFTFontFaceLoader()
{
}
///----------------------------------------------------------------------------------------------//
TFTFontFaceLoader::~TFTFontFaceLoader()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTFTFontFace TFTFontFaceLoader::Run(const wstring &FPath, const wstring &FName)
{
	TExceptionGenerator Ex(L"TFTFontFaceLoader: ");
	Ex(FTGlobalLibrary!=NULL,L"in Run(...) FTGlobalLibrary not init");

	FT_Face Face;
	FT_Error Error;
	Error = FT_New_Face(FTGlobalLibrary, WStrToStr(FPath).c_str(),0,&Face);

	Ex(Error==0,L"in Run(...) can't load font face from file: "+FPath);

	pTFTFontFace Data(new TFTFontFace);

	Data->SetName(FName);
	Data->SetFace(Face);

	return Data;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
