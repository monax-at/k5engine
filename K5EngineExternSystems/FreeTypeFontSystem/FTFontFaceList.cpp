#include "FTFontFaceList.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TFTFontFaceList::TFTFontFaceList():Exception(L"TFTFontFaceList: ")
{
}
///----------------------------------------------------------------------------------------------//
TFTFontFaceList::~TFTFontFaceList()
{
	Clear();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
inline pTFTFontFace TFTFontFaceList::GetElem(const int &Id)
{
	int Size = Elems.size();
	Exception(Id>=0 && Id<Size,L"in function GetElem(Id) Id not in range");
	return Elems[Id];
}
///----------------------------------------------------------------------------------------------//
inline pTFTFontFace TFTFontFaceList::GetElem(const wstring &Name)
{
	int Size = Elems.size();
	Exception(Size>0,L"in function GetElem(Name) List is enpty");
	Exception(Name.length()>0,L"in function GetElem(Name) Name not set");

	for(int i=0; i<Size; i++){
		if(Elems[i]->GetName() == Name){
			return Elems[i];
		}
	}

	Exception(L"in function GetElem(wstring) elem whis name "+Name+L" not exist");
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTFTFontFace TFTFontFaceList::operator[](const int &Id)
{
	return GetElem(Id);
}
///----------------------------------------------------------------------------------------------//
pTFTFontFace TFTFontFaceList::operator[](const wstring &Name)
{
	return GetElem(Name);
}
///----------------------------------------------------------------------------------------------//
pTFTFontFace TFTFontFaceList::Get(const int &Id)
{
	return GetElem(Id);
}
///----------------------------------------------------------------------------------------------//
pTFTFontFace TFTFontFaceList::Get(const wstring &Name)
{
	return GetElem(Name);
}
///----------------------------------------------------------------------------------------------//
pTFTFontFace TFTFontFaceList::GetLast()
{
	int Size(Elems.size());
	Exception(Size > 0,L"in GetLast() list is enpty");
	return Elems[Size-1];
}
///----------------------------------------------------------------------------------------------//
pTFTFontFace TFTFontFaceList::Add(const pTFTFontFace &Val)
{
	Exception(Val!=NULL,L"in Add(...) elem is NULL");
	wstring ValName = Val->GetName();
	Exception(!IsExist(ValName),L"in Add(...) elem whis name "+ValName+L" already exist");

	Elems.push_back(Val);

	return Val;
}
///----------------------------------------------------------------------------------------------//
bool TFTFontFaceList::IsExist(const wstring &Name)
{
	int Size = Elems.size();
	for(int i=0; i<Size; i++){
		if(Elems[i]->GetName() == Name){return true;}
	}
	return false;
}
///----------------------------------------------------------------------------------------------//
int  TFTFontFaceList::GetSize()
{
	return Elems.size();
}
///----------------------------------------------------------------------------------------------//
void TFTFontFaceList::Clear()
{
	int Size = Elems.size();
	for(int i=0; i<Size; i++){
		Elems[i]->Del();
		delete Elems[i];
	}
	Elems.clear();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
