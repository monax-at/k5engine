///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "FTGlyphCreator.h"

#include <memory>

using std::auto_ptr;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TFTFont:public TBaseFont
{
	protected:
		pTFTFontFace FontFace;
		pTBaseDevice Device;
	public:
		TFTFont();
		TFTFont(const pTFTFontFace &NewFontFace, const pTBaseDevice  &DeviceVal,
				const int &NewFontSize = 16, const wstring &FontName=L"");

		virtual ~TFTFont();

		void Set(	const pTFTFontFace &NewFontFace, const pTBaseDevice  &DeviceVal,
					const int &NewFontSize = 16, const wstring &FontName=L"");

		pTGlyph GetGlyph(const wchar_t &Symbol);
};

typedef TFTFont* pTFTFont;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // FONT_H_INCLUDED
