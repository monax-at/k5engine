#include "FTFont.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TFTFont::TFTFont():TBaseFont(), FontFace(NULL), Device(NULL)
{
}
///----------------------------------------------------------------------------------------------//
TFTFont::TFTFont(const pTFTFontFace &NewFontFace, const pTBaseDevice  &DeviceVal,
				 const int &NewFontSize, const wstring &FontName)
{
	FontFace = NewFontFace;
	Device   = DeviceVal;

	FontSize = NewFontSize;
	Name     = FontName;
}
///----------------------------------------------------------------------------------------------//
TFTFont::~TFTFont()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TFTFont::Set(const pTFTFontFace &NewFontFace , const pTBaseDevice &DeviceVal,
				  const int &NewFontSize, const wstring &FontName)
{
	FontFace = NewFontFace;
	Device   = DeviceVal;

	FontSize = NewFontSize;
	Name 	 = FontName;
}
///----------------------------------------------------------------------------------------------//
pTGlyph TFTFont::GetGlyph(const wchar_t  &Symbol)
{
	TExceptionGenerator Ex(L"TFTFont: ");

	Ex(Device	!= NULL, L"in GetGlyph(...) Device is NULL");
	Ex(FontFace	!= NULL, L"in GetGlyph(...) FontFace is NULL");

	if(GlyphList.IsExist(Symbol)){ return GlyphList[Symbol]; }

	TFTGlyphCreator GlyphCreator(Device,FontFace,FontSize);
	GlyphList.Add( GlyphCreator.Run(Symbol) );

	return GlyphList.GetLast();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
