///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef FTBASEGLYPHCREATOR_H_INCLUDED
#define FTBASEGLYPHCREATOR_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "FTFontFace.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TFTGlyphCreator
{
	protected:
		TExceptionGenerator Exception;

		FT_Face FTFace;

		FT_Glyph FTGlyph;

		FT_Bitmap FTBitmap;

		float HoriAdvance;
		float VertBearingX;
		float VertBearingY;

		pTBaseDevice Device;

		pTFTFontFace FontFace;
		int          FontSize;
	protected:
		void PrepareFTData(const wchar_t &Symbol);
	public:
		TFTGlyphCreator();
		TFTGlyphCreator(const pTBaseDevice &DevVal, const pTFTFontFace &FFace, const int &FSize=16);
		~TFTGlyphCreator();

		pTGlyph Run(const wchar_t &Symbol);

		void SetParams(const pTBaseDevice &DevVal, const pTFTFontFace &FFace, const int &FSize=16);
};

typedef TFTGlyphCreator* pTFTGlyphCreator;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // FTBASEGLYPHCREATOR_H_INCLUDED
