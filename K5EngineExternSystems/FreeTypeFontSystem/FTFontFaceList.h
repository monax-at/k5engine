#ifndef FTFONTFACELIST_H_INCLUDED
#define FTFONTFACELIST_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "FTFontFace.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TFTFontFaceList
{
	protected:
		TExceptionGenerator Exception;
		vector<pTFTFontFace> Elems;
	protected:
        inline pTFTFontFace GetElem(const int &Id);
        inline pTFTFontFace GetElem(const wstring &Name);
	public:
		TFTFontFaceList();
		~TFTFontFaceList();

        pTFTFontFace operator[](const int &Id);
        pTFTFontFace operator[](const wstring &Name);

        pTFTFontFace Get(const int &Id);
        pTFTFontFace Get(const wstring &Name);
        pTFTFontFace GetLast();

		pTFTFontFace Add(const pTFTFontFace &Val);
		bool IsExist(const wstring &Name);

        int  GetSize();
		void Clear();
};

typedef TFTFontFaceList* pTFTFontFaceList;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // FTFONTFACELIST_H_INCLUDED
