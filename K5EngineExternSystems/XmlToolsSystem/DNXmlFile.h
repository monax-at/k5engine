///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNXMLFILE_H_INCLUDED
#define DNXMLFILE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"
#include "XmlBaseDataDecoder.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNXmlFile
{
	protected:
		TExceptionGenerator Ex;
	protected:
		void ParseXmlTeg(TiXmlElement* XmlElem, const pTDataNode &Node);
		TiXmlElement* ParseDataNode(const pTDataNode &Node);
	public:
		TDNXmlFile ();
		~TDNXmlFile();

		void Load(const wstring &FileName, const pTDataNode &Node);
		TDataNode Load(const wstring &FileName);

		void Save  (const wstring &FileName, const pTDataNode &Node);

		void Create(const wstring &FileName);
		void Clear (const wstring &FileName);

		wstring   ToXmlText(TDataNode &Node);
		TDataNode FromXmlText(const wstring &Text);
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNXMLFILE_H_INCLUDED
