///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLENGINEDATADECODER_H_INCLUDED
#define XMLENGINEDATADECODER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "XmlBaseDataDecoder.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TXmlEngineDataDecoder:public TXmlBaseDataDecoder
{
	protected:
		pTTextureListManager	 Textures;
		pTSpriteListManager		 Sprites;
		pTTextListManager		 Texts;

		pTActionListManager		 Actions;
	public:
		TXmlEngineDataDecoder();
		virtual ~TXmlEngineDataDecoder();

		pTTexture 	  		ToTexture		(TiXmlElement *Elem);
		pTSprite 	  		ToSprite 		(TiXmlElement *Elem);
		pTText				ToText			(TiXmlElement *Elem);
		pTBaseAction		ToAction		(TiXmlElement *Elem);

		pTTexture 	 	  ToTexture		(TiXmlElement *Elem, const wstring &TegName);
		pTSprite 	  	  ToSprite 	 	(TiXmlElement *Elem, const wstring &TegName);
		pTText			  ToText		(TiXmlElement *Elem, const wstring &TegName);
		pTBaseAction	  ToAction	 	(TiXmlElement *Elem, const wstring &TegName);

		pTActionList AttrToActionList(TiXmlElement *Elem, const wstring &Attr);

		TPoint GetObjectPosWithAlignment(TiXmlElement *Elem,const pTBaseDevice &Device);

		TPoint GetObjectPosWithAlignment(	TiXmlElement *Elem,const wstring &TegName,
											const pTBaseDevice &Device);

		void SetTextures (const pTTextureListManager &Val);
		void SetSprites  (const pTSpriteListManager  &Val);
		void SetTexts    (const pTTextListManager    &Val);
		void SetActions  (const pTActionListManager  &Val);

		pTTextureListManager GetTextures();
		pTSpriteListManager  GetSprites ();
		pTTextListManager    GetTexts   ();
		pTActionListManager  GetActions ();
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // XMLENGINEDATADECODER_H_INCLUDED
