///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLBASEDATADECODER_H_INCLUDED
#define XMLBASEDATADECODER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"

#include <tinyxml.h>
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TXmlBaseDataDecoder
{
	protected:
		TExceptionGenerator Exception;
	protected:
		inline wstring CheckData(TiXmlElement *Val);
		inline TiXmlElement* GetSubTeg(TiXmlElement *Elem, const wstring &ElemName);

		inline TVector2D TegToVector2D(TiXmlElement *Val);
		inline TVector3D TegToVector3D(TiXmlElement *Val);
		inline TPoint    TegToPoint   (TiXmlElement *Val);
		inline TColor 	 TegToColor   (TiXmlElement *Val);
	public:
		TXmlBaseDataDecoder();
		virtual ~TXmlBaseDataDecoder();

		void SetExceptionPrefix(const wstring &Val);

		TiXmlElement* GetElem(TiXmlElement *Elem, const wstring &ElemName);
		TiXmlElement* GetFirstElem(TiXmlElement *Elem);

		wstring ToWStr (TiXmlElement *Val);
		int 	ToInt  (TiXmlElement *Val);
		bool 	ToBool (TiXmlElement *Val);
		float 	ToFloat(TiXmlElement *Val);

		bool IsTegExist	(TiXmlElement  *Elem, const wstring &TegName);
		bool IsAttrExist(TiXmlElement  *Elem, const wstring &Attr);

		wstring AttrToWStr	(TiXmlElement *Elem, const wstring &Attr);
		int 	AttrToInt	(TiXmlElement *Elem, const wstring &Attr);
		bool 	AttrToBool	(TiXmlElement *Elem, const wstring &Attr);
		float 	AttrToFloat	(TiXmlElement *Elem, const wstring &Attr);

		enTextureFilter AttrToTextureFilter(TiXmlElement *Elem, const wstring &Attr);

		wstring GetTegText(TiXmlElement  *Elem);

		TVector2D 	ToVector2D	(TiXmlElement *Val);
		TVector3D 	ToVector3D	(TiXmlElement *Val);
		TPoint 	    ToPoint  	(TiXmlElement *Val);
		TColor 	 	ToColor  	(TiXmlElement *Val);

		TVector2D 	ToVector2D	(TiXmlElement *Elem, const wstring &TegName);
		TVector3D 	ToVector3D	(TiXmlElement *Elem, const wstring &TegName);
		TPoint 	    ToPoint 	(TiXmlElement *Elem, const wstring &TegName);
		TColor 	 	ToColor  	(TiXmlElement *Elem, const wstring &TegName);

		TiXmlElement* WStrToElem	(const wstring &Elem,const wstring &Val);
		TiXmlElement* IntToElem		(const wstring &Elem,const int &Val);
		TiXmlElement* BoolToElem	(const wstring &Elem,const bool &Val);
		TiXmlElement* FloatToElem	(const wstring &Elem,const float &Val);

		void WStrToAttr(TiXmlElement*Elem,
						const wstring &Attr,const wstring &Val);

		void IntToAttr(TiXmlElement*Elem,
						const wstring &Attr,const int &Val);

		void BoolToAttr(TiXmlElement*Elem,
						const wstring &Attr,const bool &Val);

		void FloatToAttr(TiXmlElement*Elem,
						 const wstring &Attr,const float &Val);

		TiXmlElement* Vector2DToElem(const wstring &Elem, const TVector2D &Val);
		TiXmlElement* Vector3DToElem(const wstring &Elem, const TVector3D &Val);
		TiXmlElement* Point3DToElem	(const wstring &Elem, const TPoint  &Val);
		TiXmlElement* ColorToElem	(const wstring &Elem, const TColor    &Val);
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DATADECODER_H_INCLUDED
