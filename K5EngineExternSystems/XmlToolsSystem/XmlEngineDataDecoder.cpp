#include "XmlEngineDataDecoder.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TXmlEngineDataDecoder::TXmlEngineDataDecoder():TXmlBaseDataDecoder(),
		Textures(NULL), Sprites(NULL), Texts(NULL), Actions(NULL)
{
	Exception.SetPrefix(L"TXmlEngineDataDecoder: ");
}
///----------------------------------------------------------------------------------------------//
TXmlEngineDataDecoder::~TXmlEngineDataDecoder()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTTexture TXmlEngineDataDecoder::ToTexture(TiXmlElement *Elem)
{
	Exception(Textures != NULL, L"in ToTexture(...) Textures is NULL");
	Exception(Elem 	   != NULL, L"in ToTexture(...) Elem is NULL");

	return Textures->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///----------------------------------------------------------------------------------------------//
pTSprite TXmlEngineDataDecoder::ToSprite(TiXmlElement *Elem)
{
	Exception(Sprites != NULL, L"in ToSprite(...) Sprites is NULL");
	Exception(Elem 	  != NULL, L"in ToSprite(...) Elem is NULL");

	return Sprites->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///----------------------------------------------------------------------------------------------//
pTText TXmlEngineDataDecoder::ToText(TiXmlElement *Elem)
{
	Exception(Texts	!= NULL, L"in ToTextLine(...) Texts is NULL");
	Exception(Elem	!= NULL, L"in ToTextLine(...) Elem is NULL");

	return Texts->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TXmlEngineDataDecoder::ToAction(TiXmlElement *Elem)
{
	Exception(Actions != NULL, L"in ToAction(...) Actions is NULL");
	Exception(Elem 	  != NULL, L"in ToAction(...) Elem is NULL");

	return Actions->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///----------------------------------------------------------------------------------------------//
pTTexture TXmlEngineDataDecoder::ToTexture(	TiXmlElement *Elem,
											const wstring &TegName)
{
	return ToTexture(GetElem(Elem,TegName));
}
///----------------------------------------------------------------------------------------------//
pTSprite TXmlEngineDataDecoder::ToSprite(TiXmlElement *Elem,const wstring &TegName)
{
	return ToSprite(GetElem(Elem,TegName));
}
///----------------------------------------------------------------------------------------------//
pTText TXmlEngineDataDecoder::ToText(TiXmlElement *Elem,const wstring &TegName)
{
	return ToText(GetElem(Elem,TegName));
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TXmlEngineDataDecoder::ToAction(TiXmlElement *Elem,
											 const wstring &TegName)
{
	return ToAction(GetElem(Elem,TegName));
}
///----------------------------------------------------------------------------------------------//
pTActionList TXmlEngineDataDecoder::AttrToActionList(	TiXmlElement *Elem,
														const wstring &Attr)
{
	Exception(Actions!=NULL,L"in AttrToActionList(...) Actions is NULL");
	return Actions->Get(AttrToWStr(Elem,Attr));
}
///----------------------------------------------------------------------------------------------//
TPoint TXmlEngineDataDecoder::GetObjectPosWithAlignment(	TiXmlElement *Elem,
															const pTBaseDevice &Device)
{
	if(Device == NULL){ return ToPoint(Elem); }

	TPoint Pos(ToPoint(Elem));

	if( IsAttrExist(Elem, L"alignment_x") ){

		wstring AlignmentX(AttrToWStr(Elem, L"alignment_x"));

		if(AlignmentX == L"left" ) 	{ Pos[0] = Pos.GetX(); }
		if(AlignmentX == L"right" )	{ Pos[0] = Device->GetWidth() - Pos.GetX(); }
		if(AlignmentX == L"center" ){ Pos[0]+= Device->GetWidth()/2.0f; }

	}

	if( IsAttrExist(Elem, L"alignment_y") ){
		wstring AlignmentY(AttrToWStr(Elem, L"alignment_y"));

		if(AlignmentY == L"top" )	{ Pos[1] = Pos.GetY(); }
		if(AlignmentY == L"bottom" ){ Pos[1] = Device->GetHeight() - Pos.GetY();}
		if(AlignmentY == L"center" ){ Pos[1]+= Device->GetHeight()/2.0f; }
	}

	return Pos;
}
///----------------------------------------------------------------------------------------------//
TPoint TXmlEngineDataDecoder::GetObjectPosWithAlignment(	TiXmlElement *Elem,
										const wstring &TegName,const pTBaseDevice &Device)
{
	return GetObjectPosWithAlignment(GetElem(Elem,TegName),Device);
}
///----------------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///----------------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Sprites = Val;
}
///----------------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetTexts(const pTTextListManager &Val)
{
	Texts = Val;
}
///----------------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetActions(const pTActionListManager &Val)
{
	Actions = Val;
}
///----------------------------------------------------------------------------------------------//
pTTextureListManager TXmlEngineDataDecoder::GetTextures()
{
	return Textures;
}
///----------------------------------------------------------------------------------------------//
pTSpriteListManager	TXmlEngineDataDecoder::GetSprites()
{
	return Sprites;
}
///----------------------------------------------------------------------------------------------//
pTTextListManager TXmlEngineDataDecoder::GetTexts()
{
	return Texts;
}
///----------------------------------------------------------------------------------------------//
pTActionListManager	TXmlEngineDataDecoder::GetActions()
{
	return Actions;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
