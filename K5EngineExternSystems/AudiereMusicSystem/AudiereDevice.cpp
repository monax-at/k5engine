#include "AudiereDevice.h"
#include "AudiereMusicSystemGlobals.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TAudiereDevice::TAudiereDevice():TBaseMusicDevice()
{
	Exception.SetPrefix(L"TAudiereDevice: ");
}
///--------------------------------------------------------------------------------------//
TAudiereDevice::~TAudiereDevice()
{
}
///--------------------------------------------------------------------------------------//
void TAudiereDevice::Create()
{
	AudiereAudioDevice = OpenDevice();
	Exception(AudiereAudioDevice,L"in Create() can't open AudioDevice");
}
///--------------------------------------------------------------------------------------//
pTBaseMusicTrack TAudiereDevice::CreateMusicTrack()
{
	return new TAudiereMusicTrack;
}
///--------------------------------------------------------------------------------------//
pTBaseMusicTrackLoader TAudiereDevice::CreateMusicTrackLoader()
{
	return new TAudiereMusicTrackLoader;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
