#include "AudiereMusicTrackLoader.h"
#include "AudiereMusicSystemGlobals.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TAudiereMusicTrackLoader::TAudiereMusicTrackLoader():TBaseMusicTrackLoader()
{
	Exception.SetPrefix(L"TAudiereMusicTrackLoader: ");
}
///--------------------------------------------------------------------------------------//
TAudiereMusicTrackLoader::~TAudiereMusicTrackLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseMusicTrack TAudiereMusicTrackLoader::Load(const wstring &File,
												const wstring &MusicName)
{
	Exception(AudiereAudioDevice,L"in Load(...) sound system not init");
	OutputStreamPtr Stream(OpenSound(AudiereAudioDevice,WStrToStr(File).c_str(),true));
	Exception(Stream,L"in Load(...) can't load sound: " + File);

	return new TAudiereMusicTrack(Stream,MusicName);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
