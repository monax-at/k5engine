///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef AUDIEREMUSICTRACK_H_INCLUDED
#define AUDIEREMUSICTRACK_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"

#include <audiere.h>

using namespace audiere;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TAudiereMusicTrack:public TBaseMusicTrack
{
	protected:
		OutputStreamPtr Sound;
		bool PlaySoundState;
	protected:
		void ToSetActive();
		void ToSetRepeat();
		void ToSetVolume();

		void ToPlay();
		void ToStop();
		void ToReset();
		bool ToIsPlaying();
	public:
		TAudiereMusicTrack();
		TAudiereMusicTrack(const OutputStreamPtr &StreamVal,const wstring &NameVal);
		virtual ~TAudiereMusicTrack();

		void SetSoundData(const OutputStreamPtr &Val);
		OutputStreamPtr GetSoundData();
};

typedef TAudiereMusicTrack* pTAudiereMusicTrack;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // MUSICTRACK_H_INCLUDED
