#include "AudiereMusicTrack.h"
#include "AudiereMusicSystemGlobals.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TAudiereMusicTrack::TAudiereMusicTrack():TBaseMusicTrack(),Sound(NULL)
{
}
///--------------------------------------------------------------------------------------//
TAudiereMusicTrack::TAudiereMusicTrack(	const OutputStreamPtr &StreamVal,
										const wstring &NameVal):TBaseMusicTrack()
{
	Sound = StreamVal;
	Name  = NameVal;
}
///--------------------------------------------------------------------------------------//
TAudiereMusicTrack::~TAudiereMusicTrack()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TAudiereMusicTrack::ToSetActive()
{
	if(Active == false){
		if(Sound){
			PlaySoundState = Sound->isPlaying();
			if(PlaySoundState){ Sound->stop();}
		}
	}
	else{
		if(Sound){
			if(PlaySoundState){Sound->play();}
		}
	}
}
///--------------------------------------------------------------------------------------//
void TAudiereMusicTrack::ToSetRepeat()
{
	if(Sound){Sound->setRepeat(Repeat);}
}
///--------------------------------------------------------------------------------------//
void TAudiereMusicTrack::ToSetVolume()
{
	if(Sound){return Sound->setVolume(Volume);}
}
///--------------------------------------------------------------------------------------//
void TAudiereMusicTrack::ToPlay()
{
	if(!Sound){return;}

	if(Sound->isPlaying()){Sound->reset();}
	Sound->play();
}
///--------------------------------------------------------------------------------------//
void TAudiereMusicTrack::ToStop()
{
	if(Sound){Sound->stop();}
}
///--------------------------------------------------------------------------------------//
void TAudiereMusicTrack::ToReset()
{
	if(Sound){Sound->reset();}
}
///--------------------------------------------------------------------------------------//
bool TAudiereMusicTrack::ToIsPlaying()
{
	if(Sound){
		return Sound->isPlaying();
	}
	return false;
}
///--------------------------------------------------------------------------------------//
void TAudiereMusicTrack::SetSoundData(const OutputStreamPtr &Val)
{
	Sound = Val;
}
///--------------------------------------------------------------------------------------//
OutputStreamPtr TAudiereMusicTrack::GetSoundData()
{
	return Sound;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
