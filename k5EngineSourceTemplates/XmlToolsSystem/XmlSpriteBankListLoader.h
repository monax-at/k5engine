///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITEBANKLISTLOADER_H_INCLUDED
#define XMLSPRITEBANKLISTLOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "XmlSpriteListLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpriteBankListLoader:public TXmlBaseLoader
{
	private:
		pTGraphicBankList 	 List;
		pTTextureListManager Textures;
	private:
		inline void SpriteBankFromXmlElement(TiXmlElement* Elem);

		void ToRun(TiXmlElement *Teg);
		void ToRun(const wstring &File);
	public:
		TXmlSpriteBankListLoader();
		TXmlSpriteBankListLoader(const pTGraphicBankList &Bank,
								 const pTTextureListManager &Tex);
		virtual ~TXmlSpriteBankListLoader();

		void SetList(const pTGraphicBankList &Val);
		void SetTextures(const pTTextureListManager &Val);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSPRITEBANKLISTLOADER_H_INCLUDED
