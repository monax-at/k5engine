#include "XmlSpriteBankListManagerLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteBankListManagerLoader::TXmlSpriteBankListManagerLoader():
							TXmlBaseLoader(),SpriteBanks(NULL),Textures(NULL)
{
	Exception.SetPrefix(L"TXmlSpriteBankListManagerLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlSpriteBankListManagerLoader::TXmlSpriteBankListManagerLoader
			(const pTSpriteBankListManager &Spr,const pTTextureListManager &Tex):
			 TXmlBaseLoader(),SpriteBanks(Spr),Textures(Tex)
{
	Exception.SetPrefix(L"TXmlSpriteBankListManagerLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlSpriteBankListManagerLoader::~TXmlSpriteBankListManagerLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListManagerLoader::ListFromXmlElement(TiXmlElement *Elem)
{
	TXmlBaseDataDecoder XmlDecoder;

	SpriteBanks->Add(XmlDecoder.AttrToWStr(Elem,L"name"));
	TSpriteBankList *List = SpriteBanks->GetLast();

	if(XmlDecoder.IsAttrExist(Elem,L"view")){
		List->SetView(XmlDecoder.AttrToBool(Elem,L"view"));
	}

	TXmlSpriteBankListLoader ListLoader(List,Textures);
	ListLoader(Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListManagerLoader::ToRun(TiXmlElement *Teg)
{
    if(Teg == NULL){return;}
    ListFromXmlElement(Teg);
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListManagerLoader::ToRun(const wstring &File)
{
	Exception(SpriteBanks!=NULL, L"in ToRun(...) SpriteBanks must be set");
	Exception(Textures	 !=NULL, L"in ToRun(...) Textures must be set");

	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in ToRun(...) can't open file");

	TiXmlHandle Handle(&Doc);

	TiXmlElement* ElemData = Handle.FirstChildElement().Element();
	Exception(ElemData!=NULL,L"in ToRun(...) teg Data must be set");

	TiXmlElement* ElemSprite = NULL;
	for(ElemSprite = ElemData->FirstChildElement(); ElemSprite != NULL;
		ElemSprite = ElemSprite->NextSiblingElement())
	{
		ListFromXmlElement(ElemSprite);
	}
	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListManagerLoader::SetSpriteBanks
											(const pTGraphicBankListManager &Val)
{
	SpriteBanks = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListManagerLoader::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListManagerLoader::Set(	const pTGraphicBankListManager &Spr,
											const pTTextureListManager &Tex)
{
	SpriteBanks = Spr;
	Textures = Tex;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
