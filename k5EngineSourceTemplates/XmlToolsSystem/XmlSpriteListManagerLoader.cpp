#include "XmlSpriteListManagerLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteListManagerLoader::TXmlSpriteListManagerLoader():
								TXmlBaseLoader(),Sprites(NULL),Textures(NULL)
{
	Exception.SetPrefix(L"TXmlSpriteListManagerLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlSpriteListManagerLoader::TXmlSpriteListManagerLoader
				(const pTSpriteListManager &Spr,const pTTextureListManager &Tex):
								TXmlBaseLoader(),Sprites(Spr),Textures(Tex)
{
	Exception.SetPrefix(L"TXmlSpriteListManagerLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlSpriteListManagerLoader::~TXmlSpriteListManagerLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlSpriteListManagerLoader::ListFromXmlElement(TiXmlElement *Elem)
{
	TXmlBaseDataDecoder XmlDecoder;

	Sprites->Add(XmlDecoder.AttrToWStr(Elem,L"name"));
	pTSpriteList List = Sprites->GetLast();

	if(XmlDecoder.IsAttrExist(Elem,L"view")){
		List->SetView(XmlDecoder.AttrToBool(Elem,L"view"));
	}

	TXmlSpriteListLoader ListLoader(List,Textures);
	ListLoader(Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListManagerLoader::ToRun(TiXmlElement *Teg)
{
	Exception(Sprites  != NULL, L"in ToRun(...) Sprites must be set");
	Exception(Textures != NULL, L"in ToRun(...) Textures must be set");

	if(Teg != NULL) ListFromXmlElement(Teg);
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListManagerLoader::ToRun(const wstring &File)
{
	Exception(Sprites  != NULL, L"in ToRun(...) Sprites must be set");
	Exception(Textures != NULL, L"in ToRun(...) Textures must be set");

	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in ToRun(...) can't open file: "+File);

	TiXmlHandle Handle(&Doc);

	TiXmlElement* ElemData = Handle.FirstChildElement().Element();
	Exception(ElemData!=NULL,L"in ToRun(...) teg Data must be set");

	TiXmlElement* ElemSprite = NULL;
	for(ElemSprite = ElemData->FirstChildElement(); ElemSprite != NULL;
		ElemSprite = ElemSprite->NextSiblingElement())
	{
		ListFromXmlElement(ElemSprite);
	}
	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListManagerLoader::SetSprites(const pTSpriteListManager &Val)
{
	Sprites = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListManagerLoader::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListManagerLoader::Set(	const pTSpriteListManager &Spr,
										const pTTextureListManager &Tex)
{
	Sprites = Spr;
	Textures = Tex;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
