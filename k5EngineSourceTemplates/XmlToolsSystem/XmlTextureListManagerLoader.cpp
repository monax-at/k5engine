#include "XmlTextureListManagerLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlTextureListManagerLoader::TXmlTextureListManagerLoader():TXmlBaseLoader(),
					Exception(L"TXmlTextureListManagerLoader: "),
					Device(NULL),Textures(NULL)
{
	DevInitWorker.Run();
}
///--------------------------------------------------------------------------------------//
TXmlTextureListManagerLoader::TXmlTextureListManagerLoader
					(const pTBaseDevice &Dev,const pTTextureListManager &Tex):
					TXmlBaseLoader(),Exception(L"TXmlTextureListManagerLoader: "),
					Device(Dev),Textures(Tex)
{
	DevInitWorker.Run();
}
///--------------------------------------------------------------------------------------//
TXmlTextureListManagerLoader::~TXmlTextureListManagerLoader()
{
	DevInitWorker.Run();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
int TXmlTextureListManagerLoader::GetPowTow(const int &Val)
{
	int Pow = 1;
	while(Val > Pow){
		Pow = Pow * 2;
	}
	return Pow;
}
///--------------------------------------------------------------------------------------//
int TXmlTextureListManagerLoader::GetQuadSize(const int &WidthVal,const int &HeightVal)
{
	int Width  = GetPowTow(WidthVal);
	int Hieght = GetPowTow(HeightVal);
///------->
	if(Width >= Hieght) return Width;
	return Hieght;
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::LoadTextureFromFile(	const pTTextureList &List,
														TiXmlElement *Elem)
{
	wstring Name(XmlDecoder.AttrToWStr(Elem,L"name"));
	wstring Path(XmlDecoder.AttrToWStr(Elem,L"path"));

	pTBaseTextureCreater pTextureCreater = Device->CreateTextureCreater();

	if(	XmlDecoder.IsAttrExist(Elem,L"x") 	  && XmlDecoder.IsAttrExist(Elem, L"y") &&
		XmlDecoder.IsAttrExist(Elem,L"width") && XmlDecoder.IsAttrExist(Elem,L"height") )
	{
		int x(XmlDecoder.AttrToInt(Elem,L"x"));
		int y(XmlDecoder.AttrToInt(Elem,L"y"));
		int width(XmlDecoder .AttrToInt(Elem,L"width"));
		int height(XmlDecoder.AttrToInt(Elem,L"height"));

		TTextureLoader Loader(Device);

		List->Add(Loader.Run(BuildPathStr(Path),x,y,width,height,Name));
	}
	else{
//		TTextureLoader Loader(Device);

		TFolderWorker FolderWorker;
		TDevilCanvas DefaultTexture(FolderWorker.GetAppPath() + BuildPathStr(Path));
		int iQuadSize = GetQuadSize(DefaultTexture.GetWidth(), DefaultTexture.GetHeight());
		TDevilCanvas PowTowTexture(iQuadSize,iQuadSize);

		TDevilCanvasWorker CanvasWorker;
		CanvasWorker.Copy(&DefaultTexture, &PowTowTexture);
		CanvasWorker.Mirror(&PowTowTexture);


		pTTexture pText = pTextureCreater->Run(Name,iQuadSize,iQuadSize,PowTowTexture.GetImageData());

		List->Add(pText);

//		List->Add(Loader.Run(BuildPathStr(Path),Name));
	}

	delete pTextureCreater;
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::LoadMaskFromFile(const pTTextureList &List,
													TiXmlElement *Elem, int MaskType)
{
	wstring Name(XmlDecoder.AttrToWStr(Elem,L"name"));
	wstring Path(XmlDecoder.AttrToWStr(Elem,L"path"));

	TTextureLoader Loader(Device);

	pTTexture Texture = Loader.Mask(Path,Name);
	List->Add(Texture);
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::LoadMaskFromTexture(	const pTTextureList &List,
														TiXmlElement *Elem, int MaskType)
{
	wstring Name(XmlDecoder.AttrToWStr(Elem,L"name"));
	wstring SourceList(XmlDecoder.AttrToWStr(Elem,L"source_list"));
	wstring SourceName(XmlDecoder.AttrToWStr(Elem,L"source_name"));

	pTTexture Source  = Textures->Get(SourceList,SourceName);
	pTTexture Texture = NULL;

	pTBaseTextureCreater pTexCreater = Device->CreateTextureCreater();

	switch(MaskType){
		case 0:{ Texture = Source->CreateCutMask(Name); }break;
		case 1:{ Texture = Source->CreateMask(Name);    }break;
		case 2:{
			int  StrokeSize = XmlDecoder.AttrToInt(Elem,L"size");

			pTTexture TextureMask = Source->CreateMask(Name);

			int iQuadSize = GetQuadSize(TextureMask->GetWidth() + 2 * StrokeSize, TextureMask->GetHeight() + 2 * StrokeSize) ;

			pTTexture TextureStroke         = pTexCreater->Run(Name, iQuadSize, iQuadSize);
			pTTextureRawData TextureRawData = TextureMask->GetCopyRawData(TextureMask->GetWidth(), TextureMask->GetHeight());
			TextureStroke->WriteRawData(TextureRawData, StrokeSize, 2 * StrokeSize, twmOr);
			TextureStroke->WriteRawData(TextureRawData, StrokeSize, 0, twmOr);
			TextureStroke->WriteRawData(TextureRawData, 0, StrokeSize, twmOr);
			TextureStroke->WriteRawData(TextureRawData, 2 * StrokeSize, StrokeSize, twmOr);

			Texture = TextureStroke;

			delete TextureRawData;
			delete TextureMask;
		}break;
		default:{
			Exception(L"Unknown mask type.");
		}break;
	}

	delete pTexCreater;
	List->Add(Texture);
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::ParseTexture(const pTTextureList &List,
                                            TiXmlElement *Elem)
{
	int LoadMode = 0; // простая загрузка текстуры
	// загрузка маски из файла
	// загрузка маски из существующей текстуры
	if(XmlDecoder.IsAttrExist(Elem,L"") == true){
		bool Mask = XmlDecoder.AttrToBool(Elem,L"cut_mask");
		if(Mask == true){
			if(XmlDecoder.IsAttrExist(Elem,L"path")){ LoadMode = 1;}
			else									{ LoadMode = 2;}
		}
	}
	if(XmlDecoder.IsAttrExist(Elem,L"soft_mask") == true){
		bool Mask = XmlDecoder.AttrToBool(Elem,L"soft_mask");
		if(Mask == true){
			if(XmlDecoder.IsAttrExist(Elem,L"path")){ LoadMode = 3;}
			else									{ LoadMode = 4;}
		}
	}
	if(XmlDecoder.IsAttrExist(Elem,L"stroke_mask") == true){
		bool Mask       = XmlDecoder.AttrToBool(Elem,L"stroke_mask");
		if(Mask == true){
			if(XmlDecoder.IsAttrExist(Elem,L"path")){ LoadMode = 5;}
			else									{ LoadMode = 6;}
		}
	}

	switch(LoadMode){
		case 0:{ LoadTextureFromFile(List,Elem); }break;
		case 1:{ LoadMaskFromFile	(List,Elem, 0); }break;
		case 2:{ LoadMaskFromTexture(List,Elem, 0); }break;
		case 3:{ LoadMaskFromFile	(List,Elem, 1); }break;
		case 4:{ LoadMaskFromTexture(List,Elem, 1); }break;
		case 5:{ LoadMaskFromFile	(List,Elem, 2); }break;
		case 6:{ LoadMaskFromTexture(List,Elem, 2); }break;
	}
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::ParseTextures(const pTTextureList &List,
												 TiXmlElement *Elem)
{
	for(TiXmlElement *ElemTexture = Elem->FirstChildElement();
		ElemTexture != NULL; ElemTexture = ElemTexture->NextSiblingElement())
	{
		ParseTexture(List,ElemTexture);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::CreateLists(TiXmlElement *Elem)
{
	for(TiXmlElement *ElemList = Elem->FirstChildElement();
		ElemList != NULL; ElemList = ElemList->NextSiblingElement())
	{
		Textures->Add(XmlDecoder.AttrToWStr(ElemList,L"name"));
	}
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::CreateTextures(TiXmlElement *Elem)
{
	for(TiXmlElement *ElemList = Elem->FirstChildElement();
		ElemList != NULL; ElemList = ElemList->NextSiblingElement())
	{
		pTTextureList List = Textures->Get(XmlDecoder.AttrToWStr(ElemList,L"name"));
		ParseTextures(List,ElemList);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::ToRun(TiXmlElement *Teg)
{
	if(Teg == NULL){return;}
	return;
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::ToRun(const wstring &File)
{
    Exception(Device!=NULL		, L"in FromXmlFile(...) Device is NULL");
    Exception(Textures!=NULL	, L"in FromXmlFile(...) Textures is NULL");
    Exception(File.length()>0	, L"in FromXmlFile(...) File not set");

	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"Can't open file.");

	try{
		TiXmlHandle Handle(&Doc);
		TiXmlElement* ElemData = Handle.FirstChildElement().Element();

		Exception(XmlDecoder.GetTegText(ElemData)==L"Data",L"first teg must be <Data>");

		CreateLists(ElemData);
		CreateTextures(ElemData);

		Doc.Clear();
	}
	catch(TException &Ex)
	{
		Exception(Ex);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlTextureListManagerLoader::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
