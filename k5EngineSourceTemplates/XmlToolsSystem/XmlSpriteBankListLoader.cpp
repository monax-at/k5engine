#include "XmlSpriteBankListLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteBankListLoader::TXmlSpriteBankListLoader():
							TXmlBaseLoader(),List(NULL),Textures(NULL)
{
	Exception.SetPrefix(L"TXmlSpriteBankListLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlSpriteBankListLoader::TXmlSpriteBankListLoader
					(const pTSpriteBankList &Bank,const pTTextureListManager &Tex):
					TXmlBaseLoader(),List(Bank),Textures(Tex)
{
	Exception.SetPrefix(L"TXmlSpriteBankListLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlSpriteBankListLoader::~TXmlSpriteBankListLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListLoader::SpriteBankFromXmlElement(TiXmlElement* Elem)
{
	TXmlBaseDataDecoder XmlDecoder;
	pTSpriteBank SpriteBank(new TSpriteBank);

	SpriteBank->SetName( XmlDecoder.AttrToWStr(Elem,L"name"));

	// элемент View, его может не быть, поэтому проверка на наличие
	if(XmlDecoder.IsAttrExist(Elem,L"view")){
		SpriteBank->SetView( XmlDecoder.AttrToBool(Elem,L"view"));
	}

	// элемент "Pos", элемент не обязателен
	TiXmlElement *ElemPos = Elem->FirstChildElement("Pos");
	if(ElemPos!=NULL){
		SpriteBank->Pos(XmlDecoder.ToPoint3D(ElemPos));
	}

	TiXmlElement *ElemCenter = Elem->FirstChildElement("Center");
	if(ElemCenter!=NULL){
		SpriteBank->Center(XmlDecoder.ToPoint3D(ElemCenter));
	}

	// элемент "Angle", элемент не обязателен
	TiXmlElement *ElemAngle = Elem->FirstChildElement("Angle");
	if(ElemAngle!=NULL){
		SpriteBank->Angle(XmlDecoder.AttrToFloat(ElemAngle,L"val"));
	}

	TiXmlElement *ElemSprites = Elem->FirstChildElement("Sprites");
	if(ElemSprites!=NULL){
		TXmlSpriteListLoader SpriteListLoader(SpriteBank->GetSpritesPtr(),Textures);
		SpriteListLoader.Run(ElemSprites);
	}
	SpriteBank->Sort();
	List->Add(SpriteBank);
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListLoader::ToRun(TiXmlElement *Teg)
{
	Exception(List != NULL,L"in Load(Teg) List is NULL");
	Exception(Textures != NULL,L"in Load(Teg) Textures is NULL");

	for(TiXmlElement* Bank = Teg->FirstChildElement();
		Bank != NULL; Bank = Bank->NextSiblingElement())
	{
		SpriteBankFromXmlElement(Bank);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListLoader::ToRun(const wstring &File)
{
	Exception(List != NULL,L"in Load(File) List is NULL");
	Exception(Textures != NULL,L"in Load(File) Textures is NULL");

	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in Load(File) can't open file.");

	TiXmlHandle Handle(&Doc);

	for(TiXmlElement* Bank = Handle.FirstChildElement().Element();
		Bank != NULL; Bank = Bank->NextSiblingElement())
	{
		SpriteBankFromXmlElement(Bank);
	}
	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListLoader::SetList(const pTGraphicBankList &Val)
{
	List = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBankListLoader::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
