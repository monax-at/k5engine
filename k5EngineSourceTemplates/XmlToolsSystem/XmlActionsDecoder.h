///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLACTIONSDECODER_H_INCLUDED
#define XMLACTIONSDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"
#include "Base/XmlBaseDataDecoder.h"
#include "Base/XmlBaseActionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlActionsDecoder
{
	protected:
		TExceptionGenerator Exception;
		TXmlBaseDataDecoder Decoder;
		vector<pTXmlBaseActionDecoder> Elems;
	public:
		TXmlActionsDecoder();
		~TXmlActionsDecoder();

		pTBaseAction Create(TiXmlElement* Elem);
		void Init(const pTBaseAction &Action, TiXmlElement* Elem);

		void CreateList(const pTActionList &List,TiXmlElement* Elem);
		void InitList(const pTActionList &List,TiXmlElement* Elem);

		void Add(const pTXmlBaseActionDecoder &Val);
		void Clear();

};

typedef TXmlActionsDecoder* pTXmlActionsDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLACTIONSDECODER_H_INCLUDED
