#include "XmlKinematicsSpriteManagerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlKinematicsSpriteManagerDecoder::TXmlKinematicsSpriteManagerDecoder():
									TXmlBaseEventActionDecoder()

{
	Exception.SetPrefix(L"TXmlKinematicsSpriteManagerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlKinematicsSpriteManagerDecoder: ");

	ActionTypeName = L"KinematicsSpriteManager";
}
///--------------------------------------------------------------------------------------//
TXmlKinematicsSpriteManagerDecoder::~TXmlKinematicsSpriteManagerDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlKinematicsSpriteManagerDecoder::Construct()
{
	return new TKinematicsSpriteManager;
}
///--------------------------------------------------------------------------------------//
void TXmlKinematicsSpriteManagerDecoder::Init(	const pTBaseEventAction &Action,
												TiXmlElement* Elem)
{
	pTKinematicsSpriteManager Obj(static_cast<pTKinematicsSpriteManager>(Action));

	if(Decoder.IsAttrExist(Elem,L"auto_run")){
		Obj->SetAutoRunFlag(Decoder.AttrToBool(Elem,L"auto_run"));
	}

	if(Decoder.IsAttrExist(Elem,L"pos_border")){
		Obj->SetPosBorderFlag(Decoder.AttrToBool(Elem,L"pos_border"));
	}

	if(Decoder.IsTegExist(Elem,L"CutPosFlags")){
		TiXmlElement* ElemCutPosFlags(Decoder.GetElem(Elem,L"CutPosFlags"));

		Obj->SetCutPosFlags(Decoder.AttrToBool(ElemCutPosFlags, L"left"),
							Decoder.AttrToBool(ElemCutPosFlags, L"right"),
							Decoder.AttrToBool(ElemCutPosFlags, L"top"),
							Decoder.AttrToBool(ElemCutPosFlags, L"bottom"));
	}

	if(Decoder.IsTegExist(Elem,L"CutPosLimits")){
		TiXmlElement* ElemCutPosLimits(Decoder.GetElem(Elem,L"CutPosLimits"));

		Obj->SetCutPosLimits(Decoder.AttrToFloat(ElemCutPosLimits, L"left"),
							 Decoder.AttrToFloat(ElemCutPosLimits, L"right"),
							 Decoder.AttrToFloat(ElemCutPosLimits, L"top"),
							 Decoder.AttrToFloat(ElemCutPosLimits, L"bottom"));
	}

	TXmlKinematicsForcesDecoder ForceDecoder;

	if(Decoder.IsTegExist(Elem,L"Initiators")){
		TiXmlElement* ElemInitiators(Decoder.GetElem(Elem,L"Initiators"));

		for(TiXmlElement* Initiator = ElemInitiators->FirstChildElement();
			Initiator != NULL; Initiator = Initiator->NextSiblingElement())
		{
			Obj->AddInitiator(ForceDecoder(Initiator));
		}
	}

	if(Decoder.IsTegExist(Elem,L"Forces")){
		TiXmlElement* ElemForces(Decoder.GetElem(Elem,L"Forces"));

		for(TiXmlElement* Force = ElemForces->FirstChildElement();
			Force != NULL; Force = Force->NextSiblingElement())
		{
			Obj->AddForce(ForceDecoder(Force));
		}
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
