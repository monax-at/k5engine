///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLKINEMATICSSPRITEMANAGERDECODER_H_INCLUDED
#define XMLKINEMATICSSPRITEMANAGERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"

#include "../XmlKinematicsForcesDecoder.h"
#include "STKinematicsSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlKinematicsSpriteManagerDecoder:public TXmlBaseEventActionDecoder
{
	public:
		TXmlKinematicsSpriteManagerDecoder();
		virtual ~TXmlKinematicsSpriteManagerDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);
};

typedef TXmlKinematicsSpriteManagerDecoder* pTXmlKinematicsSpriteManagerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSQUARECHECKERDECODER_H_INCLUDED
