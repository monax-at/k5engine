#include "XmlSpriteListLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteListLoader::TXmlSpriteListLoader():
					TXmlBaseLoader(),List(NULL),Textures(NULL)
{
	Exception.SetPrefix(L"TXmlSpriteListLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlSpriteListLoader::TXmlSpriteListLoader
					(const pTSpriteList &Sprites,const pTTextureListManager &Tex):
					TXmlBaseLoader(),List(Sprites),Textures(Tex)
{
	Exception.SetPrefix(L"TXmlSpriteListLoader: ");
	XmlSprite.SetTextures(Textures);
}
///--------------------------------------------------------------------------------------//
TXmlSpriteListLoader::~TXmlSpriteListLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlSpriteListLoader::ToRun(TiXmlElement *Teg)
{
	Exception(List != NULL,L"in Load(Teg) List is NULL");
	Exception(Textures != NULL,L"in Load(Teg) Textures is NULL");

	for(TiXmlElement* ElemSprite = Teg->FirstChildElement();
		ElemSprite != NULL; ElemSprite = ElemSprite->NextSiblingElement())
	{
		List->Add(XmlSprite.ToSprite(ElemSprite));
	}
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListLoader::ToRun(const wstring &File)
{
	Exception(List != NULL,L"in Load(File) List is NULL");
	Exception(Textures != NULL,L"in Load(File) Textures is NULL");

	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in Load(File) can't open file.");

	TiXmlHandle Handle(&Doc);

	for(TiXmlElement* ElemSprite = Handle.FirstChildElement().Element();
		ElemSprite != NULL; ElemSprite = ElemSprite->NextSiblingElement())
	{
		List->Add(XmlSprite.ToSprite(ElemSprite));
	}

	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListLoader::SetList(const pTSpriteList &Value)
{
	List = Value;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListLoader::SetTextures(const pTTextureListManager &Value)
{
	Textures = Value;
	XmlSprite.SetTextures(Textures);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
