#include "XmlTextListLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlTextListLoader::TXmlTextListLoader():
						TXmlBaseLoader(),TextList(NULL),FontList(NULL)
{
	Exception.SetPrefix(L"TXmlTextListLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlTextListLoader::TXmlTextListLoader
						(TBaseDevice *Dev, TTextList *Text, TFontList *Font):
						TXmlBaseLoader(), TextList(Text),FontList(Font)
{
	Exception.SetPrefix(L"TXmlTextListLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlTextListLoader::~TXmlTextListLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
inline void TXmlTextListLoader::ParseElement(TiXmlElement* Elem)
{
	TXmlText XmlText;
	XmlText.SetFontList(FontList);

	TextList->Add(XmlText.ToText(Elem));
}
///--------------------------------------------------------------------------------------//
void TXmlTextListLoader::ToRun(TiXmlElement *Teg)
{
	Exception(TextList	!=NULL, L"in LoadFromXmlTeg(...) TextList pointer must be set");
	Exception(FontList	!=NULL, L"in LoadFromXmlTeg(...) FontList pointer must be set");

	for(TiXmlElement* Elem = Teg->FirstChildElement(); Elem != NULL;
		Elem = Elem->NextSiblingElement())
	{
		ParseElement(Elem);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlTextListLoader::ToRun(const wstring &File)
{
	Exception(TextList	!=NULL, L"in LoadFromXmlFile(...) TextList pointer must be set");
	Exception(FontList	!=NULL, L"in LoadFromXmlFile(...) FontList pointer must be set");

	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in LoadFromXmlFile(...) can't open file");

	//try{
		TiXmlHandle Handle(&Doc);
		TiXmlElement* ElemData = Handle.FirstChildElement().Element();
		if(ElemData == NULL){return;}

		for(TiXmlElement* Elem = ElemData->FirstChildElement(); Elem != NULL;
			Elem = Elem->NextSiblingElement())
		{
			ParseElement(Elem);
		}
		Doc.Clear();
	//}
	//catch(TBaseException &Ex){
//		Exception(Ex);
	//}
}
///--------------------------------------------------------------------------------------//
void TXmlTextListLoader::SetParams(	TBaseDevice *Dev,TTextList *Text,
										TFontList *Font)
{
	Device 	 = Dev;
	TextList = Text;
	FontList = Font;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
