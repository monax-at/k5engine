#include "XmlTextListManagerLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlTextListManagerLoader::TXmlTextListManagerLoader():
								TXmlBaseLoader(),Texts(NULL),Fonts(NULL)
{
	Exception.SetPrefix(L"TXmlTextListManagerLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlTextListManagerLoader::TXmlTextListManagerLoader
								(const pTTextListManager &Tl,const pTFontList &F):
								TXmlBaseLoader(),Texts(Tl),Fonts(F)
{
	Exception.SetPrefix(L"TXmlTextListManagerLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlTextListManagerLoader::~TXmlTextListManagerLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlTextListManagerLoader::ListFromXmlElement(TiXmlElement *Elem)
{
	TXmlBaseDataDecoder XmlDecoder;

	Texts->Add(XmlDecoder.AttrToWStr(Elem,L"name"));
	TTextList *List = Texts->GetLast();

	if(XmlDecoder.IsAttrExist(Elem,L"view")){
		List->SetView(XmlDecoder.AttrToBool(Elem,L"view"));
	}

	TXmlTextListLoader ListLoader;
	ListLoader.SetParams(Texts->GetDevice(),List,Fonts);
	ListLoader.Run(Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlTextListManagerLoader::ToRun(TiXmlElement *Teg)
{
	Exception(Texts!=NULL, 	L"in Load(...) Texts must be set");
	Exception(Fonts!=NULL,	L"in Load(...) Fonts must be set");

	if(Teg != NULL) ListFromXmlElement(Teg);
}
///--------------------------------------------------------------------------------------//
void TXmlTextListManagerLoader::ToRun(const wstring &File)
{
	Exception(Texts!=NULL, 	L"in Load(...) Texts must be set");
	Exception(Fonts!=NULL,		L"in Load(...) Fonts must be set");

	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in Load(...) can't open file");

	TiXmlHandle Handle(&Doc);

	TiXmlElement* ElemData = Handle.FirstChildElement().Element();
	Exception(ElemData!=NULL,L"in Load(...) teg Data must be set");

	for(TiXmlElement* ElemList = ElemData->FirstChildElement(); ElemList != NULL;
		ElemList = ElemList->NextSiblingElement())
	{
		ListFromXmlElement(ElemList);
	}
	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlTextListManagerLoader::SetTexts(const pTTextListManager &Val)
{
	Texts = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlTextListManagerLoader::SetFonts(const pTFontList &Val)
{
	Fonts = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlTextListManagerLoader::Set(const pTTextListManager &Tl,const pTFontList &F)
{
	Texts = Tl;
	Fonts = F;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
