///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITELISTCOLORITERATORDECODER_H_INCLUDED
#define XMLSPRITELISTCOLORITERATORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpriteListColorIteratorDecoder:public TXmlBaseEventActionDecoder
{
	protected:
		pTSpriteListManager Sprites;
	public:
		TXmlSpriteListColorIteratorDecoder();
		virtual ~TXmlSpriteListColorIteratorDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetSprites(const pTSpriteListManager &Val);
};

typedef TXmlSpriteListColorIteratorDecoder* pTXmlSpriteListColorIteratorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSPRITELISTCOLORITERATORDECODER_H_INCLUDED
