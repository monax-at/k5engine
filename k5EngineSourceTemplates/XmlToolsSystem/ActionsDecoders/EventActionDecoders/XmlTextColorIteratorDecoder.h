///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLTEXTCOLORITERATORDECODER_H_INCLUDED
#define XMLTEXTCOLORITERATORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlTextColorIteratorDecoder:public TXmlBaseEventActionDecoder
{
	protected:
	public:
		TXmlTextColorIteratorDecoder();
		virtual ~TXmlTextColorIteratorDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetTexts(const pTTextListManager &Val);
};

typedef TXmlTextColorIteratorDecoder* pTXmlTextColorIteratorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLTEXTCOLORITERATORDECODER_H_INCLUDED
