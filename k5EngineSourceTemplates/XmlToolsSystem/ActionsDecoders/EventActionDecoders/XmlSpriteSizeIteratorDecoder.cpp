#include "XmlSpriteSizeIteratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteSizeIteratorDecoder::TXmlSpriteSizeIteratorDecoder():
												TXmlBaseEventActionDecoder()
{
	Exception.SetPrefix(L"TXmlSpriteSizeIteratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlSpriteSizeIteratorDecoder: ");

	ActionTypeName = L"SpriteSizeIterator";
}
///--------------------------------------------------------------------------------------//
TXmlSpriteSizeIteratorDecoder::~TXmlSpriteSizeIteratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlSpriteSizeIteratorDecoder::Construct()
{
	return new TEventActionSpriteSizeIterator;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteSizeIteratorDecoder::Init(const pTBaseEventAction &Action,
										TiXmlElement* Elem)
{
	pTEventActionSpriteSizeIterator Obj
						(static_cast<pTEventActionSpriteSizeIterator>(Action));

	if(Decoder.IsAttrExist(Elem,L"delay")){
		Obj->SetDelay(Decoder.AttrToFloat(Elem,L"delay"));
	}

	Obj->SetDirection(Decoder.AttrToChangeValueDirection(Elem,L"direction"));

	if(Decoder.IsAttrExist(Elem,L"delay")){
		Obj->SetDelay(Decoder.AttrToFloat(Elem,L"delay"));
	}

	Obj->SetWorkTime(Decoder.AttrToFloat(Elem,L"time"));

	Obj->SetChangeSizeValue(	Decoder.AttrToFloat(Elem,L"width"),
								Decoder.AttrToFloat(Elem,L"height"));

	Obj->Set(Decoder.ToSprite(Decoder.GetElem(Elem,L"Sprite")));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteSizeIteratorDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
