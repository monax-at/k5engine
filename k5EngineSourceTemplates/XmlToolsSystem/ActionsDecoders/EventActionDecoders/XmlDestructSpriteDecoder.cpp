#include "XmlDestructSpriteDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlDestructSpriteDecoder::TXmlDestructSpriteDecoder():
							TXmlBaseEventActionDecoder(),RenderQueue(NULL)
{
	Exception.SetPrefix(L"TXmlDestructSpriteDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlDestructSpriteDecoder: ");

	ActionTypeName = L"DestructSprite";
}
///--------------------------------------------------------------------------------------//
TXmlDestructSpriteDecoder::~TXmlDestructSpriteDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlDestructSpriteDecoder::Construct()
{
	return new TEventActionDestructSprite;
}
///--------------------------------------------------------------------------------------//
void TXmlDestructSpriteDecoder::Init(const pTBaseEventAction &Action,
									 TiXmlElement* Elem)
{
	Exception(RenderQueue!=NULL,L"in Init(...) RenderQueue is NULL");

	pTEventActionDestructSprite Obj
						(static_cast<pTEventActionDestructSprite>(Action));

	Obj->SetRenderQueue(RenderQueue);
	Obj->SetSpriteBanks(Decoder.GetSpriteBanks());
	Obj->Set(Decoder.ToSprite(Decoder.GetElem(Elem,L"Sprite")));

	if(Decoder.IsAttrExist(Elem,L"size")){
		Obj->SetCellSize(Decoder.AttrToInt(Elem,L"size"));
	}

	TXmlKinematicsForcesDecoder ForceDecoder;

	TiXmlElement* ElemInitiators(Decoder.GetElem(Elem,L"Initiators"));
	TiXmlElement* ElemForces(Decoder.GetElem(Elem,L"Forces"));

	if(ElemInitiators!=NULL){
		for(TiXmlElement* Initiator = ElemInitiators->FirstChildElement();
			Initiator != NULL; Initiator = Initiator->NextSiblingElement())
		{
			Obj->AddInitiator(ForceDecoder(Initiator));
		}
	}

	if(ElemForces!=NULL){
		for(TiXmlElement* Force = ElemForces->FirstChildElement();
			Force != NULL; Force = Force->NextSiblingElement())
		{
			Obj->AddForce(ForceDecoder(Force));
		}
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlDestructSpriteDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlDestructSpriteDecoder::SetSpriteBanks(const pTSpriteBankListManager &Val)
{
	Decoder.SetSpriteBanks(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlDestructSpriteDecoder::SetRenderQueue(const pTRenderQueue &Val)
{
	RenderQueue = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
