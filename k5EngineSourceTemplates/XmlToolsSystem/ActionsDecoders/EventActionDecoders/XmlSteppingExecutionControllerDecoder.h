///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSTEPPINGEXECUTIONCONTROLLERDECODER_H_INCLUDED
#define XMLSTEPPINGEXECUTIONCONTROLLERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlEventActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSteppingExecutionControllerDecoder:public TXmlBaseEventActionDecoder
{
	protected:
		pTXmlEventActionsDecoder EventActionsDecoder;
	public:
		TXmlSteppingExecutionControllerDecoder();
		virtual ~TXmlSteppingExecutionControllerDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetEventActionsDecoder(const pTXmlEventActionsDecoder &Val);
};

typedef TXmlSteppingExecutionControllerDecoder*
						pTXmlSteppingExecutionControllerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSTEPPINGEXECUTIONCONTROLLERDECODER_H_INCLUDED
