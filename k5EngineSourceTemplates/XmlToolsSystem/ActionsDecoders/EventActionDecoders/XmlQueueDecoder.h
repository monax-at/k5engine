///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLQUEUEDECODER_H_INCLUDED
#define XMLQUEUEDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlActionsDecoder.h"
#include "../../XmlEventActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlQueueDecoder:public TXmlBaseEventActionDecoder
{
	protected:
		pTXmlActionsDecoder 	 ActionsDecoder;
		pTXmlEventActionsDecoder EventActionsDecoder;
	public:
		TXmlQueueDecoder();
		virtual ~TXmlQueueDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetActionsDecoder(const pTXmlActionsDecoder &Val);
		void SetEventActionsDecoder(const pTXmlEventActionsDecoder &Val);
};

typedef TXmlQueueDecoder* pTXmlQueueDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLQUEUEDECODER_H_INCLUDED
