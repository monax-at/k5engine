///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLEVENTACTIONATTACHSPRITETOMOUSEDECODER_H_INCLUDED
#define XMLEVENTACTIONATTACHSPRITETOMOUSEDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlAttachSpriteToMouseDecoder:public TXmlBaseEventActionDecoder
{
	protected:
	public:
		TXmlAttachSpriteToMouseDecoder();
		virtual ~TXmlAttachSpriteToMouseDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetSprites(const pTSpriteListManager &Sprites);
};

typedef TXmlAttachSpriteToMouseDecoder* pTXmlAttachSpriteToMouseDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLEVENTACTIONATTACHSPRITETOMOUSEDECODER_H_INCLUDED
