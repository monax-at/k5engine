#include "XmlSpriteListColorIteratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteListColorIteratorDecoder::TXmlSpriteListColorIteratorDecoder():
								TXmlBaseEventActionDecoder(),Sprites(NULL)
{
	Exception.SetPrefix(L"TXmlSpriteListColorIteratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlSpriteListColorIteratorDecoder: ");

	ActionTypeName = L"SpriteListColorIterator";
}
///--------------------------------------------------------------------------------------//
TXmlSpriteListColorIteratorDecoder::~TXmlSpriteListColorIteratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlSpriteListColorIteratorDecoder::Construct()
{
	return new TEventActionSpriteListColorIterator;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListColorIteratorDecoder::Init(	const pTBaseEventAction &Action,
												TiXmlElement* Elem)
{
	Exception(Sprites!=NULL,L"in Init(...) Sprites is NULL");

	pTEventActionSpriteListColorIterator Obj
					(static_cast<pTEventActionSpriteListColorIterator>(Action));

	Obj->SetDirection(Decoder.AttrToChangeValueDirection(Elem,L"direction"));

	if(Decoder.IsAttrExist(Elem,L"delay")){
		Obj->SetDelay(Decoder.AttrToFloat(Elem,L"delay"));
	}

	Obj->SetWorkTime	(Decoder.AttrToFloat(Elem,L"time"));
	Obj->SetColorValue	(Decoder.ToColor(Decoder.GetElem(Elem, L"Color")));
	Obj->SetStartColor	(Decoder.ToColor(Decoder.GetElem(Elem, L"StartColor")));
	Obj->Set			(Sprites->Get(Decoder.AttrToWStr(Elem, L"list")));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteListColorIteratorDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Sprites = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
