///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITEANGLEITERATORDECODER_H_INCLUDED
#define XMLSPRITEANGLEITERATORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpriteAngleIteratorDecoder:public TXmlBaseEventActionDecoder
{
	public:
		TXmlSpriteAngleIteratorDecoder();
		virtual ~TXmlSpriteAngleIteratorDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetSprites(const pTSpriteListManager &Val);
};

typedef TXmlSpriteAngleIteratorDecoder* pTXmlSpriteAngleIteratorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // TXMLCONTAINERDECODER_H_INCLUDED
