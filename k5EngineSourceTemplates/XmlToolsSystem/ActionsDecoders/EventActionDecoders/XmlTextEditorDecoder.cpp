#include "XmlTextEditorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlTextEditorDecoder::TXmlTextEditorDecoder(): TXmlBaseEventActionDecoder()
{
	Exception.SetPrefix(L"TXmlTextEditorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlTextEditorDecoder: ");

	ActionTypeName = L"TextEditor";
}
///--------------------------------------------------------------------------------------//
TXmlTextEditorDecoder::~TXmlTextEditorDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlTextEditorDecoder::Construct()
{
	return new TEventActionTextEditor;
}
///--------------------------------------------------------------------------------------//
void TXmlTextEditorDecoder::Init(const pTBaseEventAction &Action,
										TiXmlElement* Elem)
{
	pTEventActionTextEditor Obj(static_cast<pTEventActionTextEditor>(Action));

	Obj->Set(Decoder.ToText( Decoder.GetElem(Elem,L"Text") ));

	if(Decoder.IsAttrExist(Elem,L"max_width")){
		Obj->SetMaxWidth(Decoder.AttrToFloat(Elem,L"max_width"));
	}

	if(Decoder.IsAttrExist(Elem,L"max_length")){
		Obj->SetMaxStrLength(Decoder.AttrToInt(Elem,L"max_length"));
	}

    if(Decoder.IsTegExist(Elem, L"CursorText" )){
        Obj->SetCursorText(Decoder.ToText( Decoder.GetElem(Elem,L"CursorText") ));
    }

	if(Decoder.IsAttrExist(Elem,L"cursor_blink_time")){
		Obj->SetCursorBlinkTime(Decoder.AttrToFloat(Elem,L"cursor_blink_time"));
	}



	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlTextEditorDecoder::SetTexts(const pTTextListManager &Val)
{
	Decoder.SetTexts(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
