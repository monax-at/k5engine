#include "XmlSpriteAngleIteratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteAngleIteratorDecoder::TXmlSpriteAngleIteratorDecoder():
												TXmlBaseEventActionDecoder()
{
	Exception.SetPrefix(L"TXmlSpriteAngleIteratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlSpriteAngleIteratorDecoder: ");

	ActionTypeName = L"SpriteAngleIterator";
}
///--------------------------------------------------------------------------------------//
TXmlSpriteAngleIteratorDecoder::~TXmlSpriteAngleIteratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlSpriteAngleIteratorDecoder::Construct()
{
	//return new TEventActionSpriteAngleIterator;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteAngleIteratorDecoder::Init(const pTBaseEventAction &Action,
										TiXmlElement* Elem)
{
	/*pTEventActionSpriteAngleIterator Obj
						(static_cast<pTEventActionSpriteAngleIterator>(Action));

	if(Decoder.IsAttrExist(Elem,L"delay")){
		Obj->SetDelay(Decoder.AttrToFloat(Elem,L"delay"));
	}

	if(Decoder.IsAttrExist(Elem,L"cyclic")){
		Obj->SetCyclic(Decoder.AttrToBool(Elem,L"cyclic"));
	}

	Obj->SetDirection(Decoder.AttrToChangeValueDirection(Elem,L"direction"));

	Obj->SetWorkTime(Decoder.AttrToFloat(Elem,L"time"));
	Obj->SetAngle	(Decoder.AttrToFloat(Elem,L"angle"));
	Obj->Set(Decoder.ToSprite(Decoder.GetElem(Elem,L"Sprite")));

	SetDefaultParams(Obj,Elem);*/
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteAngleIteratorDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
