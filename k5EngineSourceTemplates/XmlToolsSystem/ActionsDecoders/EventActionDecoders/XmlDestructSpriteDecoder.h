///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLDESTRUCTSPRITEDECODER_H_INCLUDED
#define XMLDESTRUCTSPRITEDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlKinematicsForcesDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlDestructSpriteDecoder:public TXmlBaseEventActionDecoder
{
	protected:
		pTRenderQueue RenderQueue;
	public:
		TXmlDestructSpriteDecoder();
		virtual ~TXmlDestructSpriteDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetSprites(const pTSpriteListManager &Val);
		void SetSpriteBanks(const pTSpriteBankListManager &Val);
		void SetRenderQueue(const pTRenderQueue &Val);
};

typedef TXmlDestructSpriteDecoder* pTXmlDestructSpriteDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLDESTRUCTSPRITEDECODER_H_INCLUDED
