#include "XmlSpriteTextureAnimationDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteTextureAnimationDecoder::TXmlSpriteTextureAnimationDecoder():
												TXmlBaseEventActionDecoder()
{
	Exception.SetPrefix(L"TXmlSpriteTextureAnimationDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlSpriteTextureAnimationDecoder: ");

	ActionTypeName = L"SpriteTextureAnimation";
}
///--------------------------------------------------------------------------------------//
TXmlSpriteTextureAnimationDecoder::~TXmlSpriteTextureAnimationDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlSpriteTextureAnimationDecoder::Construct()
{
	//return new TEventActionSpriteTextureAnimation;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteTextureAnimationDecoder::Init(const pTBaseEventAction &Action,
											 TiXmlElement* Elem)
{
/*	pTEventActionSpriteTextureAnimation Obj
					(static_cast<pTEventActionSpriteTextureAnimation>(Action));

	Obj->Set(Decoder.ToSprite(Decoder.GetElem(Elem,L"Sprite")));

	TiXmlElement* ElemRects (Decoder.GetElem(Elem,L"Rects"));
	TiXmlElement* ElemFrames(Decoder.GetElem(Elem,L"Frames"));

	Exception(ElemRects !=NULL ,L"in Init(...) ElemRects is NULL");
	Exception(ElemFrames!=NULL,L"in Init(...) ElemFrames is NULL");

	for(TiXmlElement* ElemRect = ElemRects->FirstChildElement();
		ElemRect != NULL; ElemRect = ElemRect->NextSiblingElement())
	{
		float X(Decoder.AttrToFloat(ElemRect,L"x"));
		float Y(Decoder.AttrToFloat(ElemRect,L"y"));
		float Width(Decoder.AttrToFloat(ElemRect,L"width"));
		float Height(Decoder.AttrToFloat(ElemRect,L"height"));

		Obj->AddRect(X,Y,Width,Height);
	}

	for(TiXmlElement* ElemFrame = ElemFrames->FirstChildElement();
		ElemFrame != NULL; ElemFrame = ElemFrame->NextSiblingElement())
	{
		Obj->AddFrameId(Decoder.AttrToInt(ElemFrame,L"id"));
	}

	if(Decoder.IsAttrExist(Elem,L"cyclic")){
		Obj->SetCyclic(Decoder.AttrToBool(Elem,L"cyclic"));
	}

	if(Decoder.IsAttrExist(Elem,L"frame_time")){
		Obj->SetFrameTime(Decoder.AttrToFloat(Elem,L"frame_time"));
	}

	if(Decoder.IsAttrExist(Elem,L"frame")){
		Obj->SetFrame(Decoder.AttrToInt(Elem,L"frame"));
	}

	SetDefaultParams(Obj,Elem);*/
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteTextureAnimationDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
