///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLEVENTREACTIONDECODER_H_INCLUDED
#define XMLEVENTREACTIONDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlEventReactionDecoder:public TXmlBaseEventActionDecoder
{
	protected:
		pTXmlActionsDecoder ActionsDecoder;
	public:
		TXmlEventReactionDecoder();
		virtual ~TXmlEventReactionDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetActionsDecoder(const pTXmlActionsDecoder &Val);
};

typedef TXmlEventReactionDecoder* pTXmlEventReactionDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // TXMLCONTAINERDECODER_H_INCLUDED
