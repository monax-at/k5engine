///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITETEXTUREANIMATIONDECODER_H_INCLUDED
#define XMLSPRITETEXTUREANIMATIONDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpriteTextureAnimationDecoder:public TXmlBaseEventActionDecoder
{
	public:
		TXmlSpriteTextureAnimationDecoder();
		virtual ~TXmlSpriteTextureAnimationDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetSprites(const pTSpriteListManager &Val);
};

typedef TXmlSpriteTextureAnimationDecoder* pTXmlSpriteTextureAnimationDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSPRITETEXTUREANIMATIONDECODER_H_INCLUDED
