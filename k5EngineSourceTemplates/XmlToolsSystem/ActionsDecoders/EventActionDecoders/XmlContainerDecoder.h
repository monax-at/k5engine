///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef TXMLCONTAINERDECODER_H_INCLUDED
#define TXMLCONTAINERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlActionsDecoder.h"
#include "../../XmlEventActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlContainerDecoder:public TXmlBaseEventActionDecoder
{
	protected:
		pTXmlActionsDecoder 	 ActionsDecoder;
		pTXmlEventActionsDecoder EventActionsDecoder;
	public:
		TXmlContainerDecoder();
		virtual ~TXmlContainerDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetActionsDecoder(const pTXmlActionsDecoder &Val);
		void SetEventActionsDecoder(const pTXmlEventActionsDecoder &Val);
};

typedef TXmlContainerDecoder* pTXmlContainerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // TXMLCONTAINERDECODER_H_INCLUDED
