#include "XmlPosIteratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlPosIteratorDecoder::TXmlPosIteratorDecoder():
												TXmlBaseEventActionDecoder()
{
	Exception.SetPrefix(L"TXmlPosIteratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlPosIteratorDecoder: ");


	ActionTypeName = L"PosIterator";
}
///--------------------------------------------------------------------------------------//
TXmlPosIteratorDecoder::~TXmlPosIteratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlPosIteratorDecoder::Construct()
{
	return new TEventActionPosIterator;
}
///--------------------------------------------------------------------------------------//
void TXmlPosIteratorDecoder::Init(const pTBaseEventAction &Action,
										TiXmlElement* Elem)
{
	pTEventActionPosIterator Obj
							(static_cast<pTEventActionPosIterator>(Action));

	Obj->SetDirection(Decoder.AttrToChangeValueDirection(Elem,L"direction"));

	if(Decoder.IsAttrExist(Elem,L"delay")){
		Obj->SetDelay(Decoder.AttrToFloat(Elem,L"delay"));
	}

    if(Decoder.IsTegExist(Elem,L"Sprite"))
        Obj->Set(&Decoder.ToSprite(Decoder.GetElem(Elem,L"Sprite"))->Pos);
    else
        if(Decoder.IsTegExist(Elem,L"SpriteBank"))
            Obj->Set(&Decoder.ToGraphicBank(Decoder.GetElem(Elem,L"SpriteBank"))->Pos);
        else
            if(Decoder.IsTegExist(Elem,L"Text"))
                Obj->Set(&Decoder.ToText(Decoder.GetElem(Elem,L"Text"))->Pos);
            else Exception(L"TXmlPosIteratorDecoder: <" + ActionTypeName + L">" + L"Teg Sprite,SpriteBank or Text not exist");

	Obj->SetWorkTime(Decoder.AttrToFloat(Elem,L"time"));
	Obj->SetDistance(Decoder.ToPoint3D(Decoder.GetElem(Elem,L"Distance")));

    if(Decoder.IsTegExist(Elem,L"RandShiftDistance")){
        Obj->SetRandShiftDistance(Decoder.ToPoint3D(Decoder.GetElem(Elem,L"RandShiftDistance")));
    }

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlPosIteratorDecoder::SetTexts(const pTTextListManager &Val)
{
    Decoder.SetTexts(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlPosIteratorDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlPosIteratorDecoder::SetSpriteBanks(const pTGraphicBankListManager &Val)
{
    Decoder.SetGraphicBanks(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
