///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLTEXTEDITORDECODER_H_INCLUDED
#define XMLTEXTEDITORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlTextEditorDecoder:public TXmlBaseEventActionDecoder
{
	protected:
	public:
		TXmlTextEditorDecoder();
		virtual ~TXmlTextEditorDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetTexts(const pTTextListManager &Val);
};

typedef TXmlTextEditorDecoder* pTXmlTextEditorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLTEXTCOLORITERATORDECODER_H_INCLUDED
