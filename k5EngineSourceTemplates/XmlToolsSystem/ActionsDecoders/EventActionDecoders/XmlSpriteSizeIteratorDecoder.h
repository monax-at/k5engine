///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITESIZEITERATORDECODER_H_INCLUDED
#define XMLSPRITESIZEITERATORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpriteSizeIteratorDecoder:public TXmlBaseEventActionDecoder
{
	public:
		TXmlSpriteSizeIteratorDecoder();
		virtual ~TXmlSpriteSizeIteratorDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetSprites(const pTSpriteListManager &Val);
};

typedef TXmlSpriteSizeIteratorDecoder* pTXmlSpriteSizeIteratorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSPRITESIZEITERATORDECODER_H_INCLUDED
