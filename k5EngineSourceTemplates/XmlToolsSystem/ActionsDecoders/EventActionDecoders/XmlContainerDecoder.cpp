#include "XmlContainerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlContainerDecoder::TXmlContainerDecoder():TXmlBaseEventActionDecoder(),
									ActionsDecoder(NULL),EventActionsDecoder(NULL)
{
	Exception.SetPrefix(L"TXmlContainerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlContainerDecoder: ");

	ActionTypeName = L"Container";
}
///--------------------------------------------------------------------------------------//
TXmlContainerDecoder::~TXmlContainerDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlContainerDecoder::Construct()
{
	return new TEventActionContainer;
}
///--------------------------------------------------------------------------------------//
void TXmlContainerDecoder::Init(const pTBaseEventAction &Action,TiXmlElement* Elem)
{
	Exception(ActionsDecoder	 !=NULL, L"int Init(...) ActionsDecoder is NULL");
	Exception(EventActionsDecoder!=NULL, L"int Init(...) EventActionsDecoder is NULL");

	pTEventActionContainer Obj(static_cast<pTEventActionContainer>(Action));

	TiXmlElement* ElemEventActions = Decoder.GetElem(Elem,L"EventActions");
	Exception(ElemEventActions!=NULL,L"in InitContainer(...) ElemEventActions is NULL");

	EventActionsDecoder->CreateList	(Obj->GetActionsPtr(),ElemEventActions);
	EventActionsDecoder->InitList	(Obj->GetActionsPtr(),ElemEventActions);

	TiXmlElement* ElemStartActions	(Decoder.GetElem(Elem,L"StartActions"));

	ActionsDecoder->CreateList	(Obj->GetStartActionsPtr(),ElemStartActions);
	ActionsDecoder->InitList	(Obj->GetStartActionsPtr(),ElemStartActions);

	TiXmlElement* ElemStopActions	(Decoder.GetElem(Elem,L"StopActions"));

	ActionsDecoder->CreateList	(Obj->GetStopActionsPtr() ,ElemStopActions);
	ActionsDecoder->InitList	(Obj->GetStopActionsPtr() ,ElemStopActions);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlContainerDecoder::SetActionsDecoder(const pTXmlActionsDecoder &Val)
{
	ActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlContainerDecoder::SetEventActionsDecoder
										(const pTXmlEventActionsDecoder &Val)
{
	EventActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
