///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
/// Аторы:
///		Колесников Максим [mpkmaximus@gmail.com][http://mpkmaximus.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITEPOSITERATORDECODER_H_INCLUDED
#define XMLSPRITEPOSITERATORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlPosIteratorDecoder:public TXmlBaseEventActionDecoder
{
	public:

		TXmlPosIteratorDecoder();
		virtual ~TXmlPosIteratorDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetTexts(const pTTextListManager &Val);
		void SetSprites(const pTSpriteListManager &Val);
		void SetSpriteBanks(const pTGraphicBankListManager &Val);
};

typedef TXmlPosIteratorDecoder* pTXmlPosIteratorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // TXMLCONTAINERDECODER_H_INCLUDED
