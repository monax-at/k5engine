#include "XmlEffectTimerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlEffectTimerDecoder::TXmlEffectTimerDecoder():TXmlBaseEventActionDecoder(),
													ActionsDecoder(NULL)
{
	Exception.SetPrefix(L"TXmlEffectTimerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlEffectTimerDecoder: ");

	ActionTypeName = L"EffectTimer";
}
///--------------------------------------------------------------------------------------//
TXmlEffectTimerDecoder::~TXmlEffectTimerDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlEffectTimerDecoder::Construct()
{
	return new TEventActionEffectTimer;
}
///--------------------------------------------------------------------------------------//
void TXmlEffectTimerDecoder::Init(const pTBaseEventAction &Action,
								  TiXmlElement* Elem)
{
	Exception(ActionsDecoder !=NULL, L"int Init(...) ActionsDecoder is NULL");

	pTEventActionEffectTimer Obj(static_cast<pTEventActionEffectTimer>(Action));

	Obj->SetActivationTime(Decoder.AttrToFloat(Elem,L"timer"));

	if(Decoder.IsAttrExist(Elem,L"cyclic")){
		Obj->SetCyclicWork(Decoder.AttrToBool(Elem,L"cyclic"));
	}

	if(Decoder.IsAttrExist(Elem,L"work_count")){
		Obj->SetWorkCount(Decoder.AttrToInt(Elem,L"work_count"));
	}

	TiXmlElement* ElemActions(Decoder.GetElem(Elem,L"Actions"));
	ActionsDecoder->CreateList(Obj->GetActionsPtr(),ElemActions);
	ActionsDecoder->InitList  (Obj->GetActionsPtr(),ElemActions);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlEffectTimerDecoder::SetActionsDecoder(const pTXmlActionsDecoder &Val)
{
	ActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
