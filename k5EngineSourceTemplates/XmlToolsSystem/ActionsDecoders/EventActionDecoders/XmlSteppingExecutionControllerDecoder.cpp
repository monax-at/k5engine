#include "XmlSteppingExecutionControllerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSteppingExecutionControllerDecoder::TXmlSteppingExecutionControllerDecoder():
							TXmlBaseEventActionDecoder(),EventActionsDecoder(NULL)
{
	Exception.SetPrefix(L"TXmlSteppingExecutionControllerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlSteppingExecutionControllerDecoder: ");

	ActionTypeName = L"SteppingExecutionController";
}
///--------------------------------------------------------------------------------------//
TXmlSteppingExecutionControllerDecoder::~TXmlSteppingExecutionControllerDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlSteppingExecutionControllerDecoder::Construct()
{
	return new TEventActionSteppingExecutionController;
}
///--------------------------------------------------------------------------------------//
void TXmlSteppingExecutionControllerDecoder::Init(const pTBaseEventAction &Action,
												 TiXmlElement* Elem)
{
	Exception(	EventActionsDecoder!=NULL,
				L"int Init(...) EventActionsDecoder is NULL");

	pTEventActionSteppingExecutionController Obj
					(static_cast<pTEventActionSteppingExecutionController>(Action));

	if(Decoder.IsAttrExist(Elem,L"autoupdate_cursor")){
		Obj->SetAutoUpdateCursor(Decoder.AttrToBool(Elem,L"autoupdate_cursor"));
	}

	TiXmlElement* ElemEventActions(Decoder.GetElem(Elem,L"EventActions"));
	Exception(ElemEventActions!=NULL,L"in Init(...) ElemEventActions is NULL");

	EventActionsDecoder->CreateList	(Obj->GetActionsPtr(),ElemEventActions);
	EventActionsDecoder->InitList	(Obj->GetActionsPtr(),ElemEventActions);

	Obj->Stop();

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlSteppingExecutionControllerDecoder::SetEventActionsDecoder
											(const pTXmlEventActionsDecoder &Val)
{
	EventActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
