#include "XmlSpriteColorIteratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteColorIteratorDecoder::TXmlSpriteColorIteratorDecoder():
								TXmlBaseEventActionDecoder()
{
	Exception.SetPrefix(L"TXmlSpriteColorIteratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlSpriteColorIteratorDecoder: ");

	ActionTypeName = L"SpriteColorIterator";
}
///--------------------------------------------------------------------------------------//
TXmlSpriteColorIteratorDecoder::~TXmlSpriteColorIteratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlSpriteColorIteratorDecoder::Construct()
{
	return new TEventActionSpriteColorIterator;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteColorIteratorDecoder::Init(	const pTBaseEventAction &Action,
											TiXmlElement* Elem)
{
	pTEventActionSpriteColorIterator Obj
						= static_cast<pTEventActionSpriteColorIterator>(Action);

	Obj->SetDirection(Decoder.AttrToChangeValueDirection(Elem,L"direction"));

	if(Decoder.IsAttrExist(Elem,L"delay")){
		Obj->SetDelay(Decoder.AttrToFloat(Elem,L"delay"));
	}

	Obj->SetWorkTime(Decoder.AttrToFloat(Elem,L"time"));
	Obj->SetColorValue	(Decoder.ToColor ( Decoder.GetElem(Elem,L"Color")  ));
	Obj->Set			(Decoder.ToSprite( Decoder.GetElem(Elem,L"Sprite") ));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteColorIteratorDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
