#include "XmlAttachSpriteToMouseDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlAttachSpriteToMouseDecoder::TXmlAttachSpriteToMouseDecoder():
								TXmlBaseEventActionDecoder()
{
	Exception.SetPrefix(L"TXmlAttachSpriteToMouseDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlAttachSpriteToMouseDecoder: ");

	ActionTypeName = L"AttachSpriteToMouse";
}
///--------------------------------------------------------------------------------------//
TXmlAttachSpriteToMouseDecoder::~TXmlAttachSpriteToMouseDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlAttachSpriteToMouseDecoder::Construct()
{
	return new TEventActionAttachSpriteToMouse;
}
///--------------------------------------------------------------------------------------//
void TXmlAttachSpriteToMouseDecoder::Init(	const pTBaseEventAction &Action,
											TiXmlElement* Elem)
{
	pTEventActionAttachSpriteToMouse Obj =
						static_cast<pTEventActionAttachSpriteToMouse>(Action);

	Obj->Set(Decoder.ToSprite( Decoder.GetElem(Elem,L"Sprite") ));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlAttachSpriteToMouseDecoder::SetSprites(const pTSpriteListManager &Sprites)
{
	Decoder.SetSprites(Sprites);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
