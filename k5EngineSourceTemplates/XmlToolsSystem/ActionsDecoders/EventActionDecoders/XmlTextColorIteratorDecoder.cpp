#include "XmlTextColorIteratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlTextColorIteratorDecoder::TXmlTextColorIteratorDecoder():
								TXmlBaseEventActionDecoder()
{
	Exception.SetPrefix(L"TXmlTextColorIteratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlTextColorIteratorDecoder: ");

	ActionTypeName = L"TextColorIterator";
}
///--------------------------------------------------------------------------------------//
TXmlTextColorIteratorDecoder::~TXmlTextColorIteratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlTextColorIteratorDecoder::Construct()
{
	//return new TEventActionTextColorIterator;
}
///--------------------------------------------------------------------------------------//
void TXmlTextColorIteratorDecoder::Init(const pTBaseEventAction &Action,
										TiXmlElement* Elem)
{
/*	pTEventActionTextColorIterator Obj
						= static_cast<pTEventActionTextColorIterator>(Action);

	Obj->SetDirection(Decoder.AttrToChangeValueDirection(Elem,L"direction"));

	if(Decoder.IsAttrExist(Elem,L"delay")){
		Obj->SetDelay(Decoder.AttrToFloat(Elem,L"delay"));
	}

	Obj->SetWorkTime	(Decoder.AttrToFloat(Elem,L"time"));
	Obj->SetColorValue	(Decoder.ToColor( Decoder.GetElem(Elem,L"Color") ));

	Obj->Set(Decoder.ToText( Decoder.GetElem(Elem,L"Text") ));

	SetDefaultParams(Obj,Elem);*/
}
///--------------------------------------------------------------------------------------//
void TXmlTextColorIteratorDecoder::SetTexts(const pTTextListManager &Val)
{
	Decoder.SetTexts(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
