#include "XmlDragSpriteToPosDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlDragSpriteToPosDecoder::TXmlDragSpriteToPosDecoder():
							TXmlBaseEventActionDecoder(),ActionsDecoder(NULL)
{
	Exception.SetPrefix(L"TXmlDragSpriteToPosDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlDragSpriteToPosDecoder: ");

	ActionTypeName = L"DragSpriteToPos";
}
///--------------------------------------------------------------------------------------//
TXmlDragSpriteToPosDecoder::~TXmlDragSpriteToPosDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlDragSpriteToPosDecoder::Construct()
{
	return new TEventActionDragSpriteToPos;
}
///--------------------------------------------------------------------------------------//
void TXmlDragSpriteToPosDecoder::Init(	const pTBaseEventAction &Action,
										TiXmlElement* Elem)
{
	Exception(ActionsDecoder !=NULL, L"int Init(...) ActionsDecoder is NULL");


	pTEventActionDragSpriteToPos Obj
							(static_cast<pTEventActionDragSpriteToPos>(Action));

	Obj->Set(Decoder.ToSprite(Decoder.GetElem(Elem,L"Sprite")));
	Obj->SetFinishPos(Decoder.ToPoint3D(Decoder.GetElem(Elem,L"FinishPos")));

	if(Decoder.IsAttrExist(Elem,L"distance")){
		Obj->SetCheckDistance(Decoder.AttrToFloat(Elem,L"distance"));
	}

	Obj->SetDevice(Decoder.GetSprites()->GetDevice());

	TiXmlElement* ElemStartDragActions	(Decoder.GetElem(Elem,L"StartDragActions"));
	TiXmlElement* ElemCancelDragActions	(Decoder.GetElem(Elem,L"CancelDragActions"));
	TiXmlElement* ElemFinishDragActions	(Decoder.GetElem(Elem,L"FinishDragActions"));

	pTActionList StartDragActions (Obj->GetStartDragActionsPtr());
	pTActionList CancelDragActions(Obj->GetCancelDragActionsPtr());
	pTActionList FinishDragActions(Obj->GetFinishDragActionsPtr());

	ActionsDecoder->CreateList	(StartDragActions,ElemStartDragActions);
	ActionsDecoder->CreateList	(CancelDragActions,ElemCancelDragActions);
	ActionsDecoder->CreateList	(FinishDragActions,ElemFinishDragActions);

	ActionsDecoder->InitList(StartDragActions,ElemStartDragActions);
	ActionsDecoder->InitList(CancelDragActions,ElemCancelDragActions);
	ActionsDecoder->InitList(FinishDragActions,ElemFinishDragActions);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlDragSpriteToPosDecoder::SetActionsDecoder(const pTXmlActionsDecoder &Val)
{
	ActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlDragSpriteToPosDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
