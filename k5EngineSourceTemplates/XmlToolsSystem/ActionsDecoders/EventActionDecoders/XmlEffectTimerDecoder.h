///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLEFFECTTIMERDECODER_H_INCLUDED
#define XMLEFFECTTIMERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlEffectTimerDecoder:public TXmlBaseEventActionDecoder
{
	protected:
		pTXmlActionsDecoder ActionsDecoder;
	public:
		TXmlEffectTimerDecoder();
		virtual ~TXmlEffectTimerDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action,TiXmlElement* Elem);

		void SetActionsDecoder(const pTXmlActionsDecoder &Val);
};

typedef TXmlEffectTimerDecoder* pTXmlEffectTimerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // TXMLCONTAINERDECODER_H_INCLUDED
