#include "XmlQueueDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlQueueDecoder::TXmlQueueDecoder():TXmlBaseEventActionDecoder(),
									ActionsDecoder(NULL),EventActionsDecoder(NULL)
{
	Exception.SetPrefix(L"TXmlQueueDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlQueueDecoder: ");

	ActionTypeName = L"Queue";
}
///--------------------------------------------------------------------------------------//
TXmlQueueDecoder::~TXmlQueueDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlQueueDecoder::Construct()
{
	return new TEventActionQueue;
}
///--------------------------------------------------------------------------------------//
void TXmlQueueDecoder::Init(const pTBaseEventAction &Action,TiXmlElement* Elem)
{
	Exception(ActionsDecoder	 !=NULL, L"int Init(...) ActionsDecoder is NULL");
	Exception(EventActionsDecoder!=NULL, L"int Init(...) EventActionsDecoder is NULL");

	pTEventActionQueue Obj(static_cast<pTEventActionQueue>(Action));

	TiXmlElement* ElemEventActions = Decoder.GetElem(Elem,L"EventActions");
	Exception(ElemEventActions!=NULL,L"in Init(...) ElemEventActions is NULL");

	EventActionsDecoder->CreateList	(Obj->GetActionsPtr(),ElemEventActions);
	EventActionsDecoder->InitList	(Obj->GetActionsPtr(),ElemEventActions);

	TiXmlElement* ElemStartActions	(Decoder.GetElem(Elem,L"StartActions"));

	ActionsDecoder->CreateList	(Obj->GetStartActionsPtr(),ElemStartActions);
	ActionsDecoder->InitList	(Obj->GetStartActionsPtr(),ElemStartActions);

	TiXmlElement* ElemStopActions	(Decoder.GetElem(Elem,L"StopActions"));

	ActionsDecoder->CreateList	(Obj->GetStopActionsPtr() ,ElemStopActions);
	ActionsDecoder->InitList	(Obj->GetStopActionsPtr() ,ElemStopActions);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlQueueDecoder::SetActionsDecoder(const pTXmlActionsDecoder &Val)
{
	ActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlQueueDecoder::SetEventActionsDecoder
										(const pTXmlEventActionsDecoder &Val)
{
	EventActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
