#include "XmlEventReactionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlEventReactionDecoder::TXmlEventReactionDecoder():TXmlBaseEventActionDecoder(),
													ActionsDecoder(NULL)
{
	Exception.SetPrefix(L"TXmlEventReactionDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlEventReactionDecoder: ");

	ActionTypeName = L"EventReaction";
}
///--------------------------------------------------------------------------------------//
TXmlEventReactionDecoder::~TXmlEventReactionDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlEventReactionDecoder::Construct()
{
	return new TEventActionEventReaction;
}
///--------------------------------------------------------------------------------------//
void TXmlEventReactionDecoder::Init(const pTBaseEventAction &Action,
									TiXmlElement* Elem)
{
	Exception(ActionsDecoder !=NULL, L"int Init(...) ActionsDecoder is NULL");

	pTEventActionEventReaction Obj
							(static_cast<pTEventActionEventReaction>(Action));

	wstring Type(L"user");
	wstring SubType(L"user");
	wstring Val(L"user");

	TiXmlElement* ElemEvent(Elem->FirstChildElement("Event"));
	if(ElemEvent!=NULL){
		if(Decoder.IsAttrExist(ElemEvent,L"type")){
			Type = Decoder.AttrToWStr(ElemEvent,L"type");
		}

		if(Decoder.IsAttrExist(ElemEvent,L"sub_type")){
			SubType = Decoder.AttrToWStr(ElemEvent,L"sub_type");
		}

		if(Decoder.IsAttrExist(ElemEvent,L"val")){
			Val = Decoder.AttrToWStr(ElemEvent,L"val");
		}
	}

	TEvent Event;
	if(Type == L"mouse")	{ Event.Type = EN_MOUSE; }
	if(Type == L"keyboard")	{ Event.Type = EN_KEYBOARD; }
	if(Type == L"timer")	{ Event.Type = EN_TIMER; }
	if(Type == L"system")	{ Event.Type = EN_SYSTEM; }
	if(Type == L"user")		{ Event.Type = EN_USER; }

	// значения для mouse
	if(Type == L"mouse"){
		if(SubType == L"motion")	{ Event.Mouse.Type = EN_MOUSEMOTION; }
		if(SubType == L"buttondown"){ Event.Mouse.Type = EN_MOUSEBUTTONDOWN; }
		if(SubType == L"buttonup")	{ Event.Mouse.Type = EN_MOUSEBUTTONUP; }
		if(SubType == L"user")		{ Event.Mouse.Type = EN_USER; }

		Event.Mouse.Button = EN_USER;
		if(Val == L"left")	{Event.Mouse.Button = EN_MOUSEBUTTONLEFT;}
		if(Val == L"right")	{Event.Mouse.Button = EN_MOUSEBUTTONRIGHT;}
	}

	if(Type == L"keyboard")	{ Event.Keyboard.Type = EN_USER; }
	if(Type == L"system")	{ Event.System.Type = EN_USER; }

	Obj->SetTemplate(Event);

	TiXmlElement* ElemActions(Decoder.GetElem(Elem,L"Actions"));
	pTActionList Actions(Obj->GetActionsPtr());

	ActionsDecoder->CreateList	(Actions, ElemActions);
	ActionsDecoder->InitList	(Actions, ElemActions);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlEventReactionDecoder::SetActionsDecoder(const pTXmlActionsDecoder &Val)
{
	ActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
