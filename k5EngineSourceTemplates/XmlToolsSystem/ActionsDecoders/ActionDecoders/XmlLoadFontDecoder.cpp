#include "XmlLoadFontDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlLoadFontDecoder::TXmlLoadFontDecoder():	TXmlBaseActionDecoder(),
											FontFaceList(NULL),FontList(NULL)
{
	Exception.SetPrefix(L"TXmlLoadFontDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlLoadFontDecoder: ");

	ActionTypeName = L"LoadFont";
}
///--------------------------------------------------------------------------------------//
TXmlLoadFontDecoder::~TXmlLoadFontDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlLoadFontDecoder::Construct()
{
	return new TActionLoadFont;
}
///--------------------------------------------------------------------------------------//
void TXmlLoadFontDecoder::Init(const pTBaseAction &Action,TiXmlElement* Elem)
{
	Exception(FontFaceList	!=NULL, L"in Init(...) FontFaceList is NULL");
	Exception(FontList		!=NULL, L"in Init(...) FontList is NULL");

	pTActionLoadFont Obj(static_cast<pTActionLoadFont>(Action));

	Obj->SetFontFaceList(FontFaceList);
	Obj->SetFontList(FontList);

	Obj->SetFontName(Decoder.AttrToWStr(Elem,L"font"));
	Obj->SetFilePath(Decoder.AttrToWStr(Elem,L"file"));

	for(TiXmlElement* ElemItem = Elem->FirstChildElement();
		ElemItem != NULL; ElemItem = ElemItem->NextSiblingElement())
	{
		Obj->AddSubFontInfo(Decoder.AttrToWStr(ElemItem,L"name"),
							Decoder.AttrToInt(ElemItem, L"size"));
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlLoadFontDecoder::SetFontFaceList(const pTFontFaceList &Val)
{
	FontFaceList = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlLoadFontDecoder::SetFontList(const pTFontList &Val)
{
	FontList = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
