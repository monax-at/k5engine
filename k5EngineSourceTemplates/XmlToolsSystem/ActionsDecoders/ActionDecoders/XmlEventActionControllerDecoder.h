///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLEVENTACTIONCONTROLLERDECODER_H_INCLUDED
#define XMLEVENTACTIONCONTROLLERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlEventActionControllerDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlEventActionControllerDecoder();
		virtual ~TXmlEventActionControllerDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetEventActions(const pTEventActionListManager &Val);
};

typedef TXmlEventActionControllerDecoder* pTXmlEventActionControllerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLEVENTACTIONCONTROLLERDECODER_H_INCLUDED
