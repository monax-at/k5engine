///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLACTIONCONTAINERDECODER_H_INCLUDED
#define XMLACTIONCONTAINERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlActionContainerDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTXmlActionsDecoder ActionsDecoder;
	public:
		TXmlActionContainerDecoder();
		virtual ~TXmlActionContainerDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetActionsDecoder(const pTXmlActionsDecoder &Val);
};

typedef TXmlActionContainerDecoder* pTXmlActionContainerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLACTIONLISTCONTROLLERDECODER_H_INCLUDED
