///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSTOPAPPLICATIONDECODER_H_INCLUDED
#define XMLSTOPAPPLICATIONDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "STActionSystem.h"
#include "Base/XmlBaseActionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlStopApplicationDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTBaseApplication App;
		pTBaseEventAction EventActionApp;
	public:
		TXmlStopApplicationDecoder();
		virtual ~TXmlStopApplicationDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void Set(const pTBaseApplication &Val);
		void Set(const pTBaseEventAction &Val);
};

typedef TXmlStopApplicationDecoder* pTXmlStopApplicationDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSTOPAPPLICATIONDECODER_H_INCLUDED
