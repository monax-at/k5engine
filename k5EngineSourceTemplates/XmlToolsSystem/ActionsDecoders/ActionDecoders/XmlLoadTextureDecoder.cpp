#include "XmlLoadTextureDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlLoadTextureDecoder::TXmlLoadTextureDecoder():	TXmlBaseActionDecoder(),
													Device(NULL),Textures(NULL)
{
	Exception.SetPrefix(L"TXmlLoadTextureDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlLoadTextureDecoder: ");

	ActionTypeName = L"LoadTexture";
}
///--------------------------------------------------------------------------------------//
TXmlLoadTextureDecoder::~TXmlLoadTextureDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlLoadTextureDecoder::Construct()
{
	return new TActionLoadTexture;
}
///--------------------------------------------------------------------------------------//
void TXmlLoadTextureDecoder::Init(const pTBaseAction &Action,TiXmlElement* Elem)
{
	Exception(Device!=NULL, 	L"in Init Device is NULL");
	Exception(Textures!=NULL, 	L"in Init Textures is NULL");

	pTActionLoadTexture Obj(static_cast<pTActionLoadTexture>(Action));

	Obj->SetDevice(Device);
	Obj->SetTextures(Textures);

	Obj->SetListName	(Decoder.AttrToWStr(Elem,L"list"));
	Obj->SetTextureName	(Decoder.AttrToWStr(Elem,L"texture"));
	Obj->SetFileName	(Decoder.AttrToWStr(Elem,L"file"));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlLoadTextureDecoder::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlLoadTextureDecoder::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
