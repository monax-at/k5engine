///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITESGENERATORDECODER_H_INCLUDED
#define XMLSPRITESGENERATORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "../../GraphicSystem/XmlSprite.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpritesGeneratorDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlSpritesGeneratorDecoder();
		virtual ~TXmlSpritesGeneratorDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetTextures(const pTTextureListManager &Val);
		void SetSprites(const pTSpriteListManager &Val);
};

typedef TXmlSpritesGeneratorDecoder* pTXmlSpritesGeneratorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSPRITESGENERATORDECODER_H_INCLUDED
