///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSORTRENDERQUEUEDECODER_H_INCLUDED
#define XMLSORTRENDERQUEUEDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSortRenderQueueDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTRenderQueue RenderQueue;
	public:
		TXmlSortRenderQueueDecoder();
		virtual ~TXmlSortRenderQueueDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetRenderQueue(const pTRenderQueue &Val);
};

typedef TXmlSortRenderQueueDecoder* pTXmlSortRenderQueueDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSORTRENDERQUEUEDECODER_H_INCLUDED
