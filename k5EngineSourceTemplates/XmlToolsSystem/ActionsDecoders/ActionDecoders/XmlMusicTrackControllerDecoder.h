///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLMUSICTRACKCONTROLLERDECODER_H_INCLUDED
#define XMLMUSICTRACKCONTROLLERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
//#include "STMusicSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlMusicTrackControllerDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTMusicTrackListManager MusicTracks;
	public:
		TXmlMusicTrackControllerDecoder();
		virtual ~TXmlMusicTrackControllerDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetMusicTracks(const pTMusicTrackListManager &Val);
};

typedef TXmlMusicTrackControllerDecoder* pTXmlMusicTrackControllerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLMUSICTRACKCONTROLLERDECODER_H_INCLUDED
