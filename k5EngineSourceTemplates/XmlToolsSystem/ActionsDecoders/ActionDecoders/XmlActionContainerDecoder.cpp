#include "XmlActionContainerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlActionContainerDecoder::TXmlActionContainerDecoder():	TXmlBaseActionDecoder(),
															ActionsDecoder(NULL)
{
	Exception.SetPrefix(L"TXmlActionContainerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlActionContainerDecoder: ");

	ActionTypeName = L"ActionContainer";
}
///--------------------------------------------------------------------------------------//
TXmlActionContainerDecoder::~TXmlActionContainerDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlActionContainerDecoder::Construct()
{
	return new TActionContainer;
}
///--------------------------------------------------------------------------------------//
void TXmlActionContainerDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	Exception(ActionsDecoder!=NULL, L"int Init(...) ActionsDecoder is NULL");

	pTActionContainer Obj(static_cast<pTActionContainer>(Action));

	TiXmlElement* ElemStartActions	(Decoder.GetElem(Elem,L"StartActions"));

	ActionsDecoder->CreateList	(Obj->GetStartActionsPtr(),ElemStartActions);
	ActionsDecoder->InitList	(Obj->GetStartActionsPtr(),ElemStartActions);

	TiXmlElement* ElemActions	(Decoder.GetElem(Elem,L"Actions"));

	ActionsDecoder->CreateList	(Obj->GetActionsPtr(),ElemActions);
	ActionsDecoder->InitList	(Obj->GetActionsPtr(),ElemActions);

	TiXmlElement* ElemStopActions	(Decoder.GetElem(Elem,L"StopActions"));

	ActionsDecoder->CreateList	(Obj->GetStopActionsPtr() ,ElemStopActions);
	ActionsDecoder->InitList	(Obj->GetStopActionsPtr() ,ElemStopActions);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlActionContainerDecoder::SetActionsDecoder(const pTXmlActionsDecoder &Val)
{
	ActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
