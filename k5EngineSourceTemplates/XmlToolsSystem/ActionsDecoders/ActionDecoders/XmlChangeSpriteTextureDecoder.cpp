#include "XmlChangeSpriteTextureDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlChangeSpriteTextureDecoder::TXmlChangeSpriteTextureDecoder():
								TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlChangeSpriteTextureDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlChangeSpriteTextureDecoder: ");

	ActionTypeName = L"ChangeSpriteTexture";
}
///--------------------------------------------------------------------------------------//
TXmlChangeSpriteTextureDecoder::~TXmlChangeSpriteTextureDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlChangeSpriteTextureDecoder::Construct()
{
	return new TActionChangeSpriteTexture;
}
///--------------------------------------------------------------------------------------//
void TXmlChangeSpriteTextureDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	pTActionChangeSpriteTexture Obj =
							static_cast<pTActionChangeSpriteTexture>(Action);

	Obj->Set(Decoder.ToSprite(Decoder.GetElem(Elem,L"Sprite")));

	TiXmlElement *ElemTexture(Decoder.GetElem(Elem,L"Texture"));
	if(	Decoder.IsAttrExist(ElemTexture,L"list") &&
		Decoder.IsAttrExist(ElemTexture,L"name"))
	{
		Obj->SetTexture(Decoder.ToTexture(ElemTexture));
	}

	TiXmlElement *ElemRect(Decoder.GetElem(ElemTexture,L"Rect"));
	if(ElemRect!=NULL){
		TPoint2D Pos(	Decoder.AttrToFloat(ElemRect,L"x"),
						Decoder.AttrToFloat(ElemRect,L"y"));

		TPoint2D Size(	Decoder.AttrToFloat(ElemRect,L"width"),
						Decoder.AttrToFloat(ElemRect,L"height"));

		Obj->SetRect(Pos,Size);
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlChangeSpriteTextureDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlChangeSpriteTextureDecoder::SetTextures(const pTTextureListManager &Val)
{
	Decoder.SetTextures(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
