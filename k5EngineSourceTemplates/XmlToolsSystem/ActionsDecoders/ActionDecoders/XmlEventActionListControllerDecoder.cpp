#include "XmlEventActionListControllerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlEventActionListControllerDecoder::TXmlEventActionListControllerDecoder():
								   TXmlBaseActionDecoder(),EventActions(NULL)
{
	Exception.SetPrefix(L"TXmlEventActionListControllerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlEventActionListControllerDecoder: ");

	ActionTypeName = L"EventActionListController";
}
///--------------------------------------------------------------------------------------//
TXmlEventActionListControllerDecoder::~TXmlEventActionListControllerDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlEventActionListControllerDecoder::Construct()
{
	return new TActionEventActionListController;
}
///--------------------------------------------------------------------------------------//
void TXmlEventActionListControllerDecoder::Init(const pTBaseAction &Action,
												TiXmlElement* Elem)
{
	Exception(EventActions!=NULL,L"in Init(...) EventActions is NULL");

	pTActionEventActionListController Obj =
						static_cast<pTActionEventActionListController>(Action);

	Obj->SetControlState(Decoder.AttrToActionControlState(Elem,L"state"));
	Obj->SetActions(EventActions->Get(Decoder.AttrToWStr(Elem,L"list")));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlEventActionListControllerDecoder::SetEventActions
											(const pTEventActionListManager &Val)
{
	EventActions = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
