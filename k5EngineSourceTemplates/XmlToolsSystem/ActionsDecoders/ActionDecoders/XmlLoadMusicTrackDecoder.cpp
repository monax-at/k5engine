#include "XmlLoadMusicTrackDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlLoadMusicTrackDecoder::TXmlLoadMusicTrackDecoder():	TXmlBaseActionDecoder(),
														MusicTracks(NULL)

{
	Exception.SetPrefix(L"TXmlLoadMusicTrackDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlLoadMusicTrackDecoder: ");

	ActionTypeName = L"LoadMusicTrack";
}
///--------------------------------------------------------------------------------------//
TXmlLoadMusicTrackDecoder::~TXmlLoadMusicTrackDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlLoadMusicTrackDecoder::Construct()
{
	return new TActionLoadMusicTrack;
}
///--------------------------------------------------------------------------------------//
void TXmlLoadMusicTrackDecoder::Init(const pTBaseAction &Action,TiXmlElement* Elem)
{
	Exception(MusicTracks!=NULL, L"in Init(...) MusicTracks is NULL");

	pTActionLoadMusicTrack Obj(static_cast<pTActionLoadMusicTrack>(Action));

	Obj->SetMusicTracks(MusicTracks);

	Obj->SetListName	(Decoder.AttrToWStr(Elem,L"list"));
	Obj->SetTrackName	(Decoder.AttrToWStr(Elem,L"track"));
	Obj->SetFileName	(Decoder.AttrToWStr(Elem,L"file"));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlLoadMusicTrackDecoder::SetMusicTracks(const pTMusicTrackListManager &Val)
{
	MusicTracks = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
