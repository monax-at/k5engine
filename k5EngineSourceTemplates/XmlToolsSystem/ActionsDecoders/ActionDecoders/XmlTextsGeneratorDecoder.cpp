#include "XmlTextsGeneratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlTextsGeneratorDecoder::TXmlTextsGeneratorDecoder():	TXmlBaseActionDecoder(),Fonts(NULL)
{
	Exception.SetPrefix(L"TXmlTextsGeneratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlTextsGeneratorDecoder: ");

	ActionTypeName = L"TextsGenerator";
}
///--------------------------------------------------------------------------------------//
TXmlTextsGeneratorDecoder::~TXmlTextsGeneratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlTextsGeneratorDecoder::Construct()
{
	return new TActionTextsGenerator;
}
///--------------------------------------------------------------------------------------//
void TXmlTextsGeneratorDecoder::Init(const pTBaseAction &Action,TiXmlElement* Elem)
{
	Exception(Fonts!=NULL,L"in Init(...) Fonts is NULL");

	pTActionTextsGenerator Obj(static_cast<pTActionTextsGenerator>(Action));

	Obj->SetTexts(Decoder.GetTexts());
	Obj->SetTextListName(Decoder.AttrToWStr(Elem,L"list_name"));

	TXmlText XmlText;
	XmlText.SetDevice(Decoder.GetTexts()->GetDevice());
	XmlText.SetFontList(Fonts);

	for(TiXmlElement* ElemText = Elem->FirstChildElement();
		ElemText != NULL; ElemText = ElemText->NextSiblingElement())
	{
		Obj->AddTemplate(XmlText.ToText(ElemText));
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlTextsGeneratorDecoder::SetFonts(const pTFontList &Val)
{
	Fonts = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlTextsGeneratorDecoder::SetTexts(const pTTextListManager &Val)
{
	Decoder.SetTexts(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
