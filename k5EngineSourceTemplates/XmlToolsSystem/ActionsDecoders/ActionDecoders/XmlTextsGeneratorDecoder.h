///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLTEXTSGENERATORDECODER_H_INCLUDED
#define XMLTEXTSGENERATORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "../../GraphicSystem/XmlText.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlTextsGeneratorDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTFontList Fonts;
	public:
		TXmlTextsGeneratorDecoder();
		virtual ~TXmlTextsGeneratorDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetFonts(const pTFontList &Val);
		void SetTexts(const pTTextListManager &Val);
};

typedef TXmlTextsGeneratorDecoder* pTXmlTextsGeneratorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLTEXTSGENERATORDECODER_H_INCLUDED
