#include "XmlDeleteGraphicObjectDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlDeleteGraphicObjectDecoder::TXmlDeleteGraphicObjectDecoder():TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlDeleteGraphicObjectDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlDeleteGraphicObjectDecoder: ");

	ActionTypeName = L"DeleteGraphicObject";
}
///--------------------------------------------------------------------------------------//
TXmlDeleteGraphicObjectDecoder::~TXmlDeleteGraphicObjectDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlDeleteGraphicObjectDecoder::Construct()
{
	return new TActionDeleteGraphicObject;
}
///--------------------------------------------------------------------------------------//
void TXmlDeleteGraphicObjectDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	pTActionDeleteGraphicObject Obj =
						static_cast<pTActionDeleteGraphicObject>(Action);

	if(Elem == NULL){return;}

	TiXmlElement* ElemObject = Decoder.GetElem(Elem,L"Object");
	Exception(ElemObject!=NULL,L"in Init(...) ElemObject is NULL ");

	if(!Decoder.IsAttrExist(ElemObject,L"type")){
		Obj->SetSprites(Decoder.GetSprites());
//		Obj->SetSpriteBanks(Decoder.GetSpriteBanks());
		Obj->SetTexts(Decoder.GetTexts());
	}
	else{
		wstring ObjectType(Decoder.AttrToWStr(ElemObject,L"type"));

		if(ObjectType == L"Sprite" || ObjectType == L"SpriteBank" || ObjectType == L"Text"){

			if(ObjectType == L"Sprite")	 	{ Obj->SetSprites		( Decoder.GetSprites() );}
//			if(ObjectType == L"SpriteBank")	{ Obj->SetSpriteBanks	( Decoder.GetSpriteBanks() );}
			if(ObjectType == L"Text")		{ Obj->SetTexts			( Decoder.GetTexts() );}
		}
		else{
			Obj->SetSprites(Decoder.GetSprites());
//			Obj->SetSpriteBanks(Decoder.GetSpriteBanks());
			Obj->SetTexts(Decoder.GetTexts());
		}
	}

	if(Decoder.IsAttrExist(ElemObject,L"list")){
		Obj->SetObjectList(Decoder.AttrToWStr(ElemObject,L"list"));
	}

	Obj->SetObjectName(Decoder.AttrToWStr(ElemObject,L"name"));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlDeleteGraphicObjectDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlDeleteGraphicObjectDecoder::SetSpriteBanks(const pTGraphicBankListManager &Val)
{
//	Decoder.SetSpriteBanks(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlDeleteGraphicObjectDecoder::SetTexts(const pTTextListManager &Val)
{
	Decoder.SetTexts(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
