#include "XmlSortRenderQueueDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSortRenderQueueDecoder::TXmlSortRenderQueueDecoder():TXmlBaseActionDecoder(),
														RenderQueue(NULL)
{
	Exception.SetPrefix(L"TXmlSortRenderQueueDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlSortRenderQueueDecoder: ");

	ActionTypeName = L"SortRenderQueue";
}
///--------------------------------------------------------------------------------------//
TXmlSortRenderQueueDecoder::~TXmlSortRenderQueueDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlSortRenderQueueDecoder::Construct()
{
	return new TActionSortRenderQueue;
}
///--------------------------------------------------------------------------------------//
void TXmlSortRenderQueueDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	Exception(RenderQueue!=NULL,L"in Init(...) RenderQueue is NULL");

	pTActionSortRenderQueue Obj(static_cast<pTActionSortRenderQueue>(Action));
	Obj->Set(RenderQueue);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlSortRenderQueueDecoder::SetRenderQueue(const pTRenderQueue &Val)
{
	RenderQueue = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
