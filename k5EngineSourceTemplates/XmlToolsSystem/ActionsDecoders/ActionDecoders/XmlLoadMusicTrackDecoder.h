///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLLOADMUSICTRACKDECODER_H_INCLUDED
#define XMLLOADMUSICTRACKDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "STActionSystem.h"
#include "Base/XmlBaseActionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlLoadMusicTrackDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTMusicTrackListManager MusicTracks;
	public:
		TXmlLoadMusicTrackDecoder();
		virtual ~TXmlLoadMusicTrackDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetMusicTracks(const pTMusicTrackListManager &Val);
};

typedef TXmlLoadMusicTrackDecoder* pTXmlLoadMusicTrackDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLLOADMUSICTRACKDECODER_H_INCLUDED
