#include "XmlChangeGraphicObjectListViewDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlChangeGraphicObjectListViewDecoder::TXmlChangeGraphicObjectListViewDecoder():
								   TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlChangeGraphicObjectListViewDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlChangeGraphicObjectListViewDecoder: ");

	ActionTypeName = L"ChangeGraphicObjectListView";
}
///--------------------------------------------------------------------------------------//
TXmlChangeGraphicObjectListViewDecoder::~TXmlChangeGraphicObjectListViewDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlChangeGraphicObjectListViewDecoder::Construct()
{
	return new TActionChangeGraphicObjectListView;
}
///--------------------------------------------------------------------------------------//
void TXmlChangeGraphicObjectListViewDecoder::Init(	const pTBaseAction &Action,
													TiXmlElement* Elem)
{
/*	pTActionChangeGraphicObjectListView Obj =
					static_cast<pTActionChangeGraphicObjectListView>(Action);

	if(Elem == NULL){return;}

    if(Decoder.IsAttrExist(Elem,L"mode")){
		wstring ActionMode(Decoder.AttrToWStr(Elem,L"mode"));
        if(ActionMode == L"set")
            Obj->SetMode(false);
        if(ActionMode == L"toggle")
            Obj->SetMode(true);
    }

	wstring ListType(L"sprite");
	if(Decoder.IsAttrExist(Elem,L"list_type")){
		ListType = Decoder.AttrToWStr(Elem,L"list_type");
	}

	wstring ListName(Decoder.AttrToWStr(Elem,L"list_name"));

	if(ListType==L"sprite")	  	{ Obj->Set(Decoder.GetSprites()->Get(ListName));}
	if(ListType==L"sprite_bank"){ Obj->Set(Decoder.GetGraphicBanks()->Get(ListName));}
	if(ListType==L"text")		{ Obj->Set(Decoder.GetTexts()->Get(ListName));}

	if(Decoder.IsAttrExist(Elem,L"view")){
		Obj->SetFlag(Decoder.AttrToBool(Elem,L"view"));
	}

	SetDefaultParams(Obj,Elem);*/
}
///--------------------------------------------------------------------------------------//
void TXmlChangeGraphicObjectListViewDecoder::SetSprites
											(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlChangeGraphicObjectListViewDecoder::SetSpriteBanks
											(const pTGraphicBankListManager &Val)
{
//	Decoder.SetSpriteBanks(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlChangeGraphicObjectListViewDecoder::SetTexts(const pTTextListManager &Val)
{
	Decoder.SetTexts(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
