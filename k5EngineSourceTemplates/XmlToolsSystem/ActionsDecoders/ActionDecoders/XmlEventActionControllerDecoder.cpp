#include "XmlEventActionControllerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlEventActionControllerDecoder::TXmlEventActionControllerDecoder():
								   TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlEventActionControllerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlEventActionControllerDecoder: ");

	ActionTypeName = L"EventActionController";
}
///--------------------------------------------------------------------------------------//
TXmlEventActionControllerDecoder::~TXmlEventActionControllerDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlEventActionControllerDecoder::Construct()
{
	return new TActionEventActionController;
}
///--------------------------------------------------------------------------------------//
void TXmlEventActionControllerDecoder::Init(const pTBaseAction &Action, TiXmlElement* Elem)
{
	pTActionEventActionController Obj =
						static_cast<pTActionEventActionController>(Action);

	if(Decoder.IsAttrExist(Elem,L"use_is_active_function")){
		Obj->SetUseIsActiveFunction(Decoder.AttrToBool(Elem,L"use_is_active_function"));
	}

	Obj->SetControlState(Decoder.AttrToActionControlState(Elem,L"state"));

	for(TiXmlElement* ElemEAction = Elem->FirstChildElement();
		ElemEAction != NULL; ElemEAction = ElemEAction->NextSiblingElement())
	{
		Obj->Add(Decoder.ToEventAction(ElemEAction));
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlEventActionControllerDecoder::SetEventActions(const pTEventActionListManager &Val)
{
	Decoder.SetEventActions(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
