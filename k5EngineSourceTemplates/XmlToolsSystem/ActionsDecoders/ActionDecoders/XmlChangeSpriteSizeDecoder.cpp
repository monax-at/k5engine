#include "XmlChangeSpriteSizeDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlChangeSpriteSizeDecoder::TXmlChangeSpriteSizeDecoder():
								TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlChangeSpriteSizeDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlChangeSpriteSizeDecoder: ");

	ActionTypeName = L"ChangeSpriteSize";
}
///--------------------------------------------------------------------------------------//
TXmlChangeSpriteSizeDecoder::~TXmlChangeSpriteSizeDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlChangeSpriteSizeDecoder::Construct()
{
	return new TActionChangeSpriteSize;
}
///--------------------------------------------------------------------------------------//
void TXmlChangeSpriteSizeDecoder::Init(const pTBaseAction &Action,
										TiXmlElement* Elem)
{
	pTActionChangeSpriteSize Obj(static_cast<pTActionChangeSpriteSize>(Action));

	Obj->Set(Decoder.ToSprite(Elem,L"Sprite"));

	if(Decoder.IsAttrExist(Elem,L"direction")){
		Obj->SetDir(Decoder.AttrToChangeValueDirection(Elem,L"direction"));
	}

	if(Decoder.IsAttrExist(Elem,L"width")){
		Obj->SetWidth(Decoder.AttrToFloat(Elem,L"width"));
	}

	if(Decoder.IsAttrExist(Elem,L"height")){
		Obj->SetHeight(Decoder.AttrToFloat(Elem,L"height"));
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlChangeSpriteSizeDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
