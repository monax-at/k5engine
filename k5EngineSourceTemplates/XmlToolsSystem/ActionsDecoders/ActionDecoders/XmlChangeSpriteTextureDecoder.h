///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLCHANGESPRITETEXTUREDECODER_H_INCLUDED
#define XMLCHANGESPRITETEXTUREDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlChangeSpriteTextureDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlChangeSpriteTextureDecoder();
		virtual ~TXmlChangeSpriteTextureDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetSprites(const pTSpriteListManager &Val);
		void SetTextures(const pTTextureListManager &Val);
};

typedef TXmlChangeSpriteTextureDecoder* pTXmlChangeSpriteTextureDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLCHANGESPRITETEXTUREDECODER_H_INCLUDED
