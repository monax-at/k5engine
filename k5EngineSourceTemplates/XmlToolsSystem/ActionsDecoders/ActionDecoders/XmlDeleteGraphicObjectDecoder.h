///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLDELETEGRAPHICOBJECTDECODER_H_INCLUDED
#define XMLDELETEGRAPHICOBJECTDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlDeleteGraphicObjectDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlDeleteGraphicObjectDecoder();
		virtual ~TXmlDeleteGraphicObjectDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetSprites		(const pTSpriteListManager &Val);
		void SetSpriteBanks	(const pTGraphicBankListManager &Val);
		void SetTexts		(const pTTextListManager &Val);
};

typedef TXmlDeleteGraphicObjectDecoder* pTXmlDeleteGraphicObjectDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLDELETEGRAPHICOBJECTDECODER_H_INCLUDED
