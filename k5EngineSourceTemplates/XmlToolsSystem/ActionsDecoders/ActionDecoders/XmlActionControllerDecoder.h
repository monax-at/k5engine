///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLACTIONCONTROLLERDECODER_H_INCLUDED
#define XMLACTIONCONTROLLERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlActionControllerDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlActionControllerDecoder();
		virtual ~TXmlActionControllerDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetActions(const pTActionListManager &Val);
};

typedef TXmlActionControllerDecoder* pTXmlActionControllerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLACTIONLISTCONTROLLERDECODER_H_INCLUDED
