#include "XmlActionControllerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlActionControllerDecoder::TXmlActionControllerDecoder():TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlActionControllerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlActionControllerDecoder: ");

	ActionTypeName = L"ActionController";
}
///--------------------------------------------------------------------------------------//
TXmlActionControllerDecoder::~TXmlActionControllerDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlActionControllerDecoder::Construct()
{
	return new TActionActionController;
}
///--------------------------------------------------------------------------------------//
void TXmlActionControllerDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	pTActionActionController Obj(static_cast<pTActionActionController>(Action));

	Obj->Set(Decoder.ToAction(Decoder.GetElem(Elem,L"Action")));

	if(Decoder.IsAttrExist(Elem,L"state")){
		Obj->SetState(Decoder.AttrToActionControlState(Elem,L"state"));
	}
	else{
		Obj->SetState(EN_ACS_Run);
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlActionControllerDecoder::SetActions(const pTActionListManager &Val)
{
	Decoder.SetActions(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
