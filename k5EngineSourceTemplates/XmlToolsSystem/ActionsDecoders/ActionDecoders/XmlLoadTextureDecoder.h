///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLLOADTEXTUREDECODER_H_INCLUDED
#define XMLLOADTEXTUREDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlLoadTextureDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTBaseDevice Device;
		pTTextureListManager Textures;
	public:
		TXmlLoadTextureDecoder();
		virtual ~TXmlLoadTextureDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetDevice(const pTBaseDevice &Val);
		void SetTextures(const pTTextureListManager &Val);
};

typedef TXmlLoadTextureDecoder* pTXmlLoadTextureDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLLOADTEXTUREDECODER_H_INCLUDED
