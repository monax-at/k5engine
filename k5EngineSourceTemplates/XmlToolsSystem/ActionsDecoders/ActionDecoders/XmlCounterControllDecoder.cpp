#include "XmlCounterControllDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlCounterControllDecoder::TXmlCounterControllDecoder():
								 TXmlBaseActionDecoder(),ActionsDecoder(NULL)
{
	Exception.SetPrefix(L"TXmlCounterControllDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlCounterControllDecoder: ");

	ActionTypeName = L"CounterControll";
}
///--------------------------------------------------------------------------------------//
TXmlCounterControllDecoder::~TXmlCounterControllDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlCounterControllDecoder::Construct()
{
	return new TActionCounterControll;
}
///--------------------------------------------------------------------------------------//
void TXmlCounterControllDecoder::Init(	const pTBaseAction &Action,
										TiXmlElement* Elem)
{
	Exception(ActionsDecoder!=NULL,L"in Init(...) ActionsDecoder is NULL");

	pTActionCounterControll Obj(static_cast<pTActionCounterControll>(Action));
	Obj->SetCount(Decoder.AttrToInt(Elem,L"count"));

	TiXmlElement* ElemTickActions(Decoder.GetElem(Elem,L"TickActions"));
	TiXmlElement* ElemFinishTickActions(Decoder.GetElem(Elem,L"FinishTickActions"));

	pTActionList TickActions(Obj->GetTickActionsPtr());
	pTActionList FinishTickActions(Obj->GetFinishTickActionsPtr());

	ActionsDecoder->CreateList(TickActions,ElemTickActions);
	ActionsDecoder->CreateList(FinishTickActions,ElemFinishTickActions);

	ActionsDecoder->InitList(TickActions,ElemTickActions);
	ActionsDecoder->InitList(FinishTickActions,ElemFinishTickActions);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlCounterControllDecoder::SetActionsDecoder(const pTXmlActionsDecoder &Val)
{
	ActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
