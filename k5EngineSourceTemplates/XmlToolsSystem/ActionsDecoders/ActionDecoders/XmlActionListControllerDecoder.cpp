#include "XmlActionListControllerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlActionListControllerDecoder::TXmlActionListControllerDecoder():
								TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlActionListControllerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlActionListControllerDecoder: ");

	ActionTypeName = L"ActionListController";
}
///--------------------------------------------------------------------------------------//
TXmlActionListControllerDecoder::~TXmlActionListControllerDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlActionListControllerDecoder::Construct()
{
	return new TActionActionListController;
}
///--------------------------------------------------------------------------------------//
void TXmlActionListControllerDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	pTActionActionListController Obj =
							static_cast<pTActionActionListController>(Action);

	Obj->SetControlState(Decoder.AttrToActionControlState(Elem,L"state"));
	Obj->SetActions(Decoder.AttrToActionList(Elem,L"list"));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlActionListControllerDecoder::SetActions(const pTActionListManager &Val)
{
	Decoder.SetActions(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
