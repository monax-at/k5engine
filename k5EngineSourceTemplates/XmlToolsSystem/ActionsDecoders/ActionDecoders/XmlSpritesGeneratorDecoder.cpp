#include "XmlSpritesGeneratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpritesGeneratorDecoder::TXmlSpritesGeneratorDecoder():TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlSpritesGeneratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlSpritesGeneratorDecoder: ");

	ActionTypeName = L"SpritesGenerator";
}
///--------------------------------------------------------------------------------------//
TXmlSpritesGeneratorDecoder::~TXmlSpritesGeneratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlSpritesGeneratorDecoder::Construct()
{
	return new TActionSpritesGenerator;
}
///--------------------------------------------------------------------------------------//
void TXmlSpritesGeneratorDecoder::Init(const pTBaseAction &Action,TiXmlElement* Elem)
{
	pTActionSpritesGenerator Obj(static_cast<pTActionSpritesGenerator>(Action));

	Obj->SetSprites(Decoder.GetSprites());
	Obj->SetSpriteListName(Decoder.AttrToWStr(Elem,L"list_name"));

	TXmlSprite XmlSprite;
	XmlSprite.SetDevice(Decoder.GetSprites()->GetDevice());
	XmlSprite.SetTextures(Decoder.GetTextures());

	for(TiXmlElement* ElemSprite = Elem->FirstChildElement();
		ElemSprite != NULL; ElemSprite = ElemSprite->NextSiblingElement())
	{
		Obj->AddTemplate(XmlSprite.ToSprite(ElemSprite));
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlSpritesGeneratorDecoder::SetTextures(const pTTextureListManager &Val)
{
	Decoder.SetTextures(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlSpritesGeneratorDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
