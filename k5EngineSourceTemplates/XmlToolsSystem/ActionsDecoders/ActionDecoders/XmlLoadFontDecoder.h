///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLLOADFONTDECODER_H_INCLUDED
#define XMLLOADFONTDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "STActionSystem.h"
#include "Base/XmlBaseActionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlLoadFontDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTFontFaceList 	FontFaceList;
		pTFontList 		FontList;
	public:
		TXmlLoadFontDecoder();
		virtual ~TXmlLoadFontDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetFontFaceList(const pTFontFaceList &Val);
		void SetFontList	(const pTFontList &Val);
};

typedef TXmlLoadFontDecoder* pTXmlLoadFontDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLLOADFONTDECODER_H_INCLUDED
