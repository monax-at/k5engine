///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLCHANGEGRAPHICOBJECTVIEWDECODER_H_INCLUDED
#define XMLCHANGEGRAPHICOBJECTVIEWDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlChangeGraphicObjectViewDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlChangeGraphicObjectViewDecoder();
		virtual ~TXmlChangeGraphicObjectViewDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetSprites		(const pTSpriteListManager &Val);
		void SetSpriteBanks	(const pTGraphicBankListManager &Val);
		void SetTexts		(const pTTextListManager &Val);
};

typedef TXmlChangeGraphicObjectViewDecoder* pTXmlChangeGraphicObjectViewDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMCHANGEGRAPHICOBJECTVIEWDECODER_H_INCLUDED
