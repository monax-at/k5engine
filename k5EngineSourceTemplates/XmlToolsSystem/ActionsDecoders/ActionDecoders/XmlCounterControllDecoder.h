///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLCOUNTERCONTROLLDECODER_H_INCLUDED
#define XMLCOUNTERCONTROLLDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"

#include "../../XmlActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlCounterControllDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTXmlActionsDecoder ActionsDecoder;
	public:
		TXmlCounterControllDecoder();
		virtual ~TXmlCounterControllDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetActionsDecoder(const pTXmlActionsDecoder &Val);
};

typedef TXmlCounterControllDecoder* pTXmlCounterControllDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLCOUNTERCONTROLLDECODER_H_INCLUDED
