///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLACTIONLISTCONTROLLERDECODER_H_INCLUDED
#define XMLACTIONLISTCONTROLLERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "STActionSystem.h"
#include "Base/XmlBaseActionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlActionListControllerDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlActionListControllerDecoder();
		virtual ~TXmlActionListControllerDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetActions(const pTActionListManager &Val);
};

typedef TXmlActionListControllerDecoder* pTXmlActionListControllerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLACTIONLISTCONTROLLERDECODER_H_INCLUDED
