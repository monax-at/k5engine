#include "XmlStopApplicationDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlStopApplicationDecoder::TXmlStopApplicationDecoder():	TXmlBaseActionDecoder(),
															App(NULL),EventActionApp(NULL)
{
	Exception.SetPrefix(L"TXmlStopApplicationDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlStopApplicationDecoder: ");

	ActionTypeName = L"StopApplication";
}
///--------------------------------------------------------------------------------------//
TXmlStopApplicationDecoder::~TXmlStopApplicationDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlStopApplicationDecoder::Construct()
{
	return new TActionStopApplication;
}
///--------------------------------------------------------------------------------------//
void TXmlStopApplicationDecoder::Init(const pTBaseAction &Action,TiXmlElement* Elem)
{
	pTActionStopApplication Obj(static_cast<pTActionStopApplication>(Action));

	Obj->Set(App);
	Obj->Set(EventActionApp);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlStopApplicationDecoder::Set(const pTBaseApplication &Val)
{
	App = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlStopApplicationDecoder::Set(const pTBaseEventAction &Val)
{
	EventActionApp = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
