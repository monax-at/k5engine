///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLCHANGEGRAPHICOBJECTLISTVIEWDECODER_H_INCLUDED
#define XMLCHANGEGRAPHICOBJECTLISTVIEWDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlChangeGraphicObjectListViewDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlChangeGraphicObjectListViewDecoder();
		virtual ~TXmlChangeGraphicObjectListViewDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetSprites		(const pTSpriteListManager &Val);
		void SetSpriteBanks	(const pTGraphicBankListManager &Val);
		void SetTexts		(const pTTextListManager &Val);
};

typedef TXmlChangeGraphicObjectListViewDecoder*
										pTXmlChangeGraphicObjectListViewDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLCHANGEGRAPHICOBJECTLISTVIEWDECODER_H_INCLUDED
