#include "XmlChangeGraphicObjectViewDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlChangeGraphicObjectViewDecoder::TXmlChangeGraphicObjectViewDecoder():
								   TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlChangeGraphicObjectViewDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlChangeGraphicObjectViewDecoder: ");

	ActionTypeName = L"ChangeGraphicObjectView";
}
///--------------------------------------------------------------------------------------//
TXmlChangeGraphicObjectViewDecoder::~TXmlChangeGraphicObjectViewDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlChangeGraphicObjectViewDecoder::Construct()
{
	return new TActionChangeGraphicObjectView;
}
///--------------------------------------------------------------------------------------//
void TXmlChangeGraphicObjectViewDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	pTActionChangeGraphicObjectView Obj =
						static_cast<pTActionChangeGraphicObjectView>(Action);

	if(Elem == NULL){return;}

    if(Decoder.IsAttrExist(Elem,L"mode")){
		wstring ActionMode(Decoder.AttrToWStr(Elem,L"mode"));
        if(ActionMode == L"set")
            Obj->SetMode(false);
        if(ActionMode == L"toggle")
            Obj->SetMode(true);
    }

	for(TiXmlElement* ElemObject = Elem->FirstChildElement();
		ElemObject != NULL; ElemObject = ElemObject->NextSiblingElement())
	{
		wstring ObjectType(Decoder.AttrToWStr(ElemObject,L"type"));

		if(Decoder.IsAttrExist(ElemObject,L"view")){
            bool ViewFlagVal = Decoder.AttrToBool(ElemObject,L"view");

            if(ObjectType==L"Sprite")	 { Obj->Add(Decoder.ToSprite(ElemObject)    , ViewFlagVal);}
            if(ObjectType==L"SpriteBank"){ Obj->Add(Decoder.ToGraphicBank(ElemObject), ViewFlagVal);}
            if(ObjectType==L"Text")		 { Obj->Add(Decoder.ToText(ElemObject)      , ViewFlagVal);}
		}else{
            if(ObjectType==L"Sprite")	 { Obj->Add(Decoder.ToSprite(ElemObject));}
            if(ObjectType==L"SpriteBank"){ Obj->Add(Decoder.ToGraphicBank(ElemObject));}
            if(ObjectType==L"Text")		 { Obj->Add(Decoder.ToText(ElemObject));}
        }
	}

    if(Decoder.IsAttrExist(Elem,L"view"))
        Obj->SetFlag(Decoder.AttrToBool(Elem,L"view"));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlChangeGraphicObjectViewDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlChangeGraphicObjectViewDecoder::SetSpriteBanks
											(const pTGraphicBankListManager &Val)
{
	//Decoder.SetGraphicBanks(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlChangeGraphicObjectViewDecoder::SetTexts(const pTTextListManager &Val)
{
	Decoder.SetTexts(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
