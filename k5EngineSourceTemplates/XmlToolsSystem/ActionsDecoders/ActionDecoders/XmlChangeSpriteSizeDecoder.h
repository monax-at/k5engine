///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLCHANGESPRITESIZEDECODER_H_INCLUDED
#define XMLCHANGESPRITESIZEDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlChangeSpriteSizeDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlChangeSpriteSizeDecoder();
		virtual ~TXmlChangeSpriteSizeDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetSprites(const pTSpriteListManager &Val);
};

typedef TXmlChangeSpriteSizeDecoder* pTXmlChangeSpriteSizeDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLCHANGESPRITESIZEDECODER_H_INCLUDED
