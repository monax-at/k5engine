#include "XmlMusicTrackControllerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlMusicTrackControllerDecoder::TXmlMusicTrackControllerDecoder():
								TXmlBaseActionDecoder(),MusicTracks(NULL)
{
	Exception.SetPrefix(L"TXmlMusicTrackControllerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlMusicTrackControllerDecoder: ");

	ActionTypeName = L"MusicTrackController";
}
///--------------------------------------------------------------------------------------//
TXmlMusicTrackControllerDecoder::~TXmlMusicTrackControllerDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlMusicTrackControllerDecoder::Construct()
{
	return new TActionMusicTrackController;
}
///--------------------------------------------------------------------------------------//
void TXmlMusicTrackControllerDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	Exception(MusicTracks!=NULL,L"in Init(...) MusicTracks is NULL");

	pTActionMusicTrackController Obj
							(static_cast<pTActionMusicTrackController>(Action));

	Obj->SetMusicTrack(MusicTracks->Get(Decoder.AttrToWStr(Elem,L"list"),
										Decoder.AttrToWStr(Elem,L"track")));

	Obj->SetMode(Decoder.AttrToWStr(Elem,L"state"));

	if(Decoder.IsAttrExist(Elem,L"state_val")){
		Obj->SetModeVal(Decoder.AttrToFloat(Elem,L"state_val"));
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlMusicTrackControllerDecoder::SetMusicTracks
											(const pTMusicTrackListManager &Val)
{
	MusicTracks = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
