///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLEVENTACTIONLISTCONTROLLERDECODER_H_INCLUDED
#define XMLEVENTACTIONLISTCONTROLLERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlEventActionListControllerDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTEventActionListManager EventActions;
	public:
		TXmlEventActionListControllerDecoder();
		virtual ~TXmlEventActionListControllerDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetEventActions(const pTEventActionListManager &Val);
};

typedef TXmlEventActionListControllerDecoder* pTXmlEventActionListControllerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLEVENTACTIONLISTCONTROLLERDECODER_H_INCLUDED
