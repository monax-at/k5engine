#include "XmlRandActionControllSystemDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlRandActionControllSystemDecoder::TXmlRandActionControllSystemDecoder():
									 TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlRandActionControllSystemDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlRandActionControllSystemDecoder: ");

	ActionTypeName = L"RandActionControllSystem";
}
///--------------------------------------------------------------------------------------//
TXmlRandActionControllSystemDecoder::~TXmlRandActionControllSystemDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlRandActionControllSystemDecoder::Construct()
{
	return new TRandActionControllSystem;
}
///--------------------------------------------------------------------------------------//
void TXmlRandActionControllSystemDecoder::Init(	const pTBaseAction &Action,
												TiXmlElement* Elem)
{
	pTRandActionControllSystem Obj(static_cast<pTRandActionControllSystem>(Action));

	for(TiXmlElement* ElemCell=Elem->FirstChildElement();
		ElemCell!=NULL; ElemCell = ElemCell->NextSiblingElement())
	{
		pTRandActionControllSystemCell Cell(new TRandActionControllSystemCell);

		Cell->SetAction(Decoder.ToAction(ElemCell));

		if(Decoder.IsAttrExist(ElemCell,L"cell_name")){
			Cell->SetName(Decoder.AttrToWStr(ElemCell,L"cell_name"));
		}

		if(Decoder.IsAttrExist(ElemCell,L"active")){
			Cell->SetActive(Decoder.AttrToBool(ElemCell,L"active"));
		}

		if(Decoder.IsAttrExist(ElemCell,L"state")){
			Cell->SetState(Decoder.AttrToActionControlState(ElemCell,L"state"));
		}

		if(Decoder.IsAttrExist(ElemCell,L"priority")){
			Cell->SetPriority(Decoder.AttrToInt(ElemCell,L"priority"));
		}

		if(Decoder.IsAttrExist(ElemCell,L"one_execute")){
			Cell->SetPriority(Decoder.AttrToBool(ElemCell,L"one_execute"));
		}

		Obj->Add(Cell);
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlRandActionControllSystemDecoder::SetActions
												(const pTActionListManager &Val)
{
	Decoder.SetActions(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
