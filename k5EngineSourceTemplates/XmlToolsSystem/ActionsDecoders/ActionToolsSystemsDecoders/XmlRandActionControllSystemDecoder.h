///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLRANDACTIONCONTROLLSYSTEMDECODER_H_INCLUDED
#define XMLRANDACTIONCONTROLLSYSTEMDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlRandActionControllSystemDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlRandActionControllSystemDecoder();
		virtual ~TXmlRandActionControllSystemDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetActions(const pTActionListManager &Val);
};

typedef TXmlRandActionControllSystemDecoder* pTXmlRandActionControllSystemDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLCOUNTERCONTROLLDECODER_H_INCLUDED
