#include "XmlRandActionControllSystemCellControllerDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlRandActionControllSystemCellControllerDecoder::
							TXmlRandActionControllSystemCellControllerDecoder():
													TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlRandActionControllSystemCellControllerDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlRandActionControllSystemCellControllerDecoder: ");

	ActionTypeName = L"RandActionControllSystemCellController";
}
///--------------------------------------------------------------------------------------//
TXmlRandActionControllSystemCellControllerDecoder::
							~TXmlRandActionControllSystemCellControllerDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlRandActionControllSystemCellControllerDecoder::Construct()
{
	return new TRandActionControllSystemCellController;
}
///--------------------------------------------------------------------------------------//
void TXmlRandActionControllSystemCellControllerDecoder::Init(
									const pTBaseAction &Action,TiXmlElement* Elem)
{
	pTRandActionControllSystemCellController Obj
				(static_cast<pTRandActionControllSystemCellController>(Action));

	Obj->SetSystem(Decoder.ToAction(Decoder.GetElem(Elem,L"System")));

	if(Decoder.IsAttrExist(Elem,L"cell_name")){
		Obj->SetCellName(Decoder.AttrToWStr(Elem,L"cell_name"));
	}

	if(Decoder.IsAttrExist(Elem,L"cell_id")){
		Obj->SetCellId(Decoder.AttrToInt(Elem,L"cell_id"));
	}

	Obj->SetState(Decoder.AttrToActionControlState(Elem,L"state"));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlRandActionControllSystemCellControllerDecoder::SetActions
												(const pTActionListManager &Val)
{
	Decoder.SetActions(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
