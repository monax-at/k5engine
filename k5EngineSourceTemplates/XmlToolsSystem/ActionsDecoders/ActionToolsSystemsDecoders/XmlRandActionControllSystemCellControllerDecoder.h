///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLRANDACTIONCONTROLLSYSTEMCELLCONTROLLERDECODER_H_INCLUDED
#define XMLRANDACTIONCONTROLLSYSTEMCELLCONTROLLERDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STActionSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlRandActionControllSystemCellControllerDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlRandActionControllSystemCellControllerDecoder();
		virtual ~TXmlRandActionControllSystemCellControllerDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetActions(const pTActionListManager &Val);
};

typedef TXmlRandActionControllSystemCellControllerDecoder*
							pTXmlRandActionControllSystemCellControllerDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLCOUNTERCONTROLLDECODER_H_INCLUDED
