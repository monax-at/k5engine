#include "XmlEngineDataDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlEngineDataDecoder::TXmlEngineDataDecoder():TXmlBaseDataDecoder(),
		Textures(NULL), Sprites(NULL), GraphicBanks(NULL), Texts(NULL),
		Actions(NULL),EventActions(NULL)
{
	Exception.SetPrefix(L"TXmlEngineDataDecoder: ");
}
///--------------------------------------------------------------------------------------//
TXmlEngineDataDecoder::~TXmlEngineDataDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTTexture TXmlEngineDataDecoder::ToTexture(TiXmlElement *Elem)
{
	Exception(Textures != NULL, L"in ToTexture(...) Textures is NULL");
	Exception(Elem 	   != NULL, L"in ToTexture(...) Elem is NULL");

	return Textures->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///--------------------------------------------------------------------------------------//
pTSprite TXmlEngineDataDecoder::ToSprite(TiXmlElement *Elem)
{
	Exception(Sprites != NULL, L"in ToSprite(...) Sprites is NULL");
	Exception(Elem 	  != NULL, L"in ToSprite(...) Elem is NULL");

	return Sprites->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///--------------------------------------------------------------------------------------//
pTGraphicBank TXmlEngineDataDecoder::ToGraphicBank(TiXmlElement *Elem)
{
	Exception(GraphicBanks 	!= NULL, L"in ToSpriteBank(...) GraphicBanks is NULL");
	Exception(Elem 			!= NULL, L"in ToSpriteBank(...) Elem is NULL");

	return GraphicBanks->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///--------------------------------------------------------------------------------------//
pTText TXmlEngineDataDecoder::ToText(TiXmlElement *Elem)
{
	Exception(Texts	!= NULL, L"in ToTextLine(...) Texts is NULL");
	Exception(Elem	!= NULL, L"in ToTextLine(...) Elem is NULL");

	return Texts->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlEngineDataDecoder::ToAction(TiXmlElement *Elem)
{
	Exception(Actions != NULL, L"in ToAction(...) Actions is NULL");
	Exception(Elem 	  != NULL, L"in ToAction(...) Elem is NULL");

	return Actions->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlEngineDataDecoder::ToEventAction(TiXmlElement *Elem)
{
	Exception(EventActions != NULL, L"in ToEventAction(...) EventActions is NULL");
	Exception(Elem 	  	   != NULL, L"in ToEventAction(...) Elem is NULL");

	return EventActions->Get(AttrToWStr(Elem,L"list"), AttrToWStr(Elem,L"name"));
}
///--------------------------------------------------------------------------------------//
pTTexture TXmlEngineDataDecoder::ToTexture(	TiXmlElement *Elem,
											const wstring &TegName)
{
	return ToTexture(GetElem(Elem,TegName));
}
///--------------------------------------------------------------------------------------//
pTSprite TXmlEngineDataDecoder::ToSprite(TiXmlElement *Elem,const wstring &TegName)
{
	return ToSprite(GetElem(Elem,TegName));
}
///--------------------------------------------------------------------------------------//
pTGraphicBank TXmlEngineDataDecoder::ToGraphicBank(TiXmlElement *Elem, const wstring &TegName)
{
	return ToGraphicBank(GetElem(Elem,TegName));
}
///--------------------------------------------------------------------------------------//
pTText TXmlEngineDataDecoder::ToText(TiXmlElement *Elem,const wstring &TegName)
{
	return ToText(GetElem(Elem,TegName));
}
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlEngineDataDecoder::ToAction(TiXmlElement *Elem,
											 const wstring &TegName)
{
	return ToAction(GetElem(Elem,TegName));
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlEngineDataDecoder::ToEventAction(	TiXmlElement *Elem,
														const wstring &TegName)
{
	return ToEventAction(GetElem(Elem,TegName));
}
///--------------------------------------------------------------------------------------//
pTActionList TXmlEngineDataDecoder::AttrToActionList(	TiXmlElement *Elem,
														const wstring &Attr)
{
	Exception(Actions!=NULL,L"in AttrToActionList(...) Actions is NULL");
	return Actions->Get(AttrToWStr(Elem,Attr));
}
///--------------------------------------------------------------------------------------//
pTEventActionList TXmlEngineDataDecoder::AttrToEventActionList
									(TiXmlElement *Elem, const wstring &Attr)
{
	Exception(	EventActions!=NULL,
				L"in AttrToEventActionList(...) EventActions is NULL");

	return EventActions->Get(AttrToWStr(Elem,Attr));
}
///--------------------------------------------------------------------------------------//
TPoint3D TXmlEngineDataDecoder::GetObjectPosWithAlignment(	TiXmlElement *Elem,
															const pTBaseDevice &Device)
{
	if(Device == NULL){ return ToPoint3D(Elem); }

	TPoint3D Pos(ToPoint3D(Elem));

	if( IsAttrExist(Elem, L"alignment_x") ){

		wstring AlignmentX(AttrToWStr(Elem, L"alignment_x"));

		if(AlignmentX == L"left" ) 	{ Pos[0] = Pos.GetX(); }
		if(AlignmentX == L"right" )	{ Pos[0] = Device->GetWindowWidth() - Pos.GetX(); }
		if(AlignmentX == L"center" ){ Pos[0]+= Device->GetWindowWidth()/2.0f; }

	}

	if( IsAttrExist(Elem, L"alignment_y") ){
		wstring AlignmentY(AttrToWStr(Elem, L"alignment_y"));

		if(AlignmentY == L"top" )	{ Pos[1] = Pos.GetY(); }
		if(AlignmentY == L"bottom" ){ Pos[1] = Device->GetWindowHeight() - Pos.GetY();}
		if(AlignmentY == L"center" ){ Pos[1]+= Device->GetWindowHeight()/2.0f; }
	}

	return Pos;
}
///--------------------------------------------------------------------------------------//
TPoint3D TXmlEngineDataDecoder::GetObjectPosWithAlignment(	TiXmlElement *Elem,
										const wstring &TegName,const pTBaseDevice &Device)
{
	return GetObjectPosWithAlignment(GetElem(Elem,TegName),Device);
}
///--------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Sprites = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetGraphicBanks(const pTGraphicBankListManager &Val)
{
	GraphicBanks = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetTexts(const pTTextListManager &Val)
{
	Texts = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetActions(const pTActionListManager &Val)
{
	Actions = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlEngineDataDecoder::SetEventActions(const pTEventActionListManager &Val)
{
	EventActions = Val;
}
///--------------------------------------------------------------------------------------//
pTTextureListManager TXmlEngineDataDecoder::GetTextures()
{
	return Textures;
}
///--------------------------------------------------------------------------------------//
pTSpriteListManager	TXmlEngineDataDecoder::GetSprites()
{
	return Sprites;
}
///--------------------------------------------------------------------------------------//
pTGraphicBankListManager	TXmlEngineDataDecoder::GetGraphicBanks()
{
	return GraphicBanks;
}
///--------------------------------------------------------------------------------------//
pTTextListManager TXmlEngineDataDecoder::GetTexts()
{
	return Texts;
}
///--------------------------------------------------------------------------------------//
pTActionListManager	TXmlEngineDataDecoder::GetActions()
{
	return Actions;
}
///--------------------------------------------------------------------------------------//
pTEventActionListManager TXmlEngineDataDecoder::GetEventActions()
{
	return EventActions;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
