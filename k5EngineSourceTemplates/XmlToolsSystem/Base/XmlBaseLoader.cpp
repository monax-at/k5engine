#include "XmlBaseLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlBaseLoader::TXmlBaseLoader():Exception(L"TXmlBaseLoader: ")
{
}
///--------------------------------------------------------------------------------------//
TXmlBaseLoader::~TXmlBaseLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlBaseLoader::ToRun(TiXmlElement *Teg)
{
}
///--------------------------------------------------------------------------------------//
void TXmlBaseLoader::ToRun(const wstring &File)
{
}
///--------------------------------------------------------------------------------------//
void TXmlBaseLoader::operator()(TiXmlElement *Teg)
{
	ToRun(Teg);
}
///--------------------------------------------------------------------------------------//
void TXmlBaseLoader::Run(TiXmlElement *Teg)
{
	ToRun(Teg);
}
///--------------------------------------------------------------------------------------//
void TXmlBaseLoader::operator()(const wstring &File)
{
	ToRun(File);
}
///--------------------------------------------------------------------------------------//
void TXmlBaseLoader::Run(const wstring &File)
{
	ToRun(File);
}
///--------------------------------------------------------------------------------------//
void TXmlBaseLoader::ManyFiles(TiXmlElement *Teg)
{
	if(Teg == NULL){return;}

	for(TiXmlElement *ElemFile = Teg->FirstChildElement();
		ElemFile != NULL; ElemFile = ElemFile->NextSiblingElement())
	{
		TXmlBaseDataDecoder DataDecoder;
		Run(DataDecoder.AttrToWStr(ElemFile,L"name"));
	}
}
///--------------------------------------------------------------------------------------//
void TXmlBaseLoader::ManyFiles(const wstring &File)
{
	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in ManyFiles(File) can't open file");

	TiXmlHandle Handle(&Doc);

	TiXmlElement* ElemData = Handle.FirstChildElement().Element();
	Exception(ElemData!=NULL,L"in ManyFiles(File) teg Data must be set");

	ManyFiles(ElemData);

	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
