///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLBASEEVENTACTIONDECODER_H_INCLUDED
#define XMLBASEEVENTACTIONDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "XmlEngineDataDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlBaseEventActionDecoder
{
	protected:
		TExceptionGenerator Exception;
		TXmlEngineDataDecoder Decoder;

		wstring ActionTypeName;
	protected:
		void SetDefaultParams(const pTBaseEventAction &Obj, TiXmlElement *Elem);
	public:
		TXmlBaseEventActionDecoder();
		virtual ~TXmlBaseEventActionDecoder();

		bool CheckActionType(const wstring &Type);

		virtual pTBaseEventAction Construct() = 0;
		virtual void Init(const pTBaseEventAction &Action,TiXmlElement* Elem) = 0;
};

typedef TXmlBaseEventActionDecoder* pTXmlBaseEventActionDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLBASEEVENTACTIONDECODER_H_INCLUDED
