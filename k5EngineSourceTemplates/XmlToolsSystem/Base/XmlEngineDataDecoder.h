///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLENGINEDATADECODER_H_INCLUDED
#define XMLENGINEDATADECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "XmlBaseDataDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlEngineDataDecoder:public TXmlBaseDataDecoder
{
	protected:
		pTTextureListManager	 Textures;
		pTSpriteListManager		 Sprites;
		pTGraphicBankListManager GraphicBanks;
		pTTextListManager		 Texts;

		pTActionListManager		 Actions;
		pTEventActionListManager EventActions;
	public:
		TXmlEngineDataDecoder();
		virtual ~TXmlEngineDataDecoder();

		pTTexture 	  		ToTexture		(TiXmlElement *Elem);
		pTSprite 	  		ToSprite 		(TiXmlElement *Elem);
		pTGraphicBank  		ToGraphicBank	(TiXmlElement *Elem);
		pTText				ToText			(TiXmlElement *Elem);
		pTBaseAction		ToAction		(TiXmlElement *Elem);
		pTBaseEventAction	ToEventAction	(TiXmlElement *Elem);

		pTTexture 	 	  ToTexture		(TiXmlElement *Elem, const wstring &TegName);
		pTSprite 	  	  ToSprite 	 	(TiXmlElement *Elem, const wstring &TegName);
		pTGraphicBank  	  ToGraphicBank	(TiXmlElement *Elem, const wstring &TegName);
		pTText			  ToText		(TiXmlElement *Elem, const wstring &TegName);
		pTBaseAction	  ToAction	 	(TiXmlElement *Elem, const wstring &TegName);
		pTBaseEventAction ToEventAction	(TiXmlElement *Elem, const wstring &TegName);

		pTActionList AttrToActionList(TiXmlElement *Elem, const wstring &Attr);

		pTEventActionList AttrToEventActionList
									(TiXmlElement *Elem, const wstring &Attr);

		TPoint3D GetObjectPosWithAlignment(TiXmlElement *Elem,const pTBaseDevice &Device);

		TPoint3D GetObjectPosWithAlignment(	TiXmlElement *Elem,const wstring &TegName,
											const pTBaseDevice &Device);

		void SetTextures	(const pTTextureListManager &Val);
		void SetSprites		(const pTSpriteListManager &Val);
		void SetGraphicBanks(const pTGraphicBankListManager &Val);
		void SetTexts		(const pTTextListManager &Val);
		void SetActions		(const pTActionListManager &Val);
		void SetEventActions(const pTEventActionListManager &Val);

		pTTextureListManager	    GetTextures();
		pTSpriteListManager		    GetSprites();
		pTGraphicBankListManager	GetGraphicBanks();
		pTTextListManager		    GetTexts();

		pTActionListManager		    GetActions();
		pTEventActionListManager    GetEventActions();
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLENGINEDATADECODER_H_INCLUDED
