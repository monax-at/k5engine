#include "XmlBaseEventActionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlBaseEventActionDecoder::TXmlBaseEventActionDecoder()
{
}
///--------------------------------------------------------------------------------------//
TXmlBaseEventActionDecoder::~TXmlBaseEventActionDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlBaseEventActionDecoder::SetDefaultParams(	const pTBaseEventAction &Obj,
													TiXmlElement *Elem)
{
	if(Decoder.IsAttrExist(Elem,L"name")){
		Obj->SetName(Decoder.AttrToWStr(Elem,L"name"));
	}

	if(Decoder.IsAttrExist(Elem,L"active")){
		if(Decoder.AttrToBool(Elem,L"active") == true)	{ Obj->Start(); }
		else											{ Obj->SetActive(false); }
	}
}
///--------------------------------------------------------------------------------------//
bool TXmlBaseEventActionDecoder::CheckActionType(const wstring &Type)
{
	return (ActionTypeName == Type);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
