///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLBASEACTIONDECODER_H_INCLUDED
#define XMLBASEACTIONDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "XmlEngineDataDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlBaseActionDecoder
{
	protected:
		TExceptionGenerator Exception;
		TXmlEngineDataDecoder Decoder;

		wstring ActionTypeName;
	protected:
		void SetDefaultParams(const pTBaseAction &Obj,TiXmlElement *Elem);
	public:
		TXmlBaseActionDecoder();
		virtual ~TXmlBaseActionDecoder();

		bool CheckActionType(const wstring &Type);
		virtual pTBaseAction Construct() = 0;
		virtual void Init(const pTBaseAction &Action,TiXmlElement* Elem) = 0;
};

typedef TXmlBaseActionDecoder* pTXmlBaseActionDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLBASEACTIONDECODER_H_INCLUDED
