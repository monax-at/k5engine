#include "XmlBaseActionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlBaseActionDecoder::TXmlBaseActionDecoder()
{
}
///--------------------------------------------------------------------------------------//
TXmlBaseActionDecoder::~TXmlBaseActionDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlBaseActionDecoder::SetDefaultParams(const pTBaseAction &Obj,TiXmlElement *Elem)
{
	if(Decoder.IsAttrExist(Elem,L"name")){
		Obj->SetName(Decoder.AttrToWStr(Elem,L"name"));
	}

	if(Decoder.IsAttrExist(Elem,L"active")){
		if(Decoder.AttrToBool(Elem,L"active") == true){
			Obj->Start();
			Obj->Run();
		}
	}
}
///--------------------------------------------------------------------------------------//
bool TXmlBaseActionDecoder::CheckActionType(const wstring &Type)
{
	return (ActionTypeName == Type);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//

