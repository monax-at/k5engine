///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLBASELOADER_H_INCLUDED
#define XMLBASELOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "XmlBaseDataDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlBaseLoader
{
	protected:
		TExceptionGenerator Exception;
	protected:
		virtual void ToRun(TiXmlElement *Teg);
		virtual void ToRun(const wstring &File);
	public:
		TXmlBaseLoader();
		virtual ~TXmlBaseLoader();

		void operator()(TiXmlElement *Teg);
		void Run(TiXmlElement *Teg);

		void operator()(const wstring &File);
		void Run(const wstring &File);

		virtual void ManyFiles(TiXmlElement *Teg);
		virtual void ManyFiles(const wstring &File);
};

typedef TXmlBaseLoader* pTXmlBaseLoader;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLBASELOADER_H_INCLUDED
