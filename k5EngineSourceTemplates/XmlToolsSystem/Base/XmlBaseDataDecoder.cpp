#include "XmlBaseDataDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlBaseDataDecoder::TXmlBaseDataDecoder():Exception(L"TXmlBaseDataDecoder: ")
{
}
///--------------------------------------------------------------------------------------//
TXmlBaseDataDecoder::~TXmlBaseDataDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
wstring TXmlBaseDataDecoder::CheckData(TiXmlElement *Val)
{
	Exception(Val!=NULL, L"TiXmlElement* is NULL");
	const char* Text = Val->GetText();
	if(Text==NULL){return wstring(L"");}
	return UTF8ToUnicode(Text);
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::GetSubTeg(TiXmlElement *Elem,
											 const wstring &ElemName)
{
	Exception(Elem != NULL, L"in GetSubTeg(...) Elem is NULL");
	return Elem->FirstChildElement(WStrToStr(ElemName).c_str());
}
///--------------------------------------------------------------------------------------//
TVector2D TXmlBaseDataDecoder::TegToVector2D(TiXmlElement *Val)
{
	Exception(Val!=NULL,L"in TegToVector2D(...) TiXmlElement is NULL");

	const char *X = Val->Attribute("x");
	const char *Y = Val->Attribute("y");

	TVector2D V;

	if(X!=NULL){V.fx = StrToFloat(X);}
	if(Y!=NULL){V.fy = StrToFloat(Y);}

	return V;
}
///--------------------------------------------------------------------------------------//
TPoint TXmlBaseDataDecoder::TegToPoint (TiXmlElement *Val)
{
	Exception(Val!=NULL,L"in TegToPoint(...) TiXmlElement is NULL");

	const char *X = Val->Attribute("x");
	const char *Y = Val->Attribute("y");
	const char *Z = Val->Attribute("z");

	TPoint3D Point;

	if(X!=NULL){Point[0] = StrToFloat(X);}
	if(Y!=NULL){Point[1] = StrToFloat(Y);}
	if(Z!=NULL){Point[2] = StrToFloat(Z);}

	return Point;
}
///--------------------------------------------------------------------------------------//
TColor TXmlBaseDataDecoder::TegToColor(TiXmlElement *Val)
{
	Exception(Val!=NULL,L"in TegToColor(...) TiXmlElement is NULL");

	const char *R = Val->Attribute("red");
	const char *G = Val->Attribute("green");
	const char *B = Val->Attribute("blue");
	const char *A = Val->Attribute("alpha");

	TColor Color;

	if(R!=NULL){ Color(0,StrToFloat(R));}
	if(G!=NULL){ Color(1,StrToFloat(G));}
	if(B!=NULL){ Color(2,StrToFloat(B));}
	if(A!=NULL){ Color(3,StrToFloat(A));}

	return Color;
}
///--------------------------------------------------------------------------------------//
void TXmlBaseDataDecoder::SetExceptionPrefix(const wstring &Val)
{
	Exception.SetPrefix(Val);
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::GetElem(	TiXmlElement *Elem, const wstring &ElemName)
{
	return GetSubTeg(Elem,ElemName);
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::GetFirstElem(TiXmlElement *Elem)
{
	Exception(Elem != NULL, L"in GetFirstElem(...) Elem is NULL");
	return Elem->FirstChildElement();
}
///--------------------------------------------------------------------------------------//
wstring TXmlBaseDataDecoder::ToWStr(TiXmlElement *Val)
{
	return CheckData(Val);
}
///--------------------------------------------------------------------------------------//
int TXmlBaseDataDecoder::ToInt(TiXmlElement *Val)
{
	wstring Data = CheckData(Val);
	Exception(IsWStrInt(Data), L"in ToInt(...) TiXmlElement data not integer");
	return WStrToInt(Data);
}
///--------------------------------------------------------------------------------------//
bool TXmlBaseDataDecoder::ToBool(TiXmlElement *Val)
{
	wstring Data = CheckData(Val);
	Exception(IsWStrInt(Data), L"in ToInt(...) TiXmlElement data not integer");
	return WStrToBool(Data);
}
///--------------------------------------------------------------------------------------//
float TXmlBaseDataDecoder::ToFloat(TiXmlElement *Val)
{
	wstring Data = CheckData(Val);
	Exception(IsWStrFloat(Data), L"in ToFloat(...) TiXmlElement data not float");
	return WStrToFloat(Data);
}
///--------------------------------------------------------------------------------------//
bool TXmlBaseDataDecoder::IsTegExist(TiXmlElement *Elem, const wstring &TegName)
{
	Exception(Elem != NULL, L"in IsTegExist(...) Elem is NULL");
	if(Elem->FirstChildElement(WStrToStr(TegName).c_str()) == NULL){
		return false;
	}
	return true;
}
///--------------------------------------------------------------------------------------//
bool TXmlBaseDataDecoder::IsAttrExist(TiXmlElement  *Elem, const wstring &Attr)
{
	Exception(Elem != NULL, L"in AttrToWStr(...) Elem is NULL");
	const char *Attribute = Elem->Attribute(WStrToStr(Attr).c_str());
	if(Attribute==NULL){return false;}
	return true;
}
///--------------------------------------------------------------------------------------//
wstring TXmlBaseDataDecoder::AttrToWStr(TiXmlElement *Elem,const wstring &Attr)
{
	Exception(Elem != NULL, L"in AttrToWStr(...) Elem is NULL");
	const char *Attribute = Elem->Attribute(WStrToStr(Attr).c_str());
	Exception(Attribute != NULL, L"in AttrToWStr(...) Attribute is not exist");

	return StrToWStr(Attribute);
}
///--------------------------------------------------------------------------------------//
int TXmlBaseDataDecoder::AttrToInt(TiXmlElement   *Elem, const wstring &Attr)
{
	Exception(Elem != NULL, L"in AttrToInt(...) Elem is NULL");
	int Res = 0;
	int ErrCode = Elem->QueryIntAttribute(WStrToStr(Attr).c_str(),&Res);
	if(ErrCode == TIXML_NO_ATTRIBUTE){
		Exception(L"in AttrToInt(...) Attribute is not exist");
	}
	if(ErrCode == TIXML_WRONG_TYPE){
		Exception(L"in AttrToInt(...) Attribute is not integer");
	}
	return Res;
}
///--------------------------------------------------------------------------------------//
bool TXmlBaseDataDecoder::AttrToBool(TiXmlElement  *Elem, const wstring &Attr)
{
	Exception(Elem != NULL, L"in AttrToBool(...) Elem is NULL");
	int Res = 0;
	int ErrCode = Elem->QueryIntAttribute(WStrToStr(Attr).c_str(),&Res);
	if(ErrCode == TIXML_NO_ATTRIBUTE){
		Exception(L"in AttrToBool(...) Attribute is not exist");
	}
	if(ErrCode == TIXML_WRONG_TYPE){
		Exception(L"in AttrToBool(...) Attribute is not bool");
	}
	if(Res == 0){return false;}
	return true;
}
///--------------------------------------------------------------------------------------//
float TXmlBaseDataDecoder::AttrToFloat(TiXmlElement *Elem, const wstring &Attr)
{
	Exception(Elem != NULL, L"in AttrToFloat(...) Elem is NULL");
	float Res = 0.0f;
	int ErrCode = Elem->QueryFloatAttribute(WStrToStr(Attr).c_str(),&Res);
	if(ErrCode == TIXML_NO_ATTRIBUTE){
		Exception(L"in AttrToFloat(...) Attribute is not exist");
	}
	if(ErrCode == TIXML_WRONG_TYPE){
		Exception(L"in AttrToFloat(...) Attribute is not float");
	}
	return Res;
}
///--------------------------------------------------------------------------------------//
enTextureFilter TXmlBaseDataDecoder::AttrToTextureFilter(	TiXmlElement *Elem,
															const wstring &Attr)
{
	Exception(Elem != NULL, L"in AttrToTextureFilter(...) Elem is NULL");

	const char *Attribute = Elem->Attribute(WStrToStr(Attr).c_str());
	Exception(Attribute != NULL, 	L"in AttrToTextureFilter(...)"
									L"Attribute:" + Attr + L" is not exist");

	wstring Val(StrToWStr(Attribute));

	if(Val == L"linear"){return EN_TF_Linear;}
	if(Val == L"none")	{return EN_TF_None;}
	if(Val == L"point")	{return EN_TF_Point;}

	return EN_TF_Unknown;
}
///--------------------------------------------------------------------------------------//
enChangeValueDirection TXmlBaseDataDecoder::AttrToChangeValueDirection(
										TiXmlElement *Elem,const wstring &Attr)
{
	Exception(Elem != NULL, L"in AttrToChangeValueDirection(...) Elem is NULL");

	const char *Attribute = Elem->Attribute(WStrToStr(Attr).c_str());
	Exception(Attribute != NULL, 	L"in AttrToChangeValueDirection(...)"
									L"Attribute:" + Attr + L" is not exist");

	wstring Val(StrToWStr(Attribute));

	if(Val == L"inc"){return EN_CVD_Inc;}
	if(Val == L"dec"){return EN_CVD_Dec;}
	if(Val == L"set"){return EN_CVD_Set;}

	return EN_CVD_Unknown;
}
///--------------------------------------------------------------------------------------//
enActionControlState TXmlBaseDataDecoder::AttrToActionControlState(
										TiXmlElement *Elem,const wstring &Attr)
{
	Exception(Elem != NULL, L"in AttrToActionControlState(...) Elem is NULL");

	const char *Attribute = Elem->Attribute(WStrToStr(Attr).c_str());
	Exception(Attribute != NULL, 	L"in AttrToActionControlState(...) "
									L"Attribute:" + Attr + L" is not exist");

	wstring Val(StrToWStr(Attribute));

	if(Val == L"run")	 {return EN_ACS_Run;}
	if(Val == L"start")	 {return EN_ACS_Start;}
	if(Val == L"stop")	 {return EN_ACS_Stop;}
	if(Val == L"resume") {return EN_ACS_Resume;}
	if(Val == L"suspend"){return EN_ACS_Suspend;}

	return EN_ACS_Unknown;
}
///--------------------------------------------------------------------------------------//
wstring TXmlBaseDataDecoder::GetTegText(TiXmlElement  *Elem)
{
	Exception(Elem!=NULL,L"in GetTegText(...) TiXmlElement is NULL");
	return StrToWStr(Elem->Value());
}
///--------------------------------------------------------------------------------------//
TVector2D TXmlBaseDataDecoder::ToVector2D(TiXmlElement *Val)
{
	return TegToVector2D(Val);
}
///--------------------------------------------------------------------------------------//
TPoint TXmlBaseDataDecoder::ToPoint3D(TiXmlElement *Val)
{
	return TegToPoint(Val);
}
///--------------------------------------------------------------------------------------//
TColor TXmlBaseDataDecoder::ToColor(TiXmlElement *Val)
{
	return TegToColor(Val);
}
///--------------------------------------------------------------------------------------//
TVector2D TXmlBaseDataDecoder::ToVector2D(TiXmlElement *Elem,const wstring &TegName)
{
	return TegToVector2D(GetSubTeg(Elem,TegName));
}
///--------------------------------------------------------------------------------------//
TPoint TXmlBaseDataDecoder::ToPoint(TiXmlElement *Elem, const wstring &TegName)
{
	return TegToPoint(GetSubTeg(Elem,TegName));
}
///--------------------------------------------------------------------------------------//
TColor TXmlBaseDataDecoder::ToColor(TiXmlElement *Elem, const wstring &TegName)
{
	return TegToColor(GetSubTeg(Elem,TegName));
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::WStrToElem(const wstring &Elem,const wstring &Val)
{
	TiXmlElement* TiEl 	 = new TiXmlElement(WStrToStr(Elem).c_str());
	TiXmlText*	  TiText = new TiXmlText(WStrToStr(Val).c_str());
	TiEl->LinkEndChild( TiText );
	return TiEl;
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::IntToElem(const wstring &Elem,const int &Val)
{
	TiXmlElement* TiEl 	 = new TiXmlElement(WStrToStr(Elem).c_str());
	TiXmlText*	  TiText = new TiXmlText(IntToStr(Val).c_str());
	TiEl->LinkEndChild( TiText );
	return TiEl;
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::BoolToElem(const wstring &Elem,const bool &Val)
{
	TiXmlElement* TiEl 	 = new TiXmlElement(WStrToStr(Elem).c_str());
	TiXmlText*	  TiText = new TiXmlText(BoolToStr(Val).c_str());
	TiEl->LinkEndChild( TiText );
	return TiEl;
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::FloatToElem(const wstring &Elem,const float &Val)
{
	TiXmlElement* TiEl 	 = new TiXmlElement(WStrToStr(Elem).c_str());
	TiXmlText*	  TiText = new TiXmlText(FloatToStr(Val).c_str());
	TiEl->LinkEndChild( TiText );
	return TiEl;
}
///--------------------------------------------------------------------------------------//
void TXmlBaseDataDecoder::WStrToAttr(TiXmlElement*Elem,
										const wstring &Attr,const wstring &Val)
{
	Elem->SetAttribute(WStrToStr(Attr).c_str(),WStrToStr(Val).c_str());
}
///--------------------------------------------------------------------------------------//
void TXmlBaseDataDecoder::IntToAttr(TiXmlElement*Elem,
										const wstring &Attr,const int &Val)
{
	Elem->SetAttribute(WStrToStr(Attr).c_str(),IntToStr(Val).c_str());
}
///--------------------------------------------------------------------------------------//
void TXmlBaseDataDecoder::BoolToAttr(TiXmlElement*Elem,
										const wstring &Attr,const bool &Val)
{
	Elem->SetAttribute(WStrToStr(Attr).c_str(),BoolToStr(Val).c_str());
}
///--------------------------------------------------------------------------------------//
void TXmlBaseDataDecoder::FloatToAttr(TiXmlElement*Elem,
										const wstring &Attr,const float &Val)
{
	Elem->SetAttribute(WStrToStr(Attr).c_str(),FloatToStr(Val).c_str());
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::Vector2DToElem(	const wstring &Elem,
													const TVector2D &Val)
{
	TiXmlElement* TiEl = new TiXmlElement(WStrToStr(Elem).c_str());
	FloatToAttr(TiEl,L"x",Val.fx);
	FloatToAttr(TiEl,L"y",Val.fy);
	return TiEl;
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::PointToElem(	const wstring &Elem,
													const TPoint &Val)
{
	TiXmlElement* TiEl(new TiXmlElement(WStrToStr(Elem).c_str()));

	FloatToAttr(TiEl, L"x",Val.GetX());
	FloatToAttr(TiEl, L"y",Val.GetY());
	FloatToAttr(TiEl, L"z",Val.GetZ());

	return TiEl;
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlBaseDataDecoder::ColorToElem	(	const wstring &Elem,
													const TColor &Val)
{
	TiXmlElement* TiEl(new TiXmlElement(WStrToStr(Elem).c_str()));

	FloatToAttr(TiEl, L"red",Val.GetRed());
	FloatToAttr(TiEl, L"green",Val.GetGreen());
	FloatToAttr(TiEl, L"blue",Val.GetBlue());
	FloatToAttr(TiEl, L"alpha",Val.GetAlpha());

	return TiEl;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
