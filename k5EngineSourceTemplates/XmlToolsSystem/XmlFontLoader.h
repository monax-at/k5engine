///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLFONTLOADER_H_INCLUDED
#define XMLFONTLOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlFontLoader:public TXmlBaseLoader
{
	private:
		pTFontFaceList 	FontFaceList;
		pTFontList 		FontList;
	private:
		inline void ParseFontItems(TBaseFontFace *FontFace, TiXmlElement* Elem);
		inline void ParseFont(TiXmlElement* Elem);

		void ToRun(TiXmlElement *Teg);
		void ToRun(const wstring &File);
	public:
		TXmlFontLoader();
		TXmlFontLoader(const pTFontFaceList &FFList,const pTFontList &FList);
		virtual ~TXmlFontLoader();

		void SetLists(const pTFontFaceList &FFList,const pTFontList &FList);
};

typedef TXmlFontLoader* pTXmlFontLoader;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // FONTLOADER_H_INCLUDED
