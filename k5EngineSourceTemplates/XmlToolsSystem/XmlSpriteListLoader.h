///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITELISTLOADER_H_INCLUDED
#define XMLSPRITELISTLOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseLoader.h"
#include "GraphicSystem/XmlSprite.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpriteListLoader:public TXmlBaseLoader
{
	private:
		pTSpriteList 		 List;
		pTTextureListManager Textures;

		TXmlSprite XmlSprite;
	private:
		void ToRun(TiXmlElement *Teg);
		void ToRun(const wstring &File);
	public:
		TXmlSpriteListLoader();
		TXmlSpriteListLoader(const pTSpriteList &Sprites,
							 const pTTextureListManager &Tex);

		virtual ~TXmlSpriteListLoader();

		void SetList(const pTSpriteList &Val);
		void SetTextures(const pTTextureListManager &Val);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // CLASS_SPRITELISTLOADER_H_INCLUDED
