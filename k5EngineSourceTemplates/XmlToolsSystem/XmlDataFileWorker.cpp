#include "XmlDataFileWorker.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlDataFileWorker::TXmlDataFileWorker()//:TDataFileWorker()
{
	//Exception.SetPrefix(L"TXmlDataFileWorker: ");
}
///--------------------------------------------------------------------------------------//
TXmlDataFileWorker::~TXmlDataFileWorker()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlDataFileWorker::SaveToXmlFile()
{
}
///--------------------------------------------------------------------------------------//
void TXmlDataFileWorker::LoadFromXmlFile()
{
	TiXmlDocument Doc(WStrToStr(FileName).c_str());

	//Exception(Doc.LoadFile(),L"can't open file: " + FileName);

	TXmlBaseDataDecoder Decoder;

	TiXmlHandle Handle(&Doc);
	TiXmlElement* Elem(Handle.FirstChildElement().Element());

	for(TiXmlElement* ConfElem = Elem->FirstChildElement();
		ConfElem != NULL; ConfElem = ConfElem->NextSiblingElement())
	{
		Set(StrToWStr(ConfElem->Value()), Decoder.AttrToWStr(ConfElem, L"val") );
	}

	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlDataFileWorker::SaveToXmlFile(const wstring &Value)
{
	//try{
		//FileName = Value;
		//SaveToXmlFile();
	//}
	//catch(TBaseException &Ex){ Exception(Ex); }
}
///--------------------------------------------------------------------------------------//
void TXmlDataFileWorker::LoadFromXmlFile(const wstring &Value)
{
	//try{
		//FileName = Value;
		//LoadFromXmlFile();
	//}
	//catch(TBaseException &Ex){ Exception(Ex); }
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
