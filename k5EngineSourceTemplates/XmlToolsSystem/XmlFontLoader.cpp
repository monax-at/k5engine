#include "XmlFontLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlFontLoader::TXmlFontLoader():TXmlBaseLoader(),FontFaceList(NULL),FontList(NULL)
{
	Exception.SetPrefix(L"TXmlFontLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlFontLoader::TXmlFontLoader(const pTFontFaceList &FFList,const pTFontList &FList):
							TXmlBaseLoader(),FontFaceList(FFList),FontList(FList)
{
	Exception.SetPrefix(L"TXmlFontLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlFontLoader::~TXmlFontLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlFontLoader::SetLists(const pTFontFaceList &FFList,const pTFontList &FList)
{
	FontFaceList = FFList;
	FontList 	 = FList;
}
///--------------------------------------------------------------------------------------//
void TXmlFontLoader::ParseFontItems(TBaseFontFace *FontFace, TiXmlElement* Elem)
{
	TXmlBaseDataDecoder XmlDecoder;
	TiXmlElement* Item = NULL;
	for(Item = Elem->FirstChildElement(); Item != NULL;
		Item = Item->NextSiblingElement())
	{
		wstring Name = XmlDecoder.AttrToWStr(Item,L"name");
		int Size = XmlDecoder.AttrToInt(Item,L"size");

		FontList->Add(FontFace,Size,Name);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlFontLoader::ParseFont(TiXmlElement* Elem)
{
	TXmlBaseDataDecoder XmlDecoder;
	wstring Name = XmlDecoder.AttrToWStr(Elem,L"name");
	wstring Path = XmlDecoder.AttrToWStr(Elem,L"path");

	TFTFontFaceLoader FontFaceLoader;
	FontFaceList->Add(FontFaceLoader.Run(Path,Name));

	TBaseFontFace *FontFace = FontFaceList->GetLast();
	ParseFontItems(FontFace,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlFontLoader::ToRun(TiXmlElement *Teg)
{
	if(Teg == NULL){return;}
	return;
}
///--------------------------------------------------------------------------------------//
void TXmlFontLoader::ToRun(const wstring &File)
{
	Exception(FontFaceList 	!= NULL, L"in ToRun(File) FontFaceList pointer must be set");
	Exception(FontList   	!= NULL, L"in ToRun(File) FontList pointer must be set");

	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in ToRun(File) can't open file: " + File);

	TiXmlHandle Handle(&Doc);

	TiXmlElement* ElemData = Handle.FirstChildElement().Element();
	if(ElemData == NULL){return;}

	TiXmlElement* Elem = NULL;
	for(Elem = ElemData->FirstChildElement(); Elem != NULL;
		Elem = Elem->NextSiblingElement())
	{
		ParseFont(Elem);
	}
	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
