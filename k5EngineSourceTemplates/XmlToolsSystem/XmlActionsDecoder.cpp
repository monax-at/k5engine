#include "XmlActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlActionsDecoder::TXmlActionsDecoder():Exception(L"TXmlActionsDecoder: ")
{
	Decoder.SetExceptionPrefix(L"TXmlActionsDecoder: ");
}
///--------------------------------------------------------------------------------------//
TXmlActionsDecoder::~TXmlActionsDecoder()
{
	Clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlActionsDecoder::Create(TiXmlElement* Elem)
{
	wstring Type = Decoder.AttrToWStr(Elem,L"type");
	int Size = Elems.size();

	for(int i=0;i<Size;i++){
		if(Elems[i]->CheckActionType(Type)){ return Elems[i]->Construct(); }
	}

	Exception(L"in Create(...) unknown action type");
	return NULL;
}
///--------------------------------------------------------------------------------------//
void TXmlActionsDecoder::Init(const pTBaseAction &Action, TiXmlElement* Elem)
{
	wstring Type = Decoder.AttrToWStr(Elem,L"type");
	int Size = Elems.size();

	for(int i=0;i<Size;i++){
		if(Elems[i]->CheckActionType(Type)){
			Elems[i]->Init(Action,Elem);
			return;
		}
	}

	Exception(L"in Init(...) unknown action type");
}
///--------------------------------------------------------------------------------------//
void TXmlActionsDecoder::CreateList(const pTActionList &List,TiXmlElement* Elem)
{
	if(List == NULL || Elem == NULL){return;}

	// создание в списке действий
	for(TiXmlElement* ElemAction = Elem->FirstChildElement();
		ElemAction != NULL; ElemAction = ElemAction->NextSiblingElement())
	{
		pTBaseAction Action(Create(ElemAction));

		if(Decoder.IsAttrExist(ElemAction,L"name")){
			Action->SetName(Decoder.AttrToWStr(ElemAction,L"name"));
		}

		List->Add(Action);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlActionsDecoder::InitList(const pTActionList &List,TiXmlElement* Elem)
{
	if(List == NULL || Elem == NULL){return;}

	int ActionCounter(0);

	for(TiXmlElement* ElemAction = Elem->FirstChildElement();
		ElemAction != NULL; ElemAction = ElemAction->NextSiblingElement())
	{
		Init(List->Get(ActionCounter),ElemAction);
		ActionCounter++;
	}
}
///--------------------------------------------------------------------------------------//
void TXmlActionsDecoder::Add(const pTXmlBaseActionDecoder &Val)
{
	Elems.push_back(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlActionsDecoder::Clear()
{
	int Size = Elems.size();
	for(int i=0;i<Size;i++){ delete Elems[i]; }
	Elems.clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
