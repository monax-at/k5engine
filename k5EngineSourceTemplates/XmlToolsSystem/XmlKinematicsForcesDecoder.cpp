#include "XmlKinematicsForcesDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlKinematicsForcesDecoder::TXmlKinematicsForcesDecoder():
											TXmlBaseDataDecoder(),XmlElem(NULL)
{
	Exception.SetPrefix(L"TXmlKinematicsForcesDecoder: ");
}
///--------------------------------------------------------------------------------------//
TXmlKinematicsForcesDecoder::~TXmlKinematicsForcesDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsBaseForce* TXmlKinematicsForcesDecoder::ToDirectedForce()
{
	TKinematicsDirectedForce *Force(new TKinematicsDirectedForce);

	Force->SetVal(AttrToFloat(XmlElem,L"val"));
	Force->SetPos(AttrToFloat(XmlElem,L"x"),AttrToFloat(XmlElem,L"y"));

	return Force;
}
///--------------------------------------------------------------------------------------//
TKinematicsBaseForce* TXmlKinematicsForcesDecoder::ToGlobalRandDirectedForce()
{
	TKinematicsGlobalRandDirectedForce *Force(new
										TKinematicsGlobalRandDirectedForce);

	Force->SetVal(AttrToFloat(XmlElem,L"val"));
	Force->SetMinDir(ToVector2D(XmlElem->FirstChildElement("Min")));
	Force->SetMaxDir(ToVector2D(XmlElem->FirstChildElement("Max")));

	return Force;
}
///--------------------------------------------------------------------------------------//
TKinematicsBaseForce* TXmlKinematicsForcesDecoder::ToGlobalStaticForce()
{
	TKinematicsGlobalStaticForce *Force(new TKinematicsGlobalStaticForce);

	Force->SetVal(AttrToFloat(XmlElem,L"val"));
	Force->SetDir(AttrToFloat(XmlElem,L"x"),AttrToFloat(XmlElem,L"y"));

	return Force;
}
///--------------------------------------------------------------------------------------//
TKinematicsBaseForce* TXmlKinematicsForcesDecoder::ToSpriteAngleSpeedRandRange()
{
	TKinematicsSpriteAngleSpeedRandRange *Force(new
										TKinematicsSpriteAngleSpeedRandRange);

	Force->SetMin(AttrToInt(XmlElem,L"min"));
	Force->SetMax(AttrToInt(XmlElem,L"max"));

	return Force;
}
///--------------------------------------------------------------------------------------//
TKinematicsBaseForce* TXmlKinematicsForcesDecoder::ToSpriteColorSpeedRandRange()
{
	TKinematicsSpriteColorSpeedRandRange *Force(new
										TKinematicsSpriteColorSpeedRandRange);

	Force->SetMin(ToColor(XmlElem->FirstChildElement("Min")));
	Force->SetMax(ToColor(XmlElem->FirstChildElement("Max")));

	if(IsAttrExist(XmlElem,L"direction")){
		Force->SetDir(AttrToChangeValueDirection(XmlElem,L"direction"));
	}

	return Force;
}
///--------------------------------------------------------------------------------------//
int TXmlKinematicsForcesDecoder::CheckType(const wstring &Type)
{
	if(Type == L"DirectedForce")			{return 1;}
	if(Type == L"GlobalRandDirectedForce")	{return 2;}
	if(Type == L"GlobalStaticForce")		{return 3;}
	if(Type == L"SpriteAngleSpeedRandRange"){return 4;}
	if(Type == L"SpriteColorSpeedRandRange"){return 5;}

	return 0;
}
///--------------------------------------------------------------------------------------//
TKinematicsBaseForce* TXmlKinematicsForcesDecoder::Decode(TiXmlElement *Val)
{
	XmlElem = Val;
	Exception(XmlElem!=NULL,L"in Decode() XmlElem is NULL");

	switch(CheckType( AttrToWStr(XmlElem,L"type") )){
		default:break;
		case 1 :{return ToDirectedForce();				}break;
		case 2 :{return ToGlobalRandDirectedForce();	}break;
		case 3 :{return ToGlobalStaticForce();			}break;
		case 4 :{return ToSpriteAngleSpeedRandRange();	}break;
		case 5 :{return ToSpriteColorSpeedRandRange();	}break;
	}

	Exception(L"in Decode() unknown force type");
	return NULL;
}
///--------------------------------------------------------------------------------------//
TKinematicsBaseForce* TXmlKinematicsForcesDecoder::operator()(TiXmlElement *Val)
{
	return Decode(Val);
}
///--------------------------------------------------------------------------------------//
TKinematicsBaseForce* TXmlKinematicsForcesDecoder::Run(TiXmlElement *Val)
{
	return Decode(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
