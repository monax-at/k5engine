#include "XmlUISimpleButtonDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlUISimpleButtonDecoder::TXmlUISimpleButtonDecoder():TXmlBaseEventActionDecoder(),
									ActionsDecoder(NULL)
{
	Exception.SetPrefix(L"TXmlUISimpleButtonDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlUISimpleButtonDecoder: ");

	ActionTypeName = L"UISimpleButton";
}
///--------------------------------------------------------------------------------------//
TXmlUISimpleButtonDecoder::~TXmlUISimpleButtonDecoder()
{
}
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlUISimpleButtonDecoder::Construct()
{
	return new TUISimpleButton;
}
///--------------------------------------------------------------------------------------//
void TXmlUISimpleButtonDecoder::Init(const pTBaseEventAction &Action,TiXmlElement* Elem)
{
	Exception(ActionsDecoder !=NULL, L"int Init(...) ActionsDecoder is NULL");

	pTUISimpleButton Obj(static_cast<pTUISimpleButton>(Action));

	Obj->Set(Decoder.ToSprite(Elem,L"Sprite"));

	TiXmlElement* ElemOnMouseEnter(Decoder.GetElem(Elem,L"OnMouseEnter"));

	ActionsDecoder->CreateList	(Obj->GetOnMouseEnterPtr(),ElemOnMouseEnter);
	ActionsDecoder->InitList	(Obj->GetOnMouseEnterPtr(),ElemOnMouseEnter);

	TiXmlElement* ElemOnMouseLeave(Decoder.GetElem(Elem,L"OnMouseLeave"));

	ActionsDecoder->CreateList	(Obj->GetOnMouseLeavePtr() ,ElemOnMouseLeave);
	ActionsDecoder->InitList	(Obj->GetOnMouseLeavePtr() ,ElemOnMouseLeave);

	TiXmlElement* ElemOnMouseButtonDown(Decoder.GetElem(Elem,L"OnMouseButtonDown"));

	ActionsDecoder->CreateList	(Obj->GetOnMouseButtonDownPtr(),ElemOnMouseButtonDown);
	ActionsDecoder->InitList	(Obj->GetOnMouseButtonDownPtr(),ElemOnMouseButtonDown);

	TiXmlElement* ElemOnMouseButtonUp(Decoder.GetElem(Elem,L"OnMouseButtonUp"));

	ActionsDecoder->CreateList	(Obj->GetOnMouseButtonUpPtr() ,ElemOnMouseButtonUp);
	ActionsDecoder->InitList	(Obj->GetOnMouseButtonUpPtr() ,ElemOnMouseButtonUp);

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlUISimpleButtonDecoder::SetActionsDecoder(const pTXmlActionsDecoder &Val)
{
	ActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlUISimpleButtonDecoder::SetSprites(const pTSpriteListManager &Val)
{
	Decoder.SetSprites(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
