///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLUISIMPLEBUTTONDECODER_H_INCLUDED
#define XMLUISIMPLEBUTTONDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"
#include "STActionSystem.h"

#include "../Base/XmlBaseEventActionDecoder.h"
#include "../XmlActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlUISimpleButtonDecoder:public TXmlBaseEventActionDecoder
{
	protected:
		pTXmlActionsDecoder ActionsDecoder;
	public:
		TXmlUISimpleButtonDecoder();
		virtual ~TXmlUISimpleButtonDecoder();

		pTBaseEventAction Construct();
		void Init(const pTBaseEventAction &Action, TiXmlElement* Elem);

		void SetActionsDecoder(const pTXmlActionsDecoder &Val);
		void SetSprites(const pTSpriteListManager &Val);
};

typedef TXmlUISimpleButtonDecoder* pTXmlUISimpleButtonDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLUISIMPLEBUTTONDECODER_H_INCLUDED
