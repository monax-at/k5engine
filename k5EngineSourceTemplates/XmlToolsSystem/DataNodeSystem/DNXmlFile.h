///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNXMLFILE_H_INCLUDED
#define DNXMLFILE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"
#include "XmlBaseDataDecoder.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNXmlFile
{
	protected:
		TExceptionGenerator Ex;
	protected:
		void ParceXmlTeg(TiXmlElement* XmlElem, const pTDataNode &Node, bool Update = false);
		TiXmlElement* ParceDataNode(const pTDataNode &Node);
	public:
		TDNXmlFile ();
		~TDNXmlFile();

		void Load(const wstring &FileName, const pTDataNode &Node,bool Update = false);
		TDataNode Load(const wstring &FileName);

		void Save  (const wstring &FileName, const pTDataNode &Node);

		void Create(const wstring &FileName);
		void Clear (const wstring &FileName);
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNXMLFILE_H_INCLUDED
