#include "DNXmlFile.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNXmlFile::TDNXmlFile():Ex(L"TDNXmlFile: ")
{
}
///----------------------------------------------------------------------------------------------//
TDNXmlFile::~TDNXmlFile()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDNXmlFile::ParceXmlTeg(TiXmlElement* XmlElem, const pTDataNode &Node, bool Update)
{
	if(XmlElem == NULL){return;}

	Node->SetName( StrToWStr(XmlElem->Value()) );

	const char* TextVal = XmlElem->GetText();

	if(TextVal != NULL){
		Node->SetType(EN_DNT_Other);
		Node->SetVal(StrToWStr(TextVal));
	}

	// ���� ���� ������ ������, �� ��������� � ������ ����������
	// ����� - ���� �������� ������ � ����
	int NodeSize(Node->GetSize());
	if(NodeSize == 0){
		for(TiXmlAttribute* Attr = XmlElem->FirstAttribute(); Attr != NULL; Attr = Attr->Next())
		{
			Node->Add( StrToWStr(Attr->Value()), StrToWStr(Attr->Name()));
		}

		for(TiXmlElement* ChildElem = XmlElem->FirstChildElement(); ChildElem != NULL;
			ChildElem = ChildElem->NextSiblingElement())
		{
			ParceXmlTeg(ChildElem, Node->Add(new TDataNode));
		}
	}
	else{
		int NodeItr(0);
		for(TiXmlAttribute* Attr = XmlElem->FirstAttribute(); Attr != NULL; Attr = Attr->Next())
		{
			pTDataNode NodeChild(Node->Get(NodeItr));

			NodeChild->SetName(StrToWStr( Attr->Name() ));
			NodeChild->Set    (StrToWStr( Attr->Value()));


			NodeItr++;
		}

		for(TiXmlElement* ChildElem = XmlElem->FirstChildElement(); ChildElem != NULL;
			ChildElem = ChildElem->NextSiblingElement())
		{
			ParceXmlTeg(ChildElem, Node->Get(NodeItr));
			NodeItr++;
		}
	}
}
///----------------------------------------------------------------------------------------------//
TiXmlElement* TDNXmlFile::ParceDataNode(const pTDataNode &Node)
{
	Ex(Node->GetName().length() >0, L"in ParceDataNode(Node) Node Name not set");

	TiXmlElement * XmlElem(new TiXmlElement( WStrToStr(Node->GetName()).c_str() ));

	int Size(Node->GetSize());
	for(int i=0;i<Size;i++){
		pTDataNode ChildNode(Node->Get(i));

		if(ChildNode->GetSize()>0){
			XmlElem->LinkEndChild( ParceDataNode(ChildNode) );
		}
		else{
			switch(ChildNode->GetType()){
				case EN_DNT_Bool :{
					XmlElem->SetAttribute( 	WStrToStr(ChildNode->GetName()).c_str(),
											(int)(ChildNode->GetBoolVal()) );
				} break;

				case EN_DNT_Int :{
					XmlElem->SetAttribute( 	WStrToStr(ChildNode->GetName()).c_str(),
											ChildNode->GetIntVal() );
				} break;

				case EN_DNT_Float :{
					XmlElem->SetDoubleAttribute( WStrToStr(ChildNode->GetName()).c_str(),
												 ChildNode->GetFloat() );
				} break;

				case EN_DNT_String :{
					XmlElem->SetAttribute(	WStrToStr(ChildNode->GetName()).c_str(),
											WStrToStr(ChildNode->GetStringVal()).c_str() );
				} break;

				case EN_DNT_Other :{
					TiXmlElement * XmlElemText(new TiXmlElement(
											WStrToStr(ChildNode->GetName()).c_str() ));

					XmlElemText->LinkEndChild( new TiXmlText(
											WStrToStr(ChildNode->GetStringVal()).c_str()) );

					XmlElem->LinkEndChild( XmlElemText );
				} break;

				default:break;
			}
		}
	}

	return XmlElem;
}
///----------------------------------------------------------------------------------------------//
void TDNXmlFile::Load(const wstring &FileName, const pTDataNode &Node ,bool Update)
{
	Ex(FileName.length()>0, L"in Save(FileName,Node) FileName length = 0 ");

	TiXmlDocument Doc(WStrToStr(FileName).c_str());

	Ex(Doc.LoadFile(), L"in Load(FileName,NodeName) can't open file: " + FileName);

	TiXmlHandle Handle(&Doc);

	ParceXmlTeg(Handle.FirstChildElement().Element(), Node, Update);

	Doc.Clear();
}
///----------------------------------------------------------------------------------------------//
TDataNode TDNXmlFile::Load(const wstring &FileName)
{
	TDataNode Node;
	Load(FileName,&Node);
	return Node;
}
///----------------------------------------------------------------------------------------------//
void TDNXmlFile::Save(const wstring &FileName,const pTDataNode &Node)
{
	Ex(Node!=NULL			, L"in Save(FileName,Node) Node is NULL ");
	Ex(FileName.length()>0	, L"in Save(FileName,Node) FileName length = 0 ");

	TiXmlDocument Doc;
	TiXmlDeclaration *Decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );
	Doc.LinkEndChild( Decl );

	Doc.LinkEndChild( ParceDataNode(Node) );

	Doc.SaveFile( WStrToStr(FileName).c_str() );
}
///----------------------------------------------------------------------------------------------//
void TDNXmlFile::Create(const wstring &FileName)
{
	TFolderWorker FolderWorker;
	if(FolderWorker.IsFileExist(FileName)){return;}

	TiXmlDocument Doc;
	Doc.LinkEndChild(new TiXmlDeclaration( "1.0", "UTF-8", "" ));
	Doc.SaveFile( WStrToStr(FileName).c_str() );
}
///----------------------------------------------------------------------------------------------//
void TDNXmlFile::Clear(const wstring &FileName)
{
	Ex(FileName.length()>0, L"in Clear(FileName,Node) FileName length = 0 ");

	TiXmlDocument Doc(WStrToStr(FileName).c_str());

	Ex(Doc.LoadFile(), L"in Clear(FileName,NodeName) can't open file: " + FileName);

	Doc.Clear();
	Doc.LinkEndChild(new TiXmlDeclaration( "1.0", "UTF-8", "" ));
	Doc.SaveFile( WStrToStr(FileName).c_str() );
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
