///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XmlSpriteBank_H_INCLUDED
#define XmlSpriteBank_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "XmlSprite.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpriteBank
{
	protected:
		pTTextureListManager	Textures;
		pTBaseDevice			Device;
	public:
		TXmlSpriteBank();
		virtual ~TXmlSpriteBank();

		pTGraphicBank ToSpriteBank(TiXmlElement* Elem);
		TiXmlElement* ToElem(const pTGraphicBank &SpriteBank);

		void SetTextures(const pTTextureListManager &Val);
		void SetDevice	(const pTBaseDevice &Val);
};

typedef TXmlSpriteBank* pTXmlSpriteBank;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLSPRITE_H_INCLUDED
