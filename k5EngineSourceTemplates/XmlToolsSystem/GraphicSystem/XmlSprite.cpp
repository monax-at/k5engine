#include "XmlSprite.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSprite::TXmlSprite():Textures(NULL),Device(NULL)
{
}
///--------------------------------------------------------------------------------------//
TXmlSprite::~TXmlSprite()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTSprite TXmlSprite::ToSprite(TiXmlElement* Elem)
{
	TExceptionGenerator	Exception(L"TXmlSprite: ");
	Exception(Textures!=NULL,L"in ToSprite() Textures is NULL");

	TXmlEngineDataDecoder Decoder;
	Decoder.SetExceptionPrefix(L"TXmlSprite: ");
	Decoder.SetTextures(Textures);

	pTSprite Sprite(new TSprite);

	// элемент "Name", его может не быть, поэтому проверка на наличие
	if(Decoder.IsAttrExist(Elem,L"name")){
		Sprite->SetName( Decoder.AttrToWStr(Elem,L"name"));
	}

	// элемент View, его может не быть, поэтому проверка на наличие
	if(Decoder.IsAttrExist(Elem,L"view")){
		Sprite->SetView( Decoder.AttrToBool(Elem,L"view"));
	}

	// фильтр текстур
	if(Decoder.IsAttrExist(Elem,L"min_filter") ){
		Sprite->SetMinFilter(Decoder.AttrToTextureFilter(Elem,L"min_filter"));
	}

	if(Decoder.IsAttrExist(Elem,L"mag_filter") ){
		Sprite->SetMagFilter(Decoder.AttrToTextureFilter(Elem,L"mag_filter"));
	}

	if(Decoder.IsAttrExist(Elem,L"filter") ){
		Sprite->SetFilter(Decoder.AttrToTextureFilter(Elem,L"filter"));
	}

	// элемент "Texture", элемент обязателен
	TiXmlElement *ElemTexture(Decoder.GetElem(Elem,L"Texture"));
	pTTexture Texture = Decoder.ToTexture(ElemTexture);
	Sprite->Texture = Texture;
	Sprite->Square.Set((float)Texture->GetWidth(),(float)Texture->GetHeight());

	// Mirror не обязателен
	TiXmlElement *ElemMirror = Decoder.GetElem(ElemTexture,L"Mirror");
	if(ElemMirror!=NULL){
		if(Decoder.IsAttrExist(ElemMirror,L"x")){
			Sprite->Texture.SetMirrorX(Decoder.AttrToBool(ElemMirror,L"x"));
		}

		if(Decoder.IsAttrExist(ElemMirror,L"y")){
			Sprite->Texture.SetMirrorY(Decoder.AttrToBool(ElemMirror,L"y"));
		}
	}

	TiXmlElement *ElemRepeat = Decoder.GetElem(ElemTexture,L"Repeat");
	if(ElemRepeat != NULL){
		if(Decoder.IsAttrExist(ElemRepeat,L"x")){
			Sprite->Texture.SetRepeatX(Decoder.AttrToFloat(ElemRepeat,L"x"));
		}

		if(Decoder.IsAttrExist(ElemRepeat,L"y")){
			Sprite->Texture.SetRepeatY(Decoder.AttrToFloat(ElemRepeat,L"y"));
		}
	}

	// Rect не обязателен
	TiXmlElement *ElemRect(Decoder.GetElem(ElemTexture,L"Rect"));
	if(ElemRect != NULL){
		float X(Decoder.AttrToFloat(ElemRect,L"x"));
		float Y(Decoder.AttrToFloat(ElemRect,L"y"));
		float Width(Decoder.AttrToFloat(ElemRect,L"width"));
		float Height(Decoder.AttrToFloat(ElemRect,L"height"));

		Sprite->Texture.SetRect(X, Y, Width, Height);
		Sprite->Square.Set(Width,Height);
	}

	// элемент "Pos", элемент не обязателен
	if(Decoder.IsTegExist(Elem,L"Pos")){
		Sprite->Pos(Decoder.GetObjectPosWithAlignment(Elem,L"Pos",Device));
	}

	if(Decoder.IsTegExist(Elem,L"Center")){
		Sprite->Center(Decoder.ToPoint3D(Elem,L"Center"));
	}

	// элемент "Angle", элемент не обязателен
	TiXmlElement *ElemAngle(Decoder.GetElem(Elem,L"Angle"));
	if(ElemAngle!=NULL){
		Sprite->Angle(Decoder.AttrToFloat(ElemAngle,L"val"));
	}

	// элемент "Square", элемент не обязателен
	TiXmlElement *ElemSquare(Decoder.GetElem(Elem,L"Square"));
	if(ElemSquare!=NULL){
		Sprite->Square(	Decoder.AttrToFloat(ElemSquare,L"width"),
						Decoder.AttrToFloat(ElemSquare,L"height"));
	}

	// элемент "Color", элемент не обязателен
	TiXmlElement *ElemColor(Decoder.GetElem(Elem,L"Color"));
	if(ElemColor!=NULL){
		Sprite->Color.Set(Decoder.ToColor(ElemColor));
	}

	if(Device!=NULL){Sprite->SetDevice(Device);}

	return Sprite;
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlSprite::ToElem(const pTSprite &Sprite)
{
	TiXmlElement* ElemSprite(new TiXmlElement("Sprite"));
	ElemSprite->SetAttribute("name", WStrToStr(Sprite->GetName()).c_str() );
	ElemSprite->SetAttribute("view", Sprite->GetView() );

	// настройка ElemTexture
	TiXmlElement* ElemTexture(new TiXmlElement("Texture"));

	if(Textures != NULL){
		pTTextureList TextureList(Textures->GetListByTexture(Sprite->Texture.Get()));
		ElemTexture->SetAttribute("list", WStrToStr(TextureList->GetName()).c_str() );
	}

	ElemTexture->SetAttribute("name", WStrToStr(Sprite->Texture.Get()->GetName()).c_str() );
	ElemSprite->LinkEndChild(ElemTexture);

	// настройка ElemTextureMirror
	if(Sprite->Texture.GetMirrorX() == true || Sprite->Texture.GetMirrorX() == true){
		TiXmlElement* ElemTextureMirror(new TiXmlElement("Mirror"));

		if(Sprite->Texture.GetMirrorX()){
			ElemTextureMirror->SetAttribute("x", Sprite->Texture.GetMirrorX() );
		}

		if(Sprite->Texture.GetMirrorY()){
			ElemTextureMirror->SetAttribute("y", Sprite->Texture.GetMirrorY() );
		}

		ElemTexture->LinkEndChild(ElemTextureMirror);
	}

	// настройка ElemTextureRect
	TiXmlElement* ElemTextureRect(new TiXmlElement("Rect"));
	ElemTextureRect->SetDoubleAttribute("x", Sprite->Texture.GetRectX() );
	ElemTextureRect->SetDoubleAttribute("y", Sprite->Texture.GetRectY() );
	ElemTextureRect->SetDoubleAttribute("width",  Sprite->Texture.GetRectWidth() );
	ElemTextureRect->SetDoubleAttribute("height", Sprite->Texture.GetRectHeight() );
	ElemTexture->LinkEndChild(ElemTextureRect);

	// ElemPos
	TPoint3D Pos(Sprite->Pos);
	if(Pos[0] != 0.0f || Pos[1] != 0.0f || Pos[2] != 0.0f){

		TiXmlElement* ElemPos(new TiXmlElement("Pos"));

		if(Pos[0]!=0.0f){ ElemPos->SetDoubleAttribute("x", Pos[0] ); }
		if(Pos[1]!=0.0f){ ElemPos->SetDoubleAttribute("y", Pos[1] ); }
		if(Pos[2]!=0.0f){ ElemPos->SetDoubleAttribute("z", Pos[2] ); }

		ElemSprite->LinkEndChild(ElemPos);
	}

	// ElemCenter
	TPoint3D Center(Sprite->Center);
	if(Center[0] != 0.0f || Center[1] != 0.0f || Center[2] != 0.0f){

		TiXmlElement* ElemCenter(new TiXmlElement("Center"));

		if(Center[0]!=0.0f){ ElemCenter->SetDoubleAttribute("x", Center[0] ); }
		if(Center[1]!=0.0f){ ElemCenter->SetDoubleAttribute("y", Center[1] ); }
		if(Center[2]!=0.0f){ ElemCenter->SetDoubleAttribute("z", Center[2] ); }

		ElemSprite->LinkEndChild(ElemCenter);
	}
	
	if(Sprite->Angle.Get() != 0.0f ){
		TiXmlElement* ElemAngle(new TiXmlElement("Angle"));
		ElemAngle->SetDoubleAttribute("val", Sprite->Angle.Get() );
		ElemSprite->LinkEndChild(ElemAngle);
	}

	TiXmlElement* ElemSquare(new TiXmlElement("Square"));
	ElemSquare->SetDoubleAttribute("width",  Sprite->Square.GetWidth() );
	ElemSquare->SetDoubleAttribute("height", Sprite->Square.GetHeight() );
	ElemSprite->LinkEndChild(ElemSquare);


	TColor Color(Sprite->Color.Get());
	if(	Color.Get(0) != 1.0f || Color.Get(1) != 1.0f ||
		Color.Get(2) != 1.0f || Color.Get(3) != 1.0f )
	{
		TiXmlElement* ElemColor(new TiXmlElement("Color"));
		if(Color.Get(0) != 1.0f){ ElemColor->SetDoubleAttribute("red"	, Color.Get(0) );}
		if(Color.Get(1) != 1.0f){ ElemColor->SetDoubleAttribute("green"	, Color.Get(1) );}
		if(Color.Get(2) != 1.0f){ ElemColor->SetDoubleAttribute("blue"	, Color.Get(2) );}
		if(Color.Get(3) != 1.0f){ ElemColor->SetDoubleAttribute("alpha"	, Color.Get(3) );}
		ElemSprite->LinkEndChild(ElemColor);
	}

	return ElemSprite;
}
///--------------------------------------------------------------------------------------//
void TXmlSprite::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlSprite::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
