#include "XmlSpriteBank.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlSpriteBank::TXmlSpriteBank():Textures(NULL),Device(NULL)
{
}
///--------------------------------------------------------------------------------------//
TXmlSpriteBank::~TXmlSpriteBank()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTGraphicBank TXmlSpriteBank::ToSpriteBank(TiXmlElement* Elem)
{
	TExceptionGenerator	Exception(L"TXmlSpriteBank: ");

	Exception(Textures	!=NULL, L"in ToSpriteBank() Textures is NULL");
	Exception(Device	!=NULL, L"in ToSpriteBank() Device is NULL");

	TXmlEngineDataDecoder Decoder;
	Decoder.SetExceptionPrefix(L"TXmlSpriteBank: ");

	pTGraphicBank SpriteBank(new TGraphicBank);

	// элемент "Name", его может не быть, поэтому проверка на наличие
	if(Decoder.IsAttrExist(Elem,L"name")){
		SpriteBank->SetName( Decoder.AttrToWStr(Elem,L"name"));
	}

	// элемент View, его может не быть, поэтому проверка на наличие
	if(Decoder.IsAttrExist(Elem,L"view")){
		SpriteBank->SetView( Decoder.AttrToBool(Elem,L"view"));
	}


	// фильтр текстур
	if(Decoder.IsAttrExist(Elem,L"min_filter") ){
		SpriteBank->SetMinFilter(Decoder.AttrToTextureFilter(Elem,L"min_filter"));
	}

	if(Decoder.IsAttrExist(Elem,L"mag_filter") ){
		SpriteBank->SetMagFilter(Decoder.AttrToTextureFilter(Elem,L"mag_filter"));
	}

	if(Decoder.IsAttrExist(Elem,L"filter") ){
		SpriteBank->SetFilter(Decoder.AttrToTextureFilter(Elem,L"filter"));
	}


	// элемент "Pos", элемент не обязателен
	if(Decoder.IsTegExist(Elem,L"Pos")){
		SpriteBank->Pos(Decoder.GetObjectPosWithAlignment(Elem,L"Pos",Device));
	}

	// элемент "Center", элемент не обязателен
	TiXmlElement *ElemCenter(Decoder.GetElem(Elem,L"Center"));
	if(ElemCenter!=NULL){
		SpriteBank->Center(Decoder.ToPoint3D(ElemCenter));
	}

	// элемент "Angle", элемент не обязателен
	TiXmlElement *ElemAngle(Decoder.GetElem(Elem,L"Angle"));
	if(ElemAngle!=NULL){
		SpriteBank->Angle(Decoder.AttrToFloat(ElemAngle,L"val"));
	}

	TXmlSprite XmlSprite;
	XmlSprite.SetTextures(Textures);

	TiXmlElement *ElemSprites(Decoder.GetElem(Elem,L"Sprites"));

	if(ElemSprites!=NULL){
		for(TiXmlElement* ElemSprite = ElemSprites->FirstChildElement();
			ElemSprite != NULL; ElemSprite = ElemSprite->NextSiblingElement())
		{
			SpriteBank->Add(XmlSprite.ToSprite(ElemSprite));
		}
	}

	SpriteBank->Sort();

	if(Device!=NULL){SpriteBank->SetDevice(Device);}

	return SpriteBank;
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlSpriteBank::ToElem(const pTGraphicBank &SpriteBank)
{
	return NULL;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBank::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlSpriteBank::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
