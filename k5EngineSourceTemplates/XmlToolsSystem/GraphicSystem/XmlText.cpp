#include "XmlText.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlText::TXmlText():FontList(NULL),Device(NULL)
{
}
///--------------------------------------------------------------------------------------//
TXmlText::~TXmlText()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTText TXmlText::ToText(TiXmlElement* Elem)
{
	TExceptionGenerator Exception(L"TXmlText: ");

	Exception(FontList!=NULL,L"in ToText(...) FontList is NULL");

	TXmlEngineDataDecoder Decoder;
	Decoder.SetExceptionPrefix(L"TXmlText: ");

	pTText Text(new TText);

	if(Decoder.IsAttrExist(Elem,L"name")){
		Text->SetName(Decoder.AttrToWStr(Elem,L"name"));
	}

	if(Decoder.IsAttrExist(Elem,L"view")){
		Text->SetView(Decoder.AttrToBool(Elem,L"view"));
	}


	// фильтр текстур
	if(Decoder.IsAttrExist(Elem,L"min_filter") ){
		Text->SetMinFilter(Decoder.AttrToTextureFilter(Elem,L"min_filter"));
	}

	if(Decoder.IsAttrExist(Elem,L"mag_filter") ){
		Text->SetMagFilter(Decoder.AttrToTextureFilter(Elem,L"mag_filter"));
	}

	if(Decoder.IsAttrExist(Elem,L"filter") ){
		Text->SetFilter(Decoder.AttrToTextureFilter(Elem,L"filter"));
	}


	Text->SetFont(FontList->Get(Decoder.AttrToWStr(Elem,L"font")));

	if(Decoder.IsAttrExist(Elem,L"alignment")){
		wstring Alignment = Decoder.AttrToWStr(Elem,L"alignment");
		if(Alignment == L"left")	{Text->SetAlignment(EN_A_Left);}
		if(Alignment == L"center")	{Text->SetAlignment(EN_A_Center);}
		if(Alignment == L"right")	{Text->SetAlignment(EN_A_Right);}
	}

	// элемент "Pos", элемент не обязателен
	if(Decoder.IsTegExist(Elem,L"Pos")){
		Text->Pos(Decoder.GetObjectPosWithAlignment(Elem,L"Pos",Device));
	}

	if(Decoder.IsTegExist(Elem,L"Color")){
		Text->Color(Decoder.ToColor(Elem,L"Color"));
	}

	TiXmlElement* ElemText(Decoder.GetElem(Elem,L"Text"));

	if(ElemText!=NULL){
		int LineCount(0);
		wstring TextLine;
		for(TiXmlElement* ElemLine = ElemText->FirstChildElement();
			ElemLine != NULL; ElemLine = ElemLine->NextSiblingElement())
		{
			if(LineCount>0){TextLine += L"\n";}
			TextLine += Decoder.ToWStr(ElemLine);
			LineCount++;
		}
		Text->Set(TextLine);
	}

	if(Device!= NULL){Text->SetDevice(Device);}

	return Text;
}
///--------------------------------------------------------------------------------------//
TiXmlElement* TXmlText::ToElem(const pTText &Text)
{
	return NULL;
}
///--------------------------------------------------------------------------------------//
void TXmlText::SetFontList(const pTFontList &Val)
{
	FontList = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlText::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
