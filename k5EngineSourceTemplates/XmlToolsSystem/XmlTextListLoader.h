///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#ifndef XMLTEXTLISTLOADER_H_INCLUDED
#define XMLTEXTLISTLOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseLoader.h"
#include "GraphicSystem/XmlText.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlTextListLoader:public TXmlBaseLoader
{
	protected:
		TBaseDevice	*Device;
		TTextList 	*TextList;
		TFontList	*FontList;
		TXmlBaseDataDecoder XmlDecoder;
	protected:
		inline void ParseElement(TiXmlElement* Elem);

		void ToRun(TiXmlElement *Teg);
		void ToRun(const wstring &File);
	public:
		TXmlTextListLoader();
		TXmlTextListLoader(TBaseDevice *Dev, TTextList *Text, TFontList *Font);
		virtual ~TXmlTextListLoader();

		void SetParams(TBaseDevice *Dev, TTextList *Text, TFontList *Font);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLTEXTLISTLOADER_H_INCLUDED
