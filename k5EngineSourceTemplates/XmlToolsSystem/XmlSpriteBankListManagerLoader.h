///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITEBANKLISTMANAGERLOADER_H_INCLUDED
#define XMLSPRITEBANKLISTMANAGERLOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "XmlSpriteBankListLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpriteBankListManagerLoader:public TXmlBaseLoader
{
	private:
		TExceptionGenerator Exception;
		pTGraphicBankListManager GraphicBanks;
		TTextureListManager *Textures;
	private:
		inline void ListFromXmlElement(TiXmlElement *Elem);

		void ToRun(TiXmlElement *Teg);
		void ToRun(const wstring &File);
	public:
		TXmlSpriteBankListManagerLoader();
		TXmlSpriteBankListManagerLoader(const pTGraphicBankListManager &Spr,
										const pTTextureListManager &Tex);
		virtual ~TXmlSpriteBankListManagerLoader();

		void SetSpriteBanks(const pTGraphicBankListManager &Val);
		void SetTextures(const pTTextureListManager &Val);
		void Set(const pTGraphicBankListManager &Spr,
				 const pTTextureListManager &Tex);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // SPRITELISTMANAGERLOADER_H_INCLUDED
