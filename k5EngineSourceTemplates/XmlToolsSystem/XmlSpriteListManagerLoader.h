///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLSPRITELISTMANAGERLOADER_H_INCLUDED
#define XMLSPRITELISTMANAGERLOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "XmlSpriteListLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlSpriteListManagerLoader:public TXmlBaseLoader
{
	private:
		pTSpriteListManager  Sprites;
		pTTextureListManager Textures;
	private:
		inline void ListFromXmlElement(TiXmlElement *Elem);

		void ToRun(TiXmlElement *Teg);
		void ToRun(const wstring &File);
	public:
		TXmlSpriteListManagerLoader();
		TXmlSpriteListManagerLoader(const pTSpriteListManager &Spr,
									const pTTextureListManager &Tex);
		virtual ~TXmlSpriteListManagerLoader();

		void SetSprites(const pTSpriteListManager &Val);
		void SetTextures(const pTTextureListManager &Val);
		void Set(const pTSpriteListManager &Spr,const pTTextureListManager &Tex);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // SPRITELISTMANAGERLOADER_H_INCLUDED
