///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLKINEMATICSFORCESDECODER_H_INCLUDED
#define XMLKINEMATICSFORCESDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "../../Base/XmlBaseEventActionDecoder.h"
#include "STKinematicsSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlKinematicsForcesDecoder: public TXmlBaseDataDecoder
{
	protected:
		TiXmlElement *XmlElem;
	protected:
		inline TKinematicsBaseForce* ToDirectedForce();
		inline TKinematicsBaseForce* ToGlobalRandDirectedForce();
		inline TKinematicsBaseForce* ToGlobalStaticForce();
		inline TKinematicsBaseForce* ToSpriteAngleSpeedRandRange();
		inline TKinematicsBaseForce* ToSpriteColorSpeedRandRange();

		int CheckType(const wstring &Type);

		inline TKinematicsBaseForce* Decode(TiXmlElement *Val);
	public:
		TXmlKinematicsForcesDecoder();
		virtual ~TXmlKinematicsForcesDecoder();

		TKinematicsBaseForce* operator()(TiXmlElement *Val);
		TKinematicsBaseForce* Run(TiXmlElement *Val);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLPARTICLEFORCEDECODER_H_INCLUDED
