///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMUIPANELGENERATORDECODER_H_INCLUDED
#define XMUIPANELGENERATORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseActionDecoder.h"
#include "STGraphicObjectGenerators.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlUIPanelGeneratorDecoder:public TXmlBaseActionDecoder
{
	public:
		TXmlUIPanelGeneratorDecoder();
		virtual ~TXmlUIPanelGeneratorDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetSpriteBanks(const pTSpriteBankListManager &Val);
};

typedef TXmlUIPanelGeneratorDecoder* pTXmlUIPanelGeneratorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMUIPANELGENERATORDECODER_H_INCLUDED
