#include "XmlUITextPanelGeneratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlUITextPanelGeneratorDecoder::TXmlUITextPanelGeneratorDecoder():
								TXmlBaseActionDecoder(),Texts(NULL),Fonts(NULL),
								SpriteBanks(NULL),Textures(NULL)
{
	Exception.SetPrefix(L"TXmlUITextPanelGeneratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlUITextPanelGeneratorDecoder: ");

	ActionTypeName = L"UITextPanelGenerator";
}
///--------------------------------------------------------------------------------------//
TXmlUITextPanelGeneratorDecoder::~TXmlUITextPanelGeneratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlUITextPanelGeneratorDecoder::Construct()
{
	return new TUITextPanelGenerator;
}
///--------------------------------------------------------------------------------------//
void TXmlUITextPanelGeneratorDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	Exception(Texts		 !=NULL, L"in Init(...) Texts is NULL");
	Exception(Fonts		 !=NULL, L"in Init(...) Fonts is NULL");
	Exception(SpriteBanks!=NULL, L"in Init(...) SpriteBanks is NULL");
	Exception(Textures	 !=NULL, L"in Init(...) Textures is NULL");

	pTUITextPanelGenerator Obj(static_cast<pTUITextPanelGenerator>(Action));

	Obj->SetTexts(Texts);
	Obj->SetFonts(Fonts);
	Obj->SetSpriteBanks(SpriteBanks);

	if(Decoder.IsAttrExist(Elem,L"repeat")){
		Obj->SetRepeat(Decoder.AttrToBool(Elem,L"repeat"));
	}

	if(Decoder.IsAttrExist(Elem,L"view")){
		Obj->SetView(Decoder.AttrToBool(Elem,L"view"));
	}

	if(Decoder.IsAttrExist(Elem,L"border")){
		Obj->SetBorder(Decoder.AttrToFloat(Elem,L"border"));
	}

	TiXmlElement* ElemIndent = Decoder.GetElem(Elem,L"Indent");
	if(ElemIndent!=NULL){
		Obj->SetIndent(	Decoder.AttrToFloat(ElemIndent,L"left"),
						Decoder.AttrToFloat(ElemIndent,L"right"),
						Decoder.AttrToFloat(ElemIndent,L"top"),
						Decoder.AttrToFloat(ElemIndent,L"bottom"));
	}

	Obj->SetTemplate(Decoder.ToSpriteBank(Decoder.GetElem(Elem,L"Template")));

	TiXmlElement* ElemSpriteBank = Decoder.GetElem(Elem,L"SpriteBank");
	Exception(	ElemSpriteBank!=NULL, L"in Init(...) ElemSpriteBank is NULL");

	wstring SBListName	(Decoder.AttrToWStr(ElemSpriteBank,L"list"));
	wstring SBName		(Decoder.AttrToWStr(ElemSpriteBank,L"name"));

	Obj->SetSpriteBankNames(SBListName,SBName);
	Obj->SetPos(Decoder.ToPoint3D		(Decoder.GetElem(Elem, L"Pos")));
	Obj->SetPanelColor(Decoder.ToColor	(Decoder.GetElem(Elem, L"PanelColor")));
	Obj->SetTextColor(Decoder.ToColor	(Decoder.GetElem(Elem, L"TextColor")));

	TiXmlElement* ElemText = Decoder.GetElem(Elem,L"Text");

	TiXmlElement* ElemPanelSprites = Decoder.GetElem(Elem,L"PanelAditionalSprites");
	if(ElemPanelSprites!=NULL){
		TXmlSpriteListLoader SpriteListLoader(Obj->GetAditionSpritesPtr(),Textures);
		SpriteListLoader.Run(ElemPanelSprites);
	}

	wstring Alignment = Decoder.AttrToWStr(ElemText,L"alignment");
	/*if(Alignment == L"left")	{Obj->SetTextAlignment(EN_TA_Left);}
	if(Alignment == L"center")	{Obj->SetTextAlignment(EN_TA_Center);}
	if(Alignment == L"right")	{Obj->SetTextAlignment(EN_TA_Right);}*/

	Obj->SetFontName(	Decoder.AttrToWStr(ElemText,L"font"));
	Obj->SetTextNames(	Decoder.AttrToWStr(ElemText,L"list"),
						Decoder.AttrToWStr(ElemText,L"name"));

	if(ElemText!=NULL){
		int LineCount(0);
		wstring TextLines;
		for(TiXmlElement* ElemLine = ElemText->FirstChildElement();
			ElemLine != NULL; ElemLine = ElemLine->NextSiblingElement())
		{
			if(LineCount>0){TextLines += L"\n";}
			TextLines += Decoder.ToWStr(ElemLine);
			LineCount++;
		}
		Obj->SetText(TextLines);
	}

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlUITextPanelGeneratorDecoder::SetTexts(const pTTextListManager &Val)
{
	Texts = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlUITextPanelGeneratorDecoder::SetFonts(const pTFontList &Val)
{
	Fonts = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlUITextPanelGeneratorDecoder::SetSpriteBanks
											(const pTSpriteBankListManager &Val)
{
	SpriteBanks = Val;
	Decoder.SetSpriteBanks(SpriteBanks);
}
///--------------------------------------------------------------------------------------//
void TXmlUITextPanelGeneratorDecoder::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
