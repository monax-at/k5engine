///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef TXMLUITEXTPANELGENERATORDECODER_H_INCLUDED
#define TXMLUITEXTPANELGENERATORDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "STGraphicObjectGenerators.h"
#include "../XmlSpriteListLoader.h"
#include "Base/XmlBaseActionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlUITextPanelGeneratorDecoder:public TXmlBaseActionDecoder
{
	protected:
		pTTextListManager Texts;
		pTFontList Fonts;
		pTSpriteBankListManager SpriteBanks;
		pTTextureListManager Textures;
	public:
		TXmlUITextPanelGeneratorDecoder();
		virtual ~TXmlUITextPanelGeneratorDecoder();

		pTBaseAction Construct();
		void Init(const pTBaseAction &Action,TiXmlElement* Elem);

		void SetTexts(const pTTextListManager &Val);
		void SetFonts(const pTFontList &Val);
		void SetSpriteBanks(const pTSpriteBankListManager &Val);
		void SetTextures(const pTTextureListManager &Val);

};

typedef TXmlUITextPanelGeneratorDecoder* pTXmlUITextPanelGeneratorDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // TXMLUITEXTPANELGENERATORDECODER_H_INCLUDED
