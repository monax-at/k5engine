#include "XmlUIPanelGeneratorDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlUIPanelGeneratorDecoder::TXmlUIPanelGeneratorDecoder():TXmlBaseActionDecoder()
{
	Exception.SetPrefix(L"TXmlUIPanelGeneratorDecoder: ");
	Decoder.SetExceptionPrefix(L"TXmlUIPanelGeneratorDecoder: ");

	ActionTypeName = L"UIPanelGenerator";
}
///--------------------------------------------------------------------------------------//
TXmlUIPanelGeneratorDecoder::~TXmlUIPanelGeneratorDecoder()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseAction TXmlUIPanelGeneratorDecoder::Construct()
{
	return new TUIPanelGenerator;
}
///--------------------------------------------------------------------------------------//
void TXmlUIPanelGeneratorDecoder::Init(	const pTBaseAction &Action,
											TiXmlElement* Elem)
{
	pTUIPanelGenerator Obj(static_cast<pTUIPanelGenerator>(Action));

	if(Decoder.IsAttrExist(Elem,L"repeat")){
		Obj->SetRepeat(Decoder.AttrToBool(Elem,L"repeat"));
	}

	Obj->SetSizes(	Decoder.AttrToBool(Elem,L"width"),
					Decoder.AttrToBool(Elem,L"height"));

	Obj->Set		(Decoder.ToSpriteBank( Decoder.GetElem(Elem,L"SpriteBank") ));
	Obj->SetTemplate(Decoder.ToSpriteBank( Decoder.GetElem(Elem,L"Template") ));

	SetDefaultParams(Obj,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlUIPanelGeneratorDecoder::SetSpriteBanks(const pTSpriteBankListManager &Val)
{
	Decoder.SetSpriteBanks(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
