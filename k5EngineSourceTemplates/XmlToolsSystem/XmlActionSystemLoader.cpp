#include "XmlActionSystemLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlActionSystemLoader::TXmlActionSystemLoader():TXmlBaseLoader(),
								ActionsDecoder(NULL),EventActionsDecoder(NULL),
								Actions(NULL),EventActions(NULL),
								ValidateIfSetActions(true),ValidateIfSetEventActions(true)
{
	Exception.SetPrefix(L"TXmlActionSystemLoader: ");
	Decoder.SetExceptionPrefix(L"TXmlActionSystemLoader: ");
}
///--------------------------------------------------------------------------------------//
TXmlActionSystemLoader::~TXmlActionSystemLoader()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::ValidateExternalData(const wstring &FunkName)
{
	if(ValidateIfSetActions){
		Exception(	ActionsDecoder!=NULL,L"in "+FunkName+L" ActionsDecoder is NULL");
		Exception(	Actions!=NULL,		L"in " + FunkName + L" Actions is NULL");
	}

	if(ValidateIfSetEventActions){
		Exception(	EventActionsDecoder!=NULL,
					L"in " + FunkName + L" EventActionsDecoder is NULL");

		Exception(	EventActions!=NULL,	L"in " + FunkName + L" EventActions is NULL");
	}
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::CreateActionList(TiXmlElement *Elem)
{
	// создание и добавление нового списка дейсвий
	pTActionList List = new TActionList;

	List->SetName(Decoder.AttrToWStr(Elem,L"name"));

	if(Decoder.IsAttrExist(Elem,L"active")){
		List->SetActive(Decoder.AttrToBool(Elem,L"active"));
	}

	Actions->Add(List);
	ActionsDecoder->CreateList(List,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::CreateEventActionList(TiXmlElement *Elem)
{
	// создание и добавление нового списка дейсвий
	pTEventActionList List = new TEventActionList;

	List->SetName(Decoder.AttrToWStr(Elem,L"name"));

	if(Decoder.IsAttrExist(Elem,L"active")){
		List->SetActive(Decoder.AttrToBool(Elem,L"active"));
	}

	EventActions->Add(List);
	EventActionsDecoder->CreateList(List,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::InitActionList(TiXmlElement *Elem)
{
	pTActionList List(Actions->Get(Decoder.AttrToWStr(Elem,L"name")));
	ActionsDecoder->InitList(List,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::InitEventActionList(TiXmlElement *Elem)
{
	pTEventActionList List(EventActions->Get(Decoder.AttrToWStr(Elem,L"name")));
	EventActionsDecoder->InitList(List,Elem);
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::CreateList(TiXmlElement *Elem)
{
	wstring Type(Decoder.AttrToWStr(Elem,L"type"));

	if(Type == L"Actions")		{CreateActionList(Elem);}
	if(Type == L"EventActions") {CreateEventActionList(Elem);}
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::InitList(TiXmlElement *Elem)
{
	wstring Type(Decoder.AttrToWStr(Elem,L"type"));

	if(Type == L"Actions")		{InitActionList(Elem);}
	if(Type == L"EventActions") {InitEventActionList(Elem);}
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::CreateLists(TiXmlElement *Elem)
{
	for(TiXmlElement* ElemList = Elem->FirstChildElement();
		ElemList != NULL; ElemList = ElemList->NextSiblingElement())
	{
		CreateList(ElemList);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::InitLists(TiXmlElement *Elem)
{
	for(TiXmlElement* ElemList = Elem->FirstChildElement();
		ElemList != NULL; ElemList = ElemList->NextSiblingElement())
	{
		InitList(ElemList);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::ToRun(TiXmlElement *Teg)
{
	ValidateExternalData(L"ToRun(Teg)");
	CreateLists(Teg);
	InitLists(Teg);
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::ToRun(const wstring &File)
{
	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in ToRun(File) can't open file: "+File);

	TiXmlHandle Handle(&Doc);

	TiXmlElement* ElemData = Handle.FirstChildElement().Element();
	Exception(ElemData!=NULL,L"in ToRun(File) teg Data must be set");

	ToRun(ElemData);

	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::ManyFiles(TiXmlElement *Teg)
{
	ValidateExternalData(L"ManyFiles(Teg)");

	if(Teg == NULL){return;}

	// создание всех действий из всех указанных файлов
	for(TiXmlElement *ElemFile = Teg->FirstChildElement();
		ElemFile != NULL; ElemFile = ElemFile->NextSiblingElement())
	{
		wstring FileName = Decoder.AttrToWStr(ElemFile,L"name");

		TiXmlDocument Doc(WStrToStr(FileName).c_str());
		Exception(Doc.LoadFile(),L"in ManyFiles(Teg) can't open file: "+FileName);

		TiXmlHandle Handle(&Doc);

		TiXmlElement* ElemData = Handle.FirstChildElement().Element();
		Exception(ElemData!=NULL,L"in ManyFiles(Teg) teg Data must be set");

		CreateLists(ElemData);

		Doc.Clear();
	}

	// инициализация всех действий из всех указанных файлов
	for(TiXmlElement *ElemFile = Teg->FirstChildElement();
		ElemFile != NULL; ElemFile = ElemFile->NextSiblingElement())
	{
		wstring FileName = Decoder.AttrToWStr(ElemFile,L"name");

		TiXmlDocument Doc(WStrToStr(FileName).c_str());
		Exception(Doc.LoadFile(),L"in ManyFiles(Teg) can't open file: "+FileName);

		TiXmlHandle Handle(&Doc);

		TiXmlElement* ElemData = Handle.FirstChildElement().Element();
		Exception(ElemData!=NULL,L"in ManyFiles(Teg) teg Data must be set");

		InitLists(ElemData);

		Doc.Clear();
	}
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::ManyFiles(const wstring &File)
{
	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in ManyFiles(File) can't open file: "+File);

	TiXmlHandle Handle(&Doc);

	TiXmlElement* ElemData = Handle.FirstChildElement().Element();
	Exception(ElemData!=NULL,L"in ManyFiles(File) teg Data must be set");

	ManyFiles(ElemData);

	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::ConstructFromManyFiles(TiXmlElement *Teg)
{
	ValidateExternalData(L"ConstructFromManyFiles(Teg)");

	if(Teg == NULL){return;}

	// создание всех действий из всех указанных файлов
	for(TiXmlElement *ElemFile = Teg->FirstChildElement();
		ElemFile != NULL; ElemFile = ElemFile->NextSiblingElement())
	{
		wstring FileName = Decoder.AttrToWStr(ElemFile,L"name");

		TiXmlDocument Doc(WStrToStr(FileName).c_str());
		Exception(Doc.LoadFile(),L"in ManyFiles(Teg) can't open file: "+FileName);

		TiXmlHandle Handle(&Doc);

		TiXmlElement* ElemData = Handle.FirstChildElement().Element();
		Exception(ElemData!=NULL,L"in ManyFiles(Teg) teg Data must be set");

		CreateLists(ElemData);

		Doc.Clear();
	}
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::ConstructFromManyFiles(const wstring &File)
{
	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in ManyFiles(File) can't open file: "+File);

	TiXmlHandle Handle(&Doc);

	TiXmlElement* ElemData = Handle.FirstChildElement().Element();
	Exception(ElemData!=NULL,L"in ManyFiles(File) teg Data must be set");

	ConstructFromManyFiles(ElemData);

	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::InitFromManyFiles(TiXmlElement *Teg)
{
	ValidateExternalData(L"InitFromManyFiles(Teg)");

	if(Teg == NULL){return;}

	// инициализация всех действий из всех указанных файлов
	for(TiXmlElement *ElemFile = Teg->FirstChildElement();
		ElemFile != NULL; ElemFile = ElemFile->NextSiblingElement())
	{
		wstring FileName = Decoder.AttrToWStr(ElemFile,L"name");

		TiXmlDocument Doc(WStrToStr(FileName).c_str());
		Exception(Doc.LoadFile(),L"in InitFromManyFiles(Teg) can't open file: "+FileName);

		TiXmlHandle Handle(&Doc);

		TiXmlElement* ElemData = Handle.FirstChildElement().Element();
		Exception(ElemData!=NULL,L"in InitFromManyFiles(Teg) teg Data must be set");

		InitLists(ElemData);

		Doc.Clear();
	}
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::InitFromManyFiles(const wstring &File)
{
	TiXmlDocument Doc(WStrToStr(File).c_str());
	Exception(Doc.LoadFile(),L"in InitFromManyFiles(File) can't open file: "+File);

	TiXmlHandle Handle(&Doc);

	TiXmlElement* ElemData = Handle.FirstChildElement().Element();
	Exception(ElemData!=NULL,L"in InitFromManyFiles(File) teg Data must be set");

	InitFromManyFiles(ElemData);

	Doc.Clear();
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::SetActionsDecoder(const pTXmlActionsDecoder &Val)
{
	ActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::SetEventActionsDecoder
											(const pTXmlEventActionsDecoder &Val)
{
	EventActionsDecoder = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::SetActions(const pTActionListManager &Val)
{
	Actions = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::SetEventActions(const pTEventActionListManager &Val)
{
	EventActions = Val;
}
///--------------------------------------------------------------------------------------//
void TXmlActionSystemLoader::SetValidateFlags(	const bool &bActions,
												const bool &bEventActions)
{
	ValidateIfSetActions = bActions;
	ValidateIfSetEventActions = bEventActions;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
