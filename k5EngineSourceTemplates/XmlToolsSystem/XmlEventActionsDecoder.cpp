#include "XmlEventActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TXmlEventActionsDecoder::TXmlEventActionsDecoder():
						 Exception(L"TXmlEventActionsDecoder: ")
{
	Decoder.SetExceptionPrefix(L"TXmlEventActionsDecoder: ");
}
///--------------------------------------------------------------------------------------//
TXmlEventActionsDecoder::~TXmlEventActionsDecoder()
{
	Clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
pTBaseEventAction TXmlEventActionsDecoder::Create(TiXmlElement* Elem)
{
	wstring Type = Decoder.AttrToWStr(Elem,L"type");
	int Size = Elems.size();

	for(int i=0;i<Size;i++){
		if(Elems[i]->CheckActionType(Type)){ return Elems[i]->Construct(); }
	}

	Exception(L"in Create(...) unknown action type");
	return NULL;
}
///--------------------------------------------------------------------------------------//
void TXmlEventActionsDecoder::Init(	const pTBaseEventAction &Action,
									TiXmlElement* Elem)
{
	wstring Type = Decoder.AttrToWStr(Elem,L"type");
	int Size = Elems.size();

	for(int i=0;i<Size;i++){
		if(Elems[i]->CheckActionType(Type)){
			Elems[i]->Init(Action,Elem);
			return;
		}
	}

	Exception(L"in Init(...) unknown action type");
}
///--------------------------------------------------------------------------------------//
void TXmlEventActionsDecoder::CreateList(const pTEventActionList &List,
										 TiXmlElement* Elem)
{
	// создание в списке действий
	for(TiXmlElement* ElemAction = Elem->FirstChildElement();
		ElemAction != NULL; ElemAction = ElemAction->NextSiblingElement())
	{
		pTBaseEventAction Action(Create(ElemAction));

		if(Decoder.IsAttrExist(ElemAction,L"name")){
			Action->SetName(Decoder.AttrToWStr(ElemAction,L"name"));
		}

		List->Add(Action);
	}
}
///--------------------------------------------------------------------------------------//
void TXmlEventActionsDecoder::InitList(	const pTEventActionList &List,
										TiXmlElement* Elem)
{
	int ActionCounter(0);

	for(TiXmlElement* ElemAction = Elem->FirstChildElement();
		ElemAction != NULL; ElemAction = ElemAction->NextSiblingElement())
	{
		Init(List->Get(ActionCounter),ElemAction);
		ActionCounter++;
	}
}
///--------------------------------------------------------------------------------------//
void TXmlEventActionsDecoder::Add(const pTXmlBaseEventActionDecoder &Val)
{
	Elems.push_back(Val);
}
///--------------------------------------------------------------------------------------//
void TXmlEventActionsDecoder::Clear()
{
	int Size = Elems.size();
	for(int i=0;i<Size;i++){ delete Elems[i]; }
	Elems.clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
