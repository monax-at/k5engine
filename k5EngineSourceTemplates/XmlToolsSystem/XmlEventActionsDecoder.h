///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLEVENTACTIONSDECODER_H_INCLUDED
#define XMLEVENTACTIONSDECODER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"
#include "Base/XmlBaseDataDecoder.h"
#include "Base/XmlBaseEventActionDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlEventActionsDecoder
{
	protected:
		TExceptionGenerator Exception;
		TXmlBaseDataDecoder Decoder;
		vector<pTXmlBaseEventActionDecoder> Elems;
	public:
		TXmlEventActionsDecoder();
		~TXmlEventActionsDecoder();

		pTBaseEventAction Create(TiXmlElement* Elem);
		void Init(const pTBaseEventAction &Action, TiXmlElement* Elem);

		void CreateList(const pTEventActionList &List,TiXmlElement* Elem);
		void InitList(const pTEventActionList &List,TiXmlElement* Elem);

		void Add(const pTXmlBaseEventActionDecoder &Val);
		void Clear();
};

typedef TXmlEventActionsDecoder* pTXmlEventActionsDecoder;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLEVENTACTIONSDECODER_H_INCLUDED
