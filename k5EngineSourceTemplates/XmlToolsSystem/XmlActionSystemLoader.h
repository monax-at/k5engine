///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLACTIONSYSTEMLOADER_H_INCLUDED
#define XMLACTIONSYSTEMLOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseLoader.h"
#include "XmlActionsDecoder.h"
#include "XmlEventActionsDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlActionSystemLoader:public TXmlBaseLoader
{
	protected:
		TXmlEngineDataDecoder Decoder;

		pTXmlActionsDecoder 	 ActionsDecoder;
		pTXmlEventActionsDecoder EventActionsDecoder;

		pTActionListManager 	 Actions;
		pTEventActionListManager EventActions;

		bool ValidateIfSetActions;
		bool ValidateIfSetEventActions;
	protected:
		inline void ValidateExternalData(const wstring &FunkName);

		inline void CreateActionList(TiXmlElement *Elem);
		inline void CreateEventActionList(TiXmlElement *Elem);

		inline void InitActionList(TiXmlElement *Elem);
		inline void InitEventActionList(TiXmlElement *Elem);

		inline void CreateList(TiXmlElement *Elem);
		inline void InitList(TiXmlElement *Elem);

		inline void CreateLists(TiXmlElement *Elem);
		inline void InitLists(TiXmlElement *Elem);

		void ToRun(TiXmlElement *Teg);
		void ToRun(const wstring &File);
	public:
		TXmlActionSystemLoader();
		virtual ~TXmlActionSystemLoader();

		void ManyFiles(TiXmlElement *Teg);
		void ManyFiles(const wstring &File);

		void ConstructFromManyFiles(TiXmlElement *Teg);
		void ConstructFromManyFiles(const wstring &File);

		void InitFromManyFiles(TiXmlElement *Teg);
		void InitFromManyFiles(const wstring &File);

		void SetActionsDecoder(const pTXmlActionsDecoder &Val);
		void SetEventActionsDecoder(const pTXmlEventActionsDecoder &Val);
		void SetActions(const pTActionListManager &Val);
		void SetEventActions(const pTEventActionListManager &Val);

		void SetValidateFlags(const bool &bActions=true, const bool &bEventActions=true);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLACTIONSYSTEMLOADER_H_INCLUDED
