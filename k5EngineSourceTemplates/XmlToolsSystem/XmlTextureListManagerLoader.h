///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLTEXTURELISTMANAGERLOADER_H_INCLUDED
#define XMLTEXTURELISTMANAGERLOADER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "DevInitWorker.h"
#include "Globals.h"
#include "DevilCanvas.h"
#include "DevilCanvasWorker.h"
///--------------------------------------------------------------------------------------//
#include "Base/XmlBaseLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlTextureListManagerLoader:public TXmlBaseLoader
{
	protected:
		TExceptionGenerator  Exception;
		TXmlBaseDataDecoder  XmlDecoder;

		pTBaseDevice		 Device;
		pTTextureListManager Textures;

		TDevInitWorker   DevInitWorker;
	protected:
		inline void LoadTextureFromFile	(const pTTextureList &List,
										 TiXmlElement *Elem);

		inline void LoadMaskFromFile	(const pTTextureList &List,
										 TiXmlElement *Elem, int MaskType);

		inline void LoadMaskFromTexture	(const pTTextureList &List,
										 TiXmlElement *Elem, int MaskType);

		inline void ParseTexture	(const pTTextureList &List,TiXmlElement *Elem);
		inline void ParseTextures	(const pTTextureList &List,TiXmlElement *Elem);

		inline void CreateLists		(TiXmlElement *Elem);
		inline void CreateTextures	(TiXmlElement *Elem);

		inline int GetPowTow(const int &Val);
		int GetQuadSize(const int &WidthVal,const int &HeightVal);

		void ToRun(TiXmlElement *Teg);
		void ToRun(const wstring &File);
	public:
		TXmlTextureListManagerLoader();
		TXmlTextureListManagerLoader(const pTBaseDevice &Dev,
									 const pTTextureListManager &Tex);
		virtual ~TXmlTextureListManagerLoader();

		void SetDevice(const pTBaseDevice &Val);
		void SetTextures(const pTTextureListManager &Val);
};
typedef TXmlTextureListManagerLoader *pTXmlTextureListManagerLoader;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // CLASS_GLTEXTURELISTMANAGERLOADER_H_INCLUDED
