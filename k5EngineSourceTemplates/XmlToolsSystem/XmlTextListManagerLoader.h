///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XmlTextLineListManagerLoader_H_INCLUDED
#define XmlTextLineListManagerLoader_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "XmlTextListLoader.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlTextListManagerLoader:public TXmlBaseLoader
{
	private:
		pTTextListManager Texts;
		pTFontList 		  Fonts;
	private:
		inline void ListFromXmlElement(TiXmlElement *Elem);

		void ToRun(TiXmlElement *Teg);
		void ToRun(const wstring &File);
	public:
		TXmlTextListManagerLoader();
		TXmlTextListManagerLoader(const pTTextListManager &Tl,const pTFontList &F);
		virtual ~TXmlTextListManagerLoader();

		void SetTexts(const pTTextListManager &Val);
		void SetFonts(const pTFontList &Val);
		void Set(const pTTextListManager &Tl,const pTFontList &F);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // XMLTEXTAREALISTMANAGERLOADER_H_INCLUDED
