///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef XMLDATAFILEWORKER_H_INCLUDED
#define XMLDATAFILEWORKER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Base/XmlEngineDataDecoder.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TXmlDataFileWorker//: public TDataFileWorker
{
	private:
		wstring FileName;

	public:
		TXmlDataFileWorker();
		virtual ~TXmlDataFileWorker();

		void SaveToXmlFile();
		void LoadFromXmlFile();

		void SaveToXmlFile(const wstring &Value);
		void LoadFromXmlFile(const wstring &Value);
};

typedef TXmlDataFileWorker* pTXmlDataFileWorker;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // CONFIGWORKER_H_INCLUDED
