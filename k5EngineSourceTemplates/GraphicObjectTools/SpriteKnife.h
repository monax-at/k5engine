///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef SPRITEKNIFE_H_INCLUDED
#define SPRITEKNIFE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TSpriteKnife:public TBaseAction
{
	protected:
		TExceptionGenerator Exception;

		pTSpriteList List;

		int CellSize;

		pTSprite Sprite;

		float TextModifWidth;
		float TextModifHeight;

		int CellsByWidth;
		int CellsByHeight;

		float LastCellWidth;
		float LastCellHeight;

		bool MeshNoiseFlag;
		bool UseSpritePosShistFlag;
	protected:
		inline void FillLastRow	(const TPoint3D &StartPos);
		inline void FillRow		(const TPoint3D &StartPos, const int &RowIter);

		inline void GenerateSprites();
		inline void AddMeshNoise();

		void ToRun();
	public:
		TSpriteKnife();
		virtual ~TSpriteKnife();

		void Set(const pTSpriteList  &Val);
		void Set(const pTGraphicBank &Val);

		void SetCellSize(const int &Val);
		void SetSprite(TSprite* Val);

		void SetMeshNoiseFlag(const bool &Val);
		bool GetMeshNoiseFlag() const;

		void SetUseSpritePosShistFlag(const bool &Val);
		bool GetUseSpritePosShistFlag();
};

typedef TSpriteKnife* pTSpriteKnife;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // SPRITEKNIFE_H_INCLUDED
