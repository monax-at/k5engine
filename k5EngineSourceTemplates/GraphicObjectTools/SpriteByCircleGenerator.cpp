#include "SpriteByCircleGenerator.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TSpriteByCircleGenerator::TSpriteByCircleGenerator():TBaseAction(),
					Exception(L"TSpriteByCircleGenerator:"),
					List(NULL), NumSprites(0), MinRadius(0.0f), MaxRadius(0.0f),
					FloatingSpriteCenter(false), RandPosZ(false),
					GenRandPos(true), StartAngle(0.0f), AngleAlongPathFlag(false)
{
}
///--------------------------------------------------------------------------------------//
TSpriteByCircleGenerator::~TSpriteByCircleGenerator()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::ToRun()
{
	Exception(NumSprites >0,L"in ToRun() NumSprites is 0");
	Exception(List !=NULL, L"in ToRun() List is NULL");


	float PosX(Pos.GetX());
	float PosY(Pos.GetY());
	float PosZ(Pos.GetZ());

	float ShiftPosZ(0.0f);

	if(RandPosZ){ ShiftPosZ = 1.0f/((float)NumSprites);}

	float SpriteByAngle(360.0f/((float)NumSprites));

	for(int i=0;i<NumSprites;i++){

		TSprite Sprite;
		Sprite = SpriteTemplate;

		int Radius(GenRandRangeValue((int)(MinRadius),(int)(MaxRadius)));

		float Angle(StartAngle);
		if(GenRandPos){ Angle = (float)GenRandValue(360); }
		else		  { Angle  = SpriteByAngle*i; }

		float ShiftX = ((float)Radius)*cos(DegToRad(Angle));
		float ShiftY = ((float)Radius)*sin(DegToRad(Angle));

		if(FloatingSpriteCenter){
			Sprite.Pos(PosX, PosY);
			Sprite.Center(ShiftX, ShiftY);
		}
		else{
			Sprite.Pos(PosX+ShiftX, PosY+ShiftY);
			if(AngleAlongPathFlag){ Sprite.Angle(Angle-90.0f); }
		}

		if(RandPosZ){Sprite.Pos.SetZ(PosZ + ShiftPosZ*i);}
		else		{Sprite.Pos.SetZ(PosZ);}

		List->Add(Sprite);
	}

	List->Sort();
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetTemplateSprite(const TSprite &Val)
{
	SpriteTemplate = Val;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetTemplateSprite(const pTSprite &Val)
{
	SpriteTemplate = *Val;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::Set(const pTSpriteList &Val)
{
	List = Val;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::Set(const pTGraphicBank &Val)
{
	List = Val->GetSpritesPtr();
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetNumSprites(const int &Val)
{
	NumSprites = Val;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetRadius(const float &Val)
{
	MinRadius = Val;
	MaxRadius = Val;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetRadius(const float &Min,const float &Max)
{
	MinRadius = Min;
	MaxRadius = Max;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetMinRadius(const float &Val)
{
	MinRadius = Val;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetMaxRadius(const float &Val)
{
	MaxRadius = Val;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetPos(const float &Val)
{
	Pos(Val);
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetPos(const float &X,const float &Y)
{
	Pos(X,Y);
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetPos(const float &X,const float &Y,const float &Z)
{
	Pos(X,Y,Z);
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetPos(const TPoint2D &Val)
{
	Pos(Val.GetX(),Val.GetY());
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetPos(const TPoint3D &Val)
{
	Pos(Val);
}
///--------------------------------------------------------------------------------------//
TPoint3D TSpriteByCircleGenerator::GetPos()  const
{
	return Pos;
}
///--------------------------------------------------------------------------------------//
float TSpriteByCircleGenerator::GetPosX() const
{
	return Pos.GetX();
}
///--------------------------------------------------------------------------------------//
float TSpriteByCircleGenerator::GetPosY() const
{
	return Pos.GetY();
}
///--------------------------------------------------------------------------------------//
float TSpriteByCircleGenerator::GetPosZ() const
{
	return Pos.GetZ();
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetFloatingSpriteCenterFlag(const bool &Val)
{
	FloatingSpriteCenter = Val;
}
///--------------------------------------------------------------------------------------//
bool TSpriteByCircleGenerator::GetFloatingSpriteCenterFlag() const
{
	return FloatingSpriteCenter;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetRandPosZFlag(const bool &Val)
{
	RandPosZ = Val;
}
///--------------------------------------------------------------------------------------//
bool TSpriteByCircleGenerator::GetRandPosZFlag()const
{
	return RandPosZ;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetGenRandPosFlag(const bool &Val)
{
	GenRandPos = Val;
}
///--------------------------------------------------------------------------------------//
bool TSpriteByCircleGenerator::GetGenRandPosFlag()const
{
	return GenRandPos;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetStartAngle(const float &Val)
{
	StartAngle = Val;
}
///--------------------------------------------------------------------------------------//
float TSpriteByCircleGenerator::GetStartAngle() const
{
	return StartAngle;
}
///--------------------------------------------------------------------------------------//
void TSpriteByCircleGenerator::SetAngleAlongPathFlag(const bool &Val)
{
	AngleAlongPathFlag = Val;
}
///--------------------------------------------------------------------------------------//
bool TSpriteByCircleGenerator::GetAngleAlongPathFlag() const
{
	return AngleAlongPathFlag;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
