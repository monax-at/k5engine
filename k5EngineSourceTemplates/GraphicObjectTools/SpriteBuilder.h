///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef SPRITEBUILDER_H_INCLUDED
#define SPRITEBUILDER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TSpriteBuilder
{
	public:
		TSpriteBuilder();
		~TSpriteBuilder();

		void Init(	const pTSprite &Sprite, const pTTexture &Texture,
					const float &RectX=0.0f, const float &RectY=0.0f,
					const float &RectWidth=0.0f, const float &RectHeight=0.0f);

		void Init(	const pTSprite &Sprite,
					const float &X, const float &Y, const float &Z,
					const pTTexture &Texture,
					const float &RectX=0.0f, const float &RectY=0.0f,
					const float &RectWidth=0.0f, const float &RectHeight=0.0f);

		void Init(	const pTSprite &Sprite,
					const TPoint SpritePos, const pTTexture &Texture,
					const float &RectX=0.0f, const float &RectY=0.0f,
					const float &RectWidth=0.0f, const float &RectHeight=0.0f);


		pTSprite Run(const pTTexture &Texture,
					 const float &RectX=0.0f, const float &RectY=0.0f,
					 const float &RectWidth=0.0f, const float &RectHeight=0.0f);

		pTSprite Run(const float &X, const float &Y, const float &Z,
					 const pTTexture &Texture,
					 const float &RectX=0.0f, const float &RectY=0.0f,
					 const float &RectWidth=0.0f, const float &RectHeight=0.0f);

		pTSprite Run(const TPoint &SpritePos,
					 const pTTexture &Texture,
					 const float &RectX=0.0f, const float &RectY=0.0f,
					 const float &RectWidth=0.0f, const float &RectHeight=0.0f);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // SPRITEBUILDER_H_INCLUDED
