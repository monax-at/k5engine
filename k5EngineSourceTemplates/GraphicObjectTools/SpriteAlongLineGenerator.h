///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef SPRITEALONGLINEGENERATOR_H_INCLUDED
#define SPRITEALONGLINEGENERATOR_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TSpriteAlongLineGenerator:public TBaseAction
{
	protected:
		TExceptionGenerator Exception;
		TSprite SpriteTemplate;

		pTSpriteList List;

		int NumSprites;

		TPoint StartPos;
		TPoint FinishPos;

		bool RandPosZ;
	protected:
		void ToRun();
	public:
		TSpriteAlongLineGenerator();
		virtual ~TSpriteAlongLineGenerator();

		void Set(const pTSpriteList  &Val);
		void Set(const pTGraphicBank &Val);

		void SetTemplateSprite(const TSprite &Val);
		void SetTemplateSprite(const pTSprite &Val);

		void SetNumSprites(const int &Val);

		void SetPositions(const TPoint &StartVal,const TPoint3D &FinishVal);

		void SetStartPos(const TPoint &Val);
		void SetStartPos(const float &X,const float &Y);
		void SetStartPos(const float &X,const float &Y,const float &Z);

		void SetFinishPos(const TPoint &Val);
		void SetFinishPos(const float &X,const float &Y);
		void SetFinishPos(const float &X,const float &Y,const float &Z);

		void SetRandPosZFlag(const bool &Val);
		bool GetRandPosZFlag()const;
};

typedef TSpriteAlongLineGenerator* pTSpriteAlongLineGenerator;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // SPRITEALONGLINEGENERATOR_H_INCLUDED
