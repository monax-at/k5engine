#include "RandActionControllSystemCell.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TRandActionControllSystemCell::TRandActionControllSystemCell():TBaseAction(),
			Action(NULL),State(EN_ACS_Unknown),Priority(1),OneExecuteFlag(false)
{
}
///--------------------------------------------------------------------------------------//
TRandActionControllSystemCell::~TRandActionControllSystemCell()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCell::ToRun()
{
	switch(State){
		case EN_ACS_Run:{
			Action->Start();
			Action->Run();
		}break;

		case EN_ACS_Start 	:{ Action->Start(); }break;
		case EN_ACS_Stop 	:{ Action->Stop(); 	}break;
		case EN_ACS_Resume	:{ Action->SetActive(true);	}break;
		case EN_ACS_Suspend	:{ Action->SetActive(false);}break;

		default:break;
	}

	if(OneExecuteFlag){Stop();}
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCell::Set(const pTBaseAction &ActionVal,
										const enActionControlState &StateVal,
										const int &PriorityVal)
{
	Action = ActionVal;
	State = StateVal;
	Priority = PriorityVal;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCell::UnSet()
{
	Action = NULL;
	State = EN_ACS_Unknown;
	Priority = 1;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCell::SetAction(const pTBaseAction &Val)
{
	Action = Val;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCell::SetState(const enActionControlState &Val)
{
	State = Val;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCell::SetPriority(const int &Val)
{
	Priority = Val;
}
///--------------------------------------------------------------------------------------//
pTBaseAction TRandActionControllSystemCell::GetAction() const
{
	return Action;
}
///--------------------------------------------------------------------------------------//
enActionControlState TRandActionControllSystemCell::GetState() const
{
	return State;
}
///--------------------------------------------------------------------------------------//
int TRandActionControllSystemCell::GetPriority() const
{
	return Priority;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCell::SetOneExecuteFlag(const bool &Val)
{
	OneExecuteFlag = Val;
}
///--------------------------------------------------------------------------------------//
bool TRandActionControllSystemCell::GetOneExecuteFlag() const
{
	return OneExecuteFlag;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
