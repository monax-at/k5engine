#include "RandActionControllSystemCellController.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TRandActionControllSystemCellController::TRandActionControllSystemCellController():
				TBaseAction(),System(NULL),CellId(-1),State(EN_ACS_Unknown)
{
}
///--------------------------------------------------------------------------------------//
TRandActionControllSystemCellController::~TRandActionControllSystemCellController()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCellController::ToRun()
{
	if(System == NULL){return;}
	if(CellName == L"" && CellId == -1){return;}

	pTRandActionControllSystemCell Cell(NULL);

	if(CellId != -1)	{Cell = System->Get(CellId);}
	if(CellName != L"")	{Cell = System->Get(CellName);}

	switch(State){
		case EN_ACS_Run		:{Cell->Run()	;}break;
		case EN_ACS_Start	:{Cell->Start()	;}break;
		case EN_ACS_Stop	:{Cell->Stop()	;}break;
		case EN_ACS_Resume	:{Cell->SetActive(true)	;}break;
		case EN_ACS_Suspend	:{Cell->SetActive(false);}break;
		default:break;
	}
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCellController::SetSystem
										(const pTRandActionControllSystem &Val)
{
	System = Val;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCellController::SetSystem(const pTBaseAction &Val)
{
	System = static_cast<pTRandActionControllSystem>(Val);
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCellController::SetCellName(const wstring Val)
{
	CellName = Val;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCellController::UnSetCellName()
{
	CellName = L"";
}
///--------------------------------------------------------------------------------------//
wstring TRandActionControllSystemCellController::GetCellName() const
{
	return CellName;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCellController::SetCellId(const int Val)
{
	CellId = Val;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCellController::UnCellId()
{
	CellId = -1;
}
///--------------------------------------------------------------------------------------//
int TRandActionControllSystemCellController::GetCellId() const
{
	return CellId;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCellController::SetState
												(const enActionControlState &Val)
{
	State = Val;
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystemCellController::UnSetState()
{
	State = EN_ACS_Unknown;
}
///--------------------------------------------------------------------------------------//
enActionControlState TRandActionControllSystemCellController::GetState() const
{
	return State;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
