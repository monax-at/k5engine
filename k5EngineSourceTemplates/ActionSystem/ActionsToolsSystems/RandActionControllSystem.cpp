#include "RandActionControllSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TRandActionControllSystem::TRandActionControllSystem():TBaseAction()
{
}
///--------------------------------------------------------------------------------------//
TRandActionControllSystem::~TRandActionControllSystem()
{
	Clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TRandActionControllSystem::ToRun()
{
	int Size(Cells.GetSize());
	if(Size == 0){return;}

	int PrioritySum(-1);
	for(int i=0;i<Size;i++){
		pTRandActionControllSystemCell Cell(Cells.Get(i));
		if(Cell->IsActive()){ PrioritySum += Cell->GetPriority(); }
	}

	if(PrioritySum == -1){return;}

    int PriorityId(GenRandValue(PrioritySum));

    for(int i=0;i<Size;i++){
		pTRandActionControllSystemCell Cell(Cells.Get(i));
		if(Cell->IsActive()){
			PriorityId -= Cell->GetPriority();
			if(PriorityId<0){
				Cell->Run();
				break;
			}
		}
    }
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystem::Add(const pTRandActionControllSystemCell &Cell)
{
	Cells.Add(Cell);
}
///--------------------------------------------------------------------------------------//
pTRandActionControllSystemCell TRandActionControllSystem::Get(const wstring &CName)
{
	return Cells.Get(CName);
}
///--------------------------------------------------------------------------------------//
pTRandActionControllSystemCell TRandActionControllSystem::Get(const int &CId)
{
	return Cells.Get(CId);
}
///--------------------------------------------------------------------------------------//
void TRandActionControllSystem::Clear()
{
	Cells.Clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
