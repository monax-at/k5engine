///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef RANDACTIONCONTROLLSYSTEM_H_INCLUDED
#define RANDACTIONCONTROLLSYSTEM_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "RandActionControllSystemCell.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TRandActionControllSystem:public TBaseAction
{
	protected:
		TBaseActionList<TRandActionControllSystemCell> Cells;
	protected:
		void ToRun();
	public:
		TRandActionControllSystem();
		virtual ~TRandActionControllSystem();

		void Add(const pTRandActionControllSystemCell &Cell);

		pTRandActionControllSystemCell Get(const wstring &CName);
		pTRandActionControllSystemCell Get(const int &CId);

		void Clear();
};

typedef TRandActionControllSystem* pTRandActionControllSystem;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // RANDACTIONCONTROLLSYSTEM_H_INCLUDED
