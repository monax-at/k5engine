///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef RANDACTIONCONTROLLSYSTEMCELL_H_INCLUDED
#define RANDACTIONCONTROLLSYSTEMCELL_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TRandActionControllSystemCell:public TBaseAction
{
	protected:
		pTBaseAction 		 Action;
		enActionControlState State;
		int 				 Priority;

		bool OneExecuteFlag;
	protected:
		void ToRun();
	public:
		TRandActionControllSystemCell();
		virtual ~TRandActionControllSystemCell();

		void Set(const pTBaseAction &ActionVal,
				 const enActionControlState &StateVal = EN_ACS_Run,
				 const int &PriorityVal = 1);

		void UnSet();

		void SetAction(const pTBaseAction &Val);
		void SetState(const enActionControlState &Val);
		void SetPriority(const int &Val);

		pTBaseAction GetAction() const;
		enActionControlState GetState() const;
		int GetPriority() const;

		void SetOneExecuteFlag(const bool &Val);
		bool GetOneExecuteFlag() const;
};

typedef TRandActionControllSystemCell* pTRandActionControllSystemCell;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // RANDACTIONCONTROLLSYSTEMCELL_H_INCLUDED
