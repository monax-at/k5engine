///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef RANDACTIONCONTROLLSYSTEMCELLCONTROLLER_H_INCLUDED
#define RANDACTIONCONTROLLSYSTEMCELLCONTROLLER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "RandActionControllSystem.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TRandActionControllSystemCellController:public TBaseAction
{
	protected:
		pTRandActionControllSystem System;

		// значения, по которым определяется нужная ячейка, используется
		// только одно значение, если заданы оба, то будет использоваться CellName
		// пустые значения: CellName=L"" и CellId=-1
		wstring CellName;
		int CellId;

		//
		enActionControlState State;
	protected:
		void ToRun();
	public:
		TRandActionControllSystemCellController();
		virtual~TRandActionControllSystemCellController();

		void SetSystem(const pTRandActionControllSystem &Val);
		void SetSystem(const pTBaseAction &Val);

		void SetCellName(const wstring Val);
		void UnSetCellName();
		wstring GetCellName() const;

		void SetCellId(const int Val);
		void UnCellId();
		int  GetCellId() const;

		void SetState(const enActionControlState &Val);
		void UnSetState();
		enActionControlState GetState() const;
};

typedef TRandActionControllSystemCellController*
										pTRandActionControllSystemCellController;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // RANDACTIONCONTROLLSYSTEMCELLCONTROLLER_H_INCLUDED
