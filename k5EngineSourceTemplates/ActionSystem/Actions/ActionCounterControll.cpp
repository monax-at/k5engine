#include "ActionCounterControll.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TActionCounterControll::TActionCounterControll():TBaseAction(),Count(0),Counter(0)
{
}
///--------------------------------------------------------------------------------------//
TActionCounterControll::~TActionCounterControll()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TActionCounterControll::ToStart()
{
	Counter = 0;
}
///--------------------------------------------------------------------------------------//
void TActionCounterControll::ToRun()
{
	Counter++;
	if(Counter<Count){
		TickActions.Start();
		TickActions.Run();
	}
	else{
		FinishTickActions.Start();
		FinishTickActions.Run();
		Stop();
	}
}
///--------------------------------------------------------------------------------------//
void TActionCounterControll::SetCount(const int &Val)
{
	Count = Val;
}
///--------------------------------------------------------------------------------------//
void TActionCounterControll::AddTickAction(const pTBaseAction &Val)
{
	TickActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
pTActionList TActionCounterControll::GetTickActionsPtr()
{
	return &TickActions;
}
///--------------------------------------------------------------------------------------//
void TActionCounterControll::AddFinishTickAction(const pTBaseAction &Val)
{
	FinishTickActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
pTActionList TActionCounterControll::GetFinishTickActionsPtr()
{
	return &FinishTickActions;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
