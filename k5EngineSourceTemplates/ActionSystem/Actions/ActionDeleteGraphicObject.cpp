#include "ActionDeleteGraphicObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TActionDeleteGraphicObject::TActionDeleteGraphicObject():TBaseAction(),
						Sprites(NULL),Texts(NULL),Banks(NULL),GraphicObject(NULL)
{
}
///--------------------------------------------------------------------------------------//
TActionDeleteGraphicObject::~TActionDeleteGraphicObject()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TActionDeleteGraphicObject::ToRun()
{
	if(GraphicObject!=NULL){
		if(Sprites	!=NULL)	{Sprites->DelGraphicObjectIfExist(GraphicObject->GetID());}
		if(Texts	!=NULL)	{Texts	->DelGraphicObjectIfExist(GraphicObject->GetID());}
		if(Banks	!=NULL)	{Banks	->DelGraphicObjectIfExist(GraphicObject->GetID());}
	}
	else{
		if(ObjectList.length() == 0){
			if(Sprites	!=NULL)	{Sprites->DelGraphicObjectIfExist(ObjectName);}
			if(Texts	!=NULL)	{Texts	->DelGraphicObjectIfExist(ObjectName);}
			if(Banks	!=NULL)	{Banks	->DelGraphicObjectIfExist(ObjectName);}
		}
		else{
			if(Sprites	!=NULL)	{Sprites->DelIfExist(ObjectList,ObjectName);}
			if(Texts	!=NULL)	{Texts	->DelIfExist(ObjectList,ObjectName);}
			if(Banks	!=NULL)	{Banks	->DelIfExist(ObjectList,ObjectName);}
		}
	}
}
///--------------------------------------------------------------------------------------//
void TActionDeleteGraphicObject::SetSprites(const pTSpriteListManager &Val)
{
	Sprites = Val;
}
///--------------------------------------------------------------------------------------//
void TActionDeleteGraphicObject::SetGraphicBanks(const pTGraphicBankListManager &Val)
{
	Banks = Val;
}
///--------------------------------------------------------------------------------------//
void TActionDeleteGraphicObject::SetTexts(const pTTextListManager &Val)
{
	Texts = Val;
}
///--------------------------------------------------------------------------------------//
void TActionDeleteGraphicObject::SetGraphicObject(const pTBaseGraphicObject &Val)
{
	GraphicObject = Val;
}
///--------------------------------------------------------------------------------------//
void TActionDeleteGraphicObject::SetObjectName(const wstring &Val)
{
	ObjectName = Val;
}
///--------------------------------------------------------------------------------------//
void TActionDeleteGraphicObject::SetObjectList(const wstring &Val)
{
	ObjectList = Val;
}
///--------------------------------------------------------------------------------------//
void TActionDeleteGraphicObject::Reset()
{
	Sprites = NULL;
	Texts 	= NULL;
	Banks 	= NULL;

	GraphicObject = NULL;

	ObjectName.clear();
	ObjectList.clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
