#include "ActionLoadMusicTrack.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TActionLoadMusicTrack::TActionLoadMusicTrack():TBaseAction(),MusicTracks(NULL),Device(NULL)
{
}
///--------------------------------------------------------------------------------------//
TActionLoadMusicTrack::~TActionLoadMusicTrack()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TActionLoadMusicTrack::ToRun()
{
	TExceptionGenerator Ex(L"TActionLoadMusicTrack: ");
	Ex(MusicTracks	!=NULL, L"in ToRun() MusicTracks is NULL");
	Ex(Device		!=NULL, L"in ToRun() Music Device is NULL");

	if(!MusicTracks->IfExist(ListName)){MusicTracks->Add(ListName);}

	TMusicTrackLoader Loader;
	Loader.SetDevice(Device);

	MusicTracks->Add(ListName, Loader.Run(FileName,TrackName));
}
///--------------------------------------------------------------------------------------//
void TActionLoadMusicTrack::SetMusicTracks(const pTMusicTrackListManager &Val)
{
	MusicTracks = Val;
}
///--------------------------------------------------------------------------------------//
void TActionLoadMusicTrack::SetDevice(const pTMusicDevice &Val)
{
	Device = Val;
}
///--------------------------------------------------------------------------------------//
void TActionLoadMusicTrack::SetListName(const wstring &Val)
{
	ListName = Val;
}
///--------------------------------------------------------------------------------------//
void TActionLoadMusicTrack::SetTrackName(const wstring &Val)
{
	TrackName = Val;
}
///--------------------------------------------------------------------------------------//
void TActionLoadMusicTrack::SetFileName(const wstring &Val)
{
	FileName = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
