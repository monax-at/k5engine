///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONLOADMUSICTRACK_H_INCLUDED
#define ACTIONLOADMUSICTRACK_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TActionLoadMusicTrack:public TBaseAction
{
	protected:
		pTMusicTrackListManager MusicTracks;
		pTMusicDevice				Device;

		wstring ListName;
		wstring TrackName;

		wstring FileName;
	protected:
		void ToRun();
	public:
		TActionLoadMusicTrack();
		virtual ~TActionLoadMusicTrack();

		void SetMusicTracks(const pTMusicTrackListManager &Val);
		void SetDevice(const pTMusicDevice &Val);

		void SetListName	(const wstring &Val);
		void SetTrackName	(const wstring &Val);
		void SetFileName	(const wstring &Val);
};

typedef TActionLoadMusicTrack* pTActionLoadMusicTrack;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // ACTIONLOADMUSICTRACK_H_INCLUDED
