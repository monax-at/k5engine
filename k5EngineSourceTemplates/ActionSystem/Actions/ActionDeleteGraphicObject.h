///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONDELETEGRAPHICOBJECT_H_INCLUDED
#define ACTIONDELETEGRAPHICOBJECT_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TActionDeleteGraphicObject:public TBaseAction
{
	protected:
		pTSpriteListManager 		Sprites;
		pTTextListManager			Texts;
		pTGraphicBankListManager 	Banks;

		pTBaseGraphicObject GraphicObject;
		wstring ObjectName;
		wstring ObjectList;
	protected:
		void ToRun();
	public:
		TActionDeleteGraphicObject();
		virtual ~TActionDeleteGraphicObject();

		void SetSprites(const pTSpriteListManager &Val);
		void SetTexts(const pTTextListManager &Val);
		void SetGraphicBanks(const pTGraphicBankListManager &Val);

		void SetGraphicObject(const pTBaseGraphicObject &Val);
		void SetObjectName(const wstring &Val);
		void SetObjectList(const wstring &Val);

		void Reset();
};

typedef TActionDeleteGraphicObject* pTActionDeleteGraphicObject;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // ACTIONDELETEGRAPHICOBJECT_H_INCLUDED
