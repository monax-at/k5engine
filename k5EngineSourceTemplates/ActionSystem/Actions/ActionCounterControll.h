///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONCOUNTERCONTROLL_H_INCLUDED
#define ACTIONCOUNTERCONTROLL_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TActionCounterControll:public TBaseAction
{
	protected:
		int Count;
		int Counter;

		TActionList TickActions;
		TActionList FinishTickActions;
	protected:
		void ToStart();
		void ToRun();
	public:
		TActionCounterControll();
		virtual ~TActionCounterControll();

		void SetCount(const int &Val);

		void AddTickAction(const pTBaseAction &Val);
		pTActionList GetTickActionsPtr();

		void AddFinishTickAction(const pTBaseAction &Val);
		pTActionList GetFinishTickActionsPtr();
};

typedef TActionCounterControll* pTActionCounterControll;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // ACTIONCOUNTERCONTROLL_H_INCLUDED
