#include "ActionMusicTrackController.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TActionMusicTrackController::TActionMusicTrackController():TBaseAction(),
										MusicTrack(NULL),ModeVal(0.0f)
{
}
///--------------------------------------------------------------------------------------//
TActionMusicTrackController::~TActionMusicTrackController()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TActionMusicTrackController::ToRun()
{
	if(ModeStr == L"" || MusicTrack == NULL){return;}

	if(ModeStr == L"play"){MusicTrack->Play();}
	if(ModeStr == L"stop"){MusicTrack->Stop();}

	if(ModeStr == L"repeat"){
		if(ModeVal == 1.0f)	{ MusicTrack->SetRepeat(true); }
		else				{ MusicTrack->SetRepeat(false); }
	}

	if(ModeStr == L"volume"){
		MusicTrack->SetVolume(ModeVal);
	}
}
///--------------------------------------------------------------------------------------//
void TActionMusicTrackController::SetMusicTrack(const pTBaseMusicTrack &Val)
{
	MusicTrack = Val;
}
///--------------------------------------------------------------------------------------//
void TActionMusicTrackController::SetMode(const wstring &Val)
{
	ModeStr = Val;
}
///--------------------------------------------------------------------------------------//
void TActionMusicTrackController::SetModeVal(const float &Val)
{
	ModeVal = Val;
}
///--------------------------------------------------------------------------------------//
void TActionMusicTrackController::SetModeVal(const bool &Val)
{
	if(Val){ModeVal = 1.0f;}else{ModeVal = 0.0f;}
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
