///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONMUSICTRACKCONTROLLER_H_INCLUDED
#define ACTIONMUSICTRACKCONTROLLER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TActionMusicTrackController:public TBaseAction
{
	protected:
		pTBaseMusicTrack MusicTrack;
		wstring ModeStr;
		float ModeVal;
	protected:
		void ToRun();
	public:
		TActionMusicTrackController();
		virtual ~TActionMusicTrackController();

		void SetMusicTrack(const pTBaseMusicTrack &Val);
		void SetMode(const wstring &Val);
		void SetModeVal(const float &Val);
		void SetModeVal(const bool &Val);
};

typedef TActionMusicTrackController* pTActionMusicTrackController;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // ACTIONMUSICTRACKCONTROLLER_H_INCLUDED
