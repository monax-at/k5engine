///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONSETACTIVEEVENTACTIONOBJECT_H_INCLUDED
#define ACTIONSETACTIVEEVENTACTIONOBJECT_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TActionSetActiveEventActionObject:public TBaseAction
{
	protected:
		bool SetFlag;
		TEventActionListManager *Manager;
		TEventActionList *List;
		TBaseEventAction *Action;
	protected:
		void ToRun();
	public:
		TActionSetActiveEventActionObject();
		virtual ~TActionSetActiveEventActionObject();

		void SetSetFlag(const bool &Val);

		void SetActions(TEventActionListManager *Val);
		void SetList(TEventActionList *Val);
		void SetAction(TBaseEventAction *Val);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // ACTIONSETACTIVEEVENTACTIONOBJECT_H_INCLUDED
