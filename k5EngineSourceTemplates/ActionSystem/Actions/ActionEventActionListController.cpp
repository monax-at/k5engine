#include "ActionEventActionListController.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TActionEventActionListController::TActionEventActionListController():
							TBaseAction(),List(NULL),ControlState(EN_ACS_Unknown)
{
}
///--------------------------------------------------------------------------------------//
TActionEventActionListController::~TActionEventActionListController()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TActionEventActionListController::ToRun()
{
	if(List==NULL){return;}

	switch(ControlState){
		case EN_ACS_Start	:{List->Start();}break;
		case EN_ACS_Stop 	:{List->Stop ();}break;
		case EN_ACS_Resume 	:{List->SetActive(true) ;}break;
		case EN_ACS_Suspend :{List->SetActive(false);}break;
		default:break;
	}
}
///--------------------------------------------------------------------------------------//
void TActionEventActionListController::SetActions(const pTEventActionList &Val)
{
	List = Val;
}
///--------------------------------------------------------------------------------------//
void TActionEventActionListController::SetControlState
												(const enActionControlState &Val)
{
	ControlState = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
