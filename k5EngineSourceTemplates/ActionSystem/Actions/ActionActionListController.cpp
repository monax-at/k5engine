#include "ActionActionListController.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TActionActionListController::TActionActionListController():TBaseAction(),
											List(NULL),ControlState(EN_ACS_Unknown)
{
}
///--------------------------------------------------------------------------------------//
TActionActionListController::~TActionActionListController()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TActionActionListController::ToRun()
{
	if(List==NULL){return;}

	switch(ControlState){
		case EN_ACS_Run 	:{ List->Start(); List->Run(); 	}break;
		case EN_ACS_Start 	:{ List->Start(); 				}break;
		case EN_ACS_Stop 	:{ List->Stop();  				}break;
		case EN_ACS_Resume	:{ List->SetActive(true);		}break;
		case EN_ACS_Suspend	:{ List->SetActive(false);		}break;
		default:break;
	}
}
///--------------------------------------------------------------------------------------//
void TActionActionListController::SetActions(const pTActionList &Val)
{
	List = Val;
}
///--------------------------------------------------------------------------------------//
void TActionActionListController::SetControlState(const enActionControlState &Val)
{
	ControlState = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
