#include "ActionSetActiveEventActionObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TActionSetActiveEventActionObject::TActionSetActiveEventActionObject():
						TBaseAction(),Manager(NULL),List(NULL),Action(NULL)
{
}
///--------------------------------------------------------------------------------------//
TActionSetActiveEventActionObject::~TActionSetActiveEventActionObject()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TActionSetActiveEventActionObject::ToRun()
{
	if(Manager == NULL){return;}

	if(SetFlag == true){
		if(List != NULL){
			if(Action == NULL)	{Manager->SetActiveObject(List);}
			else				{List->SetActiveObject(Action);}
		}
	}
	else{
		if(List == NULL){Manager->UnSetActiveObject();}
		else			{List->UnSetActiveObject();}
	}
}
///--------------------------------------------------------------------------------------//
void TActionSetActiveEventActionObject::SetSetFlag(const bool &Val)
{
	SetFlag = Val;
}
///--------------------------------------------------------------------------------------//
void TActionSetActiveEventActionObject::SetActions(TEventActionListManager *Val)
{
	Manager = Val;
}
///--------------------------------------------------------------------------------------//
void TActionSetActiveEventActionObject::SetList(TEventActionList *Val)
{
	List = Val;
}
///--------------------------------------------------------------------------------------//
void TActionSetActiveEventActionObject::SetAction(TBaseEventAction *Val)
{
	Action = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
