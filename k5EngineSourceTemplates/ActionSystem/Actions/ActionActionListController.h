///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef TACTIONACTIONLISTCONTROLLER_H_INCLUDED
#define TACTIONACTIONLISTCONTROLLER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TActionActionListController:public TBaseAction
{
	protected:
		pTActionList List;
		enActionControlState ControlState;
	protected:
		void ToRun();
	public:
		TActionActionListController();
		virtual ~TActionActionListController();

		void SetActions(const pTActionList &Val);
		void SetControlState(const enActionControlState &Val);
};

typedef TActionActionListController* pTActionActionListController;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // TACTIONACTIONLISTCONTROLLER_H_INCLUDED
