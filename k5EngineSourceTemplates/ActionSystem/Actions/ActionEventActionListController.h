///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONEVENTACTIONLISTCONTROLLER_H_INCLUDED
#define ACTIONEVENTACTIONLISTCONTROLLER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TActionEventActionListController:public TBaseAction
{
	protected:
		pTEventActionList List;
		enActionControlState ControlState;
	protected:
		void ToRun();
	public:
		TActionEventActionListController();
		virtual ~TActionEventActionListController();

		void SetActions(const pTEventActionList &Val);
		void SetControlState(const enActionControlState &Val);
};

typedef TActionEventActionListController* pTActionEventActionListController;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // ACTIONEVENTACTIONLISTCONTROLLER_H_INCLUDED
