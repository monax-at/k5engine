#include "EventActionSpritePointerListColorIterator.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionSpritePointerListColorIterator::
	TEventActionSpritePointerListColorIterator():
	TBaseEventAction(),List(NULL),Delay(0),DelayTimer(0),DelayTimerReady(true),
	WorkTime(0),WorkTimer(0)
{
}
///--------------------------------------------------------------------------------------//
TEventActionSpritePointerListColorIterator::
	~TEventActionSpritePointerListColorIterator()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::ToStart()
{
	TExceptionGenerator Ex(L"TEventActionSpritePointerListColorIterator: ");
	Ex(List!=NULL,L" in ToStart() List is NULL");

	DelayTimer = 0.0f;
	WorkTimer  = 0.0f;
	ColorSpeed = ColorValue;
	if(WorkTime >0){
		ColorSpeed(0,ColorSpeed.Get(0)/WorkTime);
		ColorSpeed(1,ColorSpeed.Get(1)/WorkTime);
		ColorSpeed(2,ColorSpeed.Get(2)/WorkTime);
		ColorSpeed(3,ColorSpeed.Get(3)/WorkTime);
	}

	DelayTimerReady = false;
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::ToStop()
{
	TColor ResColor(StartColor);
	if(Direction == EN_CVD_Inc){ ResColor.Inc(ColorValue);}
	if(Direction == EN_CVD_Dec){ ResColor.Dec(ColorValue);}

	int Size(List->GetSize());
	for(int i=0;i<Size;i++){List->Get(i)->Color(ResColor);}
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::ToRun(const TEvent &Event)
{
	if(Event.Type != EN_TIMER){return;}

	if(DelayTimerReady == false){
		DelayTimer += Event.Timer.Ms;
		if(DelayTimer>=Delay){
			DelayTimerReady = true;
		}
	}

	if(DelayTimerReady == true){

		WorkTimer += Event.Timer.Ms;
		if(WorkTimer>=WorkTime){
			Stop();
		}
		else{
			TColor DeltaColor;
			for(int i=0;i<4;i++){
				DeltaColor(i, ColorSpeed.Get(i)*Event.Timer.Ms);
			}

			int Size(List->GetSize());
			for(int i=0;i<Size;i++){
				if(Direction == EN_CVD_Inc){
					List->Get(i)->Color.Inc(DeltaColor);
				}

				if(Direction == EN_CVD_Dec){
					List->Get(i)->Color.Dec(DeltaColor);
				}
			}
		}
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::Set(const pTSpritePointerList &Val)
{
	List = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::SetDirection
											(const enChangeValueDirection &Val)
{
	Direction = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::SetWorkTime(const float &Val)
{
	WorkTime = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::SetDelay(const float &Val)
{
	Delay = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::SetColorValue(	const float &r,
								const float &g,const float &b,const float &a)
{
	ColorValue(r,g,b,a);
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::SetColorValue(const TColor &Val)
{
	ColorValue = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::SetStartColor(const float &r,
								const float &g,const float &b,const float &a)
{
	StartColor(r,g,b,a);
}
///--------------------------------------------------------------------------------------//
void TEventActionSpritePointerListColorIterator::SetStartColor(const TColor &Val)
{
	StartColor(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
