#include "EventActionSteppingExecutionController.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionSteppingExecutionController::TEventActionSteppingExecutionController():
					TBaseEventAction(),ExecutionCursor(0),AutoUpdateCursor(false)
{
	Actions = new TEventActionList;
}
///--------------------------------------------------------------------------------------//
TEventActionSteppingExecutionController::~TEventActionSteppingExecutionController()
{
	delete Actions;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionSteppingExecutionController::ToRun(const TEvent &Event)
{
	Actions->RunOne(Event,ExecutionCursor);

	if(!Actions->IsOneActive(ExecutionCursor)){
		if(AutoUpdateCursor == true){ ToStart();}
		else						{ ActiveFlag = false;}
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionSteppingExecutionController::ToStart()
{
	Actions->Get(ExecutionCursor)->StopIsActive();

	ExecutionCursor++;
	if(ExecutionCursor >= Actions->GetSize()){ ExecutionCursor = 0;}
	Actions->StartOne(ExecutionCursor);
}
///--------------------------------------------------------------------------------------//
void TEventActionSteppingExecutionController::ToStop()
{
	ExecutionCursor = Actions->GetSize() - 1;
}
///--------------------------------------------------------------------------------------//
void TEventActionSteppingExecutionController::Clear()
{
	Actions->Clear();
}
///--------------------------------------------------------------------------------------//
void TEventActionSteppingExecutionController::Add(const pTBaseEventAction &Val)
{
	Actions->Add(Val);
	ExecutionCursor = Actions->GetSize() - 1;
}
///--------------------------------------------------------------------------------------//
pTEventActionList TEventActionSteppingExecutionController::GetActionsPtr()
{
	return Actions;
}
///--------------------------------------------------------------------------------------//
void TEventActionSteppingExecutionController::SetAutoUpdateCursor(const bool &Val)
{
	AutoUpdateCursor = Val;
}
///--------------------------------------------------------------------------------------//
bool TEventActionSteppingExecutionController::GetAutoUpdateCursor()
{
	return AutoUpdateCursor;
}
///--------------------------------------------------------------------------------------//
void TEventActionSteppingExecutionController::SetCursor(const int &Val)
{
	Actions->Get(ExecutionCursor)->StopIsActive();
	ExecutionCursor = Val;
	Actions->Get(ExecutionCursor)->Start();
}
///--------------------------------------------------------------------------------------//
int TEventActionSteppingExecutionController::GetCursor() const
{
	return ExecutionCursor;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
