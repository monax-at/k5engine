#include "EventActionSpriteGhost.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionSpriteGhost::TEventActionSpriteGhost():TBaseSpriteEventAction(),
					Ghost(NULL),Distance(0.0f),ViewGhost(false),AlphaCof(0.5f)
{
}
///--------------------------------------------------------------------------------------//
TEventActionSpriteGhost::~TEventActionSpriteGhost()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionSpriteGhost::ToStart()
{
	if(Ghost == NULL){return;}
	Ghost->Color.SetAlpha(0.0f);
}
///--------------------------------------------------------------------------------------//
void TEventActionSpriteGhost::ToStop()
{
	if(Ghost == NULL){return;}
	Ghost->Color.SetAlpha(0.0f);
}
///--------------------------------------------------------------------------------------//
void TEventActionSpriteGhost::ToRun(const TEvent &Event)
{
	if(Ghost == NULL || Sprite == NULL)		{return;}
	if(Event.Type != EN_MOUSE)				{return;}
	if(Event.Mouse.Type != EN_MOUSEMOTION)	{return;}

	float Dist = GetDistance2D(Ghost->Pos,Sprite->Pos);

	if(ViewGhost == false){
		if(Dist > Distance){return;}
		ViewGhost = true;
	}
	else{
		if(Dist > Distance){
			ViewGhost = false;
			Ghost->Color.SetAlpha(0.0f);
		}
		else{
			float Alpha = AlphaCof*(1.0f - Dist/Distance);
			if(Alpha<0.0f){Alpha = 0.0f;}
			Ghost->Color.SetAlpha(Alpha);
		}
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionSpriteGhost::SetGhost(TSprite *Val)
{
	Ghost = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionSpriteGhost::SetDistance(const float &Val)
{
	Distance = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionSpriteGhost::SetAlphaCof(const float &Val)
{
	AlphaCof = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
