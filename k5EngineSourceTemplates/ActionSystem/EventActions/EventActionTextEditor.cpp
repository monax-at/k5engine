#include "EventActionTextEditor.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionTextEditor::TEventActionTextEditor():TBaseTextEventAction(),
										MaxWidth(0.0f),MaxStrLength(0),CursorBlinkTime(500),
										TextCursor(NULL)
{
}
///--------------------------------------------------------------------------------------//
TEventActionTextEditor::~TEventActionTextEditor()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionTextEditor::ProcessTimer(const TEvent &Event)
{
    /*if(TextCursor == NULL) return;
    TextCursorColorIterators[TextCursorColorIteratorCounter].Run(Event);
        if(!TextCursorColorIterators[TextCursorColorIteratorCounter].IsActive()){
        TextCursorColorIteratorCounter++;
        if(TextCursorColorIteratorCounter == 2) {TextCursorColorIteratorCounter = 0;}
        TextCursorColorIterators[TextCursorColorIteratorCounter].Start();
    }*/
}
//--------------------------------------------------------------------------------------//
void TEventActionTextEditor::UpdateTextCursorPos()
{
    if(TextCursor == NULL) return;

	TPoint3D CurPos(Text->Pos);
	CurPos.IncX(Text->GetWidth()/2.0f + 3.0f);
	TextCursor->Pos(CurPos);
}
///--------------------------------------------------------------------------------------//
void TEventActionTextEditor::ToStart()
{
/*	if(TextCursor != NULL){
        for(int i=0;i<2;i++){
            TextCursorColorIterators[i].SetColorValue(0.0f,0.0f,0.0f,1.0f);
            TextCursorColorIterators[i].SetWorkTime(CursorBlinkTime);
            TextCursorColorIterators[i].Set(TextCursor);
        }

        TextCursorColorIterators[0].SetDirection(EN_CVD_Inc);
        TextCursorColorIterators[1].SetDirection(EN_CVD_Dec);

        TextCursorColorIteratorCounter = 0;
        TextCursorColorIterators[TextCursorColorIteratorCounter].Start();

        UpdateTextCursorPos();
    }*/
}
///--------------------------------------------------------------------------------------//
void TEventActionTextEditor::ToRun(const TEvent &Event)
{
	/*if(Event.Type == EN_TIMER) ProcessTimer(Event);

	if(Text == NULL)					 {return;}
	if(Event.Type!= EN_KEYBOARD)		 {return;}
	if(Event.Keyboard.Type != EN_KEYDOWN){return;}


	//pTBaseDevice Device(Text->GetDevice());
	//if(Device->IsKeyPres(EN_KM_LCTRL)){return;}
	//if(Device->IsKeyPres(EN_KM_RCTRL)){return;}

	if(MaxWidth		!=0.0f  && Text->GetWidth() >= MaxWidth)		 {return;}
	if(MaxStrLength	!=0 	&& Text->GetStrLength() >= MaxStrLength) {return;}


	// обработка ввода
	if(Event.Keyboard.Key>=2000 && Event.Keyboard.Key <=2049){
		Text->Add(Event.Keyboard.Symbol);
		UpdateTextCursorPos();
	}

	if(Event.Keyboard.Key == EN_KEY_BACK){
		Text->Del();
		UpdateTextCursorPos();
	}*/
}
///--------------------------------------------------------------------------------------//
void TEventActionTextEditor::ToStop()
{
    /*if(TextCursor != NULL)
        for(int i=0;i<2;i++){
            TextCursorColorIterators[i].Stop();
        }*/
}
///--------------------------------------------------------------------------------------//
void TEventActionTextEditor::SetMaxWidth(const float &Val)
{
	MaxWidth = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionTextEditor::SetMaxStrLength(const int &Val)
{
	MaxStrLength = Val;
}
///--------------------------------------------------------------------------------------//
float TEventActionTextEditor::GetMaxWidth()const
{
	return MaxWidth;
}
///--------------------------------------------------------------------------------------//
int TEventActionTextEditor::GetMaxStrLength() const
{
	return MaxStrLength;
}
///--------------------------------------------------------------------------------------//
void TEventActionTextEditor::SetCursorText(const pTText &Val)
{
    TextCursor = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionTextEditor::SetCursorBlinkTime(const float &Val)
{
    CursorBlinkTime = Val;
}
///--------------------------------------------------------------------------------------//
bool TEventActionTextEditor::GetCursorBlinkTime()
{
    return CursorBlinkTime;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
