///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONVIBRATIONSPRITEPOS_H_INCLUDED
#define EVENTACTIONVIBRATIONSPRITEPOS_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionVibrationSpritePos:public TBaseSpriteEventAction
{
	protected:
		TPoint3D StartPos;

		float WorkTime;
		float WorkTimer;

		float RunTime;
		float RunTimer;

		int VibrationCount;

		float VibrationLength;
	protected:
		void ToStart();
		void ToStop();
		void ToRun(const TEvent &Event);
		void ToSet();
	public:
		TEventActionVibrationSpritePos();
		virtual ~TEventActionVibrationSpritePos();

		void SetWorkTime(const float &Val);
		void SetVibrationCount (const int &Val);
		void SetVibrationLength(const float &Val);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONVIBRATIONSPRITEPOS_H_INCLUDED
