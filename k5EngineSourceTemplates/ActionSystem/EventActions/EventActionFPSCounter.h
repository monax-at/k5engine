///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONFPSCOUNTER_H_INCLUDED
#define EVENTACTIONFPSCOUNTER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionFPSCounter:public TBaseTextEventAction
{
	protected:
		float ShowDelay;
		float ShowDelayTimer;
		int	  ShowDelayCounter;

		int MinFps;
		int MaxFps;

		bool ShowMinFps;
		bool ShowMaxFps;

		TText *MinFpsText;
		TText *MaxFpsText;
		float UpdateFpsTimer;
	protected:
		void ToStart();
		void ToRun(const TEvent &Event);
	public:
		TEventActionFPSCounter();
		virtual ~TEventActionFPSCounter();

		void SetShowDelay(const float &Val);

		void SetMinFpsText(TText *Val);
		void SetMaxFpsText(TText *Val);
};

typedef TEventActionFPSCounter* pTEventActionFPSCounter;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONFPSCOUNTER_H_INCLUDED
