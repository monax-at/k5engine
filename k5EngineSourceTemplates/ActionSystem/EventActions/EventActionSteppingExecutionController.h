///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONSTEPPINGEXECUTIONCONTROLLER_H_INCLUDED
#define EVENTACTIONSTEPPINGEXECUTIONCONTROLLER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionSteppingExecutionController: public TBaseEventAction
{
	protected:
		int ExecutionCursor;

		pTEventActionList Actions;

		bool AutoUpdateCursor;
		int ExecutionCount;
		int ExecutionCounter;
	protected:
		void ToRun(const TEvent &Event);

		void ToStart();
		void ToStop();
	public:
		TEventActionSteppingExecutionController();
		virtual ~TEventActionSteppingExecutionController();

		void Clear();

		void Add(const pTBaseEventAction &Val);

		pTEventActionList GetActionsPtr();

		void SetAutoUpdateCursor(const bool &Val);
		bool GetAutoUpdateCursor();

		void SetCursor(const int &Val);
		int GetCursor() const;
};

typedef TEventActionSteppingExecutionController*
			pTEventActionSteppingExecutionController;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONSTEPPINGEXECUTIONCONTROLLER_H_INCLUDED

