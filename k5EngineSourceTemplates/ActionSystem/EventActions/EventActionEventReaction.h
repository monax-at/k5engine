///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONEVENTREACTION_H_INCLUDED
#define EVENTACTIONEVENTREACTION_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionEventReaction: public TBaseEventAction
{
	protected:
		TEvent Template;
		TActionList Actions;
	protected:
		inline void RunActions();
		inline void RunMouse(const TEvent &Event);
		inline void RunKeyboard(const TEvent &Event);
		inline void RunSystem(const TEvent &Event);
		inline void RunTimer(const TEvent &Event);

		void ToRun(const TEvent &Event);
	public:
		TEventActionEventReaction();
		virtual ~TEventActionEventReaction();

		void SetTemplate(const TEvent &Val);
		void AddAction(const pTBaseAction &Val);
		pTActionList GetActionsPtr();
		void Clear();
};

typedef TEventActionEventReaction* pTEventActionEventReaction;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONEVENTREACTION_H_INCLUDED
