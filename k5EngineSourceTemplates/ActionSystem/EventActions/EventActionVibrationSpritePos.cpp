#include "EventActionVibrationSpritePos.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionVibrationSpritePos::TEventActionVibrationSpritePos():
				TBaseSpriteEventAction(),
				WorkTime(0.0f),WorkTimer(0.0f),RunTime(0.0f),RunTimer(0.0f),
				VibrationCount(0),VibrationLength(5.0f)
{
}
///--------------------------------------------------------------------------------------//
TEventActionVibrationSpritePos::~TEventActionVibrationSpritePos()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionVibrationSpritePos::ToStart()
{
	RunTime = WorkTime/VibrationCount;
}
///--------------------------------------------------------------------------------------//
void TEventActionVibrationSpritePos::ToStop()
{
	Sprite->Pos = StartPos;
	WorkTimer = 0.0f;
	RunTimer = 0.0f;
}
///--------------------------------------------------------------------------------------//
void TEventActionVibrationSpritePos::ToRun(const TEvent &Event)
{
	if(Event.Type != EN_TIMER){return;}

	WorkTimer+= Event.Timer.Ms;
	if(WorkTimer>=WorkTime){
		Stop();
		return;
	}
	RunTimer+=Event.Timer.Ms;
	if(RunTimer>=RunTime){
		RunTimer-=RunTime;

		// дрожание позиции спрайта, случайным образом выбирается
		// напровление смещения
		TPoint3D NPos(StartPos);
		switch(GenRandValue(8)){

			case 0:{NPos.IncY(VibrationLength);}break;
			case 1:{NPos.Inc(VibrationLength,VibrationLength);}break;
			case 2:{NPos.IncX(VibrationLength);}break;

			case 3:{
				NPos.IncX(VibrationLength);
				NPos.DecY(VibrationLength);
			}break;

			case 4:{NPos.DecY(VibrationLength);}break;
			case 5:{NPos.Dec(VibrationLength,VibrationLength);}break;
			case 6:{NPos.DecX(VibrationLength);}break;

			case 7:{
				NPos.DecX(VibrationLength);
				NPos.IncY(VibrationLength);
			}break;
		}
		Sprite->Pos = NPos;
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionVibrationSpritePos::ToSet()
{
	StartPos = Sprite->Pos;
}
///--------------------------------------------------------------------------------------//
void TEventActionVibrationSpritePos::SetWorkTime(const float &Val)
{
	WorkTime = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionVibrationSpritePos::SetVibrationCount (const int &Val)
{
	VibrationCount = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionVibrationSpritePos::SetVibrationLength(const float &Val)
{
	VibrationLength = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
