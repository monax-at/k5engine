#include "EventActionPointersContainer.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionPointersContainer::TEventActionPointersContainer():TBaseEventAction()
{
}
///--------------------------------------------------------------------------------------//
TEventActionPointersContainer::~TEventActionPointersContainer()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionPointersContainer::ToStart()
{
	EventActions.Start();
	StartActions.Start();
	StartActions.Run();
}
///--------------------------------------------------------------------------------------//
void TEventActionPointersContainer::ToStop()
{
	EventActions.Stop();
	StopActions.Start();
	StopActions.Run();
}
///--------------------------------------------------------------------------------------//
void TEventActionPointersContainer::ToRun(const TEvent &Event)
{
	if(EventActions.IsAllActive() == true){
		EventActions.Run(Event);
		if(EventActions.IsAllActive() == false){ Stop(); }
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionPointersContainer::Clear()
{
	EventActions.Clear();
	StartActions.Clear();
	StopActions.Clear();
}
///--------------------------------------------------------------------------------------//
void TEventActionPointersContainer::AddAction(TBaseEventAction *Val)
{
	EventActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
void TEventActionPointersContainer::AddStartAction(TBaseAction *Val)
{
	StartActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
void TEventActionPointersContainer::AddStopAction(TBaseAction *Val)
{
	StopActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
TEventActionPointerList* TEventActionPointersContainer::GetActionsPtr()
{
	return &EventActions;
}
///--------------------------------------------------------------------------------------//
pTActionList TEventActionPointersContainer::GetStartActionsPtr()
{
	return &StartActions;
}
///--------------------------------------------------------------------------------------//
pTActionList TEventActionPointersContainer::GetStopActionsPtr()
{
	return &StopActions;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
