///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONSPRITEPOINTERLISTCOLORITERATOR_H_INCLUDED
#define EVENTACTIONSPRITEPOINTERLISTCOLORITERATOR_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionSpritePointerListColorIterator: public TBaseEventAction
{
	protected:
		enChangeValueDirection Direction;

		pTSpritePointerList List;

		float Delay;
		float DelayTimer;
		bool  DelayTimerReady;

		float WorkTime;
		float WorkTimer;

		TColor ColorValue;
		TColor ColorSpeed;
		TColor StartColor;
	protected:
		void ToStart();
		void ToStop();
		void ToRun(const TEvent &Event);
	public:
		TEventActionSpritePointerListColorIterator();
		virtual ~TEventActionSpritePointerListColorIterator();

		void Set(const pTSpritePointerList &Val);

		void SetDirection(const enChangeValueDirection &Val);
		void SetWorkTime(const float &Val);
		void SetDelay(const float &Val);

		void SetColorValue(	const float &r,const float &g,
							const float &b,const float &a);

		void SetColorValue(const TColor &Val);

		void SetStartColor(	const float &r,const float &g,
							const float &b,const float &a);

		void SetStartColor(const TColor &Val);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONSPRITELISTCOLORITERATOR_H_INCLUDED
