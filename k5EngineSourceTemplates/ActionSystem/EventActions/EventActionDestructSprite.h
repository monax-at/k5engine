///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONDESTRUCTSPRITE_H_INCLUDED
#define EVENTACTIONDESTRUCTSPRITE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
#include "STKinematicsSystem.h"

#include "SpriteKnife.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionDestructSprite:public TBaseSpriteEventAction
{
	protected:
		bool InitFlag;
		int  CellSize;
		int  ActivationLimit;
		pTRenderQueue			RenderQueue;
		pTSpriteBank 			SpriteBank;
		pTSpriteBankList 		SpriteBankList;
		pTSpriteBankListManager SpriteBanks;

		TKinematicsSpriteManager ParticlesWorker;
	protected:
		inline void CreateSpriteBank();
		inline void CreateSprites();
		inline void CreateParticles();
		inline void Init();
		void ToStart();
		void ToStop();
		void ToRun(const TEvent &Event);
	public:
		TEventActionDestructSprite();
		virtual ~TEventActionDestructSprite();

		void SetCellSize(const int &Val);
		void SetRenderQueue(const pTRenderQueue &Val);
		void SetSpriteBanks(const pTSpriteBankListManager &Val);

		void AddInitiator(const pTKinematicsBaseForce &Val);
		void AddForce	 (const pTKinematicsBaseForce &Val);
};

typedef TEventActionDestructSprite* pTEventActionDestructSprite;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONDESTRUCTSPRITE_H_INCLUDED
