///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONATTACHSPRITETOMOUSE_H_INCLUDED
#define EVENTACTIONATTACHSPRITETOMOUSE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionAttachSpriteToMouse:public TBaseSpriteEventAction
{
	protected:
		void ToRun(const TEvent &Event);
	public:
		TEventActionAttachSpriteToMouse();
		virtual ~TEventActionAttachSpriteToMouse();
};

typedef TEventActionAttachSpriteToMouse* pTEventActionAttachSpriteToMouse;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONATTACHSPRITETOMOUSE_H_INCLUDED
