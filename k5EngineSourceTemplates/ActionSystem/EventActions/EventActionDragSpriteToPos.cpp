#include "EventActionDragSpriteToPos.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionDragSpriteToPos::TEventActionDragSpriteToPos():
	TBaseSpriteEventAction(),Exception(L"TEventActionDragSpriteToPos: "),
							 Device(NULL),CheckDistance(50.0f)
{
}
///--------------------------------------------------------------------------------------//
TEventActionDragSpriteToPos::~TEventActionDragSpriteToPos()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::ToStart()
{
	Exception(Sprite!=NULL,	L"in ToStart() Sprite is NULL");
	Exception(Device!=NULL,	L"in ToStart() Device is NULL");

	StarPos = Sprite->Pos;

	TPoint3D CPos(Device->GetCursor());
	Sprite->Pos(CPos.GetX(),CPos.GetY());

	StartDragActions.Start();
	StartDragActions.Run();
}
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::ToRun(const TEvent &Event)
{
	if(Event.Type != EN_MOUSE){return;}

	if(Event.Mouse.Type == EN_MOUSEMOTION){
		Sprite->Pos(Event.Mouse.X,Event.Mouse.Y);
	}

	if(Event.Mouse.Type == EN_MOUSEBUTTONUP){
		if(GetDistance2D(Sprite->Pos,FinishPos) > CheckDistance){
			Sprite->Pos = StarPos;
			CancelDragActions.Start();
			CancelDragActions.Run();
		}
		else{
			Sprite->Pos = FinishPos;
			FinishDragActions.Start();
			FinishDragActions.Run();
		}
		Stop();
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::SetCheckDistance(const float &Val)
{
	CheckDistance = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::SetFinishPos(const TPoint3D &Val)
{
	FinishPos = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::SetFinishPos(const float &X,const float &Y)
{
	FinishPos.Set(X,Y);
}
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::SetFinishPos(	const float &X,const float &Y,
												const float &Z)
{
	FinishPos.Set(X,Y,Z);
}
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::AddStartDragAction (const pTBaseAction &Val)
{
	StartDragActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::AddCancelDragAction(const pTBaseAction &Val)
{
	CancelDragActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
void TEventActionDragSpriteToPos::AddFinishDragAction(const pTBaseAction &Val)
{
	FinishDragActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
pTActionList TEventActionDragSpriteToPos::GetStartDragActionsPtr()
{
	return &StartDragActions;
}
///--------------------------------------------------------------------------------------//
pTActionList TEventActionDragSpriteToPos::GetCancelDragActionsPtr()
{
	return &CancelDragActions;
}
///--------------------------------------------------------------------------------------//
pTActionList TEventActionDragSpriteToPos::GetFinishDragActionsPtr()
{
	return &FinishDragActions;
}
///--------------------------------------------------------------------------------------//
pTBaseAction TEventActionDragSpriteToPos::GetStartDragAction(const int &AId)
{
	return StartDragActions.Get(AId);
}
///--------------------------------------------------------------------------------------//
pTBaseAction TEventActionDragSpriteToPos::GetCancelDragAction(const int &AId)
{
	return CancelDragActions.Get(AId);
}
///--------------------------------------------------------------------------------------//
pTBaseAction TEventActionDragSpriteToPos::GetFinishDragAction(const int &AId)
{
	return FinishDragActions.Get(AId);
}
///--------------------------------------------------------------------------------------//
pTBaseAction TEventActionDragSpriteToPos::GetStartDragAction
														(const wstring &AName)
{
	return StartDragActions.Get(AName);
}
///--------------------------------------------------------------------------------------//
pTBaseAction TEventActionDragSpriteToPos::GetCancelDragAction
														(const wstring &AName)
{
	return CancelDragActions.Get(AName);
}
///--------------------------------------------------------------------------------------//
pTBaseAction TEventActionDragSpriteToPos::GetFinishDragAction
														(const wstring &AName)
{
	return FinishDragActions.Get(AName);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
