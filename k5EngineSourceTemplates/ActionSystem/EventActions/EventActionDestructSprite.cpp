#include "EventActionDestructSprite.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionDestructSprite::TEventActionDestructSprite():
						TBaseSpriteEventAction(),
						InitFlag(false),CellSize(16),RenderQueue(NULL),
						SpriteBank(NULL),SpriteBankList(NULL),SpriteBanks(NULL)
{
}
///--------------------------------------------------------------------------------------//
TEventActionDestructSprite::~TEventActionDestructSprite()
{
	if(SpriteBank!=NULL){
		ParticlesWorker.Clear();
		SpriteBankList->Del(SpriteBank->GetID());
	}
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::CreateSpriteBank()
{
	if(SpriteBankList == NULL){
		if(!SpriteBanks->IfExist(L"EventActionDestructSpriteBanks_Pool")){
			SpriteBanks->Add(L"EventActionDestructSpriteBanks_Pool");
			SpriteBankList = SpriteBanks->GetLast();
		}
		else{
			SpriteBankList = SpriteBanks->Get(L"EventActionDestructSpriteBanks_Pool");
		}
	}

	if(SpriteBank==NULL){
		SpriteBankList->Add(new TSpriteBank);
		SpriteBank = SpriteBankList->GetLast();
		SpriteBank->Pos(Sprite->Pos);
		SpriteBank->SetView(false);
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::CreateSprites()
{
	TSpriteKnife Knife;
	Knife.Set(SpriteBank);
	Knife.SetSprite(Sprite);
	Knife.SetCellSize(CellSize);
	Knife.Run();
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::CreateParticles()
{
	int NumElems = SpriteBank->GetSize();

	for(int i=0;i<NumElems;i++){
		TSprite *SpritePoiter = SpriteBank->Get(i);
		TKinematicsSpriteObject *Obj(new TKinematicsSpriteObject);
		Obj->Set(SpritePoiter,SpriteBank);
		ParticlesWorker.Add(Obj);
	}
	ParticlesWorker.Start();
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::Init()
{
	TExceptionGenerator Ex(L"TEventActionDestructSprite: ");

	Ex(RenderQueue!=NULL,	L"in Init() RenderQueue is NULL");
	Ex(Sprite!=NULL,		L"in Init() Sprite is NULL");
	Ex(SpriteBanks!=NULL,	L"in Init() SpriteBanks is NULL");
	Ex(CellSize>0,			L"in Init() CellSize out of range");

	CreateSpriteBank();
	CreateSprites();
	CreateParticles();

	RenderQueue->Sort();
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::ToStart()
{
	Sprite->SetView(false);
	Init();
	SpriteBank->SetView(true);
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::ToStop()
{
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::ToRun(const TEvent &Event)
{
	ParticlesWorker.Run(Event);
	if(ParticlesWorker.IsActive() == false){
		Stop();
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::SetCellSize(const int &Val)
{
	CellSize = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::SetRenderQueue(const pTRenderQueue &Val)
{
	RenderQueue = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::SetSpriteBanks(const pTSpriteBankListManager &Val)
{
	SpriteBanks = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::AddInitiator(const pTKinematicsBaseForce &Val)
{
	ParticlesWorker.AddInitiator(Val);
}
///--------------------------------------------------------------------------------------//
void TEventActionDestructSprite::AddForce(const pTKinematicsBaseForce &Val)
{
	ParticlesWorker.AddForce(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
