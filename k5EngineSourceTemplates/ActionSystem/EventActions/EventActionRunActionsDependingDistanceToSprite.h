///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONRUNACTIONSDEPENDINGDISTANCETOSPRITE_H_INCLUDED
#define EVENTACTIONRUNACTIONSDEPENDINGDISTANCETOSPRITE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class EventActionRunActionsDependingDistanceToSprite:public TBaseSpriteEventAction
{
	protected:
		TActionList	Actions;
		TActionList	StartActions;
		TActionList	StopActions;

		TPoint3D StartPos;
		TPoint3D ControlPos;
		float ControlDistance;
		bool StopIfOnPosFlag;

		enEventTypes EventType;
	protected:
		void ToSet();
		void ToRun(const TEvent &Event);
	public:
		EventActionRunActionsDependingDistanceToSprite();
		virtual ~EventActionRunActionsDependingDistanceToSprite();

		void SetControlPos(const TPoint3D &Val);
		void SetControlDistance(const float &Val);
		void SetStopIfOnPosFlag(const bool &Val);
		void SetEventType(const enEventTypes &Val);

		void AddAction(const pTBaseAction &Val);
		void AddStartActions(const pTBaseAction &Val);
		void AddStopActions(const pTBaseAction &Val);

		pTActionList GetActionsPtr();
		pTActionList GetStartActionsPtr();
		pTActionList GetStopActionsPtr();
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONRUNACTIONSDEPENDINGDISTANCETOSPRITE_H_INCLUDED
