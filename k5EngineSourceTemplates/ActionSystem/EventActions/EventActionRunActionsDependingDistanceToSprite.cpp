#include "EventActionRunActionsDependingDistanceToSprite.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
EventActionRunActionsDependingDistanceToSprite::
	EventActionRunActionsDependingDistanceToSprite():TBaseSpriteEventAction(),
	ControlDistance(0.0f),StopIfOnPosFlag(true),EventType(EN_TIMER)
{
}
///--------------------------------------------------------------------------------------//
EventActionRunActionsDependingDistanceToSprite::
	~EventActionRunActionsDependingDistanceToSprite()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void EventActionRunActionsDependingDistanceToSprite::ToSet()
{
	StartPos = Sprite->Pos;
}
///--------------------------------------------------------------------------------------//
void EventActionRunActionsDependingDistanceToSprite::ToRun(const TEvent &Event)
{
	if(Event.Type != EventType){return;}
}
///--------------------------------------------------------------------------------------//
void EventActionRunActionsDependingDistanceToSprite::SetControlPos
													(const TPoint3D &Val)
{
	ControlPos = Val;
}
///--------------------------------------------------------------------------------------//
void EventActionRunActionsDependingDistanceToSprite::SetControlDistance
													(const float &Val)
{
	ControlDistance = Val;
}
///--------------------------------------------------------------------------------------//
void EventActionRunActionsDependingDistanceToSprite::SetStopIfOnPosFlag
													(const bool &Val)
{
	StopIfOnPosFlag = Val;
}
///--------------------------------------------------------------------------------------//
void EventActionRunActionsDependingDistanceToSprite::SetEventType
													(const enEventTypes &Val)
{
	EventType = Val;
}
///--------------------------------------------------------------------------------------//
void EventActionRunActionsDependingDistanceToSprite::AddAction
													(const pTBaseAction &Val)
{
	Actions.Add(Val);
}
///--------------------------------------------------------------------------------------//
void EventActionRunActionsDependingDistanceToSprite::AddStartActions
													(const pTBaseAction &Val)
{
	StartActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
void EventActionRunActionsDependingDistanceToSprite::AddStopActions
													(const pTBaseAction &Val)
{
	StopActions.Add(Val);
}
///--------------------------------------------------------------------------------------//
pTActionList EventActionRunActionsDependingDistanceToSprite::GetActionsPtr()
{
	return &Actions;
}
///--------------------------------------------------------------------------------------//
pTActionList EventActionRunActionsDependingDistanceToSprite::GetStartActionsPtr()
{
	return &StartActions;
}
///--------------------------------------------------------------------------------------//
pTActionList EventActionRunActionsDependingDistanceToSprite::GetStopActionsPtr()
{
	return &StopActions;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
