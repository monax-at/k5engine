///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///		   Колесников Максим [MPK_Maximus@mail.ru][http://mpkmaximus.blogspot.com]

///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONTEXTEDITOR_H_INCLUDED
#define EVENTACTIONTEXTEDITOR_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5EngineExtensions.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionTextEditor:public TBaseTextEventAction
{
	protected:
		float MaxWidth;
		int   MaxStrLength;
        float CursorBlinkTime;
		pTText TextCursor;

		int TextCursorColorIteratorCounter;
//		TEventActionTextColorIterator TextCursorColorIterators[2];
	protected:
        inline void UpdateTextCursorPos();
		void ProcessTimer(const TEvent &Event);

		void ToStart();
		void ToRun(const TEvent &Event);
		void ToStop();
	public:
		TEventActionTextEditor();
		virtual~TEventActionTextEditor();

		void SetMaxWidth(const float &Val);
		void SetMaxStrLength(const int &Val);

		float GetMaxWidth()const;
		int   GetMaxStrLength() const;

		void SetCursorText(const pTText &Val);

		void SetCursorBlinkTime(const float &Val);
		bool GetCursorBlinkTime();
};

typedef TEventActionTextEditor* pTEventActionTextEditor;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONTEXTEDITOR_H_INCLUDED
