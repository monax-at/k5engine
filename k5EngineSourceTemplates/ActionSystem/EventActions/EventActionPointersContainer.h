///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONPOINTERSCONTAINER_H_INCLUDED
#define EVENTACTIONPOINTERSCONTAINER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionPointersContainer:public TBaseEventAction
{
	protected:
		TEventActionPointerList EventActions;
		TActionList	StartActions;
		TActionList	StopActions;
	protected:
		void ToStart();
		void ToStop();
		void ToRun(const TEvent &Event);
	public:
		TEventActionPointersContainer();
		virtual ~TEventActionPointersContainer();

		void Clear();

		void AddAction(TBaseEventAction *Val);
		void AddStartAction(TBaseAction *Val);
		void AddStopAction(TBaseAction *Val);

		TEventActionPointerList* GetActionsPtr();
		pTActionList GetStartActionsPtr();
		pTActionList GetStopActionsPtr();};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONPOINTERSCONTAINER_H_INCLUDED
