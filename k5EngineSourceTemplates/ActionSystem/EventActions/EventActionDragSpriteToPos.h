///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONDRAGSPRITETOPOS_H_INCLUDED
#define EVENTACTIONDRAGSPRITETOPOS_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionDragSpriteToPos: public TBaseSpriteEventAction
{
	protected:
		TExceptionGenerator Exception;

		pTBaseDevice Device;

		TPoint3D StarPos;
		TPoint3D FinishPos;

		float CheckDistance;

		TActionList StartDragActions;
		TActionList CancelDragActions;
		TActionList FinishDragActions;
	protected:
		void ToStart();
		void ToRun(const TEvent &Event);
	public:
		TEventActionDragSpriteToPos();
		virtual ~TEventActionDragSpriteToPos();

		void SetDevice(const pTBaseDevice &Val);

		void SetCheckDistance(const float &Val);

		void SetFinishPos(const TPoint3D &Val);
		void SetFinishPos(const float &X,const float &Y);
		void SetFinishPos(const float &X,const float &Y,const float &Z);

		void AddStartDragAction (const pTBaseAction &Val);
		void AddCancelDragAction(const pTBaseAction &Val);
		void AddFinishDragAction(const pTBaseAction &Val);

		pTActionList GetStartDragActionsPtr();
		pTActionList GetCancelDragActionsPtr();
		pTActionList GetFinishDragActionsPtr();

		pTBaseAction GetStartDragAction	(const int &AId);
		pTBaseAction GetCancelDragAction(const int &AId);
		pTBaseAction GetFinishDragAction(const int &AId);

		pTBaseAction GetStartDragAction	(const wstring &AName);
		pTBaseAction GetCancelDragAction(const wstring &AName);
		pTBaseAction GetFinishDragAction(const wstring &AName);
};

typedef TEventActionDragSpriteToPos* pTEventActionDragSpriteToPos;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONDRAGSPRITETOPOS_H_INCLUDED
