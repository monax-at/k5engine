#include "EventActionEventReaction.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionEventReaction::TEventActionEventReaction():TBaseEventAction()
{
	ActiveFlag = false;
}
///--------------------------------------------------------------------------------------//
TEventActionEventReaction::~TEventActionEventReaction()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionEventReaction::RunActions()
{
	Actions.Start();
	Actions.Run();
}
///--------------------------------------------------------------------------------------//
void TEventActionEventReaction::RunMouse(const TEvent &Event)
{
	if(Template.Mouse.Type == EN_USER){
		RunActions();
		return;
	}

	switch(Event.Mouse.Type)
	{
		default:break;
		case EN_MOUSEMOTION:{
			if(Template.Mouse.Type == Event.Mouse.Type){
				RunActions();
			}
		}break;

		case EN_MOUSEBUTTONDOWN:{
			if(Template.Mouse.Button==EN_USER){RunActions();}
			else{
				if(Template.Mouse.Type == EN_MOUSEBUTTONDOWN){
					if(Template.Mouse.Button==Event.Mouse.Button){
						RunActions();
					}
				}
			}
		}break;

		case EN_MOUSEBUTTONUP:{
			if(Template.Mouse.Button==EN_USER){RunActions();}
			else{
				if(Template.Mouse.Type == EN_MOUSEBUTTONUP){
					if(Template.Mouse.Button==Event.Mouse.Button){
						RunActions();
					}
				}
			}
		}break;
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionEventReaction::RunKeyboard(const TEvent &Event)
{
	if(Template.Keyboard.Type == EN_USER){
		RunActions();
		return;
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionEventReaction::RunSystem(const TEvent &Event)
{
	if(Template.Keyboard.Type == EN_USER){
		RunActions();
		return;
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionEventReaction::RunTimer(const TEvent &Event)
{
	RunActions();
}
///--------------------------------------------------------------------------------------//
void TEventActionEventReaction::ToRun(const TEvent &Event)
{
	if(Template.Type == EN_USER){
		RunActions();
		return;
	}

	if(Template.Type != Event.Type){return;}

	switch(Event.Type){
		case EN_MOUSE	:{RunMouse(Event);}break;
		case EN_KEYBOARD:{RunMouse(Event);}break;
		case EN_SYSTEM	:{RunMouse(Event);}break;
		case EN_TIMER	:{RunMouse(Event);}break;
		default:break;
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionEventReaction::SetTemplate(const TEvent &Val)
{
	Template = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionEventReaction::AddAction(const pTBaseAction &Val)
{
	Actions.Add(Val);
}
///--------------------------------------------------------------------------------------//
pTActionList TEventActionEventReaction::GetActionsPtr()
{
	return &Actions;
}
///--------------------------------------------------------------------------------------//
void TEventActionEventReaction::Clear()
{
	Actions.Clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
