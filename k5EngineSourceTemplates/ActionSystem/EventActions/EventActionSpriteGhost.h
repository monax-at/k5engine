///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef EVENTACTIONSPRITEGHOST_H_INCLUDED
#define EVENTACTIONSPRITEGHOST_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TEventActionSpriteGhost: public TBaseSpriteEventAction
{
	protected:
		TSprite *Ghost;
		float Distance;
		bool ViewGhost;
		float AlphaCof;
	protected:
		void ToStart();
		void ToStop();
		void ToRun(const TEvent &Event);
	public:
		TEventActionSpriteGhost();
		virtual ~TEventActionSpriteGhost();

		void SetGhost(TSprite *Val);
		void SetDistance(const float &Val);
		void SetAlphaCof(const float &Val);
};

typedef TEventActionSpriteGhost* pTEventActionSpriteGhost;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // EVENTACTIONSPRITEGHOST_H_INCLUDED
