#include "EventActionAttachSpriteToMouse.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionAttachSpriteToMouse::TEventActionAttachSpriteToMouse():
													TBaseSpriteEventAction()
{
	Start();
}
///--------------------------------------------------------------------------------------//
TEventActionAttachSpriteToMouse::~TEventActionAttachSpriteToMouse()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionAttachSpriteToMouse::ToRun(const TEvent &Event)
{
	if(Sprite == NULL){return;}
	if(Event.Type != EN_MOUSE){return;}

	Sprite->Pos(Event.Mouse.X,Event.Mouse.Y);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
