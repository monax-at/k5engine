#include "EventActionFPSCounter.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TEventActionFPSCounter::TEventActionFPSCounter():TBaseTextEventAction(),
					ShowDelay(100.0f),ShowDelayTimer(0.0f),ShowDelayCounter(0),
					MinFps(1000),MaxFps(0),ShowMinFps(false),ShowMaxFps(false),
					MinFpsText(NULL),MaxFpsText(NULL),UpdateFpsTimer(0.0f)

{
}
///--------------------------------------------------------------------------------------//
TEventActionFPSCounter::~TEventActionFPSCounter()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TEventActionFPSCounter::ToStart()
{
	ShowDelayTimer = 0.0f;
	ShowDelayCounter = 0;
	MinFps = 1000;
	MaxFps = 0;
	UpdateFpsTimer = 0.0f;
}
///--------------------------------------------------------------------------------------//
void TEventActionFPSCounter::ToRun(const TEvent &Event)
{
	if(Event.Type != EN_TIMER){return;}

	if(ShowDelay == 0.0f){
		Text->Set((int)(1000.0f/Event.Timer.Ms));
	}
	else{
		ShowDelayTimer+=Event.Timer.Ms;
		ShowDelayCounter++;
		if(ShowDelayTimer>=ShowDelay){

			Text->Set(IntToWStr((int)(1000.0f/( ShowDelayTimer/ShowDelayCounter ))));
			ShowDelayTimer	 = 0.0f;
			ShowDelayCounter = 0;
		}
	}

	if(ShowMinFps == true || ShowMaxFps == true){
		UpdateFpsTimer+=Event.Timer.Ms;
		if(UpdateFpsTimer >= 5000.0f){
			UpdateFpsTimer = 0;
			MinFps = 5000;
			MaxFps = 0;
		}

		if(ShowMinFps == true){
			int Fps((int)(1000.0f/Event.Timer.Ms));
			if(MinFps>Fps){
				MinFps = Fps;
				MinFpsText->Set(MinFps);
			}
		}

		if(ShowMaxFps == true){
			int Fps((int)(1000.0f/Event.Timer.Ms));
			if(MaxFps<Fps){
				MaxFps = Fps;
				MaxFpsText->Set(MaxFps);
			}
		}
	}
}
///--------------------------------------------------------------------------------------//
void TEventActionFPSCounter::SetShowDelay(const float &Val)
{
	ShowDelay = Val;
}
///--------------------------------------------------------------------------------------//
void TEventActionFPSCounter::SetMinFpsText(TText *Val)
{
	MinFpsText = Val;
	if(MinFpsText==NULL){ShowMinFps = false;}
	else				{ShowMinFps = true;}
}
///--------------------------------------------------------------------------------------//
void TEventActionFPSCounter::SetMaxFpsText(TText *Val)
{
	MaxFpsText = Val;
	if(MaxFpsText==NULL){ShowMaxFps = false;}
	else				{ShowMaxFps = true;}
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
