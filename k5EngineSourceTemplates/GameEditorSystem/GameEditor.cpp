#include "GameEditor.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TGameEditor::TGameEditor()
{
//	wsEditorExec   		    = FolderWorker.GetAppPath() + L"GameEditor.exe";
//	IsEditorExists 		    = false;
//	IsCorrectionFileExists  = false;
//	IsLevelsFileExists	    = false;
//	Device					= NULL;
//	HMapFile				= NULL;
//	SpeedId					= 0;
//	WriteString				= "";
//	TimerShift				= 1.0f;
/////--------->
//	if(FolderWorker.IsFileExist(wsEditorExec)) 	IsEditorExists = true;
//	SendData();
}
///--------------------------------------------------------------------------------------//
TGameEditor::~TGameEditor()
{
	CloseHandle(HMapFile);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TGameEditor::StartEditorApp()
{
//	if(IsEditorExists){
//		HWND hwndGameEditor = FindWindow(L"TFormMain", L"Game Editor: Farm Mania 3");
//		if(hwndGameEditor == NULL){
//			ShellExecute(NULL, L"open", wsEditorExec.c_str(), NULL, NULL, SW_SHOW);
//			SendData();
//		}
//	}
}
///--------------------------------------------------------------------------------------//
void TGameEditor::ProcessEvent(TEvent &Event)
{
//	if(IsEditorExists){
//		if(Event.Type == EN_TIMER){
//			Event.Timer.Ms  *= TimerShift;
//			Event.Timer.Sec *= TimerShift;
//		}
//		if(Event.Type == EN_KEYBOARD){
//			if(Event.Keyboard.Key == EN_KEY_F04) StartEditorApp();
//
//			if(Event.Keyboard.Key == EN_KEY_1)     { TimerShift = 1.0f;  SpeedId = 0; SendData(); }
//			if(Event.Keyboard.Key == EN_KEY_2)     { TimerShift = 1.5f;  SpeedId = 1; SendData(); }
//			if(Event.Keyboard.Key == EN_KEY_3)     { TimerShift = 2.0f;  SpeedId = 2; SendData(); }
//			if(Event.Keyboard.Key == EN_KEY_4)     { TimerShift = 3.0f;  SpeedId = 3; SendData(); }
//			if(Event.Keyboard.Key == EN_KEY_0)     { TimerShift = 0.25f; SpeedId = 4; SendData(); }
//		}
//		if(Event.Type == EN_SYSTEM){
//			if(Event.System.Type == EN_EDIT_UPDATE){
//				Device->ResetTimer();
//				if(IsCorrectionFileExists)  DNXmlFile.Load(wsCorrectionFile, &DNCorrection, true);
//				if(IsLevelsFileExists)		DNXmlFile.Load(wsCorrectionFile, &DNLevels, true);
//				ToUpdate();
//			}
//			if(Event.System.Type == EN_EDIT_READ) ReadData();
//		}
//	}
}
///--------------------------------------------------------------------------------------//
void TGameEditor::SendData()
{
//	if(IsEditorExists){
//		HWND hwndGameEditor = FindWindow(L"TFormMain", L"Game Editor: Farm Mania 3");
//		if(hwndGameEditor != NULL){
//			HMapFile = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE,0 , 1024, L"GameEditorMapFile");
//			if(HMapFile != INVALID_HANDLE_VALUE){
//				char *MappedFileData = (char*) MapViewOfFile(HMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0);
//				if(MappedFileData != NULL){
//					string WriteString = "SpeedId=" + IntToStr(SpeedId) + ",";
//					memcpy(MappedFileData,WriteString.c_str(),WriteString.size());
//					UnmapViewOfFile(MappedFileData);
//					PostMessage(hwndGameEditor, WM_APP_READ, 0, 0);
//				}
//			}
//		}
//	}
}
///--------------------------------------------------------------------------------------//
void TGameEditor::ReadData()
{
//	if(IsEditorExists){
//		HWND hwndGameEditor = FindWindow(L"TFormMain", L"Game Editor: Farm Mania 3");
//		if(hwndGameEditor != NULL){
//			char cBuffer[1024];
//			HMapFile = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE,0 , 1024, L"GameEditorMapFile");
//			if(HMapFile != INVALID_HANDLE_VALUE){
//				char *MappedFileData = (char*) MapViewOfFile(HMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0);
//				if(MappedFileData != NULL){
//					memcpy(&cBuffer,MappedFileData,sizeof(cBuffer));
//					UnmapViewOfFile(MappedFileData);
/////----------->
//					stringstream strParseComma(cBuffer);
//					string 		 ValueString("");
//					while(getline(strParseComma, ValueString, ',')) {
//						if(ValueString.size() > 0){
//							stringstream strParseEqual (ValueString);
//							string Name  = "";
//							string Value = "" ;
//							getline(strParseEqual, Name,  '=');
//							getline(strParseEqual, Value, '=');
//
//							if(Name == "SpeedId") SetTimeShiftById(StrToInt(Value));
//						}
//					}
//				}
//			}
//		}
//	}
}
///--------------------------------------------------------------------------------------//
void TGameEditor::SetTimeShiftById(const int &Val)
{
//	if(Val == 0) TimerShift = 1.0f;
//	if(Val == 1) TimerShift = 1.5f;
//	if(Val == 2) TimerShift = 2.0f;
//	if(Val == 3) TimerShift = 3.0f;
//	if(Val == 4) TimerShift = 0.25f;
}
///--------------------------------------------------------------------------------------//
void TGameEditor::SetDevice(const pTDevice &Val)
{
	Device = Val;
}
///--------------------------------------------------------------------------------------//
void TGameEditor::SetCorrectionFile(const wstring &FileName)
{
//	if(FolderWorker.IsFileExist(FileName)){
//		IsCorrectionFileExists = true;
//		wsCorrectionFile	   = FileName;
//		DNXmlFile.Load(wsCorrectionFile, &DNCorrection, true);
//	}
}
///--------------------------------------------------------------------------------------//
void TGameEditor::SetLevelsFile(const wstring &FileName)
{
//	if(FolderWorker.IsFileExist(FileName)){
//		IsLevelsFileExists  = true;
//		wsLevelsFile		= FileName;
//		DNXmlFile.Load(wsCorrectionFile, &DNLevels, true);
//	}
}
///--------------------------------------------------------------------------------------//
pTDataNode TGameEditor::GetDNCorrection()
{
	return(&DNCorrection);
}
///--------------------------------------------------------------------------------------//
pTDataNode TGameEditor::GetDNLevels()
{
	return(&DNLevels);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
