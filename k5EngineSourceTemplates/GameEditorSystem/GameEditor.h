///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
/// Аторы:
///		Колесников Максим [mpkmaximus@gmail.com][http://mpkmaximus.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef GAMEEDITOR_H_INCLUDED
#define GAMEEDITOR_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include <windows.h>
///--------------------------------------------------------------------------------------//
#include "DNXmlFile.h"
#include "K5EngineExtensions.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TGameEditor
{
	private:
		HANDLE 		   HMapFile;

		TDNXmlFile 	   DNXmlFile;

		TDataNode	   DNCorrection;
		TDataNode	   DNLevels;

		wstring 	   wsEditorExec;
		wstring 	   wsCorrectionFile;
		wstring 	   wsLevelsFile;
		TFolderWorker  FolderWorker;

		bool 		   IsEditorExists;
		bool 		   IsCorrectionFileExists;
		bool 		   IsLevelsFileExists;

		pTDevice	   Device;

		int			   SpeedId;
		string 		   WriteString;
		float		   TimerShift;

		void StartEditorApp();
		void SendData();
		void ReadData();

		void SetTimeShiftById(const int &Val);
	protected:
		virtual void ToUpdate() = 0;
	public:
		TGameEditor();
		virtual ~TGameEditor();
///------>
		void SetDevice(const pTDevice &Val);
		void SetCorrectionFile(const wstring &FileName);
		void SetLevelsFile(const wstring &FileName);
///------>
		void ProcessEvent(TEvent &Event);
///------>
		pTDataNode GetDNCorrection();
		pTDataNode GetDNLevels();

};
typedef TGameEditor *pTGameEditor;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // GAMEEDITOR_H_INCLUDED
