///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef KINEMATICSSPRITEOBJECTGENERATOR_H_INCLUDED
#define KINEMATICSSPRITEOBJECTGENERATOR_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsSpriteManager.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsSpriteObjectGenerator:public TBaseSpriteBankAction
{
	protected:
		TExceptionGenerator 		Exception;

		TKinematicsSpriteObject 	TemplateObject;

		TSpriteBank 				TempPool;
		pTBaseSpriteBankAction		SpritesGenerator;
		pTKinematicsSpriteManager	KinematicsManager;

		TKinematicsForcesList Initiators;
		pTKinematicsForcesList ExternalInitiators;
	protected:
		void ToSet();
		void ToRun();
	public:
		TKinematicsSpriteObjectGenerator();
		virtual ~TKinematicsSpriteObjectGenerator();

		void SetSpritesGenerator(const pTBaseSpriteBankAction &Val);
		void SetSpritesGenerator(const pTBaseAction &Val);

		void SetKinematicsManager(const pTKinematicsSpriteManager &Val);
		void SetKinematicsManager(const pTBaseEventAction &Val);

		void SetTemplate(const TKinematicsSpriteObject &Val);

		void SetTemplateActive(const bool &Val);
		void SetTemplateLive(const bool &Val);

		void SetTemplateActivationDelay(const float &Val);
		void SetTemplateLiveTime(const float &Val);

		void SetTemplateSpeed(const float &Val);

		void SetTemplateMoveDirection(const float &x,const float &y);
		void SetTemplateMoveDirection(const TVector2D &Val);

		void SetTemplateEnableChangeColor(const bool &Val);

		void SetTemplateColorSpeed(const TColor &Val);
		void SetTemplateColorSpeed(	const float &r,const float &g,
									const float &b,const float &a);

		void SetTemplateBlinkColorMinAlpha(const float &Val);
		void SetTemplateBlinkColorMaxAlpha(const float &Val);

		void SetTemplateEnableBlinkColor(const bool &Val);
		void SetTemplateBlinkColorCount(const int &Val);

		void SetTemplateColorSpeedDirection(const enChangeValueDirection &Val);

		void SetTemplateEnableAngleSpeed(const bool &Val);
		void SetTemplateAngleSpeed(const float &Val);

		void AddInitiator(const pTKinematicsBaseForce &Val);
		void ClearInitiator();

		void SetExternalInitiators(const pTKinematicsForcesList &Val);
};

typedef TKinematicsSpriteObjectGenerator* pTKinematicsSpriteObjectGenerator;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICSSPRITEOBJECTGENERATOR_H_INCLUDED
