#include "KinematicsSpriteManager.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsSpriteManager::TKinematicsSpriteManager():TBaseEventAction(),
			AutoRunFlag(true),PosBorderFlag(true),DeadCounter(0)
{
	// проверка частиц на выход за границу экрана
	CutPosFlags[0]=true; // left
	CutPosFlags[1]=true; // right
	CutPosFlags[2]=true; // top
	CutPosFlags[3]=true; // bottom

	CutPosLimits[0] = 0.0f; 	// left
	CutPosLimits[1] = 1024.0f;	// right
	CutPosLimits[2] = 768.0f;	// top
	CutPosLimits[3] = 0.0f;		// bottom

}
///--------------------------------------------------------------------------------------//
TKinematicsSpriteManager::~TKinematicsSpriteManager()
{
	// важное отличие от функции ClearData(), так как элементы используют ссылки
	// на спрайты, то при удалении элементов вместе с спрайтами может произойти
	// ошибка - так как банки спрайтов могут быть удалены раньше, чем элементы
	// кинематики, а ссылки на спрайты останутся, то произойдёт попытка удалить
	// уже удалённые спрайты. Поэтому нужно удалить только сами элементы
	// кинематики без удаления спрайтов. Ну а сами спрайты будут удалены вместе
	// со своим банком.
	// TODO: надо подумать по поводу системы подсчёта ссылок, так как всё же
	// она может очень помочь вылавливать некоторые ошибки, например: удаление
	// банка спрайтов, который в данный момент используется как пул частиц
	Clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::CheckPosBorder(const pTKinematicsSpriteObject &Object)
{
	if(PosBorderFlag  == false){return;}

	TSprite *Sprite(Object->GetSprite());
	if(Sprite == NULL){return;}

	float Width (Sprite->Square.GetWidth());
	float Height(Sprite->Square.GetHeight());
	float X(Sprite->Pos.GetX());
	float Y(Sprite->Pos.GetY());

	TPoint3D PoolPos(Object->GetSpritePool()->Pos);

	// left
	if(CutPosFlags[0] == true){
		if( X < CutPosLimits[0] - PoolPos[0] - Width/2.0f){
			Object->SetLive(false);
		}
	}

	// right
	if(CutPosFlags[1] == true){
		if( X > CutPosLimits[1] - PoolPos[0] + Width/2.0f){
			Object->SetLive(false);
		}
	}

	// top
	if(CutPosFlags[2] == true){
		if( Y > CutPosLimits[2] - PoolPos[1] + Height/2.0f){
			Object->SetLive(false);
		}
	}

	// bottom
	if(CutPosFlags[3] == true){
		if( Y < CutPosLimits[3] - PoolPos[1] - Height/2.0f){
			Object->SetLive(false);
		}
	}
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::ClearDeadObject()
{
	if(DeadCounter==0){return;}

	// зачем нужна такая конструкция: так как при каждом удалении объекта
	// будет изменяться размер контейнера, и по старым данным будет ошибочно
	// искать объекты на удаление.
	for(int i=0;i<DeadCounter;i++){
		int Size(Elems.size());
		for(int j=0; j<Size; j++){
			if(Elems[j]->IsLive() == false){
				Elems[j]->Del();
				delete Elems[j];
				Elems.erase(Elems.begin()+j);
				break;
			}
		}
	}
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::ToRun(const TEvent &Event)
{
	if(Event.Type != EN_TIMER){return;}

	DeadCounter = 0;

	float Time(Event.Timer.Ms);

	// обход списка объектов, применение к активным сил и обновление их состояния
	int Size(Elems.size());
	for(int i=0; i<Size; i++){
		TKinematicsSpriteObject *Object(Elems[i]);

		if(Object->CheckActivation(Time) == true){
			Forces(Object,Time);
			Object->Update(Time);
			CheckPosBorder(Object);

			if(Object->IsLive() == false){
				DeadCounter++;
			}
		}
	}

	// отчистка отработавших объектов
	ClearDeadObject();

	if(AutoRunFlag == true){
		if(Elems.size()==0){Stop();}
	}
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::SetAutoRunFlag(const bool &Val)
{
	AutoRunFlag = Val;
}
///--------------------------------------------------------------------------------------//
bool TKinematicsSpriteManager::GetAutoRunFlag() const
{
	return AutoRunFlag;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::SetPosBorderFlag(const bool &Val)
{
	PosBorderFlag = Val;
}
///--------------------------------------------------------------------------------------//
bool TKinematicsSpriteManager::GetPosBorder() const
{
	return PosBorderFlag;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::SetCutPosFlags(	const bool &Left,const bool &Right,
												const bool &Top,const bool &Bottom)
{
	CutPosFlags[0] = Left;
	CutPosFlags[1] = Right;
	CutPosFlags[2] = Top;
	CutPosFlags[3] = Bottom;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::SetCutPosLimits(const float &Left,const float &Right,
											   const float &Top,const float &Bottom)
{
	CutPosLimits[0] = Left;
	CutPosLimits[1] = Right;
	CutPosLimits[2] = Top;
	CutPosLimits[3] = Bottom;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::Add(const pTKinematicsSpriteObject &Val)
{
	Elems.push_back(Val);
	Initiators(Val,1000.0f);

	if(AutoRunFlag == true){ Start(); }
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::AddInitiator(const pTKinematicsBaseForce &Val)
{
	Initiators.Add(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::AddForce(const pTKinematicsBaseForce &Val)
{
	Forces.Add(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::Clear()
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){ delete Elems[i]; }
	Elems.clear();
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteManager::ClearData()
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){
		Elems[i]->Del();
		delete Elems[i];
	}
	Elems.clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
