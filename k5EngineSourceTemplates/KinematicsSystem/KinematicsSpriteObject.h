///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef KINEMATICSSPRITEOBJECT_H_INCLUDED
#define KINEMATICSSPRITEOBJECT_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsSpriteObject:public TKinematicsBaseObject
{
	protected:
		pTSprite 	 Sprite;
//		pTSpriteBank SpritePool;

		bool 		EnableChangeColor;
		TColor		StartColor;
		TColor		ColorSpeed;
		enChangeValueDirection	ColorSpeedDirection;
		bool		EnableBlinkColor;
		int 		BlinkColorCount;
		int 		BlinkColorCounter;

		float BlinkColorAlphaMin;
		float BlinkColorAlphaMax;

		bool EnableSizeSpeed;

		bool  EnableAngleSpeed;
		float AngleSpeed;

		bool  EnableScaleSpeed;
		float Scale;
		float ScaleSpeed;
		TPoint2D StartSize;
	protected:
		inline void ToSetSprite();
		inline void UpdateSpritePos	();

		inline void UpdateColor		(const float &Time);
		inline void UpdateAngle		(const float &Time);
		inline void UpdateSize		(const float &Time);

		void ToUpdate(const float &Time);
		void ToDel();
	public:
		TKinematicsSpriteObject();
		virtual ~TKinematicsSpriteObject();

		void Set(const pTSprite &SpriteVal,const pTSpriteBank &SpritePoolVal);
		void SetSprite(const pTSprite &Val);
		void SetSpritePool(const pTSpriteBank &Val);

		TSprite* GetSprite();
		TSpriteBank* GetSpritePool();

		void SetEnableChangeColor(const bool &Val);
		bool GetEnableChangeColor();

		void SetColorSpeed(const TColor &Val);
		void SetColorSpeed(	const float &r,const float &g,
							const float &b,const float &a);

		void SetColorSpeedDirection(const enChangeValueDirection &Val);
		enChangeValueDirection GetColorSpeedDirection();

		void SetEnableBlinkColor(const bool &Val);
		bool GetEnableBlinkColor();

		void SetBlinkColorCount(const int &Val);
		int GetBlinkColorCount() const;

		void SetBlinkColorAlphaRange(const float &Min,const float &Max);
		void SetBlinkColorAlphaMin(const float &Val);
		void SetBlinkColorAlphaMax(const float &Val);
		float GetBlinkColorAlphaMin();
		float GetBlinkColorAlphaMax();

		TColor GetColorSpeed();

		void SetEnableAngleSpeed(const bool &Val);
		bool GetEnableAngleSpeed();

		void SetAngleSpeed(const float &Val);
		float GetAngleSpeed();

		void SetEnableScaleSpeed(const bool &Val);
		bool GetEnableScaleSpeed() const;

		void SetScaleSpeed(const float &Val);
		float GetScaleSpeed() const;
};

typedef TKinematicsSpriteObject* pTKinematicsSpriteObject;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICSPRITEOBJECT_H_INCLUDED
