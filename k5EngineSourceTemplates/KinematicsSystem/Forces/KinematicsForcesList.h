#pragma once
#ifndef KINEMATICSFORCESLIST_H_INCLUDED
#define KINEMATICSFORCESLIST_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseForce.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsForcesList
{
	protected:
		vector<pTKinematicsBaseForce> Elems;
	protected:
		inline void UseForces(	const pTKinematicsBaseObject &Object,
								const float &Time);
	public:
		TKinematicsForcesList();
		~TKinematicsForcesList();

		void operator()(const pTKinematicsBaseObject &Object,const float &Time);
		void Run(const pTKinematicsBaseObject &Object,const float &Time);

		void Add(const pTKinematicsBaseForce &Force);
		void Clear();

		int GetSize();
};

typedef TKinematicsForcesList* pTKinematicsForcesList;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICSFORCESLIST_H_INCLUDED
