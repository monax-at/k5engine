#pragma once
#ifndef KINEMATICSPARTICLEWEIGHTRANDRANGE_H_INCLUDED
#define KINEMATICSPARTICLEWEIGHTRANDRANGE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseForce.h"
#include "../KinematicsSpriteObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsParticleWeightRandRange:public TKinematicsBaseForce
{
	protected:
		float Min;
		float Max;
	public:
		TKinematicsParticleWeightRandRange();
		virtual ~TKinematicsParticleWeightRandRange();

		void Run(const pTKinematicsBaseObject &Object,const float &Time);

		void Set(const float &MinVal,const float &MaxVal);
		void SetMin(const float &Val);
		void SetMax(const float &Val);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // LIVERANDRANGEFORCE_H_INCLUDED
