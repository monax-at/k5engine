///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef KINEMATICSDIRECTEDFORCE_H_INCLUDED
#define KINEMATICSDIRECTEDFORCE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseForce.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsDirectedForce:public TKinematicsBaseForce
{
	protected:
		float Value;
		TPoint2D Pos;
	public:
		TKinematicsDirectedForce();
		virtual ~TKinematicsDirectedForce();

		void Run(const pTKinematicsBaseObject &Object,const float &Time);

		void SetVal(const float &Val);
		void SetPos(const float &x,const float &y);
		void SetPos(const TPoint2D &Val);
		void SetPos(const TPoint3D &Val);
};

typedef TKinematicsDirectedForce* pTKinematicsDirectedForce;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICSDIRECTEDFORCE_H_INCLUDED
