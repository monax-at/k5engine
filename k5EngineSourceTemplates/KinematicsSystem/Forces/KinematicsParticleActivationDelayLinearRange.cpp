#include "KinematicsParticleActivationDelayLinearRange.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsParticleActivationDelayLinearRange::
					TKinematicsParticleActivationDelayLinearRange():
					TKinematicsBaseForce(),Min(0.0f),Step(0.0f),StepCounter(0.0f),
					IterationsCounter(0),IterationsMax(0)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsParticleActivationDelayLinearRange::
								~TKinematicsParticleActivationDelayLinearRange()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsParticleActivationDelayLinearRange::Run
						(const pTKinematicsBaseObject &Object,const float &Time)
{
	Object->SetActivationDelay(Min+StepCounter);
	StepCounter+=Step;

	IterationsCounter++;
	if(IterationsCounter>=IterationsMax){
		IterationsCounter = 0;
		StepCounter = 0.0f;
	}
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleActivationDelayLinearRange::SetMin(const float &Val)
{
	Min = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleActivationDelayLinearRange::SetStep(const float &Val)
{
	Step = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleActivationDelayLinearRange::SetIterationsLimit
															(const int &Val)
{
	IterationsMax = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleActivationDelayLinearRange::ResetCounters()
{
	IterationsCounter = 0;
	StepCounter = 0.0f;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
