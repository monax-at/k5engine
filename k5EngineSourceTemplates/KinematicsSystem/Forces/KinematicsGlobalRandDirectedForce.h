///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef KINEMATICSGLOBALRANDDIRECTEDFORCE_H_INCLUDED
#define KINEMATICSGLOBALRANDDIRECTEDFORCE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseForce.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsGlobalRandDirectedForce:public TKinematicsBaseForce
{
	protected:
		float Value;
		TVector2D MinDir;
		TVector2D MaxDir;
	public:
		TKinematicsGlobalRandDirectedForce();
		virtual ~TKinematicsGlobalRandDirectedForce();

		void Run(const pTKinematicsBaseObject &Object,const float &Time);

		void SetVal(const float &Val);

		void SetMinDir(const float &X,const float &Y);
		void SetMaxDir(const float &X,const float &Y);

		void SetMinDir(const TPoint2D &Val);
		void SetMaxDir(const TPoint2D &Val);

		void SetMinDir(const TVector2D &Val);
		void SetMaxDir(const TVector2D &Val);
};

typedef TKinematicsGlobalRandDirectedForce* pTKinematicsGlobalRandDirectedForce;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICSGLOBALRANDDIRECTEDFORCE_H_INCLUDED
