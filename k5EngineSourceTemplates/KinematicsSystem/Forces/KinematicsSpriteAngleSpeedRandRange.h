#pragma once
#ifndef KINEMATICSSPRITEANGLESPEEDRANDRANGE_H_INCLUDED
#define KINEMATICSSPRITEANGLESPEEDRANDRANGE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseForce.h"
#include "../KinematicsSpriteObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsSpriteAngleSpeedRandRange:public TKinematicsBaseForce
{
	protected:
		int Min;
		int Max;
	public:
		TKinematicsSpriteAngleSpeedRandRange();
		virtual ~TKinematicsSpriteAngleSpeedRandRange();

		void Run(const pTKinematicsBaseObject &Object,const float &Time);

		void Set(const int &MinVal,const int &MaxVal);
		void SetMin(const int &Val);
		void SetMax(const int &Val);
};

typedef TKinematicsSpriteAngleSpeedRandRange*
								pTKinematicsSpriteAngleSpeedRandRange;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // LIVERANDRANGEFORCE_H_INCLUDED
