#include "KinematicsSpriteColorSpeedRandRange.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsSpriteColorSpeedRandRange::TKinematicsSpriteColorSpeedRandRange():
										TKinematicsBaseForce(),Dir(EN_CVD_Dec)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsSpriteColorSpeedRandRange::~TKinematicsSpriteColorSpeedRandRange()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteColorSpeedRandRange::Run(const pTKinematicsBaseObject &Object,
											   const float &Time)
{
	pTKinematicsSpriteObject Obj(dynamic_cast<pTKinematicsSpriteObject>(Object));

	float DeltaTime = Time/1000.0f;
	int RangeModif 	= GenRandValue(101);
	float MinRangeModif = (100.0f - RangeModif)/100.0f;
	float MaxRangeModif = RangeModif/100.0f;
	TColor Res;

	for(int i=0;i<4;i++){
		Res(i, (MinRangeModif * Min.Get(i) + MaxRangeModif *Max.Get(i)) * DeltaTime);
	}

	Obj->SetColorSpeed(Res);
	Obj->SetColorSpeedDirection(Dir);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteColorSpeedRandRange::SetMin(const TColor &Val)
{
	Min(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteColorSpeedRandRange::SetMax(const TColor &Val)
{
	Max(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteColorSpeedRandRange::SetMin(	const float &r,const float &g,
													const float &b,const float &a)
{
	Min(r,g,b,a);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteColorSpeedRandRange::SetMax(	const float &r,const float &g,
													const float &b,const float &a)
{
	Max(r,g,b,a);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteColorSpeedRandRange::SetDir(const enChangeValueDirection &Val)
{
	Dir = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//

