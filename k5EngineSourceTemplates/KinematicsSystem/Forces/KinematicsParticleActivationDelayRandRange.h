#pragma once
#ifndef KINEMATICSPARTICLEACTIVATIONDELAYRANDRANGE_H_INCLUDED
#define KINEMATICSPARTICLEACTIVATIONDELAYRANDRANGE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseForce.h"
#include "../KinematicsSpriteObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsParticleActivationDelayRandRange:public TKinematicsBaseForce
{
	protected:
		float Min;
		float Max;
	public:
		TKinematicsParticleActivationDelayRandRange();
		virtual ~TKinematicsParticleActivationDelayRandRange();

		void Run(const pTKinematicsBaseObject &Object,const float &Time);

		void Set(const float &MinVal,const float &MaxVal);
		void SetMin(const float &Val);
		void SetMax(const float &Val);
};

typedef TKinematicsParticleActivationDelayRandRange*
									pTKinematicsParticleActivationDelayRandRange;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICSPARTICLEACTIVATIONDELAYRANDRANGE_H_INCLUDED
