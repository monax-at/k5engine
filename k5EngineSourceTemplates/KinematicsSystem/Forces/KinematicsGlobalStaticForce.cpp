#include "KinematicsGlobalStaticForce.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsGlobalStaticForce::TKinematicsGlobalStaticForce():
														TKinematicsBaseForce()
{
}
///--------------------------------------------------------------------------------------//
TKinematicsGlobalStaticForce::~TKinematicsGlobalStaticForce()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalStaticForce::Run(	const pTKinematicsBaseObject &Object,
										const float &Time)
{
	TVector2D ObjectSpeedCof(Object->GetMoveDirection());
	ObjectSpeedCof*=Object->GetSpeed();

	TVector2D ForceSpeedCof(Direction);
	ForceSpeedCof*=Value*(Time/1000.0f);

	TVector2D Res(ObjectSpeedCof+ForceSpeedCof);

	Object->SetSpeed(Res.GetLength());

	Res.Normalize();
	Object->SetMoveDirection(Res);
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalStaticForce::SetVal(const float &Val)
{
	Value = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalStaticForce::SetDir(const TVector2D &Val)
{
	Direction(Val);
	Direction.Normalize();
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalStaticForce::SetDir(const float &x,const float &y)
{
	Direction(x,y);
	Direction.Normalize();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
