///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef KINEMATICSGLOBALSTATICFORCE_H_INCLUDED
#define KINEMATICSGLOBALSTATICFORCE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseForce.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsGlobalStaticForce:public TKinematicsBaseForce
{
	protected:
		TVector2D Direction;
		float Value;
	public:
		TKinematicsGlobalStaticForce();
		virtual ~TKinematicsGlobalStaticForce();

		void Run(const pTKinematicsBaseObject &Object,const float &Time);

		void SetVal(const float &Val);
		void SetDir(const TVector2D &Val);
		void SetDir(const float &x,const float &y);
};
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICSGLOBALSTATICFORCE_H_INCLUDED
