#include "KinematicsForcesList.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsForcesList::TKinematicsForcesList()
{
}
///--------------------------------------------------------------------------------------//
TKinematicsForcesList::~TKinematicsForcesList()
{
	Clear();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsForcesList::UseForces(	const pTKinematicsBaseObject &Object,
										const float &Time)
{
	int Size = Elems.size();

	for(int i=0;i<Size;i++){
		Elems[i]->Run(Object,Time);
	}
}
///--------------------------------------------------------------------------------------//
void TKinematicsForcesList::operator()(	const pTKinematicsBaseObject &Object,
										const float &Time)
{
	UseForces(Object,Time);
}
///--------------------------------------------------------------------------------------//
void TKinematicsForcesList::Run(const pTKinematicsBaseObject &Object,
								const float &Time)
{
	UseForces(Object,Time);
}
///--------------------------------------------------------------------------------------//
void TKinematicsForcesList::Add(const pTKinematicsBaseForce &Force)
{
	Elems.push_back(Force);
}
///--------------------------------------------------------------------------------------//
void TKinematicsForcesList::Clear()
{
	int Size = Elems.size();
	for(int i=0;i<Size;i++){ delete Elems[i]; }
	Elems.clear();
}
///--------------------------------------------------------------------------------------//
int TKinematicsForcesList::GetSize()
{
	return Elems.size();
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//

