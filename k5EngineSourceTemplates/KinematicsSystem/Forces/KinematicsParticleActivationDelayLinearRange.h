#pragma once
#ifndef KINEMATICSPARTICLEACTIVATIONDELAYLINEARRANGE_H_INCLUDED
#define KINEMATICSPARTICLEACTIVATIONDELAYLINEARRANGE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseForce.h"
#include "../KinematicsSpriteObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsParticleActivationDelayLinearRange:public TKinematicsBaseForce
{
	protected:
		float Min;

		float Step;
		float StepCounter;

		int IterationsCounter;
		int IterationsMax;
	public:
		TKinematicsParticleActivationDelayLinearRange();
		virtual ~TKinematicsParticleActivationDelayLinearRange();

		void Run(const pTKinematicsBaseObject &Object,const float &Time);

		void SetMin(const float &Val);
		void SetStep(const float &Val);
		void SetIterationsLimit(const int &Val);
		void ResetCounters();
};

typedef TKinematicsParticleActivationDelayLinearRange*
								pTKinematicsParticleActivationDelayLinearRange;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICSPARTICLEACTIVATIONDELAYLINEARRANGE_H_INCLUDED
