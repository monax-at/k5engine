#include "KinematicsParticleWeightRandRange.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsParticleWeightRandRange::TKinematicsParticleWeightRandRange():
										TKinematicsBaseForce(),Min(1.0f),Max(1.0f)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsParticleWeightRandRange::~TKinematicsParticleWeightRandRange()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsParticleWeightRandRange::Run(const pTKinematicsBaseObject &Object,
											 const float &Time)
{
	pTKinematicsSpriteObject Obj(dynamic_cast<pTKinematicsSpriteObject>(Object));

	float Val(GenRandRangeValue((int)(Min*100.0f),(int)(Max*100.0f))/100.0f);
	Obj->SetWeight(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleWeightRandRange::Set(	const float &MinVal,
												const float &MaxVal)
{
	Min = MinVal;
	Max = MaxVal;
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleWeightRandRange::SetMin(const float &Val)
{
	Min = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleWeightRandRange::SetMax(const float &Val)
{
	Max = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
