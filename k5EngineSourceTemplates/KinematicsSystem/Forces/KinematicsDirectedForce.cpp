#include "KinematicsDirectedForce.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsDirectedForce::TKinematicsDirectedForce():TKinematicsBaseForce(),
																Value(0.0f)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsDirectedForce::~TKinematicsDirectedForce()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsDirectedForce::Run(	const pTKinematicsBaseObject &Object,
									const float &Time)
{
	TPoint3D ObjectPos(Object->GetCurrentPos());
	TVector2D ForceSpeedCof(ObjectPos[0]-Pos[0],ObjectPos[1]-Pos[1]);

	// проверка, когда объект и сила находятся в одной точке.
	if(ForceSpeedCof.GetLength() == 0.0f){
		int RandDir(GenRandValue(4));
		switch(RandDir){
			case 0: ForceSpeedCof( 0.0f, 1.0f); break;
			case 1: ForceSpeedCof( 0.0f,-1.0f); break;
			case 2: ForceSpeedCof( 1.0f, 0.0f); break;
			case 3: ForceSpeedCof(-1.0f, 0.0f); break;
		}
	}

	ForceSpeedCof.Normalize();
	ForceSpeedCof*=Value*(Time/1000.0f);

	TVector2D ObjectSpeedCof(Object->GetMoveDirection());
	ObjectSpeedCof*=Object->GetSpeed();

	TVector2D Res(ObjectSpeedCof+ForceSpeedCof);

	Object->SetSpeed(Res.GetLength());

	Res.Normalize();
	Object->SetMoveDirection(Res);
}
///--------------------------------------------------------------------------------------//
void TKinematicsDirectedForce::SetVal(const float &Val)
{
	Value = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsDirectedForce::SetPos(const float &x,const float &y)
{
	Pos(x,y);
}
///--------------------------------------------------------------------------------------//
void TKinematicsDirectedForce::SetPos(const TPoint2D &Val)
{
	Pos(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsDirectedForce::SetPos(const TPoint3D &Val)
{
	Pos(Val.GetX(),Val.GetY());
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//

