#include "KinematicsParticleActivationDelayRandRange.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsParticleActivationDelayRandRange::
									TKinematicsParticleActivationDelayRandRange():
									TKinematicsBaseForce(),Min(0.0f),Max(0.0f)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsParticleActivationDelayRandRange::
									~TKinematicsParticleActivationDelayRandRange()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsParticleActivationDelayRandRange::Run
						(const pTKinematicsBaseObject &Object,const float &Time)
{
	Object->SetActivationDelay((float)GenRandRangeValue((int)Min, (int)Max));
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleActivationDelayRandRange::Set(	const float &MinVal,
														const float &MaxVal)
{
	Min = MinVal;
	Max = MaxVal;
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleActivationDelayRandRange::SetMin(const float &Val)
{
	Min = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsParticleActivationDelayRandRange::SetMax(const float &Val)
{
	Max = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
