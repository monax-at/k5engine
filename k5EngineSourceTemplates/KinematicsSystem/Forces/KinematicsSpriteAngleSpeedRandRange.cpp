#include "KinematicsSpriteAngleSpeedRandRange.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsSpriteAngleSpeedRandRange::TKinematicsSpriteAngleSpeedRandRange():
										TKinematicsBaseForce(),Min(0),Max(0)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsSpriteAngleSpeedRandRange::~TKinematicsSpriteAngleSpeedRandRange()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteAngleSpeedRandRange::Run
						(const pTKinematicsBaseObject &Object,const float &Time)
{
	pTKinematicsSpriteObject Obj(dynamic_cast<pTKinematicsSpriteObject>(Object));
	Obj->SetAngleSpeed(GenRandRangeValue(Min,Max));
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteAngleSpeedRandRange::Set(	const int &MinVal,
												const int &MaxVal)
{
	Min = MinVal;
	Max = MaxVal;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteAngleSpeedRandRange::SetMin(const int &Val)
{
	Min = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteAngleSpeedRandRange::SetMax(const int &Val)
{
	Max = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
