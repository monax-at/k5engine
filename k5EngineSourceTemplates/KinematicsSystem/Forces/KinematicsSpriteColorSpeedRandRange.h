///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef KINEMATICSSPRITECOLORSPEEDRANDRANGE_H_INCLUDED
#define KINEMATICSSPRITECOLORSPEEDRANDRANGE_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "KinematicsBaseForce.h"
#include "../KinematicsSpriteObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsSpriteColorSpeedRandRange:public TKinematicsBaseForce
{
	protected:
		TColor Min;
		TColor Max;
		enChangeValueDirection Dir;
	public:
		TKinematicsSpriteColorSpeedRandRange();
		virtual ~TKinematicsSpriteColorSpeedRandRange();

		void Run(const pTKinematicsBaseObject &Object,const float &Time);

		void SetMin(const TColor &Val);
		void SetMax(const TColor &Val);
		void SetMin(const float &r,const float &g,const float &b,const float &a);
		void SetMax(const float &r,const float &g,const float &b,const float &a);

		void SetDir(const enChangeValueDirection &Val);
};

typedef TKinematicsSpriteColorSpeedRandRange* pTKinematicsSpriteColorSpeedRandRange;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICSSPRITECOLORSPEEDRANDRANGE_H_INCLUDED
