#include "KinematicsGlobalRandDirectedForce.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsGlobalRandDirectedForce::TKinematicsGlobalRandDirectedForce():
									TKinematicsBaseForce(),Value(0.0f)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsGlobalRandDirectedForce::~TKinematicsGlobalRandDirectedForce()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalRandDirectedForce::Run(const pTKinematicsBaseObject &Object,
											 const float &Time)
{
	float DirX(GenRandRangeValue((int)(MinDir[0]*100.0f), (int)(MaxDir[0]*100.0f)));
	float DirY(GenRandRangeValue((int)(MinDir[1]*100.0f), (int)(MaxDir[1]*100.0f)));

	TVector2D Direction(DirX, DirY);
	Direction.Normalize();

	TVector2D ObjectSpeedCof(Object->GetMoveDirection());
	ObjectSpeedCof*=Object->GetSpeed();

	TVector2D ForceSpeedCof(Direction);
	ForceSpeedCof*=Value*(Time/1000.0f);

	TVector2D Res(ObjectSpeedCof+ForceSpeedCof);

	Object->SetSpeed(Res.GetLength());

	Res.Normalize();

	Object->SetMoveDirection(Res);
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalRandDirectedForce::SetVal(const float &Val)
{
	Value = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalRandDirectedForce::SetMinDir(	const float &X,
													const float &Y)
{
	MinDir(X,Y);
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalRandDirectedForce::SetMaxDir(	const float &X,
													const float &Y)
{
	MaxDir(X,Y);
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalRandDirectedForce::SetMinDir(const TPoint2D &Val)
{
	MinDir(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalRandDirectedForce::SetMaxDir(const TPoint2D &Val)
{
	MaxDir(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalRandDirectedForce::SetMinDir(const TVector2D &Val)
{
	MinDir(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsGlobalRandDirectedForce::SetMaxDir(const TVector2D &Val)
{
	MaxDir(Val);
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
