///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef KINEMATICSBASEOBJECT_H_INCLUDED
#define KINEMATICSBASEOBJECT_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsBaseObject
{
	protected:
		bool  ActiveFlag;
		bool  ActivationDelayFlag;
		float ActivationDelay;
		float ActivationTimer;

		bool  LiveFlag;
		bool  LiveTimeFlag;
		float LiveTime;
		float LiveTimer;

		float Weight;

		float	  Speed;
		TVector2D MoveDirection;

		TPoint3D StartPos;
		TPoint3D CurrentPos;
	protected:
		void UpdatePos(const float &Time);

		virtual void ToUpdate(const float &Time);
		virtual void ToDel();
	public:
		TKinematicsBaseObject();
		virtual ~TKinematicsBaseObject();

		bool CheckActivation(const float &Time);

		void Update(const float &Time);
		void Del();

		void SetActive(const bool &Val);
		bool IsActive();

		void SetActivationDelayFlag(const bool &Val);
		bool GetActivationDelayFlag();

		void SetActivationDelay(const float &Val);
		float GetActivationDelay();

		void SetActivationTimer(const float &Val);
		void IncActivationTimer(const float &Val);
		void DecActivationTimer(const float &Val);
		float GetActivationTimer();

		void SetLive(const bool &Val);
		bool IsLive();

		void SetLiveTime(const float &Val);

		void SetWeight(const float &Val);
		float GetWeight() const;

		void SetSpeed(const float &Val);
		float GetSpeed();

		void SetMoveDirection(const float &x,const float &y);
		void SetMoveDirection(const TVector2D &Val);
		TVector2D GetMoveDirection();

		TPoint3D GetStartPos();
		TPoint3D GetCurrentPos();
};

typedef TKinematicsBaseObject* pTKinematicsBaseObject;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // KINEMATICBASEOBJECT_H_INCLUDED
