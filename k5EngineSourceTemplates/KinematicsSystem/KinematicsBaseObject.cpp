#include "KinematicsBaseObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsBaseObject::TKinematicsBaseObject():
								ActiveFlag(true),ActivationDelayFlag(false),
								ActivationDelay(0.0f),ActivationTimer(0.0f),
								LiveFlag(true),LiveTimeFlag(false),
								LiveTime(0.0f),LiveTimer(0.0f),
								Weight(1.0f),Speed(0.0f)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsBaseObject::~TKinematicsBaseObject()
{
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::UpdatePos(const float &Time)
{
	float Cof((Speed*Time)/(1000.0f*Weight));
	CurrentPos.Inc(MoveDirection[0]*Cof,MoveDirection[1]*Cof);
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::ToUpdate(const float &Time)
{
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::ToDel()
{
}
///--------------------------------------------------------------------------------------//
bool TKinematicsBaseObject::CheckActivation(const float &Time)
{
	if(ActiveFlag){return true;}

	if(ActivationDelayFlag==false){
		ActiveFlag = true;
	}
	else{
		ActivationTimer+=Time;
		if(ActivationTimer>=ActivationDelay){
			ActivationTimer = 0.0f;
			ActiveFlag = true;
		}
	}

	return ActiveFlag;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::Update(const float &Time)
{
	UpdatePos(Time);
	ToUpdate(Time);
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::Del()
{
	ToDel();
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetActive(const bool &Val)
{
	ActiveFlag = Val;
}
///--------------------------------------------------------------------------------------//
bool TKinematicsBaseObject::IsActive()
{
	return ActiveFlag;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetActivationDelayFlag(const bool &Val)
{
	ActivationDelayFlag = Val;
}
///--------------------------------------------------------------------------------------//
bool TKinematicsBaseObject::GetActivationDelayFlag()
{
	return ActivationDelayFlag;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetActivationDelay(const float &Val)
{
	ActivationDelay = Val;
	if(ActivationDelay >  0.0f){ActivationDelayFlag = true; }
	if(ActivationDelay == 0.0f){ActivationDelayFlag = false;}
}
///--------------------------------------------------------------------------------------//
float TKinematicsBaseObject::GetActivationDelay()
{
	return ActivationDelay;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetActivationTimer(const float &Val)
{
	ActivationTimer = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::IncActivationTimer(const float &Val)
{
	ActivationTimer+=Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::DecActivationTimer(const float &Val)
{
	ActivationTimer-=Val;
}
///--------------------------------------------------------------------------------------//
float TKinematicsBaseObject::GetActivationTimer()
{
	return ActivationTimer;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetLive(const bool &Val)
{
	LiveFlag = Val;
}
///--------------------------------------------------------------------------------------//
bool TKinematicsBaseObject::IsLive()
{
	return LiveFlag;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetLiveTime(const float &Val)
{
	LiveTime = Val;
	if(LiveTime > 0.0f)	{LiveTimeFlag = true; }
	if(LiveTime == 0.0f){LiveTimeFlag = false;}
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetWeight(const float &Val)
{
	Weight = Val;
}
///--------------------------------------------------------------------------------------//
float TKinematicsBaseObject::GetWeight() const
{
	return Weight;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetSpeed(const float &Val)
{
	Speed = Val;
}
///--------------------------------------------------------------------------------------//
float TKinematicsBaseObject::GetSpeed()
{
	return Speed;
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetMoveDirection(const float &x,const float &y)
{
	MoveDirection(x,y);
	MoveDirection.Normalize();
}
///--------------------------------------------------------------------------------------//
void TKinematicsBaseObject::SetMoveDirection(const TVector2D &Val)
{
	MoveDirection(Val);
	MoveDirection.Normalize();
}
///--------------------------------------------------------------------------------------//
TVector2D TKinematicsBaseObject::GetMoveDirection()
{
	return MoveDirection;
}
///--------------------------------------------------------------------------------------//
TPoint3D TKinematicsBaseObject::GetStartPos()
{
	return StartPos;
}
///--------------------------------------------------------------------------------------//
TPoint3D TKinematicsBaseObject::GetCurrentPos()
{
	return CurrentPos;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
