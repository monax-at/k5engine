#include "KinematicsSpriteObjectGenerator.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsSpriteObjectGenerator::TKinematicsSpriteObjectGenerator():
		TBaseSpriteBankAction(),
		Exception(L"TKinematicsSpriteObjectGenerator: "),
		SpritesGenerator(NULL),KinematicsManager(NULL),ExternalInitiators(NULL)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsSpriteObjectGenerator::~TKinematicsSpriteObjectGenerator()
{
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::ToSet()
{
	// устройство устанавливается для того, что бы пул спрайтов мог создать для
	// них виеверы
	if(SpriteBank!=NULL){
		TempPool.SetDevice(SpriteBank->GetDevice());
	}
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::ToRun()
{
	Exception(SpritesGenerator!=NULL,L"in ToRun() SpritesGenerator is NULL");
	Exception(SpriteBank!=NULL,L"in ToRun() SpriteBank is NULL");

	SpritesGenerator->Set(&TempPool);
	SpritesGenerator->Run();

	int Size = TempPool.GetSize();
	for(int i=0;i<Size;i++){
		TKinematicsSpriteObject* Object(new TKinematicsSpriteObject);
		*Object = TemplateObject;
		Object->Set(TempPool.Get(i),SpriteBank);

		Initiators(Object,1000.0f);
		if(ExternalInitiators!=NULL){ ExternalInitiators->Run(Object,1000.0f);}

		KinematicsManager->Add(Object);
	}
	SpriteBank->Merge(&TempPool);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetSpritesGenerator
												(const pTBaseSpriteBankAction &Val)
{
	SpritesGenerator = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetSpritesGenerator(const pTBaseAction &Val)
{
	SpritesGenerator = dynamic_cast<pTBaseSpriteBankAction>(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetKinematicsManager
										(const pTKinematicsSpriteManager &Val)
{
	KinematicsManager = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetKinematicsManager
												(const pTBaseEventAction &Val)
{
	KinematicsManager = dynamic_cast<TKinematicsSpriteManager*>(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplate
											(const TKinematicsSpriteObject &Val)
{
	TemplateObject = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateActive(const bool &Val)
{
	TemplateObject.SetActive(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateLive(const bool &Val)
{
	TemplateObject.SetLive(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateActivationDelay(const float &Val)
{
	TemplateObject.SetActivationDelay(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateLiveTime(const float &Val)
{
	TemplateObject.SetLiveTime(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateSpeed(const float &Val)
{
	TemplateObject.SetSpeed(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateMoveDirection(const float &x,
																const float &y)
{
	TemplateObject.SetMoveDirection(x,y);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateMoveDirection(const TVector2D &Val)
{
	TemplateObject.SetMoveDirection(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateEnableChangeColor(const bool &Val)
{
	TemplateObject.SetEnableChangeColor(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateColorSpeed(const TColor &Val)
{
	TemplateObject.SetColorSpeed(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateColorSpeed(
												const float &r,const float &g,
												const float &b,const float &a)
{
	TemplateObject.SetColorSpeed(r,g,b,a);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateBlinkColorMinAlpha
																(const float &Val)
{
	TemplateObject.SetBlinkColorAlphaMin(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateBlinkColorMaxAlpha
																(const float &Val)
{
	TemplateObject.SetBlinkColorAlphaMax(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateEnableBlinkColor(const bool &Val)
{
	TemplateObject.SetEnableBlinkColor(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateBlinkColorCount(const int &Val)
{
	TemplateObject.SetBlinkColorCount(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateColorSpeedDirection
										(const enChangeValueDirection &Val)
{
	TemplateObject.SetColorSpeedDirection(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateEnableAngleSpeed(const bool &Val)
{
	TemplateObject.SetEnableAngleSpeed(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetTemplateAngleSpeed(const float &Val)
{
	TemplateObject.SetAngleSpeed(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::AddInitiator
												(const pTKinematicsBaseForce &Val)
{
	Initiators.Add(Val);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::ClearInitiator()
{
	Initiators.Clear();
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObjectGenerator::SetExternalInitiators
												(const pTKinematicsForcesList &Val)
{
	ExternalInitiators = Val;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
