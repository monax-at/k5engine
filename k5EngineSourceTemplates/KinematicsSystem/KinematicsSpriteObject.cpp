#include "KinematicsSpriteObject.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
TKinematicsSpriteObject::TKinematicsSpriteObject():TKinematicsBaseObject(),
		Sprite(NULL),SpritePool(NULL),
		EnableChangeColor(true),ColorSpeedDirection(EN_CVD_Unknown),
		EnableBlinkColor(false),BlinkColorCount(0),BlinkColorCounter(0),
		BlinkColorAlphaMin(0.0f),BlinkColorAlphaMax(1.0f),
		EnableAngleSpeed(true),AngleSpeed(0.0f),Scale(0.0f),ScaleSpeed(0.0f)
{
}
///--------------------------------------------------------------------------------------//
TKinematicsSpriteObject::~TKinematicsSpriteObject()
{
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::ToSetSprite()
{
	if(Sprite!=NULL){

		StartPos = Sprite->Pos;
		CurrentPos = StartPos;
		StartColor = Sprite->Color.Get();

		StartSize(Sprite->Square.GetWidth(),Sprite->Square.GetHeight());
	}
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::UpdateSpritePos()
{
	if(Sprite == NULL){return;}
	Sprite->Pos(CurrentPos);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::UpdateColor(const float &Time)
{
	if(EnableChangeColor == false){return;}
	if(Sprite == NULL){return;}

	if(	ColorSpeed.Get(0) == 0 && ColorSpeed.Get(1) == 0 &&
		ColorSpeed.Get(2) == 0 && ColorSpeed.Get(3) == 0 )
	{
		return;
	}

	float TimeCof(Time/1000.0f);

	TColor Res(ColorSpeed);
	for(int i=0;i<4;i++){Res(i,Res.Get(i)*TimeCof);}

	switch(ColorSpeedDirection){
		case EN_CVD_Inc:{Sprite->Color.Inc(Res);}break;
		case EN_CVD_Dec:{Sprite->Color.Dec(Res);}break;
		default:break;
	}

	if(EnableBlinkColor == false){
		if(ColorSpeedDirection == EN_CVD_Dec){
			if(Sprite->Color.GetAlpha() == 0.0f){LiveFlag = false;}
		}
	}
	else{
		switch(ColorSpeedDirection){
			case EN_CVD_Inc:{
				if(Sprite->Color.GetAlpha() >= BlinkColorAlphaMax){
					ColorSpeedDirection = EN_CVD_Dec;
					if(BlinkColorCount!=0){
						BlinkColorCounter++;
						if(BlinkColorCounter>=BlinkColorCount){
							EnableBlinkColor = false;
						}
					}
				}
			}break;

			case EN_CVD_Dec:{
				if(Sprite->Color.GetAlpha() <= BlinkColorAlphaMin){
					ColorSpeedDirection = EN_CVD_Inc;
				}
			}break;

			default:break;
		}
	}
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::UpdateAngle(const float &Time)
{
	if(EnableAngleSpeed == false){return;}

	if(Sprite == NULL)		{return;}
	if(AngleSpeed == 0.0f)	{return;}

	float TimeCof(Time/1000.0f);
	Sprite->Angle.Inc(AngleSpeed*TimeCof);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::UpdateSize(const float &Time)
{
	if(EnableScaleSpeed == false){return;}

	if(Sprite == NULL)		{return;}
	if(ScaleSpeed == 0.0f)	{return;}

	float TimeCof(Time/1000.0f);

	Scale += ScaleSpeed*TimeCof;

	if(Scale<=0.0f){
		LiveFlag = false;
	}
	else{
		Sprite->Square.Set(StartSize[0]*Scale,StartSize[1]*Scale);
	}
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::ToUpdate(const float &Time)
{
	if(Sprite->GetView() == false){Sprite->SetView(true);}

	UpdateColor(Time);
	UpdateAngle(Time);
	UpdateSize(Time);
	UpdateSpritePos();
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::ToDel()
{
	if(SpritePool != NULL && Sprite != NULL){
		SpritePool->Del( Sprite->GetID() );
	}

	SpritePool = NULL;
	Sprite = NULL;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::Set(	const pTSprite &SpriteVal,
									const pTSpriteBank &SpritePoolVal)
{
	Sprite = SpriteVal;
	SpritePool = SpritePoolVal;
	ToSetSprite();
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetSprite(const pTSprite &Val)
{
	Sprite = Val;
	ToSetSprite();
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetSpritePool(const pTSpriteBank &Val)
{
	SpritePool = Val;
}
///--------------------------------------------------------------------------------------//
TSprite* TKinematicsSpriteObject::GetSprite()
{
	return Sprite;
}
///--------------------------------------------------------------------------------------//
TSpriteBank* TKinematicsSpriteObject::GetSpritePool()
{
	return SpritePool;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetEnableChangeColor(const bool &Val)
{
	EnableChangeColor = Val;
}
///--------------------------------------------------------------------------------------//
bool TKinematicsSpriteObject::GetEnableChangeColor()
{
	return EnableChangeColor;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetColorSpeed(const TColor &Val)
{
	ColorSpeed = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetColorSpeed(const float &r,const float &g,
											const float &b,const float &a)
{
	ColorSpeed(r,g,b,a);
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetColorSpeedDirection
										(const enChangeValueDirection &Val)
{
	ColorSpeedDirection = Val;
}
///--------------------------------------------------------------------------------------//
enChangeValueDirection TKinematicsSpriteObject::GetColorSpeedDirection()
{
	return ColorSpeedDirection;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetEnableBlinkColor(const bool &Val)
{
	EnableBlinkColor = Val;
}
///--------------------------------------------------------------------------------------//
bool TKinematicsSpriteObject::GetEnableBlinkColor()
{
	return EnableBlinkColor;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetBlinkColorCount(const int &Val)
{
	BlinkColorCount = Val;
}
///--------------------------------------------------------------------------------------//
int TKinematicsSpriteObject::GetBlinkColorCount() const
{
	return BlinkColorCount;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetBlinkColorAlphaRange(	const float &Min,
														const float &Max)
{
	BlinkColorAlphaMin = Min;
	BlinkColorAlphaMax = Max;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetBlinkColorAlphaMin(const float &Val)
{
	BlinkColorAlphaMin = Val;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetBlinkColorAlphaMax(const float &Val)
{
	BlinkColorAlphaMax = Val;
}
///--------------------------------------------------------------------------------------//
float TKinematicsSpriteObject::GetBlinkColorAlphaMin()
{
	return BlinkColorAlphaMin;
}
///--------------------------------------------------------------------------------------//
float TKinematicsSpriteObject::GetBlinkColorAlphaMax()
{
	return BlinkColorAlphaMax;
}
///--------------------------------------------------------------------------------------//
TColor TKinematicsSpriteObject::GetColorSpeed()
{
	return ColorSpeed;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetEnableAngleSpeed(const bool &Val)
{
	EnableAngleSpeed = Val;
}
///--------------------------------------------------------------------------------------//
bool TKinematicsSpriteObject::GetEnableAngleSpeed()
{
	return EnableAngleSpeed;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetAngleSpeed(const float &Val)
{
	AngleSpeed = Val;
}
///--------------------------------------------------------------------------------------//
float TKinematicsSpriteObject::GetAngleSpeed()
{
	return AngleSpeed;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetEnableScaleSpeed(const bool &Val)
{
	EnableScaleSpeed = Val;
}
///--------------------------------------------------------------------------------------//
bool TKinematicsSpriteObject::GetEnableScaleSpeed() const
{
	return EnableScaleSpeed;
}
///--------------------------------------------------------------------------------------//
void TKinematicsSpriteObject::SetScaleSpeed(const float &Val)
{
	ScaleSpeed = Val;
}
///--------------------------------------------------------------------------------------//
float TKinematicsSpriteObject::GetScaleSpeed() const
{
	return ScaleSpeed;
}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
