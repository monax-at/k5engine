///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef KINEMATICSPRITEMANAGER_H_INCLUDED
#define KINEMATICSPRITEMANAGER_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "Forces/KinematicsForces.h"
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
class TKinematicsSpriteManager:public TBaseEventAction
{
	protected:
		vector<pTKinematicsSpriteObject> Elems;
		bool AutoRunFlag;

		TKinematicsForcesList Initiators;
		TKinematicsForcesList Forces;

		bool  PosBorderFlag;
		bool  CutPosFlags[4];
		float CutPosLimits[4];

		int DeadCounter;
	protected:
		inline void CheckPosBorder (const pTKinematicsSpriteObject &Object);
		inline void ClearDeadObject();

		void ToRun(const TEvent &Event);
	public:
		TKinematicsSpriteManager();
		virtual ~TKinematicsSpriteManager();

		void SetAutoRunFlag(const bool &Val);
		bool GetAutoRunFlag() const;

		void SetPosBorderFlag(const bool &Val);
		bool GetPosBorder() const;

		void SetCutPosFlags(const bool &Left,const bool &Right,
							const bool &Top,const bool &Bottom);

		void SetCutPosLimits(const float &Left,const float &Right,
							 const float &Top,const float &Bottom);

		void Add(const pTKinematicsSpriteObject &Val);

		void AddInitiator(const pTKinematicsBaseForce &Val);
		void AddForce(const pTKinematicsBaseForce &Val);

		void Clear();
		void ClearData();
};

typedef TKinematicsSpriteManager* pTKinematicsSpriteManager;
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // SPRITEKINEMATICMANAGER_H_INCLUDED
