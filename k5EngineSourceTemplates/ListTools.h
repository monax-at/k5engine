///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#pragma once
#ifndef K5LISTTOOLS_H_INCLUDED
#define K5LISTTOOLS_H_INCLUDED
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#include "K5Engine.h"
#include <stdexcept>
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
namespace K5ListTools {

template<class TList, class TObject>
TObject GetListElemPtrByIndex(TList &List, const int &Indx, const pTExceptionGenerator &Ex)
{
	try{
		return List[Indx];
	}
	catch(std::out_of_range &oor){
		if(Ex!=NULL){Ex->Run(L"in GetListElemPtrByIndex(...) Index out of range");}
	}

	return NULL;
}
///--------------------------------------------------------------------------------------//
template<class TList, class TObject>
TObject GetNextListElemPtr(TList &List, const TObject &Obj, const pTExceptionGenerator &Ex)
{
	if(Obj == NULL){return NULL;}

	int Size(List.size());
	Ex->Run(Size > 0, L"in GetNextListElemPtr(...) list is empty");

	for(int i=0;i<Size;i++){
		if(List[i] == Obj){
			if(i+1<Size){ return List[i+1]; }
			else		{ return NULL; }
		}
	}

	return NULL;
}
///--------------------------------------------------------------------------------------//
template<class TList, class TObject>
TObject GetPreviousListElemPtr(TList &List,const TObject &Obj,const pTExceptionGenerator &Ex)
{
	if(Obj == NULL){return NULL;}

	int Size(List.size());
	Ex->Run(Size > 0, L"in GetPreviousListElemPtr(...) list is empty");

	for(int i=0;i<Size;i++){
		if(List[i] == Obj){
			if(i-1>=0)	{ return List[i-1]; }
			else		{ return NULL; }
		}
	}

	return NULL;
}
///--------------------------------------------------------------------------------------//
template<class TList>
void DelListElemByIndex(TList &List, const int &Indx, const pTExceptionGenerator &Ex)
{
	int Size = List.size();
	Ex->Run(Size 	>  0	, L"in DelListElemByIndex(...) list is empty");
	Ex->Run(Indx	>= 0 	, L"in DelListElemByIndex(...) Id must be more than zero");
	Ex->Run(Indx	< Size	, L"in DelListElemByIndex(...) Id must be less than list size");

	delete (List[Indx]);
	List.erase(List.begin() + Indx);
}
///--------------------------------------------------------------------------------------//
template<class TList, class TObject>
void DelListElemByPointer(TList &List, TObject *Obj, const pTExceptionGenerator &Ex)
{
	int Size(List.size());
	Ex->Run(Size>0		, L"in DelListElemByPointer(...) list is empty");
	Ex->Run(Obj!=NULL	, L"in DelListElemByPointer(...) Obj is NULL");

	int Id(-1);
	for(int i=0;i<Size;i++){
		if(List[i] == Obj){
			Id = i;
			break;
		}
	}

	Ex->Run(Id != -1,L"in DelListElemByPointer(...) Obj not exist");

	delete (List[Id]);
	List.erase(List.begin() + Id);
}
///--------------------------------------------------------------------------------------//
template<class TList>
void ClearListElems(TList &List)
{
	int Size(List.size());
	for(int i=0;i<Size;i++){ delete List[i]; }
	List.clear();
}
///--------------------------------------------------------------------------------------//
template<class TList, class TObject>
int GetListElemIndexByElem(TList &List, TObject *Obj)
{
	if(Obj == NULL){return -1;}

	int Size(List.size());
	for(int i=0;i<Size;i++){ if(List[i] == Obj){return i;} }

	return -1;
}
///--------------------------------------------------------------------------------------//
template<class TList>
void SetEngineListObjectsPos(TList *List,const TPoint3D &Pos)
{
	if(List == NULL){return;}

	int Size(List->GetSize());
	for(int i=0;i<Size;i++){ List->Get(i)->Pos(Pos); }
}
///--------------------------------------------------------------------------------------//
template<class TList>
void SetEngineListObjectsPos(TList *List,const float &x, const float &y)
{
	if(List == NULL){return;}

	int Size(List->GetSize());
	for(int i=0;i<Size;i++){ List->Get(i)->Pos(x,y); }
}
///--------------------------------------------------------------------------------------//
template<class TList>
void SetEngineListObjectsPos(TList *List,const float &x, const float &y, const float &z)
{
	if(List == NULL){return;}

	int Size(List->GetSize());
	for(int i=0;i<Size;i++){ List->Get(i)->Pos(x,y,z); }
}
///--------------------------------------------------------------------------------------//
template<class TList>
void SetEngineListObjectsView(TList *List,const bool &View)
{
	if(List == NULL){return;}

	int Size(List->GetSize());
	for(int i=0;i<Size;i++){ List->Get(i)->SetView(View); }
}

}
///--------------------------------------------------------------------------------------//
///--------------------------------------------------------------------------------------//
#endif // K5LISTTOOLS_H_INCLUDED
