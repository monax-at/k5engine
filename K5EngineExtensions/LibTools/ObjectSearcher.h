///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef OBJECTSEARCHER_H_INCLUDED
#define OBJECTSEARCHER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
#include "../ActionSystem/ExtActionSystem.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TObjectSearcher
{
	protected:
		TExceptionGenerator Exception;
	public:
		TObjectSearcher();
		~TObjectSearcher();

		pTSpriteList SpriteListBySpriteSize( const pTSpriteListManager &Manager,
										     const pTPoint &SpriteSize);

		pTSprite     SpriteBySize( const pTSpriteList &List, const pTPoint &SprSize);

		pTSprite     SpriteByPos      ( const pTSpriteListManager &Manager, const pTPoint &Pos);
		pTText       TextByPos        ( const pTTextListManager   &Manager, const pTPoint &Pos);

		pTSprite     SpriteByPos      ( const pTSpriteList &List, const pTPoint &Pos);
		pTText       TextByPos        ( const pTTextList   &List, const pTPoint &Pos);
		pTBaseAction ActionJoinByPos  ( const pTActionList &List, const pTPoint &Pos);

		pTSprite     SpriteByAngle    ( const pTSpriteList &List, const pTValue &Angle);
		pTText       TextByAngle      ( const pTTextList   &List, const pTValue &Angle);
		pTBaseAction ActionJoinByAngle( const pTActionList &List, const pTValue &Angle);

		// поиск списков, в которых находится объект с соответствующим элементом или параметром
		pTSpriteList SpriteListByObject   ( const pTSpriteListManager &Manager,
										    const pTBaseGraphicObject &Object);

		pTTextList   TextListByObject     ( const pTTextListManager   &Manager,
										    const pTBaseGraphicObject &Object);

		pTSpriteList SpriteListByPos      ( const pTSpriteListManager &Manager,
											const pTPoint &Pos);

		pTTextList   TextListByPos        ( const pTTextListManager   &Manager,
											const pTPoint &Pos);

		pTActionList ActionListByJoinPos  ( const pTActionListManager &Manager,
											const pTPoint &Pos);

		pTSpriteList SpriteListByAngle    ( const pTSpriteListManager &Manager,
											const pTValue &Angle);

		pTTextList   TextListByAngle      ( const pTTextListManager   &Manager,
											const pTValue &Angle);

		pTActionList ActionListByJoinAngle( const pTActionListManager &Manager,
											const pTValue &Angle);
};

typedef TObjectSearcher* pTObjectSearcher;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // OBJECTSEARCHER_H_INCLUDED
