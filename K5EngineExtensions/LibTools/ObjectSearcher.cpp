#include "ObjectSearcher.h"
#include <typeinfo>
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TObjectSearcher::TObjectSearcher():Exception(L"TObjectSearcher: ")
{
}
///----------------------------------------------------------------------------------------------//
TObjectSearcher::~TObjectSearcher()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTSpriteList TObjectSearcher::SpriteListBySpriteSize( const pTSpriteListManager &Manager,
												      const pTPoint &SpriteSize)
{
	Exception(Manager   != NULL, L"in SpriteListBySpriteSize(Manager,SpriteSize) Manager is NULL");
	Exception(SpriteSize!= NULL, L"in SpriteListBySpriteSize(Manager,SpriteSize) SpriteSize is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++ ){
		pTSpriteList List(Manager->Get(i));

		int ListSize(List->GetSize());
		for(int j=0; j<ListSize; j++){
			pTSprite Sprite(List->Get(j));
			if(&Sprite->Size == SpriteSize){return List;}
		}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTSprite TObjectSearcher::SpriteBySize( const pTSpriteList &List, const pTPoint &SprSize)
{
	Exception(List    != NULL, L"in SpriteBySize(List, SprSize) List is NULL");
	Exception(SprSize != NULL, L"in SpriteBySize(List, SprSize) SprSize is NULL");

	int Size(List->GetSize());
	for(int i=0; i<Size; i++){
		pTSprite Sprite(List->Get(i));
		if(&Sprite->Size == SprSize){return Sprite;}
	}
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTSprite TObjectSearcher::SpriteByPos( const pTSpriteListManager &Manager, const pTPoint &Pos)
{
	Exception(Manager != NULL, L"in SpriteByPos(Manager,Pos) Manager is NULL");
	Exception(Pos     != NULL, L"in SpriteByPos(Manager,Pos) Pos is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++ ){
		pTSpriteList List(Manager->Get(i));

		int ListSize(List->GetSize());
		for(int j=0; j<ListSize; j++){
			pTSprite Sprite(List->Get(j));
			if(&Sprite->Pos == Pos){return Sprite;}
		}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTText TObjectSearcher::TextByPos( const pTTextListManager &Manager, const pTPoint &Pos)
{
	Exception(Manager != NULL, L"in TextByPos(Manager,Pos) Manager is NULL");
	Exception(Pos     != NULL, L"in TextByPos(Manager,Pos) Pos is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++ ){
		pTTextList List(Manager->Get(i));

		int ListSize(List->GetSize());
		for(int j=0; j<ListSize; j++){
			pTText Text(List->Get(j));
			if(&Text->Pos == Pos){return Text;}
		}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTSprite TObjectSearcher::SpriteByPos( const pTSpriteList &List, const pTPoint &Pos)
{
	Exception(List != NULL, L"in SpriteByPos(List,Pos) List is NULL");
	Exception(Pos  != NULL, L"in SpriteByPos(List,Pos) Pos is NULL");

	int Size(List->GetSize());
	for(int i=0; i<Size; i++){
		pTSprite Sprite(List->Get(i));
		if(&Sprite->Pos == Pos){return Sprite;}
	}
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTText TObjectSearcher::TextByPos  ( const pTTextList   &List, const pTPoint &Pos)
{
	Exception(List != NULL, L"in TextByPos(List,Pos) List is NULL");
	Exception(Pos  != NULL, L"in TextByPos(List,Pos) Pos is NULL");

	int Size(List->GetSize());
	for(int i=0; i<Size; i++){
		pTText Text(List->Get(i));
		if(&Text->Pos == Pos){return Text;}
	}
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TObjectSearcher::ActionJoinByPos( const pTActionList &List, const pTPoint &Pos)
{
	Exception(List != NULL, L"in ActionJoinByPos(List,Pos) List is NULL");
	Exception(Pos  != NULL, L"in ActionJoinByPos(List,Pos) Pos is NULL");

	int Size(List->GetSize());
	for(int i=0; i<Size; i++){
		pTBaseAction Action(List->Get(i));
		if(typeid(*Action).name() == typeid(TActionJoin).name()){
			pTActionJoin Join(static_cast<pTActionJoin>(Action));
			if(&Join->Pos == Pos){return Action;}
		}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTSprite TObjectSearcher::SpriteByAngle( const pTSpriteList &List, const pTValue &Angle)
{
	Exception(List  != NULL, L"in SpriteByAngle(List,Angle) List is NULL");
	Exception(Angle != NULL, L"in SpriteByAngle(List,Angle) Angle is NULL");

	int Size(List->GetSize());
	for(int i=0; i<Size; i++){
		pTSprite Sprite(List->Get(i));
		if(&Sprite->Angle == Angle){return Sprite;}
	}
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTText TObjectSearcher::TextByAngle  ( const pTTextList   &List, const pTValue &Angle)
{
	Exception(List  != NULL, L"in TextByAngle(List,Angle) List is NULL");
	Exception(Angle != NULL, L"in TextByAngle(List,Angle) Angle is NULL");

	int Size(List->GetSize());
	for(int i=0; i<Size; i++){
		pTText Text(List->Get(i));
		if(&Text->Angle == Angle){return Text;}
	}
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TObjectSearcher::ActionJoinByAngle( const pTActionList &List, const pTValue &Angle)
{
	Exception(List  != NULL, L"in ActionJoinByAngle(List,Angle) List is NULL");
	Exception(Angle != NULL, L"in ActionJoinByAngle(List,Angle) Angle is NULL");

	int Size(List->GetSize());
	for(int i=0; i<Size; i++){
		pTBaseAction Action(List->Get(i));
		if(typeid(*Action).name() == typeid(TActionJoin).name()){
			pTActionJoin Join(static_cast<pTActionJoin>(Action));
			if(&Join->Angle == Angle){return Action;}
		}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TObjectSearcher::SpriteListByObject( const pTSpriteListManager &Manager,
										   		  const pTBaseGraphicObject &Object)
{
	Exception(Manager != NULL, L"in SpriteListByObject(Manager,Pos) Manager is NULL");
	Exception(Object  != NULL, L"in SpriteListByObject(Manager,Pos) Object is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++){
		pTSpriteList List(Manager->Get(i));
		if( List->GetIfExist(Object->GetID()) != NULL ){ return List; }
	}
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTTextList TObjectSearcher::TextListByObject( 	const pTTextListManager   &Manager,
										    	const pTBaseGraphicObject &Object)
{
	Exception(Manager != NULL, L"in TextListByObject(Manager,Pos) Manager is NULL");
	Exception(Object  != NULL, L"in TextListByObject(Manager,Pos) Object is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++){
		pTTextList List(Manager->Get(i));
		if( List->GetIfExist(Object->GetID()) != NULL ){ return List; }
	}
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TObjectSearcher::SpriteListByPos( 	const pTSpriteListManager &Manager,
												const pTPoint &Pos)
{
	Exception(Manager != NULL, L"in SpriteListByPos(Manager,Pos) Manager is NULL");
	Exception(Pos     != NULL, L"in SpriteListByPos(Manager,Pos) Pos is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++){
		pTSpriteList List(Manager->Get(i));
		if(SpriteByPos(List,Pos) != NULL){return List;}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTTextList TObjectSearcher::TextListByPos( const pTTextListManager   &Manager,
										   const pTPoint &Pos)
{
	Exception(Manager != NULL, L"in TextListByPos(Manager,Pos) Manager is NULL");
	Exception(Pos     != NULL, L"in TextListByPos(Manager,Pos) Pos is NULL");

	int Size(Manager->GetSize());

	for(int i=0;i<Size;i++){
		pTTextList List(Manager->Get(i));
		if(TextByPos(List,Pos) != NULL){return List;}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTActionList TObjectSearcher::ActionListByJoinPos(	const pTActionListManager &Manager,
													const pTPoint &Pos)
{
	Exception(Manager != NULL, L"in ActionListByJoinPos(Manager,Pos) Manager is NULL");
	Exception(Pos     != NULL, L"in ActionListByJoinPos(Manager,Pos) Pos is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++){
		pTActionList List(Manager->Get(i));
		if(ActionJoinByPos(List,Pos) != NULL){return List;}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TObjectSearcher::SpriteListByAngle( const pTSpriteListManager &Manager,
												 const pTValue &Angle)
{
	Exception(Manager != NULL, L"in SpriteListByAngle(Manager,Angle) Manager is NULL");
	Exception(Angle   != NULL, L"in SpriteListByAngle(Manager,Angle) Angle is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++){
		pTSpriteList List(Manager->Get(i));
		if(SpriteByAngle(List,Angle) != NULL){return List;}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTTextList   TObjectSearcher::TextListByAngle( 	const pTTextListManager   &Manager,
												const pTValue &Angle)
{
	Exception(Manager != NULL, L"in TextListByAngle(Manager,Angle) Manager is NULL");
	Exception(Angle   != NULL, L"in TextListByAngle(Manager,Angle) Angle is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++){
		pTTextList List(Manager->Get(i));
		if(TextByAngle(List,Angle) != NULL){return List;}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTActionList TObjectSearcher::ActionListByJoinAngle(const pTActionListManager &Manager,
													const pTValue &Angle)
{
	Exception(Manager != NULL, L"in ActionListByJoinAngle(Manager,Angle) Manager is NULL");
	Exception(Angle   != NULL, L"in ActionListByJoinAngle(Manager,Angle) Angle is NULL");

	int Size(Manager->GetSize());
	for(int i=0;i<Size;i++){
		pTActionList List(Manager->Get(i));
		if(ActionJoinByAngle(List,Angle) != NULL){return List;}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

