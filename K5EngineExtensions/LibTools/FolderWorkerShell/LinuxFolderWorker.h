///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор: Сергей Каленик. [Monax-At@rambler.ru][http://monax-at.blogspot.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef LINUXFOLDERWORKER_H_INCLUDED
#define LINUXFOLDERWORKER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TLinuxFolderWorker
{
	private:
		wstring             AppPath;
		TExceptionGenerator Exception;
	public:
		TLinuxFolderWorker();
		~TLinuxFolderWorker();

		// ручная установка и возвращение пути к каталогу приложения
		void SetAppPath(const wstring &Val);
		wstring GetAppPath ();

		// поиск пути к каталогу, откуда запущено приложение
		wstring FindAppPath();

		bool CreateFolder   (const wstring &Name);
		bool CreateAppFolder(const wstring &Name);

		bool IsFileExist   (const wstring &Name);
		bool IsAppFileExist(const wstring &Name);

		bool DelFile   (const wstring &Name);
		bool DelAppFile(const wstring &Name);

		bool CopyFile(const wstring &FromName,const wstring &ToName);
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // LINUXFOLDERWORKER_H_INCLUDED
