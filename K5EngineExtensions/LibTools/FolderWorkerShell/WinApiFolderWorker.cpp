#include "WinApiFolderWorker.h"
#include "windows.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TWinApiFolderWorker::TWinApiFolderWorker()
{
}
///----------------------------------------------------------------------------------------------//
TWinApiFolderWorker::~TWinApiFolderWorker()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TWinApiFolderWorker::ValidateAppPath()
{
	if(AppPath.length() == 0){ FindAppPath(); }
}
///----------------------------------------------------------------------------------------------//
void TWinApiFolderWorker::SetAppPath(const wstring &Val)
{
	AppPath = Val;
}
///----------------------------------------------------------------------------------------------//
wstring TWinApiFolderWorker::GetAppPath ()
{
	ValidateAppPath();
	return AppPath;
}
///----------------------------------------------------------------------------------------------//
wstring TWinApiFolderWorker::FindAppPath()
{
	wchar_t Buffer[MAX_PATH];
	memset(Buffer, 0, sizeof(Buffer));

	GetCurrentDirectory(sizeof(Buffer),Buffer);
	AppPath = Buffer;
	AppPath += L"\\";

	return(AppPath);
}
///----------------------------------------------------------------------------------------------//
bool TWinApiFolderWorker::CreateFolder(const wstring &Name)
{
	if(CreateDirectory(Name.c_str(),NULL)){return true;}
	return false;
}
///----------------------------------------------------------------------------------------------//
bool TWinApiFolderWorker::CreateAppFolder(const wstring &Name)
{
	ValidateAppPath();
	return CreateFolder( AppPath+Name );
}
///----------------------------------------------------------------------------------------------//
bool TWinApiFolderWorker::IsFileExist(const wstring &Name)
{
	FILE *File = NULL;
	File = fopen(WStrToStr(Name).c_str(),"r");
	if(File != NULL){
		fclose(File);
		return true;
	}
	return false;
}
///----------------------------------------------------------------------------------------------//
bool TWinApiFolderWorker::IsAppFileExist(const wstring &Name)
{
	ValidateAppPath();
	return IsFileExist(AppPath + Name);
}
///----------------------------------------------------------------------------------------------//
bool TWinApiFolderWorker::DelGlobalFile(const wstring &Name)
{
	return (bool)(::DeleteFile(Name.c_str()));
}
///----------------------------------------------------------------------------------------------//
bool TWinApiFolderWorker::DelAppFile(const wstring &Name)
{
	ValidateAppPath();
	return (bool)(::DeleteFile((AppPath + Name).c_str()));
}
///----------------------------------------------------------------------------------------------//
bool TWinApiFolderWorker::CopyGlobalFile(const wstring &FromName, const wstring &ToName)
{
	return (bool)(::CopyFile(FromName.c_str(), ToName.c_str(),true));
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
