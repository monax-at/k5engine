#include "LinuxFolderWorker.h"
#include <sys/stat.h>
#include <unistd.h>
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TLinuxFolderWorker::TLinuxFolderWorker():Exception(L"TLinuxFolderWorker: ")
{
}
///----------------------------------------------------------------------------------------------//
TLinuxFolderWorker::~TLinuxFolderWorker()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TLinuxFolderWorker::SetAppPath(const wstring &Val)
{
	AppPath = Val;
}
///----------------------------------------------------------------------------------------------//
wstring TLinuxFolderWorker::GetAppPath ()
{
	if(AppPath.length() == 0){ FindAppPath(); }

	return AppPath;
}
///----------------------------------------------------------------------------------------------//
wstring TLinuxFolderWorker::FindAppPath()
{
	char* GetcwdRes = getcwd( NULL, sizeof(char)*FILENAME_MAX );
	Exception(GetcwdRes != NULL, L"in FindAppPath() err get path");

	string sTmpPath(GetcwdRes);
	sTmpPath += "/";

	AppPath = StrToWStr(sTmpPath);

	return AppPath;
}
///----------------------------------------------------------------------------------------------//
bool TLinuxFolderWorker::CreateFolder(const wstring &Name)
{
	return mkdir(WStrToStr(Name).c_str(), 0770) == 0;
}
///----------------------------------------------------------------------------------------------//
bool TLinuxFolderWorker::CreateAppFolder(const wstring &Name)
{
	if(AppPath.length() == 0){ FindAppPath(); }

	return mkdir(WStrToStr(AppPath + Name).c_str(), 0770) == 0;
}
///----------------------------------------------------------------------------------------------//
bool TLinuxFolderWorker::IsFileExist(const wstring &Name)
{
	struct stat FileInfo;
	int IntStat = stat( WStrToStr(Name).c_str(), &FileInfo );

	if(IntStat == 0) { return true; }

	return false;
}
///----------------------------------------------------------------------------------------------//
bool TLinuxFolderWorker::IsAppFileExist(const wstring &Name)
{
	if(AppPath.length() == 0){ FindAppPath(); }

	return IsFileExist(AppPath + Name);
}
///----------------------------------------------------------------------------------------------//
bool TLinuxFolderWorker::DelFile(const wstring &Name)
{
	return remove(WStrToStr(Name).c_str()) == 0;
}
///----------------------------------------------------------------------------------------------//
bool TLinuxFolderWorker::DelAppFile(const wstring &Name)
{
	if(AppPath.length() == 0){ FindAppPath(); }

	return remove(WStrToStr(AppPath + Name).c_str()) == 0;
}
///----------------------------------------------------------------------------------------------//
bool TLinuxFolderWorker::CopyFile(const wstring &FromName, const wstring &ToName)
{
	return false;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
