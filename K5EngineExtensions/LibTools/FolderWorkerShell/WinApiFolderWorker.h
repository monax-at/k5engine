///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef CLASS_WINAPIFOLDERWORKER_H_INCLUDED
#define CLASS_WINAPIFOLDERWORKER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TWinApiFolderWorker
{
	private:
		wstring AppPath;
	private:
		inline void ValidateAppPath();
	public:
		TWinApiFolderWorker();
		~TWinApiFolderWorker();

		// ручная установка и возвращение пути к каталогу приложения
		void SetAppPath(const wstring &Val);
		wstring GetAppPath ();

		// поиск пути к каталогу, откуда запущено приложение
		wstring FindAppPath();

		bool CreateFolder(const wstring &Name);
		bool CreateAppFolder(const wstring &Name);

		bool IsFileExist(const wstring &Name);
		bool IsAppFileExist(const wstring &Name);

		bool DelGlobalFile(const wstring &Name);
		bool DelAppFile   (const wstring &Name);

		bool CopyGlobalFile(const wstring &FromName, const wstring &ToName);
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // CLASS_WINAPIFOLDERWORKER_H_INCLUDED
