#include "UIBaseControl.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TUIBaseControl::TUIBaseControl():TBaseAction(),
						Exception(L"TUIBaseControl: "), MouseOn(false), MouseButtonDowm(false),
						MouseClick(false)
{
}
///----------------------------------------------------------------------------------------------//
TUIBaseControl::TUIBaseControl(TUIBaseControl &ObjAction):TBaseAction(),
						Exception(L"TUIBaseControl: "), MouseOn(false), MouseButtonDowm(false),
						MouseClick(false)
{
}
///----------------------------------------------------------------------------------------------//
TUIBaseControl::~TUIBaseControl()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::operator =(TUIBaseControl &ObjAction)
{
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ProcessMouseMotion(const bool &CursorFlag)
{
	if(CursorFlag){
		if(MouseOn == false){
			if(MouseButtonDowm == false){
				MouseOn = true;

				OnMouseEnter.Start();
				ToMouseEnter();
			}
		}
	}
	else{
		if(MouseOn == true){
			MouseButtonDowm = false;
			MouseOn = false;

			OnMouseLeave.Start();
			ToMouseLeave();
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ProcessMouseButtonDown(const bool &CursorFlag)
{
	if(!CursorFlag)     { return; }
	if(MouseButtonDowm ){ return; }

	MouseButtonDowm = true;

	OnMouseButtonDown.Start();
	ToMouseButtonDown();
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ProcessMouseButtonUp(const bool &CursorFlag)
{
	if(!CursorFlag)     { return; }
	if(!MouseButtonDowm){ return; }

	MouseButtonDowm = false;
	MouseClick = true;

	OnMouseButtonUp.Start();
	ToMouseButtonUp();
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ProcessCursorFlag(const TEvent &Event, const bool &CursorFlag)
{
	MouseClick = false;

	switch(Event.Mouse.Type){
		case EN_MOUSEMOTION		:ProcessMouseMotion		(CursorFlag); break;
		case EN_MOUSEBUTTONDOWN	:ProcessMouseButtonDown	(CursorFlag); break;
		case EN_MOUSEBUTTONUP	:ProcessMouseButtonUp	(CursorFlag); break;
		default:break;
	}
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ProcessBaseActionLists(const TEvent &Event)
{
	OnMouseEnter.Run(Event);
	OnMouseLeave.Run(Event);

	OnMouseButtonDown.Run(Event);
	OnMouseButtonUp  .Run(Event);
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ToStart()
{
	ResetFlags();
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ToStop()
{
	ResetFlags();
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ToMouseEnter()
{
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ToMouseLeave()
{
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ToMouseButtonDown()
{
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ToMouseButtonUp()
{
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::AddOnMouseEnter(const pTBaseAction &Action)
{
	OnMouseEnter.Add(Action);
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::AddOnMouseLeave(const pTBaseAction &Action)
{
	OnMouseLeave.Add(Action);
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::AddOnMouseButtonDown(const pTBaseAction &Action)
{
	OnMouseButtonDown.Add(Action);
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::AddOnMouseButtonUp(const pTBaseAction &Action)
{
	OnMouseButtonUp.Add(Action);
}
///----------------------------------------------------------------------------------------------//
pTActionList TUIBaseControl::GetOnMouseEnterPtr()
{
	return &OnMouseEnter;
}
///----------------------------------------------------------------------------------------------//
pTActionList TUIBaseControl::GetOnMouseLeavePtr()
{
	return &OnMouseLeave;
}
///----------------------------------------------------------------------------------------------//
pTActionList TUIBaseControl::GetOnMouseButtonDownPtr()
{
	return &OnMouseButtonDown;
}
///----------------------------------------------------------------------------------------------//
pTActionList TUIBaseControl::GetOnMouseButtonUpPtr()
{
	return &OnMouseButtonUp;
}
///----------------------------------------------------------------------------------------------//
bool TUIBaseControl::GetMouseOn() const
{
	return MouseOn;
}
///----------------------------------------------------------------------------------------------//
bool TUIBaseControl::GetMouseClick() const
{
	return MouseClick;
}
///----------------------------------------------------------------------------------------------//
void TUIBaseControl::ResetFlags()
{
	MouseOn         = false;
	MouseButtonDowm = false;
	MouseClick      = false;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
