#include "UISimpleButton.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TUISimpleButton::TUISimpleButton():TUIBaseControl(),Sprite(NULL), Camera(NULL)
{
	Exception.SetPrefix(L"TUISimpleButton: ");
}
///----------------------------------------------------------------------------------------------//
TUISimpleButton::TUISimpleButton(TUISimpleButton &ObjAction):TUIBaseControl(),
												 Sprite(NULL), Camera(NULL)
{
	Exception.SetPrefix(L"TUISimpleButton: ");
}
///----------------------------------------------------------------------------------------------//
TUISimpleButton::~TUISimpleButton()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TUISimpleButton::operator =(TUISimpleButton &ObjAction)
{
}
///----------------------------------------------------------------------------------------------//
void TUISimpleButton::ToRun(const TEvent &Event)
{
	if(Sprite == NULL) {return;}

	TEvent ModEvent(Event);
	if(Camera != NULL){ ModEvent = Camera->RecountEvent(Event); }

	ProcessBaseActionLists(ModEvent);

	if( ModEvent == false || ModEvent.Type != EN_MOUSE){return;}

	ProcessCursorFlag(ModEvent, CollisionChecker.Run(Sprite, ModEvent));
}
///----------------------------------------------------------------------------------------------//
void TUISimpleButton::Set(const pTSprite &Val)
{
	Sprite = Val;
}
///----------------------------------------------------------------------------------------------//
pTSprite TUISimpleButton::Get()
{
	return Sprite;
}
///----------------------------------------------------------------------------------------------//
void TUISimpleButton::SetCamera(const pTCamera &Val)
{
	Camera = Val;
}
///----------------------------------------------------------------------------------------------//
pTCamera TUISimpleButton::GetCamera()
{
	return Camera;
}
///----------------------------------------------------------------------------------------------//
pTSimpleCollisionChecker TUISimpleButton::GetCollisionCheckerPtr()
{
	return &CollisionChecker;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
