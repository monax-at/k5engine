///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef UISIMPLEBUTTON_H_INCLUDED
#define UISIMPLEBUTTON_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "UIBaseControl.h"
#include "../CollisionSystem/SimpleCollisionChecker.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TUISimpleButton: public TUIBaseControl
{
	protected:
		pTSprite Sprite;
		pTCamera Camera;

		TSimpleCollisionChecker CollisionChecker;
	protected:
		void ToRun(const TEvent &Event);
	private:
		TUISimpleButton(TUISimpleButton &ObjAction);
		void operator =(TUISimpleButton &ObjAction);
	public:
		TUISimpleButton();
		virtual ~TUISimpleButton();

		void     Set(const pTSprite &Val);
		pTSprite Get();

		void SetCamera(const pTCamera &Val);
		pTCamera GetCamera();

		pTSimpleCollisionChecker GetCollisionCheckerPtr();
};

typedef TUISimpleButton* pTUISimpleButton;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // EVENTACTIONSIMPLEBUTTON_H_INCLUDED
