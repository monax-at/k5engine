///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef UIBASECONTROL_H_INCLUDED
#define UIBASECONTROL_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TUIBaseControl: public TBaseAction
{
	protected:
		TExceptionGenerator Exception;

		bool MouseOn;
		bool MouseButtonDowm;
		bool MouseClick;

		TActionList OnMouseEnter;
		TActionList OnMouseLeave;

		TActionList OnMouseButtonDown;
		TActionList OnMouseButtonUp;
	protected:
		void ProcessMouseMotion		(const bool &CursorFlag);
		void ProcessMouseButtonDown	(const bool &CursorFlag);
		void ProcessMouseButtonUp	(const bool &CursorFlag);

		void ProcessCursorFlag(const TEvent &Event, const bool &CursorFlag);
		void ProcessBaseActionLists(const TEvent &Event);

		void ToStart();
		void ToStop();
	private:
		TUIBaseControl(TUIBaseControl &ObjAction);
		void operator =(TUIBaseControl &ObjAction);
	public:
		TUIBaseControl();
		virtual ~TUIBaseControl();

		virtual void ToMouseEnter();
		virtual void ToMouseLeave();

		virtual void ToMouseButtonDown();
		virtual void ToMouseButtonUp();

		void AddOnMouseEnter(const pTBaseAction &Action);
		void AddOnMouseLeave(const pTBaseAction &Action);

		void AddOnMouseButtonDown(const pTBaseAction &Action);
		void AddOnMouseButtonUp	 (const pTBaseAction &Action);

		pTActionList GetOnMouseEnterPtr();
		pTActionList GetOnMouseLeavePtr();
		pTActionList GetOnMouseButtonDownPtr();
		pTActionList GetOnMouseButtonUpPtr();

		bool GetMouseOn    () const;
		bool GetMouseClick () const;

		void ResetFlags();
};

typedef TUIBaseControl* pTUIBaseControl;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // UIBASECONTROL_H_INCLUDED
