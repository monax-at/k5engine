#include "GroupCollisionChecker.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TGroupCollisionChecker::TGroupCollisionChecker()
{
}
///----------------------------------------------------------------------------------------------//
TGroupCollisionChecker::~TGroupCollisionChecker()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTSprite TGroupCollisionChecker::Run(const pTSpriteList &Sprites, const float &X, const float &Y,
									 const bool &CheckHidden)
{
	if(Sprites == NULL){return NULL;}

	Selected.Clear();

	int Size(Sprites->GetSize());

	for(int i=0;i<Size;i++){
		pTSprite Sprite(Sprites->Get(i));
		if( CollisionChecker.Run(Sprite,X,Y) ){
			if(CheckHidden){ Selected.Add(Sprite); }
			else{ if(Sprite->GetView()){ Selected.Add(Sprite);	} }
		}
	}

	if(Selected.GetSize() == 0){return NULL;}

	Selected.Sort();

	return Selected.Get(0);
}
///----------------------------------------------------------------------------------------------//
pTSprite TGroupCollisionChecker::Run(const pTSpriteListManager &Sprites,
									 const float &X, const float &Y,
									 const bool &CheckHidden)
{
	if(Sprites == NULL){return NULL;}

	Selected.Clear();

	int Size(Sprites->GetSize());

	for(int i=0; i<Size; i++){
		pTSpriteList List(Sprites->Get(i));
		int ListSize(List->GetSize());

		for(int j=0; j<ListSize; j++){
			pTSprite Sprite(List->Get(j));
			if( CollisionChecker.Run(Sprite,X,Y) ){
				if(CheckHidden){ Selected.Add(Sprite); }
				else{ if(Sprite->GetView()){ Selected.Add(Sprite);	} }
			}
		}
	}

	if(Selected.GetSize() == 0){return NULL;}

	Selected.Sort();

	return Selected.Get(0);
}
///----------------------------------------------------------------------------------------------//
pTSprite TGroupCollisionChecker::Run(const pTSpriteList &Sprites, const TEvent &Event,
									 const bool &CheckHidden)
{
	return Run(Sprites, (float)Event.Mouse.X, (float)Event.Mouse.Y, CheckHidden);
}
///----------------------------------------------------------------------------------------------//
pTSprite TGroupCollisionChecker::Run(const pTSpriteListManager &Sprites, const TEvent &Event,
									 const bool &CheckHidden)
{
	return Run(Sprites, (float)Event.Mouse.X, (float)Event.Mouse.Y, CheckHidden);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
