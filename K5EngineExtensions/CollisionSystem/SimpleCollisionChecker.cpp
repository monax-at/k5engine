#include "SimpleCollisionChecker.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TSimpleCollisionChecker::TSimpleCollisionChecker()
{
}
///----------------------------------------------------------------------------------------------//
TSimpleCollisionChecker::~TSimpleCollisionChecker()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
inline bool TSimpleCollisionChecker::CheckSprite(const TSprite &Sq, const float &x,const float &y,
									 			 const float &ShiftWidth, const float &ShiftHeight)
{
	float CheckX = x - Sq.Pos.GetX();
	float CheckY = y - Sq.Pos.GetY();

	float SpriteAngle(Sq.Angle.Get());

	if( SpriteAngle != 0.0f){
		float Angle(GetAngle2D(CheckX, CheckY) - SpriteAngle);
		float Dist (GetDistance2D(CheckX, CheckY));

		TVector2D CheckV(GetVector2DFromAngle(Angle));
		CheckX = CheckV.x *= Dist;
		CheckY = CheckV.y *= Dist;
	}

	CheckX += Sq.Center.GetX();
	CheckY += Sq.Center.GetY();

	float HalfWidth ((Sq.Size.GetX()  + ShiftWidth)  / 2.0f);
	float HalfHeight((Sq.Size.GetY() + ShiftHeight) / 2.0f);

	if(CheckX >= -HalfWidth && CheckX <= HalfWidth){
		if(CheckY >= -HalfHeight && CheckY <= HalfHeight){
			return true;
		}
	}

	return false;
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const TSprite &Sq,const float &x,const float &y)
{
	return CheckSprite(Sq, x,y);
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const TSprite &Sq,const TPoint &Point)
{
	return CheckSprite(Sq, Point.GetX(),Point.GetY());
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const TSprite &Sq,const TEvent &Event)
{
	return CheckSprite(Sq, (float)Event.Mouse.X,(float)Event.Mouse.Y);
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const TSprite &Sq,const TMouseEvent &Event)
{
	return CheckSprite(Sq, (float)Event.X,(float)Event.Y);
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const pTSprite &Sq,const float &x,const float &y)
{
	return CheckSprite(*Sq,x,y);
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const pTSprite &Sq,const TPoint &Point)
{
	return CheckSprite(*Sq,Point.GetX(),Point.GetY());
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const pTSprite &Sq,const TEvent &Event)
{
	return CheckSprite(*Sq, (float)Event.Mouse.X,(float)Event.Mouse.Y);
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const pTSprite &Sq,const TMouseEvent &Event)
{
	return CheckSprite(*Sq,(float)Event.X,(float)Event.Y);
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const pTSprite &Sq,const TEvent &Event,
					const float &ShiftWidth,const float &ShiftHeight)
{
	return CheckSprite(	*Sq,(float)Event.Mouse.X,(float)Event.Mouse.Y,
							ShiftWidth,ShiftHeight);
}
///----------------------------------------------------------------------------------------------//
bool TSimpleCollisionChecker::Run(const pTSprite &Sq,const TMouseEvent &Event,
					const float &ShiftWidth,const float &ShiftHeight)
{
	return CheckSprite(	*Sq,(float)Event.X,(float)Event.Y,
							ShiftWidth,ShiftHeight);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
