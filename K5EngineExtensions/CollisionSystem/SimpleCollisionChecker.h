///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef SIMPLECOLLISIONCHECKER_H_INCLUDED
#define SIMPLECOLLISIONCHECKER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TSimpleCollisionChecker
{
	protected:
		inline bool CheckSprite(const TSprite &Sq,const float &x,const float &y,
								const float &ShiftWidth  = 0.0f,
								const float &ShiftHeight = 0.0f);
	public:
		TSimpleCollisionChecker();
		~TSimpleCollisionChecker();

		bool Run(const TSprite &Sq,  const float &x,const float &y);
		bool Run(const TSprite &Sq,  const TPoint &Point);
		bool Run(const TSprite &Sq,  const TEvent &Event);
		bool Run(const TSprite &Sq,  const TMouseEvent &Event);

		bool Run(const pTSprite &Sq, const float &x,const float &y);
		bool Run(const pTSprite &Sq, const TPoint &Point);
		bool Run(const pTSprite &Sq, const TEvent &Event);
		bool Run(const pTSprite &Sq, const TMouseEvent &Event);

		bool Run(const pTSprite &Sq, const TEvent &Event,
					const float &ShiftWidth, const float &ShiftHeight);

		bool Run(const pTSprite &Sq, const TMouseEvent &Event,
					const float &ShiftWidth, const float &ShiftHeight);
};

typedef TSimpleCollisionChecker* pTSimpleCollisionChecker;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // SIMPLECOLLISIONCHECKER_H_INCLUDED
