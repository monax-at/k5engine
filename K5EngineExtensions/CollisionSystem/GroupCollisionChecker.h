///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef GROUPCOLLISIONCHECKER_H_INCLUDED
#define GROUPCOLLISIONCHECKER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "SimpleCollisionChecker.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TGroupCollisionChecker
{
	protected:
		TSimpleCollisionChecker CollisionChecker;
	public:
		// сюда помещаются указатели на найденные спрайты после выполнения Run
		TSpritePointerList Selected;
	public:
		TGroupCollisionChecker();
		~TGroupCollisionChecker();

		pTSprite Run(const pTSpriteList        &Sprites, const float &X, const float &Y,
					 const bool &CheckHidden = true);

		pTSprite Run(const pTSpriteListManager &Sprites, const float &X, const float &Y,
					 const bool &CheckHidden = true);

		pTSprite Run(const pTSpriteList        &Sprites, const TEvent &Event,
					 const bool &CheckHidden = true);

		pTSprite Run(const pTSpriteListManager &Sprites, const TEvent &Event,
					 const bool &CheckHidden = true);
};

typedef TGroupCollisionChecker* pTGroupCollisionChecker;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // GROUPCOLLISIONCHECKER_H_INCLUDED
