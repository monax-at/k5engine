#include "ActionBaseIterator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionBaseIterator::TActionBaseIterator():TBaseAction(),
				UseDelay(false), Delay(0.0f), DelayTimer(0.0f), DelayTimerReady(false),
				FinishOnStop(false)
{
}
///----------------------------------------------------------------------------------------------//
TActionBaseIterator::~TActionBaseIterator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
bool TActionBaseIterator::IsDelay(const TEvent &Event)
{
	if(!UseDelay)      { return false; }
	if(DelayTimerReady){ return false; }

	DelayTimer += Event.Timer.Ms;
	if(DelayTimer < Delay){ return true; }

	DelayTimerReady = true;
	return false;
}
///----------------------------------------------------------------------------------------------//
void TActionBaseIterator::SetDefParams(TActionBaseIterator *ObjAction)
{
	Active = ObjAction->GetActive();
	Name   = ObjAction->GetName ();

	UseDelay        = ObjAction->GetUseDelay();
	Delay           = ObjAction->GetDelay();
	DelayTimer      = ObjAction->GetDelayTimer();
	DelayTimerReady = ObjAction->GetDelayTimerReady();

	FinishOnStop    = ObjAction->GetFinishOnStop();
}
///----------------------------------------------------------------------------------------------//
void TActionBaseIterator::SetDelay(const float &Val)
{
	Delay    = Val;
	UseDelay = Delay > 0.0f;
}
///----------------------------------------------------------------------------------------------//
float TActionBaseIterator::GetDelay() const
{
	return Delay;
}
///----------------------------------------------------------------------------------------------//
void TActionBaseIterator::SetFinishOnStop(const bool &Val)
{
	FinishOnStop = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionBaseIterator::GetFinishOnStop() const
{
	return FinishOnStop;
}
///----------------------------------------------------------------------------------------------//
bool  TActionBaseIterator::GetUseDelay() const
{
	return UseDelay;
}
///----------------------------------------------------------------------------------------------//
float TActionBaseIterator::GetDelayTimer() const
{
	return DelayTimer;
}
///----------------------------------------------------------------------------------------------//
bool TActionBaseIterator::GetDelayTimerReady() const
{
	return DelayTimerReady;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
