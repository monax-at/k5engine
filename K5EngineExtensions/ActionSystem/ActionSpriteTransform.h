///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONSPRITETRANSFORM_H_INCLUDED
#define ACTIONSPRITETRANSFORM_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TActionSpriteTransform:public TBaseAction
{
	protected:
		TActionList Actions;

		TSprite  Template;
		pTSprite Sprite;

		float Time;

		bool ChangeView;

		bool ChangePos;
		bool ChangeCenter;
		bool ChangeAngle;

		bool ChangeTexture;
		bool ChangeSize;
		bool ChangeMesh;
		bool ChangeColor;
	protected:
		void ToRun(const TEvent &Event);
		void ToStart();
		void ToStop();
	public:
		TActionSpriteTransform();
		virtual ~TActionSpriteTransform();

		void Set(const pTSprite &Val);
		pTSprite Get();

		void SetTemplate(const TSprite &Val);
		TSprite GetTemplate() const;

		void SetTime (const float &Val);
		float GetTime() const;

		// оперирование флагами для назначения трансформаций
		void SetChangeAll(const bool &Val);
		bool GetChangeAll() const;

		void SetChangeView(const bool &Val);

		void SetChangePos   (const bool &Val);
		void SetChangeCenter(const bool &Val);
		void SetChangeAngle (const bool &Val);

		void SetChangeTexture(const bool &Val);
		void SetChangeSize   (const bool &Val);
		void SetChangeMesh   (const bool &Val);
		void SetChangeColor  (const bool &Val);

		bool GetChangeView() const;

		bool GetChangePos   () const;
		bool GetChangeCenter() const;
		bool GetChangeAngle () const;

		bool GetChangeTexture() const;
		bool GetChangeSize   () const;
		bool GetChangeMesh   () const;
		bool GetChangeColor  () const;
};

typedef TActionSpriteTransform* pTActionSpriteTransform;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // ACTIONSPRITETRANSFORM_H_INCLUDED
