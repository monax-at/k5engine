///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef EXTACTIONSYSTEM_H_INCLUDED
#define EXTACTIONSYSTEM_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "ActionAngleIterator.h"
#include "ActionChangeView.h"
#include "ActionColorArrayIterator.h"
#include "ActionColorIterator.h"
#include "ActionJoin.h"
#include "ActionPointArrayIterator.h"
#include "ActionPointIterator.h"
#include "ActionContainer.h"
#include "ActionQueue.h"
#include "ActionSpriteTextureAnimation.h"
#include "ActionSpriteTransform.h"
#include "ActionTexturesLoader.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // EXTACTIONSYSTEM_H_INCLUDED
