#include "ActionTexturesLoader.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionTexturesLoader::TActionTexturesLoader():
				TBaseAction(),Exception(L"TActionTexturesLoader: "), Device(NULL),
				Textures(NULL), LoadTextures(true), NodeListItr(0), NodeTextureItr(0)
{
}
///----------------------------------------------------------------------------------------------//
TActionTexturesLoader::TActionTexturesLoader(TActionTexturesLoader &ObjAction):
				TBaseAction(),Exception(L"TActionTexturesLoader: "), Device(NULL),
				Textures(NULL), LoadTextures(true), NodeListItr(0), NodeTextureItr(0)
{
	*this = ObjAction;
}
///----------------------------------------------------------------------------------------------//
TActionTexturesLoader::TActionTexturesLoader(const pTTextureListManager &TexturesVal,
		                      const pTBaseDevice &DeviceVal, const TDataNode  &NodeVal,
		                      const bool &LoadTexturesVal):
				TBaseAction(),Exception(L"TActionTexturesLoader: "), Device(DeviceVal),
				Textures(TexturesVal), LoadTextures(LoadTexturesVal),
				NodeListItr(0), NodeTextureItr(0)
{
	Node = NodeVal;
	TextureLoader.SetDevice(Device);
}
///----------------------------------------------------------------------------------------------//
TActionTexturesLoader::TActionTexturesLoader(const pTTextureListManager &TexturesVal,
								const pTBaseDevice &DeviceVal, const pTDataNode &NodeVal,
								const bool &LoadTexturesVal):
				TBaseAction(),Exception(L"TActionTexturesLoader: "), Device(DeviceVal),
				Textures(TexturesVal), LoadTextures(LoadTexturesVal),
				NodeListItr(0), NodeTextureItr(0)
{
	if(NodeVal != NULL){Node = *NodeVal; }
	TextureLoader.SetDevice(Device);
}
///----------------------------------------------------------------------------------------------//
TActionTexturesLoader::~TActionTexturesLoader()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
int TActionTexturesLoader::GetQuadSize(const int &WidthVal,const int &HeightVal)
{
	int Width  = NextDeg2(WidthVal);
	int Height = NextDeg2(HeightVal);

	if(Width >= Height) return Width;
	return Height;
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::LoadTexture(const pTTextureList &List,
										const pTDataNode &TextureNode)
{
	wstring TextureName(TextureNode->GetChildString(L"name"));

	// загрузка обычной текстуры
	if(TextureNode->IsExist(L"path")){
		wstring TexturePath(FolderWorker.GetAppPath() +
							BuildPathStr(TextureNode->GetChildString(L"path")));

		List->Add(TextureLoader.Run(TexturePath,TextureName));
		return;
	}

	// загрузка одного из вариантов маски: soft_mask, cut_mask, stroke_mask
	wstring SourceList(TextureNode->GetChildString(L"source_list"));
	wstring SourceName(TextureNode->GetChildString(L"source_name"));

	if(TextureNode->IsExist(L"soft_mask") && TextureNode->GetChildBool(L"soft_mask")){
		List->Add(Textures->Get(SourceList,SourceName)->CreateMask())->SetName(TextureName);
		return;
	}

	if(TextureNode->IsExist(L"cut_mask") && TextureNode->GetChildBool(L"cut_mask")){
		List->Add(Textures->Get(SourceList,SourceName)->CreateCutMask())->SetName(TextureName);;
		return;
	}

	if(TextureNode->IsExist(L"stroke_mask") && TextureNode->GetChildBool(L"stroke_mask")){

		int  StrokeSize(TextureNode->GetChildInt(L"stroke_size"));

		pTTexture Mask(Textures->Get(SourceList,SourceName)->CreateMask());

		int iQuadSize(GetQuadSize(Mask->GetWidth()  + 2*StrokeSize,
								  Mask->GetHeight() + 2* StrokeSize));

		// создание новой пустой текстуры
		pTBaseTextureCreater TexCreater(Device->CreateTextureCreater());
		pTTexture TextureStroke  = TexCreater->Run(TextureName, iQuadSize, iQuadSize);
		delete TexCreater;

		pTTextureRawData RawData(Mask->GetCopyRawData(Mask->GetWidth(), Mask->GetHeight()));
		delete Mask;

		TextureStroke->WriteRawData(RawData, StrokeSize, 2 * StrokeSize, EN_TWM_Or);
		TextureStroke->WriteRawData(RawData, StrokeSize, 0, EN_TWM_Or);
		TextureStroke->WriteRawData(RawData, 0, StrokeSize, EN_TWM_Or);
		TextureStroke->WriteRawData(RawData, 2 * StrokeSize, StrokeSize, EN_TWM_Or);

		delete RawData;

		TextureStroke->SetName(TextureName);
		List->Add(TextureStroke);
		return;
	}
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::ProcessLoad(const TEvent &Event)
{
	// загрузка сразу всех текстур и выход из функции
	if(Event == false){
		int NodeSize(Node.GetSize());
		for(int i=NodeListItr;i<NodeSize;i++){
			pTDataNode ListNode(Node.Get(i));

			pTTextureList TextureList(NULL);
			wstring ListName(ListNode->GetChildString(L"name"));

			if(Textures->IfExist(ListName)){ TextureList = Textures->Get(ListName); }
			else                           { TextureList = Textures->Add(ListName); }

			int NodeListSize(ListNode->GetSize());
			for(int j=NodeTextureItr;j<NodeListSize;j++){

				pTDataNode TextureNode(ListNode->Get(j));
				if(TextureNode->GetName() == L"Texture"){
					LoadTexture(TextureList,TextureNode);
				}
			}

			NodeListItr    = i;
			NodeTextureItr = 0;
		}

		Stop();
		return;
	}

	// это загрузка текстур последовательно, по приходу события таймера
	if(Event.Type != EN_TIMER){return;}

	pTDataNode ListNode(Node.Get(NodeListItr));
	int ListNodeSize(ListNode->GetSize());

	// если достигли конца перечня текстур, то надо переключиться на следующий
	// нод списка, если же и там достигли конца, то останавливаем работы екшена
	if(NodeTextureItr >= ListNodeSize){
		NodeTextureItr = 0;
		NodeListItr++;
		if(NodeListItr >= Node.GetSize()){
			Stop();
			return;
		}
	}

	pTTextureList TextureList(NULL);
	wstring ListName(ListNode->GetChildString(L"name"));

	if(Textures->IfExist(ListName)){ TextureList = Textures->Get(ListName); }
	else                           { TextureList = Textures->Add(ListName); }

	// это поиск ноды текстуры, нужен, так как кроме самих нод текстур в
	// описании списка могут сожержаться и другие ноды, например имя списка
	pTDataNode TextureNode   (NULL);
	pTDataNode TmpTextureNode(NULL);

	while(NodeTextureItr < ListNodeSize){
		TmpTextureNode = ListNode->Get(NodeTextureItr);
		if(TmpTextureNode->GetName() == L"Texture"){ TextureNode = TmpTextureNode; }
		NodeTextureItr++;

		if(TextureNode != NULL){
			LoadTexture(TextureList, TextureNode);
			break;
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::ProcessClear(const TEvent &Event)
{
	// удаление сразу всех текстур и выход из функции
	if(Event == false){

		int NodeSize(Node.GetSize());
		for(int i=NodeListItr;i<NodeSize;i++){
			pTDataNode ListNode(Node.Get(i));

			wstring ListName(ListNode->GetChildString(L"name"));
			pTTextureList TextureList(Textures->Get(ListName));

			int NodeListSize(ListNode->GetSize());
			for(int j=NodeTextureItr;j<NodeListSize;j++){
				pTDataNode TextureNode(ListNode->Get(j));
				if(TextureNode->GetName() == L"Texture"){
					TextureList->Del   (TextureNode->GetChildString(L"name"));
				}
			}

			if(TextureList->GetSize() <=0){Textures->Del(ListName);}

			NodeListItr    = i;
			NodeTextureItr = 0;
		}

		Stop();
		return;
	}

	// последовательная выгрузка текстур
	if(Event.Type != EN_TIMER){return;}

	pTDataNode ListNode(Node.Get(NodeListItr));
	int ListNodeSize(ListNode->GetSize());

	// если достигли конца перечня текстур, то надо переключиться на следующий
	// нод списка, если же и там достигли конца, то останавливаем работы екшена
	if(NodeTextureItr >= ListNodeSize){
		NodeTextureItr = 0;
		NodeListItr++;
		if(NodeListItr >= Node.GetSize()){
			Stop();
			return;
		}
	}

	pTTextureList TextureList(NULL);
	wstring ListName(ListNode->GetChildString(L"name"));

	TextureList = Textures->Get(ListName);

	// это поиск ноды текстуры, нужен, так как кроме самих нод текстур в
	// описании списка могут сожержаться и другие ноды, например имя списка
	pTDataNode TextureNode   (NULL);
	pTDataNode TmpTextureNode(NULL);

	while(NodeTextureItr < ListNodeSize){
		TmpTextureNode = ListNode->Get(NodeTextureItr);
		if(TmpTextureNode->GetName() == L"Texture"){ TextureNode = TmpTextureNode; }
		NodeTextureItr++;

		if(TextureNode != NULL){
			TextureList->Del(TextureNode->GetChildString(L"name"));
			if(TextureList->GetSize() <=0){Textures->Del(ListName);}
			break;
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::ToRun(const TEvent &Event)
{
	try{ if(LoadTextures){ ProcessLoad(Event); }else{ ProcessClear(Event); } }
	catch(TException &Ex){ Exception(L"in Run(...): " + Ex.GetMess()); }
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::ToStart()
{
	Exception(Textures != NULL , L"in Start() Textures not set");
	Exception(Device   != NULL , L"in Start() Device not set");
	Exception(Node.GetSize()>0 , L"in Start() Node is empty");

	NodeListItr    = 0;
	NodeTextureItr = 0;
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::ToStop()
{
	NodeListItr    = 0;
	NodeTextureItr = 0;
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::operator =(TActionTexturesLoader &ObjAction)
{
	Active = ObjAction.GetActive();
	Name   = ObjAction.GetName  ();

	Set(ObjAction.GetTextures(),ObjAction.GetDevice(),ObjAction.GetNode());

	LoadTextures = ObjAction.GetLoadTextures();

	NodeListItr    = ObjAction.GetNodeListItr();
	NodeTextureItr = ObjAction.GetNodeTextureItr();
}
///----------------------------------------------------------------------------------------------//
TBaseAction* TActionTexturesLoader::Clone()
{
	return new TActionTexturesLoader(*this);
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::Set(const pTTextureListManager &Val)
{
	Textures = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::Set(const TDataNode &Val)
{
	Node = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::Set(const pTTextureListManager &TexturesVal,
								const pTBaseDevice &DeviceVal, const TDataNode &NodeVal)
{
	Exception(DeviceVal!=NULL,L"in Set(TexturesVal,NodeVal,DeviceVal) DeviceVal is NULL");

	Textures = TexturesVal;
	Device   = DeviceVal;
	Node     = NodeVal;

	TextureLoader.SetDevice(Device);
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::Set(const pTTextureListManager &TexturesVal,
								const pTBaseDevice &DeviceVal, const pTDataNode &NodeVal)
{


	Exception(NodeVal  != NULL,L"in Set(TexturesVal,NodeVal,DeviceVal) NodeVal is NULL");
	Exception(DeviceVal!= NULL,L"in Set(TexturesVal,NodeVal,DeviceVal) DeviceVal is NULL");

	Textures = TexturesVal;
	Device   = DeviceVal;
	Node     = *NodeVal;

	TextureLoader.SetDevice(Device);
}
///----------------------------------------------------------------------------------------------//
pTTextureListManager TActionTexturesLoader::GetTextures()
{
	return Textures;
}
///----------------------------------------------------------------------------------------------//
pTBaseDevice TActionTexturesLoader::GetDevice()
{
	return Device;
}
///----------------------------------------------------------------------------------------------//
TDataNode TActionTexturesLoader::GetNode() const
{
	return Node;
}
///----------------------------------------------------------------------------------------------//
int TActionTexturesLoader::GetNodeListItr() const
{
	return NodeListItr;
}
///----------------------------------------------------------------------------------------------//
int TActionTexturesLoader::GetNodeTextureItr() const
{
	return NodeTextureItr;
}
///----------------------------------------------------------------------------------------------//
void TActionTexturesLoader::SetLoadTextures(const bool &Val)
{
	LoadTextures = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionTexturesLoader::GetLoadTextures() const
{
	return LoadTextures;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
