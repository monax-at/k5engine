///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONJOIN_H_INCLUDED
#define ACTIONJOIN_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TActionJoin:public TBaseAction
{
	protected:
		// указатели на параметры родительского объекта
		pTPoint ParrPos;
		pTValue ParrAngle;

		// значения параметров радительского объекта до изменения
		TPoint ParrOldPos;
		TValue ParrOldAngle;

		// указатели на параметры дочернего объекта
		pTPoint ChildPos;
		pTValue ChildAngle;

		// старые относительные позиция и угол
		TPoint OldPos;
		TValue OldAngle;
	public:
		TPoint Pos;    // относительная позиция, одновременно и как дистанция от Parr до Child выступает
		TValue Angle;  // относительный угол поворота Child относительно Parr
	protected:
		void InitParams();

		bool UpdatePos();
		bool UpdateAngle();

		void ToRun(const TEvent &Event);
	public:
		TActionJoin();
		TActionJoin(TActionJoin &ObjAction);
		TActionJoin(const pTPoint &PPos, const pTValue   &PAngle,
					const pTPoint &CPos, const pTValue   &CAngle);
		virtual ~TActionJoin();

		void operator =(TActionJoin &ObjAction);

		TBaseAction* Clone();

		void Set(const pTPoint &PPos, const pTValue &PAngle,
				 const pTPoint &CPos, const pTValue &CAngle);

		void SetParrent(const pTPoint &PosVal, const pTValue &AngleVal);
		void SetChild  (const pTPoint &PosVal, const pTValue &AngleVal);

		void SetParrPos  (const pTPoint &Val);
		void SetParrAngle(const pTValue &Val);

		void SetChildPos  (const pTPoint &Val);
		void SetChildAngle(const pTValue &Val);

		pTPoint GetParrPos  ();
		pTValue GetParrAngle();

		TPoint  GetParrOldPos  () const;
		TValue  GetParrOldAngle() const;

		pTPoint GetChildPos  ();
		pTValue GetChildAngle();

		TPoint GetOldPos  () const;
		TValue GetOldAngle() const;

		bool UsePosTransform  () const;
		bool UseAngleTransform() const;
};

typedef TActionJoin* pTActionJoin;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // ACTIONJOIN_H_INCLUDED
