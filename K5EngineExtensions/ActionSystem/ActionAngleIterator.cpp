#include "ActionAngleIterator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionAngleIterator::TActionAngleIterator():
				TActionBaseIterator(), InitMode(EN_AAIIM_Unknown), Angle(NULL),
				OldAngle(0.0f), NewAngle(0.0f),Dir(1),Speed(0.0f), Time(0.0f),
				UseMinAngle(true), DistanceTmp(0.0f),Distance(0.0f), DistanceCount(0.0f)
{
}
///----------------------------------------------------------------------------------------------//
TActionAngleIterator::TActionAngleIterator(TActionAngleIterator &ObjAction):
				TActionBaseIterator(), InitMode(EN_AAIIM_Unknown), Angle(NULL),
				OldAngle(0.0f), NewAngle(0.0f),Dir(1),Speed(0.0f), Time(0.0f),
				UseMinAngle(true), DistanceTmp(0.0f),Distance(0.0f), DistanceCount(0.0f)
{
	*this = ObjAction;
}
///----------------------------------------------------------------------------------------------//
TActionAngleIterator::~TActionAngleIterator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::CountDistAngle()
{
	float DistAngle(GetMinDirAngle2D(NewAngle,OldAngle));

	if(!UseMinAngle){
		float NewDistAngle(360.0f - Abs(DistAngle));
		if(DistAngle>=0.0f){NewDistAngle*= -1.0f;}
		DistAngle = NewDistAngle;
	}

	if(DistAngle>0){ Dir = 1; } else{ Dir = -1; }

	Distance = Abs(DistAngle);
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::CountDist()
{
	if(DistanceTmp >= 0.0f){ Dir = 1; }else{ Dir = -1; }
	Distance = Abs(DistanceTmp);
	NewAngle = RecountAngle(OldAngle += DistanceTmp);
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::CountSpeed()
{
	if(Time > 0.0f){ Speed = (Distance/Time)*1000.0f; }
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::CountTime()
{
	if(Speed > 0.0f){ Time = (Distance*1000.0f)/Speed; }
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::ResetParams()
{
	InitMode = EN_AAIIM_Unknown;

	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	NewAngle = 0.0f;

	Speed = 0.0f;
	Time  = 0.0f;

	Distance      = 0.0f;
	DistanceCount = 0.0f;
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::Init()
{
	if(Angle == NULL || InitMode == EN_AAIIM_Unknown){return;}

	OldAngle = RecountAngle(Angle->Get());

	switch(InitMode){
		case EN_AAIIM_AngleAndTime :{
			CountDistAngle();
			CountSpeed();
		}break;

		case EN_AAIIM_AngleAndSpeed:{
			CountDistAngle();
			CountTime();
		}break;

		case EN_AAIIM_DistAndTime :{
			CountDist();
			CountSpeed();
		}break;

		case EN_AAIIM_DistAndSpeed :{
			CountDist();
			CountTime();
		}break;

		case EN_AAIIM_CyclicBySpeed:{

		}break;

		case EN_AAIIM_CyclicByTime:{
			if(Time > 0.0f){ Speed = (DistanceTmp/Time)*1000.0f; }
			if( Speed >= 0){ Dir = 1; } else { Dir = -1; }
			Speed = Abs(Speed);
		}break;

		default:break;
	}
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::ToRun(const TEvent &Event)
{
	if(Angle == NULL){ return; }

	// при пустом собитии екшен просто установит финальное значение угла
	if(Event == false){
		Angle->Set(NewAngle);
		Stop();
		return;
	}

	// обработка, если событие не пустое
	if(Event.Type != EN_TIMER){ return; }

	// задержка выполнения действия
	if(IsDelay(Event)){return;}

	if(InitMode == EN_AAIIM_CyclicBySpeed || InitMode == EN_AAIIM_CyclicByTime){
		Angle->Inc(Event.Timer.Sec*Speed*Dir);
	}
	else{
		// если включено моментальное срабатывание, то дистанция будет пройдена сразу
		float DeltaDist(Distance);
		if(Time > 0.0f){ DeltaDist = Event.Timer.Sec*Speed; }

		DistanceCount += DeltaDist;

		// остановка и выход, если весь путь пройден
		if(DistanceCount < Distance){
			Angle->Inc(DeltaDist*Dir);
		}
		else{
			Angle->Set(NewAngle);
			Stop();
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::ToStart()
{
	DistanceCount   = 0.0f;
	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::ToStop()
{
	if(FinishOnStop && Angle != NULL){ Angle->Set(NewAngle); }
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::operator =(TActionAngleIterator &ObjAction)
{
	Angle  = ObjAction.Get();

	InitMode = ObjAction.GetInitMode();

	OldAngle = ObjAction.GetOldAngle();
	NewAngle = ObjAction.GetNewAngle();

	Dir   = ObjAction.GetDir();
	Speed = ObjAction.GetSpeed();
	Time  = ObjAction.GetTime();

	UseMinAngle = ObjAction.GetUseMinAngle();

	DistanceTmp   = ObjAction.GetDistanceTmp();
	Distance      = ObjAction.GetDistance();
	DistanceCount = ObjAction.GetDistanceCount();

	SetDefParams(&ObjAction);
}
///----------------------------------------------------------------------------------------------//
TBaseAction* TActionAngleIterator::Clone()
{
	return new TActionAngleIterator(*this);
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::Set(const pTValue &Val)
{
	Angle = Val;
}
///----------------------------------------------------------------------------------------------//
pTValue TActionAngleIterator::Get()
{
	return Angle;
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::UseAngleAndTime(const float &NewAngleVal,
										   const float &TimeVal,  bool MinAngl )
{
	ResetParams();

	InitMode = EN_AAIIM_AngleAndTime;

	NewAngle    = NewAngleVal;
	Time        = TimeVal;
	UseMinAngle = MinAngl;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::UseAngleAndTime(const TValue &NewAngleVal,
										   const float &TimeVal,  bool MinAngl )
{
	UseAngleAndTime(NewAngleVal.Get(), TimeVal, MinAngl);
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::UseAngleAndSpeed(const float &NewAngleVal,
											const float &SpeedVal, bool MinAngl )
{
	ResetParams();

	InitMode = EN_AAIIM_AngleAndSpeed;

	NewAngle    = NewAngleVal;
	Speed       = SpeedVal;
	UseMinAngle = MinAngl;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::UseAngleAndSpeed(const TValue &NewAngleVal,
											const float &SpeedVal, bool MinAngl )
{
	UseAngleAndSpeed(NewAngleVal.Get(), SpeedVal, MinAngl);
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::UseDistAndTime(const float &AngleDistVal,
											    const float &TimeVal )
{
	ResetParams();

	InitMode = EN_AAIIM_DistAndTime;

	DistanceTmp = AngleDistVal;
	Time        = TimeVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::UseDistAndSpeed (const float &AngleDistVal,
												  const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_AAIIM_DistAndSpeed;

	DistanceTmp = AngleDistVal;
	Speed       = SpeedVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::UseCyclicBySpeed(const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_AAIIM_CyclicBySpeed;

	Speed = SpeedVal;
	if( Speed >= 0){ Dir = 1; } else { Dir = -1; }
	Speed = Abs(Speed);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionAngleIterator::UseCyclicByTime (const float &AngleDistVal, const float &TimeVal)
{
	ResetParams();

	InitMode = EN_AAIIM_CyclicByTime;

	DistanceTmp = AngleDistVal;
	Time        = TimeVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
bool TActionAngleIterator::IsCyclicBySpeed() const
{
	return InitMode == EN_AAIIM_CyclicBySpeed;
}
///----------------------------------------------------------------------------------------------//
bool TActionAngleIterator::IsCyclicByTime() const
{
	return InitMode == EN_AAIIM_CyclicByTime;
}
///----------------------------------------------------------------------------------------------//
enAAIInitMode TActionAngleIterator::GetInitMode() const
{
	return InitMode;
}
///----------------------------------------------------------------------------------------------//
float TActionAngleIterator::GetOldAngle() const
{
	return OldAngle;
}
///----------------------------------------------------------------------------------------------//
float TActionAngleIterator::GetNewAngle() const
{
	return NewAngle;
}
///----------------------------------------------------------------------------------------------//
int TActionAngleIterator::GetDir() const
{
	return Dir;
}
///----------------------------------------------------------------------------------------------//
float TActionAngleIterator::GetSpeed() const
{
	return Speed;
}
///----------------------------------------------------------------------------------------------//
float TActionAngleIterator::GetTime() const
{
	return Time;
}
///----------------------------------------------------------------------------------------------//
bool TActionAngleIterator::GetUseMinAngle() const
{
	return UseMinAngle;
}
///----------------------------------------------------------------------------------------------//
float TActionAngleIterator::GetDistanceTmp() const
{
	return DistanceTmp;
}
///----------------------------------------------------------------------------------------------//
float TActionAngleIterator::GetDistance() const
{
	return Distance;
}
///----------------------------------------------------------------------------------------------//
float TActionAngleIterator::GetDistanceCount() const
{
	return DistanceCount;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

