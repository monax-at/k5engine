///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONBASEITERATOR_H_INCLUDED
#define ACTIONBASEITERATOR_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
enum enAAIInitMode // перечесление для итератора углов
{
	EN_AAIIM_Unknown       = 0,
	EN_AAIIM_AngleAndTime  = 1,
	EN_AAIIM_AngleAndSpeed = 2,
	EN_AAIIM_DistAndTime   = 3,
	EN_AAIIM_DistAndSpeed  = 4,
	EN_AAIIM_CyclicBySpeed = 5,
	EN_AAIIM_CyclicByTime  = 6
};
///----------------------------------------------------------------------------------------------//
enum enAPIInitMode // перечесление для итераторов точек
{
	EN_APIIM_Unknown        = 0,
	EN_APIIM_PosAndSpeed    = 1,
	EN_APIIM_PosAndTime     = 2,
	EN_APIIM_DistAndSpeed   = 3,
	EN_APIIM_DistAndTime    = 4
};
///----------------------------------------------------------------------------------------------//
enum enASSIMode // перечисление для итератора square спрайта
{
	EN_ASSIM_Unknown        = 0,
	EN_ASSIM_DistAndSpeed   = 1,
	EN_ASSIM_DistAndTime    = 2,
	EN_ASSIM_ValAndSpeed    = 3,
	EN_ASSIM_ValAndTime     = 4
};
///----------------------------------------------------------------------------------------------//
enum enACIInitMode
{
	EN_ACIIM_Unknown      = 0,
	EN_ACIIM_ValAndSpeed  = 1,
	EN_ACIIM_VAlAndTime   = 2,
	EN_ACIIM_DistAndSpeed = 3,
	EN_ACIIM_DistAndTime  = 4
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TActionBaseIterator:public TBaseAction
{
	protected:
		bool  UseDelay;
		float Delay;
		float DelayTimer;
		bool  DelayTimerReady;

		// флаг указывает, надо ли при остановке действия до завершения итерации
		// поставить финишное значение переменной или нет
		bool FinishOnStop;
	protected:
		bool IsDelay(const TEvent &Event);
		void SetDefParams(TActionBaseIterator *ObjAction);
	public:
		TActionBaseIterator();
		virtual ~TActionBaseIterator();

		void  SetDelay(const float &Val);
		float GetDelay() const;

		void SetFinishOnStop(const bool &Val);
		bool GetFinishOnStop() const;

		bool  GetUseDelay       () const;
		float GetDelayTimer     () const;
		bool  GetDelayTimerReady() const;
};

typedef TActionBaseIterator* pTActionBaseIterator;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // ACTIONBASEITERATOR_H_INCLUDED
