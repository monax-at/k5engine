#include "ActionJoin.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionJoin::TActionJoin():TBaseAction(), ParrPos (NULL), ParrAngle (NULL),
										  ChildPos(NULL), ChildAngle(NULL)
{
}
///----------------------------------------------------------------------------------------------//
TActionJoin::TActionJoin(TActionJoin &ObjAction):TBaseAction(), ParrPos (NULL), ParrAngle (NULL),
										  						ChildPos(NULL), ChildAngle(NULL)
{
	*this = ObjAction;
}
///----------------------------------------------------------------------------------------------//
TActionJoin::TActionJoin(const pTPoint &PPos, const pTValue   &PAngle,
						 const pTPoint &CPos, const pTValue   &CAngle):TBaseAction(),
						ParrPos (PPos), ParrAngle (PAngle), ChildPos(CPos), ChildAngle(CAngle)
{
	InitParams();
}
///----------------------------------------------------------------------------------------------//
TActionJoin::~TActionJoin()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionJoin::InitParams()
{
	if(ParrPos != NULL && ChildPos != NULL ){
		ParrOldPos = *ParrPos;
		Pos = *ChildPos - *ParrPos;
	}

	if(ParrAngle != NULL && ChildAngle != NULL ){
		ParrOldAngle = *ParrAngle;
		Angle = *ChildAngle - *ParrAngle;
	}
}
///----------------------------------------------------------------------------------------------//
bool TActionJoin::UpdatePos()
{
	if( ParrPos == NULL || ChildPos == NULL )    { return false; }

	if( ParrAngle == NULL ){
		if( *ParrPos == ParrOldPos && OldPos == Pos) { return false; }
	}
	else{
		if(ChildAngle == NULL){
			if( *ParrPos == ParrOldPos && OldPos == Pos && *ParrAngle == ParrOldAngle )
			{ return false; }

			if(*ParrAngle != ParrOldAngle){ ParrOldAngle = *ParrAngle; }
		}
	}

	if(*ParrPos != ParrOldPos){ ParrOldPos = *ParrPos; }
	if(OldPos   != Pos)       { OldPos     = Pos;      }

	return true;
}
///----------------------------------------------------------------------------------------------//
bool TActionJoin::UpdateAngle()
{
	if( ParrAngle == NULL || ChildAngle == NULL )        { return false; }
	if( *ParrAngle == ParrOldAngle && OldAngle == Angle) { return false; }

	if(*ParrAngle != ParrOldAngle){ ParrOldAngle = *ParrAngle; }
	if( OldAngle  != Angle)       { OldAngle     =  Angle;     }

	return true;
}
///----------------------------------------------------------------------------------------------//
void TActionJoin::ToRun(const TEvent &Event)
{
	// проверка, были ли обновления в позиции родителя или угле наклона, если не было
	// то выполняем выход
	if(!UpdatePos() && !UpdateAngle()){ return; }

	// самое простое - обновление угла поворота объекта
	if( ParrAngle != NULL && ChildAngle != NULL ){
		*ChildAngle = *ParrAngle;
		ChildAngle->Inc(Angle);
	}

	// а вот тут посложней, обновление позиции в зависимости от наличия угла поворота родительского
	// объекта, конечно если угол задан
	if(ParrPos == NULL || ChildPos == NULL ){return;}

	if(ParrAngle == NULL){
		*ChildPos = *ParrPos;
		ChildPos->Inc(Pos);
	}
	else{
		TVector2D GlobalDir(GetVector2DFromAngle(ParrAngle->Get() + GetAngle2D(Pos)));
		GlobalDir *= GetDistance2D(Pos);

		*ChildPos = *ParrPos;
		ChildPos->Inc(GlobalDir.x, GlobalDir.y, Pos.GetZ());
	}
}
///----------------------------------------------------------------------------------------------//
void TActionJoin::operator =(TActionJoin &ObjAction)
{
	Active = ObjAction.GetActive();
	Name   = ObjAction.GetName ();

	ParrPos   = ObjAction.GetParrPos();
	ParrAngle = ObjAction.GetParrAngle();

	ParrOldPos   = ObjAction.GetParrOldPos();
	ParrOldAngle = ObjAction.GetParrOldAngle();

	ChildPos   = ObjAction.GetChildPos();
	ChildAngle = ObjAction.GetChildAngle();

	Pos   = ObjAction.Pos;
	Angle = ObjAction.Angle;
}
///----------------------------------------------------------------------------------------------//
TBaseAction* TActionJoin::Clone()
{
	return new TActionJoin(*this);
}
///----------------------------------------------------------------------------------------------//
void TActionJoin::Set(	const pTPoint &PPos, const pTValue   &PAngle,
						const pTPoint &CPos, const pTValue   &CAngle)
{
	ParrPos    = PPos;
	ParrAngle  = PAngle;

	ChildPos   = CPos;
	ChildAngle = CAngle;

	InitParams();
}
///----------------------------------------------------------------------------------------------//
void TActionJoin::SetParrent(const pTPoint &PosVal, const pTValue   &AngleVal)
{
	ParrPos   = PosVal;
	ParrAngle = AngleVal;

	InitParams();
}
///----------------------------------------------------------------------------------------------//
void TActionJoin::SetChild  (const pTPoint &PosVal, const pTValue   &AngleVal)
{
	ChildPos   = PosVal;
	ChildAngle = AngleVal;

	InitParams();
}
///----------------------------------------------------------------------------------------------//
void TActionJoin::SetParrPos  (const pTPoint &Val)
{
	ParrPos = Val;
	InitParams();
}
///----------------------------------------------------------------------------------------------//
void TActionJoin::SetParrAngle(const pTValue   &Val)
{
	ParrAngle = Val;
	InitParams();
}
///----------------------------------------------------------------------------------------------//
void TActionJoin::SetChildPos  (const pTPoint &Val)
{
	ChildPos = Val;
	InitParams();
}
///----------------------------------------------------------------------------------------------//
void TActionJoin::SetChildAngle(const pTValue   &Val)
{
	ChildAngle = Val;
	InitParams();
}
///----------------------------------------------------------------------------------------------//
pTPoint TActionJoin::GetParrPos()
{
	return ParrPos;
}
///----------------------------------------------------------------------------------------------//
pTValue TActionJoin::GetParrAngle()
{
	return ParrAngle;
}
///----------------------------------------------------------------------------------------------//
TPoint TActionJoin::GetParrOldPos() const
{
	return ParrOldPos;
}
///----------------------------------------------------------------------------------------------//
TValue TActionJoin::GetParrOldAngle() const
{
	return ParrOldAngle;
}
///----------------------------------------------------------------------------------------------//
pTPoint TActionJoin::GetChildPos()
{
	return ChildPos;
}
///----------------------------------------------------------------------------------------------//
pTValue TActionJoin::GetChildAngle()
{
	return ChildAngle;
}
///----------------------------------------------------------------------------------------------//
TPoint TActionJoin::GetOldPos  () const
{
	return OldPos;
}
///----------------------------------------------------------------------------------------------//
TValue   TActionJoin::GetOldAngle() const
{
	return OldAngle;
}
///----------------------------------------------------------------------------------------------//
bool TActionJoin::UsePosTransform  () const
{
	return ParrPos != NULL && ChildPos != NULL;
}
///----------------------------------------------------------------------------------------------//
bool TActionJoin::UseAngleTransform() const
{
	return ParrAngle != NULL && ChildAngle != NULL;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
