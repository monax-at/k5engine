///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	Автор:
///		Maxim Kolesnikov [mpkmaximus@gmail.com]
///		Sergey Kalenik. [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONCONTAINER_H_INCLUDED
#define ACTIONCONTAINER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
// контейнер группового выполнения екшенов
// производить копирование контейнейра нельзя

class TActionContainer:public TBaseAction
{
	protected:
		TActionList Actions;
	protected:
		void ToRun(const TEvent &Event);
		void ToStart();
		void ToStop();
	private:
		TActionContainer(TActionContainer &ObjAction);
		void operator =(TActionContainer &ObjAction);
	public:
		TActionContainer();
		virtual ~TActionContainer();

		pTBaseAction Add(const pTBaseAction &Val);

		pTBaseAction Get(const int           &ObjIndex);
		pTBaseAction Get(const unsigned long &ObjID);
		pTBaseAction Get(const wstring       &ObjName);

		void Del(const int           &ObjIndex);
		void Del(const unsigned long &ObjID);
		void Del(const wstring       &ObjName);

		int GetSize() const;

		void Clear();

		pTActionList GetActionsPtr();
};

typedef TActionContainer* pTActionContainer;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // ACTIONCONTAINER_H_INCLUDED
