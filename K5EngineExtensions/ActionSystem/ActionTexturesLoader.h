///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONTEXTURESLOADER_H_INCLUDED
#define ACTIONTEXTURESLOADER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
#include "../DataNodeSystem/Base/DataNode.h"
#include "../LibTools/FolderWorker.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
// екшен загружает или удаляет из описаний Node текстуры в менеджер текстур
// режим работы (загружать или удалять) управляется флагом LoadTextures, который по
// умолчанию true
// так же есть возможность загрузить все текстуры разом или загружать по одному за
// один приход события таймера. Если классу приходит пустое событие, то автоматически
// будут загружены/удалены все екшены из описаний в Node. При приходе обычного события
// таймера текстуры будут грузиться последовательно по одной за событие

// Внимание! для индикации работы екшена внутренние переменные NodeListItr и NodeTextureItr
// не подходят

class TActionTexturesLoader:public TBaseAction
{
	protected:
		TExceptionGenerator  Exception;

		pTBaseDevice         Device;
		pTTextureListManager Textures;

		TDataNode            Node;
		bool                 LoadTextures;

		TTextureLoader       TextureLoader;
		TFolderWorker        FolderWorker;

		// временные переменные
		int NodeListItr;
		int NodeTextureItr;
	protected:
		inline int GetQuadSize(const int &WidthVal,const int &HeightVal);
		inline void LoadTexture(const pTTextureList &List, const pTDataNode &TextureNode);

		void ProcessLoad  (const TEvent &Event);
		void ProcessClear (const TEvent &Event);

		void ToRun(const TEvent &Event);
		void ToStart();
		void ToStop();
	public:
		TActionTexturesLoader();
		TActionTexturesLoader(TActionTexturesLoader &ObjAction);

		TActionTexturesLoader(const pTTextureListManager &TexturesVal,
		                      const pTBaseDevice &DeviceVal, const TDataNode  &NodeVal,
		                      const bool &LoadTexturesVal = true);

		TActionTexturesLoader(const pTTextureListManager &TexturesVal,
		                      const pTBaseDevice &DeviceVal, const pTDataNode &NodeVal,
		                      const bool &LoadTexturesVal = true);

		virtual ~TActionTexturesLoader();

		void operator =(TActionTexturesLoader &ObjAction);

		TBaseAction* Clone();

		void Set(const pTTextureListManager &Val);
		void Set(const TDataNode            &Val);
		void Set(const pTBaseDevice         &Val);

		void Set(const pTTextureListManager &TexturesVal, const pTBaseDevice &DeviceVal,
				 const TDataNode  &NodeVal);

		void Set(const pTTextureListManager &TexturesVal, const pTBaseDevice &DeviceVal,
				 const pTDataNode &NodeVal);

		pTTextureListManager GetTextures();
		pTBaseDevice         GetDevice();

		TDataNode GetNode          () const;
		int       GetNodeListItr   () const;
		int       GetNodeTextureItr() const;

		// переключает режим работы, по умолчанию true - загружать текстуры
		// если поставить false - текстуры будут удаляться
		void SetLoadTextures(const bool &Val);
		bool GetLoadTextures() const;
};

typedef TActionTexturesLoader* pTActionTexturesLoader;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // EVENTACTIONTEXTURESLOADER_H_INCLUDED
