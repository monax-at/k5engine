///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONPOINTARRAYITERATOR_H_INCLUDED
#define ACTIONPOINTARRAYITERATOR_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "ActionBaseIterator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TActionPointArrayIterator:public TActionBaseIterator
{
	protected:
		enAPIInitMode InitMode;

		pTPointArray Array;
		int PointId;

		TPoint NewPos;
		TPoint OldPos;

		float Speed;
		float Time;

		TVector3D DistDir;
		TVector3D Dir;
		float Distance;
		float DistanceCount;

		bool UseZ;
	protected:
		inline void ResetParams();

		inline void InitPosAndSpeedParams ();
		inline void InitDistAndSpeedParams();

		inline void CountSpeed();
		inline void CountTime();

		inline void Init();

		void ToRun(const TEvent &Event);
		void ToStart();
		void ToStop();
	public:
		TActionPointArrayIterator();
		TActionPointArrayIterator(TActionPointArrayIterator &ObjAction);
		virtual ~TActionPointArrayIterator();

		void operator =(TActionPointArrayIterator &ObjAction);

		TBaseAction* Clone();

		void Set(const pTPointArray &Val, const int &PointIdVal = 0);

		void SetArray  (const pTPointArray &Val);
		void SetPointId(const int &Val);

		pTPointArray GetArray();
		int GetPointId() const;

		void SetUseZ(const bool &Val);
		bool GetUseZ() const;

		void UsePosAndSpeed(const float &X, const float &Y, const float &SpeedVal);
		void UsePosAndSpeed(const float &X, const float &Y, const float &Z,
							const float &SpeedVal);

		void UsePosAndSpeed(const TPoint &NewPosVal, const float &SpeedVal,
							const bool &UseZVal = true);

		void UsePosAndTime (const float &X, const float &Y, const float &TimeVal);
		void UsePosAndTime (const float &X, const float &Y, const float &Z,
							const float &TimeVal);

		void UsePosAndTime (const TPoint &NewPosVal, const float &TimeVal,
							const bool &UseZVal = true);

		void UseDistAndSpeed(const float &X, const float &Y, const float &SpeedVal);
		void UseDistAndSpeed(const float &X, const float &Y, const float &Z,
							 const float &SpeedVal);
		void UseDistAndSpeed(const TPoint &DistVal, const float &SpeedVal);

		void UseDistAndTime(const float &X, const float &Y, const float &TimeVal);
		void UseDistAndTime(const float &X, const float &Y, const float &Z,
							const float &TimeVal);
		void UseDistAndTime(const TPoint &DistVal, const float &TimeVal);

		enAPIInitMode GetInitMode() const;

		TPoint GetNewPos() const;
		TPoint GetOldPos() const;

		float GetSpeed() const;
		float GetTime () const;

		TVector3D GetDistDir  () const;
		TVector3D GetDir      () const;
		float GetDistance     () const;
		float GetDistanceCount() const;
};

typedef TActionPointArrayIterator* pTActionPointArrayIterator;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // ACTIONPOINTITERATOR_H_INCLUDED
