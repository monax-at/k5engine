///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONSPRITETEXTUREANIMATION_H_INCLUDED
#define ACTIONSPRITETEXTUREANIMATION_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TActionSpriteTextureAnimation:public TBaseAction
{
	protected:
		TExceptionGenerator Exception;

		pTSprite Sprite;

		struct TAnimRect{
			pTTexture Texture;
			float X;
			float Y;
			float Width;
			float Height;
			bool  ChangeSpriteSize;
		};

		vector<TAnimRect*> AnimRects;
		vector<int> Frames;

		int  AnimCursor;
		bool Cyclic;

		float FrameTime;
		float FrameTimer;

		bool FinishOnStop;
	protected:
		inline void AddAnimRect(const pTTexture &Texture, const float &X, const float &Y,
								const float &Width, const float &Height,
								const bool &ChangeSpriteSize);

		inline void UpdateSpriteTexture();
		inline void SwitchShot();

		void ToRun(const TEvent &Event);
		void ToStart();
		void ToStop();
	public:
		TActionSpriteTextureAnimation();
		TActionSpriteTextureAnimation(TActionSpriteTextureAnimation &ObjAction);
		virtual ~TActionSpriteTextureAnimation();

		void operator =(TActionSpriteTextureAnimation &ObjAction);

		TBaseAction* Clone();

		void Set(const pTSprite &Val);
		pTSprite Get();

		void  SetFrameTime(const float &Val);
		float GetFrameTime() const;
		float GetFrameTimer() const;

		void SetFinishOnStop(const bool &Val);
		bool GetFinishOnStop() const;

		void AddRect(	const pTTexture &Texture ,const float &X, const float &Y,
						const float &Width, const float &Height,
						const bool &ChangeSpriteSize = false);

		void AddRect(	const float &X, const float &Y,
						const float &Width, const float &Height,
						const bool &ChangeSpriteSize = false);

		void AddRect(	const pTTexture &Texture      , const bool &ChangeSpriteSize = false);
		void AddRect(   const pTSpriteTexture &Texture, const bool &ChangeSpriteSize = false);

		void Clear();
		void ClearFrames();

		int GetRectsSize() const;

		pTTexture GetRectTexture(const int &RectID);
		float GetRectX(const int &RectID) const;
		float GetRectY(const int &RectID) const;
		float GetRectWidth (const int &RectID) const;
		float GetRectHeight(const int &RectID) const;
		bool  GetRectChangeSpriteSizeFlag(const int &RectID) const;

		void AddFrameId(const int &Id);
		int  GetFramesSize() const;
		int  GetFrameId(const int &FrameID) const;

		void SetCyclic(const bool &Val);
		bool GetCyclic() const;

		void SetFrame(const int &Val);
		void SetLastFrame();
		int  GetFrame() const;
};

typedef TActionSpriteTextureAnimation* pTActionSpriteTextureAnimation;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // ACTIONSPRITETEXTUREANIMATION_H_INCLUDED
