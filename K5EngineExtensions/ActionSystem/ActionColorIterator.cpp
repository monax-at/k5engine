#include "ActionColorIterator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionColorIterator::TActionColorIterator():TActionBaseIterator(),
					InitMode(EN_ACIIM_Unknown), Color(NULL), Component(EN_CC_Alpha),
					NewVal(0.0f), OldVal(0.0f), Speed(0.0f),Time(0.0f),
					Dir(true), Distance(0.0f), DistanceCount(0.0f)
{
}
///----------------------------------------------------------------------------------------------//
TActionColorIterator::TActionColorIterator(TActionColorIterator &ObjAction):TActionBaseIterator(),
					InitMode(EN_ACIIM_Unknown), Color(NULL), Component(EN_CC_Alpha),
					NewVal(0.0f), OldVal(0.0f), Speed(0.0f),Time(0.0f),
					Dir(true), Distance(0.0f), DistanceCount(0.0f)
{
	*this = ObjAction;
}
///----------------------------------------------------------------------------------------------//
TActionColorIterator::~TActionColorIterator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::ResetParams()
{
	InitMode = EN_ACIIM_Unknown;

	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	Speed = 0.0f;
	Time  = 0.0f;

	Dir           = true;
	Distance      = 0.0f;
	DistanceCount = 0.0f;
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::PrepareColorVal()
{
	OldVal = Color->Get(Component);

	if(NewVal > 1.0f || NewVal < 0.0f){
		if(NewVal > 1.0f){ NewVal = 1.0f; }else{ NewVal = 0.0f; }
	}

	Distance = Abs(NewVal - OldVal);

	if(Distance > Eps7){
		if(NewVal > OldVal){ Dir = true; }
		else               { Dir = false; }
	}
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::PrepareDist()
{
	if(Distance >1.0f || Distance<0.0f){
		if(Distance > 1.0f){ Distance = 1.0f; }
		if(Distance < 0.0f){ Distance = 0.0f; }
	}

	OldVal = Color->Get(Component);

	if(Distance < Eps7){
		NewVal = OldVal;
		return;
	}

	if(Dir){
		if( OldVal + Distance > 1.0f ){ Distance = 1.0f - OldVal; }
		NewVal = OldVal + Distance;
	}
	else{
		if( OldVal - Distance < 0.0f ){ Distance = 1.0f - OldVal; }
		NewVal = OldVal - Distance;
	}
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::PrepareSpeed()
{
	if(Time <=0.0f){ Speed = Distance; }
	else           { Speed = Distance/Time; }
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::Init()
{
	if(Color == NULL || InitMode == EN_ACIIM_Unknown){return;}

	switch(InitMode){
		case EN_ACIIM_ValAndSpeed:{
			PrepareColorVal();
		}break;

		case EN_ACIIM_VAlAndTime:{
			PrepareColorVal();
			PrepareSpeed();
		}break;

		case EN_ACIIM_DistAndSpeed:{
			PrepareDist();
		}break;

		case EN_ACIIM_DistAndTime:{
			PrepareDist();
			PrepareSpeed();
		}break;

		default:break;
	}
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::ToRun(const TEvent &Event)
{
	if(Color == NULL){ return; }

	// при приходе пустого события присваивается итоговое значение компоненты и
	// екшен останавливается
	if(Event == false){
		Color->Set(Component, NewVal);
		Stop();
		return;
	}

	// работа итератора по таймеру
	if(Event.Type != EN_TIMER){ return; }

	// задержка выполнения действия
	if(IsDelay(Event)){return;}

	float NewDist(Event.Timer.Ms*Speed);

	DistanceCount += NewDist;

	// остановка и выход, если весь путь пройден
	if(DistanceCount < Distance){
		if(Dir){ Color->Inc(Component, NewDist); }
		else   { Color->Dec(Component, NewDist); }
	}
	else{
		Color->Set(Component, NewVal);
		Stop();
	}

}
///-----------------------------------------------------------------------------------------------//
void TActionColorIterator::ToStart()
{
	DistanceCount   = 0.0f;
	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::ToStop()
{
	if(FinishOnStop && Color != NULL){ Color->Set(Component, NewVal); }
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::operator= (TActionColorIterator &ObjAction)
{
	Color     = ObjAction.Get();
	Component = ObjAction.GetComponent();

	InitMode = ObjAction.GetInitMode();

	NewVal = ObjAction.GetNewVal();
	OldVal = ObjAction.GetOldVal();

	Speed = ObjAction.GetSpeed();
	Time  = ObjAction.GetTime();

	Dir           = ObjAction.GetDir();
	Distance      = ObjAction.GetDistance();
	DistanceCount = ObjAction.GetDistanceCount();

	SetDefParams(&ObjAction);
}
///----------------------------------------------------------------------------------------------//
TBaseAction* TActionColorIterator::Clone()
{
	return new TActionColorIterator(*this);
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::Set(const pTColor &Val)
{
	Color     = Val;
	Component = EN_CC_Alpha;
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::Set(const pTColor &Val, const enColorComponent ComponentVal)
{
	Color     = Val;
	Component = ComponentVal;
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::Set(const pTColor &Val, const int ComponentId)
{
	Color     = Val;

	switch(ComponentId){
		case  0:{ Component = EN_CC_Red  ; }break;
		case  1:{ Component = EN_CC_Green; }break;
		case  2:{ Component = EN_CC_Blue ; }break;
		case  3:{ Component = EN_CC_Alpha; }break;
		default:{
			TExceptionGenerator Ex(L"TActionColorIterator: ");
			Ex(L"in Set(Val,ComponentId) ComponentId out of range.");
		}break;
	}
}
///----------------------------------------------------------------------------------------------//
pTColor TActionColorIterator::Get()
{
	return Color;
}
///----------------------------------------------------------------------------------------------//
enColorComponent TActionColorIterator::GetComponent() const
{
	return Component;
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::UseValAndSpeed (const float &Val, const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_ACIIM_ValAndSpeed;

	NewVal = Val;
	Speed  = SpeedVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::UseValAndTime  (const float &Val, const float &TimeVal)
{
	ResetParams();

	InitMode = EN_ACIIM_VAlAndTime;

	NewVal = Val;
	Time   = TimeVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::UseDistAndSpeed(const float &DistVal, const float &SpeedVal,
										   const bool &DirVal)
{
	ResetParams();

	InitMode = EN_ACIIM_DistAndSpeed;

	Distance = DistVal;
	Speed    = SpeedVal;
	Dir      = DirVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionColorIterator::UseDistAndTime (const float &DistVal, const float &TimeVal ,
										   const bool &DirVal)
{
	ResetParams();

	InitMode = EN_ACIIM_DistAndTime;

	Distance = DistVal;
	Time     = TimeVal;
	Dir      = DirVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
enACIInitMode TActionColorIterator::GetInitMode() const
{
	return InitMode;
}
///----------------------------------------------------------------------------------------------//
float TActionColorIterator::GetNewVal() const
{
	return NewVal;
}
///----------------------------------------------------------------------------------------------//
float TActionColorIterator::GetOldVal() const
{
	return OldVal;
}
///----------------------------------------------------------------------------------------------//
float TActionColorIterator::GetSpeed() const
{
	return Speed;
}
///----------------------------------------------------------------------------------------------//
float TActionColorIterator::GetTime () const
{
	return Time;
}
///----------------------------------------------------------------------------------------------//
bool TActionColorIterator::GetDir() const
{
	return Dir;
}
///----------------------------------------------------------------------------------------------//
float TActionColorIterator::GetDistance() const
{
	return Distance;
}
///----------------------------------------------------------------------------------------------//
float TActionColorIterator::GetDistanceCount() const
{
	return DistanceCount;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
