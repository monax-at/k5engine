///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///		   Maxim Kolesnikov [mpkmaximus@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONCHANGEVIEW_H_INCLUDED
#define ACTIONCHANGEVIEW_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"

#include <map>

using std::map;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
// выполнается сразу по приходу любого события, для переключения на реагирование от пустого
// события используется флаг UseEmptyEvent
// екшен имеет два режима работы на переключение флага видимости, переключаемые Mode
// первый (true) - инвертирование флага видимости объекта, тогда значение заданных
// флагов игнорируются, второй по умолчинию (false) - установка заданного флага.
// Задаётся или View, или флаг, установленный в Add(pTBaseGraphicObject &Val, bool &ViewVal);

class TActionChangeView:public TBaseAction
{
	protected:
		bool View;
		bool Mode;
		map<pTBaseGraphicObject, bool*> Objects;

		float Time;
		float Timer;
	protected:
		void ChangeView();
		void ToRun(const TEvent &Event);
		void ToStart();
	public:
		TActionChangeView();
		TActionChangeView(TActionChangeView &ObjAction);
		virtual ~TActionChangeView();

		void operator =(TActionChangeView &ObjAction);

		TBaseAction* Clone();

		void Add(const pTBaseGraphicObject &Val);
		void Add(const pTBaseGraphicObject &Val, const bool &ViewVal);
		void Clear();

		void SetTime(const float &Val);
		float GetTime() const;

		float GetTimer() const;

		// установка флага видимости, по умолчанию он false
		void SetFlag(const bool &Val);
		bool GetFlag() const;

		// установка режима работы екшена, по умолчанию false
		void SetMode(const bool &Val);
		bool GetMode() const;

		const map <pTBaseGraphicObject, bool* > *GetObjectsPtr();
};

typedef TActionChangeView* pTActionChangeView;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // ACTIONCHANGEVIEW_H_INCLUDED
