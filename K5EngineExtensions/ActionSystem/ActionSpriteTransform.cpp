#include "ActionSpriteTransform.h"
#include "ActionChangeView.h"
#include "ActionColorIterator.h"
#include "ActionPointArrayIterator.h"
#include "ActionPointIterator.h"
#include "ActionAngleIterator.h"
#include "ActionSpriteTextureAnimation.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionSpriteTransform::TActionSpriteTransform():TBaseAction(),Sprite(NULL), Time(0.0f),
		ChangeView(false), ChangePos(false), ChangeCenter(false), ChangeAngle(false),
		ChangeTexture(false), ChangeSize(false), ChangeMesh(false), ChangeColor(false)
{
}
///----------------------------------------------------------------------------------------------//
TActionSpriteTransform::~TActionSpriteTransform()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::ToRun(const TEvent &Event)
{
	Actions.Run(Event);

	if(Actions.IsAllNotActive()){Stop();}
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::ToStart()
{
	TExceptionGenerator Ex(L"TActionSpriteTransform: ");
	Ex( Sprite != NULL, L"in ToStart() Sprite is NULL" );

	Actions.StopIsActive();
	Actions.Clear();

	if(ChangeView){
		pTActionChangeView Changer(new TActionChangeView);
		Changer->Add(Sprite, Template.GetView());
		Actions.Add(Changer);
	}

	if(ChangePos){
		pTActionPointIterator Changer(new TActionPointIterator);
		Changer->Set(&Sprite->Pos);
		Changer->UsePosAndTime(Template.Pos, Time);
		Actions.Add(Changer);
	}

	if(ChangeCenter){
		pTActionPointIterator Changer(new TActionPointIterator);
		Changer->Set(&Sprite->Center);
		Changer->UsePosAndTime(Template.Center, Time);
		Actions.Add(Changer);
	}

	if(ChangeAngle){
		pTActionAngleIterator Changer(new TActionAngleIterator);
		Changer->Set(&Sprite->Angle);
		Changer->UseAngleAndTime(Template.Angle, Time);
		Actions.Add(Changer);
	}

	if(ChangeTexture){
		pTActionSpriteTextureAnimation Changer(new TActionSpriteTextureAnimation);
		Changer->Set(Sprite);
		Changer->AddRect(&Template.Texture);
		Changer->AddFrameId(0);
		Changer->SetFrameTime(Time);
		Actions.Add(Changer);
	}

	if(ChangeSize){
		pTActionPointIterator Changer(new TActionPointIterator);
		Changer->Set(&Sprite->Size);
		Changer->UsePosAndTime(Template.Size, Time);
		Actions.Add(Changer);
	}

	if(ChangeMesh){
		for(int i=0; i<4; i++){
			TPoint PointVal(Template.Mesh.Get(i));

			pTActionPointArrayIterator Changer(new TActionPointArrayIterator);
			Changer->Set(&Sprite->Mesh,i);
			Changer->UsePosAndTime(PointVal.GetX(), PointVal.GetY(), Time);
			Actions.Add(Changer);
		}
	}

	if(ChangeColor){
		for(int i=0; i<4; i++){
			pTActionColorIterator Changer(new TActionColorIterator);
			Changer->Set(&Sprite->Color, i);
			Changer->UseValAndTime(Template.Color.Get(i), Time);
			Actions.Add(Changer);
		}
	}

	Actions.Start();
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::ToStop()
{
	Actions.StopIsActive();
	Actions.Clear();
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::Set(const pTSprite &Val)
{
	Sprite = Val;
}
///----------------------------------------------------------------------------------------------//
pTSprite TActionSpriteTransform::Get()
{
	return Sprite;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetTemplate(const TSprite &Val)
{
	Template = Val;
}
///----------------------------------------------------------------------------------------------//
TSprite TActionSpriteTransform::GetTemplate() const
{
	return Template;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetTime(const float &Val)
{
	Time = Val;
}
///----------------------------------------------------------------------------------------------//
float TActionSpriteTransform::GetTime() const
{
	return Time;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetChangeAll(const bool &Val)
{
	ChangeView    = Val;

	ChangePos     = Val;
	ChangeCenter  = Val;
	ChangeAngle   = Val;

	ChangeTexture = Val;
	ChangeSize    = Val;
	ChangeMesh    = Val;
	ChangeColor   = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTransform::GetChangeAll() const
{
	return  ChangeView && ChangePos && ChangeCenter && ChangeAngle &&
			ChangeTexture && ChangeSize && ChangeMesh && ChangeColor;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetChangeView(const bool &Val)
{
	ChangeView = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetChangePos   (const bool &Val)
{
	ChangePos = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetChangeCenter(const bool &Val)
{
	ChangeCenter = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetChangeAngle (const bool &Val)
{
	ChangeAngle = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetChangeTexture(const bool &Val)
{
	ChangeTexture = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetChangeSize(const bool &Val)
{
	ChangeSize = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetChangeMesh(const bool &Val)
{
	ChangeMesh = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTransform::SetChangeColor(const bool &Val)
{
	ChangeColor = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTransform::GetChangeView() const
{
	return ChangeView;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTransform::GetChangePos() const
{
	return ChangePos;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTransform::GetChangeCenter() const
{
	return ChangeCenter;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTransform::GetChangeAngle() const
{
	return ChangeAngle;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTransform::GetChangeTexture() const
{
	return ChangeTexture;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTransform::GetChangeSize() const
{
	return ChangeSize;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTransform::GetChangeMesh() const
{
	return ChangeMesh;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTransform::GetChangeColor() const
{
	return ChangeColor;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

