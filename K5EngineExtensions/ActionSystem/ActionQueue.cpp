#include "ActionQueue.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionQueue::TActionQueue():TBaseAction(),Cursor(0),Cyclic(false)
{
}
///----------------------------------------------------------------------------------------------//
TActionQueue::TActionQueue(TActionQueue &ObjAction):TBaseAction(),Cursor(0),Cyclic(false)
{
}
///----------------------------------------------------------------------------------------------//
TActionQueue::~TActionQueue()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionQueue::operator =(TActionQueue &ObjAction)
{
}
///----------------------------------------------------------------------------------------------//
void TActionQueue::ToRun(const TEvent &Event)
{
	Actions.Run(Event);

	if(!Actions.IsOneActive(Cursor)){
		Cursor++;
		int Size(Actions.GetSize());

		if(Cursor < Size){
			Actions.StartOne(Cursor);
		}
		else{
			if(!Cyclic){
				Stop();
			}
			else{
				Cursor = 0;
				Actions.StartOne(Cursor);
			}
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TActionQueue::ToStart()
{
	int Size(Actions.GetSize());
	if(Size == 0 || Cursor < 0 || Cursor >= Size){
		Stop();
		return;
	}

	Actions.StartOne(Cursor);
}
///----------------------------------------------------------------------------------------------//
void TActionQueue::ToStop()
{
	Cursor = 0;
	Actions.StopIsActive();
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TActionQueue::Add(const pTBaseAction &Val)
{
	return Actions.Add(Val);
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TActionQueue::Get(const int &ObjIndex)
{
	return Actions.Get(ObjIndex);
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TActionQueue::Get(const unsigned long &ObjID)
{
	return Actions.Get(ObjID);
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TActionQueue::Get(const wstring &ObjName)
{
	return Actions.Get(ObjName);
}
///----------------------------------------------------------------------------------------------//
void TActionQueue::Del(const int &ObjIndex)
{
	Actions.Del(ObjIndex);
}
///----------------------------------------------------------------------------------------------//
void TActionQueue::Del(const unsigned long &ObjID)
{
	Actions.Del(ObjID);
}
///----------------------------------------------------------------------------------------------//
void TActionQueue::Del(const wstring &ObjName)
{
	Actions.Del(ObjName);
}
///----------------------------------------------------------------------------------------------//
int TActionQueue::GetSize() const
{
	return Actions.GetSize();
}
///----------------------------------------------------------------------------------------------//
void TActionQueue::Clear()
{
	Actions.Clear();
}
///----------------------------------------------------------------------------------------------//
pTActionList TActionQueue::GetActionsPtr()
{
	return &Actions;
}
///----------------------------------------------------------------------------------------------//
void TActionQueue::SetCyclic(const bool &Val)
{
	Cyclic = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionQueue::GetCyclic() const
{
	return Cyclic;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
