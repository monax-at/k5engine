#include "ActionPointIterator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionPointIterator::TActionPointIterator():
			TActionBaseIterator(), InitMode(EN_APIIM_Unknown), Point(NULL),
			Speed(0.0f), Time(0.0f), Distance(0.0f), DistanceCount(0.0f), UseZ(true)
{
}
///----------------------------------------------------------------------------------------------//
TActionPointIterator::TActionPointIterator(TActionPointIterator &ObjAction):
			TActionBaseIterator(), InitMode(EN_APIIM_Unknown), Point(NULL),
			Speed(0.0f), Time(0.0f), Distance(0.0f), DistanceCount(0.0f), UseZ(true)
{
	*this = ObjAction;
}
///----------------------------------------------------------------------------------------------//
TActionPointIterator::~TActionPointIterator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::ResetParams()
{
	InitMode = EN_APIIM_Unknown;

	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	Speed = 0.0f;
	Time  = 0.0f;

	Dir.Set(0.0f,0.0f,0.0f);
	Distance = 0.0f;
	DistanceCount = 0.0f;
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::InitPosAndSpeedParams()
{
	OldPos = *Point;

	Dir.Set(NewPos.GetX() - Point->GetX(), NewPos.GetY() - Point->GetY(),
			NewPos.GetZ() - Point->GetZ());

	if(!UseZ){ Dir.z = 0.0f; }
	Distance = Dir.GetLength();
	Dir.Normalize();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::InitDistAndSpeedParams()
{
	OldPos = *Point;

	if(!UseZ){DistDir.z = 0.0f;}
	Distance = DistDir.GetLength();

	NewPos = *Point;
	NewPos.Inc(DistDir.x, DistDir.y, DistDir.z);

	Dir = DistDir;
	Dir.Normalize();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::CountSpeed()
{
	if(Time > 0.0f){ Speed = (Distance/Time)*1000.0f; }
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::CountTime()
{
	if(Speed > 0.0f){ Time = (Distance*1000.0f)/Speed; }
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::Init()
{
	if(Point == NULL || InitMode == EN_APIIM_Unknown){return;}

	switch(InitMode){
		case EN_APIIM_PosAndSpeed:{
			InitPosAndSpeedParams();
			CountTime();
		}break;

		case EN_APIIM_PosAndTime:{
			InitPosAndSpeedParams();
			CountSpeed();
		}break;

		case EN_APIIM_DistAndSpeed:{
			InitDistAndSpeedParams();
			CountTime();
		}break;

		case EN_APIIM_DistAndTime:{
			InitDistAndSpeedParams();
			CountSpeed();
		}break;

		default:break;
	}
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::ToRun(const TEvent &Event)
{
	if(Point == NULL){return;}

	// по приходу пустого события просто присваивается итоговая позиция
	if( Event == false){

		if(UseZ){ *Point = NewPos; } else { Point->Set(NewPos.GetX(), NewPos.GetY()); }

		Stop();
		return;
	}

	// обработка стандартной работы екшена
	if(Event.Type != EN_TIMER){ return; }

	// задержка выполнения действия
	if(IsDelay(Event)){return;}

	float DeltaDist(Distance);
	if(Time > 0.0f){ DeltaDist = Event.Timer.Sec*Speed; }

	DistanceCount += DeltaDist;

	// остановка и выход, если весь путь пройден
	if(DistanceCount < Distance){
		Point->Inc(DeltaDist*Dir.x, DeltaDist*Dir.y, DeltaDist*Dir.z);
	}
	else{
		if(UseZ){ *Point = NewPos; } else { Point->Set(NewPos.GetX(), NewPos.GetY()); }
		Stop();
	}
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::ToStart()
{
	DistanceCount   = 0.0f;
	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::ToStop()
{
	if(FinishOnStop && Point != NULL ){
		if(UseZ){ *Point = NewPos; } else { Point->Set(NewPos.GetX(),NewPos.GetY()); }
	}
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::operator =(TActionPointIterator &ObjAction)
{
	Point = ObjAction.Get();

	InitMode = ObjAction.GetInitMode();

	NewPos = ObjAction.GetNewPos();
	OldPos = ObjAction.GetOldPos();

	Speed  = ObjAction.GetSpeed();
	Time   = ObjAction.GetTime ();

	DistDir       = ObjAction.GetDistDir();
	Dir           = ObjAction.GetDir();
	Distance      = ObjAction.GetDistance();
	DistanceCount = ObjAction.GetDistanceCount();
	UseZ          = ObjAction.GetUseZ();

	SetDefParams(&ObjAction);
}
///----------------------------------------------------------------------------------------------//
TBaseAction* TActionPointIterator::Clone()
{
	return new TActionPointIterator(*this);
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::Set(const pTPoint &Val)
{
	Point = Val;
}
///----------------------------------------------------------------------------------------------//
pTPoint TActionPointIterator::Get()
{
	return Point;
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::SetUseZ(const bool &Val)
{
	UseZ = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionPointIterator::GetUseZ() const
{
	return UseZ;
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UsePosAndSpeed(	const float &X,const float &Y,
												const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndSpeed;
	Speed    = SpeedVal;
	UseZ     = false;
	NewPos.Set(X,Y);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UsePosAndSpeed(	const float &X,const float &Y, const float &Z,
												const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndSpeed;
	Speed    = SpeedVal;
	UseZ     = true;
	NewPos.Set(X,Y,Z);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UsePosAndSpeed(	const TPoint &NewPosVal, const float &SpeedVal,
											const bool &UseZVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndSpeed;
	UseZ     = UseZVal;
	Speed    = SpeedVal;
	NewPos   = NewPosVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UsePosAndTime (	const float &X, const float &Y, const float &TimeVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndTime;
	Time     = TimeVal;
	UseZ     = false;
	NewPos.Set(X,Y);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UsePosAndTime (	const float &X, const float &Y, const float &Z,
										    const float &TimeVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndTime;
	Time     = TimeVal;
	UseZ     = false;
	NewPos.Set(X,Y,Z);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UsePosAndTime (	const TPoint &NewPosVal, const float &TimeVal,
										    const bool &UseZVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndTime;
	Time     = TimeVal;
	UseZ     = false;
	NewPos   = NewPosVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UseDistAndSpeed(const float &X, const float &Y, const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_APIIM_DistAndSpeed;
	Speed    = SpeedVal;
	UseZ     = false;
	DistDir.Set(X,Y);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UseDistAndSpeed(const float &X, const float &Y, const float &Z,
							 				 const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_APIIM_DistAndSpeed;
	Speed    = SpeedVal;
	UseZ     = true;
	DistDir.Set(X,Y,Z);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UseDistAndSpeed(const TPoint &DistVal, const float &SpeedVal)
{
	UseDistAndSpeed(DistVal.GetX(), DistVal.GetY(), DistVal.GetZ(), SpeedVal);
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UseDistAndTime(	const float &X,const float &Y, const float &TimeVal)
{
	ResetParams();

	InitMode = EN_APIIM_DistAndTime;
	Time     = TimeVal;
	UseZ     = false;
	DistDir.Set(X,Y);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UseDistAndTime(const float &X,const float &Y,const float &Z,
											const float &TimeVal)
{
	ResetParams();

	InitMode = EN_APIIM_DistAndTime;
	Time     = TimeVal;
	UseZ     = true;
	DistDir.Set(X,Y,Z);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointIterator::UseDistAndTime(const TPoint &DistVal, const float &TimeVal)
{
	UseDistAndTime(DistVal.GetX(), DistVal.GetY(), DistVal.GetZ(), TimeVal);
}
///----------------------------------------------------------------------------------------------//
enAPIInitMode TActionPointIterator::GetInitMode() const
{
	return InitMode;
}
///----------------------------------------------------------------------------------------------//
TPoint TActionPointIterator::GetNewPos() const
{
	return NewPos;
}
///----------------------------------------------------------------------------------------------//
TPoint TActionPointIterator::GetOldPos() const
{
	return OldPos;
}
///----------------------------------------------------------------------------------------------//
float TActionPointIterator::GetSpeed() const
{
	return Speed;
}
///----------------------------------------------------------------------------------------------//
float TActionPointIterator::GetTime() const
{
	return Time;
}
///----------------------------------------------------------------------------------------------//
TVector3D TActionPointIterator::GetDistDir() const
{
	return DistDir;
}
///----------------------------------------------------------------------------------------------//
TVector3D TActionPointIterator::GetDir() const
{
	return Dir;
}
///----------------------------------------------------------------------------------------------//
float TActionPointIterator::GetDistance() const
{
	return Distance;
}
///----------------------------------------------------------------------------------------------//
float TActionPointIterator::GetDistanceCount() const
{
	return DistanceCount;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

