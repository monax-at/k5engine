#include "ActionColorArrayIterator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionColorArrayIterator::TActionColorArrayIterator():TActionBaseIterator(),
					InitMode(EN_ACIIM_Unknown), Array(NULL), Component(EN_CC_Alpha), ColorId(0),
					NewVal(0.0f), OldVal(0.0f), Speed(0.0f),Time(0.0f),
					Dir(true), Distance(0.0f), DistanceCount(0.0f)
{
}
///----------------------------------------------------------------------------------------------//
TActionColorArrayIterator::TActionColorArrayIterator(TActionColorArrayIterator &ObjAction):
					TActionBaseIterator(),
					InitMode(EN_ACIIM_Unknown), Array(NULL), Component(EN_CC_Alpha), ColorId(0),
					NewVal(0.0f), OldVal(0.0f), Speed(0.0f),Time(0.0f),
					Dir(true), Distance(0.0f), DistanceCount(0.0f)
{
	*this = ObjAction;
}
///----------------------------------------------------------------------------------------------//
TActionColorArrayIterator::~TActionColorArrayIterator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::ResetParams()
{
	InitMode = EN_ACIIM_Unknown;

	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	Speed = 0.0f;
	Time  = 0.0f;

	Dir           = true;
	Distance      = 0.0f;
	DistanceCount = 0.0f;
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::PrepareColorVal()
{
	OldVal = Array->Get(ColorId, Component);

	if(NewVal > 1.0f || NewVal < 0.0f){
		if(NewVal > 1.0f){ NewVal = 1.0f; }else{ NewVal = 0.0f; }
	}

	Distance = Abs(NewVal - OldVal);

	if(Distance > Eps7){
		if(NewVal > OldVal){ Dir = true; }
		else               { Dir = false; }
	}
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::PrepareDist()
{
	if(Distance >1.0f || Distance<0.0f){
		if(Distance > 1.0f){ Distance = 1.0f; }
		if(Distance < 0.0f){ Distance = 0.0f; }
	}

	OldVal = Array->Get(ColorId, Component);

	if(Distance < Eps7){
		NewVal = OldVal;
		return;
	}

	if(Dir){
		if( OldVal + Distance > 1.0f ){ Distance = 1.0f - OldVal; }
		NewVal = OldVal + Distance;
	}
	else{
		if( OldVal - Distance < 0.0f ){ Distance = 1.0f - OldVal; }
		NewVal = OldVal - Distance;
	}
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::PrepareSpeed()
{
	if(Time <=0.0f){ Speed = Distance; }
	else           { Speed = Distance/Time; }
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::Init()
{
	if(Array == NULL || InitMode == EN_ACIIM_Unknown){return;}

	switch(InitMode){
		case EN_ACIIM_ValAndSpeed:{
			PrepareColorVal();
		}break;

		case EN_ACIIM_VAlAndTime:{
			PrepareColorVal();
			PrepareSpeed();
		}break;

		case EN_ACIIM_DistAndSpeed:{
			PrepareDist();
		}break;

		case EN_ACIIM_DistAndTime:{
			PrepareDist();
			PrepareSpeed();
		}break;

		default:break;
	}
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::ToRun(const TEvent &Event)
{
	if(Array == NULL){ return; }

	// при приходе пустого события присваивается итоговое значение компоненты и
	// екшен останавливается
	if(Event == false){
		Array->Set(ColorId,Component, NewVal);
		Stop();
		return;
	}

	// работа итератора по таймеру
	if(Event.Type != EN_TIMER){ return; }

	// задержка выполнения действия
	if(IsDelay(Event)){return;}

	float NewDist(Event.Timer.Ms*Speed);

	DistanceCount += NewDist;

	// остановка и выход, если весь путь пройден
	if(DistanceCount < Distance){
		if(Dir){ Array->Inc(ColorId, Component, NewDist); }
		else   { Array->Dec(ColorId, Component, NewDist); }
	}
	else{
		Array->Set(ColorId,Component, NewVal);
		Stop();
	}

}
///-----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::ToStart()
{
	DistanceCount   = 0.0f;
	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::ToStop()
{
	if(FinishOnStop && Array != NULL){ Array->Set(ColorId, Component, NewVal); }
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::operator= (TActionColorArrayIterator &ObjAction)
{
	Array     = ObjAction.GetArray();
	ColorId   = ObjAction.GetColorId();
	Component = ObjAction.GetComponent();

	InitMode = ObjAction.GetInitMode();

	NewVal = ObjAction.GetNewVal();
	OldVal = ObjAction.GetOldVal();

	Speed = ObjAction.GetSpeed();
	Time  = ObjAction.GetTime();

	Dir           = ObjAction.GetDir();
	Distance      = ObjAction.GetDistance();
	DistanceCount = ObjAction.GetDistanceCount();

	SetDefParams(&ObjAction);
}
///----------------------------------------------------------------------------------------------//
TBaseAction* TActionColorArrayIterator::Clone()
{
	return new TActionColorArrayIterator(*this);
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::Set(const pTColorArray &Val)
{
	Array     = Val;
	ColorId   = 0;
	Component = EN_CC_Alpha;
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::Set(const pTColorArray &Val, const int &ColorIdVal)
{
	Array     = Val;
	ColorId   = ColorIdVal;
	Component = EN_CC_Alpha;
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::Set(const pTColorArray &Val, const int &ColorIdVal,
									const enColorComponent ComponentVal)
{
	Array     = Val;
	ColorId   = ColorIdVal;
	Component = ComponentVal;
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::Set(const pTColorArray &Val,const int &ColorIdVal,
									const int ComponentId)
{
	Array   = Val;
	ColorId = ColorIdVal;

	switch(ComponentId){
		case  0:{ Component = EN_CC_Red  ; }break;
		case  1:{ Component = EN_CC_Green; }break;
		case  2:{ Component = EN_CC_Blue ; }break;
		case  3:{ Component = EN_CC_Alpha; }break;
		default:{
			TExceptionGenerator Ex(L"TActionColorArrayIterator: ");
			Ex(L"in Set(Val,ComponentId) ComponentId out of range.");
		}break;
	}
}
///----------------------------------------------------------------------------------------------//
pTColorArray TActionColorArrayIterator::GetArray()
{
	return Array;
}
///----------------------------------------------------------------------------------------------//
int TActionColorArrayIterator::GetColorId  () const
{
	return ColorId;
}
///----------------------------------------------------------------------------------------------//
enColorComponent TActionColorArrayIterator::GetComponent() const
{
	return Component;
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::UseValAndSpeed (const float &Val, const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_ACIIM_ValAndSpeed;

	NewVal = Val;
	Speed  = SpeedVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::UseValAndTime  (const float &Val, const float &TimeVal)
{
	ResetParams();

	InitMode = EN_ACIIM_VAlAndTime;

	NewVal = Val;
	Time   = TimeVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::UseDistAndSpeed(const float &DistVal, const float &SpeedVal,
										   const bool &DirVal)
{
	ResetParams();

	InitMode = EN_ACIIM_DistAndSpeed;

	Distance = DistVal;
	Speed    = SpeedVal;
	Dir      = DirVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionColorArrayIterator::UseDistAndTime (const float &DistVal, const float &TimeVal ,
										   const bool &DirVal)
{
	ResetParams();

	InitMode = EN_ACIIM_DistAndTime;

	Distance = DistVal;
	Time     = TimeVal;
	Dir      = DirVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
enACIInitMode TActionColorArrayIterator::GetInitMode() const
{
	return InitMode;
}
///----------------------------------------------------------------------------------------------//
float TActionColorArrayIterator::GetNewVal() const
{
	return NewVal;
}
///----------------------------------------------------------------------------------------------//
float TActionColorArrayIterator::GetOldVal() const
{
	return OldVal;
}
///----------------------------------------------------------------------------------------------//
float TActionColorArrayIterator::GetSpeed() const
{
	return Speed;
}
///----------------------------------------------------------------------------------------------//
float TActionColorArrayIterator::GetTime () const
{
	return Time;
}
///----------------------------------------------------------------------------------------------//
bool TActionColorArrayIterator::GetDir() const
{
	return Dir;
}
///----------------------------------------------------------------------------------------------//
float TActionColorArrayIterator::GetDistance() const
{
	return Distance;
}
///----------------------------------------------------------------------------------------------//
float TActionColorArrayIterator::GetDistanceCount() const
{
	return DistanceCount;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
