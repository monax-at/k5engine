///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONCOLORCOMPONENTITERATOR_H_INCLUDED
#define ACTIONCOLORCOMPONENTITERATOR_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "ActionBaseIterator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TActionColorIterator:public TActionBaseIterator
{
	protected:
		enACIInitMode InitMode;

		pTColor          Color;
		enColorComponent Component;

		float NewVal;
		float OldVal;

		float Speed;
		float Time;

		bool  Dir;
		float Distance;
		float DistanceCount;
	protected:
		inline void ResetParams();
		inline void PrepareColorVal();
		inline void PrepareDist();
		inline void PrepareSpeed();
		inline void Init();

		void ToRun(const TEvent &Event);
		void ToStart();
		void ToStop();
	public:
		TActionColorIterator();
		TActionColorIterator(TActionColorIterator &ObjAction);
		virtual ~TActionColorIterator();

		void operator= (TActionColorIterator &ObjAction);

		TBaseAction* Clone();

		void Set(const pTColor &Val);
		void Set(const pTColor &Val, const enColorComponent ComponentVal);
		void Set(const pTColor &Val, const int ComponentId);

		pTColor          Get();
		enColorComponent GetComponent() const;

		// тут направление итерации выбирается в зависимости от значения
		// текущей компоненты цвета и нового значения, скорость
		// задаётся только положительными значениями
		void UseValAndSpeed (const float &Val, const float &SpeedVal);
		void UseValAndTime  (const float &Val, const float &TimeVal);

		// направление задаётся флагом DirVal, если true - увеличение, false - уменьшение
		void UseDistAndSpeed(const float &DistVal, const float &SpeedVal, const bool &DirVal);
		void UseDistAndTime (const float &DistVal, const float &TimeVal , const bool &DirVal);

		enACIInitMode GetInitMode() const;

		float GetNewVal() const;
		float GetOldVal() const;

		float GetSpeed() const;
		float GetTime () const;

		bool  GetDir() const;
		float GetDistance() const;
		float GetDistanceCount() const;
};

typedef TActionColorIterator* pTActionColorIterator;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // ACTIONCOLORCOMPONENTITERATOR_H_INCLUDED
