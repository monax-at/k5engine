///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONQUEUE_INCLUDED
#define ACTIONQUEUE_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
// контейнер последовательного выполнения екшенов - очередь
// производить копирование контейнейра нельзя
class TActionQueue:public TBaseAction
{
	protected:
		TActionList Actions;
		int         Cursor;
		bool        Cyclic;
	protected:
		void ToRun(const TEvent &Event);
		void ToStart();
		void ToStop ();
	private:
		TActionQueue(TActionQueue &ObjAction);
		void operator =(TActionQueue &ObjAction);
	public:
		TActionQueue();
		virtual ~TActionQueue();

		pTBaseAction Add(const pTBaseAction &Val);

		pTBaseAction Get(const int           &ObjIndex);
		pTBaseAction Get(const unsigned long &ObjID);
		pTBaseAction Get(const wstring       &ObjName);

		void Del(const int           &ObjIndex);
		void Del(const unsigned long &ObjID);
		void Del(const wstring       &ObjName);

		int GetSize() const;

		void Clear();

		pTActionList GetActionsPtr();

		void SetCyclic(const bool &Val);
		bool GetCyclic() const;
};

typedef TActionQueue* pTActionQueue;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // ACTIONQUEUE_INCLUDED
