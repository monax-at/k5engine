#include "ActionContainer.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionContainer::TActionContainer():TBaseAction()
{
}
///----------------------------------------------------------------------------------------------//
TActionContainer::TActionContainer(TActionContainer &ObjAction):TBaseAction()
{
}
///----------------------------------------------------------------------------------------------//
TActionContainer::~TActionContainer()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionContainer::operator =(TActionContainer &ObjAction)
{
}
///----------------------------------------------------------------------------------------------//
void TActionContainer::ToRun(const TEvent &Event)
{
	Actions.Run(Event);

	if(Actions.IsAllNotActive()){ Stop(); }
}
///----------------------------------------------------------------------------------------------//
void TActionContainer::ToStart()
{
	Actions.StartIsNotActive();
}
///----------------------------------------------------------------------------------------------//
void TActionContainer::ToStop()
{
	Actions.StopIsActive();
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TActionContainer::Add(const pTBaseAction &Val)
{
	return Actions.Add(Val);
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TActionContainer::Get(const int &ObjIndex)
{
	return Actions.Get(ObjIndex);
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TActionContainer::Get(const unsigned long &ObjID)
{
	return Actions.Get(ObjID);
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TActionContainer::Get(const wstring &ObjName)
{
	return Actions.Get(ObjName);
}
///----------------------------------------------------------------------------------------------//
void TActionContainer::Del(const int &ObjIndex)
{
	Actions.Del(ObjIndex);
}
///----------------------------------------------------------------------------------------------//
void TActionContainer::Del(const unsigned long &ObjID)
{
	Actions.Del(ObjID);
}
///----------------------------------------------------------------------------------------------//
void TActionContainer::Del(const wstring &ObjName)
{
	Actions.Del(ObjName);
}
///----------------------------------------------------------------------------------------------//
int TActionContainer::GetSize() const
{
	return Actions.GetSize();
}
///----------------------------------------------------------------------------------------------//
void TActionContainer::Clear()
{
	Actions.Clear();
}
///----------------------------------------------------------------------------------------------//
pTActionList TActionContainer::GetActionsPtr()
{
	return &Actions;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
