///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef ACTIONANGLEITERATOR_H_INCLUDED
#define ACTIONANGLEITERATOR_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "ActionBaseIterator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TActionAngleIterator:public TActionBaseIterator
{
	protected:
		enAAIInitMode InitMode;

		pTValue Angle;

		float OldAngle;
		float NewAngle;

		// направление вращения, может быть +1 или -1, выставляется автоматически
		// в зависимости от параметров
		int   Dir;
		float Speed;
		float Time;

		// по умолчанию true, означает, что поворот производить по минимальному расстоянию
		bool UseMinAngle;

		float DistanceTmp;
		float Distance;
		float DistanceCount;
	protected:
		inline void CountDistAngle();
		inline void CountDist();
		inline void CountSpeed();
		inline void CountTime();

		void ResetParams();
		void Init();

		void ToRun(const TEvent &Event);
		void ToStart();
		void ToStop();
	public:
		TActionAngleIterator();
		TActionAngleIterator(TActionAngleIterator &ObjAction);
		virtual ~TActionAngleIterator();

		void operator =(TActionAngleIterator &ObjAction);

		TBaseAction* Clone();

		void Set(const pTValue &Val);
		pTValue Get();

		// улог, который должен в итоге получиться, от 0 до 360, MinAngl - указывает, что надо
		// поворачивать по минимальному углу при true и по максимальному при false
		void UseAngleAndTime (const float  &NewAngleVal , const float &TimeVal , bool MinAngl=true);
		void UseAngleAndTime (const TValue &NewAngleVal , const float &TimeVal , bool MinAngl=true);

		void UseAngleAndSpeed(const float  &NewAngleVal , const float &SpeedVal, bool MinAngl=true);
		void UseAngleAndSpeed(const TValue &NewAngleVal , const float &SpeedVal, bool MinAngl=true);

		// угол, на который надо повернуться, от - до +
		void UseDistAndTime  (const float &AngleDistVal, const float &TimeVal );
		void UseDistAndSpeed (const float &AngleDistVal, const float &SpeedVal);

		// постоянное вращение, тут указывается скорость вращения
		// ( отрицательное или положительное значение)
		void UseCyclicBySpeed(const float &SpeedVal);

		// постоянное вращение, за сколько проходить указанную дистанцию
		void UseCyclicByTime (const float &AngleDistVal, const float &TimeVal);

		bool IsCyclicBySpeed () const;
		bool IsCyclicByTime  () const;

		// получение различных параметров класса
		enAAIInitMode GetInitMode() const;

		float GetOldAngle() const;
		float GetNewAngle() const;

		int   GetDir  () const;
		float GetSpeed() const;
		float GetTime () const;

		bool  GetUseMinAngle    () const;

		float GetDistanceTmp    () const;
		float GetDistance       () const;
		float GetDistanceCount  () const;
};

typedef TActionAngleIterator* pTActionAngleIterator;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // EVENTACTIONNANGLEITERATOR_H_INCLUDED
