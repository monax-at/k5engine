#include "ActionChangeView.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionChangeView::TActionChangeView(): TBaseAction(),  View(false),Mode(false),
														Time(0.0f), Timer(0.0f)

{
}
///----------------------------------------------------------------------------------------------//
TActionChangeView::TActionChangeView(TActionChangeView &ObjAction):TBaseAction(),
														View(false),Mode(false),
														Time(0.0f), Timer(0.0f)
{
	*this = ObjAction;
}
///----------------------------------------------------------------------------------------------//
TActionChangeView::~TActionChangeView()
{
	Clear();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionChangeView::ChangeView()
{
	for (map <pTBaseGraphicObject, bool* >::iterator Itr = Objects.begin() ;
	     Itr != Objects.end(); Itr++ )
	{
		pTBaseGraphicObject Object = Itr->first;

		if(Mode){
			Object->SetView(!Object->GetView());
		}
		else{
			if(Itr->second == NULL){ Object->SetView(View); }
			else                   { Object->SetView(*(Itr->second)); }
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TActionChangeView::ToRun(const TEvent &Event)
{
	if(Event == false){
		ChangeView();
		Stop();
		return;
	}

	if(Event.Type != EN_TIMER){ return; }

	Timer += Event.Timer.Ms;
	if(Timer >= Time){
		ChangeView();
		Stop();
	}
}
///----------------------------------------------------------------------------------------------//
void TActionChangeView::ToStart()
{
	Timer = 0.0f;
}
///----------------------------------------------------------------------------------------------//
void TActionChangeView::operator =(TActionChangeView &ObjAction)
{
	Active = ObjAction.GetActive();
	Name   = ObjAction.GetName ();

	View   = ObjAction.GetFlag();
	Mode   = ObjAction.GetMode();

	const map <pTBaseGraphicObject, bool* >* ObjectsPtr(ObjAction.GetObjectsPtr());
	map <pTBaseGraphicObject, bool* >::const_iterator Itr;

	for ( Itr = ObjectsPtr->begin() ; Itr != ObjectsPtr->end(); Itr++ ){
		pTBaseGraphicObject ObjectPtr = Itr->first;
		bool* ViewPtr                 = Itr->second;

		if(ViewPtr == NULL){ Add(ObjectPtr); }
		else               { Add(ObjectPtr,*ViewPtr); }
	}
}
///----------------------------------------------------------------------------------------------//
TBaseAction* TActionChangeView::Clone()
{
	return new TActionChangeView(*this);
}
///----------------------------------------------------------------------------------------------//
void TActionChangeView::Add(const pTBaseGraphicObject &Val)
{
	Objects[Val] = NULL;
}
///----------------------------------------------------------------------------------------------//
void TActionChangeView::Add(const pTBaseGraphicObject &Val, const bool &ViewVal)
{
	Objects[Val] = new bool(ViewVal);
}
///----------------------------------------------------------------------------------------------//
void TActionChangeView::Clear()
{
	map <pTBaseGraphicObject, bool * >::iterator it;
	for ( it = Objects.begin() ; it != Objects.end(); it++ ){ delete it->second; }

	Objects.clear();
}
///----------------------------------------------------------------------------------------------//
void TActionChangeView::SetTime(const float &Val)
{
	Time = Val;
}
///----------------------------------------------------------------------------------------------//
float TActionChangeView::GetTime() const
{
	return Time;
}
///----------------------------------------------------------------------------------------------//
float TActionChangeView::GetTimer() const
{
	return Timer;
}
///----------------------------------------------------------------------------------------------//
void TActionChangeView::SetFlag(const bool &Val)
{
	View = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionChangeView::GetFlag() const
{
	return View;
}
///----------------------------------------------------------------------------------------------//
void TActionChangeView::SetMode(const bool &Val)
{
	Mode = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionChangeView::GetMode() const
{
	return Mode;
}
///----------------------------------------------------------------------------------------------//
const map <pTBaseGraphicObject, bool* > *TActionChangeView::GetObjectsPtr()
{
	return &Objects;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
