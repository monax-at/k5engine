#include "ActionSpriteTextureAnimation.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionSpriteTextureAnimation::TActionSpriteTextureAnimation():TBaseAction(),
						Exception(L"TActionSpriteTextureAnimation: "), Sprite(NULL),
						AnimCursor(0), Cyclic(false),
						FrameTime(25.0f), FrameTimer(0.0f), FinishOnStop(false)
{
}
///----------------------------------------------------------------------------------------------//
TActionSpriteTextureAnimation::TActionSpriteTextureAnimation(TActionSpriteTextureAnimation &ObjAction):
		 TBaseAction(), Exception(L"TActionSpriteTextureAnimation: "), Sprite(NULL),
						AnimCursor(0), Cyclic(false),
						FrameTime(25.0f), FrameTimer(0.0f), FinishOnStop(false)
{
	*this = ObjAction;
}
///----------------------------------------------------------------------------------------------//
TActionSpriteTextureAnimation::~TActionSpriteTextureAnimation()
{
	Clear();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::AddAnimRect(const pTTexture &Texture,
												const float &X, const float &Y,
												const float &Width, const float &Height,
												const bool &ChangeSpriteSize)
{
	TAnimRect* ARect(new TAnimRect);

	ARect->Texture 			= Texture;
	ARect->X 				= X;
	ARect->Y 				= Y;
	ARect->Width  			= Width;
	ARect->Height 			= Height;
	ARect->ChangeSpriteSize = ChangeSpriteSize;

	AnimRects.push_back(ARect);
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::UpdateSpriteTexture()
{
	if(Sprite==NULL){return;}

	TAnimRect* ARect = AnimRects[Frames[AnimCursor]];

	pTTexture Texture(ARect->Texture);
	if(Texture != NULL){

		Sprite->Texture(Texture);
		Sprite->Texture.SetRect(0.0f, 0.0f, Texture->GetWidth(), Texture->GetHeight());

		if(ARect->ChangeSpriteSize){ Sprite->Size(Texture->GetWidth(), Texture->GetHeight()); }
	}

	if(ARect->X !=0.0f || ARect->Y!=0.0f || ARect->Width != 0.0f || ARect->Height != 0.0f){
		Sprite->Texture.SetRect(ARect->X, ARect->Y, ARect->Width, ARect->Height);

		if(ARect->ChangeSpriteSize){ Sprite->Size(ARect->Width, ARect->Height); }
	}
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::SwitchShot()
{
	int SizeFrameId(Frames.size());
	if(SizeFrameId == 0){return;}

	if(AnimCursor < SizeFrameId){
		UpdateSpriteTexture();
		AnimCursor++;
	}
	else{
		if(!Cyclic)	{ Stop(); }
		else        { AnimCursor = 0; }
	}
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::ToRun(const TEvent &Event)
{
	if(Event == false){
		SwitchShot();
		return;
	}

	if(Event.Type != EN_TIMER){return;}

	FrameTimer += Event.Timer.Ms;

	if(FrameTimer<FrameTime){ return; }

	FrameTimer -= FrameTime;
	if(FrameTimer>=FrameTime){ FrameTime = 0.0f; }

	SwitchShot();
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::ToStart()
{
	AnimCursor = 0;
	FrameTimer = 0.0f;

	Exception(Frames   .size() > 0,L"in ToStart() Frames must be set");
	Exception(AnimRects.size() > 0,L"in ToStart() AnimRects must be set");

	// установка первого кадра анимации при старте
	SwitchShot();
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::ToStop()
{
	if(FinishOnStop){ SetLastFrame(); }
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::operator =(TActionSpriteTextureAnimation &ObjAction)
{
	Active = ObjAction.GetActive();
	Name   = ObjAction.GetName ();

	Sprite       = ObjAction.Get();
	AnimCursor   = ObjAction.GetFrame();
	Cyclic       = ObjAction.GetCyclic();
	FrameTime    = ObjAction.GetFrameTime();
	FrameTimer   = ObjAction.GetFrameTimer();
	FinishOnStop = ObjAction.GetFinishOnStop();

	Clear();

	int RectsSize(ObjAction.GetRectsSize());
	for(int i=0; i<RectsSize; i++){
		AddRect( ObjAction.GetRectTexture(i), ObjAction.GetRectX(i), ObjAction.GetRectY(i),
				 ObjAction.GetRectWidth (i), ObjAction.GetRectHeight(i),
				 ObjAction.GetRectChangeSpriteSizeFlag(i) );
	}

	int FramesSize(ObjAction.GetFramesSize());
	for(int i=0; i<FramesSize; i++){
		AddFrameId(ObjAction.GetFrameId(i));
	}
}
///----------------------------------------------------------------------------------------------//
TBaseAction* TActionSpriteTextureAnimation::Clone()
{
	return new TActionSpriteTextureAnimation(*this);
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::Set(const pTSprite &Val)
{
	Sprite = Val;
}
///----------------------------------------------------------------------------------------------//
pTSprite TActionSpriteTextureAnimation::Get()
{
	return Sprite;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::SetFrameTime(const float &Val)
{
	FrameTime = Val;
}
///----------------------------------------------------------------------------------------------//
float TActionSpriteTextureAnimation::GetFrameTime() const
{
	return FrameTime;
}
///----------------------------------------------------------------------------------------------//
float TActionSpriteTextureAnimation::GetFrameTimer() const
{
	return FrameTimer;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::SetFinishOnStop(const bool &Val)
{
	FinishOnStop = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTextureAnimation::GetFinishOnStop() const
{
	return FinishOnStop;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::AddRect(const pTTexture &Texture ,
											const float &X,const float &Y,
											const float &Width,const float &Height,
											const bool &ChangeSpriteSize)
{
	AddAnimRect(Texture, X, Y, Width, Height, ChangeSpriteSize);
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::AddRect(const float &X,const float &Y,
									const float &Width,const float &Height,
									const bool &ChangeSpriteSize)
{
	AddAnimRect(NULL, X, Y, Width, Height, ChangeSpriteSize);
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::AddRect(	const pTTexture &Texture,
												const bool &ChangeSpriteSize)
{
	AddAnimRect(Texture, 0.0f, 0.0f, Texture->GetWidth(), Texture->GetHeight(),
				ChangeSpriteSize);
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::AddRect(const pTSpriteTexture &Texture,
											const bool &ChangeSpriteSize)
{
	AddAnimRect(Texture->Get(), Texture->GetRectX(), Texture->GetRectY(),
				Texture->GetRectWidth(), Texture->GetRectHeight(),
				ChangeSpriteSize);
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::Clear()
{
	int AnimRectsSize(AnimRects.size());
	for(int i=0;i<AnimRectsSize;i++){ delete AnimRects[i]; }

	AnimRects.clear();
	Frames   .clear();
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::ClearFrames()
{
	Frames   .clear();
}
///----------------------------------------------------------------------------------------------//
int TActionSpriteTextureAnimation::GetRectsSize() const
{
	return AnimRects.size();
}
///----------------------------------------------------------------------------------------------//
pTTexture TActionSpriteTextureAnimation::GetRectTexture(const int &RectID)
{
	return AnimRects[RectID]->Texture;
}
///----------------------------------------------------------------------------------------------//
float TActionSpriteTextureAnimation::GetRectX(const int &RectID) const
{
	return AnimRects[RectID]->X;
}
///----------------------------------------------------------------------------------------------//
float TActionSpriteTextureAnimation::GetRectY(const int &RectID) const
{
	return AnimRects[RectID]->Y;
}
///----------------------------------------------------------------------------------------------//
float TActionSpriteTextureAnimation::GetRectWidth(const int &RectID) const
{
	return AnimRects[RectID]->Width;
}
///----------------------------------------------------------------------------------------------//
float TActionSpriteTextureAnimation::GetRectHeight(const int &RectID) const
{
	return AnimRects[RectID]->Height;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTextureAnimation::GetRectChangeSpriteSizeFlag(const int &RectID) const
{
	return AnimRects[RectID]->ChangeSpriteSize;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::AddFrameId(const int &Id)
{
	Frames.push_back(Id);
}
///----------------------------------------------------------------------------------------------//
int TActionSpriteTextureAnimation::GetFramesSize() const
{
	return Frames.size();
}
///----------------------------------------------------------------------------------------------//
int TActionSpriteTextureAnimation::GetFrameId(const int &FrameID) const
{
	return Frames[FrameID];
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::SetCyclic(const bool &Val)
{
	Cyclic = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionSpriteTextureAnimation::GetCyclic() const
{
	return Cyclic;
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::SetFrame(const int &Val)
{
	AnimCursor = Val;
	int FramesSize = Frames.size();

	Exception(AnimCursor<FramesSize,L"in SetFrame() AnimCursor out of Frames");

	UpdateSpriteTexture();
}
///----------------------------------------------------------------------------------------------//
void TActionSpriteTextureAnimation::SetLastFrame()
{
	AnimCursor = Frames.size()-1;
	UpdateSpriteTexture();
}
///----------------------------------------------------------------------------------------------//
int TActionSpriteTextureAnimation::GetFrame() const
{
	return AnimCursor;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

