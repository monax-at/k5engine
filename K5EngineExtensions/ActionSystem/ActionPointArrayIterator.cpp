#include "ActionPointArrayIterator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TActionPointArrayIterator::TActionPointArrayIterator():
			TActionBaseIterator(), InitMode(EN_APIIM_Unknown), Array(NULL), PointId(0),
			Speed(0.0f), Time(0.0f), Distance(0.0f), DistanceCount(0.0f), UseZ(true)
{
}
///----------------------------------------------------------------------------------------------//
TActionPointArrayIterator::TActionPointArrayIterator(TActionPointArrayIterator &ObjAction):
			TActionBaseIterator(), InitMode(EN_APIIM_Unknown), Array(NULL), PointId(0),
			Speed(0.0f), Time(0.0f), Distance(0.0f), DistanceCount(0.0f), UseZ(true)
{
	*this = ObjAction;
}
///----------------------------------------------------------------------------------------------//
TActionPointArrayIterator::~TActionPointArrayIterator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::ResetParams()
{
	InitMode = EN_APIIM_Unknown;

	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	Speed = 0.0f;
	Time  = 0.0f;

	Dir.Set(0.0f,0.0f,0.0f);
	Distance = 0.0f;
	DistanceCount = 0.0f;
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::InitPosAndSpeedParams()
{
	TPoint Point(Array->Get(PointId));
	OldPos = Point;

	Dir.Set(NewPos.GetX() - Point.GetX(), NewPos.GetY() - Point.GetY(),
			NewPos.GetZ() - Point.GetZ());

	if(!UseZ){ Dir.z = 0.0f; }
	Distance = Dir.GetLength();
	Dir.Normalize();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::InitDistAndSpeedParams()
{
	TPoint Point(Array->Get(PointId));
	OldPos = Point;

	if(!UseZ){DistDir.z = 0.0f;}
	Distance = DistDir.GetLength();

	NewPos = Point;
	NewPos.Inc(DistDir.x, DistDir.y, DistDir.z);

	Dir = DistDir;
	Dir.Normalize();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::CountSpeed()
{
	if(Time > 0.0f){ Speed = (Distance/Time)*1000.0f; }
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::CountTime()
{
	if(Speed > 0.0f){ Time = (Distance*1000.0f)/Speed; }
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::Init()
{
	if(Array == NULL || InitMode == EN_APIIM_Unknown){return;}

	switch(InitMode){
		case EN_APIIM_PosAndSpeed:{
			InitPosAndSpeedParams();
			CountTime();
		}break;

		case EN_APIIM_PosAndTime:{
			InitPosAndSpeedParams();
			CountSpeed();
		}break;

		case EN_APIIM_DistAndSpeed:{
			InitDistAndSpeedParams();
			CountTime();
		}break;

		case EN_APIIM_DistAndTime:{
			InitDistAndSpeedParams();
			CountSpeed();
		}break;

		default:break;
	}
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::ToRun(const TEvent &Event)
{
	if(Array == NULL){return;}

	// по приходу пустого события просто присваивается итоговая позиция
	if( Event == false){

		if(UseZ){ Array->Set(PointId, NewPos); }
		else    { Array->Set(PointId, NewPos.GetX(), NewPos.GetY()); }

		Stop();
		return;
	}

	// обработка стандартной работы екшена
	if(Event.Type != EN_TIMER){ return; }

	// задержка выполнения действия
	if(IsDelay(Event)){return;}

	float DeltaDist(Distance);
	if(Time > 0.0f){ DeltaDist = Event.Timer.Sec*Speed; }

	DistanceCount += DeltaDist;

	// остановка и выход, если весь путь пройден
	if(DistanceCount < Distance){
		Array->Inc(PointId, DeltaDist*Dir.x, DeltaDist*Dir.y, DeltaDist*Dir.z);
	}
	else{
		if(UseZ){ Array->Set(PointId, NewPos); }
		else    { Array->Set(PointId, NewPos.GetX(), NewPos.GetY()); }

		Stop();
	}
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::ToStart()
{
	DistanceCount   = 0.0f;
	DelayTimer      = 0.0f;
	DelayTimerReady = false;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::ToStop()
{
	if(FinishOnStop && Array != NULL ){
		if(UseZ){ Array->Set(PointId, NewPos); }
		else    { Array->Set(PointId, NewPos.GetX(), NewPos.GetY()); }
	}
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::operator =(TActionPointArrayIterator &ObjAction)
{
	Array   = ObjAction.GetArray();
	PointId = ObjAction.GetPointId();

	InitMode = ObjAction.GetInitMode();

	NewPos = ObjAction.GetNewPos();
	OldPos = ObjAction.GetOldPos();

	Speed  = ObjAction.GetSpeed();
	Time   = ObjAction.GetTime ();

	DistDir       = ObjAction.GetDistDir();
	Dir           = ObjAction.GetDir();
	Distance      = ObjAction.GetDistance();
	DistanceCount = ObjAction.GetDistanceCount();
	UseZ          = ObjAction.GetUseZ();

	SetDefParams(&ObjAction);
}
///----------------------------------------------------------------------------------------------//
TBaseAction* TActionPointArrayIterator::Clone()
{
	return new TActionPointArrayIterator(*this);
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::Set(const pTPointArray &Val,  const int &PointIdVal)
{
	Array   = Val;
	PointId = PointIdVal;
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::SetArray  (const pTPointArray &Val)
{
	Array   = Val;
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::SetPointId(const int &Val)
{
	PointId = Val;
}
///----------------------------------------------------------------------------------------------//
pTPointArray TActionPointArrayIterator::GetArray()
{
	return Array;
}
///----------------------------------------------------------------------------------------------//
int TActionPointArrayIterator::GetPointId() const
{
	return PointId;
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::SetUseZ(const bool &Val)
{
	UseZ = Val;
}
///----------------------------------------------------------------------------------------------//
bool TActionPointArrayIterator::GetUseZ() const
{
	return UseZ;
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UsePosAndSpeed(	const float &X,const float &Y,
												const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndSpeed;
	Speed    = SpeedVal;
	UseZ     = false;
	NewPos.Set(X,Y);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UsePosAndSpeed(	const float &X,const float &Y, const float &Z,
												const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndSpeed;
	Speed    = SpeedVal;
	UseZ     = true;
	NewPos.Set(X,Y,Z);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UsePosAndSpeed(	const TPoint &NewPosVal, const float &SpeedVal,
											const bool &UseZVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndSpeed;
	UseZ     = UseZVal;
	Speed    = SpeedVal;
	NewPos   = NewPosVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UsePosAndTime (	const float &X, const float &Y, const float &TimeVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndTime;
	Time     = TimeVal;
	UseZ     = false;
	NewPos.Set(X,Y);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UsePosAndTime (	const float &X, const float &Y, const float &Z,
										    const float &TimeVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndTime;
	Time     = TimeVal;
	UseZ     = false;
	NewPos.Set(X,Y,Z);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UsePosAndTime (	const TPoint &NewPosVal, const float &TimeVal,
										    const bool &UseZVal)
{
	ResetParams();

	InitMode = EN_APIIM_PosAndTime;
	Time     = TimeVal;
	UseZ     = false;
	NewPos   = NewPosVal;

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UseDistAndSpeed(const float &X, const float &Y, const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_APIIM_DistAndSpeed;
	Speed    = SpeedVal;
	UseZ     = false;
	DistDir.Set(X,Y);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UseDistAndSpeed(const float &X, const float &Y, const float &Z,
							 				 const float &SpeedVal)
{
	ResetParams();

	InitMode = EN_APIIM_DistAndSpeed;
	Speed    = SpeedVal;
	UseZ     = true;
	DistDir.Set(X,Y,Z);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UseDistAndSpeed(const TPoint &DistVal, const float &SpeedVal)
{
	UseDistAndSpeed(DistVal.GetX(), DistVal.GetY(), DistVal.GetZ(), SpeedVal);
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UseDistAndTime(	const float &X,const float &Y, const float &TimeVal)
{
	ResetParams();

	InitMode = EN_APIIM_DistAndTime;
	Time     = TimeVal;
	UseZ     = false;
	DistDir.Set(X,Y);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UseDistAndTime(const float &X,const float &Y,const float &Z,
											const float &TimeVal)
{
	ResetParams();

	InitMode = EN_APIIM_DistAndTime;
	Time     = TimeVal;
	UseZ     = true;
	DistDir.Set(X,Y,Z);

	Init();
}
///----------------------------------------------------------------------------------------------//
void TActionPointArrayIterator::UseDistAndTime(const TPoint &DistVal, const float &TimeVal)
{
	UseDistAndTime(DistVal.GetX(), DistVal.GetY(), DistVal.GetZ(), TimeVal);
}
///----------------------------------------------------------------------------------------------//
enAPIInitMode TActionPointArrayIterator::GetInitMode() const
{
	return InitMode;
}
///----------------------------------------------------------------------------------------------//
TPoint TActionPointArrayIterator::GetNewPos() const
{
	return NewPos;
}
///----------------------------------------------------------------------------------------------//
TPoint TActionPointArrayIterator::GetOldPos() const
{
	return OldPos;
}
///----------------------------------------------------------------------------------------------//
float TActionPointArrayIterator::GetSpeed() const
{
	return Speed;
}
///----------------------------------------------------------------------------------------------//
float TActionPointArrayIterator::GetTime() const
{
	return Time;
}
///----------------------------------------------------------------------------------------------//
TVector3D TActionPointArrayIterator::GetDistDir() const
{
	return DistDir;
}
///----------------------------------------------------------------------------------------------//
TVector3D TActionPointArrayIterator::GetDir() const
{
	return Dir;
}
///----------------------------------------------------------------------------------------------//
float TActionPointArrayIterator::GetDistance() const
{
	return Distance;
}
///----------------------------------------------------------------------------------------------//
float TActionPointArrayIterator::GetDistanceCount() const
{
	return DistanceCount;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

