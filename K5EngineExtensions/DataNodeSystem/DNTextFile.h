///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNTEXTFILE_H_INCLUDED
#define DNTEXTFILE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "Base/DataNode.h"

#include <fstream>
#include <sstream>

using std::stringstream;
using std::ifstream;
using std::ofstream;
using std::endl;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNTextFile
{
	protected:
		TExceptionGenerator Exception;

		bool EncryptFlag;
	protected:
		inline string EncryptString(const string &Val);
		inline string DecryptString(const string &Val);

		inline int 			GetNumTabs   (const string &Str);
		inline pTDataNode 	CreateStrNode(const string &Str);
		inline void 		InitNode     (const pTDataNode &Node);

		inline void WriteNode(	ofstream &File, const int &TabShiftPos,
								const pTDataNode &Node);
	public:
		TDNTextFile();
		virtual ~TDNTextFile();

		void Load(const wstring &FileName, const pTDataNode &Node);
		void Save(const wstring &FileName, const pTDataNode &Node);

		void UseEncrypt(const bool &Val = true);
		bool IsUseEncrypt() const;
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNTEXTFILE_H_INCLUDED
