///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNSPRITE_H_INCLUDED
#define DNSPRITE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "DNBaseGraphicObject.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNSprite:public TDNBaseGraphicObject
{
	protected:
		pTBaseDevice         Device;
		pTTextureListManager Textures;
	protected:
		void SetSpriteMeshPoint(const pTPointArray &Mesh, const int &PointId,
								const pTDataNode &Node, const wstring &PointNodeName);

		void AddMeshPointRecord(const pTDataNode &Node, const TPoint &Point,
						const float &XCheck, const float &YCheck, const wstring &PointNodeName);
	public:
		TDNSprite();
		TDNSprite(const pTBaseDevice &DeviceVal, const pTTextureListManager &TexturesVal);
		virtual ~TDNSprite();

		// инициализация спрайта через ноду
		void Init(const pTSprite &Object, const pTDataNode &Node);

		// инициалищация ноды чеерз спрайт
		void Init(const pTDataNode &Node, const pTSprite &Object);

		pTSprite   Run(const pTDataNode &Node);
		pTDataNode Run(const pTSprite &Object);

		void Set        (const pTBaseDevice &DeviceVal, const pTTextureListManager &TexturesVal);
		void SetTextures(const pTTextureListManager &Val);
		void SetDevice  (const pTBaseDevice &Val);
};

typedef TDNSprite* pTDNSprite;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNSPRITE_H_INCLUDED
