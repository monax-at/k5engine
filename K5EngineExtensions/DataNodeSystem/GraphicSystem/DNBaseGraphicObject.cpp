#include "DNBaseGraphicObject.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNBaseGraphicObject::TDNBaseGraphicObject()
{
}
///----------------------------------------------------------------------------------------------//
TDNBaseGraphicObject::~TDNBaseGraphicObject()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDNBaseGraphicObject::InitBaseParams(	const pTBaseGraphicObject &Object,
											const pTDataNode &Node)
{
	TDNEngineDataConverter DC;

	// базовые параметры спрайтбанка
	if(Node->IsExist(L"name")){ Object->SetName(Node->Get(L"name")->GetString()); }
	if(Node->IsExist(L"view")){ Object->SetView(Node->Get(L"view")->GetBool()); }

	// фильтр или сглаживание текстуры
	if(Node->IsExist(L"min_filter")){
		Object->SetMinFilter( DC.ToTextureFilter(Node->Get(L"min_filter")) );
	}

	if(Node->IsExist(L"mag_filter")){
		Object->SetMagFilter(DC.ToTextureFilter(Node->Get(L"mag_filter")));
	}

	if(Node->IsExist(L"filter")){
		Object->SetFilter(DC.ToTextureFilter(Node->Get(L"filter")));
	}
}
///----------------------------------------------------------------------------------------------//
void TDNBaseGraphicObject::InitBaseParams(	const pTDataNode &Node,
											const pTBaseGraphicObject &Object)
{
	TDNEngineDataConverter DC;

	// базовые параметры спрайта
	if(Object->GetName().length()>0){ Node->Add(Object->GetName(), L"name"); }
	if(Object->GetView() == false)	{ Node->Add(Object->GetView(), L"view"); }

	if(Object->GetMinFilter() != EN_TF_Linear){
		Node->Add( DC.From(Object->GetMinFilter(), L"min_filter") );
	}

	if(Object->GetMagFilter() != EN_TF_Linear){
		Node->Add( DC.From(Object->GetMagFilter(), L"mag_filter") );
	}
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
