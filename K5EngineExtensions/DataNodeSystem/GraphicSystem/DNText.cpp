#include "DNText.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNText::TDNText():TDNBaseGraphicObject(),Fonts(NULL),Font(NULL),Device(NULL)
{
}
///----------------------------------------------------------------------------------------------//
TDNText::~TDNText()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTDataNode TDNText::CreateTextLineNode(const wstring &Text)
{
	pTDataNode LineNode(new TDataNode);

	LineNode->SetName(L"Line");
	LineNode->SetVal(Text);
	LineNode->SetType(EN_DNT_Other);

	return LineNode;
}
///----------------------------------------------------------------------------------------------//
void TDNText::Init(const pTText &Object, const pTDataNode &Node)
{
	InitBaseParams(Object,Node);

	TDNEngineDataConverter DC;

	// установка заданного шрифта или взятие его из списка
	if(Fonts!=NULL){
		if(Node->IsExist(L"font")){
			Object->SetFont(Fonts->Get( Node->Get(L"font")->GetString() ));
		}
	}

	if(Font!=NULL){ Object->SetFont(Font); }

	// угол
	if(Node->IsExist(L"angle")){
		Object->Angle(Node->GetChildFloat(L"angle"));
	}

	// выравнивание текста
	if(Node->IsExist(L"alignment")){
		Object->SetAlignment(DC.ToAlignment(Node->Get(L"alignment")));
	}

	// позиция текста
	if(Node->IsExist(L"Pos")){ Object->Pos(DC.ToPoint(Node->Get(L"Pos"),Device)); }

	// центр
	if(Node->IsExist(L"Center")){ Object->Center(DC.ToPoint(Node->Get(L"Center"))); }

	// цвет
	if(Node->IsExist(L"Color")){ Object->Color(DC.ToColor(Node->Get(L"Color"))); }

	// масщтаб
	if(Node->IsExist(L"Scale")){
		pTDataNode ScaleNode(Node->Get(L"Scale"));

		float X(1.0f);
		float Y(1.0f);

		if(ScaleNode->IsExist(L"x")){X = ScaleNode->GetChildFloat(L"x");}
		if(ScaleNode->IsExist(L"y")){Y = ScaleNode->GetChildFloat(L"y");}

		Object->Scale(X,Y);
	}

	// текст
	if(Node->IsExist(L"Text")){
		Object->Set(Node->GetChildString(L"Text"));
	}
}
///----------------------------------------------------------------------------------------------//
void TDNText::Init(const pTDataNode &Node, const pTText &Object)
{
	InitBaseParams(Node,Object);

	TDNEngineDataConverter DC;

	// угол наклона
	float ObjectAngle(Object->Angle.Get());
	if(ObjectAngle != 0.0f){ Node->Add(ObjectAngle,L"angle"); }

	// позиция
	if(Object->Pos[0]!=0.0f || Object->Pos[1]!=0.0f || Object->Pos[2]!=0.0f){
		Node->Add(DC.From(Object->Pos, L"Pos"));
	}

	// центр
	if(Object->Center[0]!=0.0f || Object->Center[1]!=0.0f || Object->Center[2]!=0.0f){
		Node->Add(DC.From(Object->Center, L"Center"));
	}

	// масштаб
	TPoint ObjectScale(Object->Scale);
	if(ObjectScale[0] != 1.0f || ObjectScale[1] != 1.0f){
		pTDataNode ScaleNode(Node->Add(new TDataNode));
		ScaleNode->SetName(L"Scale");
		if(ObjectScale[0] != 1.0f){ ScaleNode->Add(ObjectScale[0], L"x"); }
		if(ObjectScale[1] != 1.0f){ ScaleNode->Add(ObjectScale[1], L"y"); }
	}

	// цвет
	TColor Color = Object->Color;
	if(	Color.Get(0) != 1.0f || Color.Get(1) != 1.0f ||
		Color.Get(2) != 1.0f || Color.Get(3) != 1.0f)
	{
		Node->Add(DC.From(Color,L"Color"));
	}

	// текст
	wstring ObjectText(Object->Get());
	if(ObjectText.length()>0){
		pTDataNode TextNode(Node->Add(new TDataNode));
		TextNode->SetName(L"Text");
		TextNode->Add(ObjectText);
	}
}
///----------------------------------------------------------------------------------------------//
pTText TDNText::Run(const pTDataNode &Node)
{
	pTText Object(new TText);

	Init(Object,Node);

	return Object;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNText::Run(const pTText &Object)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(L"Text");

	Init(Node,Object);

	return Node;
}
///----------------------------------------------------------------------------------------------//
void TDNText::SetFonts(const pTFontList &Val)
{
	if(Val == NULL){return;}

	Font = NULL;
	Fonts = Val;
}
///----------------------------------------------------------------------------------------------//
void TDNText::SetFont(const pTBaseFont &Val)
{
	if(Val == NULL){return;}

	Fonts = NULL;
	Font = Val;
}
///----------------------------------------------------------------------------------------------//
void TDNText::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
