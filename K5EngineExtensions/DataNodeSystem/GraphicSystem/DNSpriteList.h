///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNSPRITELIST_H_INCLUDED
#define DNSPRITELIST_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "DNSprite.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNSpriteList
{
	protected:
		pTBaseDevice         Device;
		pTTextureListManager Textures;
	public:
		TDNSpriteList();
		TDNSpriteList(const pTBaseDevice &DeviceVal, const pTTextureListManager &TexturesVal);
		virtual ~TDNSpriteList();

		void Init(const pTSpriteList &Object, const pTDataNode &Node);
		void Init(const pTDataNode &Node, const pTSpriteList &Object);

		void Run(const pTDataNode &Node, const pTSpriteListManager &Objects);

		pTSpriteList Run(const pTDataNode &Node);
		pTDataNode   Run(const pTSpriteList &Object);

		void Set        (const pTBaseDevice &DeviceVal,const pTTextureListManager &TexturesVal);
		void SetDevice  (const pTBaseDevice &Val);
		void SetTextures(const pTTextureListManager &Val);
};

typedef TDNSpriteList* pTDNSpriteList;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNSPRITELIST_H_INCLUDED
