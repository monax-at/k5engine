///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNTEXT_H_INCLUDED
#define DNTEXT_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "DNBaseGraphicObject.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNText: public TDNBaseGraphicObject
{
	protected:
		pTFontList 		Fonts;
		pTBaseFont 		Font;
		pTBaseDevice 	Device;
	protected:
		inline pTDataNode CreateTextLineNode(const wstring &Text);
	public:
		TDNText();
		virtual ~TDNText();

		// инициализация текстового объекта через ноду
		void Init(const pTText &Object, const pTDataNode &Node);

		// инициалищация ноды чеерз текстовый объект
		void Init(const pTDataNode &Node, const pTText &Object);

		pTText Run(const pTDataNode &Node);
		pTDataNode Run(const pTText &Object);

		void SetFonts(const pTFontList &Val);
		void SetFont(const pTBaseFont &Val);
		void SetDevice(const pTBaseDevice &Val);
};

typedef TDNText* pTDNText;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNTEXT_H_INCLUDED
