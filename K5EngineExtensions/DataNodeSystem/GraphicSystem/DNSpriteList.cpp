#include "DNSpriteList.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNSpriteList::TDNSpriteList()	:Device(NULL),Textures(NULL)
{
}
///----------------------------------------------------------------------------------------------//
TDNSpriteList::TDNSpriteList(const pTBaseDevice &DeviceVal,const pTTextureListManager &TexturesVal)
								:Device(DeviceVal), Textures(TexturesVal)
{
}
///----------------------------------------------------------------------------------------------//
TDNSpriteList::~TDNSpriteList()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDNSpriteList::Init(const pTSpriteList &Object, const pTDataNode &Node)
{
	TDNEngineDataConverter DC;

	// базовые параметры списка
	if(Node->IsExist(L"name")){ Object->SetName(Node->Get(L"name")->GetString()); }
	if(Node->IsExist(L"view")){ Object->SetView(Node->Get(L"view")->GetBool()); }

	int Size(Node->GetSize());

	TDNSprite DNSprite(Device,Textures);

	for(int i=0;i<Size;i++){
		pTDataNode ChildNode(Node->Get(i));

		if(ChildNode->GetName() == L"Sprite"){
			Object->Add(DNSprite.Run(ChildNode));
		}
	}

	Object->Sort();
}
///----------------------------------------------------------------------------------------------//
void TDNSpriteList::Init(const pTDataNode &Node, const pTSpriteList &Object)
{
	// базовые параметры спрайта
	if(Object->GetName().length()>0){ Node->Add(Object->GetName(), L"name"); }
	if(Object->GetView() == false)	{ Node->Add(Object->GetView(), L"view"); }

	if(Object->GetSize()>0){
		TDNSprite DNSprite;
		DNSprite.SetTextures(Textures);
		DNSprite.SetDevice(Device);

		Object->Sort();

		int Size(Object->GetSize());
		for(int i=0;i<Size;i++){
			Node->Add(DNSprite.Run(Object->Get(i)));
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TDNSpriteList::Run(const pTDataNode &Node,const pTSpriteListManager &Objects)
{
	pTSpriteList Object(NULL);

	if(Node->IsExist(L"name")){ Object = Objects->GetIfExist(Node->GetChildString(L"name")); }

	if(Object == NULL){Object = Objects->Add(new TSpriteList);}

	Init(Object,Node);
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TDNSpriteList::Run(const pTDataNode &Node)
{
	pTSpriteList Object(new TSpriteList);

	Init(Object,Node);

	return Object;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNSpriteList::Run(const pTSpriteList &Object)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(L"List");

	Init(Node,Object);

	return Node;
}
///----------------------------------------------------------------------------------------------//
void TDNSpriteList::Set(const pTBaseDevice &DeviceVal,const pTTextureListManager &TexturesVal)
{
	Device   = DeviceVal;
	Textures = TexturesVal;
}
///----------------------------------------------------------------------------------------------//
void TDNSpriteList::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///----------------------------------------------------------------------------------------------//
void TDNSpriteList::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
