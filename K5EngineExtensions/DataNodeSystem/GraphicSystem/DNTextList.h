///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNTEXTLIST_H_INCLUDED
#define DNTEXTLIST_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "DNText.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNTextList
{
	protected:
		pTFontList   Fonts;
		pTBaseFont   Font;
		pTBaseDevice Device;
	public:
		TDNTextList();
		TDNTextList(const pTFontList &FontsVal, const pTBaseDevice &DeviceVal);
		TDNTextList(const pTBaseFont &FontVal , const pTBaseDevice &DeviceVal);
		virtual ~TDNTextList();

		void Init(const pTTextList &Object, const pTDataNode &Node);
		void Init(const pTDataNode &Node, const pTTextList &Object);

		void Run(const pTDataNode &Node, const pTTextListManager &Objects);

		pTTextList Run(const pTDataNode &Node);
		pTDataNode Run(const pTTextList &Object);

		void SetFonts (const pTFontList &Val);
		void SetFont  (const pTBaseFont &Val);
		void SetDevice(const pTBaseDevice &Val);

		void Set(const pTFontList &FontsVal, const pTBaseDevice &DeviceVal);
		void Set(const pTBaseFont &FontVal , const pTBaseDevice &DeviceVal);
};

typedef TDNTextList* pTDNTextList;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNTEXTLIST_H_INCLUDED
