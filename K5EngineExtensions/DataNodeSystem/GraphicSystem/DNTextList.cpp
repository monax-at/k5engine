#include "DNTextList.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNTextList::TDNTextList():Fonts(NULL),Font(NULL),Device(NULL)
{
}
///----------------------------------------------------------------------------------------------//
TDNTextList::TDNTextList(const pTFontList &FontsVal, const pTBaseDevice &DeviceVal):
						Fonts(FontsVal),Font(NULL),Device(DeviceVal)
{
}
///----------------------------------------------------------------------------------------------//
TDNTextList::TDNTextList(const pTBaseFont &FontVal , const pTBaseDevice &DeviceVal):
						Fonts(NULL),Font(FontVal),Device(DeviceVal)
{
}
///----------------------------------------------------------------------------------------------//
TDNTextList::~TDNTextList()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDNTextList::Init(const pTTextList &Object, const pTDataNode &Node)
{
	TDNEngineDataConverter DC;

	// базовые параметры списка
	if(Node->IsExist(L"name")){ Object->SetName(Node->Get(L"name")->GetString()); }
	if(Node->IsExist(L"view")){ Object->SetView(Node->Get(L"view")->GetBool()); }

	int Size(Node->GetSize());

	TDNText DNText;
	if(Fonts!=NULL)	{DNText.SetFonts(Fonts);}
	if(Font!=NULL)	{DNText.SetFont(Font);}
	DNText.SetDevice(Device);

	for(int i=0;i<Size;i++){
		pTDataNode ChildNode(Node->Get(i));

		if(ChildNode->GetName() == L"Text"){
			Object->Add(DNText.Run(ChildNode));
		}
	}

	Object->Sort();
}
///----------------------------------------------------------------------------------------------//
void TDNTextList::Init(const pTDataNode &Node, const pTTextList &Object)
{
	// базовые параметры спрайта
	if(Object->GetName().length()>0){ Node->Add(Object->GetName(), L"name"); }
	if(Object->GetView() == false)	{ Node->Add(Object->GetView(), L"view"); }

	if(Object->GetSize()>0){
		TDNText DNText;
		if(Fonts!=NULL)	{DNText.SetFonts(Fonts);}
		if(Font!=NULL)	{DNText.SetFont(Font);}
		DNText.SetDevice(Device);

		Object->Sort();

		int Size(Object->GetSize());
		for(int i=0;i<Size;i++){
			Node->Add(DNText.Run(Object->Get(i)));
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TDNTextList::Run(const pTDataNode &Node, const pTTextListManager &Objects)
{
	pTTextList Object(NULL);

	if(Node->IsExist(L"name")){ Object = Objects->GetIfExist(Node->GetChildString(L"name")); }

	if(Object == NULL){Object = Objects->Add(new TTextList);}

	Init(Object, Node);
}
///----------------------------------------------------------------------------------------------//
pTTextList TDNTextList::Run(const pTDataNode &Node)
{
	pTTextList Object(new TTextList);

	Init(Object, Node);

	return Object;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNTextList::Run(const pTTextList &Object)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(L"List");

	Init(Node,Object);

	return Node;
}
///----------------------------------------------------------------------------------------------//
void TDNTextList::SetFonts(const pTFontList &Val)
{
	Font  = NULL;
	Fonts = Val;
}
///----------------------------------------------------------------------------------------------//
void TDNTextList::SetFont(const pTBaseFont &Val)
{
	Fonts = NULL;
	Font  = Val;
}
///----------------------------------------------------------------------------------------------//
void TDNTextList::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///----------------------------------------------------------------------------------------------//
void TDNTextList::Set(const pTFontList &FontsVal, const pTBaseDevice &DeviceVal)
{
	Font   = NULL;
	Fonts  = FontsVal;
	Device = DeviceVal;
}
///----------------------------------------------------------------------------------------------//
void TDNTextList::Set(const pTBaseFont &FontVal , const pTBaseDevice &DeviceVal)
{
	Fonts  = NULL;
	Font   = FontVal;
	Device = DeviceVal;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
