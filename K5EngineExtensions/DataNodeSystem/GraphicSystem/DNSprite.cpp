#include "DNSprite.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNSprite::TDNSprite():TDNBaseGraphicObject(),Device(NULL),Textures(NULL)
{
}
///----------------------------------------------------------------------------------------------//
TDNSprite::TDNSprite(const pTBaseDevice &DeviceVal,const pTTextureListManager &TexturesVal):
						TDNBaseGraphicObject(),Device(DeviceVal),Textures(TexturesVal)
{
}
///----------------------------------------------------------------------------------------------//
TDNSprite::~TDNSprite()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDNSprite::SetSpriteMeshPoint(const pTPointArray &Mesh, const int &PointId,
								   const pTDataNode &Node, const wstring &PointNodeName)
{
	if(!Node->IsExist(PointNodeName)){return;}

	pTDataNode DNPoint(Node->Get(PointNodeName));

	if(DNPoint->IsExist(L"x")){ Mesh->SetX(PointId, DNPoint->GetChildFloat(L"x")); }
	if(DNPoint->IsExist(L"y")){ Mesh->SetY(PointId, DNPoint->GetChildFloat(L"y")); }
}
///----------------------------------------------------------------------------------------------//
void TDNSprite::AddMeshPointRecord(const pTDataNode &Node, const TPoint &Point,
						const float &XCheck, const float &YCheck, const wstring &PointNodeName)
{
	if( Point.GetX() == XCheck && Point.GetY() == YCheck){ return; }

	pTDataNode DNPoint(new TDataNode);
	DNPoint->SetName(PointNodeName);

	if(Point.GetX() != XCheck){ DNPoint->Add(Point.GetX(), L"x"); }
	if(Point.GetY() != YCheck){ DNPoint->Add(Point.GetY(), L"y"); }

	if(Node->IsExist(L"Mesh")){
		Node->Get(L"Mesh")->Add(DNPoint);
	}
	else{
		pTDataNode DNMesh(Node->Add(new TDataNode));
		DNMesh->SetName(L"Mesh");
		DNMesh->Add(DNPoint);
	}
}
///----------------------------------------------------------------------------------------------//
void TDNSprite::Init(const pTSprite &Object, const pTDataNode &Node)
{
	InitBaseParams(Object,Node);

	TDNEngineDataConverter DC;

	// угол
	if(Node->IsExist(L"angle")){
		Object->Angle(Node->GetChildFloat(L"angle"));
	}

	// параметры текстуры
	if(Node->IsExist(L"Texture")){
		pTDataNode TextureNode( Node->Get(L"Texture") );

		if(Textures!=NULL){
			pTTexture Texture(Textures->Get(TextureNode->Get(L"list")->GetString(),
											TextureNode->Get(L"name")->GetString()));
			Object->Texture(Texture);
			Object->Size((float)Texture->GetWidth(), (float)Texture->GetHeight());
		}

		// Texture Mirror
		if(TextureNode->IsExist(L"Mirror")){
			pTDataNode TextureMirrorNode( TextureNode->Get(L"Mirror") );

			if(TextureMirrorNode->IsExist(L"x")){
				Object->Texture.SetMirrorX( TextureMirrorNode->Get(L"x")->GetBool() );
			}

			if(TextureMirrorNode->IsExist(L"y")){
				Object->Texture.SetMirrorY( TextureMirrorNode->Get(L"y")->GetBool() );
			}
		}

		// Texture Repeat
		if(TextureNode->IsExist(L"Repeat")){
			pTDataNode TextureRepeatNode( TextureNode->Get(L"Repeat") );

			if(TextureRepeatNode->IsExist(L"x")){
				Object->Texture.SetRepeatX( TextureRepeatNode->Get(L"x")->GetFloat() );
			}

			if(TextureRepeatNode->IsExist(L"y")){
				Object->Texture.SetRepeatY( TextureRepeatNode->Get(L"y")->GetFloat() );
			}
		}

		// Texture Rect
		if(TextureNode->IsExist(L"Rect")){
			pTDataNode TextureRectNode( TextureNode->Get(L"Rect") );

			if(TextureRectNode->IsExist(L"x")){
				Object->Texture.SetRectPosX( TextureRectNode->Get(L"x")->GetFloat() );
			}

			if(TextureRectNode->IsExist(L"y")){
				Object->Texture.SetRectPosY( TextureRectNode->Get(L"y")->GetFloat() );
			}

			if(TextureRectNode->IsExist(L"width")){
				float Width(TextureRectNode->Get(L"width")->GetFloat());
				Object->Texture.SetRectWidth( Width );
				Object->Size.SetX(Width);
			}

			if(TextureRectNode->IsExist(L"height")){
				float Height(TextureRectNode->Get(L"height")->GetFloat());
				Object->Texture.SetRectHeight( Height );
				Object->Size.SetY(Height);
			}
		}
	}

	// позиция спрайта
	if(Node->IsExist(L"Pos"))   { Object->Pos(DC.ToPoint(Node->Get(L"Pos"), Device)); }

	// центр спрайта
	if(Node->IsExist(L"Center")){ Object->Center(DC.ToPoint(Node->Get(L"Center"))); }

	// размер
	if(Node->IsExist(L"Size"))  { Object->Size(DC.ToPoint(Node->Get(L"Size"))); }

	// точки меша
	if(Node->IsExist(L"Mesh")){
		pTDataNode DNMesh(Node->Get(L"Mesh"));
		pTPointArray Mesh(&Object->Mesh);

		SetSpriteMeshPoint(Mesh, 0, DNMesh, L"Point0");
		SetSpriteMeshPoint(Mesh, 1, DNMesh, L"Point1");
		SetSpriteMeshPoint(Mesh, 2, DNMesh, L"Point2");
		SetSpriteMeshPoint(Mesh, 3, DNMesh, L"Point3");
	}

	// цвет
	if(Node->IsExist(L"Color")){ Object->Color(DC.ToColor(Node->Get(L"Color"))); }

	// цвета точек спрайта
	if(Node->IsExist(L"PointColor")){
		pTDataNode DNPointColor( Node->Get(L"PointColor") );
		int DNPointColorSize(DNPointColor->GetSize());
		for(int i=0;i<DNPointColorSize;i++){
			pTDataNode DNColor(DNPointColor->Get(i));
			int PointColorId(DNColor->GetChildInt(L"id"));

			Object->SetUsePointColor(PointColorId, true);
			Object->PointColor[PointColorId] = DC.ToColor(DNColor);
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TDNSprite::Init(const pTDataNode &Node, const pTSprite &Object)
{
	InitBaseParams(Node, Object);

	TDNEngineDataConverter DC;

	// угол наклона
	float ObjectAngle(Object->Angle.Get());
	if(ObjectAngle != 0.0f){ Node->Add(ObjectAngle, L"angle"); }

	// нода, описывающая параметры текстуры
	pTDataNode TextureNode( Node->Add(new TDataNode) );
	TextureNode->SetName(L"Texture");

	if( Textures!=NULL ){
		pTTextureList TextureList(Textures->GetListByTexture(Object->Texture.Get()));
		TextureNode->Add(TextureList->GetName(),L"list");
	}

	TextureNode->Add(Object->Texture.Get()->GetName(),L"name");

	if( Object->Texture.GetMirrorX() || Object->Texture.GetMirrorY() ){
		pTDataNode TextureMirrorNode = TextureNode->Add(new TDataNode);
		TextureMirrorNode->SetName(L"Mirror");

		bool MirorFlagX = Object->Texture.GetMirrorX();
		if( MirorFlagX ){ TextureMirrorNode->Add(MirorFlagX, L"x"); }

		bool MirorFlagY = Object->Texture.GetMirrorY();
		if( MirorFlagY ){ TextureMirrorNode->Add(MirorFlagY, L"y"); }
	}

	if( Object->Texture.GetUseState() == EN_TUS_Repeat ){
		if(Object->Texture.GetRepeatX() != 1.0f || Object->Texture.GetRepeatY() != 1.0f ){
			pTDataNode TextureRepeatNode = TextureNode->Add(new TDataNode);
			TextureRepeatNode->SetName(L"Repeat");

			float RepeatX(Object->Texture.GetRepeatX());
			if( RepeatX != 1.0f ){ TextureRepeatNode->Add(RepeatX, L"x"); }

			float RepeatY(Object->Texture.GetRepeatY());
			if( RepeatY != 1.0f ){ TextureRepeatNode->Add(RepeatY, L"y"); }
		}
	}
	else{
		pTDataNode TextureRectNode(TextureNode->Add(new TDataNode));
		TextureRectNode->SetName(L"Rect");

		TextureRectNode->Add(Object->Texture.GetRectX(), L"x");
		TextureRectNode->Add(Object->Texture.GetRectY(), L"y");

		TextureRectNode->Add(Object->Texture.GetRectWidth(), L"width");
		TextureRectNode->Add(Object->Texture.GetRectHeight(), L"height");
	}

	// позиция спрайта
	if(Object->Pos[0]!=0.0f || Object->Pos[1]!=0.0f || Object->Pos[2]!=0.0f){
		Node->Add(DC.From(Object->Pos,L"Pos"));
	}

	// центр спрайта
	if(Object->Center[0]!=0.0f || Object->Center[1]!=0.0f || Object->Center[2]!=0.0f){
		Node->Add(DC.From(Object->Center,L"Center"));
	}

	// размер спрайта
	Node->Add(DC.From(Object->Size, L"Size"));

	// добавление точек меша
	AddMeshPointRecord(Node, Object->Mesh.Get(0),  0.5f,  0.5f, L"Point0" );
	AddMeshPointRecord(Node, Object->Mesh.Get(1), -0.5f,  0.5f, L"Point1" );
	AddMeshPointRecord(Node, Object->Mesh.Get(2), -0.5f, -0.5f, L"Point2" );
	AddMeshPointRecord(Node, Object->Mesh.Get(3),  0.5f, -0.5f, L"Point3" );

	// Color спрайта
	TColor Color = Object->Color;
	if(	Color.Get(0) != 1.0f || Color.Get(1) != 1.0f ||
		Color.Get(2) != 1.0f || Color.Get(3) != 1.0f)
	{
		Node->Add(DC.From(Color, L"Color"));
	}

	//  цвета точек спрайта
	if( Object->GetUsePointColor() ){
		pTDataNode DNPointColor( Node->Add(new TDataNode) );
		DNPointColor->SetName(L"PointColor");

		for(int i=0;i<4;i++){
			if(Object->GetUsePointColor(i)){
				pTDataNode DNColor( DNPointColor->Add(DC.From(Object->PointColor[i], L"Color")) );
				DNColor->Add(i, L"id");
			}
		}
	}
}
///----------------------------------------------------------------------------------------------//
pTSprite TDNSprite::Run(const pTDataNode &Node)
{
	pTSprite Sprite(new TSprite);

	Init(Sprite,Node);

	return Sprite;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNSprite::Run(const pTSprite &Object)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(L"Sprite");

	Init(Node,Object);

	return Node;
}
///----------------------------------------------------------------------------------------------//
void TDNSprite::Set(const pTBaseDevice &DeviceVal,const pTTextureListManager &TexturesVal)
{
	Device   = DeviceVal;
	Textures = TexturesVal;
}
///----------------------------------------------------------------------------------------------//
void TDNSprite::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///----------------------------------------------------------------------------------------------//
void TDNSprite::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
