///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DATANODE_H_INCLUDED
#define DATANODE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
enum enDataNodeType
{
	EN_DNT_Unknown 	= 0,
	EN_DNT_Bool		= 1,
	EN_DNT_Int 		= 2,
	EN_DNT_Float 	= 3,
	EN_DNT_String 	= 4,
	EN_DNT_Other	= 5
};
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDataNode
{
	protected:
		static unsigned long IdCounter;
		unsigned long Id;

		wstring Name;
		bool    Active;

		enDataNodeType Type;

		bool 	BoolVal;
		int 	IntVal;
		float 	FloatVal;
		wstring StringVal;

		TDataNode* Parent;
		vector<TDataNode*> Children;
	protected:
		inline void UnSetData();
	public:
		TDataNode();
		TDataNode(const bool 	&Val, const wstring &NewName=L"" );
		TDataNode(const int 	&Val, const wstring &NewName=L"" );
		TDataNode(const float 	&Val, const wstring &NewName=L"" );
		TDataNode(const wchar_t* Val, const wstring &NewName=L"" );
		TDataNode(const wstring &Val, const wstring &NewName=L"" );
		TDataNode(const TDataNode &Node);

		~TDataNode();

		void operator=(const TDataNode &Node);

		unsigned long GetID() const;

		void SetName(const wstring &Val);
		wstring GetName() const;

		void SetActive(const bool &Val);
		bool GetActive() const;

		void SetType(const enDataNodeType &Val);
		enDataNodeType GetType() const;

		void Set(const bool &Val);
		void Set(const int &Val);
		void Set(const float &Val);
		void Set(const wchar_t* wcVal);
		void Set(const wstring &Val);
		void UnSet();

		void SetVal(const bool &Val);
		void SetVal(const int &Val);
		void SetVal(const float &Val);
		void SetVal(const wchar_t* Val);
		void SetVal(const wstring &Val);

		bool    GetBool  ();
		int     GetInt   ();
		float   GetFloat ();
		wstring GetString();

		bool    GetBoolVal  () const;
		int     GetIntVal   () const;
		float   GetFloatVal () const;
		wstring GetStringVal() const;

		void SetParent(TDataNode *Val);
		TDataNode* GetParent();
		void UnSetParent();

		int GetSize() const;

		TDataNode* Add(const bool 		&ChildVal, const wstring &ChildName=L"");
		TDataNode* Add(const int 		&ChildVal, const wstring &ChildName=L"");
		TDataNode* Add(const float 		&ChildVal, const wstring &ChildName=L"");
		TDataNode* Add(const wchar_t*	 ChildVal, const wstring &ChildName=L"");
		TDataNode* Add(const wstring 	&ChildVal, const wstring &ChildName=L"");
		TDataNode* Add(TDataNode* Child);

		TDataNode* Get(const int &Index);
		TDataNode* Get(const wstring &ChildName);

		TDataNode* Get(const int     &Index)     const;
		TDataNode* Get(const wstring &ChildName) const;

		bool IsExist(const wstring &ChildName);

		void Del(const int &Index);
		void Del(TDataNode* Child);

		unsigned long GetChildID(const int     &Index)     const;
		unsigned long GetChildID(const wstring &ChildName) const;

		enDataNodeType GetChildType(const int     &Index)     const;
		enDataNodeType GetChildType(const wstring &ChildName) const;

		bool    GetChildBool	(const int &Index);
		int     GetChildInt		(const int &Index);
		float   GetChildFloat	(const int &Index);
		wstring GetChildString	(const int &Index);

		bool    GetChildBool	(const wstring &ChildName);
		int     GetChildInt		(const wstring &ChildName);
		float   GetChildFloat	(const wstring &ChildName);
		wstring GetChildString	(const wstring &ChildName);

		bool    GetChildBoolVal	 (const int &Index) const;
		int     GetChildIntVal	 (const int &Index) const;
		float   GetChildFloatVal (const int &Index) const;
		wstring GetChildStringVal(const int &Index) const;

		bool    GetChildBoolVal	 (const wstring &ChildName) const;
		int     GetChildIntVal	 (const wstring &ChildName) const;
		float   GetChildFloatVal (const wstring &ChildName) const;
		wstring GetChildStringVal(const wstring &ChildName) const;

		void Replace(const bool    &BaseVal, const bool    &NewVal);
		void Replace(const int     &BaseVal, const int     &NewVal);
		void Replace(const float   &BaseVal, const float   &NewVal);
		void Replace(const wstring &BaseVal, const wstring &NewVal);

		void Clear();
};

typedef TDataNode* pTDataNode;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DATANODE_H_INCLUDED
