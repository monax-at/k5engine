#include "DNEngineDataConverter.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNEngineDataConverter::TDNEngineDataConverter()
{
}
///----------------------------------------------------------------------------------------------//
TDNEngineDataConverter::~TDNEngineDataConverter()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TPoint TDNEngineDataConverter::ToPoint(const pTDataNode &Node, const pTBaseDevice &Device)
{
	TPoint Point;

	if(Node->IsExist(L"x")){ Point.SetX( Node->Get(L"x")->GetFloat() ); }
	if(Node->IsExist(L"y")){ Point.SetY( Node->Get(L"y")->GetFloat() ); }
	if(Node->IsExist(L"z")){ Point.SetZ( Node->Get(L"z")->GetFloat() ); }

	if(Device==NULL){return Point;}

	if(Node->IsExist(L"alignment_x")){
		switch(ToAlignment(Node->Get(L"alignment_x"))){
			case EN_A_Left	:{ Point[0] = Point.GetX(); }break;
			case EN_A_Right	:{ Point[0] = Device->GetWidth() - Point.GetX(); }break;
			case EN_A_Center:{ Point[0]+= Device->GetWidth()/2.0f; }break;
			default:break;
		}
	}

	if(Node->IsExist(L"alignment_y")){
		switch(ToAlignment(Node->Get(L"alignment_y"))){
			case EN_A_Bottom:{ Point[1] = Point.GetY(); }break;
			case EN_A_Top	:{ Point[1] = Device->GetHeight() - Point.GetY();} break;
			case EN_A_Center:{ Point[1]+= Device->GetHeight()/2.0f; }break;
			default:break;
		}
	}

	return Point;
}
///----------------------------------------------------------------------------------------------//
TVector2D TDNEngineDataConverter::ToVector2D(const pTDataNode &Node)
{
	TVector2D Vector;

	if(Node->IsExist(L"x")){ Vector.x = Node->Get(L"x")->GetFloat(); }
	if(Node->IsExist(L"y")){ Vector.y = Node->Get(L"y")->GetFloat(); }

	return Vector;
}
///----------------------------------------------------------------------------------------------//
TVector3D TDNEngineDataConverter::ToVector3D(const pTDataNode &Node)
{
	TVector3D Vector;

	if(Node->IsExist(L"x")){ Vector.x = Node->Get(L"x")->GetFloat(); }
	if(Node->IsExist(L"y")){ Vector.y = Node->Get(L"y")->GetFloat(); }
	if(Node->IsExist(L"z")){ Vector.z = Node->Get(L"z")->GetFloat(); }

	return Vector;
}
///----------------------------------------------------------------------------------------------//
TColor TDNEngineDataConverter::ToColor(const pTDataNode &Node)
{
	TColor Color;

	if(Node->IsExist(L"r")){ Color.SetRed	(Node->Get(L"r")->GetFloat()); }
	if(Node->IsExist(L"g")){ Color.SetGreen	(Node->Get(L"g")->GetFloat()); }
	if(Node->IsExist(L"b")){ Color.SetBlue	(Node->Get(L"b")->GetFloat()); }
	if(Node->IsExist(L"a")){ Color.SetAlpha	(Node->Get(L"a")->GetFloat()); }

	return Color;
}
///----------------------------------------------------------------------------------------------//
enTextureFilter	TDNEngineDataConverter::ToTextureFilter(const pTDataNode &Node)
{
	wstring Val(Node->GetStringVal());

	if(Val == L"linear")	{ return EN_TF_Linear; }
	if(Val == L"point")		{ return EN_TF_Point; }
	if(Val == L"none")		{ return EN_TF_None; }

	return EN_TF_Unknown;
}
///----------------------------------------------------------------------------------------------//
enAlignment TDNEngineDataConverter::ToAlignment(const pTDataNode &Node)
{
	wstring Val(Node->GetStringVal());

	if(Val == L"center"){ return EN_A_Center; }
	if(Val == L"left")	{ return EN_A_Left; }
	if(Val == L"right")	{ return EN_A_Right; }
	if(Val == L"bottom"){ return EN_A_Bottom; }
	if(Val == L"top")	{ return EN_A_Top; }

	return EN_A_Unknown;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNEngineDataConverter::From(const TPoint &Point, const wstring &NodeName)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(NodeName);

	if(Point.GetX()!=0.0f){ Node->Add(Point.GetX(),L"x"); }
	if(Point.GetY()!=0.0f){ Node->Add(Point.GetY(),L"y"); }
	if(Point.GetZ()!=0.0f){ Node->Add(Point.GetZ(),L"z"); }

	return Node;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNEngineDataConverter::From(const TVector2D &Vector, const wstring &NodeName)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(NodeName);

	if(Vector.x != 0.0f){ Node->Add(Vector.x, L"x"); }
	if(Vector.y != 0.0f){ Node->Add(Vector.y, L"y"); }

	return Node;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNEngineDataConverter::From(const TVector3D &Vector, const wstring &NodeName)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(NodeName);

	if(Vector.x != 0.0f){ Node->Add(Vector.x, L"x"); }
	if(Vector.y != 0.0f){ Node->Add(Vector.y, L"y"); }
	if(Vector.z != 0.0f){ Node->Add(Vector.z, L"z"); }

	return Node;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNEngineDataConverter::From(const TColor &Color, const wstring &NodeName)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(NodeName);

	if(Color.GetRed()	!=1.0f){ Node->Add(Color.GetRed(),	L"r"); }
	if(Color.GetGreen()	!=1.0f){ Node->Add(Color.GetGreen(),L"g"); }
	if(Color.GetBlue()	!=1.0f){ Node->Add(Color.GetBlue(),	L"b"); }
	if(Color.GetAlpha()	!=1.0f){ Node->Add(Color.GetAlpha(),L"a"); }

	return Node;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNEngineDataConverter::From(const enTextureFilter &Filter,
										const wstring &NodeName)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(NodeName);

	switch(Filter){
		case EN_TF_Linear	:{ Node->Set(L"linear"); }break;
		case EN_TF_Point	:{ Node->Set(L"point"); }break;
		case EN_TF_None		:{ Node->Set(L"none"); }break;

		default:break;
	}

	return Node;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNEngineDataConverter::From(const enAlignment &Alignm,
										const wstring &NodeName)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(NodeName);

	switch(Alignm){
		case EN_A_Center	:{ Node->Set(L"center");	}break;
		case EN_A_Left		:{ Node->Set(L"left"); 		}break;
		case EN_A_Right		:{ Node->Set(L"right"); 	}break;
		case EN_A_Bottom	:{ Node->Set(L"bottom");	}break;
		case EN_A_Top		:{ Node->Set(L"top"); 		}break;
		case EN_A_Unknown	:{ Node->Set(L"unknown");	}break;

		default:break;
	}

	return Node;
}
///----------------------------------------------------------------------------------------------//
pTTexture TDNEngineDataConverter::GetTexture(const pTTextureListManager &Manager,
											 const pTDataNode &Node)
{
	return Manager->Get(Node->GetChildString(L"list"), Node->GetChildString(L"name"));
}
///----------------------------------------------------------------------------------------------//
pTSprite TDNEngineDataConverter::GetSprite(	const pTSpriteList &List, const pTDataNode &Node)
{
	return List->GetIfExist(Node->GetChildString(L"name"));
}
///----------------------------------------------------------------------------------------------//
pTSprite TDNEngineDataConverter::GetSprite(	const pTSpriteListManager &Manager,
											const pTDataNode &Node)
{
	return Manager->GetIfExist(Node->GetChildString(L"list"), Node->GetChildString(L"name"));
}
///----------------------------------------------------------------------------------------------//
pTText TDNEngineDataConverter::GetText(	const pTTextList &List, const pTDataNode &Node)
{
	return List->GetIfExist(Node->GetChildString(L"name"));
}
///----------------------------------------------------------------------------------------------//
pTText TDNEngineDataConverter::GetText(	const pTTextListManager &Manager,
										const pTDataNode &Node)
{
	return Manager->GetIfExist(Node->GetChildString(L"list"), Node->GetChildString(L"name"));
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNEngineDataConverter::GetAction(	const pTActionList &List, const pTDataNode &Node)
{
	return List->GetIfExist(Node->GetChildString(L"name"));
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNEngineDataConverter::GetAction(	const pTActionListManager &Manager,
												const pTDataNode &Node)
{
	return Manager->GetIfExist(Node->GetChildString(L"list"), Node->GetChildString(L"name"));
}
///----------------------------------------------------------------------------------------------//
pTTextureList TDNEngineDataConverter::GetTextureList(const pTTextureListManager &Manager,
													 const pTDataNode &Node)
{
	return Manager->Get(Node->GetString());
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TDNEngineDataConverter::GetSpriteList(	const pTSpriteListManager &Manager,
													const pTDataNode &Node)
{
	return Manager->Get(Node->GetString());
}
///----------------------------------------------------------------------------------------------//
pTTextList TDNEngineDataConverter::GetTextList(	const pTTextListManager &Manager,
												const pTDataNode &Node)
{
	return Manager->Get(Node->GetString());
}
///----------------------------------------------------------------------------------------------//
pTActionList TDNEngineDataConverter::GetActionList(	const pTActionListManager &Manager,
													const pTDataNode &Node)
{
	return Manager->Get(Node->GetString());
}
///----------------------------------------------------------------------------------------------//
enEventTypes TDNEngineDataConverter::ToEventTypes(const pTDataNode &Node)
{
	wstring Val(Node->GetString());

	if(Val == L"EN_ERROR" )		{return EN_ERROR;}
	if(Val == L"EN_EVENTTRUE" )	{return EN_EVENTTRUE;}
	if(Val == L"EN_EVENTFALSE" ){return EN_EVENTFALSE;}

	///-- Типы событий
	if(Val == L"EN_NONE" )		{return EN_NONE;}
	if(Val == L"EN_SYSTEM" )	{return EN_SYSTEM;}
	if(Val == L"EN_KEYBOARD" )	{return EN_KEYBOARD;}
	if(Val == L"EN_MOUSE" )		{return EN_MOUSE;}
	if(Val == L"EN_TIMER" )		{return EN_TIMER;}
	if(Val == L"EN_USER" )		{return EN_USER;}

	///-- типы событтий для системы
	if(Val == L"EN_QUIT" )			{return EN_QUIT;}
	if(Val == L"EN_WINDOWRESIZE" )	{return EN_WINDOWRESIZE;}
	if(Val == L"EN_ACTIVATE" )		{return EN_ACTIVATE;}
	if(Val == L"EN_DEACTIVATE" )	{return EN_DEACTIVATE;}
	if(Val == L"EN_PAINT" )			{return EN_PAINT;}
	if(Val == L"EN_MINIMIZE" )		{return EN_MINIMIZE;}
	if(Val == L"EN_RESTORE" )		{return EN_RESTORE;}
	if(Val == L"EN_DEVICELOST" )	{return EN_DEVICELOST;}
	if(Val == L"EN_DEVICERESTORE" )	{return EN_DEVICERESTORE;}
	if(Val == L"EN_DISPLAYCHANGE" )	{return EN_DISPLAYCHANGE;}

	///-- типы событтия для клавиатуры
	if(Val == L"EN_KEYDOWN" )	{return EN_KEYDOWN;}
	if(Val == L"EN_KEYUP" )		{return EN_KEYUP;}
	if(Val == L"EN_KEYCHAR" )	{return EN_KEYCHAR;}

	///-- модификаторы нажатых кнопок
	if(Val == L"EN_KM_NONE" )	{return EN_KM_NONE;}
	if(Val == L"EN_KM_NUM" )	{return EN_KM_NUM;}
	if(Val == L"EN_KM_CAPS" )	{return EN_KM_CAPS;}
	if(Val == L"EN_KM_LCTRL" )	{return EN_KM_LCTRL;}
	if(Val == L"EN_KM_RCTRL" )	{return EN_KM_RCTRL;}
	if(Val == L"EN_KM_RSHIFT" )	{return EN_KM_RSHIFT;}
	if(Val == L"EN_KM_LSHIFT" )	{return EN_KM_LSHIFT;}
	if(Val == L"EN_KM_RALT" )	{return EN_KM_RALT;}
	if(Val == L"EN_KM_LALT" )	{return EN_KM_LALT;}
	if(Val == L"EN_KM_SHIFT" )	{return EN_KM_SHIFT;}
	if(Val == L"EN_KM_ALT" )	{return EN_KM_ALT;}

	///-- параметр KeySum - "виртуальные" значения кнопок
	if(Val == L"EN_KEY_0" ){return EN_KEY_0;}
	if(Val == L"EN_KEY_1" ){return EN_KEY_1;}
	if(Val == L"EN_KEY_2" ){return EN_KEY_2;}
	if(Val == L"EN_KEY_3" ){return EN_KEY_3;}
	if(Val == L"EN_KEY_4" ){return EN_KEY_4;}
	if(Val == L"EN_KEY_5" ){return EN_KEY_5;}
	if(Val == L"EN_KEY_6" ){return EN_KEY_6;}
	if(Val == L"EN_KEY_7" ){return EN_KEY_7;}
	if(Val == L"EN_KEY_8" ){return EN_KEY_8;}
	if(Val == L"EN_KEY_9" ){return EN_KEY_9;}
	if(Val == L"EN_KEY_MINUS" )	{return EN_KEY_MINUS;}
	if(Val == L"EN_KEY_PLUS" )	{return EN_KEY_PLUS;}
	if(Val == L"EN_KEY_TILDE" )	{return EN_KEY_TILDE;}

	if(Val == L"EN_KEY_A" ){return EN_KEY_A;}
	if(Val == L"EN_KEY_B" ){return EN_KEY_B;}
	if(Val == L"EN_KEY_C" ){return EN_KEY_C;}
	if(Val == L"EN_KEY_D" ){return EN_KEY_D;}
	if(Val == L"EN_KEY_E" ){return EN_KEY_E;}
	if(Val == L"EN_KEY_F" ){return EN_KEY_F;}
	if(Val == L"EN_KEY_G" ){return EN_KEY_G;}
	if(Val == L"EN_KEY_H" ){return EN_KEY_H;}
	if(Val == L"EN_KEY_I" ){return EN_KEY_I;}
	if(Val == L"EN_KEY_J" ){return EN_KEY_J;}
	if(Val == L"EN_KEY_K" ){return EN_KEY_K;}
	if(Val == L"EN_KEY_L" ){return EN_KEY_L;}
	if(Val == L"EN_KEY_M" ){return EN_KEY_M;}
	if(Val == L"EN_KEY_N" ){return EN_KEY_N;}
	if(Val == L"EN_KEY_O" ){return EN_KEY_O;}
	if(Val == L"EN_KEY_P" ){return EN_KEY_P;}
	if(Val == L"EN_KEY_Q" ){return EN_KEY_Q;}
	if(Val == L"EN_KEY_R" ){return EN_KEY_R;}
	if(Val == L"EN_KEY_S" ){return EN_KEY_S;}
	if(Val == L"EN_KEY_T" ){return EN_KEY_T;}
	if(Val == L"EN_KEY_U" ){return EN_KEY_U;}
	if(Val == L"EN_KEY_V" ){return EN_KEY_V;}
	if(Val == L"EN_KEY_W" ){return EN_KEY_W;}
	if(Val == L"EN_KEY_X" ){return EN_KEY_X;}
	if(Val == L"EN_KEY_Y" ){return EN_KEY_Y;}
	if(Val == L"EN_KEY_Z" ){return EN_KEY_Z;}
	if(Val == L"EN_KEY_LBKT" )		{return EN_KEY_LBKT;}
	if(Val == L"EN_KEY_RBKT" )		{return EN_KEY_RBKT;}
	if(Val == L"EN_KEY_SEMICOLON" )	{return EN_KEY_SEMICOLON;}
	if(Val == L"EN_KEY_DQUOTE" )	{return EN_KEY_COMMA;}
	if(Val == L"EN_KEY_COMMA" )		{return EN_KEY_COMMA;}
	if(Val == L"EN_KEY_PERIOD" )	{return EN_KEY_PERIOD;}
	if(Val == L"EN_KEY_SLASH" )		{return EN_KEY_SLASH;}

	if(Val == L"EN_KEY_ESCAPE" ){return EN_KEY_ESCAPE;}
	if(Val == L"EN_KEY_SPACE" )	{return EN_KEY_SPACE;}
	if(Val == L"EN_KEY_ENTER" )	{return EN_KEY_ENTER;}
	if(Val == L"EN_KEY_CTRL" )	{return EN_KEY_CTRL;}
	if(Val == L"EN_KEY_RALT" )	{return EN_KEY_RALT;}
	if(Val == L"EN_KEY_LALT" )	{return EN_KEY_LALT;}
	if(Val == L"EN_KEY_SHIFT" )	{return EN_KEY_SHIFT;}
	if(Val == L"EN_KEY_UP" )	{return EN_KEY_UP;}
	if(Val == L"EN_KEY_DOWN" )	{return EN_KEY_DOWN;}
	if(Val == L"EN_KEY_LEFT" )	{return EN_KEY_LEFT;}
	if(Val == L"EN_KEY_RIGHT" )	{return EN_KEY_RIGHT;}
	if(Val == L"EN_KEY_BACK" )	{return EN_KEY_BACK;}
	if(Val == L"EN_KEY_TAB" )	{return EN_KEY_TAB;}

	/// типы событтий для мыши
	if(Val == L"EN_MOUSEMOTION" )		{return EN_MOUSEMOTION;}
	if(Val == L"EN_MOUSEBUTTONDOWN" )	{return EN_MOUSEBUTTONDOWN;}
	if(Val == L"EN_MOUSEBUTTONUP" )		{return EN_MOUSEBUTTONUP;}
	if(Val == L"EN_MOUSEWINDOWOUT" )	{return EN_MOUSEWINDOWOUT;}
	if(Val == L"EN_MOUSEWINDOWIN" )		{return EN_MOUSEWINDOWIN;}
	if(Val == L"EN_MOUSEWHEELUP" )		{return EN_MOUSEWHEELUP;}
	if(Val == L"EN_MOUSEWHEELDOWN" )	{return EN_MOUSEWHEELDOWN;}

	/// нажатая кнопка мыши
	if(Val == L"EN_MOUSEBUTTONLEFT" )	{return EN_MOUSEBUTTONLEFT;}
	if(Val == L"EN_MOUSEBUTTONMIDDLE" )	{return EN_MOUSEBUTTONMIDDLE;}
	if(Val == L"EN_MOUSEBUTTONRIGHT" )	{return EN_MOUSEBUTTONRIGHT;}

	return EN_ERROR;
}
///----------------------------------------------------------------------------------------------//
void TDNEngineDataConverter::From(const enEventTypes &EventTypes, const pTDataNode &Node)
{
	switch (EventTypes){
		default:{ Node->Set((int)EventTypes); }break;

		case EN_ERROR	  :{ Node->Set(L"EN_ERROR"); 		}break;
		case EN_EVENTTRUE :{ Node->Set(L"EN_EVENTTRUE"); 	}break;
		case EN_EVENTFALSE:{ Node->Set(L"EN_EVENTFALSE"); 	}break;

		///-- Типы событий
		case EN_NONE	:{ Node->Set(L"EN_NONE"); 		}break;
		case EN_SYSTEM	:{ Node->Set(L"EN_SYSTEM"); 	}break;
		case EN_KEYBOARD:{ Node->Set(L"EN_KEYBOARD"); 	}break;
		case EN_MOUSE	:{ Node->Set(L"EN_MOUSE"); 		}break;
		case EN_TIMER	:{ Node->Set(L"EN_TIMER"); 		}break;
		case EN_USER	:{ Node->Set(L"EN_USER");		}break;

		///-- типы событтий для системы
		case EN_QUIT			:{ Node->Set(L"EN_QUIT"); 			}break;
		case EN_WINDOWRESIZE	:{ Node->Set(L"EN_WINDOWRESIZE"); 	}break;
		case EN_ACTIVATE		:{ Node->Set(L"EN_ACTIVATE"); 		}break;
		case EN_DEACTIVATE	 	:{ Node->Set(L"EN_DEACTIVATE"); 	}break;
		case EN_PAINT		 	:{ Node->Set(L"EN_PAINT"); 			}break;
		case EN_MINIMIZE		:{ Node->Set(L"EN_MINIMIZE"); 		}break;
		case EN_RESTORE		 	:{ Node->Set(L"EN_RESTORE"); 		}break;
		case EN_DEVICELOST 	 	:{ Node->Set(L"EN_DEVICELOST"); 	}break;
		case EN_DEVICERESTORE 	:{ Node->Set(L"EN_DEVICERESTORE"); 	}break;
		case EN_DISPLAYCHANGE 	:{ Node->Set(L"EN_DISPLAYCHANGE"); 	}break;

		///-- типы событтия для клавиатуры
		case EN_KEYDOWN	:{ Node->Set(L"EN_KEYDOWN"); 	}break;
		case EN_KEYUP	:{ Node->Set(L"EN_KEYUP"); 		}break;
		case EN_KEYCHAR	:{ Node->Set(L"EN_KEYCHAR"); 	}break;

		///-- модификаторы нажатых кнопок
		case EN_KM_NONE             :{ Node->Set(L"EN_KM_NONE"); 	}break;
		case EN_KM_NUM              :{ Node->Set(L"EN_KM_NUM"); 	}break;  // нажат Numlock
		case EN_KM_CAPS             :{ Node->Set(L"EN_KM_CAPS"); 	}break;
		case EN_KM_LCTRL            :{ Node->Set(L"EN_KM_LCTRL"); 	}break;
		case EN_KM_RCTRL            :{ Node->Set(L"EN_KM_RCTRL"); 	}break;
		case EN_KM_RSHIFT           :{ Node->Set(L"EN_KM_RSHIFT"); 	}break;
		case EN_KM_LSHIFT           :{ Node->Set(L"EN_KM_LSHIFT"); 	}break;
		case EN_KM_RALT             :{ Node->Set(L"EN_KM_RALT"); 	}break;
		case EN_KM_LALT             :{ Node->Set(L"EN_KM_LALT"); 	}break;
		case EN_KM_SHIFT            :{ Node->Set(L"EN_KM_SHIFT"); 	}break;
		case EN_KM_ALT              :{ Node->Set(L"EN_KM_ALT"); 	}break;

		///-- параметр KeySum - "виртуальные" значения кнопок
		case EN_KEY_0               :{ Node->Set(L"EN_KEY_0"); }break;
		case EN_KEY_1               :{ Node->Set(L"EN_KEY_1"); }break;
		case EN_KEY_2               :{ Node->Set(L"EN_KEY_2"); }break;
		case EN_KEY_3               :{ Node->Set(L"EN_KEY_3"); }break;
		case EN_KEY_4               :{ Node->Set(L"EN_KEY_4"); }break;
		case EN_KEY_5               :{ Node->Set(L"EN_KEY_5"); }break;
		case EN_KEY_6               :{ Node->Set(L"EN_KEY_6"); }break;
		case EN_KEY_7               :{ Node->Set(L"EN_KEY_7"); }break;
		case EN_KEY_8               :{ Node->Set(L"EN_KEY_8"); }break;
		case EN_KEY_9               :{ Node->Set(L"EN_KEY_9"); }break;
		case EN_KEY_MINUS        	:{ Node->Set(L"EN_KEY_MINUS"); 	}break;
		case EN_KEY_PLUS           	:{ Node->Set(L"EN_KEY_PLUS"); 	}break;
		case EN_KEY_TILDE			:{ Node->Set(L"EN_KEY_TILDE"); 	}break;

		case EN_KEY_A               :{ Node->Set(L"EN_KEY_A"); }break;
		case EN_KEY_B               :{ Node->Set(L"EN_KEY_B"); }break;
		case EN_KEY_C               :{ Node->Set(L"EN_KEY_C"); }break;
		case EN_KEY_D               :{ Node->Set(L"EN_KEY_D"); }break;
		case EN_KEY_E               :{ Node->Set(L"EN_KEY_E"); }break;
		case EN_KEY_F               :{ Node->Set(L"EN_KEY_F"); }break;
		case EN_KEY_G               :{ Node->Set(L"EN_KEY_G"); }break;
		case EN_KEY_H               :{ Node->Set(L"EN_KEY_H"); }break;
		case EN_KEY_I               :{ Node->Set(L"EN_KEY_I"); }break;
		case EN_KEY_J               :{ Node->Set(L"EN_KEY_J"); }break;
		case EN_KEY_K				:{ Node->Set(L"EN_KEY_K"); }break;
		case EN_KEY_L				:{ Node->Set(L"EN_KEY_L"); }break;
		case EN_KEY_M				:{ Node->Set(L"EN_KEY_M"); }break;
		case EN_KEY_N				:{ Node->Set(L"EN_KEY_N"); }break;
		case EN_KEY_O				:{ Node->Set(L"EN_KEY_O"); }break;
		case EN_KEY_P				:{ Node->Set(L"EN_KEY_P"); }break;
		case EN_KEY_Q				:{ Node->Set(L"EN_KEY_Q"); }break;
		case EN_KEY_R				:{ Node->Set(L"EN_KEY_R"); }break;
		case EN_KEY_S				:{ Node->Set(L"EN_KEY_S"); }break;
		case EN_KEY_T				:{ Node->Set(L"EN_KEY_T"); }break;
		case EN_KEY_U				:{ Node->Set(L"EN_KEY_U"); }break;
		case EN_KEY_V				:{ Node->Set(L"EN_KEY_V"); }break;
		case EN_KEY_W				:{ Node->Set(L"EN_KEY_W"); }break;
		case EN_KEY_X				:{ Node->Set(L"EN_KEY_X"); }break;
		case EN_KEY_Y				:{ Node->Set(L"EN_KEY_Y"); }break;
		case EN_KEY_Z				:{ Node->Set(L"EN_KEY_Z"); }break;
		case EN_KEY_LBKT			:{ Node->Set(L"EN_KEY_LBKT"); 		}break;
		case EN_KEY_RBKT			:{ Node->Set(L"EN_KEY_RBKT"); 		}break;
		case EN_KEY_SEMICOLON		:{ Node->Set(L"EN_KEY_SEMICOLON"); 	}break;
		case EN_KEY_DQUOTE			:{ Node->Set(L"EN_KEY_DQUOTE"); 	}break;
		case EN_KEY_COMMA			:{ Node->Set(L"EN_KEY_COMMA"); 		}break;
		case EN_KEY_PERIOD			:{ Node->Set(L"EN_KEY_PERIOD"); 	}break;
		case EN_KEY_SLASH			:{ Node->Set(L"EN_KEY_SLASH"); 		}break;

		case EN_KEY_ESCAPE			:{ Node->Set(L"EN_KEY_ESCAPE"); }break;
		case EN_KEY_SPACE			:{ Node->Set(L"EN_KEY_SPACE"); 	}break;
		case EN_KEY_ENTER			:{ Node->Set(L"EN_KEY_ENTER"); 	}break;
		case EN_KEY_CTRL			:{ Node->Set(L"EN_KEY_CTRL"); 	}break;
		case EN_KEY_RALT			:{ Node->Set(L"EN_KEY_RALT"); 	}break;
		case EN_KEY_LALT			:{ Node->Set(L"EN_KEY_LALT"); 	}break;
		case EN_KEY_SHIFT			:{ Node->Set(L"EN_KEY_SHIFT"); 	}break;
		case EN_KEY_UP				:{ Node->Set(L"EN_KEY_UP"); 	}break;
		case EN_KEY_DOWN			:{ Node->Set(L"EN_KEY_DOWN"); 	}break;
		case EN_KEY_LEFT			:{ Node->Set(L"EN_KEY_LEFT"); 	}break;
		case EN_KEY_RIGHT			:{ Node->Set(L"EN_KEY_RIGHT"); 	}break;
		case EN_KEY_BACK			:{ Node->Set(L"EN_KEY_BACK"); 	}break;
		case EN_KEY_TAB				:{ Node->Set(L"EN_KEY_TAB"); 	}break;

		/// типы событтий для мыши
		case EN_MOUSEMOTION         :{ Node->Set(L"EN_MOUSEMOTION"); 	}break;
		case EN_MOUSEBUTTONDOWN     :{ Node->Set(L"EN_MOUSEBUTTONDOWN");}break;
		case EN_MOUSEBUTTONUP       :{ Node->Set(L"EN_MOUSEBUTTONUP"); 	}break;
		case EN_MOUSEWINDOWOUT      :{ Node->Set(L"EN_MOUSEWINDOWOUT"); }break;
		case EN_MOUSEWINDOWIN       :{ Node->Set(L"EN_MOUSEWINDOWIN"); 	}break;
		case EN_MOUSEWHEELUP        :{ Node->Set(L"EN_MOUSEWHEELUP"); 	}break;
		case EN_MOUSEWHEELDOWN      :{ Node->Set(L"EN_MOUSEWHEELDOWN"); }break;

		/// нажатая кнопка мыши
		case EN_MOUSEBUTTONLEFT     :{ Node->Set(L"EN_MOUSEBUTTONLEFT"); 	}break;
		case EN_MOUSEBUTTONMIDDLE   :{ Node->Set(L"EN_MOUSEBUTTONMIDDLE"); 	}break;
		case EN_MOUSEBUTTONRIGHT    :{ Node->Set(L"EN_MOUSEBUTTONRIGHT"); 	}break;
	}
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNEngineDataConverter::From(const enEventTypes &EventTypes, const wstring &NodeName)
{
	pTDataNode Node(new TDataNode);

	Node->SetName(NodeName);
	From(EventTypes, Node);

	return Node;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
