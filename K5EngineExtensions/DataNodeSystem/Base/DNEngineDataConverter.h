///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNENGINEDATACONVERTER_H_INCLUDED
#define DNENGINEDATACONVERTER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "DataNode.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNEngineDataConverter
{
	public:
		TDNEngineDataConverter();
		~TDNEngineDataConverter();

		// если дополнительно задан Device, то координата будет задаваться с учётом
		// присутсвия в Node элементов alignment_x и alignment_y - вырвынивание
		// относительно стороны экрана
		TPoint ToPoint(const pTDataNode &Node, const pTBaseDevice &Device=NULL);

		TVector2D 		ToVector2D		(const pTDataNode &Node);
		TVector3D 		ToVector3D		(const pTDataNode &Node);
		TColor			ToColor			(const pTDataNode &Node);
		enTextureFilter	ToTextureFilter	(const pTDataNode &Node);
		enAlignment 	ToAlignment		(const pTDataNode &Node);

		pTDataNode From(const TPoint  			&Point,	const wstring &NodeName=L"");
		pTDataNode From(const TVector2D 		&Vector,const wstring &NodeName=L"");
		pTDataNode From(const TVector3D 		&Vector,const wstring &NodeName=L"");
		pTDataNode From(const TColor 			&Color,	const wstring &NodeName=L"");
		pTDataNode From(const enTextureFilter 	&Filter,const wstring &NodeName=L"");
		pTDataNode From(const enAlignment 		&Alignm,const wstring &NodeName=L"");

		// получение объекта по именам списка и объекта, которые записанны в ноде
		pTTexture GetTexture(const pTTextureListManager &Manager,
							 const pTDataNode &Node);

		// далее функции взятия графических объектов
		// если объекта нет в казанном списке или менеджере, то будет возвращён NULL
		pTSprite GetSprite(	const pTSpriteList        &List   , const pTDataNode &Node);
		pTSprite GetSprite(	const pTSpriteListManager &Manager, const pTDataNode &Node);

		pTText GetText(	const pTTextList        &List   , const pTDataNode &Node);
		pTText GetText(	const pTTextListManager &Manager, const pTDataNode &Node);

		pTBaseAction GetAction(	const pTActionList        &List   , const pTDataNode &Node);
		pTBaseAction GetAction(	const pTActionListManager &Manager, const pTDataNode &Node);

		// получение списка объектов по ноде с его именем
		pTTextureList GetTextureList(const pTTextureListManager &Manager,
									const pTDataNode &Node);

		pTSpriteList GetSpriteList(	const pTSpriteListManager &Manager,
									const pTDataNode &Node);

		pTTextList GetTextList(	const pTTextListManager &Manager,
								const pTDataNode &Node);

		pTActionList GetActionList(	const pTActionListManager &Manager,
									const pTDataNode &Node);

		// кодирование и декодирвоание enEventTypes
		// 1- декодирование ноды
		// 2- инициализация уже существующей ноды
		// 3- создание и инициализация новой ноды
		enEventTypes ToEventTypes(const pTDataNode &Node);
		void         From(const enEventTypes &EventTypes, const pTDataNode &Node);
		pTDataNode   From(const enEventTypes &EventTypes, const wstring &NodeName=L"");
};

typedef TDNEngineDataConverter* pTDNEngineDataConverter;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNENGINEDATACONVERTER_H_INCLUDED
