#include "DataNode.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
unsigned long TDataNode::IdCounter = 0;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDataNode::TDataNode():	Active(true), Type(EN_DNT_Unknown),BoolVal(false), IntVal(0),
								FloatVal(0.0f), Parent(NULL)
{
	IdCounter++;
	Id = IdCounter;
}
///----------------------------------------------------------------------------------------------//
TDataNode::TDataNode(const bool &Val, const wstring &NewName):Name(NewName),
							Active(true), Type(EN_DNT_Bool), BoolVal(Val), IntVal(0),
							FloatVal(0.0f), Parent(NULL)
{
	IdCounter++;
	Id = IdCounter;
}
///----------------------------------------------------------------------------------------------//
TDataNode::TDataNode(const int &Val, const wstring &NewName):Name(NewName),
							Active(true), Type(EN_DNT_Int), BoolVal(false), IntVal(Val),
							FloatVal(0.0f), Parent(NULL)
{
	IdCounter++;
	Id = IdCounter;
}
///----------------------------------------------------------------------------------------------//
TDataNode::TDataNode(const float &Val, const wstring &NewName):Name(NewName),
							Active(true), Type(EN_DNT_Float), BoolVal(false), IntVal(0),
							FloatVal(Val), Parent(NULL)
{
	IdCounter++;
	Id = IdCounter;
}
///----------------------------------------------------------------------------------------------//
TDataNode::TDataNode(const wchar_t* Val, const wstring &NewName ):Name(NewName),
							Active(true), Type(EN_DNT_String),
							BoolVal(false), IntVal(0),
							FloatVal(0.0f), StringVal(Val),Parent(NULL)
{
	IdCounter++;
	Id = IdCounter;
}
///----------------------------------------------------------------------------------------------//
TDataNode::TDataNode(const wstring &Val, const wstring &NewName):Name(NewName),
							Active(true), Type(EN_DNT_String),
							BoolVal(false), IntVal(0),
							FloatVal(0.0f), StringVal(Val),Parent(NULL)
{
	IdCounter++;
	Id = IdCounter;
}
///----------------------------------------------------------------------------------------------//
TDataNode::TDataNode(const TDataNode &Node)
{
	*this = Node;
}
///----------------------------------------------------------------------------------------------//
TDataNode::~TDataNode()
{
	Clear();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDataNode::operator=(const TDataNode &Node)
{
	Clear();

	Type = Node.GetType();

	Name   = Node.GetName();
	Active = Node.GetActive();

	BoolVal   = Node.GetBoolVal();
	IntVal    = Node.GetIntVal();
	FloatVal  = Node.GetFloatVal();
	StringVal = Node.GetStringVal();

	int Size(Node.GetSize());
	for(int i=0;i<Size;i++){ Add(new TDataNode(*Node.Get(i))); }
}
///----------------------------------------------------------------------------------------------//
void TDataNode::UnSetData()
{
	Type = EN_DNT_Unknown;

	BoolVal = false;
	IntVal = 0;
	FloatVal = 0.0f;
	StringVal = L"";
}
///----------------------------------------------------------------------------------------------//
unsigned long TDataNode::GetID() const
{
	return Id;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::SetName(const wstring &Val)
{
	Name = Val;
}
///----------------------------------------------------------------------------------------------//
wstring TDataNode::GetName() const
{
	return Name;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::SetActive(const bool &Val)
{
	Active = Val;
}
///----------------------------------------------------------------------------------------------//
bool TDataNode::GetActive() const
{
	return Active;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::SetType(const enDataNodeType &Val)
{
	Type = Val;
}
///----------------------------------------------------------------------------------------------//
enDataNodeType TDataNode::GetType() const
{
	return Type;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Set(const bool &Val)
{
	UnSetData();
	Type = EN_DNT_Bool;
	BoolVal = Val;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Set(const int &Val)
{
	UnSetData();
	Type = EN_DNT_Int;
	IntVal = Val;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Set(const float &Val)
{
	UnSetData();
	Type = EN_DNT_Float;
	FloatVal = Val;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Set(const wchar_t* wcVal)
{
	UnSetData();
	Type = EN_DNT_String;
	StringVal = wcVal;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Set(const wstring &Val)
{
	UnSetData();
	Type = EN_DNT_String;
	StringVal = Val;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::UnSet()
{
	UnSetData();
}
///----------------------------------------------------------------------------------------------//
void TDataNode::SetVal(const bool &Val)
{
	BoolVal = Val;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::SetVal(const int &Val)
{
	IntVal = Val;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::SetVal(const float &Val)
{
	FloatVal = Val;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::SetVal(const wchar_t* Val)
{
	StringVal = Val;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::SetVal(const wstring &Val)
{
	StringVal = Val;
}
///----------------------------------------------------------------------------------------------//
bool TDataNode::GetBool()
{
	TExceptionGenerator Ex(L"TDataNode: ");
	Ex(Type!=EN_DNT_Unknown, L"in GetBool() Type is Unknown");

	switch(Type){
		case EN_DNT_Int		:{ BoolVal = IntVal   != 0; }break;
		case EN_DNT_Float	:{ BoolVal = FloatVal != 0.0f; }break;
		case EN_DNT_String	:{ BoolVal = WStrToBool(StringVal); }break;
		case EN_DNT_Other	:{ BoolVal = WStrToBool(StringVal); }break;
		default:{}break;
	}

	Type = EN_DNT_Bool;

	return BoolVal;
}
///----------------------------------------------------------------------------------------------//
int TDataNode::GetInt()
{
	TExceptionGenerator Ex(L"TDataNode: ");
	Ex(Type!=EN_DNT_Unknown, L"in GetInt() Type is Unknown");

	switch(Type){
		case EN_DNT_Bool	:{ IntVal = (int)BoolVal; }break;
		case EN_DNT_Float	:{ IntVal = (int)FloatVal; }break;
		case EN_DNT_String	:{ IntVal = WStrToInt(StringVal); }break;
		case EN_DNT_Other	:{ IntVal = WStrToInt(StringVal); }break;
		default:{}break;
	}

	Type = EN_DNT_Int;

	return IntVal;
}
///----------------------------------------------------------------------------------------------//
float TDataNode::GetFloat()
{
	TExceptionGenerator Ex(L"TDataNode: ");
	Ex(Type!=EN_DNT_Unknown, L"in GetFloat() Type is Unknown");

	switch(Type){
		case EN_DNT_Int		:{ FloatVal = (float)IntVal; }break;
		case EN_DNT_String	:{ FloatVal = WStrToFloat(StringVal); }break;
		case EN_DNT_Other	:{ FloatVal = WStrToFloat(StringVal); }break;
		default:{}break;
	}

	Type = EN_DNT_Float;

	return FloatVal;
}
///----------------------------------------------------------------------------------------------//
wstring TDataNode::GetString()
{
	TExceptionGenerator Ex(L"TDataNode: ");
	Ex(Type!=EN_DNT_Unknown, L"in GetString() Type is Unknown");

	switch(Type){
		case EN_DNT_Int		:{ StringVal = IntToWStr(IntVal); }break;
		case EN_DNT_Float	:{ StringVal = FloatToWStr(FloatVal); }break;
		default:{}break;
	}

	Type = EN_DNT_String;

	return StringVal;
}
///----------------------------------------------------------------------------------------------//
bool TDataNode::GetBoolVal() const
{
	return BoolVal;
}
///----------------------------------------------------------------------------------------------//
int TDataNode::GetIntVal() const
{
	return IntVal;
}
///----------------------------------------------------------------------------------------------//
float TDataNode::GetFloatVal() const
{
	return FloatVal;
}
///----------------------------------------------------------------------------------------------//
wstring TDataNode::GetStringVal() const
{
	return StringVal;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::SetParent(TDataNode *Val)
{
	Parent = Val;
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::GetParent()
{
	return Parent;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::UnSetParent()
{
	Parent = NULL;
}
///----------------------------------------------------------------------------------------------//
int TDataNode::GetSize() const
{
	return Children.size();
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Add(const bool &ChildVal, const wstring &ChildName)
{
	pTDataNode Child(new TDataNode(ChildVal,ChildName));

	Child->SetParent(this);
	Children.push_back(Child);

	return Child;
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Add(const int &ChildVal, const wstring &ChildName)
{
	pTDataNode Child(new TDataNode(ChildVal,ChildName));

	Child->SetParent(this);
	Children.push_back(Child);

	return Child;
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Add(const float &ChildVal, const wstring &ChildName)
{
	pTDataNode Child(new TDataNode(ChildVal,ChildName));

	Child->SetParent(this);
	Children.push_back(Child);

	return Child;
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Add(const wchar_t* ChildVal, const wstring &ChildName)
{
	pTDataNode Child(new TDataNode(ChildVal,ChildName));

	Child->SetParent(this);
	Children.push_back(Child);

	return Child;
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Add(const wstring &ChildVal, const wstring &ChildName)
{
	pTDataNode Child(new TDataNode(ChildVal,ChildName));

	Child->SetParent(this);
	Children.push_back(Child);

	return Child;
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Add(TDataNode* Child)
{
	Child->SetParent(this);
	Children.push_back(Child);

	return Child;
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Get(const int &Index)
{
	TExceptionGenerator Ex(L"TDataNode: ");

	int Size(Children.size());
	Ex(Size > 0,L"in Get(Index) list is enpty");
	Ex(Index >= 0 && Index < Size,L"in Get(Index) Index out of range");

	return Children[Index];
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Get(const wstring &ChildName)
{
	TExceptionGenerator Ex(L"TDataNode: ");

	int Size(Children.size());
	Ex(Size > 0,L"in Get(ChildName) list is enpty");

	for(int i=0;i<Size;i++){
		if(Children[i]->GetName() == ChildName){
			return Children[i];
		}
	}

	Ex(L"in Get(ChildName) child node whis name: "+ChildName+L" not exist");
	return NULL;
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Get(const int &Index) const
{
	TExceptionGenerator Ex(L"TDataNode: ");

	int Size(Children.size());
	Ex(Size > 0,L"in Get(Index)const list is enpty");
	Ex(Index >= 0 && Index < Size,L"in Get(Index)const Index out of range");

	return Children[Index];
}
///----------------------------------------------------------------------------------------------//
TDataNode* TDataNode::Get(const wstring &ChildName) const
{
	TExceptionGenerator Ex(L"TDataNode: ");

	int Size(Children.size());
	Ex(Size > 0,L"in Get(ChildName)const list is enpty");

	for(int i=0;i<Size;i++){
		if(Children[i]->GetName() == ChildName){
			return Children[i];
		}
	}

	Ex(L"in Get(ChildName)const child node whis name: " + ChildName + L" not exist");
	return NULL;
}
///----------------------------------------------------------------------------------------------//
bool TDataNode::IsExist(const wstring &ChildName)
{
	int Size(Children.size());

	for(int i=0;i<Size;i++){ if(Children[i]->GetName() == ChildName){ return true; } }

	return false;
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Del(const int &Index)
{
	TExceptionGenerator Ex(L"TDataNode: ");

	int Size(Children.size());
	Ex(Size > 0,L"in Del(Index) list is enpty");
	Ex(Index >= 0 && Index < Size,L"in Del(Index) Index out of range");

	delete Children[Index];
	Children.erase(Children.begin()+Index);
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Del(TDataNode* Child)
{
	TExceptionGenerator Ex(L"TDataNode: ");

	int Size(Children.size());
	Ex(Size > 0,L"in Del(Child) list is enpty");
	Ex(Child != NULL,L"in Del(Child) Child is NULL");

	unsigned long ObjID = Child->GetID();

	int DeleteId = -1;
	for(int i=0;i<Size;i++){
		if(Children[i]->GetID() == ObjID){
			DeleteId = i;
			break;
		}
	}

	Ex(DeleteId!=-1,L"in Del(Child) object not exist");

	delete Children[DeleteId];
	Children.erase(Children.begin()+DeleteId);
}
///----------------------------------------------------------------------------------------------//
unsigned long TDataNode::GetChildID(const int &Index) const
{
	return Get(Index)->GetID();
}
///----------------------------------------------------------------------------------------------//
unsigned long TDataNode::GetChildID(const wstring &ChildName) const
{
	return Get(ChildName)->GetID();
}
///----------------------------------------------------------------------------------------------//
enDataNodeType TDataNode::GetChildType(const int &Index) const
{
	return Get(Index)->GetType();
}
///----------------------------------------------------------------------------------------------//
enDataNodeType TDataNode::GetChildType(const wstring &ChildName) const
{
	return Get(ChildName)->GetType();
}
///----------------------------------------------------------------------------------------------//
bool TDataNode::GetChildBool(const int &Index)
{
	return Get(Index)->GetBool();
}
///----------------------------------------------------------------------------------------------//
int TDataNode::GetChildInt(const int &Index)
{
	return Get(Index)->GetInt();
}
///----------------------------------------------------------------------------------------------//
float TDataNode::GetChildFloat(const int &Index)
{
	return Get(Index)->GetFloat();
}
///----------------------------------------------------------------------------------------------//
wstring TDataNode::GetChildString(const int &Index)
{
	return Get(Index)->GetString();
}
///----------------------------------------------------------------------------------------------//
bool TDataNode::GetChildBool(const wstring &ChildName)
{
	return Get(ChildName)->GetBool();
}
///----------------------------------------------------------------------------------------------//
int TDataNode::GetChildInt(const wstring &ChildName)
{
	return Get(ChildName)->GetInt();
}
///----------------------------------------------------------------------------------------------//
float TDataNode::GetChildFloat(const wstring &ChildName)
{
	return Get(ChildName)->GetFloat();
}
///----------------------------------------------------------------------------------------------//
wstring TDataNode::GetChildString(const wstring &ChildName)
{
	return Get(ChildName)->GetString();
}
///----------------------------------------------------------------------------------------------//
bool TDataNode::GetChildBoolVal(const int &Index) const
{
	return Get(Index)->GetBoolVal();
}
///----------------------------------------------------------------------------------------------//
int TDataNode::GetChildIntVal(const int &Index) const
{
	return Get(Index)->GetIntVal();
}
///----------------------------------------------------------------------------------------------//
float TDataNode::GetChildFloatVal(const int &Index) const
{
	return Get(Index)->GetFloatVal();
}
///----------------------------------------------------------------------------------------------//
wstring TDataNode::GetChildStringVal(const int &Index) const
{
	return Get(Index)->GetStringVal();
}
///----------------------------------------------------------------------------------------------//
bool TDataNode::GetChildBoolVal(const wstring &ChildName) const
{
	return Get(ChildName)->GetBoolVal();
}
///----------------------------------------------------------------------------------------------//
int TDataNode::GetChildIntVal(const wstring &ChildName) const
{
	return Get(ChildName)->GetIntVal();
}
///----------------------------------------------------------------------------------------------//
float TDataNode::GetChildFloatVal(const wstring &ChildName) const
{
	return Get(ChildName)->GetFloatVal();
}
///----------------------------------------------------------------------------------------------//
wstring TDataNode::GetChildStringVal(const wstring &ChildName) const
{
	return Get(ChildName)->GetStringVal();
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Replace(const bool &BaseVal, const bool &NewVal)
{
	int Size(Children.size());

	for(int i=0;i<Size;i++){
		pTDataNode Child(Children[i]);

		if(Child->GetType() == EN_DNT_Bool && Child->GetBoolVal() == BaseVal){
			Child->SetVal(NewVal);
		}

		Child->Replace(BaseVal, NewVal);
	}
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Replace(const int &BaseVal, const int &NewVal)
{
	int Size(Children.size());

	for(int i=0;i<Size;i++){
		pTDataNode Child(Children[i]);

		if(Child->GetType() == EN_DNT_Int && Child->GetIntVal() == BaseVal){
			Child->SetVal(NewVal);
		}

		Child->Replace(BaseVal, NewVal);
	}
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Replace(const float &BaseVal, const float &NewVal)
{
	int Size(Children.size());

	for(int i=0;i<Size;i++){
		pTDataNode Child(Children[i]);

		if(Child->GetType() == EN_DNT_Float){
			float ChildVal(Child->GetFloatVal());
			if( Abs(Abs(ChildVal) - Abs(NewVal)) < Eps6){ Child->SetVal(NewVal); }
		}

		Child->Replace(BaseVal, NewVal);
	}
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Replace(const wstring &BaseVal, const wstring &NewVal)
{
	int Size(Children.size());

	for(int i=0;i<Size;i++){
		pTDataNode Child(Children[i]);

		if(Child->GetType() == EN_DNT_String && Child->GetStringVal() == BaseVal){
			Child->SetVal(NewVal);
		}

		Child->Replace(BaseVal, NewVal);
	}
}
///----------------------------------------------------------------------------------------------//
void TDataNode::Clear()
{
	int Size(Children.size());

	for(int i=0;i<Size;i++){ delete Children[i]; }

	Children.clear();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
