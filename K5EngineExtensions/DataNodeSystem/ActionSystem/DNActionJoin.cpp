#include "DNActionJoin.h"
#include "../../LibTools/ObjectSearcher.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNActionJoin::TDNActionJoin():TDNBaseAction(),Sprites(NULL), Texts(NULL), Actions(NULL),
										SpriteList(NULL), TextList(NULL), ActionList(NULL)
{
	Exception.SetPrefix(L"TDNActionJoin: ");
}
///----------------------------------------------------------------------------------------------//
TDNActionJoin::~TDNActionJoin()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTDataNode TDNActionJoin::InitNodePointRecord(const pTPoint &CheckPoint)
{
	pTDataNode Node(new TDataNode);
	TObjectSearcher Searcher;

	if(SpriteList != NULL || Sprites != NULL){
		pTSpriteList SelectedList(SpriteList);

		if(Sprites != NULL){ SelectedList = Searcher.SpriteListByPos(Sprites, CheckPoint); }

		Exception(SelectedList!=NULL, L"in InitNodePointRecord(...) SpriteList is NULL");

		pTSprite Object(Searcher.SpriteByPos(SelectedList, CheckPoint));
		if(Object != NULL){
			Node->SetName(L"Sprite");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	if(TextList != NULL || Texts != NULL){
		pTTextList SelectedList(TextList);

		if(Texts != NULL){ SelectedList = Searcher.TextListByPos(Texts, CheckPoint); }

		pTText Object(Searcher.TextByPos(SelectedList, CheckPoint));
		if(Object != NULL){
			Node->SetName(L"Text");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	if(ActionList != NULL || Actions != NULL){
		pTActionList SelectedList(ActionList);

		if(Actions != NULL){ SelectedList = Searcher.ActionListByJoinPos(Actions, CheckPoint); }

		pTBaseAction Object(Searcher.ActionJoinByPos(SelectedList,CheckPoint));
		if(Object != NULL){
			Node->SetName(L"Action");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object       ->GetName(), L"name");
			return Node;
		}
	}

	Exception(L"in InitNodePointRecord(...) Object not exist");
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNActionJoin::InitNodeAngleRecord(const pTValue &CheckAngle)
{
	pTDataNode Node(new TDataNode);
	TObjectSearcher Searcher;

	if(SpriteList != NULL || Sprites != NULL){
		pTSpriteList SelectedList(SpriteList);

		if(Sprites != NULL){ SelectedList = Searcher.SpriteListByAngle(Sprites, CheckAngle); }

		Exception(SelectedList!=NULL, L"in InitNodePointRecord(...) SpriteList is NULL");

		pTSprite Object(Searcher.SpriteByAngle(SelectedList, CheckAngle));
		if(Object != NULL){
			Node->SetName(L"Sprite");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	if(TextList != NULL || Texts != NULL){
		pTTextList SelectedList(TextList);

		if(Texts != NULL){ SelectedList = Searcher.TextListByAngle(Texts, CheckAngle); }

		pTText Object(Searcher.TextByAngle(SelectedList, CheckAngle));
		if(Object != NULL){
			Node->SetName(L"Text");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	if(ActionList != NULL || Actions != NULL){
		pTActionList SelectedList(ActionList);

		if(Actions != NULL){ SelectedList = Searcher.ActionListByJoinAngle(Actions, CheckAngle); }

		pTBaseAction Object(Searcher.ActionJoinByAngle(SelectedList,CheckAngle));
		if(Object != NULL){
			Node->SetName(L"Action");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	Exception(L"in InitNodePointRecord(...) Object not exist");
	return NULL;
}
///----------------------------------------------------------------------------------------------//
void TDNActionJoin::InitNodeObjectRecord(const pTDataNode &Node, const pTPoint &CheckPoint,
										 const pTValue &CheckAngle, const wstring &AttrNamePref)
{
	if(CheckPoint == NULL && CheckAngle == NULL){return;}

	if(CheckPoint != NULL){
		pTDataNode DNParrPos(Node->Add(new TDataNode));
		DNParrPos->SetName(AttrNamePref + L"Pos");
		DNParrPos->Add(InitNodePointRecord(CheckPoint));
	}

	if(CheckAngle != NULL){
		pTDataNode DNParrAngle(Node->Add(new TDataNode));
		DNParrAngle->SetName(AttrNamePref + L"Angle");
		DNParrAngle->Add(InitNodeAngleRecord(CheckAngle));
	}
}
///----------------------------------------------------------------------------------------------//
pTSprite TDNActionJoin::SelectSpriteFromNode(const pTDataNode &Node)
{
	if(!Node->IsExist(L"Sprite")){return NULL;}
	pTDataNode DNSprite(Node->Get(L"Sprite"));

	TDNEngineDataConverter DC;
	pTSprite Obj(NULL);

	if(SpriteList != NULL ){ Obj = DC.GetSprite(SpriteList, DNSprite); }
	if(Sprites    != NULL) { Obj = DC.GetSprite(Sprites   , DNSprite); }

	Exception(Obj != NULL, L"in SelectSpriteFromNode(...) sprite not found, check node");

	return Obj;
}
///----------------------------------------------------------------------------------------------//
pTText TDNActionJoin::SelectTextFromNode  (const pTDataNode &Node)
{
	if(!Node->IsExist(L"Text")){return NULL;}
	pTDataNode DNText(Node->Get(L"Text"));

	TDNEngineDataConverter DC;
	pTText Obj(NULL);

	if(TextList != NULL ){ Obj = DC.GetText(TextList, DNText); }
	if(Texts    != NULL ){ Obj = DC.GetText(Texts   , DNText); }

	Exception(Obj != NULL, L"in SelectTextFromNode(...) text not found, check node");

	return Obj;
}
///----------------------------------------------------------------------------------------------//
pTActionJoin TDNActionJoin::SelectJoinFromNode  (const pTDataNode &Node)
{
	if(!Node->IsExist(L"Action")){return NULL;}
	pTDataNode DNAction(Node->Get(L"Action"));

	TDNEngineDataConverter DC;
	pTBaseAction Obj(NULL);

	if(ActionList != NULL ){ Obj = DC.GetAction(ActionList, DNAction); }
	if(Actions    != NULL ){ Obj = DC.GetAction(Actions   , DNAction); }

	Exception(Obj != NULL, L"in SelectJoinFromNode(...) join not found, check node");

	Exception(typeid(*Obj).name() == typeid(TActionJoin).name(),
			  L"in SelectJoinFromNode(...) Action record tor correct, "
			  L"action type not TActionJoin");

	return static_cast<pTActionJoin>(Obj);
}
///----------------------------------------------------------------------------------------------//
pTPoint TDNActionJoin::SelectPointFromNode(const pTDataNode &Node, const wstring &ChildNodeName)
{
	if(!Node->IsExist(ChildNodeName)){return NULL;}

	pTDataNode DNVal(Node->Get(ChildNodeName));

	pTSprite Sprite(SelectSpriteFromNode(DNVal));
	if(Sprite != NULL){ return &Sprite->Pos; }

	pTText Text(SelectTextFromNode(DNVal));
	if(Text   != NULL){ return &Text->Pos; }

	pTActionJoin Join(SelectJoinFromNode(DNVal));
	if(Join   != NULL){ return &Join->Pos; }

	Exception(L"in SelectPointFromNode(...) object not found");
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTValue  TDNActionJoin::SelectAngleFromNode(const pTDataNode &Node, const wstring &ChildNodeName)
{
	if(!Node->IsExist(ChildNodeName)){return NULL;}

	pTDataNode DNVal(Node->Get(ChildNodeName));

	pTSprite Sprite(SelectSpriteFromNode(DNVal));
	if(Sprite != NULL){ return &Sprite->Angle; }

	pTText Text(SelectTextFromNode(DNVal));
	if(Text != NULL)  { return &Text->Angle; }

	pTActionJoin Join(SelectJoinFromNode(DNVal));
	if(Join != NULL)  { return &Join->Angle; }

	Exception(L"in SelectAngleFromNode(...) object not found");
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNActionJoin::ConstructAction()
{
	return new TActionJoin;
}
///----------------------------------------------------------------------------------------------//
bool TDNActionJoin::CheckType(const pTBaseAction &Action)
{
	return typeid(*Action).name() == typeid(TActionJoin).name();
}
///----------------------------------------------------------------------------------------------//
bool TDNActionJoin::CheckType(const pTDataNode &Node)
{
	return Node->GetChildString(L"type") == L"Join";
}
///----------------------------------------------------------------------------------------------//
void TDNActionJoin::Init(const pTBaseAction &Action, const pTDataNode &Node)
{
	Exception(CheckType(Action), L"in Init(Action,Node) wrong Action type");

	pTActionJoin Obj(static_cast<pTActionJoin>(Action));

	Obj->SetParrPos   ( SelectPointFromNode(Node, L"ParrPos"  ) );
	Obj->SetParrAngle ( SelectAngleFromNode(Node, L"ParrAngle") );

	Obj->SetChildPos  ( SelectPointFromNode(Node, L"ChildPos"  ) );
	Obj->SetChildAngle( SelectAngleFromNode(Node, L"ChildAngle") );

	SetActionDefParams(Action,Node);
}
///----------------------------------------------------------------------------------------------//
void TDNActionJoin::Init(const pTDataNode &Node, const pTBaseAction &Action)
{
	Exception(CheckType(Action),  L"in Init(Node,Action) wrong Action type");

	pTActionJoin Obj(static_cast<pTActionJoin>(Action));

	Node->Add(L"Join", L"type");
	SetNodeDefParams(Node, Action);

	InitNodeObjectRecord(Node, Obj->GetParrPos() , Obj->GetParrAngle() , L"Parr" );
	InitNodeObjectRecord(Node, Obj->GetChildPos(), Obj->GetChildAngle(), L"Child" );

	if(Obj->Pos.GetX()  != 0.0f){ Node->Add( Obj->Pos.GetX(), L"x"); }
	if(Obj->Pos.GetY()  != 0.0f){ Node->Add( Obj->Pos.GetY(), L"y"); }
	if(Obj->Pos.GetZ()  != 0.0f){ Node->Add( Obj->Pos.GetZ(), L"z"); }

	if(Obj->Angle.Get() != 0.0f){ Node->Add( Obj->Angle.Get(), L"angle"); }
}
///----------------------------------------------------------------------------------------------//
void TDNActionJoin::Set(const pTSpriteListManager &SpritesVal, const pTTextListManager &TextsVal,
						const pTActionListManager &ActionsVal)
{
	Sprites = SpritesVal;
	Texts   = TextsVal;
	Actions = ActionsVal;
}
///----------------------------------------------------------------------------------------------//
void TDNActionJoin::Set(const pTSpriteList &SpriteListVal, const pTTextList &TextListVal,
				 		const pTActionList &ActionListVal)
{
	SpriteList = SpriteListVal;
	TextList   = TextListVal;
	ActionList = ActionListVal;
}
///----------------------------------------------------------------------------------------------//
pTSpriteListManager TDNActionJoin::GetSprites()
{
	return Sprites;
}
///----------------------------------------------------------------------------------------------//
pTTextListManager TDNActionJoin::GetTexts()
{
	return Texts;
}
///----------------------------------------------------------------------------------------------//
pTActionListManager TDNActionJoin::GetActions()
{
	return Actions;
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TDNActionJoin::GetSpriteList()
{
	return SpriteList;
}
///----------------------------------------------------------------------------------------------//
pTTextList TDNActionJoin::GetTextList()
{
	return TextList;
}
///----------------------------------------------------------------------------------------------//
pTActionList TDNActionJoin::GetActionList()
{
	return ActionList;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
