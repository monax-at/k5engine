///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNACTIONCHANGEVIEW_H_INCLUDED
#define DNACTIONCHANGEVIEW_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../ActionSystem/ExtActionSystem.h"
#include "DNBaseAction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNActionChangeView:public TDNBaseAction
{
	protected:
		pTSpriteListManager Sprites;
		pTTextListManager   Texts;

		pTSpriteList SpriteList;
		pTTextList   TextList;
	protected:
		pTDataNode InitNodeObjectRecord(const pTBaseGraphicObject &Object);
	public:
		TDNActionChangeView();
		TDNActionChangeView(const pTSpriteListManager &SpritesVal,
							const pTTextListManager &TextsVal);
		TDNActionChangeView(const pTSpriteList     &SpriteListVal,
							const pTTextList &TextListVal);
		virtual ~TDNActionChangeView();

		pTBaseAction ConstructAction();

		bool CheckType(const pTBaseAction &Action);
		bool CheckType(const pTDataNode   &Node);

		void Init(const pTBaseAction &Action, const pTDataNode   &Node);
		void Init(const pTDataNode   &Node  , const pTBaseAction &Action);

		void Set(const pTSpriteListManager &SpritesVal, const pTTextListManager &TextsVal);
		void Set(const pTSpriteList     &SpriteListVal, const pTTextList &TextListVal);

		pTSpriteListManager GetSprites();
		pTTextListManager   GetTexts  ();

		pTSpriteList GetSpriteList();
		pTTextList   GetTextList  ();
};

typedef TDNActionChangeView* pTDNActionChangeView;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNACTIONCHANGEVIEW_H_INCLUDED
