///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNACTIONSPRITETEXTUREANIMATION_H_INCLUDED
#define DNACTIONSPRITETEXTUREANIMATION_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../ActionSystem/ExtActionSystem.h"
#include "DNBaseAction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNActionSpriteTextureAnimation:public TDNBaseAction
{
	protected:
		pTSpriteListManager  Sprites;
		pTSpriteList         SpriteList;

		pTTextureListManager Textures;
		pTTextureList        TextureList;
	public:
		TDNActionSpriteTextureAnimation();
		virtual ~TDNActionSpriteTextureAnimation();

		pTBaseAction ConstructAction();

		bool CheckType(const pTBaseAction &Action);
		bool CheckType(const pTDataNode   &Node);

		void Init(const pTBaseAction &Action, const pTDataNode   &Node);
		void Init(const pTDataNode   &Node  , const pTBaseAction &Action);

		void SetSprites(const pTSpriteListManager &SpritesVal);
		void SetSprites(const pTSpriteList &SpriteListVal);

		void SetTextures(const pTTextureListManager &TexturesVal);
		void SetTextures(const pTTextureList &TextureListVal);

		pTSpriteListManager  GetSprites();
		pTSpriteList         GetSpriteList();

		pTTextureListManager GetTextures();
		pTTextureList        GetTextureList();
};

typedef TDNActionSpriteTextureAnimation* pTDNActionSpriteTextureAnimation;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNACTIONSPRITETEXTUREANIMATION_H_INCLUDED
