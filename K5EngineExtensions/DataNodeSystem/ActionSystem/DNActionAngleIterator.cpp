#include "DNActionAngleIterator.h"
#include "../../LibTools/ObjectSearcher.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNActionAngleIterator::TDNActionAngleIterator():TDNBaseAction(),Sprites(NULL), Texts(NULL),
							Actions(NULL), SpriteList(NULL), TextList(NULL), ActionList(NULL)
{
	Exception.SetPrefix(L"TDNActionAngleIterator: ");
}
///----------------------------------------------------------------------------------------------//
TDNActionAngleIterator::~TDNActionAngleIterator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTDataNode TDNActionAngleIterator::InitNodeAngleRecord(const pTValue &CheckAngle)
{
	TObjectSearcher Searcher;

	if(SpriteList != NULL || Sprites != NULL){
		pTSpriteList SelectedList(SpriteList);

		if(Sprites != NULL){ SelectedList = Searcher.SpriteListByAngle(Sprites, CheckAngle); }

		if(SelectedList != NULL){
			pTSprite Object(Searcher.SpriteByAngle(SelectedList, CheckAngle));

			pTDataNode Node(new TDataNode);
			Node->SetName(L"Sprite");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;

		}
	}

	if(TextList != NULL || Texts != NULL){
		pTTextList SelectedList(TextList);

		if(Texts != NULL){ SelectedList = Searcher.TextListByAngle(Texts, CheckAngle); }

		if(SelectedList != NULL){
			pTText Object(Searcher.TextByAngle(SelectedList, CheckAngle));

			pTDataNode Node(new TDataNode);
			Node->SetName(L"Text");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	if(ActionList != NULL || Actions != NULL){
		pTActionList SelectedList(ActionList);

		if(Actions != NULL){ SelectedList = Searcher.ActionListByJoinAngle(Actions, CheckAngle); }

		if(SelectedList != NULL){
			pTBaseAction Object(Searcher.ActionJoinByAngle(SelectedList,CheckAngle));

			pTDataNode Node(new TDataNode);
			Node->SetName(L"Action");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTSprite TDNActionAngleIterator::SelectSpriteFromNode(const pTDataNode &Node)
{
	if(!Node->IsExist(L"Sprite")){return NULL;}
	pTDataNode DNSprite(Node->Get(L"Sprite"));

	TDNEngineDataConverter DC;
	pTSprite Obj(NULL);

	if(SpriteList != NULL ){ Obj = DC.GetSprite(SpriteList, DNSprite); }
	if(Sprites    != NULL ){ Obj = DC.GetSprite(Sprites   , DNSprite); }

	Exception(Obj != NULL, L"in SelectSpriteFromNode(...) sprite not found, check node");

	return Obj;
}
///----------------------------------------------------------------------------------------------//
pTText TDNActionAngleIterator::SelectTextFromNode  (const pTDataNode &Node)
{
	if(!Node->IsExist(L"Text")){return NULL;}
	pTDataNode DNText(Node->Get(L"Text"));

	TDNEngineDataConverter DC;

	pTText Obj(NULL);
	if(TextList != NULL ){ Obj = DC.GetText(TextList, DNText); }
	if(Texts    != NULL ){ Obj = DC.GetText(Texts   , DNText); }

	Exception(Obj != NULL, L"in SelectTextFromNode(...) text not found, check node");

	return Obj;
}
///----------------------------------------------------------------------------------------------//
pTActionJoin TDNActionAngleIterator::SelectJoinFromNode  (const pTDataNode &Node)
{
	if(!Node->IsExist(L"Action")){return NULL;}
	pTDataNode DNAction(Node->Get(L"Action"));

	TDNEngineDataConverter DC;
	pTBaseAction Obj(NULL);

	if(ActionList != NULL ){ Obj = DC.GetAction(ActionList, DNAction); }
	if(Actions    != NULL ){ Obj = DC.GetAction(Actions   , DNAction); }

	Exception(Obj != NULL, L"in SelectJoinFromNode(...) join not found, check node");

	Exception(typeid(*Obj).name() == typeid(TActionJoin).name(),
			  L"in SelectJoinFromNode(...) Action record tor correct, "
			  L"action type not TActionJoin");

	return static_cast<pTActionJoin>(Obj);
}
///----------------------------------------------------------------------------------------------//
pTValue TDNActionAngleIterator::SelectAngleFromNode(const pTDataNode &Node)
{
	pTSprite Sprite(SelectSpriteFromNode(Node));
	if(Sprite != NULL){ return &Sprite->Angle; }

	pTText Text(SelectTextFromNode(Node));
	if(Text != NULL)  { return &Text->Angle; }

	pTActionJoin Join(SelectJoinFromNode(Node));
	if(Join != NULL)  { return &Join->Angle; }

	Exception(L"in SelectAngleFromNode(...) object not found");
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNActionAngleIterator::ConstructAction()
{
	return new TActionAngleIterator;
}
///----------------------------------------------------------------------------------------------//
bool TDNActionAngleIterator::CheckType(const pTBaseAction &Action)
{
	return typeid(*Action).name() == typeid(TActionAngleIterator).name();
}
///----------------------------------------------------------------------------------------------//
bool TDNActionAngleIterator::CheckType(const pTDataNode &Node)
{
	return Node->GetChildString(L"type") == L"AngleIterator";
}
///----------------------------------------------------------------------------------------------//
void TDNActionAngleIterator::Init(const pTBaseAction &Action, const pTDataNode &Node)
{
	Exception(CheckType(Action), L"in Init(Action,Node) wrong Action type");

	pTActionAngleIterator Obj(static_cast<pTActionAngleIterator>(Action));

	Obj->Set(SelectAngleFromNode(Node));

	// далее выбирается режим работы екщена
	bool Angle(Node->IsExist(L"angle"));
	bool Dist (Node->IsExist(L"dist"));
	bool Time (Node->IsExist(L"time"));
	bool Speed(Node->IsExist(L"speed"));

	if(Angle && Time) {Obj->UseAngleAndTime( Node->GetChildFloat(L"angle"),
											 Node->GetChildFloat(L"time")); }

	if(Angle && Speed){Obj->UseAngleAndSpeed(Node->GetChildFloat(L"angle"),
											 Node->GetChildFloat(L"speed")); }

	if(Dist && Time)  {Obj->UseAngleAndTime( Node->GetChildFloat(L"dist"),
											 Node->GetChildFloat(L"time")); }

	if(Dist && Speed) {Obj->UseAngleAndSpeed(Node->GetChildFloat(L"dist"),
											 Node->GetChildFloat(L"speed")); }

	// настройка дополнительных параметров
	if(Node->IsExist(L"cyclic_speed") && Node->GetChildBool(L"cyclic_speed")){
		Obj->UseCyclicBySpeed(Node->GetChildFloat(L"speed"));
	}

	if(Node->IsExist(L"cyclic_time") && Node->GetChildBool(L"cyclic_time")){
		Obj->UseCyclicByTime( Node->GetChildFloat(L"angle"), Node->GetChildFloat(L"time") );
	}

	if(Node->IsExist(L"finish_on_stop")){
		Obj->SetFinishOnStop(Node->GetChildBool(L"finish_on_stop"));
	}

	if(Node->IsExist(L"delay")){ Obj->SetDelay(Node->GetChildFloat(L"delay")); }

	SetActionDefParams(Action,Node);
}
///----------------------------------------------------------------------------------------------//
void TDNActionAngleIterator::Init(const pTDataNode &Node, const pTBaseAction &Action)
{
	Exception(CheckType(Action),  L"in Init(Node,Action) wrong Action type");

	pTActionAngleIterator Obj(static_cast<pTActionAngleIterator>(Action));

	Node->Add(L"AngleIterator", L"type");
	SetNodeDefParams(Node, Action);

	pTDataNode AngleRecNode(InitNodeAngleRecord(Obj->Get()));
	if(AngleRecNode != NULL){Node->Add(AngleRecNode);}

	switch(Obj->GetInitMode()){
		case EN_AAIIM_AngleAndSpeed:{
			Node->Add(Obj->GetNewAngle(), L"angle");
			Node->Add(Obj->GetSpeed()   , L"speed");
		} break;

		case EN_AAIIM_AngleAndTime:{
			Node->Add(Obj->GetNewAngle(), L"angle");
			Node->Add(Obj->GetTime()    , L"time");
		} break;

		case EN_AAIIM_DistAndSpeed:{
			Node->Add(Obj->GetDistanceTmp(), L"dist");
			Node->Add(Obj->GetSpeed()      , L"speed");
		} break;

		case EN_AAIIM_DistAndTime:{
			Node->Add(Obj->GetDistanceTmp(), L"dist");
			Node->Add(Obj->GetTime()       , L"time");
		} break;

		case EN_AAIIM_CyclicBySpeed:{
			Node->Add(Obj->GetSpeed()*Obj->GetDir(), L"speed");
		} break;

		case EN_AAIIM_CyclicByTime:{
			Node->Add(Obj->GetDistanceTmp(), L"dist");
			Node->Add(Obj->GetTime()       , L"time");
		} break;

		default:break;
	}

	if(Obj->IsCyclicBySpeed())  { Node->Add(true, L"cyclic_speed"); }
	if(Obj->IsCyclicByTime())   { Node->Add(true, L"cyclic_time"); }

	if(Obj->GetFinishOnStop()){ Node->Add(true, L"finish_on_stop"); }
	if(Obj->GetUseDelay())    { Node->Add(Obj->GetDelay(), L"delay"); }
}
///----------------------------------------------------------------------------------------------//
void TDNActionAngleIterator::Set(const pTSpriteListManager &SpritesVal,
								 const pTTextListManager   &TextsVal,
								 const pTActionListManager &ActionsVal)
{
	Sprites = SpritesVal;
	Texts   = TextsVal;
	Actions = ActionsVal;
}
///----------------------------------------------------------------------------------------------//
void TDNActionAngleIterator::Set(const pTSpriteList &SpriteListVal, const pTTextList &TextListVal,
								 const pTActionList &ActionListVal)
{
	SpriteList = SpriteListVal;
	TextList   = TextListVal;
	ActionList = ActionListVal;
}
///----------------------------------------------------------------------------------------------//
pTSpriteListManager TDNActionAngleIterator::GetSprites()
{
	return Sprites;
}
///----------------------------------------------------------------------------------------------//
pTTextListManager TDNActionAngleIterator::GetTexts()
{
	return Texts;
}
///----------------------------------------------------------------------------------------------//
pTActionListManager TDNActionAngleIterator::GetActions()
{
	return Actions;
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TDNActionAngleIterator::GetSpriteList()
{
	return SpriteList;
}
///----------------------------------------------------------------------------------------------//
pTTextList TDNActionAngleIterator::GetTextList()
{
	return TextList;
}
///----------------------------------------------------------------------------------------------//
pTActionList TDNActionAngleIterator::GetActionList()
{
	return ActionList;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
