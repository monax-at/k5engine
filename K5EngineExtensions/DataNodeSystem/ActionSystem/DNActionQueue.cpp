#include "DNActionQueue.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNActionQueue::TDNActionQueue():TDNBaseAction(),DNActionList(NULL)
{
	Exception.SetPrefix(L"TDNActionQueue: ");
}
///----------------------------------------------------------------------------------------------//
TDNActionQueue::~TDNActionQueue()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNActionQueue::ConstructAction()
{
	return new TActionQueue;
}
///----------------------------------------------------------------------------------------------//
bool TDNActionQueue::CheckType(const pTBaseAction &Action)
{
	return typeid(*Action).name() == typeid(TActionQueue).name();
}
///----------------------------------------------------------------------------------------------//
bool TDNActionQueue::CheckType(const pTDataNode &Node)
{
	return Node->GetChildString(L"type") == L"Queue";
}
///----------------------------------------------------------------------------------------------//
void TDNActionQueue::Init(const pTBaseAction &Action, const pTDataNode &Node)
{
	Exception(CheckType(Action), L"in Init(Action,Node) wrong Action type");

	pTActionQueue Obj(static_cast<pTActionQueue>(Action));

	if(DNActionList != NULL){ DNActionList->Run(Obj->GetActionsPtr(),Node); }

	if(Node->IsExist(L"cyclic")){ Obj->SetCyclic(Node->GetChildBool(L"cyclic")); }

	SetActionDefParams(Action,Node);
}
///----------------------------------------------------------------------------------------------//
void TDNActionQueue::Init(const pTDataNode &Node, const pTBaseAction &Action)
{
	Exception(CheckType(Action),  L"in Init(Node,Action) wrong Action type");

	pTActionQueue Obj(static_cast<pTActionQueue>(Action));

	Node->Add(L"Queue", L"type");
	SetNodeDefParams(Node, Action);

	if(DNActionList != NULL){ DNActionList->Run(Node, Obj->GetActionsPtr()); }

	if(Obj->GetCyclic()){ Node->Add(Obj->GetCyclic(),L"cyclic"); }
}
///----------------------------------------------------------------------------------------------//
void TDNActionQueue::Set(const pTDNActionList &DNActionListVal)
{
	DNActionList = DNActionListVal;
}
///----------------------------------------------------------------------------------------------//
pTDNActionList TDNActionQueue::Get()
{
	return DNActionList;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
