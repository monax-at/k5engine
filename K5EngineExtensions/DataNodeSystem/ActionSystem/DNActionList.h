///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNACTIONLIST_H_INCLUDED
#define DNACTIONLIST_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "DNBaseAction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNActionList
{
	protected:
		TExceptionGenerator Exception;
		vector<pTDNBaseAction> Elems;
	public:
		TDNActionList();
		~TDNActionList();

		pTBaseAction Create(const pTDataNode &Node);
		void Init(const pTBaseAction &Action, const pTDataNode &Node);

		pTDataNode Create(const pTBaseAction &Action);
		void Init(const pTDataNode &Node,const pTBaseAction &Action);

		void CreateActions	(const pTActionList &Actions, const pTDataNode &Node);
		void InitActions	(const pTActionList &Actions, const pTDataNode &Node);

		void CreateNodes	(const pTDataNode &Node, const pTActionList &Actions);
		void InitNodes		(const pTDataNode &Node, const pTActionList &Actions);

		pTActionList Run(const pTDataNode   &Node);
		pTDataNode   Run(const pTActionList &Actions);

		void Run(const pTActionList &Actions, const pTDataNode &Node);
		void Run(const pTDataNode &Node     , const pTActionList &Actions);

		void Add(const pTDNBaseAction &Action);
		void Clear();
};

typedef TDNActionList* pTDNActionList;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNACTIONLIST_H_INCLUDED
