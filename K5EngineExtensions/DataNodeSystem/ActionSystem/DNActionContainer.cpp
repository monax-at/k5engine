#include "DNActionContainer.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNActionContainer::TDNActionContainer():TDNBaseAction(),DNActionList(NULL)
{
	Exception.SetPrefix(L"TDNActionContainer: ");
}
///----------------------------------------------------------------------------------------------//
TDNActionContainer::~TDNActionContainer()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNActionContainer::ConstructAction()
{
	return new TActionContainer;
}
///----------------------------------------------------------------------------------------------//
bool TDNActionContainer::CheckType(const pTBaseAction &Action)
{
	return typeid(*Action).name() == typeid(TActionContainer).name();
}
///----------------------------------------------------------------------------------------------//
bool TDNActionContainer::CheckType(const pTDataNode &Node)
{
	return Node->GetChildString(L"type") == L"Container";
}
///----------------------------------------------------------------------------------------------//
void TDNActionContainer::Init(const pTBaseAction &Action, const pTDataNode &Node)
{
	Exception(CheckType(Action), L"in Init(Action,Node) wrong Action type");

	pTActionContainer Obj(static_cast<pTActionContainer>(Action));

	if(DNActionList != NULL){ DNActionList->Run(Obj->GetActionsPtr(),Node); }

	SetActionDefParams(Action,Node);
}
///----------------------------------------------------------------------------------------------//
void TDNActionContainer::Init(const pTDataNode &Node, const pTBaseAction &Action)
{
	Exception(CheckType(Action),  L"in Init(Node,Action) wrong Action type");

	pTActionContainer Obj(static_cast<pTActionContainer>(Action));

	Node->Add(L"Container", L"type");
	SetNodeDefParams(Node, Action);

	if(DNActionList != NULL){ DNActionList->Run(Node, Obj->GetActionsPtr()); }
}
///----------------------------------------------------------------------------------------------//
void TDNActionContainer::Set(const pTDNActionList &DNActionListVal)
{
	DNActionList = DNActionListVal;
}
///----------------------------------------------------------------------------------------------//
pTDNActionList TDNActionContainer::Get()
{
	return DNActionList;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
