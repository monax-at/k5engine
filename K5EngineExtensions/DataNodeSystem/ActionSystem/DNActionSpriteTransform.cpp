#include "DNActionSpriteTransform.h"
#include "../../LibTools/ObjectSearcher.h"
#include "../GraphicSystem/DNSprite.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNActionSpriteTransform::TDNActionSpriteTransform():TDNBaseAction(),
							Sprites(NULL), SpriteList(NULL), Textures(NULL)
{
	Exception.SetPrefix(L"TDNActionSpriteTransform: ");
}
///----------------------------------------------------------------------------------------------//
TDNActionSpriteTransform::~TDNActionSpriteTransform()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNActionSpriteTransform::ConstructAction()
{
	return new TActionSpriteTransform;
}
///----------------------------------------------------------------------------------------------//
bool TDNActionSpriteTransform::CheckType(const pTBaseAction &Action)
{
	return typeid(*Action).name() == typeid(TActionSpriteTransform).name();
}
///----------------------------------------------------------------------------------------------//
bool TDNActionSpriteTransform::CheckType(const pTDataNode   &Node)
{
	return Node->GetChildString(L"type") == L"SpriteTransform";
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTransform::Init(const pTBaseAction &Action, const pTDataNode &Node)
{
	Exception(CheckType(Action), L"in Init(Action,Node) wrong Action type");

	pTActionSpriteTransform Obj(static_cast<pTActionSpriteTransform>(Action));

	if(Node->IsExist(L"time")){ Obj->SetTime(Node->GetChildFloat(L"time")); }

	// установка спрайта, к которому будет применяться трансформация
	if(Node->IsExist(L"Sprite")){
		pTDataNode DNSprite(Node->Get(L"Sprite"));
		pTSpriteList TmpList(SpriteList);
		if(Sprites != NULL && DNSprite->IsExist(L"list")){
			TmpList = Sprites->Get(DNSprite->GetChildString(L"list"));
		}

		if(TmpList != NULL && DNSprite->IsExist(L"name")){
			Obj->Set(TmpList->Get(DNSprite->GetChildString(L"name")));
		}
	}

	// установка шаблона трансфорации
	if(Node->IsExist(L"Template")){
		pTDataNode DNTemplate(Node->Get(L"Template"));

		TDNSprite DNSprite;
		DNSprite.SetTextures(Textures);
		if(SpriteList != NULL){ DNSprite.SetDevice(SpriteList->GetDevice()); }
		if(Sprites    != NULL){ DNSprite.SetDevice(Sprites   ->GetDevice()); }

		TSprite Template;
		DNSprite.Init(&Template, DNTemplate);
		Obj->SetTemplate(Template);
	}

	// настройка флагов трансформации
	if(Node->IsExist(L"all"))    { Obj->SetChangeAll    (Node->GetChildBool(L"all")); }
	if(Node->IsExist(L"view"))   { Obj->SetChangeView   (Node->GetChildBool(L"view")); }
	if(Node->IsExist(L"pos"))    { Obj->SetChangePos    (Node->GetChildBool(L"pos")); }
	if(Node->IsExist(L"center")) { Obj->SetChangeCenter (Node->GetChildBool(L"center")); }
	if(Node->IsExist(L"angle"))  { Obj->SetChangeAngle  (Node->GetChildBool(L"angle")); }
	if(Node->IsExist(L"texture")){ Obj->SetChangeTexture(Node->GetChildBool(L"texture")); }
	if(Node->IsExist(L"size"))   { Obj->SetChangeSize   (Node->GetChildBool(L"size")); }
	if(Node->IsExist(L"mesh"))   { Obj->SetChangeMesh   (Node->GetChildBool(L"mesh")); }
	if(Node->IsExist(L"color"))  { Obj->SetChangeColor  (Node->GetChildBool(L"color")); }

	SetActionDefParams(Action,Node);
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTransform::Init(const pTDataNode &Node, const pTBaseAction &Action)
{
	Exception(CheckType(Action),  L"in Init(Node,Action) wrong Action type");

	pTActionSpriteTransform Obj(static_cast<pTActionSpriteTransform>(Action));

	Node->Add(L"SpriteTransform", L"type");
	SetNodeDefParams(Node, Action);

	pTSprite Sprite(Obj->Get());
	if(Sprite != NULL){
		pTDataNode DNSprite(Node->Add(new TDataNode));
		DNSprite->SetName(L"Sprite");

		pTSpriteList TmpList(SpriteList);
		if(Sprites != NULL){ TmpList = Sprites->GetByGraphicObjectId(Sprite->GetID()); }

		DNSprite->Add(TmpList->GetName(), L"list");
		DNSprite->Add(Sprite ->GetName(), L"name");
	}

	TDNSprite DNSprite;
	DNSprite.SetTextures(Textures);
	if(SpriteList != NULL){ DNSprite.SetDevice(SpriteList->GetDevice()); }
	if(Sprites    != NULL){ DNSprite.SetDevice(Sprites   ->GetDevice()); }

	pTDataNode DNTemplate(Node->Add(new TDataNode));
	DNTemplate->SetName(L"Template");

	TSprite Template(Obj->GetTemplate());
	DNSprite.Init(DNTemplate, &Template);

	if(Obj->GetTime() > 0.0f){ Node->Add(Obj->GetTime(), L"time"); }

	// настройка флагов трансформации
	if(Obj->GetChangeAll())    { Node->Add(true, L"all")    ;}
	if(Obj->GetChangeView())   { Node->Add(true, L"view")   ;}
	if(Obj->GetChangePos())    { Node->Add(true, L"pos")    ;}
	if(Obj->GetChangeCenter()) { Node->Add(true, L"center") ;}
	if(Obj->GetChangeAngle())  { Node->Add(true, L"angle")  ;}
	if(Obj->GetChangeTexture()){ Node->Add(true, L"texture");}
	if(Obj->GetChangeSize())   { Node->Add(true, L"size")   ;}
	if(Obj->GetChangeMesh())   { Node->Add(true, L"mesh")   ;}
	if(Obj->GetChangeColor())  { Node->Add(true, L"color")  ;}
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTransform::Set(const pTSpriteListManager &Val)
{
	Sprites = Val;
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTransform::Set(const pTSpriteList &Val)
{
	SpriteList = Val;
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTransform::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}
///----------------------------------------------------------------------------------------------//
pTSpriteListManager TDNActionSpriteTransform::GetSprites()
{
	return Sprites;
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TDNActionSpriteTransform::GetSpriteList()
{
	return SpriteList;
}
///----------------------------------------------------------------------------------------------//
pTTextureListManager TDNActionSpriteTransform::GetTextures()
{
	return Textures;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
