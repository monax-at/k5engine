#include "DNActionList.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNActionList::TDNActionList():Exception(L"TDNActionList: ")
{
}
///----------------------------------------------------------------------------------------------//
TDNActionList::~TDNActionList()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNActionList::Create(const pTDataNode &Node)
{
	int Size(Elems.size());

	for(int i=0;i<Size;i++){
		pTDNBaseAction DNAction(Elems[i]);
		if(DNAction->CheckType(Node)){
			pTBaseAction Action(DNAction->ConstructAction());
			if(Node->IsExist(L"name")){Action->SetName(Node->GetChildString(L"name"));}
			return Action;
		}
	}

	wstring DNType(Node->GetChildString(L"type"));
	Exception(L"in CreateActionFromNode(...) unknown action=node type:" + DNType);

	return NULL;
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::Init(const pTBaseAction &Action, const pTDataNode &Node)
{
	int Size(Elems.size());

	for(int i=0;i<Size;i++){
		pTDNBaseAction DNAction(Elems[i]);
		if(DNAction->CheckType(Node)){
			DNAction->Init(Action,Node);
			return;
		}
	}

	wstring DNType(Node->GetChildString(L"type"));
	Exception(L"in Init(Action,Node) unknown action node type:" + DNType);
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNActionList::Create(const pTBaseAction &Action)
{
	pTDataNode Node(new TDataNode);
	Node->SetName(L"Action");

	return Node;
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::Init(const pTDataNode &Node, const pTBaseAction &Action)
{
	int Size(Elems.size());

	for(int i=0;i<Size;i++){
		pTDNBaseAction DNAction(Elems[i]);
		if(DNAction->CheckType(Action)){
			DNAction->Init(Node,Action);
			return;
		}
	}

	wstring AType(StrToWStr(typeid(*Action).name()));
	Exception(L"in Init(Node,Action) unknown action type:" + AType);
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::CreateActions(const pTActionList &Actions, const pTDataNode &Node)
{
	int Size(Node->GetSize());
	for(int i=0;i<Size;i++){
		pTDataNode ChildNode(Node->Get(i));
		if(ChildNode->GetName() == L"Action"){
			Actions->Add(Create(ChildNode));
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::InitActions(const pTActionList &Actions, const pTDataNode &Node)
{
	int ActionsIter(0);
	int Size(Node->GetSize());
	for(int i=0;i<Size;i++){
		pTDataNode ChildNode(Node->Get(i));
		if(ChildNode->GetName() == L"Action"){
			Init(Actions->Get(ActionsIter),ChildNode);
			ActionsIter++;
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::CreateNodes(const pTDataNode &Node, const pTActionList &Actions)
{
	int Size(Actions->GetSize());
	for(int i=0;i<Size;i++){ Node->Add(Create(Actions->Get(i))); }
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::InitNodes(const pTDataNode &Node,const pTActionList &Actions)
{
	int ActionsIter(0);
	int Size(Node->GetSize());
	for(int i=0;i<Size;i++){
		pTDataNode ChildNode(Node->Get(i));
		if(ChildNode->GetName() == L"Action"){
			Init(ChildNode, Actions->Get(ActionsIter));
			ActionsIter++;
		}
	}
}
///----------------------------------------------------------------------------------------------//
pTActionList TDNActionList::Run(const pTDataNode &Node)
{
	pTActionList List(new TActionList);

	if(Node->IsExist(L"name"))	{ List->SetName	 (Node->GetChildString(L"name"));}
	if(Node->IsExist(L"active")){ List->SetActive(Node->GetChildBool(L"active"));}

	CreateActions(List, Node);
	InitActions  (List, Node);

	return List;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNActionList::Run(const pTActionList &Actions)
{
	pTDataNode Node(new TDataNode);

	Node->SetName(L"List");

	if( Actions->GetName().length() >0){ Node->Add(Actions->GetName()  , L"name"); }
	if(!Actions->GetActive())          { Node->Add(Actions->GetActive(), L"active");}

	CreateNodes(Node, Actions);
	InitNodes  (Node, Actions);

	return Node;
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::Run(const pTActionList &Actions, const pTDataNode &Node)
{
	CreateActions(Actions, Node);
	InitActions  (Actions, Node);
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::Run(const pTDataNode &Node,const pTActionList &Actions)
{
	CreateNodes(Node, Actions);
	InitNodes  (Node, Actions);
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::Add(const pTDNBaseAction &Action)
{
	Elems.push_back(Action);
}
///----------------------------------------------------------------------------------------------//
void TDNActionList::Clear()
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){delete Elems[i];}
	Elems.clear();
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

