///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNACTIONSPRITETRANSFORM_H_INCLUDED
#define DNACTIONSPRITETRANSFORM_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../ActionSystem/ExtActionSystem.h"
#include "DNBaseAction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNActionSpriteTransform:public TDNBaseAction
{
	protected:
		pTSpriteListManager  Sprites;
		pTSpriteList         SpriteList;

		pTTextureListManager Textures;
	public:
		TDNActionSpriteTransform();
		virtual ~TDNActionSpriteTransform();

		pTBaseAction ConstructAction();

		bool CheckType(const pTBaseAction &Action);
		bool CheckType(const pTDataNode   &Node);

		void Init(const pTBaseAction &Action, const pTDataNode   &Node);
		void Init(const pTDataNode   &Node  , const pTBaseAction &Action);

		void Set(const pTSpriteListManager &Val);
		void Set(const pTSpriteList        &Val);

		void SetTextures(const pTTextureListManager &Val);

		pTSpriteListManager GetSprites();
		pTSpriteList        GetSpriteList();

		pTTextureListManager GetTextures();
};

typedef TDNActionSpriteTransform* pTDNActionSpriteTransform;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNACTIONSPRITETRANSFORM_H_INCLUDED
