///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNBASETACTION_H_INCLUDED
#define DNBASETACTION_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../Base/DNEngineDataConverter.h"
#include <typeinfo>
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNBaseAction
{
	protected:
		TExceptionGenerator Exception;
		bool                UseDefParams;
	protected:
		void SetNodeDefParams  (const pTDataNode   &Node  , const pTBaseAction &Action);
		void SetActionDefParams(const pTBaseAction &Action, const pTDataNode   &Node);
	public:
		TDNBaseAction();
		virtual ~TDNBaseAction();

		virtual pTBaseAction ConstructAction() = 0;

		virtual bool CheckType(const pTBaseAction &Action) = 0;
		virtual bool CheckType(const pTDataNode   &Node)   = 0;

		virtual void Init(const pTBaseAction &Action, const pTDataNode   &Node)   = 0;
		virtual void Init(const pTDataNode   &Node  , const pTBaseAction &Action) = 0;

		pTBaseAction Run(const pTDataNode   &Node);
		pTDataNode   Run(const pTBaseAction &Action);

		void SetUseDefParams(const bool &Val);
		bool GetUseDefParams() const;
};

typedef TDNBaseAction* pTDNBaseAction;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNBASEACTION_H_INCLUDED
