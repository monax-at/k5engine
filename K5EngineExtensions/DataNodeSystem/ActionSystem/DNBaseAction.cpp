#include "DNBaseAction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNBaseAction::TDNBaseAction():Exception(L"TDNBaseAction: "),UseDefParams(true)
{
}
///----------------------------------------------------------------------------------------------//
TDNBaseAction::~TDNBaseAction()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TDNBaseAction::SetNodeDefParams(const pTDataNode &Node, const pTBaseAction &Action)
{
	if(!UseDefParams){return;}

	if(Node->GetName().length() == 0){Node->SetName(L"Action");}

	if(Action->GetName().length()>0){ Node->Add(Action->GetName()  , L"name"); }
	Node->Add(Action->GetActive(), L"active");
}
///----------------------------------------------------------------------------------------------//
void TDNBaseAction::SetActionDefParams(const pTBaseAction &Action, const pTDataNode &Node)
{
	if(!UseDefParams){return;}

	if(Node->IsExist(L"name")){ Action->SetName(Node->GetChildString(L"name")); }

	if(Node->IsExist(L"active")){
		if(Node->GetChildBool(L"active")){ Action->Start(); }
		else							 { Action->SetActive(false); }
	}
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNBaseAction::Run(const pTDataNode   &Node)
{
	try{
		pTBaseAction Action(ConstructAction());

		Init(Action,Node);

		return Action;
	}
	catch(TException &Ex){
		Exception(L"in Run(Node): " + Ex.GetMess());
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTDataNode TDNBaseAction::Run(const pTBaseAction &Action)
{
	Exception(Action!=NULL,L"in Run(Action) Action is NULL");

	try{
		pTDataNode Node(new TDataNode);

		Init(Node, Action);

		return Node;
	}
	catch(TException &Ex){
		Exception(L"in Run(Node): " + Ex.GetMess());
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
void TDNBaseAction::SetUseDefParams(const bool &Val)
{
	UseDefParams = Val;
}
///----------------------------------------------------------------------------------------------//
bool TDNBaseAction::GetUseDefParams() const
{
	return UseDefParams;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

