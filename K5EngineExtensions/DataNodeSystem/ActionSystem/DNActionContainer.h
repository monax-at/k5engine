///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNACTIONCONTAINER_H_INCLUDED
#define DNACTIONCONTAINER_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../ActionSystem/ExtActionSystem.h"
#include "DNActionList.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNActionContainer:public TDNBaseAction
{
	protected:
		pTDNActionList DNActionList;
	public:
		TDNActionContainer();
		virtual ~TDNActionContainer();

		pTBaseAction ConstructAction();

		bool CheckType(const pTBaseAction &Action);
		bool CheckType(const pTDataNode   &Node);

		void Init(const pTBaseAction &Action, const pTDataNode   &Node);
		void Init(const pTDataNode   &Node  , const pTBaseAction &Action);

		void Set(const pTDNActionList &DNActionListVal);
		pTDNActionList Get();
};

typedef TDNActionContainer* pTDNActionContainer;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNACTIONCONTAINER_H_INCLUDED
