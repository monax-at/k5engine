#include "DNActionChangeView.h"
#include "../../LibTools/ObjectSearcher.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNActionChangeView::TDNActionChangeView():TDNBaseAction(),Sprites(NULL), Texts(NULL),
														SpriteList(NULL), TextList(NULL)
{
	Exception.SetPrefix(L"TDNActionChangeView: ");
}
///----------------------------------------------------------------------------------------------//
TDNActionChangeView::TDNActionChangeView(const pTSpriteListManager &SpritesVal,
										 const pTTextListManager &TextsVal): TDNBaseAction(),
					Sprites(SpritesVal), Texts(TextsVal), SpriteList(NULL), TextList(NULL)
{
}
///----------------------------------------------------------------------------------------------//
TDNActionChangeView::TDNActionChangeView(const pTSpriteList &SpriteListVal,
										 const pTTextList &TextListVal): TDNBaseAction(),
					Sprites(NULL), Texts(NULL), SpriteList(SpriteListVal), TextList(TextListVal)
{
}
///----------------------------------------------------------------------------------------------//
TDNActionChangeView::~TDNActionChangeView()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTDataNode TDNActionChangeView::InitNodeObjectRecord(const pTBaseGraphicObject &Object)
{
	TObjectSearcher Searcher;

	if(SpriteList != NULL || Sprites != NULL){
		pTSpriteList SelectedList(SpriteList);

		if(Sprites != NULL){ SelectedList = Searcher.SpriteListByObject(Sprites, Object); }

		if(SelectedList != NULL){
			pTDataNode Node(new TDataNode);
			Node->SetName(L"Sprite");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	if(TextList != NULL || Texts != NULL){
		pTTextList SelectedList(TextList);

		if(Texts != NULL){ SelectedList = Searcher.TextListByObject(Texts, Object); }

		if(SelectedList != NULL){
			pTDataNode Node(new TDataNode);
			Node->SetName(L"Text");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNActionChangeView::ConstructAction()
{
	return new TActionChangeView;
}
///----------------------------------------------------------------------------------------------//
bool TDNActionChangeView::CheckType(const pTBaseAction &Action)
{
	return typeid(*Action).name() == typeid(TActionChangeView).name();
}
///----------------------------------------------------------------------------------------------//
bool TDNActionChangeView::CheckType(const pTDataNode &Node)
{
	return Node->GetChildString(L"type") == L"ChangeView";
}
///----------------------------------------------------------------------------------------------//
void TDNActionChangeView::Init(const pTBaseAction &Action, const pTDataNode &Node)
{
	Exception(CheckType(Action), L"in Init(Action,Node) wrong Action type");

	pTActionChangeView Obj(static_cast<pTActionChangeView>(Action));

	if(Node->IsExist(L"view")){ Obj->SetFlag(Node->GetChildBool (L"view")); }
	if(Node->IsExist(L"time")){ Obj->SetTime(Node->GetChildFloat(L"time")); }
	if(Node->IsExist(L"mode")){ Obj->SetMode(Node->GetChildBool(L"mode"));  }

	int NodeSize(Node->GetSize());
	for(int i=0;i<NodeSize;i++){
		pTDataNode DNObject(Node->Get(i));
		if(DNObject->GetName() == L"Sprite"){
			pTSpriteList List(SpriteList);
			if(Sprites != NULL && DNObject->IsExist(L"list")){
				List = Sprites->Get(DNObject->GetChildString(L"list"));
			}

			if(List != NULL && DNObject->IsExist(L"name")){
				pTBaseGraphicObject Object(List->Get(DNObject->GetChildString(L"name")));

				if(!DNObject->IsExist(L"view")){ Obj->Add(Object); }
				else{ Obj->Add(Object, DNObject->GetChildBool(L"view")); }
			}
		}

		if(DNObject->GetName() == L"Text"){
			pTTextList List(TextList);
			if(Texts != NULL && DNObject->IsExist(L"list")){
				List = Texts->Get(DNObject->GetChildString(L"list"));
			}

			if(List != NULL && DNObject->IsExist(L"name")){
				pTBaseGraphicObject Object(List->Get(DNObject->GetChildString(L"name")));

				if(!DNObject->IsExist(L"view")){ Obj->Add(Object); }
				else{ Obj->Add(Object, DNObject->GetChildBool(L"view")); }
			}
		}
	}

	SetActionDefParams(Action, Node);
}
///----------------------------------------------------------------------------------------------//
void TDNActionChangeView::Init(const pTDataNode &Node, const pTBaseAction &Action)
{
	Exception(CheckType(Action),  L"in Init(Node,Action) wrong Action type");

	pTActionChangeView Obj(static_cast<pTActionChangeView>(Action));

	Node->Add(L"ChangeView", L"type");
	SetNodeDefParams(Node, Action);

	if(Obj->GetFlag())       { Node->Add(Obj->GetFlag(), L"view"); }
	if(Obj->GetMode())       { Node->Add(Obj->GetMode(), L"mode"); }
	if(Obj->GetTime() > 0.0f){ Node->Add(Obj->GetTime(), L"time"); }

	const map <pTBaseGraphicObject, bool* > *GraphicObjects(Obj->GetObjectsPtr());

	for (map <pTBaseGraphicObject, bool* >::const_iterator Itr = GraphicObjects->begin();
	     Itr != GraphicObjects->end(); Itr++ )
	{
		pTDataNode DNObject(InitNodeObjectRecord(Itr->first));
		if(DNObject != NULL){
			Node->Add(DNObject);
			if(Itr->second != NULL){ DNObject->Add(*(Itr->second), L"view"); }
		}
	}
}
///----------------------------------------------------------------------------------------------//
void TDNActionChangeView::Set(const pTSpriteListManager &SpritesVal,
							  const pTTextListManager &TextsVal)
{
	Sprites = SpritesVal;
	Texts   = TextsVal;
}
///----------------------------------------------------------------------------------------------//
void TDNActionChangeView::Set(const pTSpriteList &SpriteListVal, const pTTextList &TextListVal)
{
	SpriteList = SpriteListVal;
	TextList   = TextListVal;
}
///----------------------------------------------------------------------------------------------//
pTSpriteListManager TDNActionChangeView::GetSprites()
{
	return Sprites;
}
///----------------------------------------------------------------------------------------------//
pTTextListManager TDNActionChangeView::GetTexts()
{
	return Texts;
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TDNActionChangeView::GetSpriteList()
{
	return SpriteList;
}
///----------------------------------------------------------------------------------------------//
pTTextList TDNActionChangeView::GetTextList()
{
	return TextList;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
