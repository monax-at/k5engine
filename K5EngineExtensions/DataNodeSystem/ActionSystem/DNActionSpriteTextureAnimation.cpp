#include "DNActionSpriteTextureAnimation.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNActionSpriteTextureAnimation::TDNActionSpriteTextureAnimation():TDNBaseAction(),
								Sprites(NULL), SpriteList(NULL), Textures(NULL), TextureList(NULL)
{
	Exception.SetPrefix(L"TDNActionSpriteTextureAnimation: ");
}
///----------------------------------------------------------------------------------------------//
TDNActionSpriteTextureAnimation::~TDNActionSpriteTextureAnimation()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNActionSpriteTextureAnimation::ConstructAction()
{
	return new TActionSpriteTextureAnimation;
}
///----------------------------------------------------------------------------------------------//
bool TDNActionSpriteTextureAnimation::CheckType(const pTBaseAction &Action)
{
	return typeid(*Action).name() == typeid(TActionSpriteTextureAnimation).name();
}
///----------------------------------------------------------------------------------------------//
bool TDNActionSpriteTextureAnimation::CheckType(const pTDataNode &Node)
{
	return Node->GetChildString(L"type") == L"SpriteTextureAnimation";
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTextureAnimation::Init(const pTBaseAction &Action, const pTDataNode &Node)
{
	Exception(CheckType(Action), L"in Init(Action,Node) wrong Action type");

	pTActionSpriteTextureAnimation Obj(static_cast<pTActionSpriteTextureAnimation>(Action));

	// установка спрайта екшена
	if(SpriteList != NULL || Sprites != NULL){
		pTDataNode DNSprite(Node->Get(L"Sprite"));
		pTSpriteList TmpSpriteList(SpriteList);
		if(Sprites != NULL){ TmpSpriteList = Sprites->Get(DNSprite->GetChildString(L"list")); }
		Obj->Set(TmpSpriteList->Get(DNSprite->GetChildString(L"name")));
	}

	// инициализация параметров ректов текстуры
	if(Node->IsExist(L"Rects")){
		pTDataNode DNRects(Node->Get(L"Rects"));
		int DNRectsSize(DNRects->GetSize());
		for(int i=0;i<DNRectsSize;i++){
			pTDataNode DNRect(DNRects->Get(i));

			pTTexture Texture(NULL);
			float X(0.0f);
			float Y(0.0f);
			float Width (0.0f);
			float Height(0.0f);
			bool  ChangeSpriteSize(false);
			// получение текстуры
			if( DNRect->IsExist(L"list") || DNRect->IsExist(L"name") ){
				Exception( TextureList != NULL || Textures != NULL,
						   L"in Init(Action,Node) TextureList or Textures must be set" );

				pTTextureList TmpTextureList(TextureList);
				if( Textures != NULL ){
					TmpTextureList = Textures->Get(DNRect->GetChildString(L"list"));
				}

				Texture = TmpTextureList->Get(DNRect->GetChildString(L"name"));
			}

			// получение размеров
			if(DNRect->IsExist(L"x")){ X = DNRect->GetChildFloat(L"x"); }
			if(DNRect->IsExist(L"y")){ X = DNRect->GetChildFloat(L"y"); }
			if(DNRect->IsExist(L"width")) { Width  = DNRect->GetChildFloat(L"width") ; }
			if(DNRect->IsExist(L"height")){ Height = DNRect->GetChildFloat(L"height"); }
			if(DNRect->IsExist(L"change_sprite_size")){
				ChangeSpriteSize = DNRect->GetChildBool(L"change_sprite_size");
			}

			Obj->AddRect(Texture, X, Y, Width,Height, ChangeSpriteSize);
		}
	}

	// инициализация последовательности кадров
	if(Node->IsExist(L"Frames")){
		pTDataNode DNFrames(Node->Get(L"Frames"));
		int DNFramesSize(DNFrames->GetSize());
		for(int i=0; i<DNFramesSize; i++){ Obj->AddFrameId(DNFrames->Get(i)->GetChildInt(L"id")); }
	}

	// инициализация остальных параметров
	if(Node->IsExist(L"cyclic"))        { Obj->SetCyclic   (Node->GetChildBool(L"cyclic")); }
	if(Node->IsExist(L"frame_time"))    { Obj->SetFrameTime(Node->GetChildBool(L"frame_time")); }
	if(Node->IsExist(L"frame"))         { Obj->SetFrame    (Node->GetChildBool(L"frame")); }
	if(Node->IsExist(L"finish_on_stop")){
		Obj->SetFinishOnStop(Node->GetChildBool(L"finish_on_stop"));
	}

	SetActionDefParams(Action, Node);
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTextureAnimation::Init(const pTDataNode   &Node  , const pTBaseAction &Action)
{
	Exception(CheckType(Action),  L"in Init(Node,Action) wrong Action type");

	pTActionSpriteTextureAnimation Obj(static_cast<pTActionSpriteTextureAnimation>(Action));

	Node->Add(L"SpriteTextureAnimation", L"type");
	SetNodeDefParams(Node, Action);

	if(SpriteList != NULL || Sprites != NULL){
		pTSprite Sprite(Obj->Get());
		if(Sprite != NULL){
			pTDataNode DNSprite(Node->Add(new TDataNode));
			DNSprite->SetName(L"Sprite");

			pTSpriteList TmpSpriteList(SpriteList);
			if( Sprites != NULL ){ TmpSpriteList = Sprites->GetByGraphicObjectId(Sprite->GetID()); }

			DNSprite->Add(TmpSpriteList->GetName(), L"list");
			DNSprite->Add(Sprite->GetName()       , L"name");
		}
	}

	int RectsSize(Obj->GetRectsSize());
	if(RectsSize>0){
		pTDataNode DNRects(Node->Add(new TDataNode));
		DNRects->SetName(L"Rects");

		for(int i=0; i<RectsSize; i++){
			pTDataNode DNRect(DNRects->Add(new TDataNode));
			DNRect->SetName(L"Rect");

			pTTexture Texture(Obj->GetRectTexture(i));
			float X(Obj->GetRectX(i));
			float Y(Obj->GetRectY(i));
			float Width (Obj->GetRectWidth (i));
			float Height(Obj->GetRectHeight(i));
			bool  ChangeSpriteSize(Obj->GetRectChangeSpriteSizeFlag(i));

			if(Texture != NULL){
				pTTextureList TmpTextureList(TextureList);
				if( Textures!=NULL ){ TmpTextureList = Textures->GetListByTexture(Texture); }

				DNRect->Add(TmpTextureList->GetName(), L"list");
				DNRect->Add(Texture       ->GetName(), L"name");
			}

			if(X != 0.0f){ DNRect->Add(X, L"x"); }
			if(Y != 0.0f){ DNRect->Add(Y, L"y"); }
			if(Width  != 0.0f){ DNRect->Add(Width , L"width") ; }
			if(Height != 0.0f){ DNRect->Add(Height, L"height"); }
			if(ChangeSpriteSize){ DNRect->Add(ChangeSpriteSize, L"change_sprite_size"); }
		}
	}

	int FramesSize(Obj->GetFramesSize());
	if(FramesSize > 0){
		pTDataNode DNFrames(Node->Add(new TDataNode));
		DNFrames->SetName(L"Frames");

		for(int i=0; i<FramesSize; i++){
			pTDataNode DNFrame(DNFrames->Add(new TDataNode));
			DNFrame->SetName(L"Frame");
			DNFrame->Add(Obj->GetFrameId(i), L"id");
		}
	}

	// инициализация параметров
	if(Obj->GetCyclic())            { Node->Add(Obj->GetCyclic()      , L"cyclic")    ; }
	if(Obj->GetFrameTime() != 25.0f){ Node->Add(Obj->GetFrameTime()   , L"frame_time"); }
	if(Obj->GetFrame()     != 0)    { Node->Add(Obj->GetFrame()       , L"frame")     ; }
	if(Obj->GetFinishOnStop())      { Node->Add(Obj->GetFinishOnStop(), L"finish_on_stop"); }
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTextureAnimation::SetSprites(const pTSpriteListManager &SpritesVal)
{
	Sprites = SpritesVal;
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTextureAnimation::SetSprites(const pTSpriteList &SpriteListVal)
{
	SpriteList = SpriteListVal;
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTextureAnimation::SetTextures(const pTTextureListManager &TexturesVal)
{
	Textures = TexturesVal;
}
///----------------------------------------------------------------------------------------------//
void TDNActionSpriteTextureAnimation::SetTextures(const pTTextureList &TextureListVal)
{
	TextureList = TextureListVal;
}
///----------------------------------------------------------------------------------------------//
pTSpriteListManager  TDNActionSpriteTextureAnimation::GetSprites()
{
	return Sprites;
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TDNActionSpriteTextureAnimation::GetSpriteList()
{
	return SpriteList;
}
///----------------------------------------------------------------------------------------------//
pTTextureListManager TDNActionSpriteTextureAnimation::GetTextures()
{
	return Textures;
}
///----------------------------------------------------------------------------------------------//
pTTextureList TDNActionSpriteTextureAnimation::GetTextureList()
{
	return TextureList;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
