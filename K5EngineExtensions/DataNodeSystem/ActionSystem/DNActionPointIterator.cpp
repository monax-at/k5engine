#include "DNActionPointIterator.h"
#include "../../LibTools/ObjectSearcher.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNActionPointIterator::TDNActionPointIterator():TDNBaseAction(),Sprites(NULL), Texts(NULL),
							Actions(NULL), SpriteList(NULL), TextList(NULL), ActionList(NULL)
{
	Exception.SetPrefix(L"TDNActionPointIterator: ");
}
///----------------------------------------------------------------------------------------------//
TDNActionPointIterator::~TDNActionPointIterator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
pTDataNode TDNActionPointIterator::InitNodePointRecord(const pTPoint &CheckPoint)
{
	if(CheckPoint == NULL){return NULL;}

	TObjectSearcher Searcher;

	if(SpriteList != NULL || Sprites != NULL){
		pTSpriteList SelectedList(SpriteList);

		if(Sprites != NULL){ SelectedList = Searcher.SpriteListByPos(Sprites, CheckPoint); }

		if(SelectedList != NULL){
			pTSprite Object(Searcher.SpriteByPos(SelectedList, CheckPoint));

			pTDataNode Node(new TDataNode);
			Node->SetName(L"Sprite");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;

		}
	}

	if(TextList != NULL || Texts != NULL){
		pTTextList SelectedList(TextList);

		if(Texts != NULL){ SelectedList = Searcher.TextListByPos(Texts, CheckPoint); }

		if(SelectedList != NULL){
			pTText Object(Searcher.TextByPos(SelectedList, CheckPoint));

			pTDataNode Node(new TDataNode);
			Node->SetName(L"Text");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	if(ActionList != NULL || Actions != NULL){
		pTActionList SelectedList(ActionList);

		if(Actions != NULL){ SelectedList = Searcher.ActionListByJoinPos(Actions, CheckPoint); }

		if(SelectedList != NULL){
			pTBaseAction Object(Searcher.ActionJoinByPos(SelectedList,CheckPoint));

			pTDataNode Node(new TDataNode);
			Node->SetName(L"Action");
			Node->Add(SelectedList->GetName(), L"list");
			Node->Add(Object      ->GetName(), L"name");
			return Node;
		}
	}

	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTSprite TDNActionPointIterator::SelectSpriteFromNode(const pTDataNode &Node)
{
	if(!Node->IsExist(L"Sprite")){return NULL;}
	pTDataNode DNSprite(Node->Get(L"Sprite"));

	TDNEngineDataConverter DC;
	pTSprite Obj(NULL);

	if(SpriteList != NULL ){ Obj = DC.GetSprite(SpriteList, DNSprite); }
	if(Sprites    != NULL ){ Obj = DC.GetSprite(Sprites   , DNSprite); }

	Exception(Obj != NULL, L"in SelectSpriteFromNode(...) sprite not found, check node");

	return Obj;
}
///----------------------------------------------------------------------------------------------//
pTText TDNActionPointIterator::SelectTextFromNode(const pTDataNode &Node)
{
	if(!Node->IsExist(L"Text")){return NULL;}
	pTDataNode DNText(Node->Get(L"Text"));

	TDNEngineDataConverter DC;

	pTText Obj(NULL);
	if(TextList != NULL ){ Obj = DC.GetText(TextList, DNText); }
	if(Texts    != NULL ){ Obj = DC.GetText(Texts   , DNText); }

	Exception(Obj != NULL, L"in SelectTextFromNode(...) text not found, check node");

	return Obj;
}
///----------------------------------------------------------------------------------------------//
pTActionJoin TDNActionPointIterator::SelectJoinFromNode(const pTDataNode &Node)
{
	if(!Node->IsExist(L"Action")){return NULL;}
	pTDataNode DNAction(Node->Get(L"Action"));

	TDNEngineDataConverter DC;
	pTBaseAction Obj(NULL);

	if(ActionList != NULL ){ Obj = DC.GetAction(ActionList, DNAction); }
	if(Actions    != NULL ){ Obj = DC.GetAction(Actions   , DNAction); }

	Exception(Obj != NULL, L"in SelectJoinFromNode(...) join not found, check node");

	Exception(typeid(*Obj).name() == typeid(TActionJoin).name(),
			  L"in SelectJoinFromNode(...) Action record tor correct, "
			  L"action type not TActionJoin");

	return static_cast<pTActionJoin>(Obj);
}
///----------------------------------------------------------------------------------------------//
pTPoint TDNActionPointIterator::SelectPointFromNode(const pTDataNode &Node)
{
	pTSprite Sprite(SelectSpriteFromNode(Node));
	if(Sprite != NULL){ return &Sprite->Pos; }

	pTText Text(SelectTextFromNode(Node));
	if(Text != NULL)  { return &Text->Pos; }

	pTActionJoin Join(SelectJoinFromNode(Node));
	if(Join != NULL)  { return &Join->Pos; }

	Exception(L"in SelectPointFromNode(...) object not found");
	return NULL;
}
///----------------------------------------------------------------------------------------------//
pTBaseAction TDNActionPointIterator::ConstructAction()
{
	return new TActionPointIterator;
}
///----------------------------------------------------------------------------------------------//
bool TDNActionPointIterator::CheckType(const pTBaseAction &Action)
{
	return typeid(*Action).name() == typeid(TActionPointIterator).name();
}
///----------------------------------------------------------------------------------------------//
bool TDNActionPointIterator::CheckType(const pTDataNode &Node)
{
	return Node->GetChildString(L"type") == L"PointIterator";
}
///----------------------------------------------------------------------------------------------//
void TDNActionPointIterator::Init(const pTBaseAction &Action, const pTDataNode &Node)
{
	Exception(CheckType(Action), L"in Init(Action,Node) wrong Action type");

	pTActionPointIterator Obj(static_cast<pTActionPointIterator>(Action));

	Obj->Set(SelectPointFromNode(Node));

	// далее выбирается режим работы екщена
	bool Pos  (Node->IsExist(L"Pos"));
	bool Dist (Node->IsExist(L"Dist"));
	bool Time (Node->IsExist(L"time"));
	bool Speed(Node->IsExist(L"speed"));

	TDNEngineDataConverter DC;

	if(Pos && Time)  {Obj->UsePosAndTime  (DC.ToPoint(Node->Get(L"Pos")),
										   Node->GetChildFloat(L"time")); }

	if(Pos && Speed) {Obj->UsePosAndSpeed (DC.ToPoint(Node->Get(L"Pos")),
										    Node->GetChildFloat(L"speed")); }

	if(Dist && Time) {Obj->UseDistAndTime (DC.ToPoint(Node->Get(L"Dist")),
										   Node->GetChildFloat(L"time")); }

	if(Dist && Speed){Obj->UseDistAndSpeed(DC.ToPoint(Node->Get(L"Dist")),
										   Node->GetChildFloat(L"speed")); }

	// настройка дополнительных параметров
	if(Node->IsExist(L"finish_on_stop")){
		Obj->SetFinishOnStop(Node->GetChildBool(L"finish_on_stop"));
	}

	if(Node->IsExist(L"delay")){ Obj->SetDelay(Node->GetChildFloat(L"delay")); }

	if(Node->IsExist(L"use_z")){ Obj->SetUseZ (Node->GetChildBool(L"use_z")); }

	SetActionDefParams(Action, Node);
}
///----------------------------------------------------------------------------------------------//
void TDNActionPointIterator::Init(const pTDataNode &Node, const pTBaseAction &Action)
{
	Exception(CheckType(Action),  L"in Init(Node,Action) wrong Action type");

	pTActionPointIterator Obj(static_cast<pTActionPointIterator>(Action));

	Node->Add(L"PointIterator", L"type");
	SetNodeDefParams(Node, Action);

	pTDataNode PointRecNode(InitNodePointRecord(Obj->Get()));
	if(PointRecNode != NULL){Node->Add(PointRecNode);}

	TDNEngineDataConverter DC;

	switch(Obj->GetInitMode()){
		case EN_APIIM_PosAndSpeed:{
			Node->Add(DC.From(Obj->GetNewPos(), L"Pos"));
			Node->Add(Obj->GetSpeed()         , L"speed");
		} break;

		case EN_APIIM_PosAndTime:{
			Node->Add(DC.From(Obj->GetNewPos(), L"Pos"));
			Node->Add(Obj->GetTime()          , L"time");
		} break;

		case EN_APIIM_DistAndSpeed:{
			Node->Add(DC.From(Obj->GetDistDir(), L"Dist"));
			Node->Add(Obj->GetSpeed()          , L"speed");
		} break;

		case EN_APIIM_DistAndTime:{
			Node->Add(DC.From(Obj->GetDistDir(), L"Dist"));
			Node->Add(Obj->GetTime()           , L"time");
		} break;

		default:break;
	}

	if(Obj->GetFinishOnStop()){ Node->Add(true, L"finish_on_stop"); }
	if(Obj->GetUseDelay())    { Node->Add(Obj->GetDelay(), L"delay"); }
	if(Obj->GetUseZ())        { Node->Add(Obj->GetUseZ() , L"use_z"); }
}
///----------------------------------------------------------------------------------------------//
void TDNActionPointIterator::Set(const pTSpriteListManager &SpritesVal,
								   const pTTextListManager   &TextsVal,
								   const pTActionListManager &ActionsVal)
{
	Sprites = SpritesVal;
	Texts   = TextsVal;
	Actions = ActionsVal;
}
///----------------------------------------------------------------------------------------------//
void TDNActionPointIterator::Set(const pTSpriteList &SpriteListVal, const pTTextList &TextListVal,
								   const pTActionList &ActionListVal)
{
	SpriteList = SpriteListVal;
	TextList   = TextListVal;
	ActionList = ActionListVal;
}
///----------------------------------------------------------------------------------------------//
pTSpriteListManager TDNActionPointIterator::GetSprites()
{
	return Sprites;
}
///----------------------------------------------------------------------------------------------//
pTTextListManager TDNActionPointIterator::GetTexts()
{
	return Texts;
}
///----------------------------------------------------------------------------------------------//
pTActionListManager TDNActionPointIterator::GetActions()
{
	return Actions;
}
///----------------------------------------------------------------------------------------------//
pTSpriteList TDNActionPointIterator::GetSpriteList()
{
	return SpriteList;
}
///----------------------------------------------------------------------------------------------//
pTTextList TDNActionPointIterator::GetTextList()
{
	return TextList;
}
///----------------------------------------------------------------------------------------------//
pTActionList TDNActionPointIterator::GetActionList()
{
	return ActionList;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
