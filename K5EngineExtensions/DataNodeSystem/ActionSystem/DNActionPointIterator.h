///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNACTIONPOINTITERATOR_H_INCLUDED
#define DNACTIONPOINTITERATOR_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../ActionSystem/ExtActionSystem.h"
#include "DNBaseAction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNActionPointIterator:public TDNBaseAction
{
	protected:
		// если задан один или несколько из этих элементов (Sprites, Texts, Actions),
		// то будет произведён поиск по всем элементам в поисках угла и точки
		pTSpriteListManager Sprites;
		pTTextListManager   Texts;
		pTActionListManager Actions;

		// если задан один или несколько из этих элементов (SpriteList, TextList, ActionList),
		// то будет произведён поиск по всем элементам в поисках угла и точки
		// списки предпочтительней в поиске, чем менеджеры
		pTSpriteList SpriteList;
		pTTextList   TextList;
		pTActionList ActionList;
	protected:
		// код, инициализириующий ноду по параметрам из соединения
		inline pTDataNode InitNodePointRecord(const pTPoint &CheckPoint);

		pTSprite         SelectSpriteFromNode(const pTDataNode &Node);
		pTText           SelectTextFromNode  (const pTDataNode &Node);
		pTActionJoin     SelectJoinFromNode  (const pTDataNode &Node);

		inline pTPoint SelectPointFromNode(const pTDataNode &Node);
	public:
		TDNActionPointIterator();
		virtual ~TDNActionPointIterator();

		pTBaseAction ConstructAction();

		bool CheckType(const pTBaseAction &Action);
		bool CheckType(const pTDataNode   &Node);

		void Init(const pTBaseAction &Action, const pTDataNode   &Node);
		void Init(const pTDataNode   &Node  , const pTBaseAction &Action);

		void Set(const pTSpriteListManager &SpritesVal, const pTTextListManager &TextsVal,
				 const pTActionListManager &ActionsVal);

		void Set(const pTSpriteList &SpriteListVal, const pTTextList &TextListVal,
				 const pTActionList &ActionListVal);

		pTSpriteListManager GetSprites();
		pTTextListManager   GetTexts  ();
		pTActionListManager GetActions();

		pTSpriteList GetSpriteList();
		pTTextList   GetTextList  ();
		pTActionList GetActionList();
};

typedef TDNActionPointIterator* pTDNActionPointIterator;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNACTIONPOINT3DITERATOR_H_INCLUDED
