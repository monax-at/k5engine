///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef DNACTIONJOIN_H_INCLUDED
#define DNACTIONJOIN_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "../../ActionSystem/ExtActionSystem.h"
#include "DNBaseAction.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TDNActionJoin:public TDNBaseAction
{
	protected:
		// если задан один или несколько из этих элементов (Sprites, Texts, Actions),
		// то будет произведён поиск по всем элементам в поисках угла и точки
		pTSpriteListManager Sprites;
		pTTextListManager   Texts;
		pTActionListManager Actions;

		// если задан один или несколько из этих элементов (SpriteList, TextList, ActionList),
		// то будет произведён поиск по всем элементам в поисках угла и точки
		// списки предпочтительней в поиске, чем менеджеры
		pTSpriteList SpriteList;
		pTTextList   TextList;
		pTActionList ActionList;
	protected:
		// код, инициализириующий ноду по параметрам из соединения
		inline pTDataNode InitNodePointRecord(const pTPoint &CheckPoint);
		inline pTDataNode InitNodeAngleRecord(const pTValue &CheckAngle);
		inline void InitNodeObjectRecord(const pTDataNode &Node, const pTPoint &CheckPoint,
										 const pTValue &CheckAngle, const wstring &AttrNamePref);

		pTSprite         SelectSpriteFromNode(const pTDataNode &Node);
		pTText           SelectTextFromNode  (const pTDataNode &Node);
		pTActionJoin     SelectJoinFromNode  (const pTDataNode &Node);

		inline pTPoint SelectPointFromNode(const pTDataNode &Node, const wstring &ChildNodeName);
		inline pTValue   SelectAngleFromNode(const pTDataNode &Node, const wstring &ChildNodeName);
	public:
		TDNActionJoin();
		virtual ~TDNActionJoin();

		pTBaseAction ConstructAction();

		bool CheckType(const pTBaseAction &Action);
		bool CheckType(const pTDataNode   &Node);

		void Init(const pTBaseAction &Action, const pTDataNode   &Node);
		void Init(const pTDataNode   &Node  , const pTBaseAction &Action);

		void Set(const pTSpriteListManager &SpritesVal, const pTTextListManager &TextsVal,
				 const pTActionListManager &ActionsVal);

		void Set(const pTSpriteList &SpriteListVal, const pTTextList &TextListVal,
				 const pTActionList &ActionListVal);

		pTSpriteListManager GetSprites();
		pTTextListManager   GetTexts  ();
		pTActionListManager GetActions();

		pTSpriteList GetSpriteList();
		pTTextList   GetTextList  ();
		pTActionList GetActionList();
};

typedef TDNActionJoin* pTDNActionJoin;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // DNACTIONJOIN_H_INCLUDED
