#include "DNTextFile.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TDNTextFile::TDNTextFile():Exception(L"TDNTextFile: "),EncryptFlag(false)
{
}
///----------------------------------------------------------------------------------------------//
TDNTextFile::~TDNTextFile()
{
}
///----------------------------------------------------------------------------------------------//
int TDNTextFile::GetNumTabs(const string &Str)
{
	int NumTabs(0);
	int Size(Str.length());

	for(int i=0;i<Size;i++){
		if(Str[i] == '\t')	{ NumTabs++; }
		else				{ break; }
	}

	return NumTabs;
}
///----------------------------------------------------------------------------------------------//
inline pTDataNode TDNTextFile::CreateStrNode(const string &Str)
{
	pTDataNode Node(new TDataNode);

	int NumTabs(GetNumTabs(Str));

	Node->SetType(EN_DNT_Other);
	Node->SetVal(NumTabs);
	Node->SetVal(StrToWStr(Str.substr(NumTabs)));

	return Node;
}
///----------------------------------------------------------------------------------------------//
void TDNTextFile::InitNode(const pTDataNode &Node)
{
	if(Node->GetType() == EN_DNT_Other){
		wstring Str(Node->GetStringVal());
		size_t FoundPos(Str.find(L':'));
		Exception(FoundPos!=wstring::npos,L"in InitNode(...) after node name missed symbol ':' ");

		Node->SetType(EN_DNT_Unknown);
		Node->SetName(Str.substr(0,FoundPos));

		int StrLength(Str.length());
		if((int)FoundPos + 1 < StrLength){

			wstring ValStr(Str.substr(FoundPos + 1));

			int ValStrLength(ValStr.length());
			for(int i=0;i<ValStrLength;i++){
				if(ValStr[i] != L' ' && ValStr[i] != L'\t'){
					Node->Set(ValStr.substr(i));
					break;
				}
			}
		}
	}

	int Size(Node->GetSize());
	for(int i=0;i<Size;i++){
		InitNode(Node->Get(i));
	}
}
///----------------------------------------------------------------------------------------------//
void TDNTextFile::WriteNode(	ofstream &File,const int &TabShiftPos,
								const pTDataNode &Node)
{
	wstring TabsStr;
	for(int i=0;i<TabShiftPos;i++){
		TabsStr += L'\t';
	}

	wstring Str = TabsStr + Node->GetName() + L":";

	switch(Node->GetType()){
		case EN_DNT_Bool	:{ Str += L" " + BoolToWStr(Node->GetBoolVal()); 	}break;
		case EN_DNT_Int		:{ Str += L" " + IntToWStr(Node->GetIntVal()); 		}break;
		case EN_DNT_Float	:{ Str += L" " + FloatToWStr(Node->GetFloatVal()); 	}break;
		case EN_DNT_String	:{ Str += L" " + Node->GetStringVal(); }break;
		case EN_DNT_Other	:{ Str += L" " + Node->GetStringVal(); }break;
		default:{}break;
	}

	string cStr(WStrToStr(Str));
	File.write(cStr.c_str(),cStr.length());
	File << endl;

	int NodeSize(Node->GetSize());
	for(int i=0;i<NodeSize;i++){ WriteNode(File,TabShiftPos + 1, Node->Get(i)); }
}
///----------------------------------------------------------------------------------------------//
void TDNTextFile::Load(const wstring &FileName, const pTDataNode &Node)
{
	Exception(FileName.length()>0	, L"in Load(...) FileName not set");
	Exception(Node!=NULL			, L"in Load(...) Node not set");

	ifstream File;
	File.open(WStrToStr(FileName).c_str());
	Exception(File.is_open(), L"in Load(...) can`t open file:" + FileName);

	pTDataNode CurNode(Node);

	// адски длинный цикл, выстраивающий структуру нодов
	while(!File.eof()){
		string StrBuff;
		getline(File, StrBuff);

		int StrBuffSize(StrBuff.length());
		if(StrBuffSize > 0 && StrBuff[0] != '#'){
			// если строка состоит из чего нибудь кроме табов или пробелов,
			// то она подлежит распарсиванию и её можно зановить в структуру
			// текстовых нодов
			for(int i=0; i<StrBuffSize; i++){
				if(StrBuff[i] != ' ' && StrBuff[i] != '\t'){

					pTDataNode StrNode(CreateStrNode(StrBuff));

					if(CurNode == Node){
						CurNode->Add(StrNode);
					}
					else{
						// проверка, на одном ли уровне находится нод с текущим
						// если на одном, то они принадлежат общему отцовскому элементу
						int CurNodeTabPos(CurNode->GetIntVal());
						int StrNodeTabPos(StrNode->GetIntVal());

						if(CurNodeTabPos == StrNodeTabPos){
							CurNode->GetParent()->Add(StrNode);
						}

						// если табов больше, то новый нод будет дочерним нодом
						if(StrNodeTabPos > CurNodeTabPos){
							CurNode->Add(StrNode);
						}

						// если табов меньше, то новый нод имеет более высокое место
						// в иерархии
						if(StrNodeTabPos < CurNodeTabPos){
							pTDataNode ParentNode(CurNode->GetParent());
							while(true){
								if(ParentNode == NULL){
									Node->Add(StrNode);
									break;
								}
								else{
									int ParentTabPos(ParentNode->GetIntVal());
									if(ParentTabPos < StrNodeTabPos){
										ParentNode = ParentNode->GetParent();
									}
									else{
										ParentNode->GetParent()->Add(StrNode);
										break;
									}
								}
							}
						}
					}

					CurNode = StrNode;

					break;
				}
			}
		}
	}

	// распарсивание реальных параметров нод
	InitNode(Node);

	File.close();
}
///----------------------------------------------------------------------------------------------//
void TDNTextFile::Save(const wstring &FileName, const pTDataNode &Node)
{
	Exception(FileName.length()>0	, L"in Save(...) FileName not set");
	Exception(Node!=NULL			, L"in Save(...) Node not set");

	ofstream File;
	File.open(WStrToStr(FileName).c_str());
	Exception(File.is_open(), L"in Save(...) can`t open file:" + FileName);

	int Size(Node->GetSize());

	for(int i=0;i<Size;i++){ WriteNode(File,0,Node->Get(i)); }

	File.close();
}
///----------------------------------------------------------------------------------------------//
void TDNTextFile::UseEncrypt(const bool &Val)
{
	EncryptFlag = Val;
}
///----------------------------------------------------------------------------------------------//
bool TDNTextFile::IsUseEncrypt() const
{
	return EncryptFlag;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

