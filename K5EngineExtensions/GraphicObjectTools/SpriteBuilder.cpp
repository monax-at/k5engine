#include "SpriteBuilder.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TSpriteBuilder::TSpriteBuilder()
{
}
///----------------------------------------------------------------------------------------------//
TSpriteBuilder::~TSpriteBuilder()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TSpriteBuilder::Init(	const pTSprite &Sprite, const pTTexture &Texture,
							const float &RectX,const float &RectY,
							const float &RectWidth,const float &RectHeight)
{
	if(RectX == 0.0f && RectY == 0.0f && RectWidth == 0.0f && RectHeight == 0.0f){
		Sprite->Texture(Texture);
		Sprite->Size(Texture->GetWidth(), Texture->GetHeight());
	}
	else{
		Sprite->Texture(Texture);
		Sprite->Texture.SetRect(RectX,RectY,RectWidth,RectHeight);
		Sprite->Size(RectWidth, RectHeight);
	}
}
///----------------------------------------------------------------------------------------------//
void TSpriteBuilder::Init(	const pTSprite &Sprite,
							const float &X, const float &Y, const float &Z,
							const pTTexture &Texture,
							const float &RectX, const float &RectY,
							const float &RectWidth, const float &RectHeight)
{
	Init(Sprite, Texture, RectX, RectY, RectWidth, RectHeight);
	Sprite->Pos(X,Y,Z);
}
///----------------------------------------------------------------------------------------------//
void TSpriteBuilder::Init(	const pTSprite &Sprite,
							const TPoint SpritePos, const pTTexture &Texture,
							const float &RectX, const float &RectY,
							const float &RectWidth, const float &RectHeight)
{
	Init(Sprite, Texture, RectX, RectY, RectWidth, RectHeight);
	Sprite->Pos(SpritePos);
}
///----------------------------------------------------------------------------------------------//
pTSprite TSpriteBuilder::Run(	const pTTexture &Texture,
								const float &RectX,const float &RectY,
								const float &RectWidth,const float &RectHeight)
{
	pTSprite Sprite(new TSprite);

	Init(Sprite, Texture, RectX, RectY, RectWidth, RectHeight);

	return Sprite;
}
///----------------------------------------------------------------------------------------------//
pTSprite TSpriteBuilder::Run(const float &X, const float &Y, const float &Z,
							 const pTTexture &Texture,
							 const float &RectX, const float &RectY,
							 const float &RectWidth, const float &RectHeight)
{
	pTSprite Sprite(new TSprite);

	Init(Sprite, X, Y, Z, Texture, RectX, RectY, RectWidth, RectHeight);

	return Sprite;
}
///----------------------------------------------------------------------------------------------//
pTSprite TSpriteBuilder::Run(const TPoint &SpritePos,
							 const pTTexture &Texture,
							 const float &RectX, const float &RectY,
							 const float &RectWidth, const float &RectHeight)
{
	pTSprite Sprite(new TSprite);

	Init(Sprite, SpritePos, Texture, RectX, RectY, RectWidth, RectHeight);

	return Sprite;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
