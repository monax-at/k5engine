#include "SpriteAlongLineGenerator.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TSpriteAlongLineGenerator::TSpriteAlongLineGenerator():TBaseAction(),
								Exception(L"TSpriteAlongLineGenerator: "),
								List(NULL), NumSprites(0), RandPosZ(false)
{
}
///----------------------------------------------------------------------------------------------//
TSpriteAlongLineGenerator::~TSpriteAlongLineGenerator()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::ToRun(const TEvent &Event)
{
	Exception(NumSprites >0,L"in ToRun() NumSprites is 0");
	Exception(List != NULL,L"in ToRun() List is NULl");

	float StepX( (FinishPos[0]-StartPos[0]) / ((float)NumSprites) );
	float StepY( (FinishPos[1]-StartPos[1]) / ((float)NumSprites) );
	float PosZ(0.0f);

	float ShiftPosZ(0.0f);

	if(RandPosZ){ ShiftPosZ = 1.0f/((float)NumSprites);}

	TPoint TempPos(StartPos);

	for(int i=0;i<NumSprites;i++){

		TempPos.Inc(StepX, StepY);
		TempPos.SetZ(PosZ + ShiftPosZ*i);

		TSprite Sprite;
		Sprite = SpriteTemplate;

		Sprite.Pos(TempPos);

		List->Add(Sprite);
	}
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetTemplateSprite(const TSprite &Val)
{
	SpriteTemplate = Val;
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetTemplateSprite(const pTSprite &Val)
{
	SpriteTemplate = *Val;
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetOutput(const pTSpriteList  &Val)
{
	List = Val;
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetNumSprites(const int &Val)
{
	NumSprites = Val;
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetPositions(const TPoint &StartVal,
											 const TPoint &FinishVal)
{
	StartPos = StartVal;
	FinishPos = FinishVal;
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetStartPos(const TPoint &Val)
{
	StartPos(Val);
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetStartPos(const float &X,const float &Y)
{
	StartPos(X,Y);
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetStartPos(const float &X,const float &Y,
											const float &Z)
{
	StartPos(X,Y,Z);
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetFinishPos(const TPoint &Val)
{
	FinishPos(Val);
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetFinishPos(const float &X,const float &Y)
{
	FinishPos(X,Y);
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetFinishPos(const float &X,const float &Y,
											 const float &Z)
{
	FinishPos(X,Y,Z);
}
///----------------------------------------------------------------------------------------------//
void TSpriteAlongLineGenerator::SetRandPosZFlag(const bool &Val)
{
	RandPosZ = Val;
}
///----------------------------------------------------------------------------------------------//
bool TSpriteAlongLineGenerator::GetRandPosZFlag() const
{
	return RandPosZ;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
