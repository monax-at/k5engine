#include "SpriteKnife.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TSpriteKnife::TSpriteKnife():	TBaseAction(),
								Exception(L"TSpriteKnife: "),
								List(NULL), CellSize(2), Sprite(NULL),
								TextModifWidth(0.0f), TextModifHeight(0.0f),
								LastCellWidth(0.0f), LastCellHeight(0.0f),
								MeshNoiseFlag(true), UseSpritePosShistFlag(false)

{
}
///----------------------------------------------------------------------------------------------//
TSpriteKnife::~TSpriteKnife()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::FillLastRow(const TPoint &StartPos)
{
	TPoint Pos(StartPos);
	Pos.IncY((float)CellSize);
	Pos.DecY(((float)CellSize+LastCellHeight)/2.0f);

	TSprite Cell;
	Cell.Size((float)CellSize, LastCellHeight);
	Cell.Color(Sprite->Color);
	Cell.Texture(Sprite->Texture);
	Cell.Texture.SetRectSize(	CellSize*TextModifWidth,
								LastCellHeight*TextModifHeight);

	float TexRectPosX(Sprite->Texture.GetRectX());
	float TexRectPosY(Sprite->Texture.GetRectY());

	float RectPosY(CellsByHeight*(float)CellSize);

	for(int i=0;i<CellsByWidth;i++){

		Cell.Pos(Pos);
		Cell.Texture.SetRectPos(TexRectPosX + i*CellSize*TextModifWidth,
								TexRectPosY + RectPosY*TextModifHeight);

		List->Add(Cell);

		Pos.IncX((float)CellSize);
	}

	if(LastCellWidth>0.0f){
		Pos.DecX((float)CellSize);
		Pos.IncX(((float)CellSize+LastCellWidth)/2.0f);

		Cell.Pos(Pos);
		Cell.Size(LastCellWidth, LastCellHeight);
		Cell.Texture.SetRectSize(	LastCellWidth * TextModifWidth,
									LastCellHeight* TextModifHeight);

		Cell.Texture.SetRectPos(TexRectPosX + CellsByWidth*CellSize*TextModifWidth,
								TexRectPosY + RectPosY*TextModifHeight);

		List->Add(Cell);
	}
}
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::FillRow(const TPoint &StartPos,const int &RowIter)
{
	TSprite Cell;
	Cell.Size((float)CellSize);
	Cell.Color(Sprite->Color);
	Cell.Texture(Sprite->Texture);
	Cell.Texture.SetRectSize(CellSize*TextModifWidth,CellSize*TextModifHeight);

	float TexRectPosX(Sprite->Texture.GetRectX());
	float TexRectPosY(Sprite->Texture.GetRectY());

	TPoint Pos(StartPos);

	float RectPosY(RowIter*(float)CellSize);

	for(int i=0;i<CellsByWidth;i++){

		Cell.Pos(Pos);
		Cell.Texture.SetRectPos(TexRectPosX + i*CellSize* TextModifWidth,
								TexRectPosY + RectPosY  * TextModifHeight);

		List->Add(Cell);

		Pos.IncX((float)CellSize);
	}

	if(LastCellWidth>0.0f){
		Pos.DecX((float)CellSize);
		Pos.IncX(((float)CellSize+LastCellWidth)/2.0f);

		Cell.Pos(Pos);
		Cell.Size(LastCellWidth,(float)CellSize);
		Cell.Texture.SetRectSize(	LastCellWidth*TextModifWidth,
									CellSize*TextModifHeight);

		Cell.Texture.SetRectPos(TexRectPosX + CellsByWidth*CellSize*TextModifWidth,
								TexRectPosY + RowIter*CellSize* TextModifHeight);

		List->Add(Cell);

	}
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::GenerateSprites()
{
	Exception(CellSize>0,	L"in GenerateSprites() CellSize not set");
	Exception(Sprite!=NULL,	L"in GenerateSprites() Sprite not set");
	Exception(List	!=NULL,	L"in GenerateSprites() List must be set");

	float SpriteWidth  = Sprite->Size.GetX();
	float SpriteHeight = Sprite->Size.GetY();

	// модификаторы отношение размера спрайта к размеру текстуры
	// влияют на конечный размер ректа текстуры каждой ячейки и позицию
	// текстурынх координат
	TextModifWidth  = Sprite->Texture.GetRectWidth()/SpriteWidth;
	TextModifHeight = Sprite->Texture.GetRectHeight()/SpriteHeight;

	CellsByWidth   	= (int)IntegerPart(SpriteWidth/CellSize);
	CellsByHeight  	= (int)IntegerPart(SpriteHeight/CellSize);

	LastCellWidth	= SpriteWidth  - CellsByWidth*CellSize;
	LastCellHeight	= SpriteHeight - CellsByHeight*CellSize;

	TPoint StartPos;
	StartPos.SetX( (SpriteWidth  - (float)CellSize)/-2.0f );
	StartPos.SetY( (SpriteHeight - (float)CellSize)/ 2.0f );

	if(UseSpritePosShistFlag == true){ StartPos.Inc(Sprite->Pos); }

	if(LastCellHeight > 0.0f){
		FillLastRow(StartPos);
		StartPos.DecY(LastCellHeight);
	}

	for(int i=0;i<CellsByHeight;i++){
		FillRow(StartPos,CellsByHeight-i-1);
		StartPos.DecY((float)CellSize);
	}
}
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::AddMeshNoise()
{
	if(!MeshNoiseFlag){return;}

	int Size = List->GetSize();
	for(int i=0;i<Size;i++){
		List->Get(i)->Texture.SetCoordinatesState(EN_TCS_Floating);
	}

	int WidthCount = CellsByWidth;
	if(LastCellWidth>0.0f){ WidthCount+=1; }

	int HeightCount = CellsByHeight;
	if(LastCellHeight>0.0f){ HeightCount+=1; }

	for(int i=0;i<HeightCount;i++){
 		for(int j=0;j<WidthCount-1;j++){
			float Shift(0.125f);

			float Direction((float)GenRandRangeValue(-5,5));
			if(Direction!=0.0f){Shift = 0.5f/Direction;}

	 		int Index = i*WidthCount+j;

			List->Get(Index + 0)->Mesh.IncX(3,Shift);
			List->Get(Index + 1)->Mesh.IncX(2,Shift);
		}
	}

	for(int i=0;i<Size;i++){
		List->Get(i)->Texture.SetCoordinatesState(EN_TCS_Fixed);
	}
}
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::ToRun(const TEvent &Event)
{
	if(Event == true){return;}

	GenerateSprites();
	AddMeshNoise();
}
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::SetOutput(const pTSpriteList &Val)
{
	List = Val;
}
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::SetCellSize(const int &Val)
{
	CellSize = Val;
}
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::SetSprite(const pTSprite &Val)
{
	Sprite = Val;
}
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::SetMeshNoiseFlag(const bool &Val)
{
	MeshNoiseFlag = Val;
}
///----------------------------------------------------------------------------------------------//
bool TSpriteKnife::GetMeshNoiseFlag() const
{
	return MeshNoiseFlag;
}
///----------------------------------------------------------------------------------------------//
void TSpriteKnife::SetUseSpritePosShistFlag(const bool &Val)
{
	UseSpritePosShistFlag = Val;
}
///----------------------------------------------------------------------------------------------//
bool TSpriteKnife::GetUseSpritePosShistFlag() const
{
	return UseSpritePosShistFlag;
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//

