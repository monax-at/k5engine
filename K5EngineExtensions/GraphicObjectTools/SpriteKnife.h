///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef SPRITEKNIFE_H_INCLUDED
#define SPRITEKNIFE_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TSpriteKnife:public TBaseAction
{
	protected:
		TExceptionGenerator Exception;

		pTSpriteList List;

		int CellSize;

		pTSprite Sprite;

		float TextModifWidth;
		float TextModifHeight;

		int CellsByWidth;
		int CellsByHeight;

		float LastCellWidth;
		float LastCellHeight;

		bool MeshNoiseFlag;
		bool UseSpritePosShistFlag;
	protected:
		inline void FillLastRow	(const TPoint &StartPos);
		inline void FillRow		(const TPoint &StartPos, const int &RowIter);

		inline void GenerateSprites();
		inline void AddMeshNoise();

		void ToRun(const TEvent &Event);
	public:
		TSpriteKnife();
		virtual ~TSpriteKnife();

		void SetOutput(const pTSpriteList &Val);

		void SetCellSize(const int &Val);
		void SetSprite  (const pTSprite &Val);

		void SetMeshNoiseFlag(const bool &Val);
		bool GetMeshNoiseFlag() const;

		// по умолчанию false, указывет, надо ли производить смещение позиции генерируемых
		// спрайтов на позицию исходного спрайта
		void SetUseSpritePosShistFlag(const bool &Val);
		bool GetUseSpritePosShistFlag() const;
};

typedef TSpriteKnife* pTSpriteKnife;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // SPRITEKNIFE_H_INCLUDED
