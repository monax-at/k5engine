#include "GraphicObjectsCounter.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
TGraphicObjectsCounter::TGraphicObjectsCounter()
{
}
///----------------------------------------------------------------------------------------------//
TGraphicObjectsCounter::~TGraphicObjectsCounter()
{
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
int TGraphicObjectsCounter::GetSpritesCount(const pTSpriteListManager &Val) const
{
	int Count(0);
	int Size(Val->GetSize());
	for(int i=0;i<Size;i++){
		Count += Val->Get(i)->GetSize();
	}
	return Count;
}
///----------------------------------------------------------------------------------------------//
int TGraphicObjectsCounter::GetSpritesCount(const pTTextListManager &Val) const
{
	int Count(0);
	int NumLists(Val->GetSize());

	for(int i=0;i<NumLists;i++){
		pTTextList List(Val->Get(i));
		int NumLines(List->GetSize());
		for(int j=0;j<NumLines;j++){
			Count += List->Get(j)->GetLinePtr()->GetSize();
		}
	}
	return Count;
}
///----------------------------------------------------------------------------------------------//
int TGraphicObjectsCounter::GetSpritesCount(const pTSpriteListManager &Sprites,
											const pTTextListManager &Texts) const
{
	return GetSpritesCount(Sprites) + GetSpritesCount(Texts);
}
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
