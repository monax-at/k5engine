///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef SPRITEALONGLINEGENERATOR_H_INCLUDED
#define SPRITEALONGLINEGENERATOR_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TSpriteAlongLineGenerator:public TBaseAction
{
	protected:
		TExceptionGenerator Exception;
		TSprite SpriteTemplate;

		pTSpriteList List;

		int NumSprites;

		TPoint StartPos;
		TPoint FinishPos;

		bool RandPosZ;
	protected:
		void ToRun(const TEvent &Event);
	public:
		TSpriteAlongLineGenerator();
		virtual ~TSpriteAlongLineGenerator();

		void SetOutput(const pTSpriteList &Val);

		void SetTemplateSprite(const TSprite &Val);
		void SetTemplateSprite(const pTSprite &Val);

		void SetNumSprites(const int &Val);

		void SetPositions(const TPoint &StartVal,const TPoint &FinishVal);

		void SetStartPos(const TPoint &Val);
		void SetStartPos(const float &X,const float &Y);
		void SetStartPos(const float &X,const float &Y,const float &Z);

		void SetFinishPos(const TPoint &Val);
		void SetFinishPos(const float &X,const float &Y);
		void SetFinishPos(const float &X,const float &Y,const float &Z);

		void SetRandPosZFlag(const bool &Val);
		bool GetRandPosZFlag()const;
};

typedef TSpriteAlongLineGenerator* pTSpriteAlongLineGenerator;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // SPRITEALONGLINEGENERATOR_H_INCLUDED
