///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
///	author: Sergey Kalenik [monax.at@gmail.com]
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#pragma once
#ifndef SPRITEBYCIRCLEGENERATOR_H_INCLUDED
#define SPRITEBYCIRCLEGENERATOR_H_INCLUDED
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#include "K5Engine.h"
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
class TSpriteByCircleGenerator:public TBaseAction
{
	protected:
		TExceptionGenerator Exception;
		TSprite SpriteTemplate;

		pTSpriteList List;

		int NumSprites;
		float MinRadius;
		float MaxRadius;

		TPoint Pos;

		bool FloatingSpriteCenter;
		bool RandPosZ;

		bool GenRandPos;
		float StartAngle;
		bool  AngleAlongPathFlag;
	protected:
		void ToRun(const TEvent &Event);
	public:
		TSpriteByCircleGenerator();
		virtual ~TSpriteByCircleGenerator();

		void SetTemplateSprite(const TSprite &Val);
		void SetTemplateSprite(const pTSprite &Val);

		void SetOutput(const pTSpriteList &Val);

		void SetNumSprites(const int &Val);

		void SetRadius(const float &Val);
		void SetRadius(const float &Min,const float &Max);
		void SetMinRadius(const float &Val);
		void SetMaxRadius(const float &Val);

		void SetPos(const float &Val);
		void SetPos(const float &X,const float &Y);
		void SetPos(const float &X,const float &Y,const float &Z);
		void SetPos(const TPoint &Val);

		TPoint GetPos()  const;
		float 	 GetPosX() const;
		float 	 GetPosY() const;
		float 	 GetPosZ() const;

		void SetFloatingSpriteCenterFlag(const bool &Val);
		bool GetFloatingSpriteCenterFlag() const;

		void SetRandPosZFlag(const bool &Val);
		bool GetRandPosZFlag()const;

		void SetGenRandPosFlag(const bool &Val);
		bool GetGenRandPosFlag()const;

		void SetStartAngle(const float &Val);
		float GetStartAngle() const;

		void SetAngleAlongPathFlag(const bool &Val);
		bool GetAngleAlongPathFlag() const;
};

typedef TSpriteByCircleGenerator* pTSpriteByCircleGenerator;
///----------------------------------------------------------------------------------------------//
///----------------------------------------------------------------------------------------------//
#endif // SPRITEBYCIRCLEGENERATOR_H_INCLUDED
