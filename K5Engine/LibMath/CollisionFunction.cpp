#include "CollisionFunction.h"
#include "MathFunction.h"


bool CollisionCircleAndPoint(	const float &PosX,const float &PosY,const float &Radius,
								const float &PointX,const float &PointY)
{
	if(PointX > PosX + Radius || PointX < PosX - Radius){ return false; }
	if(PointY > PosY + Radius || PointY < PosY - Radius){ return false; }

	return true;

}

bool CollisionSquareAndPoint	(const float &PosX,const float &PosY,
								const float &Width,const float &Height,
								const float &PointX,const float &PointY)
{
	float PowWidth	(Width/2.0f);
	float PowHeight	(Height/2.0f);

	if(PointX > PosX + PowWidth || PointX < PosX - PowWidth){ return false; }
	if(PointY > PosY + PowHeight || PointY < PosY - PowHeight){ return false; }

	return true;
}

bool CollisionCircleAndLine(	const float &PosX,const float &PosY, const float &Radius,
								const float &X0,const float &Y0,
								const float &X1,const float &Y1)
{
	// перевод координат отрезка в сиситему координат окружности
	float X01 = X0 - PosX;
	float Y01 = Y0 - PosY;
	float X02 = X1 - PosX;
	float Y02 = Y1 - PosY;

	// т.н. направленный отрезок, его проекции на координатные оси
	float Dx = X02 - X01;
	float Dy = Y02 - Y01;

	//
	float A = Dx*Dx + Dy*Dy;
	float B = 2.0f*(X01*Dx+Y01*Dy);
	float C = X01*X01 + Y01*Y01 - Radius*Radius;

	if(-B < 0)     { return (C < 0); }
	if(-B<(2.0f*A)){ return 4.0f*A*C - B*B < 0; }

	return A+B+C<0;
}

bool CollisionCircleAndCircle(const float &X0,const float &Y0, const float &Radius0,
							  const float &X1,const float &Y1, const float &Radius1)
{
	return GetDistance2D(X0,Y0,X1,Y1) <= Radius0 + Radius1;
}

bool CollisionLineAndLine(	const float &Ax,const float &Ay,
							const float &Bx,const float &By,
							const float &Cx,const float &Cy,
							const float &Dx,const float &Dy)
{
	// где почитать по теории:
	// http://ru.wikipedia.org/wiki/%D0%9F%D1%80%D1%8F%D0%BC%D0%B0%D1%8F

	// Общее уравнение прямой:
	// Ax + By + C = 0
	// Уравнение прямой, занной двумя точками отрезка
	// (y1 - y2)x + (x2 - x1)y + (x1y2 - x2y1) = 0
	// Вывод - A B C известны

	// прямые паралельны если A1*B2 − A2*B1 = 0

	float x0(Ax); float y0(Ay);
	float x1(Bx); float y1(By);
	float x2(Cx); float y2(Cy);
	float x3(Dx); float y3(Dy);

	if(x0 > x1){
		float Tmp(x1);
		x1 = x0;
		x0 = Tmp;
	}

	if(y0 > y1){
		float Tmp(y1);
		y1 = y0;
		y0 = Tmp;
	}

	if(x2 > x3){
		float Tmp(x3);
		x3 = x2;
		x2 = Tmp;
	}

	if(y2 > y3){
		float Tmp(y3);
		y3 = y2;
		y2 = Tmp;
	}

	float A1 = y0 - y1;
	float B1 = x1 - x0;
	float C1 = x0*y1 - x1*y0;

	float A2 = y2 - y3;
	float B2 = x3 - x2;
	float C2 = x2*y3 - x3*y2;

	// проверка на паралельность прямых, если они паралельны, значит не пересекаются
	float Denom = A1*B2 - A2*B1;
	if( Abs(Denom) < Eps6){ return false; }

	// далее ищем точку их пересечения:
	float x = ( B1*C2 - B2*C1 )/( Denom );
	float y = ( C1*A2 - C2*A1 )/( Denom );

	// координаты точки есть, далее проверяем, попадают ли эти координаты в
	// координаты исходных точек
	if( (x>=x0 && x<x1 && x >= x2 && x <= x3) && (y>=y0 && y<=y1 && y>=y2 &&y<=y3 ))
	{return true;}

	return false;
}


