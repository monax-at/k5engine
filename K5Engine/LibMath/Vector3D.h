///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef VECTOR3D_H_INCLUDED
#define VECTOR3D_H_INCLUDED


#include "MathDefine.h"
#include "../Core/Point.h"


class TVector3D
{
    public:
		float x;
		float y;
		float z;
    public:
		TVector3D();
		TVector3D(const float &fx, const float &fy);
		TVector3D(const float &fx, const float &fy, const float &fz);
		TVector3D(const TPoint  &Point);
		TVector3D(const TPoint  &P1,const TPoint  &P2);
		TVector3D(const TVector3D &v);

		~TVector3D();

		void Set(const float &fx,const float &fy);
		void operator()(const float &fx,const float &fy);

		void Set(const float &fx,const float &fy,const float &fz);
		void operator()(const float &fx,const float &fy,const float &fz);

		void Set(const TVector3D &Val);
		void operator()(const TVector3D &Val);

		TVector3D &operator =  (const TVector3D &Val);
		TVector3D &operator =  (const TPoint  &Val);
/*
		TVector3D operator + () const;
		TVector3D operator - () const;

		TVector3D& operator *= (const float& f);
		TVector3D& operator /= (const float& f);

		TVector3D& operator += (const TVector3D& v);
		TVector3D& operator -= (const TVector3D& v);
		TVector3D& operator *= (const TVector3D& v);
		TVector3D& operator /= (const TVector3D& v);

		bool operator == (const TVector3D &v);
		bool operator != (const TVector3D &v);

		TVector3D operator * ( float f) const;
		TVector3D operator / ( float f) const;
		TVector3D operator + ( const TVector3D &v) const;
		TVector3D operator - ( const TVector3D &v) const;
		TVector3D operator * ( const TVector3D &v) const;
		TVector3D operator / ( const TVector3D &v) const;
		float operator & ( const TVector3D& v) const; //Скалярное произведение
*/
		float      	GetLength() const;
		float      	GetLengthSq(); // длинна без извлечения квадратного корня
		void 	   	Normalize();

		TVector3D  	GetNormalize();
		void 		Invert();
};

typedef TVector3D* pTVector3D;


#endif // VECTOR3D_H_INCLUDED
