///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BEZIERCURVE_H_INCLUDED
#define BEZIERCURVE_H_INCLUDED

#include "../Core/Point.h"

#include <vector>

using std::vector;


class TBezierCurve
{
	public:
		vector<TPoint> Points;
	public:
		TBezierCurve();
		~TBezierCurve();

		TPoint Run(const float &T); // T изменяется от 0 до 1

		void Add(const float &x,const float &y);
		void Add(const float &x,const float &y,const float &z);
		void Add(const TPoint &Val);

		TPoint Get(const int &Index) const;

		int   GetSize() const;
		float GetLength(const float &TStep = 0.0001f); // чем меньше TStep тем больше точность

		void Clear();
};

typedef TBezierCurve* pTBezierCurve;


#endif // BEZIERCURVE_H_INCLUDED
