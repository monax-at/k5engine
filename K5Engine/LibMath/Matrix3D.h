///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef MATRIX3D_H_INCLUDED
#define MATRIX3D_H_INCLUDED


#include "MathFunction.h"


/*
	обращение к элементам матрицы идёт по порядку: строка - столбец
*/

class TMatrix3D
{
    public:
        float a[4][4];
    public:
        TMatrix3D();
        TMatrix3D(const float &f);
        TMatrix3D(const TMatrix3D& m);
        virtual ~TMatrix3D();

        bool operator == (const TMatrix3D &m);
        bool operator != (const TMatrix3D &m);

        TMatrix3D& operator =  (const TMatrix3D& m);
        TMatrix3D& operator =  (const float &f);
        TMatrix3D& operator *= (const float &f);
        TMatrix3D& operator /= (const float &f);

        TMatrix3D& operator += (const TMatrix3D& m);
        TMatrix3D& operator -= (const TMatrix3D& m);
        TMatrix3D& operator *= (const TMatrix3D& m);

		TMatrix3D& ToIdentity();

		void InitScale		(const float &x, const float &y);
		void InitTranslate	(const float &Tx, const float &Ty, const float &Tz=0);
		void InitRotateZ	(const float &Angle);
};

typedef TMatrix3D* pTMatrix3D;


#endif
