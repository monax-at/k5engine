#include "Vector3D.h"

#include <cmath>
#include <assert.h>


TVector3D::TVector3D():x(0.0f),y(0.0f),z(0.0f)
{
}

TVector3D::TVector3D(const float &fx, const float &fy): x(fx), y(fy)
{
}

TVector3D::TVector3D(const float &fx, const float &fy, const float &fz): x(fx), y(fy), z(fz)
{
}

TVector3D::TVector3D(const TPoint &Point):x(Point.GetX()),y(Point.GetY()),z(Point.GetZ())
{
}

TVector3D::TVector3D(const TPoint  &P1,const TPoint  &P2):
			x(P2.GetX() - P1.GetX()), y(P2.GetY() - P1.GetY()), z(P2.GetZ() - P1.GetZ())
{
}

TVector3D::TVector3D(const TVector3D &v):x(v.x), y(v.y),  z(v.z)
{
}

TVector3D::~TVector3D()
{
}


void TVector3D::Set(const float &fx,const float &fy)
{
	x = fx;
	y = fy;
}

void TVector3D::operator()(const float &fx,const float &fy)
{
	x = fx;
	y = fy;
}

void TVector3D::Set(const float &fx,const float &fy,const float &fz)
{
	x = fx;
	y = fy;
	z = fz;
}

void TVector3D::operator()(const float &fx,const float &fy,const float &fz)
{
	x = fx;
	y = fy;
	z = fz;
}

void TVector3D::Set(const TVector3D &Val)
{
	x = Val.x;
	y = Val.y;
	z = Val.z;
}

void TVector3D::operator()(const TVector3D &Val)
{
	x = Val.x;
	y = Val.y;
	z = Val.z;
}

TVector3D& TVector3D::operator = (const TVector3D &Val)
{
    x = Val.x;
    y = Val.y;
    return *this;
}

TVector3D& TVector3D::operator = (const TPoint &Val)
{
    x = Val.GetX();
    y = Val.GetY();
    z = Val.GetY();
    return *this;
}
/*

TVector3D TVector3D::operator + () const
{
    return (*this);
}

TVector3D TVector3D::operator - () const
{
    return (TVector3D(-fx,-fy));
}

TVector3D& TVector3D::operator *= (const float& f)
{
    fx *= f;
    fy *= f;
    return (*this);
}

TVector3D& TVector3D::operator /= (const float& f)
{
    fx /= f;
    fy /= f;
    return (*this);
}

TVector3D& TVector3D::operator += (const TVector3D& v)
{
    fx += v.fx;
    fy += v.fy;
    return (*this);
}

TVector3D& TVector3D::operator -= (const TVector3D& v)
{
    fx -= v.fx;
    fy -= v.fy;
    return (*this);
}

TVector3D& TVector3D::operator *= (const TVector3D& v)
{
    fx *= v.fx;
    fy *= v.fy;
    return (*this);
}

TVector3D& TVector3D::operator /= (const TVector3D& v)
{
    fx /= v.fx;
    fy /= v.fy;
    return (*this);
}

bool TVector3D::operator == (const TVector3D &v)
{
    return( ((fx - v.fx)<Eps) && ((fy - v.fy)<Eps) );
}

bool TVector3D::operator != (const TVector3D &v)
{
    return( ((fx - v.fx)>Eps) || ((fy - v.fy)>Eps) );
}

TVector3D TVector3D::operator * (float f) const
{
	return TVector3D(fx*f, fy*f);
}

TVector3D TVector3D::operator / (float f) const
{
	return TVector3D(fx/f, fy/f);
}

TVector3D TVector3D::operator + (const TVector3D& v) const
{
	return TVector3D(fx + v.fx, fy + v.fy);
}

TVector3D TVector3D::operator - (const TVector3D& v) const
{
	return TVector3D(fx - v.fx, fy - v.fy);
}

TVector3D TVector3D::operator * (const TVector3D& v) const
{
	return TVector3D(fx * v.fx, fy * v.fy);
}

TVector3D TVector3D::operator / (const TVector3D& v) const
{
	return TVector3D(fx / v.fx, fy / v.fy);
}

float TVector3D::operator & (const TVector3D& v) const
{
	return (fx * v.fx + fy * v.fy);
}
*/

float TVector3D::GetLength() const
{
    return sqrt(x*x + y*y + z*z);
}

float TVector3D::GetLengthSq()
{
    return x*x + y*y + z*z;
}

void TVector3D::Normalize()
{
    float Len(GetLength());
    x /= Len;
    y /= Len;
    z /= Len;
}

TVector3D TVector3D::GetNormalize()
{
	TVector3D Result(*this);
	Result.Normalize();
	return Result;
}

void TVector3D::Invert()
{
	x*=-1;
	y*=-1;
	z*=-1;
}


