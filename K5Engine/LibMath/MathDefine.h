///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef MATHDEFINE_H_INCLUDED
#define MATHDEFINE_H_INCLUDED


const float Rad			= 0.0174532925199432957692369076849f;

const float PiDivFour	= 0.7853981633974483096156608458199f;
const float PiDivTwo	= 1.5707963267948966192313216916398f;
const float Pi			= 3.1415926535897932384626433832795f;
const float TwoPi		= 6.2831853071795864769252867665590f;

const float Eps			= 0.000000001f;
const float Eps3		= 0.001f;
const float Eps5		= 0.00001f;
const float Eps6		= 0.000001f;
const float Eps7		= 0.0000001f;


#endif // MATHDEFINE_H_INCLUDED
