///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef LIBMATH_H_INCLUDED
#define LIBMATH_H_INCLUDED


#include "BezierCurve.h"
#include "CollisionFunction.h"
#include "MathFunction.h"
#include "Matrix3D.h"
#include "Vector2D.h"
#include "Vector3D.h"


#endif // LIBMATH_H_INCLUDED
