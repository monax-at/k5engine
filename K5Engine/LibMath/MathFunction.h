///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef MATHFUNCTION_H_INCLUDED
#define MATHFUNCTION_H_INCLUDED


#include "MathDefine.h"
#include "Vector2D.h"


float DegToRad(const float &AngleDeg);
float RadToDeg(const float &AngleRad);

float 	Abs(const float &Val);
int 	Abs(const int   &Val);

float 	IntegerPart	  (const float &Val);
float 	FractionalPart(const float &Val);
int		NextDeg2      (const int &Number);

float GetDistance2D(const float &X,const float &Y);
float GetDistance2D(const TPoint &Point);
float GetDistance2D(const float &X1,const float &Y1,const float &X2,const float &Y2);
float GetDistance2D(const TPoint &P1,const TPoint &P2);

float GetDistance3D(const float &X,const float &Y,const float &Z);
float GetDistance3D(const TPoint &Point);
float GetDistance3D(const float &X1, const float &Y1, const float &Z1,
					const float &X2, const float &Y2, const float &Z2);
float GetDistance3D(const TPoint &P1, const TPoint &P2);

float GetAngle2D(const float &X,const float &Y);
float GetAngle2D(const TPoint &Val);
float GetAngle2D(const TVector2D &Val);
float GetAngle2D(const TVector2D &A,const TVector2D &B);
float GetAngle2D(const TPoint &A,const TPoint &B);

// пересчёт значения угла при выходе за диапазон от 0 до 360 градусов
float RecountAngle(const float &Val);

// отражение угла в противоположную сторону
float  SwapAngle (const float  &Val);

// проверка выхода значения угла за границу, true если в пределах, false если вышла за границы
bool CheckAngleLimits(const float &Angle, const float &Min, const float &Max);

TVector2D GetVector2DFromAngle(const float &Val);

float GetMinDirAngle2D(const float &X1,const float &Y1,const float &X2,const float &Y2);
float GetMinDirAngle2D(const TPoint &P1, const TPoint &P2);
float GetMinDirAngle2D(const TVector2D &V1, const TVector2D &V2);
float GetMinDirAngle2D(const float &Angle1, const float &Angle2);

// функция определения, в какой полуплоскости лежит точка С относительно прямой А В
// если 0, то точка лежит на прямой АВ
// так же зту задачу можно решить через получение детерминанта матрицы, решения
// одинаковые, но в этом случае убрано умнажение элементов на третий столбец матрицы
float GetOrient2D(const TPoint &A,const TPoint &B,const TPoint &C);

unsigned int GetFactorial(const unsigned int &n);

unsigned int GetBinomialCoefficient(const unsigned int &n,const unsigned int &k);


#endif // MATHFUNCTIONS_H_INCLUDED
