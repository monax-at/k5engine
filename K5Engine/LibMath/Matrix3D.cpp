#include "Matrix3D.h"
#include <cmath>


TMatrix3D::TMatrix3D()
{
	for(int i=0;i<4;i++){for(int j=0;j<4;j++){a[i][j]=0;}}
}

TMatrix3D::TMatrix3D(const float &f)
{
	for(int i=0;i<4;i++){for(int j=0;j<4;j++){a[i][j]=f;}}
}

TMatrix3D::TMatrix3D(const TMatrix3D& m)
{
    for(int i=0;i<4;i++){for(int j=0;j<4;j++){a[i][j]= m.a[i][j];}}
}

TMatrix3D::~TMatrix3D()
{
}


bool TMatrix3D::operator == (const TMatrix3D &v)
{
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){if(a[i][j] != v.a[i][j]){ return false; }}
    }

    return true;
}

bool TMatrix3D::operator != (const TMatrix3D &m)
{
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){if(a[i][j] != m.a[i][j]){ return true; }}
    }

    return false;
}

TMatrix3D& TMatrix3D::operator =  (const TMatrix3D& m)
{
    for(int i=0;i<4;i++){ for(int j=0;j<4;j++){a[i][j] = m.a[i][j];}}
    return(*this);
}

TMatrix3D& TMatrix3D::operator = (const float &f)
{
    for(int i=0;i<4;i++){ for(int j=0;j<4;j++){a[i][j] = f;}}
    return(*this);
}

TMatrix3D& TMatrix3D::operator *= (const float &f)
{
	for(int i=0;i<4;i++){ for(int j=0;j<4;j++){a[i][j] *= f;}}

	return(*this);
}

TMatrix3D& TMatrix3D::operator /= (const float &f)
{
	for(int i=0;i<4;i++){ for(int j=0;j<4;j++){a[i][j] /= f;}}

	return(*this);
}

TMatrix3D& TMatrix3D::operator += (const TMatrix3D& m)
{
	for(int i=0;i<4;i++){ for(int j=0;j<4;j++){a[i][j] += m.a[i][j];}}

	return(*this);
}

TMatrix3D& TMatrix3D::operator -= (const TMatrix3D& m)
{
	for(int i=0;i<4;i++){ for(int j=0;j<4;j++){a[i][j] -= m.a[i][j];}}

    return(*this);
}

TMatrix3D& TMatrix3D::operator *= (const TMatrix3D& m)
{
	TMatrix3D Res;

	// i - рядок, j - колонка
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			float Sum(0.0f);
			for(int k = 0; k < 4; k++){ Sum += a[i][k] * m.a[k][j]; }
			Res.a[i][j] = Sum;
		}
	}

    *this = Res;
    return(*this);
}

TMatrix3D& TMatrix3D::ToIdentity()
{
	for(int i = 0; i < 4; i++){ for(int j = 0; j < 4; j++){ a[i][j] = 0.0f; } }
	for(int i = 0; i < 4; i++){ a[i][i] = 1.0f; }

	return(*this);
}

void TMatrix3D::InitScale(const float &x, const float &y)
{
	ToIdentity();

	a[0][0] = x;
	a[1][1] = y;
}

void TMatrix3D::InitTranslate(const float &Tx, const float &Ty, const float &Tz)
{
	ToIdentity();

	a[0][3] = Tx;
	a[1][3] = Ty;
	a[2][3] = Tz;
}

void TMatrix3D::InitRotateZ(const float &Angle)
{
	ToIdentity();

	float RAngle(DegToRad(Angle));

	a[0][0] = cosf	(RAngle);
	a[1][0] = sinf	(RAngle);
	a[0][1] = -sinf	(RAngle);
	a[1][1] = cosf	(RAngle);
}
/*

float TMatrix3D::GetDeterminant()
{

//	// 	посмотреть теорию о том, как получить детерминант матрицы, можно в википедии:
//	// 	http://ru.wikipedia.org/wiki/Определитель
//	// 	по сути вид она имеет такой:
//	//		a11a22a33  − a11a23a32 −
//	//		a12a21a33  + a12a23a31 +
//	//		a13a21a32  − a13a22a31
//
//	return 	(	a[0][0]*(a[1][1]*a[2][2] - a[1][2]*a[2][1]) -
//				a[0][1]*(a[1][0]*a[2][2] - a[1][2]*a[2][0]) +
//				a[0][2]*(a[1][0]*a[2][1] - a[1][1]*a[2][0]) );


	float a0 =   	a[0][0]*(  );
	float a1 = -1* 	a[0][1]*(  );
	float a2 =   	a[0][2]*(  );
	float a3 = -1* 	a[0][3]*(  );


	return 0.0f;
}
*/


