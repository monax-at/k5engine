#include "MathFunction.h"
#include <cmath>


float DegToRad(const float &AngleDeg)
{
	return(AngleDeg*Rad);
}

float RadToDeg(const float &AngleRad)
{
	return(AngleRad/Rad);
}

float Abs(const float &Val)
{
	return fabs(Val);
}

int Abs(const int &Val)
{
	return (int)fabs((float)Val);
}

float IntegerPart(const float &Val)
{
	double IntegerPart;
	modf(Val,&IntegerPart);
	return (float)IntegerPart;
}

// Заметка: приведение результата modf к float сделано потому, что
//			компилятор Микрософта выдаёт предупреждение.


float FractionalPart(const float &Val)
{
	double IntegerPart;
	return (float)modf(Val,&IntegerPart);
}

int	NextDeg2(const int &Number)
{
	int Result = 1;
	while(Result < Number){
		Result *= 2;
	}
	return Result;
}

float GetDistance2D(const float &X, const float &Y)
{
	return sqrt( pow(X,2) + pow(Y,2) );
}

float GetDistance2D(const TPoint &Point)
{
	return sqrt( pow(Point.GetX(),2) + pow(Point.GetY(),2) );
}

float GetDistance2D(const float &X1, const float &Y1, const float &X2, const float &Y2)
{
	return sqrt( pow(X2-X1,2) + pow(Y2-Y1,2) );
}

float GetDistance2D(const TPoint &P1,const TPoint &P2)
{
	return sqrt( pow(P2.GetX()-P1.GetX(),2) + pow(P2.GetY()-P1.GetY(),2) );
}

float GetDistance3D(const float &X,const float &Y,const float &Z)
{
	return sqrt( pow(X,2) + pow(Y,2) + pow(Z,2) );
}

float GetDistance3D(const TPoint &Point)
{
	return sqrt( pow(Point.GetX(),2) + pow(Point.GetY(),2) + pow(Point.GetZ(),2) );
}

float GetDistance3D(const float &X1, const float &Y1, const float &Z1,
					const float &X2, const float &Y2, const float &Z2)
{
	return sqrt( pow(X2-X1,2) + pow(Y2-Y1,2) + pow(Z2-Z1,2) );
}

float GetDistance3D(const TPoint &P1, const TPoint &P2)
{
	return sqrt( pow(P2.GetX()-P1.GetX(),2) + pow(P2.GetY()-P1.GetY(),2) +
				 pow(P2.GetZ()-P1.GetZ(),2) );
}

float GetAngle2D(const float &X, const float &Y)
{
	float Len(sqrt(X*X + Y*Y));
	if(Len==0.0f){return 0.0f;}

	float Angle(RadToDeg(acosf(X/Len)));
	if(Y < 0.0f){Angle = 360.0f-Angle;}

	return Angle;
}

float GetAngle2D(const TPoint &Val)
{
	float X(Val.GetX());
	float Y(Val.GetY());

	float Len(sqrt(X*X + Y*Y));
	if(Len==0.0f){return 0.0f;}

	float Angle(RadToDeg(acosf(X/Len)));
	if(Y < 0.0f){Angle = 360.0f-Angle;}

	return Angle;
}

float GetAngle2D(const TVector2D &Val)
{
	float Len(Val.GetLength());
	if(Len==0.0f){return 0.0f;}

	float Angle(RadToDeg(acosf(Val.x/Len)));
	if(Val.y < 0.0f){Angle = 360.0f-Angle;}

	return Angle;
}

float GetAngle2D(const TVector2D &A,const TVector2D &B)
{
	float Scalar = A&B;

	return RadToDeg(acosf(Scalar/(A.GetLength()*B.GetLength())));
}

float GetAngle2D(const TPoint &A,const TPoint &B)
{
	return GetAngle2D(B.GetX() - A.GetX(), B.GetY() - A.GetY());
}

float RecountAngle(const float &Val)
{
	if(Val>= 0.0f && Val <= 360.0f){return Val;}

	if(Val>0.0f){
		int Cof((int)(Val/360.0f));
		return Val - 360.0f*Cof;
	}

	if(Val<0.0f){
		int Cof(1 + (int)(-Val/360.0f));
		return 360.0f*Cof + Val;
	}

	return 0.0f;
}

float SwapAngle (const float  &Val)
{
	TVector2D V(GetVector2DFromAngle(Val));
	V.Invert();
	return GetAngle2D(V);
}

bool CheckAngleLimits(const float &Angle,const float &Min, const float &Max)
{
	// отсекание случая, когда ограничивающий диапазон 0
	if(Max == Min ){ return Angle == Max; }

	if(Max > Min){ return  Angle >= Min && Angle <= Max; }
	else         { return (Angle >= Min && Angle <= 360.0f) || (Angle >= 0.0f && Angle <= Max); }

	return false;
}

TVector2D GetVector2DFromAngle(const float &Val)
{
	return TVector2D(cosf(DegToRad(Val)),sinf(DegToRad(Val)));
}

float GetMinDirAngle2D(const float &X1,const float &Y1,const float &X2,const float &Y2)
{
	float MinDirAngle(	RadToDeg( atan2(Y1,X1) - atan2(Y2,X2) ) );

	float AbsMinDirAngle(Abs(MinDirAngle));
	if(360.0f - AbsMinDirAngle < 180.0f){
		float NewMinAngle(360.0f - AbsMinDirAngle);
		if(MinDirAngle > 0.0f)	{MinDirAngle = -NewMinAngle;}
		else					{MinDirAngle = NewMinAngle;}
	}

	return MinDirAngle;
}

float GetMinDirAngle2D(const TPoint &P1, const TPoint &P2)
{
	return GetMinDirAngle2D(P1.GetX(), P1.GetY(), P2.GetX(), P2.GetY());
}

float GetMinDirAngle2D(const TVector2D &V1, const TVector2D &V2)
{
	return GetMinDirAngle2D(V1.x, V1.y, V2.x, V2.y);
}

float GetMinDirAngle2D(const float &Angle1,const float &Angle2)
{
	return GetMinDirAngle2D(GetVector2DFromAngle(Angle1),GetVector2DFromAngle(Angle2));
}

float GetOrient2D(const TPoint &A,const TPoint &B,const TPoint &C)
{
	return 	(A.GetX() - C.GetX()) * (B.GetY() - C.GetY()) -
			(A.GetY() - C.GetY()) * (B.GetX() - C.GetX());
}

unsigned int GetFactorial(const unsigned int &n)
{
	unsigned int Res(1);
	for(unsigned int i=1;i<=n;i++){ Res*=i; }
	return Res;
}

unsigned int GetBinomialCoefficient(const unsigned int &n,const unsigned int &k)
{
	return GetFactorial(n)/(GetFactorial(k)*GetFactorial(n-k));
}


