#include "BezierCurve.h"
#include "MathFunction.h"

#include <cmath>


TBezierCurve::TBezierCurve()
{
}

TBezierCurve::~TBezierCurve()
{
}


TPoint TBezierCurve::Run(const float &T)
{
	// http://ru.wikipedia.org/wiki/%D0%9A%D1%80%D0%B8%D0%B2%D0%B0%D1%8F_%D0%91%D0%B5%D0%B7%D1%8C%D0%B5
	// смотреть раздел "Определение"

	int N(Points.size() - 1);
	if(N <= 0){return TPoint();}

	TPoint Res;
	for(int i=0;i<N+1;i++){
		TPoint CurPoint(Points[i]);

		float Cof( GetBinomialCoefficient(N,i)*pow(T,i)*pow(1.0f - T,N - i) );
		CurPoint.Mult(Cof);
		Res.Inc(CurPoint);
	}

	return Res;
}

void TBezierCurve::Add(const float &x,const float &y)
{
	Points.push_back(TPoint(x,y));
}

void TBezierCurve::Add(const float &x,const float &y,const float &z)
{
	Points.push_back(TPoint(x,y,z));
}

void TBezierCurve::Add(const TPoint &Val)
{
	Points.push_back(Val);
}

TPoint TBezierCurve::Get(const int &Index) const
{
	return Points[Index];
}

float TBezierCurve::GetLength(const float &TStep)
{
	if(TStep <= 0.0f){return 0.0f;}

	int Size(Points.size());
	if(Size < 2){ return 0.0f; }
	if(Size ==2){ return GetDistance2D(Points[0], Points[1]); }

	TPoint OldPoint(Points[0]);
	float Length(0.0f);

	float TCount(0.0f);
	while(TCount<=1.0f){
		TPoint CurrPoint(Run(TCount));

		Length += GetDistance2D(OldPoint, CurrPoint);
		OldPoint = CurrPoint;

		TCount += TStep;
	}

	return Length;
}

int TBezierCurve::GetSize() const
{
	return Points.size();
}

void TBezierCurve::Clear()
{
	Points.clear();
}



