///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef COLLISIONFUNCTION_H_INCLUDED
#define COLLISIONFUNCTION_H_INCLUDED

bool CollisionCircleAndPoint(const float &PosX,const float &PosY, const float &Radius,
							 const float &PointX,const float &PointY);

bool CollisionSquareAndPoint(const float &PosX,const float &PosY,
							 const float &Width,const float &Height,
							 const float &PointX,const float &PointY);

bool CollisionCircleAndLine( const float &PosX,const float &PosY, const float &Radius,
							 const float &X0,const float &Y0,
							 const float &X1,const float &Y1);

bool CollisionCircleAndCircle(const float &X0,const float &Y0, const float &Radius0,
							  const float &X1,const float &Y1, const float &Radius1);

bool CollisionLineAndLine(	const float &Ax,const float &Ay,
							const float &Bx,const float &By,
							const float &Cx,const float &Cy,
							const float &Dx,const float &Dy);


#endif // COLISIONFUNCTION_H_INCLUDED
