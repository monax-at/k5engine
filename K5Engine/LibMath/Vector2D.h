///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef VECTOR2D_H_INCLUDED
#define VECTOR2D_H_INCLUDED


#include "MathDefine.h"
#include "../Core/Point.h"


class TVector2D
{
    public:
        float  x;
        float  y;
    public:
        TVector2D();
        TVector2D(const float &fx,const float &fy);
        TVector2D(const TPoint  &Point);
        TVector2D(const TPoint  &P1,const TPoint  &P2);
        TVector2D(const TVector2D &v);

        ~TVector2D();

        void Set(const float &fx,const float &fy);
        void operator()(const float &fx,const float &fy);

        void Set(const TVector2D &Val);
        void operator()(const TVector2D &Val);

        float     &operator[]  (const int &i);
        TVector2D &operator =  (const TVector2D &Val);
        TVector2D &operator =  (const TPoint &Val);

        TVector2D operator + () const;
        TVector2D operator - () const;

        TVector2D& operator *= (const float& f);
        TVector2D& operator /= (const float& f);

        TVector2D& operator += (const TVector2D& v);
        TVector2D& operator -= (const TVector2D& v);
        TVector2D& operator *= (const TVector2D& v);
        TVector2D& operator /= (const TVector2D& v);

        bool operator == (const TVector2D &v);
        bool operator != (const TVector2D &v);

        TVector2D operator * ( float f) const;
        TVector2D operator / ( float f) const;
        TVector2D operator + ( const TVector2D &v) const;
        TVector2D operator - ( const TVector2D &v) const;
        TVector2D operator * ( const TVector2D &v) const;
        TVector2D operator / ( const TVector2D &v) const;
        float operator & ( const TVector2D& v) const; //Скалярное произведение

        float      	GetLength() const;
        float      	GetLengthSq(); // длинна без извлечения квадратного корня
        void 	   	Normalize();
        TVector2D  	GetNormalize();
        void 		Invert();

        TVector2D  	Ort(); 		// ортогональная проекция вектора
};

typedef TVector2D* pTVector2D;


#endif // VECTOR2D_H_INCLUDED
