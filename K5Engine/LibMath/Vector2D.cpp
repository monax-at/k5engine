#include "Vector2D.h"

#include <cmath>
#include <assert.h>


TVector2D::TVector2D():x(0),y(0)
{
}

TVector2D::TVector2D(const float &fx,const float &fy):x(fx), y(fy)
{
}

TVector2D::TVector2D(const TPoint &Point):x(Point.GetX()),y(Point.GetY())
{
}

TVector2D::TVector2D(const TPoint  &P1,const TPoint  &P2)
{
	x = P2.GetX() - P1.GetX();
	y = P2.GetY() - P1.GetY();
}

TVector2D::TVector2D(const TVector2D &v):x(v.x),y(v.y)
{
}

TVector2D::~TVector2D()
{
}


void TVector2D::Set(const float &fx, const float &fy)
{
	x = fx;
	y = fy;
}

void TVector2D::operator()(const float &fx, const float &fy)
{
	x = fx;
	y = fy;
}

void TVector2D::Set(const TVector2D &Val)
{
	*this = Val;
}

void TVector2D::operator()(const TVector2D &Val)
{
	*this = Val;
}

float& TVector2D::operator[](const int &i)
{
    assert(i==0 || i==1);
    float *Value;
    Value = &x; if(i==1){Value = &y;}
    return(*Value);
}

TVector2D& TVector2D::operator = (const TVector2D &Val)
{
    x = Val.x;
    y = Val.y;
    return (*this);
}

TVector2D& TVector2D::operator = (const TPoint &Val)
{
    x = Val.GetX();
    y = Val.GetY();
    return (*this);
}

TVector2D TVector2D::operator + () const
{
    return (*this);
}

TVector2D TVector2D::operator - () const
{
    return (TVector2D(-x,-y));
}

TVector2D& TVector2D::operator *= (const float& f)
{
    x *= f;
    y *= f;
    return (*this);
}

TVector2D& TVector2D::operator /= (const float& f)
{
    x /= f;
    y /= f;
    return (*this);
}

TVector2D& TVector2D::operator += (const TVector2D& v)
{
    x += v.x;
    y += v.y;
    return (*this);
}

TVector2D& TVector2D::operator -= (const TVector2D& v)
{
    x -= v.x;
    y -= v.y;
    return (*this);
}

TVector2D& TVector2D::operator *= (const TVector2D& v)
{
    x *= v.x;
    y *= v.y;
    return (*this);
}

TVector2D& TVector2D::operator /= (const TVector2D& v)
{
    x /= v.x;
    y /= v.y;
    return (*this);
}

bool TVector2D::operator == (const TVector2D &v)
{
    return( ((x - v.x)<Eps) && ((y - v.y)<Eps) );
}

bool TVector2D::operator != (const TVector2D &v)
{
    return( ((x - v.x)>Eps) || ((y - v.y)>Eps) );
}

TVector2D TVector2D::operator * (float f) const
{
	return TVector2D(x*f, y*f);
}

TVector2D TVector2D::operator / (float f) const
{
	return TVector2D(x/f, y/f);
}

TVector2D TVector2D::operator + (const TVector2D& v) const
{
	return TVector2D(x + v.x, y + v.y);
}

TVector2D TVector2D::operator - (const TVector2D& v) const
{
	return TVector2D(x - v.x, y - v.y);
}

TVector2D TVector2D::operator * (const TVector2D& v) const
{
	return TVector2D(x * v.x, y * v.y);
}

TVector2D TVector2D::operator / (const TVector2D& v) const
{
	return TVector2D(x / v.x, y / v.y);
}

float TVector2D::operator & (const TVector2D& v) const
{
	return (x * v.x + y * v.y);
}

float TVector2D::GetLength() const
{
    return(sqrt(x*x + y*y));
}

float TVector2D::GetLengthSq()
{
    return(x*x + y*y);
}

void TVector2D::Normalize()
{
    float Len = GetLength();
    x/=Len;
    y/=Len;
}

TVector2D TVector2D::GetNormalize()
{
	TVector2D Result((*this));
	Result.Normalize();
	return Result;
}

void TVector2D::Invert()
{
	x*=-1;
	y*=-1;
}

TVector2D TVector2D::Ort()
{
    return (TVector2D(-y,x));
}


