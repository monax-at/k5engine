#include "BaseApplication.h"

TBaseApplication::TBaseApplication():LoopFlag(false)
{
}

TBaseApplication::~TBaseApplication()
{
}


void TBaseApplication::Start()
{
    LoopFlag = true;
}

void TBaseApplication::Stop()
{
    LoopFlag = false;
}

void TBaseApplication::Work()
{
    while(LoopFlag){ Main(); }
}

bool TBaseApplication::GetLoopFlag() const
{
	return LoopFlag;
}


