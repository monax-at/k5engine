///	authors:
///	Sergey Kalenik [monax.at@gmail.com]
///	Maxim Kolesnikov [mpkmaximus@gmail.com]

#pragma once
#ifndef BASEDEVICE_H_INCLUDED
#define BASEDEVICE_H_INCLUDED

#include "EventSystem.h"

#include "GraphicSystem/Base/BaseViewMatrixWorker.h"
#include "GraphicSystem/Base/BaseVertexData.h"
#include "BaseScreenPixelReader.h"
#include "BaseTextureLoader.h"
#include "BaseTextureCreater.h"

#include <string>

using std::wstring;


enum enDeviceWindowStyle
{
	EN_DWS_Default,
	EN_DWS_BorderOff
};


class TBaseDevice
{
	protected:
		TExceptionGenerator Exception;

		enDeviceWindowStyle WindowStyle;

		wstring Caption;

		int Width;
		int Height;

		bool FullScreen;
		bool DrawCursor;

		bool EnableVSync;
		int ColorBits;

		pTEventDeque EventDeque;

		TColor ClearColor;

		enTextureFilter MinTextureFilter;
		enTextureFilter MagTextureFilter;

		bool ActiveFlag;

		float TimerEventDelay;
		float TimerEventDelayCounter;
	protected:
		virtual void ToSetWindowStyle();
		virtual void ToSetCaption();
		virtual void ToSetEventDeque();
		virtual void ToSetClearColor();
		virtual void ToSetTextureFilter();
	public:
		TBaseDevice();
		virtual ~TBaseDevice();

		int GetWidth ()   const;
		int GetHeight()   const;

		void SetWindowStyle(const enDeviceWindowStyle &Val);
		enDeviceWindowStyle GetWindowStyle();

		void SetCaption(const wstring &Val);

		void SetEventDeque(const pTEventDeque &Val);

		void SetClearColor(const TColor &Val);
		void SetClearColor(	const float &Red,const float &Green,
							const float &Blue,const float &Alpha);

		TColor GetCleerColor() const;

		void SetEnableVSync(const bool &Val);
		bool GetEnableVSync() const;

		void SetColorBits(const int &Val);
		int  GetColorBits()const;

		void SetMinTextureFilter(const enTextureFilter &Val);
		void SetMagTextureFilter(const enTextureFilter &Val);
		void SetTextureFilter(const enTextureFilter &Val);
		void SetTextureFilter(const enTextureFilter &Min,const enTextureFilter &Mag);

		enTextureFilter GetMinTextureFilter();
		enTextureFilter GetMagTextureFilter();

		virtual void  SetTimerEventDelay(const float &Val);
		virtual float GetTimerEventDelay() const;
		virtual void  ResetTimerEventDelayCounter();

		virtual void MinimizeWindow() = 0;
		virtual void RestoreWindow() = 0;

		virtual TPoint GetCursor()       const = 0;
		virtual TPoint GetGlobalCursor() const = 0;

		virtual void Create(const int &NewWidth,const int &NewHeight,
							const bool &NewFullScreen = false) = 0;

		virtual void ClearSceneColor()	= 0;
		virtual void SwapSceneBuffers()	= 0;
		virtual void ProcessEvent()	= 0;

		virtual void SetDrawCursor(const bool &Val) = 0;

		virtual void ResetTimer() = 0;
		virtual void ResetDevice();

		virtual bool IsActive() const;

		virtual pTBaseTextureLoader 	CreateTextureLoader();
		virtual pTBaseTextureCreater    CreateTextureCreater();

		virtual pTBaseScreenPixelReader CreateScreenPixelReader();
		virtual pTBaseViewMatrixWorker 	CreateViewMatrixWorker();
		virtual pTBaseVertexData        CreateSpriteVertexData();
};

typedef TBaseDevice* pTBaseDevice;


#endif // BASEDEVICE_H_INCLUDED
