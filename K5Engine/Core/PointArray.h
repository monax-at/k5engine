///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef POINTARRAY_H_INCLUDED
#define POINTARRAY_H_INCLUDED


#include "ExceptionGenerator.h"
#include "Point.h"

#include <vector>
#include <algorithm>
#include <stdexcept>

using std::vector;
using std::copy;
using std::out_of_range;


class TPointArray
{
	protected:
		TExceptionGenerator Exception;
		vector<TPoint>      Points;
		bool                Changed;
	public:
		TPointArray();
		TPointArray(const int &Size);
		TPointArray(const TPointArray &Val);
		~TPointArray();

		void operator = (const TPointArray &Val);
		void operator = (const TPoint      &Val);

		void SetSize(const int &Val);

		void Set(const float &x, const float &y);
		void Set(const float &x, const float &y, const float &z);
		void Set(const TPoint &Point);

		void Set(const int &Index, const float &x, const float &y);
		void Set(const int &Index, const float &x, const float &y, const float &z);
		void Set(const int &Index, const TPoint &Point);

		void SetX(const float &Val);
		void SetY(const float &Val);
		void SetZ(const float &Val);

		void SetX(const int &Index, const float &Val);
		void SetY(const int &Index, const float &Val);
		void SetZ(const int &Index, const float &Val);

		void Inc(const int &Index, const float &x, const float &y);
		void Inc(const int &Index, const float &x, const float &y, const float &z);
		void Inc(const int &Index, const TPoint &Point);

		void Dec(const int &Index, const float &x, const float &y);
		void Dec(const int &Index, const float &x, const float &y, const float &z);
		void Dec(const int &Index, const TPoint &Point);

		void IncX(const float &Val);
		void IncY(const float &Val);
		void IncZ(const float &Val);

		void IncX(const int &Index, const float &Val);
		void IncY(const int &Index, const float &Val);
		void IncZ(const int &Index, const float &Val);

		void DecX(const float &Val);
		void DecY(const float &Val);
		void DecZ(const float &Val);

		void DecX(const int &Index, const float &Val);
		void DecY(const int &Index, const float &Val);
		void DecZ(const int &Index, const float &Val);

		TPoint Get(const int &Index = 0) const;

		float GetX(const int &Index = 0) const;
		float GetY(const int &Index = 0) const;
		float GetZ(const int &Index = 0) const;

		int GetSize() const;

		// установка и взятие флага, который показывает, были ли изменения в точках спрайта
		void SetChanged(const bool &Val = true);
		bool GetChanged() const;
};

typedef TPointArray* pTPointArray;


#endif // POINT3DARRAY_H_INCLUDED
