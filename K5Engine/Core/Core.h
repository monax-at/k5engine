///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef CORE_H_INCLUDED
#define CORE_H_INCLUDED


#include "ActionSystem.h"
#include "EventSystem.h"
#include "GraphicSystem.h"
#include "MusicSystem.h"

#include "BaseApplication.h"
#include "BaseDevice.h"
#include "BaseScreenPixelReader.h"
#include "BaseTextureLoader.h"
#include "ExceptionGenerator.h"
#include "PointArray.h"


#endif // CORE_H_INCLUDED
