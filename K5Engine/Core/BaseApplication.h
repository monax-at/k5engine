///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEAPPLICATION_H_INCLUDED
#define BASEAPPLICATION_H_INCLUDED

class TBaseApplication
{
    protected:
        bool LoopFlag;
    public:
        TBaseApplication ();
        ~TBaseApplication();

        virtual void Init() = 0; // в этой функции надо описывать инициализацию систем и переменных
        virtual void Quit() = 0; // в этой отчистку данных при выходе из программы
        virtual void Main() = 0; // эта крутится в цикле приложения в Work()

        void Start(); // старт приложения (цикла) LoopFlag = true
        void Work();  // бесконечный цикл приложения
        void Stop();  // остановка приложения (цикла) LoopFlag = false

        bool GetLoopFlag() const;
};

typedef TBaseApplication* pTBaseApplication;


#endif // BASEAPPLICATION_H_INCLUDED
