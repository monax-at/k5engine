#include "Debug.h"

TDebug Debug;


TDebug::TDebug()
{
}

TDebug::~TDebug()
{
}


void TDebug::RegistrationObj(void *PtrVal, const wstring &Val)
{
	RegisteredObj[PtrVal] = Val;
}

void TDebug::UnregistrationObj(void *PtrVal)
{
	RegisteredObj.erase(PtrVal);
}

wstring TDebug::GetObjInfo(void *PtrVal)
{
	if(RegisteredObj.end() != RegisteredObj.find(PtrVal))
		return RegisteredObj[PtrVal];
	return L"DEBUG information not Found.";
}

void TDebug::SetObjInfo(void *PtrVal,const wstring &Val)
{
	if(RegisteredObj.end() != RegisteredObj.find(PtrVal))
		RegisteredObj[PtrVal] = Val;
}

int TDebug::GetObjCount()
{
	return RegisteredObj.size();
}



