///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef MUSICDEVICE_H_INCLUDED
#define MUSICDEVICE_H_INCLUDED

#include "Base/BaseMusicDevice.h"

class TMusicDevice:public TBaseMusicDevice
{
	protected:
		pTBaseMusicDevice Device;
	public:
		TMusicDevice();
		virtual ~TMusicDevice();

		virtual void Construct(const pTBaseMusicDevice &NewDevice);
		virtual void Destruct();

		void Create();

		pTBaseMusicTrack		CreateMusicTrack();
		pTBaseMusicTrackLoader 	CreateMusicTrackLoader();
};

typedef TMusicDevice* pTMusicDevice;


#endif // MUSICDEVICE_H_INCLUDED
