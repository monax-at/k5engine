#include "MusicTrackLoader.h"


TMusicTrackLoader::TMusicTrackLoader():TBaseMusicTrackLoader(),Loader(NULL)
{
	Exception.SetPrefix(L"TMusicTrackLoader: ");
}

TMusicTrackLoader::~TMusicTrackLoader()
{
	delete Loader;
}


pTBaseMusicTrack TMusicTrackLoader::Load(const wstring &File, const wstring &MusicName)
{
	Exception(Loader!=NULL,L"in Load(...) Loader not init, Device not set");
	return Loader->Run(File,MusicName);
}

void TMusicTrackLoader::SetDevice(const pTBaseMusicDevice &Device)
{
	delete Loader;
	Loader = NULL;
	if(Device!=NULL){Loader = Device->CreateMusicTrackLoader();}
}


