///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEMUSICTRACK_H_INCLUDED
#define BASEMUSICTRACK_H_INCLUDED

#include <string>

using std::wstring;


class TBaseMusicTrack
{
	protected:
		wstring Name;

		bool  Active;
		bool  Repeat;
		float Volume;
	protected:
		virtual void ToSetActive();
		virtual void ToSetRepeat();
		virtual void ToSetVolume();

		virtual void ToPlay();
		virtual void ToStop();
		virtual void ToReset();
		virtual bool ToIsPlaying();
	public:
		TBaseMusicTrack();
		virtual ~TBaseMusicTrack();

		void SetName(const wstring &Val);
		wstring GetName() const;

		void SetActive(const bool &Val);
		bool GetActive() const;

		void SetRepeat(const bool &Val);
		bool GetRepeat() const;

		void SetVolume(const float &Val);
		float GetVolume() const;

		void Play();
		void Stop();

		void Reset();
		bool IsPlaying();
};

typedef TBaseMusicTrack* pTBaseMusicTrack;


#endif // MUSICTRACK_H_INCLUDED
