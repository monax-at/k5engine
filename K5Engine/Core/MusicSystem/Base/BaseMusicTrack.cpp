#include "BaseMusicTrack.h"


TBaseMusicTrack::TBaseMusicTrack():Active(true),Repeat(false),Volume(1.0f)
{
}

TBaseMusicTrack::~TBaseMusicTrack()
{
}


void TBaseMusicTrack::ToSetActive()
{
}

void TBaseMusicTrack::ToSetRepeat()
{
}

void TBaseMusicTrack::ToSetVolume()
{
}

void TBaseMusicTrack::ToPlay()
{
}

void TBaseMusicTrack::ToStop()
{
}

void TBaseMusicTrack::ToReset()
{
}

bool TBaseMusicTrack::ToIsPlaying()
{
	return false;
}

void TBaseMusicTrack::SetName(const wstring &Val)
{
	Name = Val;
}

wstring TBaseMusicTrack::GetName() const
{
	return Name;
}

void TBaseMusicTrack::SetActive(const bool &Val)
{
	Active = Val;
	ToSetActive();
}

bool TBaseMusicTrack::GetActive() const
{
	return Active;
}

void TBaseMusicTrack::SetRepeat(const bool &Val)
{
	Repeat = Val;
	ToSetRepeat();
}

bool TBaseMusicTrack::GetRepeat() const
{
	return Repeat;
}

void TBaseMusicTrack::SetVolume(const float &Val)
{
	Volume = Val;
	ToSetVolume();
}

float TBaseMusicTrack::GetVolume() const
{
	return Volume;
}

void TBaseMusicTrack::Play()
{
	if(Active == false){return;}
	ToPlay();
}

void TBaseMusicTrack::Stop()
{
	if(Active == false){return;}
	ToStop();
}

void TBaseMusicTrack::Reset()
{
	if(Active == false){return;}
	ToReset();
}

bool TBaseMusicTrack::IsPlaying()
{
	if(Active == false){return false;}
	return ToIsPlaying();
}


