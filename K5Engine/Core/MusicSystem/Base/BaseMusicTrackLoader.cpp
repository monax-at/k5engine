#include "BaseMusicTrackLoader.h"


TBaseMusicTrackLoader::TBaseMusicTrackLoader():Exception(L"TBaseMusicTrackLoader: ")
{
}

TBaseMusicTrackLoader::~TBaseMusicTrackLoader()
{
}


pTBaseMusicTrack TBaseMusicTrackLoader::operator()(	const wstring &File,
													const wstring &MusicName)
{
	return Load(File,MusicName);
}

pTBaseMusicTrack TBaseMusicTrackLoader::Run(const wstring &File,
											const wstring &MusicName)
{
	return Load(File,MusicName);
}


