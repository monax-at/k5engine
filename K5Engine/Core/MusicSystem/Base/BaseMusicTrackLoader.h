///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef TBASEMUSICTRACKLOADER_H_INCLUDED
#define TBASEMUSICTRACKLOADER_H_INCLUDED

#include "../../ExceptionGenerator.h"
#include "BaseMusicTrack.h"

class TBaseMusicTrackLoader
{
	protected:
		TExceptionGenerator Exception;
	protected:
		virtual pTBaseMusicTrack Load(	const wstring &File,
										const wstring &MusicName) = 0;
	public:
		TBaseMusicTrackLoader();
		virtual ~TBaseMusicTrackLoader();

		pTBaseMusicTrack operator()(const wstring &File,const wstring &MusicName = L"");
		pTBaseMusicTrack Run(const wstring &File,const wstring &MusicName = L"");
};

typedef TBaseMusicTrackLoader* pTBaseMusicTrackLoader;


#endif // TBASEMUSICTRACKLOADER_H_INCLUDED
