///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEMUSICDEVICE_H_INCLUDED
#define BASEMUSICDEVICE_H_INCLUDED


#include "BaseMusicTrackLoader.h"


class TBaseMusicDevice
{
	protected:
		TExceptionGenerator Exception;
	public:
		TBaseMusicDevice();
		virtual ~TBaseMusicDevice();

		virtual void Create() = 0;

		virtual pTBaseMusicTrack 		CreateMusicTrack();
		virtual pTBaseMusicTrackLoader 	CreateMusicTrackLoader();
};
typedef TBaseMusicDevice *pTBaseMusicDevice;


#endif // BASEMUSICDEVICE_H_INCLUDED
