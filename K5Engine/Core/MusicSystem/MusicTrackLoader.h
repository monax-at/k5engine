///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef MUSICTRACKLOADER_H_INCLUDED
#define MUSICTRACKLOADER_H_INCLUDED


#include "Base/BaseMusicDevice.h"


class TMusicTrackLoader:public TBaseMusicTrackLoader
{
	protected:
		pTBaseMusicTrackLoader Loader;
	protected:
		pTBaseMusicTrack Load(	const wstring &File,
								const wstring &MusicName);
	public:
		TMusicTrackLoader();
		virtual ~TMusicTrackLoader();

		void SetDevice(const pTBaseMusicDevice &Device);
};

typedef TMusicTrackLoader* pTMusicTrackLoader;


#endif // MUSICTRACKLOADER_H_INCLUDED
