#include "MusicDevice.h"


TMusicDevice::TMusicDevice():TBaseMusicDevice(),Device(NULL)
{
	Exception.SetPrefix(L"TMusicDevice: ");
}

TMusicDevice::~TMusicDevice()
{
	delete Device;
}


void TMusicDevice::Construct(const pTBaseMusicDevice &NewDevice)
{
	delete Device;
	Device = NewDevice;
	Exception(Device!=NULL,L"in Construct() Device is NULL");
}

void TMusicDevice::Destruct()
{
	delete Device;
	Device = NULL;
}

void TMusicDevice::Create()
{
	Exception(Device!=NULL,L"in Create() Device not construct");
	Device->Create();
}

pTBaseMusicTrack TMusicDevice::CreateMusicTrack()
{
	Exception(Device!=NULL,L"in CreateMusicTrack() Device not construct");
	return Device->CreateMusicTrack();
}

pTBaseMusicTrackLoader TMusicDevice::CreateMusicTrackLoader()
{
	Exception(Device!=NULL,L"in CreateMusicTrackLoader() Device not construct");
	return Device->CreateMusicTrackLoader();
}


