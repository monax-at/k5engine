#include "MusicTrackList.h"


TMusicTrackList::TMusicTrackList():Exception(L"TMusicTrackList: "),ActiveFlag(true)
{
}

TMusicTrackList::~TMusicTrackList()
{
	Clear();
}


pTBaseMusicTrack TMusicTrackList::GetElem(const int &Id)
{
	int Size = Elems.size();

	Exception(Size>0,L" in GetElem(Id) Size out of range");
	Exception(Id>=0 || Id<Size,L" in GetElem(Id) Id out of range");

	return Elems[Id];
}

pTBaseMusicTrack TMusicTrackList::GetElem(const wstring &Name)
{
	int Size = Elems.size();
	Exception(Size>0,L"in GetElem(Name) list empty");

	for(int i=0;i<Size;i++){
		if(Elems[i]->GetName() == Name){
			return Elems[i];
		}
	}
	Exception(L"in GetElem(Name) elem whis name: "+Name+L" not exist");
	return NULL;
}

pTBaseMusicTrack TMusicTrackList::operator[](const int &Id)
{
	return GetElem(Id);
}

pTBaseMusicTrack TMusicTrackList::operator[](const wstring &Name)
{
	return GetElem(Name);
}

pTBaseMusicTrack TMusicTrackList::Get(const int &Id)
{
	return GetElem(Id);
}

pTBaseMusicTrack TMusicTrackList::Get(const wstring &Name)
{
	return GetElem(Name);
}

void TMusicTrackList::Add(const pTBaseMusicTrack &Val)
{
	Elems.push_back(Val);
}

void TMusicTrackList::Del(const int &Id)
{
	int Size = Elems.size();
	Exception(Size>0,			L" in Del(Id) Size out of range");
	Exception(Id>=0 || Id<Size,	L" in Del(Id) Id out of range");

	delete Elems[Id];
	Elems.erase(Elems.begin() + Id);
}

void TMusicTrackList::Del(const wstring &Name)
{
	int Size = Elems.size();
	Exception(Size>0, L" in Del(Name Size out of range");

	int ResId = -1;
	for(int i=0;i<Size;i++){
		if(Elems[i]->GetName() == Name){
			ResId = i;
			break;
		}
	}

	Exception(ResId!=-1,L"in GetElem(Name) elem whis name: "+Name+L" not exist");

	delete Elems[ResId];
	Elems.erase(Elems.begin() + ResId);
}

bool TMusicTrackList::IsExist(const wstring &Name)
{
	int Size = Elems.size();
	if(Size == 0){ return false; }

	for(int i=0; i<Size; i++){ if(Elems[i]->GetName() == Name){return true;} }

	return false;
}

int TMusicTrackList::GetSize()
{
	return Elems.size();
}

void TMusicTrackList::Clear()
{
	int Size = Elems.size();
	for(int i=0;i<Size;i++){
		delete Elems[i];
	}
	Elems.clear();
}

void TMusicTrackList::SetName(const wstring &Val)
{
	Name = Val;
}

wstring TMusicTrackList::GetName()
{
	return Name;
}

void TMusicTrackList::SetActive(const bool &Val)
{
	ActiveFlag = Val;
	int Size = Elems.size();
	for(int i=0;i<Size;i++){ Elems[i]->SetActive(Val);
	}
}

bool TMusicTrackList::GetActive() const
{
	return ActiveFlag;
}


