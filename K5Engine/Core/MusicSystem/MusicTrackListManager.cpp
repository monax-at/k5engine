#include "MusicTrackListManager.h"


TMusicTrackListManager::TMusicTrackListManager():Exception(L"TMusicTrackListManager: ")
{
}

TMusicTrackListManager::~TMusicTrackListManager()
{
	Clear();
}


inline int TMusicTrackListManager::CheckAndGetSize()
{
	int Size(Lists.size());
	Exception(Size > 0,L"in CheckAndGetSize() list size 0 ");
	return Size;
}

pTBaseMusicTrackList TMusicTrackListManager::GetElem(const int &Id)
{
    int Size(CheckAndGetSize());
    Exception(Id >= 0 && Id < Size,L"in GetElem(Id) Id out of rangle");
	return Lists[Id];
}

pTBaseMusicTrackList TMusicTrackListManager::GetElem(const wstring &Name)
{
	int Size(CheckAndGetSize());

	for(int i=0;i<Size;i++){ if(Lists[i]->GetName() == Name){ return Lists[i]; } }

	Exception(L"in GetElem(Name) elem with name:" + Name + L"not exist");

	return NULL;
}

pTBaseMusicTrackList TMusicTrackListManager::operator[](const int &Val)
{
	return GetElem(Val);
}

pTBaseMusicTrackList TMusicTrackListManager::operator[](const wstring &Val)
{
	return GetElem(Val);
}

pTBaseMusicTrackList TMusicTrackListManager::Get(const int &Val)
{
	return GetElem(Val);
}

pTBaseMusicTrackList TMusicTrackListManager::Get(const wstring &Val)
{
	return GetElem(Val);
}

pTBaseMusicTrackList TMusicTrackListManager::GetLast()
{
	int Size(Lists.size());
	Exception(Size > 0,L"in GetLast() lists is enpty");
	return Lists[Size-1];
}

pTBaseMusicTrack TMusicTrackListManager::Get(	const int &Id,
												const wstring &Name)
{
	return GetElem(Id)->Get(Name);
}

pTBaseMusicTrack TMusicTrackListManager::Get(	const wstring &List,
												const wstring &Name)
{
	return GetElem(List)->Get(Name);
}

void TMusicTrackListManager::Add(const wstring &Name)
{
	Exception(Name.length()>0	, L"in Add(Name) Name not set");
	Exception(!IfExist(Name)	, L"in Add(Name) elem already exist");

	TMusicTrackList *List(new TMusicTrackList);
	List->SetName(Name);
	Lists.push_back(List);
}

void TMusicTrackListManager::Add(const wstring &List,const pTBaseMusicTrack &Val)
{
	GetElem(List)->Add(Val);
}

bool TMusicTrackListManager::Del(const int &Id)
{
	int Size(CheckAndGetSize());
	Exception(Id >= 0 && Id < Size,L"in Del(Id) Id out of rangle");

	delete Lists[Id];
	Lists.erase(Lists.begin() + Id);
	return (true);
}

bool TMusicTrackListManager::Del(const wstring &Name)
{
	int Size = CheckAndGetSize();
	int Pos  = -1;

	for(int i=0;i<Size;i++){
		if(Lists[i]->GetName() == Name){
			Pos = i;
			break;
		}
	}

	if(Pos != -1){
		delete Lists[Pos];
		Lists.erase(Lists.begin() + Pos);
		return true;
	}

	return false;
}

bool TMusicTrackListManager::IfExist(const wstring &Name)
{
	int Size(Lists.size());
	for(int i=0;i<Size;i++){ if(Lists[i]->GetName() == Name){return true;} }
	return false;
}

int TMusicTrackListManager::GetSize()
{
	return(Lists.size());
}

void TMusicTrackListManager::Clear()
{
	int Size(Lists.size());
	for(int i=0;i<Size;i++){ delete Lists[i]; }
	Lists.clear();
}


