///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef MUSICTRACKLIST_H_INCLUDED
#define MUSICTRACKLIST_H_INCLUDED


#include "../ExceptionGenerator.h"
#include "Base/BaseMusicTrack.h"

#include <vector>

using std::vector;


class TMusicTrackList
{
	private:
		TExceptionGenerator Exception;
		wstring Name;
		vector<pTBaseMusicTrack> Elems;
		bool ActiveFlag;
	private:
		inline void CheckId(const int &Id);
		inline pTBaseMusicTrack GetElem(const int &Id);
		inline pTBaseMusicTrack GetElem(const wstring &Name);
	public:
		TMusicTrackList();
		virtual ~TMusicTrackList();

        pTBaseMusicTrack operator[](const int &Id);
        pTBaseMusicTrack operator[](const wstring &Name);

        pTBaseMusicTrack Get(const int &Id);
        pTBaseMusicTrack Get(const wstring &Name);

		void Add	(const pTBaseMusicTrack &Val);
		void Del	(const int &Id);
		void Del	(const wstring &Name);
		bool IsExist(const wstring &Name);

        int  GetSize();
		void Clear();

		void SetName(const wstring &Val);
		wstring GetName();

		void SetActive(const bool &Val);
		bool GetActive() const;
};

typedef TMusicTrackList* pTBaseMusicTrackList;


#endif // MUSICTRACKLIST_H_INCLUDED
