///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef MUSICTRACKLISTMANAGER_H_INCLUDED
#define MUSICTRACKLISTMANAGER_H_INCLUDED


#include "MusicTrackList.h"


class TMusicTrackListManager
{
	private:
		vector<pTBaseMusicTrackList> Lists;
		TExceptionGenerator Exception;
	private:
		inline int CheckAndGetSize();
        inline pTBaseMusicTrackList GetElem(const int &Id);
        inline pTBaseMusicTrackList GetElem(const wstring &Name);
	public:
		TMusicTrackListManager();
		virtual ~TMusicTrackListManager();

        pTBaseMusicTrackList operator[](const int &Val);
        pTBaseMusicTrackList operator[](const wstring &Val);

        pTBaseMusicTrackList Get(const int &Val);
        pTBaseMusicTrackList Get(const wstring &Val);

        pTBaseMusicTrackList GetLast();

        pTBaseMusicTrack Get(const int &Id,	 	 const wstring &Name);
        pTBaseMusicTrack Get(const wstring &List,const wstring &Name);

		void Add(const wstring &Name);
		void Add(const wstring &List, const pTBaseMusicTrack &Val);

		bool Del	(const int &Id);
		bool Del	(const wstring &Name);
		bool IfExist(const wstring &Name);

        int  GetSize();
		void Clear();
};

typedef TMusicTrackListManager* pTMusicTrackListManager;


#endif // MUSICTRACKLISTMANAGER_H_INCLUDED
