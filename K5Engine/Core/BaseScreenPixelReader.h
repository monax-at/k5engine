///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef BASESCREENPIXELREADER_H_INCLUDED
#define BASESCREENPIXELREADER_H_INCLUDED


#include "GraphicSystem/Base/Color.h"
#include "EventSystem/Event.h"
#include "Point.h"


class TBaseScreenPixelReader
{
	protected:
		virtual TColor ToRun(const float &X,const float &Y) = 0;
		virtual void ToSetScreenSizes();
	public:
		TBaseScreenPixelReader();
		virtual ~TBaseScreenPixelReader();

		TColor operator()(const float &X,const float &Y);
		TColor operator()(const TPoint &Point);
		TColor operator()(const TEvent &Event);
		TColor operator()(const TMouseEvent &Event);

		TColor Run(const float &X,const float &Y);
		TColor Run(const TPoint &Point);
		TColor Run(const TEvent &Event);
		TColor Run(const TMouseEvent &Event);
};

typedef TBaseScreenPixelReader* pTBaseScreenPixelReader;


#endif // BASESCREENPIXELREADER_H_INCLUDED
