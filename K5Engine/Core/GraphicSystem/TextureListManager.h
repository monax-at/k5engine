///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef TEXTURELISTMANAGER_H_INCLUDED
#define TEXTURELISTMANAGER_H_INCLUDED


#include "TextureList.h"


class TTextureListManager
{
	private:
		TExceptionGenerator Exception;
		vector<pTTextureList> Lists;
	private:
		inline pTTextureList GetElem(const int &Id);
		inline pTTextureList GetElem(const wstring &Name);

        inline pTTexture GetTex(const int		&List, const int 	 &Texture);
        inline pTTexture GetTex(const int 		&List, const wstring &Texture);
        inline pTTexture GetTex(const wstring 	&List, const int 	 &Texture);
        inline pTTexture GetTex(const wstring 	&List, const wstring &Texture);
	public:
		TTextureListManager();
		~TTextureListManager();

        pTTextureList operator()(const int &Id);
        pTTextureList operator()(const wstring &Name);

        pTTexture operator()(const int	&List, const int 	 &Texture);
        pTTexture operator()(const int 	&List, const wstring &Texture);
        pTTexture operator()(const wstring &List, const int 	&Texture);
        pTTexture operator()(const wstring &List, const wstring &Texture);

        pTTextureList Get(const int &Id);
        pTTextureList Get(const wstring &Name);
        pTTextureList GetLast();
        pTTextureList GetListByTexture(const pTTexture &Val);

        pTTexture Get(const int		&List, const int 	 &Texture);
        pTTexture Get(const int 	&List, const wstring &Texture);
        pTTexture Get(const wstring &List, const int 	 &Texture);
        pTTexture Get(const wstring &List, const wstring &Texture);

        pTTexture GetIfExist(const int 	   &List, const wstring &Texture);
        pTTexture GetIfExist(const wstring &List, const wstring &Texture);

		pTTextureList Add(const wstring &Name);
		pTTextureList Add(const pTTextureList &List);

        void Add(const int 		&List, const pTTexture &Val);
        void Add(const wstring 	&List, const pTTexture &Val);

		bool Del	(const int &Id);
		bool Del	(const wstring &Name);
		bool IfExist(const wstring &Name);

        int  GetSize();
		void Clear();
		void ClearTexturesData();
};

typedef TTextureListManager* pTTextureListManager;


#endif // TEXTURELISTMANAGER_H_INCLUDED
