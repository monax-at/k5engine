#include "TextureList.h"


TTextureList::TTextureList():Exception(L"TTextureList: ")
{
}

TTextureList::~TTextureList()
{
	Clear();
}


pTTexture TTextureList::GetElem(const int &Id)
{
	int Size(Elems.size());
	Exception(Size > 0,				L"in GetElem(Id) list is empty");
	Exception(Id >= 0 && Id < Size,	L"in GetElem(Id) Id out of range");
	return Elems[Id];
}

pTTexture TTextureList::GetElem(const wstring &Name)
{
	int Size(Elems.size());
	Exception(Size > 0, L"in GetElem(Name) list is empty");

	for(int i=0;i<Size;i++){
		if(Elems[i]->GetName() == Name){
			return Elems[i];
		}
	}
	Exception(L"in GetElem(Name) elem whis name: "+Name+L" not exist");
	return NULL;
}

void TTextureList::SetName(const wstring &Val)
{
	Name = Val;
}

wstring TTextureList::GetName() const
{
	return Name;
}

pTTexture TTextureList::operator[](const int &Id)
{
	return GetElem(Id);
}

pTTexture TTextureList::operator[](const wstring &Name)
{
	return GetElem(Name);
}

pTTexture TTextureList::Get(const int &Id)
{
	return GetElem(Id);
}

pTTexture TTextureList::Get(const wstring &Name)
{
	return GetElem(Name);
}

pTTexture TTextureList::GetLast()
{
	int Size(Elems.size());
	Exception(Size > 0,L"in GetLast() list is enpty");
	return Elems[Size-1];
}

int  TTextureList::GetSize() const
{
	return Elems.size();
}

pTTexture TTextureList::Add(const pTTexture &Texture)
{
	Exception(Texture != NULL, L"in Add(...) texture is NULL");
	Elems.push_back(Texture);
	return Texture;
}

void TTextureList::Del(const int &Id)
{
	int Size(Elems.size());
	Exception(Size > 0,				L"in Delete(Id) list is empty");
	Exception(Id >= 0 && Id < Size,	L"in Delete(Id) Id out of range");

	Elems[Id]->Del();
	delete Elems[Id];
	Elems.erase(Elems.begin() + Id);
}

void TTextureList::Del(const wstring &Name)
{
    int Size(Elems.size());
    Exception(Size > 0, L"in Del(Id) list is empty");

    int ErasePos(-1);

    for(int i=0;i<Size;i++){
        if(Elems[i]->GetName() == Name){
        	ErasePos = i;
        	break;
		}
    }

	Exception(ErasePos != -1, L"in Del(Name) elem whis name: "+Name+L" not exist");

	Elems[ErasePos]->Del();
	delete Elems[ErasePos];
	Elems.erase(Elems.begin()+ErasePos);
}

bool TTextureList::IfExist(const unsigned long &ID)
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){ if(Elems[i]->GetID() == ID){return true;} }
	return false;
}

bool TTextureList::IfExist(const wstring &Name)
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){ if(Elems[i]->GetName() == Name){return true;} }
	return false;
}

void TTextureList::Clear()
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){
		Elems[i]->Del();
		delete Elems[i];
	}
	Elems.clear();
}

void TTextureList::ClearTexturesData()
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){ Elems[i]->Del(); }
}


