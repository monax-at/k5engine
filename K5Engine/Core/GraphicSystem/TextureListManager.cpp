#include "TextureListManager.h"


TTextureListManager::TTextureListManager():Exception(L"TTextureListManager: ")
{
}

TTextureListManager::~TTextureListManager()
{
	Clear();
}


pTTextureList TTextureListManager::GetElem(const int &Id)
{
    int Size = Lists.size();

    Exception(Size>0,				L"in GetElem(Name) List size 0");
	Exception(Id >= 0 && Id < Size,	L"in GetElem(Name) Id out of range");

	return Lists[Id];
}

pTTextureList TTextureListManager::GetElem(const wstring &Name)
{
	int Size = Lists.size();
	Exception(Size>0,L"in GetElem(Name) List size 0");

	for(int i=0;i<Size;i++){
		if(Lists[i]->GetName() == Name){ return Lists[i]; }
	}

	Exception(L"in GetElem(Name) List whis name: "+Name+L" not exist");
	return NULL;
}

pTTexture TTextureListManager::GetTex(const int	&List, const int &Texture)
{
	return GetElem(List)->Get(Texture);
}

pTTexture TTextureListManager::GetTex(const int &List, const wstring &Texture)
{
	return GetElem(List)->Get(Texture);
}

pTTexture TTextureListManager::GetTex(const wstring &List, const int &Texture)
{
	return GetElem(List)->Get(Texture);
}

pTTexture TTextureListManager::GetTex(const wstring &List, const wstring &Texture)
{
	return GetElem(List)->Get(Texture);
}

pTTextureList TTextureListManager::operator()(const int &Id)
{
	return GetElem(Id);
}

pTTextureList TTextureListManager::operator()(const wstring &Name)
{
	return GetElem(Name);
}

pTTexture TTextureListManager::operator()(	const int	&List,
											const int &Texture)
{
	return GetTex(List,Texture);
}

pTTexture TTextureListManager::operator()(	const int &List,
											const wstring &Texture)
{
	return GetTex(List,Texture);
}

pTTexture TTextureListManager::operator()(	const wstring &List,
											const int &Texture)
{
	return GetTex(List,Texture);
}

pTTexture TTextureListManager::operator()(	const wstring &List,
											const wstring &Texture)
{
	return GetTex(List,Texture);
}

pTTextureList TTextureListManager::Get(const int &Id)
{
	return GetElem(Id);
}

pTTextureList TTextureListManager::Get(const wstring &Name)
{
	return GetElem(Name);
}

pTTextureList TTextureListManager::GetLast()
{
	int Size(Lists.size());
	Exception(Size > 0,L"in GetLast() lists is enpty");
	return Lists[Size-1];
}

pTTextureList TTextureListManager::GetListByTexture(const pTTexture &Val)
{
	int Size = Lists.size();
    Exception(Size>0,L"in GetListByTexture(Val) List size 0");

	unsigned long TexID(Val->GetID());

	for(int i=0;i<Size;i++){
		if(Lists[i]->IfExist(TexID)){ return Lists[i]; }
	}

	Exception(L"in GetListByTexture(Val) Texture not exist in lists");
	return NULL;
}

pTTexture TTextureListManager::Get(	const int &List, const int &Texture)
{
	return GetTex(List,Texture);
}

pTTexture TTextureListManager::Get(	const int &List,const wstring &Texture)
{
	return GetTex(List,Texture);
}

pTTexture TTextureListManager::Get(	const wstring &List,const int &Texture)
{
	return GetTex(List,Texture);
}

pTTexture TTextureListManager::Get(const wstring &List,const wstring &Texture)
{
	return GetTex(List,Texture);
}

pTTexture TTextureListManager::GetIfExist(const int &List,const wstring &Texture)
{
	int Size = Lists.size();
	if(List >= 0 && List < Size){
		TTextureList *TexList = Lists[List];

		if(TexList->IfExist(Texture) == true){
			return TexList->Get(Texture);
		}
	}
	return NULL;
}

pTTexture TTextureListManager::GetIfExist(	const wstring &List,
											const wstring &Texture)
{
	if(IfExist(List)){
		TTextureList *ListPtr = GetElem(List);
		if(ListPtr->IfExist(Texture)){
			return ListPtr->Get(Texture);
		}
	}
	return NULL;
}

pTTextureList TTextureListManager::Add(const wstring &Name)
{
	Exception(!IfExist(Name), L"in Add(Name) list whis name:" + Name + L" already exist");

	pTTextureList List(new TTextureList);
	List->SetName(Name);
	Lists.push_back(List);

	return List;
}

pTTextureList TTextureListManager::Add(const pTTextureList &List)
{
	Exception(List != NULL, L"in Add(List) list is NULL");

	wstring Name(List->GetName());
	Exception(!IfExist(Name), L"in Add(List) list whis name:" + Name + L" already exist");

	Lists.push_back(List);

	return List;
}

void TTextureListManager::Add(const int &List, const pTTexture &Val)
{
	GetElem(List)->Add(Val);
}

void TTextureListManager::Add(const wstring &List, const pTTexture &Val)
{
	GetElem(List)->Add(Val);
}

bool TTextureListManager::Del(const int &Id)
{
	int Size = Lists.size();

    Exception(Size>0,				L"in Del(Id) List size 0");
	Exception(Id >= 0 && Id < Size,	L"in Del(Id) Id out of range");

	delete Lists[Id];
	Lists.erase(Lists.begin() + Id);
	return (true);
}

bool TTextureListManager::Del(const wstring &Name)
{
	int Size = Lists.size();
    Exception(Size>0, L"in Del(Name) List size 0");

	int Pos  = -1;

	for(int i=0;i<Size;i++){
		if(Lists[i]->GetName() == Name){Pos = i;}
	}

	if(Pos != -1){
		delete Lists[Pos];
		Lists.erase(Lists.begin() + Pos);
		return true;
	}
	return false;
}

bool TTextureListManager::IfExist(const wstring &Name)
{
	int Size = Lists.size();
	for(int i=0;i<Size;i++){
		if(Lists[i]->GetName() == Name){return true;}
	}
	return false;
}

int  TTextureListManager::GetSize()
{
	return(Lists.size());
}

void TTextureListManager::Clear()
{
	int Size = Lists.size();
	for(int i=0;i<Size;i++){
		delete Lists[i];
	}
	Lists.clear();
}

void TTextureListManager::ClearTexturesData()
{
	int Size = Lists.size();
	for(int i=0;i<Size;i++){ Lists[i]->ClearTexturesData(); }
}


