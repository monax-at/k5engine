#include "ColorArray.h"


TColorArray::TColorArray():Exception(L"TColorArray: "), Changed(false)
{
}

TColorArray::TColorArray(const int &Size):Exception(L"TColorArray: "), Changed(false)
{
	if(Size <= 0){ return; }
	Colors.assign(Size, TColor());
}

TColorArray::TColorArray(const TColorArray &Val)
{
	Set(Val);
}

TColorArray::~TColorArray()
{
}


void TColorArray::SetSize(const int &Val)
{
	Colors.clear();
	if(Val <= 0){ return; }
	Colors.assign(Val, TColor());
}

int TColorArray::GetSize() const
{
	return Colors.size();
}

void TColorArray::SetChanged(const bool &Val)
{
	Changed = Val;
}

bool TColorArray::GetChanged() const
{
	return Changed;
}

void TColorArray::operator =(const TColorArray &Val)
{
	Set(Val);
}

void TColorArray::operator = (const TColor &Val)
{
	Set(Val);
}

void TColorArray::operator()(const TColorArray &Val)
{
	Set(Val);
}

void TColorArray::operator()(const TColor &Val)
{
	Set(Val);
}

void TColorArray::operator()(const float &Val)
{
	Set(Val);
}

void TColorArray::operator() (const float &R,const float &G,const float &B)
{
	Set(R,G,B);
}

void TColorArray::operator() (const float &R,const float &G,const float &B,const float &A)
{
	Set(R,G,B,A);
}

void TColorArray::operator()(const int &Index, const enColorComponent &Type, const float &Val)
{
	Set(Index,Type,Val);
}

void TColorArray::operator()(const int &Index,const TColor &Val)
{
	Set(Index,Val);
}

void TColorArray::operator()(const int &Index, const float &R, const float &G, const float &B)
{
	Set(Index,R,G,B);
}

void TColorArray::operator()(const int &Index, const float &R, const float &G,
							 const float &B, const float &A)
{
	Set(Index,R,G,B,A);
}

void TColorArray::Set(const TColorArray &Val)
{
	int Size(Val.GetSize());
	SetSize(Size);

	for(int i=0;i<Size;i++){ Colors[i] = Val.Get(i); }

	SetChanged();
}

void TColorArray::Set(const float &Val)
{
	int Size(Colors.size());
	for(int i=0;i<Size;i++){Colors[i].Set(Val);}
	SetChanged();
}

void TColorArray::Set(const TColor &Val)
{
	int Size(Colors.size());
	for(int i=0;i<Size;i++){Colors[i].Set(Val);}
	SetChanged();
}

void TColorArray::Set (const float &R,const float &G,const float &B)
{
	int Size(Colors.size());
	for(int i=0;i<Size;i++){Colors[i].Set(R,G,B);}
	SetChanged();
}

void TColorArray::Set (const float &R,const float &G,const float &B,const float &A)
{
	int Size(Colors.size());
	for(int i=0;i<Size;i++){Colors[i].Set(R,G,B,A);}
	SetChanged();
}

void TColorArray::Set(const enColorComponent &Type, const float &Val)
{
	int Size(Colors.size());
	for(int i=0;i<Size;i++){Colors[i].Set(Type, Val);}
	SetChanged();
}

void TColorArray::SetRed(const float &Val)
{
	Set(EN_CC_Red, Val);
}

void TColorArray::SetGreen(const float &Val)
{
	Set(EN_CC_Green, Val);
}

void TColorArray::SetBlue(const float &Val)
{
	Set(EN_CC_Blue, Val);
}

void TColorArray::SetAlpha(const float &Val)
{
	Set(EN_CC_Alpha, Val);
}

void TColorArray::Set(const int &Index, const enColorComponent &Type, const float &Val)
{
	try {
		Colors[Index].Set(Type,Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Set(Index,Type,Val) Index out of range"); }
}

void TColorArray::Set(const int &Index, const TColor &Val)
{
	try {
		Colors[Index].Set(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Set(Index,Val) Index out of range"); }
}

void TColorArray::Set(const int &Index, const float &R, const float &G,const float &B)
{
	try {
		Colors[Index].Set(R,G,B);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Set(Index,R,G,B) Index out of range"); }
}

void TColorArray::Set(const int &Index,const float &R, const float &G,const float &B,const float &A)
{
	try {
		Colors[Index].Set(R,G,B,A);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Set(Index,R,G,B,A) Index out of range"); }
}

TColor TColorArray::Get(const int &Index) const
{
	try                     { return Colors[Index]; }
	catch(out_of_range &Ex) { Exception(L"in Get(Index) Index out of range"); }
}

float TColorArray::Get(const int &Index, const enColorComponent &Component) const
{
	try                     { return Colors[Index].Get(Component); }
	catch(out_of_range &Ex) { Exception(L"in Get(Index,Component) Index out of range"); }
	return 0.0f;
}

float TColorArray::GetRed(const int &Index)const
{
	return Get(Index, EN_CC_Red);
}

float TColorArray::GetGreen(const int &Index)const
{
	return Get(Index, EN_CC_Green);
}

float TColorArray::GetBlue(const int &Index)const
{
	return Get(Index, EN_CC_Blue);
}

float TColorArray::GetAlpha(const int &Index)const
{
	return Get(Index, EN_CC_Alpha);
}

void TColorArray::Inc(const TColor &Val)
{
	int Size(Colors.size());
	for(int i=0;i<Size;i++){Colors[i].Inc(Val);}
	SetChanged();
}

void TColorArray::Dec(const TColor &Val)
{
	int Size(Colors.size());
	for(int i=0;i<Size;i++){Colors[i].Dec(Val);}
	SetChanged();
}

void TColorArray::Inc(const enColorComponent &Type, const float &Val)
{
	int Size(Colors.size());
	for(int i=0;i<Size;i++){Colors[i].Dec(Type,Val);}
	SetChanged();
}

void TColorArray::Dec(const enColorComponent &Type, const float &Val)
{
	int Size(Colors.size());
	for(int i=0;i<Size;i++){Colors[i].Dec(Type,Val);}
	SetChanged();
}

void TColorArray::IncRed(const float &Val)
{
	Inc(EN_CC_Red, Val);
}

void TColorArray::DecRed(const float &Val)
{
	Dec(EN_CC_Red, Val);
}

void TColorArray::IncGreen(const float &Val)
{
	Inc(EN_CC_Green, Val);
}

void TColorArray::DecGreen(const float &Val)
{
	Dec(EN_CC_Green, Val);
}

void TColorArray::IncBlue(const float &Val)
{
	Inc(EN_CC_Blue, Val);
}

void TColorArray:: DecBlue(const float &Val)
{
	Dec(EN_CC_Blue, Val);
}

void TColorArray::IncAlpha(const float &Val)
{
	Inc(EN_CC_Alpha, Val);
}

void TColorArray::DecAlpha(const float &Val)
{
	Dec(EN_CC_Alpha, Val);
}

void TColorArray::Inc(const int &Index, const TColor &Val)
{
	try{
		Colors[Index].Inc(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Inc(Index,Val) Index out of range"); }
}

void TColorArray::Dec(const int &Index, const TColor &Val)
{
	try{
		Colors[Index].Dec(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Dec(Index,Val) Index out of range"); }
}

void TColorArray::Inc(const int &Index, const enColorComponent &Type, const float &Val)
{
	try{
		Colors[Index].Inc(Type, Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Inc(Index,Type,Val) Index out of range"); }
}

void TColorArray::Dec(const int &Index, const enColorComponent &Type, const float &Val)
{
	try{
		Colors[Index].Dec(Type, Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Dec(Index,Type,Val) Index out of range"); }
}

void TColorArray::IncRed  (const int &Index, const float &Val)
{
	Inc(Index, EN_CC_Red, Val);
}

void TColorArray::DecRed  (const int &Index, const float &Val)
{
	Dec(Index, EN_CC_Red, Val);
}

void TColorArray::IncGreen(const int &Index, const float &Val)
{
	Inc(Index, EN_CC_Green, Val);
}

void TColorArray::DecGreen(const int &Index, const float &Val)
{
	Dec(Index, EN_CC_Green, Val);
}

void TColorArray::IncBlue (const int &Index, const float &Val)
{
	Inc(Index, EN_CC_Blue, Val);
}

void TColorArray::DecBlue (const int &Index, const float &Val)
{
	Dec(Index, EN_CC_Blue, Val);
}

void TColorArray::IncAlpha(const int &Index, const float &Val)
{
	Inc(Index, EN_CC_Alpha, Val);
}

void TColorArray::DecAlpha(const int &Index, const float &Val)
{
	Dec(Index, EN_CC_Alpha, Val);
}


