#include "TexturePointerList.h"

TTexturePointerList::TTexturePointerList():Exception(L"TTexturePointerList: ")
{
}

TTexturePointerList::~TTexturePointerList()
{
	Clear();
}


pTTexture TTexturePointerList::GetElem(const int &Id)
{
	int Size(Elems.size());
	Exception(Size > 0,				L"in GetElem(Id) list is empty");
	Exception(Id >= 0 && Id < Size,	L"in GetElem(Id) Id out of range");
	return Elems[Id];
}

pTTexture TTexturePointerList::GetElem(const wstring &Name)
{
	int Size(Elems.size());
	Exception(Size > 0, L"in GetElem(Name) list is empty");

	for(int i=0;i<Size;i++){
		if(Elems[i]->GetName() == Name){
			return Elems[i];
		}
	}
	Exception(L"in GetElem(Name) elem whis name: "+Name+L" not exist");
	return NULL;
}

void TTexturePointerList::SetName(const wstring &Val)
{
	Name = Val;
}

wstring TTexturePointerList::GetName() const
{
	return Name;
}

pTTexture TTexturePointerList::operator[](const int &Id)
{
	return GetElem(Id);
}

pTTexture TTexturePointerList::operator[](const wstring &Name)
{
	return GetElem(Name);
}

pTTexture TTexturePointerList::Get(const int &Id)
{
	return GetElem(Id);
}

pTTexture TTexturePointerList::Get(const wstring &Name)
{
	return GetElem(Name);
}

pTTexture TTexturePointerList::GetLast()
{
	int Size(Elems.size());
	Exception(Size > 0,L"in GetLast() list is enpty");
	return Elems[Size-1];
}

int TTexturePointerList::GetSize() const
{
	return Elems.size();
}

void TTexturePointerList::Add(const pTTexture &Texture)
{
	Exception(Texture != NULL, L"in Add(...) texture is NULL");
	Elems.push_back(Texture);
}

void TTexturePointerList::Del(const int &Id)
{
	int Size(Elems.size());
	Exception(Size > 0,				L"in Delete(Id) list is empty");
	Exception(Id >= 0 && Id < Size,	L"in Delete(Id) Id out of range");
	Elems.erase(Elems.begin() + Id);
}

void TTexturePointerList::Del(const wstring &Name)
{
    int Size(Elems.size());
    Exception(Size > 0, L"in Del(Id) list is empty");

    int ErasePos(-1);

    for(int i=0;i<Size;i++){
        if(Elems[i]->GetName() == Name){
        	ErasePos = i;
        	break;
		}
    }

	Exception(ErasePos != -1, L"in Del(Name) elem whis name: "+Name+L" not exist");

	Elems.erase(Elems.begin()+ErasePos);
}

bool TTexturePointerList::IfExist(const unsigned long &ID)
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){ if(Elems[i]->GetID() == ID){return true;} }
	return false;
}

bool TTexturePointerList::IfExist(const wstring &Name)
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){ if(Elems[i]->GetName() == Name){return true;} }
	return false;
}

void TTexturePointerList::Clear()
{
	Elems.clear();
}

void TTexturePointerList::ClearTexturesData()
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){ Elems[i]->Del(); }
}


