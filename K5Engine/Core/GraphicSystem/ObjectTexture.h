///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef OBJECTTEXTURE_H_INCLUDED
#define OBJECTTEXTURE_H_INCLUDED

#include "Base/Texture.h"
#include "../PointArray.h"

enum enTextureUseState
{
	EN_TUS_Repeat,
	EN_TUS_Rect
};

enum enTextureCoordinatesState
{
	EN_TCS_Fixed,
	EN_TCS_Floating
};


class TObjectTexture
{
	protected:
		pTTexture Texture;

		enTextureUseState         UseState;
		enTextureCoordinatesState CoordinatesState;

		float TexRepeatX;
		float TexRepeatY;

		float TexRectX;
		float TexRectY;
		float TexRectWidth;
		float TexRectHeight;

		bool MirrorX;
		bool MirrorY;

		bool Changed;
	protected:
		void SetTextureRect(	const float &X, const float &Y,
								const float &Width, const float &Height);

		void SetTexture(const pTTexture      &NewTexture);
		void SetTexture(const TObjectTexture &NewElem);

		void SetTextureRepeat(const float &X,const float &Y);
		void SetTextureRepeatX(const float &Val);
		void SetTextureRepeatY(const float &Val);

		virtual void UpdateData() =0;
	public:
		TObjectTexture ();
		~TObjectTexture();

		void Set(const pTTexture &Val);
		void Set(const TObjectTexture &Val);

		pTTexture Get() const;

		void UnSet ();
		bool IsInit();

		void SetRepeat(const float &Val);
		void SetRepeat(const float &RepeatX,const float &RepeatY);
		void SetRepeatX(const float &Val);
		void SetRepeatY(const float &Val);

		float GetRepeatX() const;
		float GetRepeatY() const;

		void SetRect(	const float &X,const float &Y,
						const float &Width,const float &Height);

		void SetRect(const TPoint &Coord,const TPoint &Sizes);

		void SetRectPos(const float &X,const float &Y);
		void SetRectSize(const float &Width,const float &Height);

		void SetRectPosX(const float &Val);
		void SetRectPosY(const float &Val);

		void SetRectWidth(const float &Val);
		void SetRectHeight(const float &Val);

		void IncRectPos(const float &X,const float &Y);
		void DecRectPos(const float &X,const float &Y);
		void IncRectPosX(const float &Val);
		void DecRectPosX(const float &Val);
		void IncRectPosY(const float &Val);
		void DecRectPosY(const float &Val);

		float GetRectX() const;
		float GetRectY() const;
		float GetRectWidth() const;
		float GetRectHeight() const;

		void SetMirror (const bool &X,const bool &Y);
		void SetMirrorX(const bool &Val);
		void SetMirrorY(const bool &Val);
		bool GetMirrorX() const;
		bool GetMirrorY() const;

		void SetUseState(const enTextureUseState &Val);
		enTextureUseState GetUseState() const;

		void SetCoordinatesState(const enTextureCoordinatesState &Val);
		enTextureCoordinatesState GetCoordinatesState() const;

		int GetWidth()  const;
		int GetHeight() const;

		// установка и взятие флага, который показывает, были ли изменения в текстуре объекта
		void SetChanged(const bool &Val = true);
		bool GetChanged() const;

		void Run ();
		void Stop();
};


#endif // OBJECTTEXTURE_H_INCLUDED
