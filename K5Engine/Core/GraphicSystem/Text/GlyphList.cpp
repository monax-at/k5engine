#include "GlyphList.h"


TGlyphList::TGlyphList()
{
}

TGlyphList::~TGlyphList()
{
	Clear();
}


pTGlyph TGlyphList::GetElemByIndex(const int &Id)
{
	int Size = Elems.size();
	assert(Size != 0);
	assert(Id >= 0 && Id < Size);
	return Elems[Id];
}

pTGlyph TGlyphList::GetElemBySymbol(const wchar_t &Symbol)
{
	int Size = Elems.size();
	assert(Size != 0);

	TGlyph *ResultTexture = NULL;

	for(int i=0;i<Size;i++){
		if(Elems[i]->GetSymbol() == Symbol){
			ResultTexture = Elems[i];
			break;
		}
	}
	assert(ResultTexture != NULL);
	return ResultTexture;
}

void TGlyphList::SetName(const wstring &Val)
{
	Name = Val;
}

wstring TGlyphList::GetName()
{
	return Name;
}

pTGlyph TGlyphList::operator[](const int &Id)
{
	return GetElemByIndex(Id);
}

pTGlyph TGlyphList::operator[](const wchar_t &Symbol)
{
	return GetElemBySymbol(Symbol);
}

pTGlyph TGlyphList::Get(const int &Id)
{
	return GetElemByIndex(Id);
}

pTGlyph TGlyphList::Get(const wchar_t &Symbol)
{
	return GetElemBySymbol(Symbol);
}

pTGlyph TGlyphList::GetLast()
{
	return GetElemByIndex(Elems.size()-1);
}

int  TGlyphList::GetSize()
{
	return Elems.size();
}

void TGlyphList::Add(TGlyph *Val)
{
	assert(Val != NULL);
	Elems.push_back(Val);
}

void TGlyphList::Add(const TGlyph &Val)
{
	TGlyph *Glyph = new TGlyph;
	*Glyph = Val;
	Elems.push_back(Glyph);
}

void TGlyphList::Del(const int &Id)
{
	int Size = Elems.size();
	assert(Size > 0);
	assert(Id >= 0 && Id < Size);

	Elems[Id]->GetTexture()->Del();
	delete Elems[Id];
	Elems.erase(Elems.begin() + Id);
}

void TGlyphList::Del(const wchar_t &Symbol)
{
    int Size = Elems.size();
    assert(Size > 0);

    int ErasePos = -1;

    for(int i=0;i<Size;i++){
        if(Elems[i]->GetSymbol() == Symbol){
        	ErasePos = i;
        	break;
		}
    }
    if(ErasePos != -1){
    	Elems[ErasePos]->GetTexture()->Del();
    	delete Elems[ErasePos];
    	Elems.erase(Elems.begin()+ErasePos);
    }
}

bool TGlyphList::IsExist(const wchar_t &Symbol)
{
	int Size = Elems.size();
	for(int i=0;i<Size;i++){
		if(Elems[i]->GetSymbol() == Symbol){return true;}
	}
	return false;
}

void TGlyphList::Clear()
{
	int Size = Elems.size();
	for(int i=0;i<Size;i++){
		Elems[i]->GetTexture()->Del();
		delete Elems[i];
	}
	Elems.clear();
}


