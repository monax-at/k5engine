///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef TEXTLISTMANAGER_H_INCLUDED
#define TEXTLISTMANAGER_H_INCLUDED

#include "../Base/BaseGraphicObjectListManager.h"
#include "TextList.h"

class TTextListManager: public TBaseGraphicObjectListManager<TText,TTextList>
{
	public:
		TTextListManager();
		virtual ~TTextListManager();
};

typedef TTextListManager* pTTextListManager;


#endif // TEXTLISTMANAGER_H_INCLUDED
