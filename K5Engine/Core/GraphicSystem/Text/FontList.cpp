#include "FontList.h"


TFontList::TFontList():Exception(L"TFontList: "), Device(NULL)
{
}

TFontList::~TFontList()
{
	Clear();
}


inline pTBaseFont TFontList::GetElem(const int &Id)
{
	int Size = Elems.size();
	Exception(Id >= 0 || Id < Size, L"in GetElem(Id) Id not in range");
	return Elems[Id];
}

inline pTBaseFont TFontList::GetElem(const wstring &Name)
{
	int Size = Elems.size();
	Exception(Size != 0,L"in GetElem(Name) list is empty");

	for(int i=0; i<Size; i++){
		if(Elems[i]->GetName() == Name){
			return Elems[i];
		}
	}
	Exception(L"in GetElem(Name) elem whis name "+Name+L" not exist");
	return NULL;
}

pTBaseFont TFontList::operator[](const int &Id)
{
	return GetElem(Id);
}

pTBaseFont TFontList::operator[](const wstring &Name)
{
	return GetElem(Name);
}

pTBaseFont TFontList::Get(const int &Id)
{
	return GetElem(Id);
}

pTBaseFont TFontList::Get(const wstring &Name)
{
	return GetElem(Name);
}

void TFontList::Add(const pTBaseFont &Font)
{
	Elems.push_back(Font);
}

bool TFontList::IsExist(const wstring &Name) const
{
	int Size(Elems.size());
	for(int i=0; i<Size; i++){ if(Elems[i]->GetName() == Name){return true;} }
	return false;
}

void TFontList::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;

	int Size(Elems.size());
	if(Size <= 0){return;}

	for(int i=0;i<Size;i++){Elems[i]->SetDevice(Device);}
}

void TFontList::SetDevice(TBaseDevice &Val)
{
	SetDevice(&Val);
}

pTBaseDevice TFontList::GetDevice()
{
	return Device;
}

int TFontList::GetSize() const
{
	return Elems.size();
}

void TFontList::Clear()
{
	int Size = Elems.size();
	for(int i=0; i<Size; i++){delete Elems[i];}
	Elems.clear();
}

void TFontList::ClearGlyphs()
{
	int Size = Elems.size();
	for(int i=0; i<Size; i++){Elems[i]->Clear();}
}


