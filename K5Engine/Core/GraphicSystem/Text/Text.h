///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef TEXT_H_INCLUDED
#define TEXT_H_INCLUDED

#include "BaseFont.h"
#include "../Sprite/SpriteList.h"

class TText: public TBaseGraphicObject
{
	protected:
		wstring 	Text;
		pTBaseFont  Font;

		pTBaseViewMatrixWorker ViewMatrix;

		TSpriteList	Line;

		enAlignment	Alignment;

		float TextWidth;
		float TextHeight;

		TColor OldColor;
		TPoint RealDrawPos;
	public:
		TPoint Scale;
		TColor Color;
	protected:
		inline void GenLine(const wstring &TextLine);
		inline void InitData();

		void ToSetDevice();
		void ToDraw();
	public:
		TText();
		TText(const TText &Val);
		virtual ~TText();

		void operator=(const TText &Val);

		void SetFont(const pTBaseFont &Val);
		pTBaseFont GetFont() const;

		void Set(const wstring &Val);
		void Set(const int &Val);
		void Set(const float &Val);
		wstring Get() const;

		void Add(const wchar_t &Val);
		void Add(const wstring &Val);

		void Del();

		int GetFontSize() const;

		float GetWidth()  const;
		float GetHeight() const;

		void SetAlignment(const enAlignment &Value);
		enAlignment GetAlignment() const;

		pTSpriteList GetLinePtr();
		int GetStrLength() const;
};

typedef TText* pTText;


#endif // TEXT_H_INCLUDED
