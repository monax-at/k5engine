

///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEFONT_H_INCLUDED
#define BASEFONT_H_INCLUDED

#include "../../BaseDevice.h"

#include "GlyphList.h"

#include <string>

using std::wstring;


class TBaseFont
{
	protected:
		wstring		 Name;
		pTBaseDevice Device;
		TGlyphList   GlyphList;
		int          FontSize;
	protected:
		virtual void ToClear();
	public:
		TBaseFont();
		virtual ~TBaseFont();

		void SetName(const wstring &Val);
		wstring GetName() const;

		void SetDevice(const pTBaseDevice &Val);
		pTBaseDevice GetDevice() const;

		virtual pTGlyph GetGlyph(const wchar_t &Symbol) = 0;

		void SetFontSize(const int &Val);
		int GetFontSize() const;

		void Clear();
};

typedef TBaseFont* pTBaseFont;


#endif // BASEFONT_H_INCLUDED
