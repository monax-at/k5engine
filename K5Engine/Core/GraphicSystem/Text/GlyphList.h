///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef GLGLYPHLIST_H_INCLUDED
#define GLGLYPHLIST_H_INCLUDED

#include "Glyph.h"

#include <vector>
#include <string>
#include <assert.h>

using std::vector;
using std::wstring;


class TGlyphList
{
	protected:
		wstring Name;
		vector<pTGlyph> Elems;
	protected:
		inline pTGlyph GetElemByIndex(const int &Id);
		inline pTGlyph GetElemBySymbol(const wchar_t &Symbol);
	public:
		TGlyphList();
		virtual ~TGlyphList();

		void SetName(const wstring &Val);
        wstring GetName();

		pTGlyph operator[](const int &Id);
		pTGlyph operator[](const wchar_t &Symbol);

        pTGlyph Get(const int &Id);
        pTGlyph Get(const wchar_t &Symbol);
        pTGlyph GetLast();

        int  GetSize();

        void Add(TGlyph *Val);
        void Add(const TGlyph &Val);

        void Del(const int &Id);
        void Del(const wchar_t &Symbol);

        bool IsExist(const wchar_t &Symbol);

        void Clear();
};


#endif // CLASS_GLGLYPHLIST_H_INCLUDED
