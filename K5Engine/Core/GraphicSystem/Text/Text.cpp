#include "Text.h"
#include "../../../LibMath/LibMath.h"


TText::TText():TBaseGraphicObject(), Font(NULL),ViewMatrix(NULL),
				Alignment(EN_A_Center), TextWidth(0.0f), TextHeight(0.0f), Scale(1.0f)
{
}

TText::TText(const TText &Val):TBaseGraphicObject(),Font(NULL),ViewMatrix(NULL),
				Alignment(EN_A_Center), TextWidth(0.0f), TextHeight(0.0f), Scale(1.0f)
{
	*this = Val;
}

TText::~TText()
{
	delete ViewMatrix;
}


void TText::GenLine(const wstring &TextLine)
{
	int TextLineCount(TextLine.length());

	TPoint LinePos(Pos);
	float LineLength(0.0f);
	float CursorPosX(0.0f);

	for(int i=0;i<TextLineCount;i++){
		wchar_t wcChar(TextLine[i]);

		if(wcChar != L'\n' && wcChar != L'\t'){
			pTGlyph   Glyph  (Font->GetGlyph(wcChar));
			pTTexture Texture(Glyph->GetTexture());

			float GlyphWidth (0.0f);
			float GlyphHeight(0.0f);

			if(Glyph->GetUseRectFlag()){
				GlyphWidth  = Glyph->GetRectWidth();
				GlyphHeight = Glyph->GetRectHeight();
			}
			else{
				GlyphWidth  = (float)Texture->GetWidth();
				GlyphHeight = (float)Texture->GetHeight();
			}

			float CurPosX = CursorPosX + Glyph->GetShiftX() + GlyphWidth/2;
			float CurPosY = GlyphHeight/2.0f - Glyph->GetShiftY();

			CursorPosX += Glyph->GetCursorShift();
			LineLength += Glyph->GetCursorShift();

			TSprite Sprite;

			Sprite.Pos (CurPosX, CurPosY);
			Sprite.Size(GlyphWidth, GlyphHeight);

			Sprite.Color(Color);
			Sprite.Texture(Texture);

			if(Glyph->GetUseRectFlag()){
				Sprite.Texture.SetRect(Glyph->GetRectPos(),Glyph->GetRectSize());
			}

			Line.Add(Sprite);
		}
	}

	TextWidth  = LineLength;
	TextHeight = (float)Font->GetFontSize();
}

inline void TText::InitData()
{
	Line.Clear();

	OldColor = Color;

	TextWidth 	= 0;
	TextHeight	= 0;

	if(Device == NULL)	{return;}
	if(Font   == NULL)	{return;}

	if(Text.length() == 0){ return; }

	GenLine(Text);
}

void TText::operator=(const TText &Val)
{
	Del();

	SetDefParams(&Val);

	Scale = Val.Scale;
	Color = Val.Color;

	Font      = Val.GetFont();
	Alignment = Val.GetAlignment();

	Set(Val.Get());
}

void TText::SetFont(const pTBaseFont &Val)
{
	Font = Val;
	InitData();
}

pTBaseFont TText::GetFont() const
{
	return Font;
}

void TText::Set(const wstring &Val)
{
	Text = Val;
	InitData();
}

void TText::Set(const int &Val)
{
	Text = IntToWStr(Val);
	InitData();
}

void TText::Set(const float &Val)
{
	Text = FloatToWStr(Val);
	InitData();
}

wstring TText::Get() const
{
	return Text;
}

void TText::Add(const wchar_t &Val)
{
	Text += Val;
	InitData();
}

void TText::Add(const wstring &Val)
{
	Text += Val;
	InitData();
}

void TText::Del()
{
	if(Text.length() == 0){return;}
	Text = Text.substr(0,Text.length()-1);
	InitData();
}

int TText::GetFontSize() const
{
	if(Font!=NULL){return Font->GetFontSize();}
	return 0;
}

float TText::GetWidth() const
{
	return TextWidth;
}

float TText::GetHeight() const
{
	return TextHeight;
}

void TText::SetAlignment(const enAlignment &Value)
{
	Alignment = Value;
}

enAlignment TText::GetAlignment() const
{
	return Alignment;
}

pTSpriteList TText::GetLinePtr()
{
	return &Line;
}

void TText::ToSetDevice()
{
	delete ViewMatrix;
	ViewMatrix = NULL;
	if(Device != NULL){ ViewMatrix = Device->CreateViewMatrixWorker(); }

	Line.Clear();
	Line.SetDevice(Device);

	InitData();
}

void TText::ToDraw()
{
	if(Color != OldColor){
		int LineSize(Line.GetSize());
		for(int i=0;i<LineSize;i++){Line[i]->Color(Color);}
		OldColor = Color;
	}

	float ScaleX(Scale.GetX());
	float ScaleY(Scale.GetY());

	float PosX(Pos[0]);

	switch(Alignment){
		case EN_A_Left	:{ PosX = Pos[0]; 						    }break;
		case EN_A_Center:{ PosX = Pos[0] - (TextWidth/2.0f)*ScaleX; }break;
		case EN_A_Right	:{ PosX = Pos[0] - TextWidth*ScaleX; 	    }break;
		default:break;
	}

	RealDrawPos.Set(IntegerPart(PosX), IntegerPart(Pos[1] + (TextHeight/4.0f)*ScaleY), Pos[2]);

	ViewMatrix->Clear();

	ViewMatrix->Translate	(RealDrawPos.GetX(),RealDrawPos.GetY(),RealDrawPos.GetZ());
	ViewMatrix->Rotate		(Angle.Get());
	ViewMatrix->Translate	(-Center.GetX(), -Center.GetY(), -Center.GetZ());
	ViewMatrix->Scale		(ScaleX, ScaleY);

	ViewMatrix->Run();

	Line.Draw();

	ViewMatrix->Stop();
}

int TText::GetStrLength() const
{
	return Text.length();
}


