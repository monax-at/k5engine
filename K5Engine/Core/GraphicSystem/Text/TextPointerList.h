///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef TEXTPOINTERLIST_H_INCLUDED
#define TEXTPOINTERLIST_H_INCLUDED

#include "../Base/BaseGraphicObjectPointerList.h"
#include "Text.h"

class TTextPointerList:public TBaseGraphicObjectPointerList<TText>
{
	public:
		TTextPointerList();
		virtual ~TTextPointerList();
};


#endif // TEXTPOINTERLIST_H_INCLUDED
