

///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BITMAPFONT_H_INCLUDED
#define BITMAPFONT_H_INCLUDED

#include "../TexturePointerList.h"
#include "BaseFont.h"
#include "BitmapFontCell.h"

class TBitmapFont:public TBaseFont
{
	protected:
		vector<TBitmapFontCell> GlyphsInfo;
		TTexturePointerList Textures;
	protected:
		void ToClear();
	public:
		TBitmapFont();
		virtual ~TBitmapFont();

		pTGlyph GetGlyph(const wchar_t &Symbol);

		void AddFontTexture(const pTTexture &Val);

		void AddGlyphInfo(const TBitmapFontCell &Val);

		void AddGlyphInfo(	const wchar_t &CharVal, const int &TextureIdVal,
							const int &RectXVal, const int &RectYVal,
							const int &RectWidthVal, const int &RectHeightVal,
							const int &CharShiftXVal, const int &CharShiftYVal,
							const int &TextCursorShiftVal);

		void ClearGlyphsInfo();
};

typedef TBitmapFont* pTBitmapFont;


#endif // BITMAPFONT_H_INCLUDED
