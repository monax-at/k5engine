///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef GLYPH_H_INCLUDED
#define GLYPH_H_INCLUDED

#include "../Base/Texture.h"
#include "../../Point.h"

class TGlyph
{
	protected:
		pTTexture Texture;

		float 	CursorShift;

		float 	ShiftX;
		float 	ShiftY;

		int 	GlyphWidth;
		int 	GlyphHeight;

		wchar_t Symbol;

		bool UseRect;

		TPoint RectPos;
		TPoint RectSize;
	public:
		TGlyph();
		~TGlyph();

		void SetTexture(const pTTexture &Val);
		pTTexture GetTexture();

		bool IsEmptySymbol();

		void     SetSymbol(const wchar_t &Val);
		wchar_t  GetSymbol() const;

		void  SetShifts(const float &cursor,const float &x,const float &y);

		void  SetCursorShift(const float &Val);
		void  SetShiftX(const float &Val);
		void  SetShiftY(const float &Val);

		float GetCursorShift() const;
		float GetShiftX() const;
		float GetShiftY() const;

		void SetGlyphSizes(const int &iWidth,const int &iHeight);
		void SetGlyphWidth(const int &Val);
		void SetGlyphHeight(const int &Val);

		int GetGlyphWidth() const;
		int GetGlyphHeight() const;

		void SetUseRectFlag(const bool &Val);
		bool GetUseRectFlag() const;

		void SetRect(	const float &Rx, const float &Ry,
						const float &RWidth, const float &RHeight);

		float GetRectX() const;
		float GetRectY() const;

		TPoint GetRectPos() const;

		float GetRectWidth() const;
		float GetRectHeight() const;

		TPoint GetRectSize() const;
};

typedef TGlyph* pTGlyph;


#endif // GLYPH_H_INCLUDED
