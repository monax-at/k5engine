#include "BitmapFontCell.h"


TBitmapFontCell::TBitmapFontCell(): TextureId(-1),RectX(0),RectY(0),
									RectWidth(0),RectHeight(0),CharShiftX(0),CharShiftY(0),
									TextCursorShift(0)
{
}

TBitmapFontCell::~TBitmapFontCell()
{
}


void TBitmapFontCell::SetParams(const wchar_t &CharVal, const int &TextureIdVal,
								const int &RectXVal, const int &RectYVal,
								const int &RectWidthVal, const int &RectHeightVal,
								const int &CharShiftXVal, const int &CharShiftYVal,
								const int &TextCursorShiftVal)
{
	Char = CharVal;
	TextureId = TextureIdVal;

	RectX = RectXVal;
	RectY = RectYVal;

	RectWidth = RectWidthVal;
	RectHeight = RectHeightVal;

	CharShiftX = CharShiftXVal;
	CharShiftY = CharShiftYVal;

	TextCursorShift = TextCursorShiftVal;
}


