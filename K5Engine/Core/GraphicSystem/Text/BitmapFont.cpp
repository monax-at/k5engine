#include "BitmapFont.h"


TBitmapFont::TBitmapFont():TBaseFont()
{
}

TBitmapFont::~TBitmapFont()
{
}


void TBitmapFont::ToClear()
{
	GlyphsInfo.clear();
}

pTGlyph TBitmapFont::GetGlyph(const wchar_t  &Symbol)
{
	if( GlyphList.IsExist(Symbol) ){ return GlyphList[Symbol]; }

	TBitmapFontCell Cell;
	int Size(GlyphsInfo.size());

	bool FindCellFlag(false);
	for(int i=0;i<Size;i++){
		if(GlyphsInfo[i].Char == Symbol){
			FindCellFlag = true;
			Cell = GlyphsInfo[i];
			break;
		}
	}

	TExceptionGenerator Ex(L"TBitmapFont: ");
	Ex(FindCellFlag, L"in GetGlyph(...) none char info for:" + Symbol);

	pTGlyph Glyph(new TGlyph);

	Glyph->SetSymbol(Cell.Char);

	Glyph->SetShifts((float)Cell.TextCursorShift, (float)Cell.CharShiftX, (float)Cell.CharShiftY);
	Glyph->SetGlyphSizes(Cell.RectWidth, Cell.RectHeight);
	Glyph->SetTexture(Textures.Get(Cell.TextureId));

	Glyph->SetUseRectFlag(true);
	Glyph->SetRect((float)Cell.RectX, (float)Cell.RectY, (float)Cell.RectWidth, (float)Cell.RectHeight);

	GlyphList.Add(Glyph);

	return Glyph;
}

void TBitmapFont::AddFontTexture(const pTTexture &Val)
{
	Textures.Add(Val);
}

void TBitmapFont::AddGlyphInfo(const TBitmapFontCell &Val)
{
	GlyphsInfo.push_back(Val);
}

void TBitmapFont::AddGlyphInfo(	const wchar_t &CharVal, const int &TextureIdVal,
								const int &RectXVal, const int &RectYVal,
								const int &RectWidthVal, const int &RectHeightVal,
								const int &CharShiftXVal, const int &CharShiftYVal,
								const int &TextCursorShiftVal)
{
	TBitmapFontCell Cell;

	Cell.SetParams(	CharVal,TextureIdVal,RectXVal,RectYVal,RectWidthVal,RectHeightVal,
					CharShiftXVal, CharShiftYVal, TextCursorShiftVal);

	GlyphsInfo.push_back(Cell);
}


