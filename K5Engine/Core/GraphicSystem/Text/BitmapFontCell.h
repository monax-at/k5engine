///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BITMAPFONTCELL_H_INCLUDED
#define BITMAPFONTCELL_H_INCLUDEDj

class TBitmapFontCell
{
	public:
		wchar_t Char;
		int TextureId;

		int RectX;
		int RectY;

		int RectWidth;
		int RectHeight;

		int CharShiftX;
		int CharShiftY;

		int TextCursorShift;
	public:
		TBitmapFontCell();
		virtual ~TBitmapFontCell();

		void SetParams(	const wchar_t &CharVal, const int &TextureIdVal,
						const int &RectXVal, const int &RectYVal,
						const int &RectWidthVal, const int &RectHeightVal,
						const int &CharShiftXVal, const int &CharShiftYVal,
						const int &TextCursorShiftVal);
};

typedef TBitmapFontCell* pTBitmapFontCell;


#endif // TBITMAPFONTCELL_H_INCLUDED
