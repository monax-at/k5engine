#include "BaseFont.h"


TBaseFont::TBaseFont():Device(NULL),FontSize(16)
{
}

TBaseFont::~TBaseFont()
{
	Clear();
}


void TBaseFont::ToClear()
{
}

void TBaseFont::SetName(const wstring &Val)
{
	Name = Val;
}

wstring TBaseFont::GetName() const
{
	return Name;
}

void TBaseFont::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
}

pTBaseDevice TBaseFont::GetDevice() const
{
	return Device;
}

void TBaseFont::SetFontSize(const int &Val)
{
	FontSize = Val;
}

int TBaseFont::GetFontSize() const
{
	return FontSize;
}

void TBaseFont::Clear()
{
	GlyphList.Clear();

	ToClear();
}


