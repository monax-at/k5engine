///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef TEXTLIST_H_INCLUDED
#define TEXTLIST_H_INCLUDED

#include "../Base/BaseGraphicObjectList.h"
#include "Text.h"

class TTextList:public TBaseGraphicObjectList<TText>
{
	public:
		TTextList();
		virtual ~TTextList();
};

typedef TTextList* pTTextList;


#endif // TEXTLIST_H_INCLUDED
