///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef FONTLIST_H_INCLUDED
#define FONTLIST_H_INCLUDED

#include "BaseFont.h"
#include "BitmapFont.h"

#include <vector>
#include <string>

using std::vector;
using std::wstring;

class TFontList
{
	protected:
		TExceptionGenerator Exception;

		pTBaseDevice 		Device;
		vector<pTBaseFont> 	Elems;
	protected:
        inline pTBaseFont GetElem(const int &Id);
        inline pTBaseFont GetElem(const wstring &Name);
	public:
		TFontList();
		~TFontList();

        pTBaseFont operator[](const int &Id);
        pTBaseFont operator[](const wstring &Name);

        pTBaseFont Get(const int &Id);
        pTBaseFont Get(const wstring &Name);

		void Add(const pTBaseFont &Font);

		bool IsExist(const wstring &Name) const;

        void SetDevice(const pTBaseDevice &Val);
        void SetDevice(TBaseDevice &Val);
        pTBaseDevice GetDevice();

        int  GetSize() const;
		void Clear();
		void ClearGlyphs();
};

typedef TFontList* pTFontList;


#endif // FONTLIST_H_INCLUDED
