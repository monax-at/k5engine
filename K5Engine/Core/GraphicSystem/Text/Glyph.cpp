#include "Glyph.h"


TGlyph::TGlyph():Texture(NULL),CursorShift(0),ShiftX(0),ShiftY(0),UseRect(false)
{
}

TGlyph::~TGlyph()
{
}


void TGlyph::SetTexture(const pTTexture &Val)
{
	Texture = Val;
}

pTTexture TGlyph::GetTexture()
{
	return Texture;
}

bool TGlyph::IsEmptySymbol()
{
	if(Texture == NULL){return false;}
	return true;
}

void TGlyph::SetSymbol(const wchar_t  &Val)
{
    Symbol = Val;
}

wchar_t  TGlyph::GetSymbol() const
{
    return Symbol;
}

void  TGlyph::SetShifts(const float &cursor,const float &x,const float &y)
{
	CursorShift = cursor;
	ShiftX = x;
	ShiftY = y;
}

void  TGlyph::SetCursorShift(const float &Val)
{
	CursorShift = Val;
}

void  TGlyph::SetShiftX(const float &Val)
{
	ShiftX = Val;
}

void  TGlyph::SetShiftY(const float &Val)
{
	ShiftY = Val;
}

float TGlyph::GetCursorShift() const
{
	return (CursorShift);
}

float TGlyph::GetShiftX() const
{
	return (ShiftX);
}

float TGlyph::GetShiftY() const
{
	return (ShiftY);
}

void TGlyph::SetGlyphSizes(const int &iWidth,const int &iHeight)
{
	GlyphWidth  = iWidth;
	GlyphHeight = iHeight;
}

void TGlyph::SetGlyphWidth(const int &Val)
{
	GlyphWidth = Val;
}

void TGlyph::SetGlyphHeight(const int &Val)
{
	GlyphHeight = Val;
}

int TGlyph::GetGlyphWidth() const
{
	return GlyphWidth;
}

int TGlyph::GetGlyphHeight() const
{
	return GlyphHeight;
}

void TGlyph::SetUseRectFlag(const bool &Val)
{
	UseRect = Val;
}

bool TGlyph::GetUseRectFlag() const
{
	return UseRect;
}

void TGlyph::SetRect(	const float &Rx, const float &Ry,
						const float &RWidth, const float &RHeight)
{
	RectPos(Rx,Ry);
	RectSize(RWidth,RHeight);
}

float TGlyph::GetRectX() const
{
	return RectPos.GetX();
}

float TGlyph::GetRectY() const
{
	return RectPos.GetY();
}

TPoint TGlyph::GetRectPos() const
{
	return RectPos;
}

float TGlyph::GetRectWidth() const
{
	return RectSize.GetX();
}

float TGlyph::GetRectHeight() const
{
	return RectSize.GetY();
}

TPoint TGlyph::GetRectSize() const
{
	return RectSize;
}


