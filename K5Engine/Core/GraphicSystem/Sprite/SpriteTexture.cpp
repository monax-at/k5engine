#include "SpriteTexture.h"

TSpriteTexture::TSpriteTexture():TObjectTexture()
{
	Points[0]( 1.0f, 1.0f);
	Points[1]( 0.0f, 1.0f);
	Points[2]( 0.0f, 0.0f);
	Points[3]( 1.0f, 0.0f);
}

TSpriteTexture::~TSpriteTexture()
{
}

void TSpriteTexture::BuilCoordRepeat()
{
	float MinX(0.0f);
	float MinY(0.0f);

	float MaxX(1.0f*TexRepeatX);
	float MaxY(1.0f*TexRepeatY);

    if(MirrorX){
    	float Temp(MinX);
    	MinX = MaxX;
    	MaxX = Temp;
    }

    if(MirrorY){
    	float Temp(MinY);
    	MinY = MaxY;
    	MaxY = Temp;
    }

	Points[0]( MaxX, MaxY);
	Points[1]( MinX, MaxY);
	Points[2]( MinX, MinY);
	Points[3]( MaxX, MinY);
}

void TSpriteTexture::BuilCoordRect()
{
	if(Texture == NULL){return;}

	float WidthCof (1.0f/Texture->GetWidth());
	float HeightCof(1.0f/Texture->GetHeight());

	float MinX(WidthCof *TexRectX);
	float MinY(HeightCof*TexRectY);

	float MaxX(WidthCof *(TexRectX+TexRectWidth));
	float MaxY(HeightCof*(TexRectY+TexRectHeight));

    if(MirrorX){
    	float Temp(MinX);
    	MinX = MaxX;
    	MaxX = Temp;
    }

    if(MirrorY){
    	float Temp(MinY);
    	MinY = MaxY;
    	MaxY = Temp;
    }

	Points[0]( MaxX, MaxY);
	Points[1]( MinX, MaxY);
	Points[2]( MinX, MinY);
	Points[3]( MaxX, MinY);
}

void TSpriteTexture::UpdateData()
{
	switch(UseState){
		case EN_TUS_Repeat	:{ BuilCoordRepeat();}break;
		case EN_TUS_Rect	:{ BuilCoordRect()	;}break;
	}
}

void TSpriteTexture::BuildCoordinatesByMesh(const pTPointArray &Mesh)
{
	Points[0]( 1.0f, 1.0f);
	Points[1]( 0.0f, 1.0f);
	Points[2]( 0.0f, 0.0f);
	Points[3]( 1.0f, 0.0f);

	switch(UseState){
		case EN_TUS_Repeat	:{ BuilCoordRepeat();}break;
		case EN_TUS_Rect	:{ BuilCoordRect()	;}break;
	}

	float TexWidth (0.0f);
	float TexHeight(0.0f);

	switch(UseState){
		case EN_TUS_Repeat:{
			TexWidth  = TexRepeatX;
			TexHeight = TexRepeatY;
		}break;

		case EN_TUS_Rect:{
			float WidthCof (1.0f/Texture->GetWidth());
			float HeightCof(1.0f/Texture->GetHeight());

			TexWidth  = WidthCof *TexRectWidth;
			TexHeight = HeightCof*TexRectHeight;
		}break;
	}

	float CofX(0.0f);
	float CofY(0.0f);

	float ModeMirrorX(1.0f);
	float ModeMirrorY(1.0f);

    if(MirrorX){ ModeMirrorX = -1.0f; }
    if(MirrorY){ ModeMirrorY = -1.0f; }

	CofX = ModeMirrorX*(0 + Mesh->GetX(0))*TexWidth;
	CofY = ModeMirrorY*(0 + Mesh->GetY(0))*TexHeight;
	Points[0].Inc(CofX,CofY);

	CofX = ModeMirrorX*(1 + Mesh->GetX(1))*TexWidth;
	CofY = ModeMirrorY*(0 + Mesh->GetY(1))*TexHeight;
	Points[1].Inc(CofX,CofY);

	CofX = ModeMirrorX*(1 + Mesh->GetX(2))*TexWidth;
	CofY = ModeMirrorY*(1 + Mesh->GetY(2))*TexHeight;
	Points[2].Inc(CofX,CofY);

	CofX = ModeMirrorX*(0 + Mesh->GetX(3))*TexWidth;
	CofY = ModeMirrorY*(1 + Mesh->GetY(3))*TexHeight;
	Points[3].Inc(CofX,CofY);
}

void TSpriteTexture::operator =(const pTTexture &Val)
{
	SetTexture(Val);
}

void TSpriteTexture::operator =(const TSpriteTexture &Val)
{
	for(int i=0;i<4;i++){ Points[i] = Val.GetPoint(i); }
	SetTexture(Val);
}

void TSpriteTexture::operator ()(const pTTexture &Val)
{
	SetTexture(Val);
}

void TSpriteTexture::operator ()(const TSpriteTexture &Val)
{
	for(int i=0;i<4;i++){ Points[i] = Val.GetPoint(i); }
	SetTexture(Val);
}

TPoint TSpriteTexture::GetPoint(const int &Id) const
{
	return Points[Id];
}

pTPoint TSpriteTexture::GetPointPtr(const int &Id)
{
	return &Points[Id];
}


