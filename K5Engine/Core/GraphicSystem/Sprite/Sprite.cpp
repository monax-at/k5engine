#include "Sprite.h"


TSprite::TSprite():TBaseGraphicObject(),ViewMatrix(NULL), VertexData(NULL), Mesh(4)
{
	Mesh.Set(0 , 0.5f, 0.5f, 0.0f);
	Mesh.Set(1 ,-0.5f, 0.5f, 0.0f);
	Mesh.Set(2 ,-0.5f,-0.5f, 0.0f);
	Mesh.Set(3 , 0.5f,-0.5f, 0.0f);

	for(int i=0;i<4;i++){ UsePointColor[i] = false; }
}

TSprite::TSprite(const TSprite &Val):TBaseGraphicObject(),ViewMatrix(NULL),VertexData(NULL)
{
	*this = Val;
}

TSprite::~TSprite()
{
	delete ViewMatrix;

	if(VertexData!=NULL){VertexData->Delete();}
	delete VertexData;
}


void TSprite::ToSetDevice()
{
	delete ViewMatrix;
	ViewMatrix = NULL;

	if(VertexData!=NULL){VertexData->Delete();}
	delete VertexData;
	VertexData = NULL;

	if(Device == NULL){return;}

	ViewMatrix = Device->CreateViewMatrixWorker();

	VertexData = static_cast<pTSpriteVertexData>(Device->CreateSpriteVertexData());
	VertexData->Set(&Color, UsePointColor, PointColor, &Mesh, &Texture);
	VertexData->Create();
}

void TSprite::ToDraw()
{
	ViewMatrix->Clear();

	ViewMatrix->Translate( Pos.GetX(), Pos.GetY(), Pos.GetZ());
	ViewMatrix->Rotate	 ( Angle.Get() );
	ViewMatrix->Translate(-Center.GetX(), -Center.GetY(), -Center.GetZ());
	ViewMatrix->Scale	 ( Size.GetX(), Size.GetY());

	ViewMatrix->Run();

	Texture   .Run();
	VertexData->Run();
	Texture   .Stop();

	ViewMatrix->Stop();
}

void TSprite::SetUsePointColor(const bool &Val)
{
	for(int i=0;i<4;i++){ UsePointColor[i] = Val; }
}

void TSprite::SetUsePointColor(const int  &PointId, const bool &Val)
{
	TExceptionGenerator Ex(L"TSprite");
	Ex(PointId >= 0 && PointId <4, L"in SetUsePointColor(PointId, Val) PointId out of range");
	UsePointColor[PointId] = Val;
}

bool TSprite::GetUsePointColor() const
{
	for(int i=0;i<4;i++){ if(UsePointColor[i] == true){return true;} }
	return false;
}

bool TSprite::GetUsePointColor(const int  &PointId) const
{
	TExceptionGenerator Ex(L"TSprite");
	Ex(PointId >= 0 && PointId <4, L"in GetUsePointColor(PointId) PointId out of range");
	return UsePointColor[PointId];
}

void TSprite::operator=(const TSprite &Val)
{
	SetDefParams(&Val);

	Size    = Val.Size;
	Mesh 	= Val.Mesh;

	Color 	= Val.Color;
	Texture = Val.Texture;

	for(int i=0;i<4;i++){
		UsePointColor[i] = Val.GetUsePointColor(i);
		PointColor   [i] = Val.PointColor[i];
	}
}


