///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include "../Base/BaseGraphicObject.h"
#include "SpriteVertexData.h"

class TSprite: public TBaseGraphicObject
{
	protected:
		pTBaseViewMatrixWorker ViewMatrix;
		pTSpriteVertexData     VertexData;

		bool UsePointColor[4];
	protected:
		void ToSetDevice();
		void ToDraw();
	public:
		TColor         Color;
		TColor         PointColor[4];

		TPoint         Size;
		TPointArray    Mesh;
		TSpriteTexture Texture;
	public:
		TSprite();
		TSprite(const TSprite &Val);
		virtual ~TSprite();

		void SetUsePointColor(const bool &Val);
		void SetUsePointColor(const int  &PointId, const bool &Val);

		bool GetUsePointColor() const;
		bool GetUsePointColor(const int  &PointId) const;

		void operator=(const TSprite &Val);
};

typedef TSprite* pTSprite;


#endif // SPRITE_H_INCLUDED
