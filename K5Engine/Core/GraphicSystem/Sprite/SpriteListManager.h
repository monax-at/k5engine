///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef SPRITELISTMANAGER_H_INCLUDED
#define SPRITELISTMANAGER_H_INCLUDED

#include "../Base/BaseGraphicObjectListManager.h"
#include "SpriteList.h"

class TSpriteListManager: public TBaseGraphicObjectListManager<TSprite,TSpriteList>
{
	public:
		TSpriteListManager();
		virtual ~TSpriteListManager();
};

typedef TSpriteListManager* pTSpriteListManager;


#endif // CLASS_SPRITELISTMANAGER_H_INCLUDED
