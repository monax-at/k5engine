///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef SPRITELIST_H_INCLUDED
#define SPRITELIST_H_INCLUDED

#include "../Base/BaseGraphicObjectList.h"
#include "Sprite.h"

class TSpriteList: public TBaseGraphicObjectList<TSprite>
{
	public:
		TSpriteList();
		virtual ~TSpriteList();
};

typedef TSpriteList* pTSpriteList;


#endif // SPRITELIST_H_INCLUDED
