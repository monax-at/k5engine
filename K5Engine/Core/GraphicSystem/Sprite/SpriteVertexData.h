///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef SPRITEVERTEXDATA_H_INCLUDED
#define SPRITEVERTEXDATA_H_INCLUDED

#include "../../PointArray.h"
#include "SpriteTexture.h"

#include "../Base/Color.h"
#include "../Base/BaseVertexData.h"

class TSpriteVertexData:public TBaseVertexData
{
	protected:
		TExceptionGenerator Exception;

		pTColor         Color;

		bool*           UsePointColor;
		pTColor         PointColor;

		pTPointArray    Mesh;
		pTSpriteTexture Texture;

		TColor OldColor;
		TColor OldPointColor[4];
	protected:
		bool CheckNeedUpdatePointColors();
		bool CheckNeedUpdate();
	public:
		TSpriteVertexData();
		TSpriteVertexData(const unsigned long &IdVal);
		virtual ~TSpriteVertexData();

		void Set(const pTColor &ColorVal, bool* UsePointColorVal, const pTColor &PointColorVal,
				 const pTPointArray &MeshVal, const pTSpriteTexture &TextureVal);

		void SetColor         (const pTColor &Val);

		void SetUsePointColor (bool* Val);
		void SetPointColor    (const pTColor &Val);

        void SetMesh	      (const pTPointArray &Val);
        void SetTexture	      (const pTSpriteTexture &Val);

        pTColor         GetColor();

		bool*           GetUsePointColor();
		pTColor         GetPointColor();

        pTPointArray    GetMesh();
        pTSpriteTexture GetTexture();
};

typedef TSpriteVertexData* pTSpriteVertexData;


#endif // SPRITEVERTEXDATA_H_INCLUDED
