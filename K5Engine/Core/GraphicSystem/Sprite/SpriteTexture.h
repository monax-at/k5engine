///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef SPRITETEXTURE_H_INCLUDED
#define SPRITETEXTURE_H_INCLUDED

#include "../ObjectTexture.h"

class TSpriteTexture:public TObjectTexture
{
	private:
		TPoint Points[4];
	protected:
		void BuilCoordRepeat();
		void BuilCoordRect();

		void UpdateData();
	public:
		TSpriteTexture();
		virtual ~TSpriteTexture();

		void BuildCoordinatesByMesh(const pTPointArray &Mesh);

		void operator =(const pTTexture &Val);
		void operator =(const TSpriteTexture &Val);

		void operator ()(const pTTexture &Val);
		void operator ()(const TSpriteTexture &Val);

        TPoint  GetPoint(const int &Id) const;
        pTPoint GetPointPtr(const int &Id);
};

typedef TSpriteTexture* pTSpriteTexture;


#endif // SPRITETEXTURE_H_INCLUDED
