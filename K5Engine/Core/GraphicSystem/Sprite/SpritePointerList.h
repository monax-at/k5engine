///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef SPRITEPOINTERLIST_H_INCLUDED
#define SPRITEPOINTERLIST_H_INCLUDED

#include "Sprite.h"
#include "../Base/BaseGraphicObjectPointerList.h"

class TSpritePointerList:public TBaseGraphicObjectPointerList<TSprite>
{
	public:
		TSpritePointerList();
		virtual ~TSpritePointerList();
};

typedef TSpritePointerList* pTSpritePointerList;


#endif // CLASS_SPRITEPOINTERLIST_H_INCLUDED
