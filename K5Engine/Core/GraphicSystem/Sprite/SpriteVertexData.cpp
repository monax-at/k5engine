#include "SpriteVertexData.h"


TSpriteVertexData::TSpriteVertexData():	TBaseVertexData(),
					Color(NULL), UsePointColor(NULL), PointColor(NULL), Mesh(NULL), Texture(NULL)
{
}

TSpriteVertexData::TSpriteVertexData(const unsigned long &IdVal): TBaseVertexData(IdVal),
					Color(NULL), UsePointColor(NULL), PointColor(NULL), Mesh(NULL), Texture(NULL)
{
}

TSpriteVertexData::~TSpriteVertexData()
{
}


bool TSpriteVertexData::CheckNeedUpdatePointColors()
{
	bool Res(false);
	for(int i=0;i<4;i++){
		if(UsePointColor[i]){
			if( PointColor[i] != OldPointColor[i] ){
				Res = true;
				OldPointColor[i] = PointColor[i];
			}
		}
	}

	return Res;
}

bool TSpriteVertexData::CheckNeedUpdate()
{
	if( !Mesh->GetChanged() && *Color == OldColor && !CheckNeedUpdatePointColors() &&
		!Texture->GetChanged())
	{
		return false;
	}

	Mesh   ->SetChanged(false);
	Texture->SetChanged(false);

	OldColor = *Color;

	return true;
}

void TSpriteVertexData::Set(const pTColor &ColorVal, bool* UsePointColorVal,
							const pTColor &PointColorVal,
							const pTPointArray &MeshVal, const pTSpriteTexture &TextureVal)
{
	Color         = ColorVal;
	UsePointColor = UsePointColorVal;
	PointColor    = PointColorVal;
	Mesh          = MeshVal;
	Texture       = TextureVal;

	OldColor = *Color;

	for(int i=0;i<4;i++){ OldPointColor[i] = PointColor[i]; }
}

void TSpriteVertexData::SetColor(const pTColor &Val)
{
	Color = Val;
	OldColor = *Color;
}

void TSpriteVertexData::SetUsePointColor(bool* Val)
{
	UsePointColor = Val;
}

void TSpriteVertexData::SetPointColor(const pTColor &Val)
{
	PointColor = Val;
	for(int i=0;i<4;i++){ OldPointColor[i] = PointColor[i]; }
}

void TSpriteVertexData::SetMesh(const pTPointArray &Val)
{
	Mesh = Val;
}

void TSpriteVertexData::SetTexture(const pTSpriteTexture &Val)
{
	Texture = Val;
}

pTColor TSpriteVertexData::GetColor()
{
	return Color;
}

bool* TSpriteVertexData::GetUsePointColor()
{
	return UsePointColor;
}

pTColor TSpriteVertexData::GetPointColor()
{
	return PointColor;
}

pTPointArray TSpriteVertexData::GetMesh()
{
	return Mesh;
}

pTSpriteTexture TSpriteVertexData::GetTexture()
{
	return Texture;
}



