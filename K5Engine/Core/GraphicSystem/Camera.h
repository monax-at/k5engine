///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include "../BaseDevice.h"

class TCamera
{
	protected:
		pTBaseViewMatrixWorker  ViewMatrix;
		pTBaseDevice			Device;

		bool   UseIntShift;
		TPoint WindowSize;

		TPoint OldShift;
		TPoint OldScale;
	protected:
		inline void Update();
	public:
		TPoint Shift;
		TPoint Scale;
	public:
		TCamera();
		virtual ~TCamera();

		void Run ();
		void Stop();

		TPoint RecountPoint(const float &X, const float &Y);
		TPoint RecountPoint(const TPoint &Point);

		TEvent RecountEvent(const TEvent &Event);

		void SetDevice(const pTBaseDevice &Val);
		void SetDevice(TBaseDevice &Val);
		pTBaseDevice GetDevice();

		void SetUseIntShift(const bool &Val);
		bool GetUseIntShift() const;
};

typedef TCamera* pTCamera;


#endif // CAMERA_H_INCLUDED
