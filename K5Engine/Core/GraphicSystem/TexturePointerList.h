///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef TEXTUREPOINTERLIST_H_INCLUDED
#define TEXTUREPOINTERLIST_H_INCLUDED

#include "Base/Texture.h"

#include <vector>
#include <string>

using std::vector;
using std::wstring;


class TTexturePointerList
{
	protected:
		TExceptionGenerator Exception;
		wstring Name;
		vector<pTTexture> Elems;
	protected:
		inline pTTexture GetElem(const int &Id);
		inline pTTexture GetElem(const wstring &Name);
	public:
		TTexturePointerList();
		~TTexturePointerList();

		void SetName(const wstring &Val);
        wstring GetName() const;

		pTTexture operator[](const int &Id);
		pTTexture operator[](const wstring &Name);

        pTTexture Get(const int &Id);
        pTTexture Get(const wstring &Name);
        pTTexture GetLast();

        int  GetSize() const;

        void Add(const pTTexture &Texture);

        void Del(const int &Id);
        void Del(const wstring &Name);

        bool IfExist(const unsigned long &ID);
        bool IfExist(const wstring &Name);

        void Clear();
        void ClearTexturesData();
};

typedef TTexturePointerList* pTTexturePointerList;


#endif // BASETEXTUREPOINTERLIST_H_INCLUDED
