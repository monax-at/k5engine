#include "ObjectTexture.h"


TObjectTexture::TObjectTexture():
		Texture(NULL),UseState(EN_TUS_Repeat),CoordinatesState(EN_TCS_Fixed),
		TexRepeatX(1),TexRepeatY(1),
		TexRectX(0.0f),TexRectY(0.0f),TexRectWidth(0.0f),TexRectHeight(0.0f),
		MirrorX(false), MirrorY(false), Changed(false)
{
}

TObjectTexture::~TObjectTexture()
{
}


void TObjectTexture::SetTextureRect(const float &X, const float &Y,
									const float &Width, const float &Height)
{
	UseState = EN_TUS_Rect;

	TexRectX = X;
	TexRectY = Y;
	TexRectWidth  = Width;
	TexRectHeight = Height;

	UpdateData();
	SetChanged();
}

void TObjectTexture::SetTexture(const pTTexture &NewTexture)
{
	Texture = NewTexture;

	UpdateData();
	SetChanged();
}

void TObjectTexture::SetTexture(const TObjectTexture &NewElem)
{
	Texture = NewElem.Get();

	UseState = NewElem.GetUseState();
	CoordinatesState = NewElem.GetCoordinatesState();

	TexRepeatX = NewElem.GetRepeatX();
	TexRepeatY = NewElem.GetRepeatY();

	TexRectX      = NewElem.GetRectX();
	TexRectY      = NewElem.GetRectY();
	TexRectWidth  = NewElem.GetRectWidth();
	TexRectHeight = NewElem.GetRectHeight();

	MirrorX = NewElem.GetMirrorX();
	MirrorY = NewElem.GetMirrorY();

	UpdateData();
	SetChanged();
}

void TObjectTexture::SetTextureRepeat(const float &X,const float &Y)
{
	UseState = EN_TUS_Repeat;

	TexRepeatX = X;
	TexRepeatY = Y;

	UpdateData();
	SetChanged();
}

void TObjectTexture::SetTextureRepeatX(const float &Val)
{
	UseState = EN_TUS_Repeat;

	TexRepeatX = Val;

	UpdateData();
	SetChanged();
}

void TObjectTexture::SetTextureRepeatY(const float &Val)
{
	UseState = EN_TUS_Repeat;

	TexRepeatY = Val;

	UpdateData();
	SetChanged();
}

void TObjectTexture::Set(const pTTexture &Val)
{
	SetTexture(Val);
}

void TObjectTexture::Set(const TObjectTexture &Val)
{
	SetTexture(Val);
}

pTTexture TObjectTexture::Get() const
{
	return Texture;
}

void TObjectTexture::UnSet()
{
	Texture = NULL;
}

bool TObjectTexture::IsInit()
{
	if(Texture == NULL){return false;}
	return true;
}

void TObjectTexture::SetRepeat(const float &Val)
{
	SetTextureRepeat(Val,Val);
}

void TObjectTexture::SetRepeat(const float &RepeatX,const float &RepeatY)
{
	SetTextureRepeat(RepeatX,RepeatY);
}

void TObjectTexture::SetRepeatX(const float &Val)
{
	SetTextureRepeatX(Val);
}

void TObjectTexture::SetRepeatY(const float &Val)
{
	SetTextureRepeatY(Val);
}

float TObjectTexture::GetRepeatX() const
{
	return TexRepeatX;
}

float TObjectTexture::GetRepeatY() const
{
	return TexRepeatY;
}

void TObjectTexture::SetRect(const float &X,const float &Y,const float &Width,const float &Height)
{
	SetTextureRect(X,Y,Width,Height);
}

void TObjectTexture::SetRect(const TPoint &Coord, const TPoint &Sizes)
{
	SetTextureRect(Coord.GetX(), Coord.GetY(), Sizes.GetX(), Sizes.GetY());
}

void TObjectTexture::SetRectPos(const float &X,const float &Y)
{
	SetTextureRect(X, Y, TexRectWidth, TexRectHeight);
}

void TObjectTexture::SetRectSize(const float &Width,const float &Height)
{
	SetTextureRect(TexRectX, TexRectY, Width, Height);
}

void TObjectTexture::SetRectPosX(const float &Val)
{
	SetTextureRect(Val,TexRectY,TexRectWidth,TexRectHeight);
}

void TObjectTexture::SetRectPosY(const float &Val)
{
	SetTextureRect(TexRectX,Val,TexRectWidth,TexRectHeight);
}

void TObjectTexture::SetRectWidth(const float &Val)
{
	SetTextureRect(TexRectX,TexRectY,Val,TexRectHeight);
}

void TObjectTexture::SetRectHeight(const float &Val)
{
	SetTextureRect(TexRectX,TexRectY,TexRectWidth,Val);
}

void TObjectTexture::IncRectPos(const float &X,const float &Y)
{
	SetTextureRect(TexRectX + X, TexRectY + Y, TexRectWidth, TexRectHeight);
}

void TObjectTexture::DecRectPos(const float &X,const float &Y)
{
	SetTextureRect(TexRectX - X, TexRectY - Y, TexRectWidth, TexRectHeight);
}

void TObjectTexture::IncRectPosX(const float &Val)
{
	SetTextureRect(TexRectX + Val, TexRectY, TexRectWidth, TexRectHeight);
}

void TObjectTexture::DecRectPosX(const float &Val)
{
	SetTextureRect(TexRectX - Val, TexRectY, TexRectWidth, TexRectHeight);
}

void TObjectTexture::IncRectPosY(const float &Val)
{
	SetTextureRect(TexRectX, TexRectY + Val, TexRectWidth, TexRectHeight);
}

void TObjectTexture::DecRectPosY(const float &Val)
{
	SetTextureRect(TexRectX, TexRectY - Val, TexRectWidth, TexRectHeight);
}

float TObjectTexture::GetRectX() const
{
	return TexRectX;
}

float TObjectTexture::GetRectY() const
{
	return TexRectY;
}

float TObjectTexture::GetRectWidth() const
{
	return TexRectWidth;
}

float TObjectTexture::GetRectHeight() const
{
	return TexRectHeight;
}

void TObjectTexture::SetMirror(const bool &X, const bool &Y)
{
	MirrorX = X;
	MirrorY = Y;

	UpdateData();
	SetChanged();
}

void TObjectTexture::SetMirrorX(const bool &Val)
{
	MirrorX = Val;

	UpdateData();
	SetChanged();
}

void TObjectTexture::SetMirrorY(const bool &Val)
{
	MirrorY = Val;

	UpdateData();
	SetChanged();
}

bool TObjectTexture::GetMirrorX() const
{
	return MirrorX;
}

bool TObjectTexture::GetMirrorY() const
{
	return MirrorY;
}

void TObjectTexture::SetUseState(const enTextureUseState &Val)
{
	UseState = Val;
}

enTextureUseState TObjectTexture::GetUseState() const
{
	return UseState;
}

void TObjectTexture::SetCoordinatesState(const enTextureCoordinatesState &Val)
{
	CoordinatesState = Val;
}

enTextureCoordinatesState TObjectTexture::GetCoordinatesState() const
{
	return CoordinatesState;
}

int TObjectTexture::GetWidth() const
{
	if(Texture!=NULL){return Texture->GetWidth();}
	return 0;
}

int TObjectTexture::GetHeight() const
{
	if(Texture!=NULL){return Texture->GetHeight();}
	return 0;
}

void TObjectTexture::SetChanged(const bool &Val)
{
	Changed = Val;
}

bool TObjectTexture::GetChanged() const
{
	return Changed;
}

void TObjectTexture::Run()
{
	if(Texture!=NULL){Texture->Run();}
}

void TObjectTexture::Stop()
{
	if(Texture!=NULL){Texture->Stop();}
}


