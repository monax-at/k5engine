///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef COLORARRAY_H_INCLUDED
#define COLORARRAY_H_INCLUDED


#include "Base/Color.h"
#include "../ExceptionGenerator.h"

#include <vector>
#include <algorithm>
#include <stdexcept>

using std::vector;
using std::copy;
using std::out_of_range;


class TColorArray
{
	protected:
		TExceptionGenerator Exception;
		vector<TColor>      Colors;
		bool                Changed;
	public:
		TColorArray();
		TColorArray(const int &Size);
		TColorArray(const TColorArray &Val);
		~TColorArray();

		void operator = (const TColorArray &Val);
		void operator = (const TColor      &Val);

		void operator()(const TColorArray &Val);
		void operator()(const float  &Val);
		void operator()(const TColor &Val);
		void operator()(const float &R,const float &G, const float &B);
		void operator()(const float &R,const float &G, const float &B,const float &A);

		void operator()(const int &Index, const enColorComponent &Type, const float &Val);
		void operator()(const int &Index, const TColor &Val);
		void operator()(const int &Index, const float &R, const float &G,const float &B);
		void operator()(const int &Index, const float &R, const float &G,const float &B,const float &A);

		void SetSize(const int &Val);
		int  GetSize() const;

		// установка и взятие флага, который показывает, были ли изменения в элементах массива
		void SetChanged(const bool &Val = true);
		bool GetChanged() const;

		void Set(const TColorArray  &Val);
		// особенность всех функций Set без индекса - устанавливают значение всем
		// внутренним компонентам
		void Set     (const float  &Val);
		void Set     (const TColor &Val);
		void Set     (const float &R, const float &G, const float &B);
		void Set     (const float &R, const float &G, const float &B, const float &A);

		void Set     (const enColorComponent &Type, const float &Val);
		void SetRed  (const float &Val);
		void SetGreen(const float &Val);
		void SetBlue (const float &Val);
		void SetAlpha(const float &Val);

		void Set(const int &Index, const enColorComponent &Type, const float &Val);
		void Set(const int &Index, const TColor &Val);
		void Set(const int &Index, const float &R,const float &G,const float &B);
		void Set(const int &Index, const float &R,const float &G,const float &B,const float &A);

		TColor Get		(const int &Index = 0) const;

		float  Get      (const int &Index, const enColorComponent &Component) const;
		float  GetRed	(const int &Index = 0) const;
		float  GetGreen	(const int &Index = 0) const;
		float  GetBlue	(const int &Index = 0) const;
		float  GetAlpha	(const int &Index = 0) const;

		void Inc(const TColor &Val);
		void Dec(const TColor &Val);

		void Inc(const enColorComponent &Type, const float &Val);
		void Dec(const enColorComponent &Type, const float &Val);

		void IncRed  (const float &Val);
		void DecRed  (const float &Val);

		void IncGreen(const float &Val);
		void DecGreen(const float &Val);

		void IncBlue (const float &Val);
		void DecBlue (const float &Val);

		void IncAlpha(const float &Val);
		void DecAlpha(const float &Val);

		void Inc(const int &Index, const TColor &Val);
		void Dec(const int &Index, const TColor &Val);

		void Inc(const int &Index, const enColorComponent &Type, const float &Val);
		void Dec(const int &Index, const enColorComponent &Type, const float &Val);

		void IncRed  (const int &Index, const float &Val);
		void DecRed  (const int &Index, const float &Val);

		void IncGreen(const int &Index, const float &Val);
		void DecGreen(const int &Index, const float &Val);

		void IncBlue (const int &Index, const float &Val);
		void DecBlue (const int &Index, const float &Val);

		void IncAlpha(const int &Index, const float &Val);
		void DecAlpha(const int &Index, const float &Val);
};

typedef TColorArray* pTColorArray;


#endif // COLORARRAY_H_INCLUDED
