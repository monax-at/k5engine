#include "Camera.h"


TCamera::TCamera():ViewMatrix(NULL),Device(NULL),UseIntShift(true),Scale(1.0f)
{
}

TCamera::~TCamera()
{
	delete ViewMatrix;
}

void TCamera::Update()
{
	if(OldScale == Scale && OldShift == Shift){return;}

	OldScale = Scale;
	OldShift = Shift;

	ViewMatrix->Clear();

	float ShiftX(-Shift.GetX());
	float ShiftY(-Shift.GetY());

	float ScaleX(Scale.GetX());
	float ScaleY(Scale.GetY());

	if(ScaleX != 1.0f){
		float Width(WindowSize.GetX());
		float ScaleSizeX((Width - Width*ScaleX)/2.0f);
		ShiftX *= ScaleX;
		ShiftX += ScaleSizeX;
	}

	if(ScaleY != 1.0f){
		float Height(WindowSize.GetY());
		float ScaleSizeY((Height - Height*ScaleY)/2.0f);
		ShiftY *= ScaleY;
		ShiftY += ScaleSizeY;
	}

	if(UseIntShift){
		ShiftX = (int)ShiftX;
		ShiftY = (int)ShiftY;
	}

	ViewMatrix->Translate	(ShiftX, ShiftY, 0);
	ViewMatrix->Scale		(ScaleX, ScaleY);
}

void TCamera::Run()
{
	if(ViewMatrix==NULL){return;}

	Update();
	ViewMatrix->Run();
}

void TCamera::Stop()
{
	if(ViewMatrix==NULL){return;}

	ViewMatrix->Stop();
}

TPoint TCamera::RecountPoint(const float &X, const float &Y)
{
	float ResX(X);
	float ResY(Y);

	float ScaleX(Scale.GetX());
	float ScaleY(Scale.GetY());

	if(ScaleX > 0){
		float Width(WindowSize.GetX());
		float ScaleSizeX((Width - Width*ScaleX)/2.0f);

		ResX -= ScaleSizeX;
		ResX /= ScaleX;
	}

	if(ScaleY > 0){
		float Height(WindowSize.GetY());
		float ScaleSizeY((Height - Height*ScaleY)/2.0f);

		ResY -= ScaleSizeY;
		ResY /= ScaleY;
	}

	ResX += Shift.GetX();
	ResY += Shift.GetY();

	return TPoint(ResX,ResY);
}

TPoint TCamera::RecountPoint(const TPoint &Point)
{
	return RecountPoint(Point.GetX(), Point.GetY());
}

TEvent TCamera::RecountEvent(const TEvent &Event)
{
	if(Event.Type != EN_MOUSE){return Event;}

	TEvent ResEvent(Event);
	TPoint ResPoint(RecountPoint((float)ResEvent.Mouse.X, (float)ResEvent.Mouse.Y));

	ResEvent.Mouse.X = (int)ResPoint.GetX();
	ResEvent.Mouse.Y = (int)ResPoint.GetY();

	return ResEvent;
}

void TCamera::SetDevice(const pTBaseDevice &Val)
{
	delete ViewMatrix;
	ViewMatrix = NULL;

	Device = Val;

	if(Device == NULL){return;}

	WindowSize((float)Device->GetWidth(), (float)Device->GetHeight());

	ViewMatrix = Device->CreateViewMatrixWorker();
}

void TCamera::SetDevice(TBaseDevice &Val)
{
	SetDevice(&Val);
}

pTBaseDevice TCamera::GetDevice()
{
	return Device;
}

void TCamera::SetUseIntShift(const bool &Val)
{
	UseIntShift = Val;
}

bool TCamera::GetUseIntShift() const
{
	return UseIntShift;
}


