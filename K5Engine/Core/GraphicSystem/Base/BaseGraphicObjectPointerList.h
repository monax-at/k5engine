///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEGRAPHICOBJECTPOINTERLIST_H_INCLUDED
#define BASEGRAPHICOBJECTPOINTERLIST_H_INCLUDED

#include "../../ExceptionGenerator.h"
#include "BaseGraphicObject.h"

#include <vector>
#include <algorithm>

using std::vector;
using std::sort;

template <class TObject>
class TBaseGraphicObjectPointerList
{
	protected:
		TExceptionGenerator Exception;
		vector<TObject*> Elems;
	public:
		TBaseGraphicObjectPointerList(): Exception(L"TBaseGraphicObjectPointerList: ")
		{
		}
		// ---------------------------------//
		virtual ~TBaseGraphicObjectPointerList()
		{
			Clear();
		}
		// ---------------------------------//
		void Add(TObject *Val)
		{
			Elems.push_back(Val);
		}
		// ---------------------------------//
		TObject* Get(const int &Id)
		{
			int Size = Elems.size();
			Exception(Id >= 0 && Id < Size,L"In Get(Id) Id out of range");
			return Elems[Id];
		}
		// ---------------------------------//
		TObject* Get(const wstring &ObjName)
		{
			TObject* Obj(GetIfExist(ObjName));
			Exception(Obj!=NULL, L"in Get(ObjName) list is enpty or object not exist" );
            return Obj;
		}
        // ---------------------------------//
		TObject* Get(const unsigned long &ObjId)
        {
			TObject* Obj(GetIfExist(ObjId));
			Exception(Obj!=NULL, L"in Get(ObjId) list is enpty or object not exist" );
            return Obj;
        }
		// ---------------------------------//
		bool IfExist(const unsigned long &ObjId)
		{
            int Size(Elems.size());
            for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return true; }
            }
            return false;
		}
		// ---------------------------------//
		bool IfExist(const wstring &ObjName)
		{
            int Size(Elems.size());
            for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return true; }
            }
            return false;
		}
		// ---------------------------------//
		TObject* GetIfExist(const wstring &ObjName)
		{
			int Size = Elems.size();
            for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return Elems[i]; }
            }
            return NULL;
		}
		// ---------------------------------//
		TObject* GetIfExist(const unsigned long &ObjId)
		{
			int Size = Elems.size();
            for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return Elems[i]; }
            }
            return NULL;
		}
        // ---------------------------------//
		void Del(const int &Id)
		{
            int Size(Elems.size());
            Exception(Size > 0,L"in Del(Id) list is enpty");
            Exception(Id >= 0 && Id < Size,L"in Del(Id) Id out of range");
            Elems.erase(Elems.begin()+Id);
		}
		 // ---------------------------------//
		void Del(const unsigned long &ObjId)
		{
            int Size(Elems.size());
            Exception(Size > 0,L"in Del(ObjId) list is enpty");

            int DeleteId(-1);
            for(int i=0;i<Size;i++){
            	if(Elems[i]->GetID() == ObjId){
            		DeleteId = i;
            		break;
            	}
            }

			Exception(DeleteId!=-1,L"in Del(ObjId) object whis id not exist");
            Elems.erase(Elems.begin()+DeleteId);
		}
        // ---------------------------------//
		void Del(const wstring &ObjName)
		{
            int Size(Elems.size());
            Exception(Size > 0,L"in Del(ObjName) list is enpty");

            int DeleteId(-1);
            for(int i=0;i<Size;i++){
            	if(Elems[i]->GetName() == ObjName){
            		DeleteId = i;
            		break;
            	}
            }
			Exception(DeleteId!=-1,L"in Del(ObjName) object whis name: "+ObjName+L" not exist");

            Elems.erase(Elems.begin()+DeleteId);
		}
		// ---------------------------------//
		int GetSize() const
		{
			return Elems.size();
		}
		// ---------------------------------//
		void Clear()
		{
			Elems.clear();
		}
		// ---------------------------------//
		void Sort()
		{
			sort(Elems.begin(), Elems.end(), CheckObjectPosZ<TObject> );
		}
		// ---------------------------------//
		void SetView(const bool &Val)
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				Elems[i]->SetView(Val);
			}
		}
		// ---------------------------------//

};


#endif // BASEGRAPHICOBJECTPOINTERLIST_H_INCLUDED
