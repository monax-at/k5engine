///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef RENDERQUEUE_H_INCLUDED
#define RENDERQUEUE_H_INCLUDED

#include "BaseGraphicObject.h"
#include "RenderQueueCell.h"

#include <algorithm>
#include <vector>
#include <string>

using std::vector;
using std::wstring;
using std::sort;


class TRenderQueue
{
    protected:
		TExceptionGenerator Exception;
		vector<pTRenderQueueCell> Elems;
		bool View;
    public:
		TRenderQueue();
		~TRenderQueue();

		void Draw ();
		void Clear();
		void Sort ();

		void Add(const pTRenderQueueCell   &Cell);
		void Add(const pTBaseGraphicObject &Object, const bool &CellView = true);

		pTRenderQueueCell Get(const int &Index);

		void Del(const unsigned long &Id);
		void Del(const pTBaseGraphicObject &Object);

		void SetCellDraw(const unsigned long &Id, const bool &DrawFlag);
		bool GetCellView(const unsigned long &Id) const;

		void SetView(const bool &Val);
		bool GetView() const;

		int GetSize() const;
};

typedef TRenderQueue* pTRenderQueue;


#endif // RENDERQUEUE_H_INCLUDED
