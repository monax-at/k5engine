#include "BaseViewMatrixWorker.h"
//#include "Globals.h"


TBaseViewMatrixWorker::TBaseViewMatrixWorker()
{
}

TBaseViewMatrixWorker::~TBaseViewMatrixWorker()
{
}

void TBaseViewMatrixWorker::Scale(const float &X,const float &Y)
{
	TMatrix3D MScale;
	MScale.InitScale(X,Y);

	Transform *= MScale;
}

void TBaseViewMatrixWorker::Translate(const float &X,const float &Y,const float &Z)
{
	TMatrix3D MTranslate;
	MTranslate.InitTranslate(X,Y,Z);

	Transform *= MTranslate;
}

void TBaseViewMatrixWorker::Rotate(const float &Angle)
{
	TMatrix3D MRotate;
	MRotate.InitRotateZ(Angle);

	Transform *= MRotate;
}

void TBaseViewMatrixWorker::Clear()
{
	Transform.ToIdentity();
}


