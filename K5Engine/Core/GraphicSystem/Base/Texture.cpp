#include "Texture.h"


TTextureRawData::TTextureRawData()
{
	Width       = 0;
	Height      = 0;
	TextureData = NULL;
}

TTextureRawData::TTextureRawData(int WidthVal, int HeightVal, unsigned char *TextureDataVal)
{
	Width       = WidthVal;
	Height      = HeightVal;
	TextureData = TextureDataVal;
}

TTextureRawData::~TTextureRawData()
{
	delete[] TextureData;
}

TTextureRawData *TTextureRawData::Clone()
{
	TTextureRawData	*CloneRawData = new TTextureRawData();
	CloneRawData->Width    = Width;
	CloneRawData->Height   = Height;

	int Size = Width * Height * 4;
	CloneRawData->TextureData = new unsigned char[Size];
	memcpy(CloneRawData->TextureData, TextureData, Size);

	return CloneRawData;
}


unsigned long TTexture::IdCounter = 0;


TTexture::TTexture():	Exception(L"Texture: "),Name(L""),Path(L""),
						ActiveFlag(true),LockFlag(false),Width(0),Height(0)
{
	IdCounter++;
	Id = IdCounter;
}

TTexture::~TTexture()
{
}


void TTexture::SetName(const wstring &Val)
{
	Name = Val;
}

void TTexture::SetPath(const wstring &Val)
{
	Path = Val;
}

void TTexture::SetActive(const bool &Val)
{
	ActiveFlag = Val;
}

unsigned long TTexture::GetID() const
{
	return Id;
}

wstring TTexture::GetName() const
{
	return Name;
}

wstring TTexture::GetPath() const
{
	return Path;
}

bool TTexture::GetActive() const
{
	return ActiveFlag;
}

int TTexture::GetWidth() const
{
	return Width;
}

int TTexture::GetHeight() const
{
	return Height;
}

void TTexture::Run()
{
	Exception(!LockFlag, L"Can't run texture, texture:" + Name + L"lock to change");

	if(ActiveFlag == true){ToRun();}
}

void TTexture::Stop()
{
	ToStop();
}

void TTexture::Del()
{
	ToDel();
}

TTexture* TTexture::CreateMask(const wstring &MaskName)
{
    return ToCreateMask(MaskName);
}

TTexture* TTexture::CreateCutMask(const wstring &MaskName)
{
	return ToCreateCutMask(MaskName);
}

TTexture* TTexture::Clone(const wstring &TextName)
{
    return ToClone(TextName);
}

TTexture* TTexture::CopyRect(const wstring &TextName, const int &RectWidth,const int &RectHeight,
							 const int &X,const int &Y, const bool &UseDeg2TexSizeFlag)
{
	Exception(X >= 0 && X < Width	,L"in CopyRest(...) X out of range");
	Exception(Y >= 0 && Y < Height	,L"in CopyRest(...) Y out of range");

	Exception(RectWidth  > 0 && RectWidth <= Width	,L"in CopyRest(...) RectWidth out of range");
	Exception(RectHeight > 0 && RectHeight <= Height,L"in CopyRest(...) RectHeight out of range");

	Exception(X + RectWidth  <= Width, L"in CopyRest(...) sizes out of range");
	Exception(Y + RectHeight <= Height,L"in CopyRest(...) sizes out of range");

	return ToCopyRect(TextName, RectWidth, RectHeight, X, Y, UseDeg2TexSizeFlag);
}

pTTextureRawData TTexture::GetCopyRawData(const int &RectWidth, const int &RectHeight,
										  const int &X, const int &Y)
{
	Exception(X >= 0 && X < Width	,L"in GetCopyRawData(...) X out of range");
	Exception(Y >= 0 && Y < Height	,L"in GetCopyRawData(...) Y out of range");

	Exception(RectWidth  > 0 && RectWidth  <= Width	,L"in GetCopyRawData(...) Width out of range");
	Exception(RectHeight > 0 && RectHeight <= Height,L"in GetCopyRawData(...) Height out of range");

	Exception(X + RectWidth  <= Width, L"in GetCopyRawData(...) sizes out of range");
	Exception(Y + RectHeight <= Height,L"in GetCopyRawData(...) sizes out of range");

	return ToGetCopyRawData(RectWidth, RectHeight, X, Y);
}

void TTexture::WriteRawData(const pTTextureRawData &RawDataVal, const int &X, const int &Y,
							const enTexWriteMode &ModeVal)
{
	Exception(RawDataVal->TextureData != NULL,L"in WriteRawData(...) TextureData is NULL");

	Exception(X >= 0 && X < Width	,L"in WriteRawData(...) X out of range");
	Exception(Y >= 0 && Y < Height	,L"in WriteRawData(...) Y out of range");

	Exception(RawDataVal->Width  > 0 && RawDataVal->Width <= Width	,
			  L"in WriteRawData(...) Width out of range");

	Exception(RawDataVal->Height > 0 && RawDataVal->Height <= Height,
			  L"in WriteRawData(...) Height out of range");

	Exception(X + RawDataVal->Width  <= Width, L"in WriteRawData(...) sizes out of range");
	Exception(Y + RawDataVal->Height <= Height,L"in WriteRawData(...) sizes out of range");

	ToWriteRawData(RawDataVal, X, Y, ModeVal);
}


