/// Authors:
///	Sergey Kalenik    [monax.at@gmail.com]
///	Maxim Kolesnikov [mpkmaximus@gmail.com]

#pragma once
#ifndef PALET_H_INCLUDED
#define PALET_H_INCLUDED

#include "enum_GraphicSystem.h"

class TPalet
{
	protected:
		enPaletType PLPalet;
	public:
		TPalet();
		TPalet(enPaletType PLFormat);
		~TPalet();

		enPaletType GetPL() const;
		void SetPL(enPaletType PLFormat);

		bool CheckPL(enPaletType PLFormat);
};


#endif // PALET_H_INCLUDED
