///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEVERTEXDATA_H_INCLUDED
#define BASEVERTEXDATA_H_INCLUDED

class TBaseVertexData
{
	protected:
		static unsigned long IdCounter;
		static unsigned long IdActive;
		unsigned long        Id;
	public:
		TBaseVertexData();
		TBaseVertexData(const unsigned long &IdVal);
		~TBaseVertexData();

		unsigned long GetId() const;

		virtual void Create() = 0;
		virtual void Delete() = 0;
		virtual void Update() = 0;
		virtual void Run()    = 0;
};

typedef TBaseVertexData* pTBaseVertexData;


#endif // BASEVERTEXDATA_H_INCLUDED
