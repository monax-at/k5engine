///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef COLOR_H_INCLUDED
#define COLOR_H_INCLUDED

#include "enum_GraphicSystem.h"
#include "../../Exception.h"

class TColor
{
    private:
        float Red;
        float Green;
        float Blue;
        float Alpha;
    private:
		inline void SetColorComponent(const int &Index,const float &Value);
		inline void CheckElems();

        inline void SetData(const float &Val);
        inline void SetData(const TColor &Val);
        inline void SetData(const float &R,const float &G,const float &B);
        inline void SetData(const float &R, const float &G,const float &B,const float &A);
    public:
        TColor();
        TColor(float Val,float A=1.0f);
        TColor(float R,float G,float B);
        TColor(float R,float G,float B,float A);
        virtual ~TColor();

        bool operator ==(const TColor &Val) const;
        bool operator !=(const TColor &Val) const;

        void  operator()(const float &Val);
        void  operator()(const TColor &Val);
        void  operator()(const float &R, const float &G, const float &B);
        void  operator()(const float &R, const float &G, const float &B, const float &A);

		void  operator()(const int &i,const float &Val);
		void  operator()(const enColorComponent &Type,const float &Val);

        void  Set(const float &Val);
        void  Set(const TColor &Val);
        void  Set(const float &R,const float &G,const float &B);
        void  Set(const float &R, const float &G,const float &B,const float &A);

		void  Set(const int &i,const float &Val);
		void  Set(const enColorComponent &Type,const float &Val);

        void  SetRed  (const float &Val);
        void  SetGreen(const float &Val);
        void  SetBlue (const float &Val);
        void  SetAlpha(const float &Val);

        float GetRed()  const;
        float GetGreen()const;
        float GetBlue() const;
        float GetAlpha()const;
        float Get(const int &Id) const;
        float Get(const enColorComponent &Type)const;

		void Inc(const TColor &Val);
		void Inc(const float &R, const float &G,const float &B,const float &A);
		void Inc(const enColorComponent &Type, const float &Val);

		void Dec(const TColor &Val);
		void Dec(const float &R, const float &G,const float &B,const float &A);
		void Dec(const enColorComponent &Type, const float &Val);

		void IncRed(const float &Val);
		void DecRed(const float &Val);

		void IncGreen(const float &Val);
		void DecGreen(const float &Val);

		void IncBlue(const float &Val);
		void DecBlue(const float &Val);

        void IncAlpha(const float &Val);
        void DecAlpha(const float &Val);
};

typedef TColor* pTColor;


#endif // COLOR_H_INCLUDED
