///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef ENUM_GRAPHICSYSTEM_H_INCLUDED
#define ENUM_GRAPHICSYSTEM_H_INCLUDED

enum enTextureFilter
{
	EN_TF_Unknown,
	EN_TF_None,
	EN_TF_Point,
	EN_TF_Linear
};

enum enColorComponent
{
	EN_CC_Red   = 0,
	EN_CC_Green = 1,
	EN_CC_Blue  = 2,
	EN_CC_Alpha = 3
};

enum enPaletType
{
	EN_PL_RGBA		= 4,
	EN_PL_RGB		= 3,
	EN_PL_GRAYSCALE = 1,
	EN_PL_GRAYALPHA = 2,
	EN_PL_NOTSET    = 0
};

enum enAlignment
{
	EN_A_Unknown 	= 0,
	EN_A_Center 	= 1,
	EN_A_Left 		= 2,
	EN_A_Right 		= 3,
	EN_A_Bottom		= 4,
	EN_A_Top		= 5
};

enum enTexWriteMode
{
	EN_TWM_Normal = 0,
	EN_TWM_And    = 1,
	EN_TWM_Or     = 2
};

enum enMeshDrawMode
{
	EN_MDW_Points 		 = 0,
	EN_MDW_Lines 		 = 1,
	EN_MDW_LineStrip     = 2,
	EN_MDW_LineLoop      = 3,
	EN_MDW_Triangles     = 4,
	EN_MDW_TriangleStrip = 5,
	EN_MDW_TriangleFan   = 6
};


#endif // ENUM_GRAPHICSYSTEM_H_INCLUDED
