///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEGRAPHICOBJECT_H_INCLUDED
#define BASEGRAPHICOBJECT_H_INCLUDED

#include "../../BaseDevice.h"
#include "../../BaseSceneObject.h"

class TBaseGraphicObject:public TBaseSceneObject
{
	protected:
		bool    		View;
		pTBaseDevice 	Device;

		enTextureFilter TexMinFilter;
		enTextureFilter TexMagFilter;
	protected:
		void SetDefParams(const TBaseGraphicObject *Val);

		virtual void ToSetDevice() = 0;
		virtual void ToDraw() = 0;
	public:
		TBaseGraphicObject();
		TBaseGraphicObject(const TBaseGraphicObject &Val);
		virtual ~TBaseGraphicObject();

		void operator=(const TBaseGraphicObject &Val);

		void SetView(const bool &Val = true);
		bool GetView() const;

		void SetDevice(const pTBaseDevice &Val);
		pTBaseDevice GetDevice();

		void SetMinFilter(const enTextureFilter &Val);
		void SetMagFilter(const enTextureFilter &Val);
		void SetFilter(const enTextureFilter &Val);

		enTextureFilter GetMinFilter() const;
		enTextureFilter GetMagFilter() const;

        void Draw();
};

typedef TBaseGraphicObject* pTBaseGraphicObject;


#endif // BASEGRAPHICOBJECT_H_INCLUDED
