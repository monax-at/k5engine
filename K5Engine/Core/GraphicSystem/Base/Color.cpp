#include "Color.h"
#include "../../../LibMath/LibMath.h"



TColor::TColor():Red(1.0f), Green(1.0f), Blue(1.0f), Alpha(1.0f)
{
}

TColor::TColor(float Val,float A):Red(Val), Green(Val), Blue(Val), Alpha(A)
{
}

TColor::TColor(float R,float G,float B):Red(R), Green(G), Blue(B), Alpha(1.0f)
{
}

TColor::TColor(float R,float G,float B,float A):Red(R),Green(G),Blue(B), Alpha(A)
{
}

TColor::~TColor()
{
}


void  TColor::SetColorComponent(const int &Index,const float &Val)
{
    switch(Index){
    	case 0:{Red 	= Val;}break;
    	case 1:{Green 	= Val;}break;
    	case 2:{Blue 	= Val;}break;
    	case 3:{Alpha 	= Val;}break;
    	default:{throw TException(L"TColor: in SetColorComponent() Id out of range");}break;
    }
}

void TColor::CheckElems()
{
	if(Red>1.0f){Red = 1.0f;}
	if(Red<0.0f){Red = 0.0f;}

	if(Green>1.0f){Green = 1.0f;}
	if(Green<0.0f){Green = 0.0f;}

	if(Blue>1.0f){Blue = 1.0f;}
	if(Blue<0.0f){Blue = 0.0f;}

	if(Alpha>1.0f){Alpha = 1.0f;}
	if(Alpha<0.0f){Alpha = 0.0f;}
}

void TColor::SetData(const float &Val)
{
    Red    = Val;
    Green  = Val;
    Blue   = Val;
    Alpha  = Val;
}

void TColor::SetData(const TColor &Val)
{
	*this = Val;
}

void TColor::SetData(const float &R,const float &G,const float &B)
{
    Red    = R;
    Green  = G;
    Blue   = B;
}

void TColor::SetData(const float &R, const float &G,const float &B,const float &A)
{
    Red    = R;
    Green  = G;
    Blue   = B;
    Alpha  = A;
}

bool TColor::operator ==(const TColor &Val) const
{
	if( Abs(Red 	- Val.GetRed() ) > Eps6 ){ return false; }
	if( Abs(Green  - Val.GetGreen()) > Eps6 ){ return false; }
	if( Abs(Blue   - Val.GetBlue() ) > Eps6 ){ return false; }
	if( Abs(Alpha  - Val.GetAlpha()) > Eps6 ){ return false; }

	return true;
}

bool TColor::operator !=(const TColor &Val) const
{
	if( Abs(Red 	- Val.GetRed() ) > Eps6 ){ return true; }
	if( Abs(Green  - Val.GetGreen()) > Eps6 ){ return true; }
	if( Abs(Blue   - Val.GetBlue() ) > Eps6 ){ return true; }
	if( Abs(Alpha  - Val.GetAlpha()) > Eps6 ){ return true; }

	return false;
}

void TColor::operator()(const float &Val)
{
	SetData(Val);
}

void TColor::operator()(const TColor &Val)
{
	SetData(Val);
}

void TColor::operator()(const float &R,const float &G,const float &B)
{
	SetData(R,G,B);
}

void TColor::operator()(const float &R, const float &G,const float &B,const float &A)
{
	SetData(R,G,B,A);
}

void TColor::operator()(const int &i, const float &Val)
{
	SetColorComponent(i,Val);
}

void TColor::operator()(const enColorComponent &Type,const float &Val)
{
	SetColorComponent(Type,Val);
}

void  TColor::Set(const float &Val)
{
	SetData(Val);
}

void  TColor::Set(const TColor &Val)
{
	SetData(Val);
}

void TColor::Set(const float &R,const float &G,const float &B)
{
	SetData(R,G,B);
}

void TColor::Set(const float &R,const float &G,const float &B,const float &A)
{
	SetData(R,G,B,A);
}

void  TColor::Set(const int &i,const float &Val)
{
	SetColorComponent(i,Val);
}

void  TColor::Set(const enColorComponent &Type,const float &Val)
{
	SetColorComponent(Type,Val);
}

void  TColor::SetRed(const float &Val)
{
	Red   = Val;
}

void  TColor::SetGreen(const float &Val)
{
	Green = Val;
}

void  TColor::SetBlue(const float &Val)
{
	Blue  = Val;
}

void  TColor::SetAlpha(const float &Val)
{
	Alpha = Val;
}

float TColor::GetRed()  const
{
	return(Red);
}

float TColor::GetGreen()const
{
	return(Green);
}

float TColor::GetBlue() const
{
	return(Blue);
}

float TColor::GetAlpha()const
{
	return(Alpha);
}

float TColor::Get(const int &Id) const
{
	switch(Id){
		case 0:{ return Red;	}break;
		case 1:{ return Green;	}break;
		case 2:{ return Blue;	}break;
		case 3:{ return Alpha;	}break;
		default:{throw TException(L"TColor: in Get(Id) Id out of range");}break;
	}
	return 0;
}

float TColor::Get(const enColorComponent &Type)const
{
    switch(Type){
    	case EN_CC_Red	:{ return Red;	}break;
    	case EN_CC_Green:{ return Green;}break;
    	case EN_CC_Blue	:{ return Blue;	}break;
    	case EN_CC_Alpha:{ return Alpha;}break;
    }
    return 0;
}

void TColor::Inc(const TColor &Val)
{
	Inc(Val.GetRed(),Val.GetGreen(),Val.GetBlue(),Val.GetAlpha());
}

void TColor::Inc(const float &R, const float &G,const float &B,const float &A)
{
	Red		+= R;
	Green 	+= G;
	Blue 	+= B;
	Alpha 	+= A;

	CheckElems();
}

void TColor::Inc(const enColorComponent &Type, const float &Val)
{
	switch(Type){
		case EN_CC_Red   :{ Red   += Val; }break;
		case EN_CC_Green :{ Green += Val; }break;
		case EN_CC_Blue  :{ Blue  += Val; }break;
		case EN_CC_Alpha :{ Alpha += Val; }break;
	}
	CheckElems();
}

void TColor::Dec(const TColor &Val)
{
	Dec(Val.GetRed(),Val.GetGreen(),Val.GetBlue(),Val.GetAlpha());
}

void TColor::Dec(const float &R, const float &G,const float &B,const float &A)
{
	Red	  -= R;
	Green -= G;
	Blue  -= B;
	Alpha -= A;

	CheckElems();
}

void TColor::Dec(const enColorComponent &Type, const float &Val)
{
	switch(Type){
		case EN_CC_Red   :{ Red   -= Val; }break;
		case EN_CC_Green :{ Green -= Val; }break;
		case EN_CC_Blue  :{ Blue  -= Val; }break;
		case EN_CC_Alpha :{ Alpha -= Val; }break;
	}
	CheckElems();
}

void TColor::IncRed(const float &Val)
{
	Red += Val;
	if(Red>1.0f){Red = 1.0f;}
	if(Red<0.0f){Red = 0.0f;}
}

void TColor::DecRed(const float &Val)
{
	Red -= Val;
	if(Red>1.0f){Red = 1.0f;}
	if(Red<0.0f){Red = 0.0f;}
}

void TColor::IncGreen(const float &Val)
{
	Green += Val;
	if(Green>1.0f){Green = 1.0f;}
	if(Green<0.0f){Green = 0.0f;}
}

void TColor::DecGreen(const float &Val)
{
	Green -= Val;
	if(Green>1.0f){Green = 1.0f;}
	if(Green<0.0f){Green = 0.0f;}
}

void TColor::IncBlue(const float &Val)
{
	Blue += Val;
	if(Blue>1.0f){Blue = 1.0f;}
	if(Blue<0.0f){Blue = 0.0f;}
}

void TColor::DecBlue(const float &Val)
{
	Blue -= Val;
	if(Blue>1.0f){Blue = 1.0f;}
	if(Blue<0.0f){Blue = 0.0f;}
}

void TColor::IncAlpha(const float &Val)
{
	Alpha += Val;
	if(Alpha>1.0f){Alpha = 1.0f;}
	if(Alpha<0.0f){Alpha = 0.0f;}
}

void TColor::DecAlpha(const float &Val)
{
	Alpha -= Val;
	if(Alpha>1.0f){Alpha = 1.0f;}
	if(Alpha<0.0f){Alpha = 0.0f;}
}


