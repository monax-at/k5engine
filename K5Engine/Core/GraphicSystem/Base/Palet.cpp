#include "Palet.h"


TPalet::TPalet():PLPalet(EN_PL_NOTSET)
{
}

TPalet::TPalet(enPaletType PLFormat):PLPalet(PLFormat)
{
}

TPalet::~TPalet()
{
}


enPaletType TPalet::GetPL() const
{
	return PLPalet;
}

void TPalet::SetPL(enPaletType PLFormat)
{
	PLPalet = PLFormat;
}

bool TPalet::CheckPL(enPaletType PLFormat)
{
	if(PLPalet == PLFormat){ return true;}
	return false;
}


