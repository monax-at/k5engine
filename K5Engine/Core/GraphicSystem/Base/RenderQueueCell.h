///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef RENDERQUEUECELL_H_INCLUDED
#define RENDERQUEUECELL_H_INCLUDED

#include "BaseGraphicObject.h"

class TRenderQueueCell
{
	protected:
		bool View;
		pTBaseGraphicObject Obj;
	public:
		TRenderQueueCell(const pTBaseGraphicObject &ObjVal, const bool &ViewVal = true);
		~TRenderQueueCell();

		void Draw();

		void SetView(const bool &Val);
		bool GetView() const;

		unsigned long GetID  () const;
		float         GetPosZ() const;
};

typedef TRenderQueueCell* pTRenderQueueCell;


#endif // RENDERQUEUECELL_H_INCLUDED
