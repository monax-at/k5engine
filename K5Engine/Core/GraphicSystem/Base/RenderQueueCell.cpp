#include "RenderQueueCell.h"


TRenderQueueCell::TRenderQueueCell(const pTBaseGraphicObject &ObjVal, const bool &ViewVal):
										View(ViewVal), Obj(ObjVal)
{
}

TRenderQueueCell::~TRenderQueueCell()
{
}


void TRenderQueueCell::Draw()
{
	if(View && Obj != NULL){ Obj->Draw(); }
}

void TRenderQueueCell::SetView(const bool &Val)
{
	View = Val;
}

bool TRenderQueueCell::GetView() const
{
	return View;
}

unsigned long TRenderQueueCell::GetID () const
{
	if(Obj != NULL){ return Obj->GetID(); }
	return 0;
}

float TRenderQueueCell::GetPosZ() const
{
	if(Obj != NULL){ return Obj->Pos.GetZ(); }
	return 0.0f;
}


