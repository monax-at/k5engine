#include "RenderQueue.h"


bool CheckRenderQueueCellPosZ(TRenderQueueCell* One,TRenderQueueCell* Two)
{
	return One->GetPosZ() < Two->GetPosZ();
}

TRenderQueue::TRenderQueue():Exception(L"TRenderQueue: "), View(true)
{
}

TRenderQueue::~TRenderQueue()
{
	Clear();
}


void TRenderQueue::Draw()
{
	if(!View){return;}
	int Size(Elems.size());
	for(int i=Size-1;i>=0;i--){ Elems[i]->Draw(); }
}

void TRenderQueue::Clear()
{
	int Size(Elems.size());
	for(int i=Size-1; i>=0; i--){ delete Elems[i]; }
	Elems.clear();
}

void TRenderQueue::Sort()
{
	sort(Elems.begin(), Elems.end(), CheckRenderQueueCellPosZ);
}

void TRenderQueue::Add(const pTRenderQueueCell &Cell)
{
	Elems.push_back(Cell);
}

void TRenderQueue::Add(const pTBaseGraphicObject &Object, const bool &CellView)
{
	Elems.push_back(new TRenderQueueCell(Object, CellView));
}

pTRenderQueueCell TRenderQueue::Get(const int &Index)
{
	int Size(Elems.size());
	Exception(Size > 0,L"in Get(...) list is enpty");
	Exception(Index >=0 && Index <Size,L"in Get(...) Index out of range");

	return Elems[Index];
}

void TRenderQueue::Del(const unsigned long &Id)
{
	Exception(Id > 0,L"in Del(...) Id is 0");

	int Size(Elems.size());
	Exception(Size > 0,L"in Del(...) list is enpty");

	int DeleteSpriteId(-1);
	for(int i=0;i<Size;i++){
		if(Elems[i]->GetID() == Id){
			DeleteSpriteId = i;
			break;
		}
	}

	Exception(DeleteSpriteId!=-1,L"in Del(...) object not exist");

	delete Elems[DeleteSpriteId];
	Elems.erase(Elems.begin()+DeleteSpriteId);
}

void TRenderQueue::Del(const pTBaseGraphicObject &Object)
{
	Del(Object->GetID());
}

void TRenderQueue::SetCellDraw(const unsigned long &Id,const bool &DrawFlag)
{
	int Size(Elems.size());
	for(int i=0;i<Size;i++){
		pTRenderQueueCell Cell(Elems[i]);
		if(Cell->GetID() == Id){
			Cell->SetView(DrawFlag);
			break;
		}
	}
}

bool TRenderQueue::GetCellView(const unsigned long &Id) const
{
	int Size(Elems.size());

	for(int i=0;i<Size;i++){
		TRenderQueueCell *Cell(Elems[i]);
		if(Cell->GetID() == Id){ return Cell->GetView(); }
	}

	return false;
}

void TRenderQueue::SetView(const bool &Val)
{
	View = Val;
}

bool TRenderQueue::GetView() const
{
	return View;
}

int TRenderQueue::GetSize() const
{
	return Elems.size();
}


