///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEGRAPHICOBJECTLISTMANAGER_H_INCLUDED
#define BASEGRAPHICOBJECTLISTMANAGER_H_INCLUDED

#include "../../BaseDevice.h"
#include "RenderQueue.h"

#include <vector>
#include <string>

using std::vector;
using std::wstring;

template <class TObject, class TObjectList>
class TBaseGraphicObjectListManager
{
	protected:
		TExceptionGenerator Exception;

		pTBaseDevice Device;

		pTRenderQueue RenderQueue;
		bool ViewFlag;
		vector<TObjectList*> Elems;
	protected:
		// ---------------------------------//
		inline TObjectList* GetElemIfExist(const int &Id)
		{
			Exception(Device!=NULL, L"in GetElem(Id) Device must be set");
			int Size(Elems.size());
			if(Size == 0){return NULL;}
			if(Id < 0 || Id >= Size){return NULL;}
			return Elems[Id];
		}
		// ---------------------------------//
		inline TObjectList* GetElemIfExist(const wstring &ObjName)
		{
			Exception(Device!=NULL, L"in GetElem(Id) Device must be set");
			int Size(Elems.size());
			if(Size == 0){return NULL;}
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return Elems[i]; }
			}
			return NULL;
		}
		// ---------------------------------//
		inline TObjectList* GetElem(const int &Id)
		{
			Exception(Device!=NULL, L"in GetElem(Id) Device must be set");

			int Size(Elems.size());
			Exception(Size>0, 		 	L"in GetElem(Id) list is empty");
			Exception(Id>=0 && Id<Size, L"in GetElem(Id) Id out of range");

			return Elems[Id];
		}
		// ---------------------------------//
		inline TObjectList* GetElem(const wstring &ObjName)
		{
			Exception(Device!=NULL, L"in GetElem(ObjName) Device must be set");

			int Size(Elems.size());
			Exception(Size>0, L"in GetElem(ObjName) list is empty");

			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return Elems[i]; }
			}

			Exception(L"in GetElem(ObjName) list whis ObjName: "+ObjName+L" not exist");
			return NULL;
		}
		// ---------------------------------//
		TObject* GetElem(const int &List, const int &Id)
		{
			return GetElem(List)->Get(Id);
		}
		// ---------------------------------//
		TObject* GetElem(const int &List, const unsigned long &Id)
		{
			return GetElem(List)->Get(Id);
		}
		// ---------------------------------//
		TObject* GetElem(const int &List, const wstring &ObjName)
		{
			return GetElem(List)->Get(ObjName);
		}
		// ---------------------------------//
		TObject* GetElem(const wstring &List, const int &Id)
		{
			return GetElem(List)->Get(Id);
		}
		// ---------------------------------//
		TObject* GetElem(const wstring &List, const unsigned long &Id)
		{
			return GetElem(List)->Get(Id);
		}
		// ---------------------------------//
		TObject* GetElem(const wstring &List, const wstring &ObjName)
		{
			return GetElem(List)->Get(ObjName);
		}
		// ---------------------------------//
	public:
		// ---------------------------------//
		TBaseGraphicObjectListManager():
					Device(NULL),RenderQueue(NULL),ViewFlag(true)
		{
		}
		// ---------------------------------//
		virtual ~TBaseGraphicObjectListManager()
		{
			Clear();
		}
		// ---------------------------------//
		void SetRenderQueue(const pTRenderQueue &Val)
		{
			RenderQueue = Val;

			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				Elems[i]->SetRenderQueue(RenderQueue);
			}
		}
		// ---------------------------------//
		void SetRenderQueue(TRenderQueue &Val)
		{
			SetRenderQueue(&Val);
		}
		// ---------------------------------//
		pTRenderQueue GetRenderQueue()
		{
			return RenderQueue;
		}
		// ---------------------------------//
		void SortRenderQueue()
		{
			if(RenderQueue!=NULL){ RenderQueue->Sort(); }
		}
		// ---------------------------------//
		void SetView(const bool &Val)
		{
			ViewFlag = Val;
		}
		// ---------------------------------//
		bool GetView() const
		{
			return ViewFlag;
		}
		// ---------------------------------//
		TObjectList* operator[](const int &Id)
		{
			return GetElem(Id);
		}
		// ---------------------------------//
		TObjectList* operator[](const wstring &ObjName)
		{
			return GetElem(ObjName);
		}
		// ---------------------------------//
		TObjectList* Get(const int &Id)
		{
			return GetElem(Id);
		}
		// ---------------------------------//
		TObjectList* Get(const wstring &ObjName)
		{
			return GetElem(ObjName);
		}
		// ---------------------------------//
		TObjectList* GetLast()
		{
			return GetElem(Elems.size()-1);
		}
		// ---------------------------------//
		TObjectList* Get(TObject* Obj)
		{
       		if(Obj == NULL){ return NULL; }

       		int Size(Elems.size());
			if(Size==0){ return NULL; }

			unsigned long ObjId(Obj->GetID());

			for(int i=0;i<Size;i++){
				if(Elems[i]->IfExist(ObjId)){return Elems[i];}
			}

			return NULL;
		}
		// ---------------------------------//
		TObjectList* GetByGraphicObjectId(unsigned long ObjId)
		{
       		int Size(Elems.size());
			if(Size==0){ return NULL; }

			for(int i=0;i<Size;i++){
				if(Elems[i]->IfExist(ObjId)){return Elems[i];}
			}

			return NULL;
		}
		// ---------------------------------//
		TObject* GetLast(const int &List)
		{
			return GetElem(List)->GetLast();
		}
		// ---------------------------------//
		TObject* GetLast(const wstring &List)
		{
			return GetElem(List)->GetLast();
		}
		// ---------------------------------//
		TObject* Get(const int &List, const int &Id)
		{
			return GetElem(List,Id);
		}
		// ---------------------------------//
		TObject* Get(const int &List, const unsigned long &Id)
		{
			return GetElem(List,Id);
		}
		// ---------------------------------//
		TObject* Get(const int &List, const wstring &ObjName)
		{
			return GetElem(List,ObjName);
		}
		// ---------------------------------//
		TObject* Get(const wstring &List, const int &Id)
		{
			return GetElem(List,Id);
		}
		// ---------------------------------//
		TObject* Get(const wstring &List, const unsigned long &Id)
		{
			return GetElem(List,Id);
		}
		// ---------------------------------//
		TObject* Get(const wstring &List, const wstring &ObjName)
		{
			return GetElem(List,ObjName);
		}
		// ---------------------------------//
		TObjectList* Add(TObjectList* Obj, const bool &SetDeviceFlag=true)
		{
			if(SetDeviceFlag){Obj->SetDevice(Device);}

			Obj->SetRenderQueue(RenderQueue);

			Elems.push_back(Obj);

			return Obj;
		}
		// ---------------------------------//
		TObjectList* Add(const wstring &ObjName)
		{
			Exception(Device!=NULL	 , 			L"in Add(ObjName) Device must be set");
			Exception(ObjName.length()>0, 		L"in Add(ObjName) ObjName not set");
			Exception(IfExist(ObjName) == false,L"in Add(ObjName) list whis ObjName: "+ ObjName +L" already exist");

			TObjectList *List(new TObjectList);

			List->SetName       (ObjName);
			List->SetDevice     (Device);
			List->SetRenderQueue(RenderQueue);

			Elems.push_back(List);

			return List;
		}
		// ---------------------------------//
		TObjectList* AddIfNotExist(const wstring &ObjName)
		{
			Exception(Device != NULL    , L"in AddIfNotExist(ObjName) Device must be set");
			Exception(ObjName.length()>0, L"in AddIfNotExist(ObjName) ObjName not set");

			if(IfExist(ObjName)){return Get(ObjName);}

			TObjectList *List(new TObjectList);

			List->SetName       (ObjName);
			List->SetDevice     (Device);
			List->SetRenderQueue(RenderQueue);

			Elems.push_back(List);

			return List;
		}
		// ---------------------------------//
		TObject* Add(const int &List, const TObject &Object)
		{
			return GetElem(List)->Add(Object);
		}
		// ---------------------------------//
		TObject* Add(const int &List, TObject *Object)
		{
			return GetElem(List)->Add(Object);
		}
		// ---------------------------------//
		TObject* Add(const wstring &List, const TObject &Object)
		{
			return GetElem(List)->Add(Object);
		}
		// ---------------------------------//
		TObject* Add(const wstring &List, TObject *Object)
		{
			return GetElem(List)->Add(Object);
		}
		// ---------------------------------//
		void Del(const int &Id)
		{
			Exception(Device!=NULL, L"in Del(Id) Device must be set");

			int Size(Elems.size());
			Exception(Id >= 0 && Id < Size, L"in Del(Id) Id out of range");

			delete Elems[Id];
			Elems.erase(Elems.begin() + Id);
		}
		// ---------------------------------//
		void Del(const wstring &ObjName)
		{
			Exception(Device!=NULL, L"in Delete(ObjName) Device must be set");

			int Size(Elems.size());
			Exception(Size>0, L"in Del(ObjName) list is empty");

			int Pos(-1);
			for(int i=0;i<Size;i++){ if(Elems[i]->GetName() == ObjName){Pos = i; break; } }

			Exception(Pos!=-1, L"in Del(ObjName) list whis ObjName: " + ObjName + L" not exist");

			delete Elems[Pos];
			Elems.erase(Elems.begin() + Pos);
		}
		// ---------------------------------//
		void Del(TObjectList* ObjectPtr)
		{
			Exception(Device!=NULL, L"in Delete(ObjectPtr) Device must be set");

			int Size(Elems.size());
			Exception(Size>0, L"in Del(ObjectPtr) list is empty");

			int Pos(-1);
			for(int i=0;i<Size;i++){ if(Elems[i] == ObjectPtr){Pos = i; break; } }

			Exception(Pos!=-1, L"in Del(ObjectPtr) list ot exist");

			delete Elems[Pos];
			Elems.erase(Elems.begin() + Pos);
		}
		// ---------------------------------//
		void DelIfExist(const wstring &ObjName)
		{
			Exception(Device!=NULL, L"in DelIfExist(ObjName) Device must be set");
			int Size(Elems.size());
			if(Size <= 0){return;}

			int Pos(-1);
			for(int i=0;i<Size;i++){ if(Elems[i]->GetName() == ObjName){Pos = i; break; } }

			if(Pos == -1){return;}

			delete Elems[Pos];
			Elems.erase(Elems.begin() + Pos);
		}
		// ---------------------------------//
		void DelIfExist(TObjectList* ObjectPtr)
		{
			Exception(Device!=NULL, L"in DelIfExist(ObjectPtr) Device must be set");
			int Size(Elems.size());
			if(Size <= 0){return;}

			int Pos(-1);
			for(int i=0;i<Size;i++){ if(Elems[i] == ObjectPtr){Pos = i; break; } }

			if(Pos == -1){return;}

			delete Elems[Pos];
			Elems.erase(Elems.begin() + Pos);
		}
		// ---------------------------------//
		void Del(const int &List, const int &Id)
		{
			GetElem(List)->Del(Id);
		}
		// ---------------------------------//
		void Del(const int &List, const wstring &ObjName)
		{
			GetElem(List)->Del(ObjName);
		}
		// ---------------------------------//
		void Del(const int &List, const unsigned long &Id)
		{
			GetElem(List)->Del(Id);
		}
		// ---------------------------------//
		void Del(const wstring &List, const int &Id)
		{
			GetElem(List)->Del(Id);
		}
		// ---------------------------------//
		void Del(const wstring &List, const wstring &ObjName)
		{
			GetElem(List)->Del(ObjName);
		}
		// ---------------------------------//
		void Del(const wstring &List, const unsigned long &Id)
		{
			GetElem(List)->Del(Id);
		}
		// ---------------------------------//
		void DelIfExist(const int &List, const unsigned long &Id)
		{
			int Size(Elems.size());
			if(List >= Size){return;}

			GetElem(List)->DelIfExist(Id);
		}
		// ---------------------------------//
		void DelIfExist(const wstring &List, const unsigned long &Id)
		{
			if(!IfExist(List)){return;}

			GetElem(List)->DelIfExist(Id);
		}
		// ---------------------------------//
		void DelIfExist(const int &List, wstring &ObjName)
		{
			int Size(Elems.size());
			if(List >= Size){return;}

			GetElem(List)->DelIfExist(ObjName);
		}
		// ---------------------------------//
		void DelIfExist(const wstring &List, wstring &ObjName)
		{
			if(!IfExist(List)){return;}

			GetElem(List)->DelIfExist(ObjName);
		}
		// ---------------------------------//
		void DelGraphicObjectIfExist(const wstring &ObjName)
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){ Elems[i]->DelIfExist(ObjName); }
		}
		// ---------------------------------//
		void DelGraphicObjectIfExist(const unsigned long &Id)
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){ Elems[i]->DelIfExist(Id); }
		}
		// ---------------------------------//
		bool IfExist(const wstring &ObjName)
		{
			Exception(Device!=NULL, L"in IfExist(ObjName) Device must be set");
			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){return true;}
			}
			return false;
		}
		// ---------------------------------//
		bool IfExist(const int &List, const int &Id)
		{
			return GetElem(List)->IfExist(Id);
		}
		// ---------------------------------//
		bool IfExist(const int &List, const wstring &ObjName)
		{
			return GetElem(List)->IfExist(ObjName);
		}
		// ---------------------------------//
		bool IfExist(const wstring &List, const int &Id)
		{
			return GetElem(List)->IfExist(Id);
		}
		// ---------------------------------//
		bool IfExist(const wstring &List, const wstring &ObjName)
		{
			return GetElem(List)->IfExist(ObjName);
		}
		// ---------------------------------//
		TObjectList* GetIfExist(const int &Id)
		{
			return GetElemIfExist(Id);
		}
		// ---------------------------------//
		TObjectList* GetIfExist(const wstring &ObjName)
		{
			return GetElemIfExist(ObjName);
		}
		// ---------------------------------//
		TObject* GetIfExist(const int &List, const int &Id)
		{
			TObjectList* ObjList = GetElemIfExist(List);
			if (ObjList == NULL){return NULL;}
			return ObjList->GetIfExist(Id);
		}
		// ---------------------------------//
		TObject* GetIfExist(const int &List, const wstring &ObjName)
		{
			TObjectList* ObjList = GetElemIfExist(List);
			if (ObjList == NULL){return NULL;}
			return ObjList->GetIfExist(ObjName);
		}
		// ---------------------------------//
		TObject* GetIfExist(const wstring &List, const int &Id)
		{
			TObjectList* ObjList = GetElemIfExist(List);
			if (ObjList == NULL){return NULL;}
			return ObjList->GetIfExist(Id);
		}
		// ---------------------------------//
		TObject* GetIfExist(const wstring &List, const wstring &ObjName)
		{
			TObjectList* ObjList = GetElemIfExist(List);
			if (ObjList == NULL){return NULL;}
			return ObjList->GetIfExist(ObjName);
		}
		// ---------------------------------//
		void SetDevice(const pTBaseDevice &Val)
		{
			Device = Val;

			int Size(Elems.size());
			if(Size == 0){return;}

			for(int i=0;i<Size;i++){
				Elems[i]->SetDevice(Device);
			}
		}
		// ---------------------------------//
		void SetDevice(TBaseDevice &Val)
		{
			SetDevice(&Val);
		}
		// ---------------------------------//
		pTBaseDevice GetDevice()
		{
			return Device;
		}
		// ---------------------------------//
		int  GetSize() const
		{
			return Elems.size();
		}
		// ---------------------------------//
		void Draw()
		{
			if(!ViewFlag){return;}

			int Size(Elems.size());
			for(int i=0;i<Size;i++){ Elems[i]->Draw(); }
		}
		// ---------------------------------//
		void Clear()
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){ delete Elems[i]; }
			Elems.clear();
		}
		// ---------------------------------//
};


#endif // BASEGRAPHICOBJECTLISTMANAGER_H_INCLUDED
