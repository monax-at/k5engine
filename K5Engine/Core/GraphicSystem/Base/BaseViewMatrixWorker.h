///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEVIEWMATRIXWORKER_H_INCLUDED
#define BASEVIEWMATRIXWORKER_H_INCLUDED


#include "../../../LibMath/Matrix3D.h"


class TBaseViewMatrixWorker
{
	protected:
		TMatrix3D Transform;
	public:
		TBaseViewMatrixWorker();
		virtual ~TBaseViewMatrixWorker();

		void Scale		(const float &X,const float &Y);
		void Translate	(const float &X,const float &Y,const float &Z);
		void Rotate		(const float &Angle);

		void Clear();

		virtual void Run() 		= 0;
		virtual void Stop() 	= 0;
};

typedef TBaseViewMatrixWorker* pTBaseViewMatrixWorker;


#endif // VIEWMATRIXWORKER_H_INCLUDED
