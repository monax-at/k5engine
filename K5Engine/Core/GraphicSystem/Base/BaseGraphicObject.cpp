#include "BaseGraphicObject.h"


TBaseGraphicObject::TBaseGraphicObject():TBaseSceneObject(), View(true), Device(NULL),
					TexMinFilter(EN_TF_Unknown), TexMagFilter(EN_TF_Unknown)
{
}

TBaseGraphicObject::TBaseGraphicObject(const TBaseGraphicObject &Val):TBaseSceneObject()
{
	SetDefParams(&Val);
}

TBaseGraphicObject::~TBaseGraphicObject()
{
}


void TBaseGraphicObject::SetDefParams(const TBaseGraphicObject *Val)
{
	View = Val->GetView();
	Name = Val->GetName();

	TexMinFilter = Val->GetMinFilter();
	TexMagFilter = Val->GetMagFilter();

	Pos    = Val->Pos;
	Center = Val->Center;
	Angle  = Val->Angle;
}

void TBaseGraphicObject::operator=(const TBaseGraphicObject &Val)
{
	SetDefParams(&Val);
}

void TBaseGraphicObject::SetView(const bool &Val)
{
	View = Val;
}

bool TBaseGraphicObject::GetView() const
{
	return View;
}

void TBaseGraphicObject::SetDevice(const pTBaseDevice &Val)
{
	Device = Val;
	ToSetDevice();
}

pTBaseDevice TBaseGraphicObject::GetDevice()
{
	return Device;
}

void TBaseGraphicObject::SetMinFilter(const enTextureFilter &Val)
{
	TexMinFilter = Val;
}

void TBaseGraphicObject::SetMagFilter(const enTextureFilter &Val)
{
	TexMagFilter = Val;
}

void TBaseGraphicObject::SetFilter(const enTextureFilter &Val)
{
	TexMinFilter = Val;
	TexMagFilter = Val;
}

enTextureFilter TBaseGraphicObject::GetMinFilter() const
{
	return TexMinFilter;
}

enTextureFilter TBaseGraphicObject::GetMagFilter() const
{
	return TexMagFilter;
}

void TBaseGraphicObject::Draw()
{
	if(Device == NULL || View == false){return;}

	enTextureFilter TempMinFilter;
	enTextureFilter TempMagFilter;

	if(TexMinFilter!=EN_TF_Unknown){
		TempMinFilter = Device->GetMinTextureFilter();
		if(TexMinFilter!=TempMinFilter){
			Device->SetMinTextureFilter(TexMinFilter);
		}
	}
	if(TexMagFilter!=EN_TF_Unknown){
		TempMagFilter = Device->GetMagTextureFilter();
		if(TexMagFilter!=TempMagFilter){
			Device->SetMagTextureFilter(TexMagFilter);
		}
	}

	ToDraw();

	if(TexMinFilter!=EN_TF_Unknown){
		if(TexMinFilter!=TempMinFilter){Device->SetMinTextureFilter(TempMinFilter);}
	}
	if(TexMagFilter!=EN_TF_Unknown){
		if(TexMagFilter!=TempMagFilter){Device->SetMagTextureFilter(TempMagFilter);}
	}
}


