/// Authors:
///	Sergey Kalenik [monax.at@gmail.com]
///	Maxim Kolesnikov [mpkmaximus@gmail.com]

#pragma once
#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include "enum_GraphicSystem.h"

#include "../../ExceptionGenerator.h"
#include "../../../LibTools/Tools.h"

#include <string>
using std::wstring;

struct TRGBAColor
{
    unsigned char R;
    unsigned char G;
    unsigned char B;
    unsigned char A;
};

typedef TRGBAColor* pTRGBAColor;


class TTextureRawData
{
	public:
		int 		   Width;
		int 		   Height;
		unsigned char *TextureData;

	public:
		TTextureRawData();
		TTextureRawData(int WidthVal, int HeightVal, unsigned char *TextureDataVal);
		virtual ~TTextureRawData();

		TTextureRawData *Clone();
};

typedef TTextureRawData* pTTextureRawData;


class TTexture
{
	protected:
		TExceptionGenerator Exception;

		static unsigned long IdCounter;
		unsigned long        Id;

		wstring Name;
		wstring Path;

		bool ActiveFlag;
		bool LockFlag;

		int Width;
		int Height;
	protected:
        virtual void ToRun()  = 0;
        virtual void ToStop() = 0;
        virtual void ToDel()  = 0;

		virtual TTexture* ToCreateMask( const wstring &MaskName) = 0;
		virtual TTexture* ToCreateCutMask(const wstring &MaskName) = 0;

		virtual TTexture* ToClone(const wstring &TextName) = 0;

		virtual TTexture* ToCopyRect(const wstring &TextName, const int &RectWidth,const int &RectHeight,
							 const int &X,const int &Y, const bool &UseDeg2TexSizeFlag) = 0;

		virtual pTTextureRawData ToGetCopyRawData(const int &WidthVal, const int &HeightVal,
												  const int &XVal, const int &YVal) = 0;

		virtual void ToWriteRawData(const pTTextureRawData &RawDataVal,
									const int &X, const int &Y, const enTexWriteMode &ModeVal) = 0;
    public:
        TTexture();
        virtual ~TTexture();

		void SetName(const wstring &Val);
		void SetPath(const wstring &Val);

		void SetActive(const bool &Val);

		unsigned long GetID() const;

		wstring GetName() const;
		wstring GetPath() const;

		bool GetActive() const;

		int GetWidth () const;
		int GetHeight() const;

        void Run();
        void Stop();
        void Del();

		// Получение маски и создание новой текстуры
        TTexture *CreateMask   (const wstring &MaskName = L"");
		TTexture *CreateCutMask(const wstring &MaskName = L"");

		// Клонирование текстуры
        TTexture *Clone(const wstring &TextName = L"");

		// Функциия копирываения участка текстуры и создание новой текстуры
		TTexture* CopyRect(const wstring &TextName, const int &RectWidth,const int &RectHeight,
							 const int &X,const int &Y, const bool &UseDeg2TexSizeFlag = true);

		// Функции для работы с данными текстуры
		// Данные текстуры возвращаются только в формате R8G8B8A8, остальные форматы не подерживаются.
		pTTextureRawData GetCopyRawData(const int &RectWidth, const int &RectHeight,
										const int &X=0, const int &Y=0);

		void WriteRawData(const pTTextureRawData &RawDataVal, const int &X=0, const int &Y=0,
						  const enTexWriteMode &ModeVal = EN_TWM_Normal);
};

typedef TTexture* pTTexture;


#endif // TEXTURE_H_INCLUDED
