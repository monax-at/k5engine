///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef BASEGRAPHICOBJECTLIST_H_INCLUDED
#define BASEGRAPHICOBJECTLIST_H_INCLUDED

#include "RenderQueue.h"
#include "BaseVertexData.h"
#include "BaseGraphicObject.h"

template <class TObject>
class TBaseGraphicObjectList
{
    protected:
		TExceptionGenerator Exception;
		wstring 			Name;

		pTBaseDevice		Device;

		vector<TObject*>	Elems;

		bool 				ViewFlag;
		pTRenderQueue		RenderQueue;
	protected:
		inline TObject* GetElem(const int &Index, const wstring &ExFuncName)
		{
			int Size(Elems.size());
			Exception(Index >= 0 && Index < Size, L"in " + ExFuncName + L" Id out of range");
			return Elems[Index];
		}
		// ---------------------------------//
		inline TObject* GetElem(const unsigned long &ObjId, const wstring &ExFuncName)
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in " + ExFuncName + L" list is enpty");

			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return Elems[i]; }
			}

			Exception(L"in " + ExFuncName + L" object not exist");
			return NULL;
		}
		// ---------------------------------//
		inline TObject* GetElem(const wstring &ObjName, const wstring &ExFuncName)
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in " + ExFuncName + L" list is enpty");

			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return Elems[i]; }
			}

			Exception(L"in " + ExFuncName + L" object whis name: "+ ObjName + L" not exist");
			return NULL;
		}
		// ---------------------------------//
		inline TObject* GetElem(const int &Index, const wstring &ExFuncName) const
		{
			int Size(Elems.size());
			Exception(Index >= 0 && Index < Size, L"in " + ExFuncName + L" Id out of range");
			return Elems[Index];
		}
		// ---------------------------------//
		inline TObject* GetElem(const unsigned long &ObjId, const wstring &ExFuncName) const
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in " + ExFuncName + L" list is enpty");

			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return Elems[i]; }
			}

			Exception(L"in " + ExFuncName + L" object not exist");
			return NULL;
		}
		// ---------------------------------//
		inline TObject* GetElem(const wstring &ObjName, const wstring &ExFuncName) const
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in " + ExFuncName + L" list is enpty");

			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return Elems[i]; }
			}

			Exception(L"in " + ExFuncName + L" object whis name: "+ ObjName + L" not exist");
			return NULL;
		}
    public:
		TBaseGraphicObjectList()
		{
			Device		 = NULL;
			ViewFlag	 = true;
			RenderQueue	 = NULL;
		}
		// ---------------------------------//
		~TBaseGraphicObjectList()
		{
			Clear();
		}
		// ---------------------------------//
		void SetName(const wstring &Val)
		{
			Name = Val;
		}
		// ---------------------------------//
		wstring GetName() const
		{
			return Name;
		}
		// ---------------------------------//
		void SetView(const bool &Val)
		{
			ViewFlag = Val;
			if(RenderQueue == NULL){return;}

			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				RenderQueue->SetCellDraw(Elems[i]->GetID(),ViewFlag);
			}
		}
		// ---------------------------------//
		bool GetView() const
		{
			return ViewFlag;
		}
		// ---------------------------------//
		void operator=(TBaseGraphicObjectList<TObject> &Val)
		{
			Clear();

			Name     = Val.GetName();
			ViewFlag = Val.GetView();

			int ValSize(Val.GetSize());
			for(int i=0;i<ValSize;i++){
				TObject* Object(new TObject);
				*Object = *Val.Get(i);
				Add(Object);
			}
		}
		// ---------------------------------//
		TObject* operator[](const int &Index)
		{
			return GetElem(Index, L"operator[](Index)");
		}
		// ---------------------------------//
		TObject* Get(const int &Index)
		{
			return GetElem(Index, L"Get(Index)");
		}
		// ---------------------------------//
		TObject* operator[](const unsigned long &ObjId)
		{
			return GetElem(ObjId, L"operator[](ObjId)");
		}
		// ---------------------------------//
		TObject* Get(const unsigned long &ObjId)
		{
			return GetElem(ObjId, L"Get(ObjId)");
		}
		// ---------------------------------//
		TObject* operator[](const wstring &ObjName)
		{
			return GetElem(ObjName, L"operator[](ObjName)");
		}
		// ---------------------------------//
		TObject* Get(const wstring &ObjName)
		{
			return GetElem(ObjName, L"Get(ObjName)");
		}
		// ---------------------------------//
		TObject* operator[](const int &Index) const
		{
			return GetElem(Index, L"operator[](Index)");
		}
		// ---------------------------------//
		TObject* Get(const int &Index) const
		{
			return GetElem(Index, L"Get(Index)");
		}
		// ---------------------------------//
		TObject* operator[](const unsigned long &ObjId) const
		{
			return GetElem(ObjId, L"operator[](ObjId)");
		}
		// ---------------------------------//
		TObject* Get(const unsigned long &ObjId) const
		{
			return GetElem(ObjId, L"Get(ObjId)");
		}
		// ---------------------------------//
		TObject* operator[](const wstring &ObjName) const
		{
			return GetElem(ObjName, L"operator[](ObjName)");
		}
		// ---------------------------------//
		TObject* Get(const wstring &ObjName) const
		{
			return GetElem(ObjName, L"Get(ObjName)");
		}
		// ---------------------------------//
		TObject* GetLast()
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in GetLast() list is enpty");
			return Elems[Size-1];
		}
		// ---------------------------------//
		bool IfExist(const unsigned long &ObjId)
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return true; }
			}
			return false;
		}
		// ---------------------------------//
		bool IfExist(const wstring &ObjName)
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return true; }
			}
			return false;
		}
		// ---------------------------------//
		TObject* GetIfExist(const unsigned long &ObjId)
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return Elems[i]; }
			}
			return NULL;
		}
		// ---------------------------------//
		TObject* GetIfExist(const wstring &ObjName)
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return Elems[i]; }
			}
			return NULL;
		}
		// ---------------------------------//
		int  GetSize() const
		{
			return Elems.size();
		}
		// ---------------------------------//
		int GetIndex(const wstring &ObjName) const
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return i; }
			}
			return -1;
		}
		// ---------------------------------//
		int GetIndex(const unsigned long &ObjId) const
		{
			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return i; }
			}
			return -1;
		}
		// ---------------------------------//
		void SetDevice(const pTBaseDevice Val)
		{
			Device = Val;

			int Size(Elems.size());
			for(int i=0;i<Size;i++){
				Elems[i]->SetDevice(Device);
			}
		}
		// ---------------------------------//
		void SetDevice(TBaseDevice &Val)
		{
			SetDevice(&Val);
		}
		// ---------------------------------//
		pTBaseDevice GetDevice()
		{
			return Device;
		}
		// ---------------------------------//
		void SetRenderQueue(const pTRenderQueue &Val)
		{
			int Size(Elems.size());

			if(RenderQueue != NULL){
				for(int i=0; i<Size; i++){
					TObject *Elem = Elems[i];
					if(Elem != NULL){ RenderQueue->Del(Elem->GetID()); }
				}
			}

			RenderQueue = Val;

			if(RenderQueue != NULL){
				for(int i=0; i<Size; i++){
					TObject *Elem = Elems[i];
					if(Elem != NULL){ RenderQueue->Add(Elem, ViewFlag); }
				}
			}
		}
		// ---------------------------------//
		void SetRenderQueue(TRenderQueue &Val)
		{
			SetRenderQueue(&Val);
		}
		// ---------------------------------//
		pTRenderQueue GetRenderQueue()
		{
			return RenderQueue;
		}
		// ---------------------------------//
		void SortRenderQueue()
		{
			if(RenderQueue!=NULL){ RenderQueue->Sort(); }
		}
		// ---------------------------------//
		TObject* Add(TObject *Object, const bool &UpdateDevice=true)
		{
			if(Device!=NULL && UpdateDevice==true){
				Object->SetDevice(Device);
			}

			Elems.push_back(Object);

			if(RenderQueue!= NULL){ RenderQueue->Add(Object, ViewFlag); }

			return Object;
		}
		// ---------------------------------//
		TObject* Add(const TObject &Object)
		{
			TObject *NewElem = new TObject;
			*NewElem = Object;

			return Add(NewElem);
		}
		// ---------------------------------//
		void Del(const int &Id)
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in Del(Id) list is enpty");
			Exception(Id >= 0 && Id < Size,L"in Del(Id) Id out of range");

			TObject *Elem = Elems[Id];
			if(RenderQueue!= NULL){ RenderQueue->Del(Elem->GetID()); }
			delete Elem;

			Elems.erase(Elems.begin()+Id);
		}
		 // ---------------------------------//
		void Del(const unsigned long &ObjId)
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in Del(ObjId) list is enpty");

			int DeleteId(-1);
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ DeleteId = i; break; }
			}
			Exception(DeleteId!=-1,L"in Del(ObjId) object whis id not exist");

			TObject *Elem = Elems[DeleteId];
			if(RenderQueue!= NULL){ RenderQueue->Del(Elem->GetID()); }
			delete Elem;

			Elems.erase(Elems.begin()+DeleteId);
		}
		// ---------------------------------//
		void Del(const wstring &ObjName)
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in Del(ObjName) list is enpty");

			int DeleteId(-1);
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ DeleteId = i; break; }
			}

			Exception(DeleteId!=-1,L"in Del(ObjName) object whis name: "+ObjName+L" not exist");

			TObject *Elem = Elems[DeleteId];
			if(RenderQueue!= NULL){ RenderQueue->Del(Elem->GetID()); }
			delete Elem;

			Elems.erase(Elems.begin()+DeleteId);
		}
		// ---------------------------------//
		void DelIfExist(const unsigned long &ObjId)
		{
			int Size(Elems.size());
			if(Size == 0){return;}

			int DeleteId = -1;
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){
					DeleteId = i;
					break;
				}
			}
			if(DeleteId == -1){return;}

			TObject *Elem = Elems[DeleteId];
			if(RenderQueue!= NULL){ RenderQueue->Del(Elem->GetID()); }
			delete Elem;

			Elems.erase(Elems.begin()+DeleteId);
		}
		// ---------------------------------//
		void DelIfExist(const wstring &ObjName)
		{
			int Size(Elems.size());
			if(Size == 0){return;}

			int DeleteId = -1;
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){
					DeleteId = i;
					break;
				}
			}
			if(DeleteId == -1){return;}

			TObject *Elem = Elems[DeleteId];
			if(RenderQueue!= NULL){ RenderQueue->Del(Elem->GetID()); }
			delete Elem;

			Elems.erase(Elems.begin()+DeleteId);
		}
		// ---------------------------------//
		TObject* Extract(const int &Index)
		{
			int Size(Elems.size());
			Exception(Size > 0                  , L"in Extract(Index) list is enpty");
			Exception(Index >= 0 && Index < Size, L"in Extract(Index) Id out of range");

			TObject *Elem = Elems[Index];
			if(RenderQueue!= NULL){ RenderQueue->Del(Elem->GetID()); }
			Elems.erase(Elems.begin()+Index);

			return Elem;
		}
		// ---------------------------------//
		TObject* Extract(const unsigned long &ObjId)
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in Extract(ObjId) list is enpty");

			int ExtractId = -1;
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){
					ExtractId = i;
					break;
				}
			}
			Exception(ExtractId!=-1,L"in Extract(ObjId) object not exist");

			TObject *Elem = Elems[ExtractId];
			if(RenderQueue!= NULL){ RenderQueue->Del(Elem->GetID()); }
			Elems.erase(Elems.begin()+ExtractId);

			return Elem;
		}
		// ---------------------------------//
		TObject* Extract(const wstring &ObjName)
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in Extract(ObjName) list is enpty");

			int ExtractId = -1;
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){
					ExtractId = i;
					break;
				}
			}

			Exception(ExtractId!=-1,
					  L"in Extract(ObjName) object whis name: " + ObjName + L" not exist");

			TObject *Elem = Elems[ExtractId];
			if(RenderQueue!= NULL){ RenderQueue->Del(Elem->GetID()); }
			Elems.erase(Elems.begin()+ExtractId);

			return Elem;
		}
		// ---------------------------------//
		void Clear()
		{
			int Size(Elems.size());
			for(int i=0; i<Size; i++){
				TObject *Elem = Elems[i];
				if(Elem!=NULL){
					if(RenderQueue != NULL){ RenderQueue->Del(Elem->GetID()); }
					delete Elem;
				}
			}
			Elems.clear();
		}
		// ---------------------------------//
		void ClearElems()
		{
			Elems.clear();
		}
		// ---------------------------------//
		void Draw()
		{
			if(!ViewFlag){return;}

			// обратный обход используется потому, что теоретически
			// он работает немного быстрее (сравнение i с нулём быстрее выполняется).
			// чем сравнение i с числом. Но это только теория и прирост если и есть,
			// то очень не значительный.
			const int Size(Elems.size() -1);
			for(int i=Size; i>=0; i--){ Elems[i]->Draw(); }
		}
		// ---------------------------------//
		// так как размещение объектов в списке влияет на отрисовку
		// (правильное смешиварие альфа канала), то те объектты, которые находятся
		// ниже по Z координате, должны отрисовываться первыми.
		// Что бы расставить объекты в нужном порядке и предназначена функция Sort()
		void Sort()
		{
			sort(Elems.begin(),Elems.end(), CheckObjectPosZ<TObject> );
		}
		// ---------------------------------//
		void Merge(TBaseGraphicObjectList &List)
		{
			while(List.GetSize()>0){ Add(List.Extract(0)); }
		}
		// ---------------------------------//
		void Merge(TBaseGraphicObjectList *List)
		{
			if(List == NULL){return;}
			Merge(*List);
		}
		// ---------------------------------//
};


#endif // CLASS_BASEGRAPHICOBJECTLIST_H_INCLUDED
