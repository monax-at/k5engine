#include "BaseVertexData.h"

unsigned long TBaseVertexData::IdCounter = 0;
unsigned long TBaseVertexData::IdActive = 0;

TBaseVertexData::TBaseVertexData()
{
	IdCounter++;
	Id = IdCounter;
}

TBaseVertexData::TBaseVertexData(const unsigned long &IdVal):Id(IdVal)
{
}

TBaseVertexData::~TBaseVertexData()
{
	if(IdActive == Id){IdActive=0;}
}


unsigned long TBaseVertexData::GetId()  const
{
	return Id;
}


