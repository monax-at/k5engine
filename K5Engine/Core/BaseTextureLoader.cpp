#include "BaseTextureLoader.h"


TBaseTextureLoader::TBaseTextureLoader():Exception(L"TBaseTextureLoader: "),
										 UseEngineFilePathFormat(false)
{
}

TBaseTextureLoader::~TBaseTextureLoader()
{
}


void TBaseTextureLoader::SetUseEngineFilePathFormat(const bool &Val)
{
	UseEngineFilePathFormat = Val;
}

bool TBaseTextureLoader::GetUseEngineFilePathFormat() const
{
	return UseEngineFilePathFormat;
}


