

///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef ENUM_EVENTTYPES_H_INCLUDED
#define ENUM_EVENTTYPES_H_INCLUDED


enum enEventTypes
{
///-- флаги состояния события
EN_EVENTFALSE	= false,
EN_EVENTTRUE	= true,
EN_ERROR		= 2,

///-- Типы событий
EN_NONE		= 5,
EN_SYSTEM	= 10,
EN_KEYBOARD	= 20,
EN_MOUSE	= 30,
EN_TIMER	= 40,
EN_USER		= 50,

///-- типы событтий для системы
EN_QUIT			 = 100,
EN_WINDOWRESIZE	 = 101,
EN_ACTIVATE		 = 102,
EN_DEACTIVATE	 = 103,
EN_PAINT		 = 104,
EN_MINIMIZE		 = 105,
EN_RESTORE		 = 106,
EN_DEVICELOST 	 = 107,
EN_DEVICERESTORE = 108,
EN_DISPLAYCHANGE = 109,

///-- типы событтия для клавиатуры
EN_KEYDOWN	= 200,
EN_KEYUP	= 201,
EN_KEYCHAR  = 202,

///-- модификаторы нажатых кнопок
EN_KM_NONE             	= 210,
EN_KM_NUM              	= 211,    // нажат Numlock
EN_KM_CAPS             	= 212,
EN_KM_LCTRL            	= 213,
EN_KM_RCTRL            	= 214,
EN_KM_RSHIFT           	= 215,
EN_KM_LSHIFT           	= 216,
EN_KM_RALT             	= 217,
EN_KM_LALT             	= 218,
EN_KM_SHIFT            	= 219,
EN_KM_ALT              	= 220,

///-- параметр KeySum - "виртуальные" значения кнопок
EN_KEY_0               	= 2000,
EN_KEY_1               	= 2001,
EN_KEY_2               	= 2002,
EN_KEY_3               	= 2003,
EN_KEY_4               	= 2004,
EN_KEY_5               	= 2005,
EN_KEY_6               	= 2006,
EN_KEY_7               	= 2007,
EN_KEY_8               	= 2008,
EN_KEY_9               	= 2009,
EN_KEY_MINUS        	= 2010,
EN_KEY_PLUS           	= 2011,
EN_KEY_TILDE			= 2012, // тильда и буква ё в русской раскдалке

EN_KEY_A               	= 2013,
EN_KEY_B               	= 2014,
EN_KEY_C               	= 2015,
EN_KEY_D               	= 2016,
EN_KEY_E               	= 2017,
EN_KEY_F               	= 2018,
EN_KEY_G               	= 2019,
EN_KEY_H               	= 2020,
EN_KEY_I               	= 2021,
EN_KEY_J               	= 2022,
EN_KEY_K				= 2023,
EN_KEY_L				= 2024,
EN_KEY_M				= 2025,
EN_KEY_N				= 2026,
EN_KEY_O				= 2027,
EN_KEY_P				= 2028,
EN_KEY_Q				= 2029,
EN_KEY_R				= 2030,
EN_KEY_S				= 2031,
EN_KEY_T				= 2032,
EN_KEY_U				= 2033,
EN_KEY_V				= 2034,
EN_KEY_W				= 2035,
EN_KEY_X				= 2036,
EN_KEY_Y				= 2037,
EN_KEY_Z				= 2038,
EN_KEY_LBKT				= 2039,
EN_KEY_RBKT				= 2040,
EN_KEY_SEMICOLON		= 2041,
EN_KEY_DQUOTE			= 2042,
EN_KEY_COMMA			= 2043,
EN_KEY_PERIOD			= 2044,
EN_KEY_SLASH			= 2045,

EN_KEY_ESCAPE			= 2050,
EN_KEY_SPACE			= 2051,
EN_KEY_ENTER			= 2052,
EN_KEY_CTRL				= 2053,
EN_KEY_RALT				= 2054,
EN_KEY_LALT				= 2055,
EN_KEY_SHIFT			= 2056,
EN_KEY_UP				= 2057,
EN_KEY_DOWN				= 2058,
EN_KEY_LEFT				= 2059,
EN_KEY_RIGHT			= 2060,
EN_KEY_BACK				= 2061,
EN_KEY_TAB				= 2062,

/// типы событтий для мыши
EN_MOUSEMOTION         = 300,
EN_MOUSEBUTTONDOWN     = 301,
EN_MOUSEBUTTONUP       = 302,
EN_MOUSEWINDOWOUT      = 303,
EN_MOUSEWINDOWIN       = 304,
EN_MOUSEWHEELUP        = 305,
EN_MOUSEWHEELDOWN      = 306,

/// нажатая кнопка мыши
EN_MOUSEBUTTONLEFT     = 310,
EN_MOUSEBUTTONMIDDLE   = 311,
EN_MOUSEBUTTONRIGHT    = 312
};


#endif // ENUM_EVENTTYPES_H_INCLUDED
