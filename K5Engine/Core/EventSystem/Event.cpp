#include "Event.h"


TSystemEvent::TSystemEvent(): Type(EN_NONE),WindowWidth(-1),WindowHeight(-1)
{
}

TSystemEvent::~TSystemEvent()
{
}


TKeyboardEvent::TKeyboardEvent():Type(EN_NONE),Key(EN_NONE)
{
}

TKeyboardEvent::~TKeyboardEvent()
{
}


TMouseEvent::TMouseEvent():Type(EN_NONE), Button(EN_NONE), X(0), Y(0)
{
}

TMouseEvent::~TMouseEvent()
{
}


void TMouseEvent::Set(const int   &XVal, const int   &YVal)
{
	X = XVal;
	Y = YVal;
}

void TMouseEvent::Set(const float &XVal, const float &YVal)
{
	X = (int)XVal;
	Y = (int)YVal;
}


TTimerEvent::TTimerEvent():Sec(0.0f),Ms(0.0f)
{
}

TTimerEvent::~TTimerEvent()
{
}


void TTimerEvent::Set(const float &SecVal, const float &MsVal)
{
	Sec = SecVal;
	Ms  = MsVal;
}


TEvent::TEvent():Event(EN_EVENTFALSE),Type(EN_NONE)
{
}

TEvent::TEvent(const bool &IsEvent):Event(IsEvent),Type(EN_NONE)
{
}

TEvent::TEvent(const TEvent &ObjEvent)
{
	*this = ObjEvent;
}

TEvent::~TEvent()
{
}

TEvent::operator bool() const
{
	return Event;
}

bool TEvent::operator ==(const bool &Val) const
{
    return Event == Val;
}

bool TEvent::operator != (const bool &Val) const
{
    return Event != Val;
}

void TEvent::operator = (const bool &Val)
{
	Event = Val;
}

void TEvent::operator = (const TEvent &ObjEvent)
{
	Event = ObjEvent.GetActive();
	Type  = ObjEvent.Type;

	System   = ObjEvent.System;
	Keyboard = ObjEvent.Keyboard;
	Mouse    = ObjEvent.Mouse;
	Timer    = ObjEvent.Timer;
}

bool TEvent::GetActive() const
{
	return Event;
}


