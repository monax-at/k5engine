

///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef EVENT_H_INCLUDED
#define EVENT_H_INCLUDED


#include "enum_EventTypes.h"

#include <string>

using std::wstring;


class TSystemEvent
{
	public:
		enEventTypes Type;

		int  WindowWidth;
		int  WindowHeight;
	public:
		TSystemEvent();
		~TSystemEvent();
};

class TKeyboardEvent
{
	public:
		enEventTypes Type;
		enEventTypes Key;
		wchar_t Symbol;
	public:
		TKeyboardEvent();
		~TKeyboardEvent();
};

class TMouseEvent
{
	public:
		enEventTypes Type;
		enEventTypes Button;
		int X;
		int Y;
	public:
		TMouseEvent ();
		~TMouseEvent();

		void Set(const int   &XVal, const int   &YVal);
		void Set(const float &XVal, const float &YVal);
};

class TTimerEvent
{
	public:
		float Sec;
		float Ms;
	public:
		TTimerEvent();
		~TTimerEvent();

		void Set(const float &SecVal, const float &MsVal);
};

class TEvent
{
	protected:
		bool Event;
	public:
		enEventTypes Type;

		TSystemEvent   System;
		TKeyboardEvent Keyboard;
		TMouseEvent    Mouse;
		TTimerEvent    Timer;
	public:
		TEvent();
		TEvent(const bool &IsEvent);
		TEvent(const TEvent &ObjEvent);
		~TEvent();

		operator bool() const;

		bool operator == (const bool &Val)  const;
		bool operator != (const bool &Val)  const;

		void operator = (const bool &Val);
		void operator = (const TEvent &ObjEvent);

		bool GetActive() const;
};

typedef TEvent* pTEvent;


#endif // EVENT_H_INCLUDED
