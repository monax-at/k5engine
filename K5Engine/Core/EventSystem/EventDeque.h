

///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef EVENTDEQUE_H_INCLUDED
#define EVENTDEQUE_H_INCLUDED


#include "Event.h"
#include "../ExceptionGenerator.h"

#include <deque>
using std::deque;


class TEventDeque
{
    private:
        TExceptionGenerator Exception;

        deque <TEvent> Events;
    public:
        TEventDeque();
        ~TEventDeque();

        bool  IsEmpty() const;
        bool  IsNotEmpty() const;
        int   GetSize() const;

        void   Push(const TEvent &Event);
        TEvent Pop();

        void Clear();
};

typedef TEventDeque* pTEventDeque;


#endif // EVENTDEQUE_H_INCLUDED
