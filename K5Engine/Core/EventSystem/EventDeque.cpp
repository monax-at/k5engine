#include "EventDeque.h"


TEventDeque::TEventDeque():Exception(L"TEventDeque: ")
{
}

TEventDeque::~TEventDeque()
{
	Clear();
}


bool TEventDeque::IsEmpty() const
{
	return Events.empty();
}

bool TEventDeque::IsNotEmpty() const
{
    return !Events.empty();
}

int  TEventDeque::GetSize() const
{
    return Events.size();
}

void TEventDeque::Push(const TEvent &Event)
{
    Events.push_back(Event);
}

TEvent TEventDeque::Pop()
{
	Exception(IsNotEmpty(),L"in Pop() deque is empty");

	TEvent Event = Events[0];
	Events.pop_front();

    return Event;
}

void TEventDeque::Clear()
{
    Events.clear();
}


