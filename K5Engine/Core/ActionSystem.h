///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef ACTIONSYSTEM_H_INCLUDED
#define ACTIONSYSTEM_H_INCLUDED

#include "ActionSystem/ActionListManager.h"
#include "ActionSystem/ActionPointerList.h"

#endif // ACTIONSYSTEM_H_INCLUDED
