///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef MUSICSYSTEM_H_INCLUDED
#define MUSICSYSTEM_H_INCLUDED


#include "MusicSystem/MusicDevice.h"
#include "MusicSystem/MusicTrackList.h"
#include "MusicSystem/MusicTrackListManager.h"
#include "MusicSystem/MusicTrackLoader.h"


#endif // MUSICSYSTEM_H_INCLUDED
