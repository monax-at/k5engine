#include "Value.h"


TValue::TValue():Value(0.0f)
{
}

TValue::TValue(const float &Val):Value(Val)
{
}

TValue::TValue(const TValue &Val):Value(0.0f)
{
	Value = Val.Get();
}

TValue::~TValue()
{
}


void TValue::operator()(const float &Val)
{
	Value = Val;
}

void TValue::operator()(const TValue &Val)
{
	Value = Val.Get();
}

void TValue::operator=(const float &Val)
{
	Value = Val;
}

void TValue::operator=(const TValue &Val)
{
	Value = Val.Get();
}

bool TValue::operator== (const float  &Val) const
{
	return Value == Val;
}

bool TValue::operator== (const TValue &Val) const
{
	return Value == Val.Get();
}

bool TValue::operator!= (const float  &Val) const
{
	return Value != Val;
}

bool TValue::operator!= (const TValue &Val) const
{
	return Value != Val.Get();
}

float TValue::operator+= (const float  &Val)
{
	Value += Val;
	return Value;
}

float TValue::operator+= (const TValue &Val)
{
	Value += Val.Get();
	return Value;
}

float TValue::operator-= (const float  &Val)
{
	Value -= Val;
	return Value;
}

float TValue::operator-= (const TValue &Val)
{
	Value -= Val.Get();
	return Value;
}

float TValue::operator + (const float  &Val)
{
	return Value + Val;
}

TValue TValue::operator + (const TValue &Val)
{
	return TValue(Value + Val.Get());
}

float TValue::operator - (const float  &Val)
{
	return Value - Val;
}

TValue TValue::operator - (const TValue &Val)
{
	return TValue(Value - Val.Get());
}

void  TValue::Set(const float &Val)
{
	Value = Val;
}

void  TValue::Inc(const float &Val)
{
	Value += Val;
}

void  TValue::Dec(const float &Val)
{
	Value -= Val;
}

void TValue::Set(const TValue &Val)
{
	Value = Val.Get();
}

void TValue::Inc(const TValue &Val)
{
	Value += Val.Get();
}

void TValue::Dec(const TValue &Val)
{
	Value -= Val.Get();
}

float TValue::Get() const
{
	return Value;
}


