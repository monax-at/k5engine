///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef EXCEPTIONGENERATOR_H_INCLUDED
#define EXCEPTIONGENERATOR_H_INCLUDED


#include "Exception.h"


class TExceptionGenerator
{
	private:
		void *Ptr;
	protected:
		wstring MessPrefix;
	public:
		TExceptionGenerator();
		TExceptionGenerator(const wstring &Prifix);
		TExceptionGenerator(const wstring &Prifix, void *PtrVal, const wstring &Info = L"User create obj");
		~TExceptionGenerator();

		void operator()	(TException &Ex,const bool &SetPref=true) const;
		void Run		(TException &Ex,const bool &SetPref=true) const;

		void operator()	(const TException &Ex,const bool &SetPref=true) const;
		void Run		(const TException &Ex,const bool &SetPref=true) const;

		void operator()	(const wstring &Mess,const bool &SetPref=true) const;
		void Run		(const wstring &Mess,const bool &SetPref=true) const;

		void operator() (	const bool &Check,const wstring &Mess,
							const bool &SetPref=true) const;

		void Run		(	const bool &Check,const wstring &Mess,
							const bool &SetPref=true) const;

		void SetPrefix(const wstring &Val);
		void SetInfo  (const wstring &Val);
};

typedef TExceptionGenerator* pTExceptionGenerator;


#endif // EXCEPTIONGENERATOR_H_INCLUDED
