/// Authors:
///	Maxim Kolesnikov [mpkmaximus@gmail.com]


#pragma once
#ifndef DEBUG_H_INCLUDED
#define DEBUG_H_INCLUDED


#include <map>
#include <string>

using std::map;
using std::wstring;


class TDebug
{
	private:
		map <void *, wstring>  RegisteredObj;
	public:
		TDebug();
		~TDebug();

		void RegistrationObj(void *PtrVal, const wstring &Val = L"");
		void UnregistrationObj(void *PtrVal);

		wstring GetObjInfo(void *PtrVal);
		int     GetObjCount();
		void 	SetObjInfo(void *PtrVal, const wstring &Val);
};

extern TDebug Debug;


#endif // DEBUG_H_INCLUDED
