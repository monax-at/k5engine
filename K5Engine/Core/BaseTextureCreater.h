/// Authors:
///	Maxim Kolesnikov [mpkmaximus@gmail.com]


#pragma once
#ifndef BASETEXTURECREATER_H_INCLUDED
#define BASETEXTURECREATER_H_INCLUDED


#include "GraphicSystem/Base/Texture.h"

#include <string>
using std::wstring;


class TBaseTextureCreater
{
	protected:
		TExceptionGenerator Exception;
	public:
		TBaseTextureCreater();
		virtual ~TBaseTextureCreater();

		virtual pTTexture Run(const wstring &TextName, const int &WidthVal, const int &HeightVal,
							  unsigned char *TexRawData = NULL) = 0;
};

typedef TBaseTextureCreater *pTBaseTextureCreater;


#endif // BASETEXTURECREATER_H_INCLUDED
