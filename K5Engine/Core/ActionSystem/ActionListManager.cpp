#include "ActionListManager.h"


TActionListManager::TActionListManager():TBaseActionList<TActionList>()
{
	Exception.SetPrefix(L"TActionListManager: ");
}

TActionListManager::~TActionListManager()
{
}

pTBaseAction TActionListManager::Get(const int &Index, const int &Action)
{
	return GetElem(Index)->Get(Action);
}

pTBaseAction TActionListManager::Get(const unsigned long &ObjID,const int &Action)
{
	return GetElem(ObjID)->Get(Action);
}

pTBaseAction TActionListManager::Get(const wstring &List, const int &Action)
{
	return GetElem(List)->Get(Action);
}

pTBaseAction TActionListManager::Get(const int &Index, const unsigned long &Action)
{
	return GetElem(Index)->Get(Action);
}

pTBaseAction TActionListManager::Get(const unsigned long &ObjID,const unsigned long &Action)
{
	return GetElem(ObjID)->Get(Action);
}

pTBaseAction TActionListManager::Get(const wstring &List,const unsigned long &Action)
{
	return GetElem(List)->Get(Action);
}

pTBaseAction TActionListManager::Get(const int &Index, const wstring &Action)
{
	return GetElem(Index)->Get(Action);
}

pTBaseAction TActionListManager::Get(const unsigned long &ObjID,const wstring &Action)
{
	return GetElem(ObjID)->Get(Action);
}

pTBaseAction TActionListManager::Get(const wstring &List, const wstring &Action)
{
	return GetElem(List)->Get(Action);
}

pTBaseAction TActionListManager::GetIfExist(const int &List, const int &ObjIndex)
{
	pTActionList ObjList = GetIfExist(List);
	if (ObjList == NULL){return NULL;}
	return ObjList->GetIfExist(ObjIndex);
}

pTBaseAction TActionListManager::GetIfExist(const int &List, const unsigned long &ObjID)
{
	pTActionList ObjList = GetIfExist(List);
	if (ObjList == NULL){return NULL;}
	return ObjList->GetIfExist(ObjID);
}

pTBaseAction TActionListManager::GetIfExist(const int &List, const wstring &ObjName)
{
	pTActionList ObjList = GetIfExist(List);
	if (ObjList == NULL){return NULL;}
	return ObjList->GetIfExist(ObjName);
}

pTBaseAction TActionListManager::GetIfExist(const wstring &List, const unsigned long &ObjID)
{
	pTActionList ObjList = GetIfExist(List);
	if (ObjList == NULL){return NULL;}
	return ObjList->GetIfExist(ObjID);
}

pTBaseAction TActionListManager::GetIfExist(const wstring &List, const int &ObjIndex)
{
	pTActionList ObjList = GetIfExist(List);
	if (ObjList == NULL){return NULL;}
	return ObjList->GetIfExist(ObjIndex);
}

pTBaseAction TActionListManager::GetIfExist(const wstring &List, const wstring &ObjName)
{
	pTActionList ObjList = GetIfExist(List);
	if (ObjList == NULL){return NULL;}
	return ObjList->GetIfExist(ObjName);
}

pTBaseAction TActionListManager::GetIfExist(const unsigned long &List, const int &ObjIndex)
{
	pTActionList ObjList = GetIfExist(List);
	if (ObjList == NULL){return NULL;}
	return ObjList->GetIfExist(ObjIndex);
}

pTBaseAction TActionListManager::GetIfExist(const unsigned long &List, const unsigned long &ObjID)
{
	pTActionList ObjList = GetIfExist(List);
	if (ObjList == NULL){return NULL;}
	return ObjList->GetIfExist(ObjID);
}

pTBaseAction TActionListManager::GetIfExist(const unsigned long &List, const wstring &ObjName)
{
	pTActionList ObjList = GetIfExist(List);
	if (ObjList == NULL){return NULL;}
	return ObjList->GetIfExist(ObjName);
}

pTActionList TActionListManager::Add(const wstring &ListName)
{
	pTActionList List(new TActionList);

	List->SetName(ListName);
	Elems.push_back(List);

	return List;
}

pTBaseAction TActionListManager::Add(const int &Index,const pTBaseAction &Action)
{
	return GetElem(Index)->Add(Action);
}

pTBaseAction TActionListManager::Add(const unsigned long &ObjID, const pTBaseAction &Action)
{
	return GetElem(ObjID)->Add(Action);
}

pTBaseAction TActionListManager::Add(const wstring &ListName, const pTBaseAction &Action)
{
	return GetElem(ListName)->Add(Action);
}

void TActionListManager::Del(const int &List, const int &Action)
{
	GetElem(List)->Del(Action);
}

void TActionListManager::Del(const unsigned long &List, const int &Action)
{
	GetElem(List)->Del(Action);
}

void TActionListManager::Del(const wstring &List, const int &Action)
{
	GetElem(List)->Del(Action);
}

void TActionListManager::Del(const int &List, const unsigned long &Action)
{
	GetElem(List)->Del(Action);
}

void TActionListManager::Del(const unsigned long &List, const unsigned long &Action)
{
	GetElem(List)->Del(Action);
}

void TActionListManager::Del(const wstring &List, const unsigned long &Action)
{
	GetElem(List)->Del(Action);
}

void TActionListManager::Del(const int &List, const wstring &Action)
{
	GetElem(List)->Del(Action);
}

void TActionListManager::Del(const unsigned long &List, const wstring &Action)
{
	GetElem(List)->Del(Action);
}

void TActionListManager::Del(const wstring &List, const wstring &Action)
{
	GetElem(List)->Del(Action);
}


