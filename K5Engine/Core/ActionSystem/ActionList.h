#pragma once
#ifndef ACTIONLIST_H_INCLUDED
#define ACTIONLIST_H_INCLUDED


#include "BaseAction.h"
#include "BaseActionList.h"


class TActionList:public TBaseActionList<TBaseAction>
{
	public:
		TActionList();
		virtual ~TActionList();
};

typedef TActionList* pTActionList;


#endif // ACTIONLIST_H_INCLUDED
