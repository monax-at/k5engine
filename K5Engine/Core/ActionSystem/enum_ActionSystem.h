#pragma once
#ifndef ENUM_ACTIONSYSTEM_H_INCLUDED
#define ENUM_ACTIONSYSTEM_H_INCLUDED


enum enChangeValueDirection
{
	EN_CVD_Unknown,
	EN_CVD_Set,
	EN_CVD_Inc,
	EN_CVD_Dec
};

enum enRotationState
{
	EN_RS_Unknown	= 0,
	EN_RS_Permanent	= 1,
	EN_RS_FixedTimer= 2,
	EN_RS_FixedAngle= 3
};

enum enActionControlState
{
	EN_ACS_Unknown,
	EN_ACS_Run,
	EN_ACS_Start,
	EN_ACS_Stop,
	EN_ACS_Resume,
	EN_ACS_Suspend
};


#endif // ENUM_ACTIONSYSTEM_H_INCLUDED
