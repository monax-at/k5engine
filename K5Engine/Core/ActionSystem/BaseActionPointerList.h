

///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef BASEACTIONPOINTERLIST_H_INCLUDED
#define BASEACTIONPOINTERLIST_H_INCLUDED


#include "../ExceptionGenerator.h"

#include <string>
#include <vector>
#include <stdexcept>

using std::wstring;
using std::vector;


template <class TObject>
class TBaseActionPointerList
{
	protected:
		static unsigned long IdCounter;
		unsigned long		Id;

		wstring Name;
		bool    Active;

		TObject *ActiveObject;

		TExceptionGenerator Exception;
		vector<TObject*>    Elems;
	protected:
		// ---------------------------------//
		inline TObject* GetElem(const int &Index)
		{
			try  { return Elems[Index]; }
			catch(std::out_of_range &oor){ Exception(L"in GetElem(Index) Index out of range"); }
		}
		// ---------------------------------//
		inline TObject* GetElem(const unsigned long &ObjID)
		{
			int Size(Elems.size());
			Exception(Size>0, L"in GetElem(ObjID) list is empty");

			for(int i=0;i<Size;i++){ if(Elems[i]->GetID() == ObjID){ return Elems[i]; } }

			Exception(L"in GetElem(ObjID) action not exist");
			return NULL;
		}
		// ---------------------------------//
		inline TObject* GetElem(const wstring &ObjName)
		{
			int Size(Elems.size());
			Exception(Size>0, L"in GetElem(ObjName) list is empty");

			for(int i=0;i<Size;i++){ if(Elems[i]->GetName() == ObjName){ return Elems[i]; } }

			Exception(L"in GetElem(ObjName) action whis name '" + ObjName + L"' not exist");
			return NULL;
		}
		// ---------------------------------//
	public:
		TBaseActionPointerList():Active(true),ActiveObject(NULL)
		{
			IdCounter++;
			Id = IdCounter;
		}
		// ---------------------------------//
		~TBaseActionPointerList()
		{
			Clear();
		}
		// ---------------------------------//
		unsigned long GetID() const
		{
			return Id;
		}
		// ---------------------------------//
		void SetName(const wstring &Val)
		{
			Name = Val;
		}
		// ---------------------------------//
		wstring GetName() const
		{
			return Name;
		}
		// ---------------------------------//
		void SetActive(const bool &Val)
		{
			Active = Val;
		}
		// ---------------------------------//
		bool GetActive() const
		{
			return Active;
		}
		// ---------------------------------//
		TObject* operator[](const int &Index)
		{
		    return GetElem(Index);
		}
		// ---------------------------------//
		TObject* operator[](const unsigned long &ObjID)
		{
		    return GetElem(ObjID);
		}
		// ---------------------------------//
		TObject* operator[](const wstring &ObjName)
		{
		    return GetElem(ObjName);
		}
		// ---------------------------------//
		TObject* Get(const int &Index)
		{
			return GetElem(Index);
		}
		// ---------------------------------//
		TObject* Get(const unsigned long &ObjID)
		{
			return GetElem(ObjID);
		}
		// ---------------------------------//
		TObject* Get(const wstring &ObjName)
		{
			return GetElem(ObjName);
		}
		// ---------------------------------//
		TObject* GetFirst()
		{
		    return GetElem(0);
		}
		// ---------------------------------//
		TObject* GetLast()
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in GetLast() list is enpty");
			return Elems[Size-1];
		}
		// ---------------------------------//
		bool IfExist(const unsigned long &ObjId)
		{
		    int Size(Elems.size());
		    for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return true; }
		    }
		    return false;
		}
		// ---------------------------------//
		bool IfExist(const wstring &ObjName)
		{
		    int Size(Elems.size());
		    for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return true; }
		    }
		    return false;
		}
		// ---------------------------------//
		TObject* GetIfExist(const unsigned long &ObjId)
		{
		    int Size(Elems.size());
		    for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return Elems[i]; }
		    }
		    return NULL;
		}
		// ---------------------------------//
		TObject* GetIfExist(const wstring &ObjName)
		{
		    int Size(Elems.size());
		    for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return Elems[i]; }
		    }
		    return NULL;
		}
		// ---------------------------------//
		int GetIndex(const unsigned long &ObjId) const
		{
			int Size(Elems.size());
			Exception(Size>0, L"in GetIndex(ObjId) list is empty");

			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){ return i; }
			}

			Exception(L"in GetIndex(ObjId) object not exist");
			return 0;
		}
		// ---------------------------------//
		int GetIndex(const wstring &ObjName) const
		{
			int Size(Elems.size());
			Exception(Size>0, L"in GetIndex(ObjName) list is empty");

			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){ return i; }
			}

			Exception(L"in GetIndex(ObjName) object whis name '" + ObjName + L"' not exist");
			return 0;
		}
		// ---------------------------------//
		int GetSize() const
		{
		    return Elems.size();
		}
		// ---------------------------------//
		TObject* Add(TObject *Object)
		{
		    Elems.push_back(Object);

		    return Object;
		}
		// ---------------------------------//
		void Del(const int &Index)
		{
			int Size(Elems.size());
		    Exception(Index >=0 && Index < Size, L"in Del(Index) list is empty");

		    Elems.erase(Elems.begin() + Index);
		}
		// ---------------------------------//
		void Del(const unsigned long &ObjID)
		{
		    int Size(Elems.size());
		    Exception(Size>0, L"in Del(ObjID) list is empty");

			int Index(-1);
		    for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjID){
					Index = i;
					break;
				}
		    }

			Exception(Index != -1,L"in Del(ObjID) action not exist");

			Elems.erase(Elems.begin() + Index);
		}
		// ---------------------------------//
		void Del(const wstring &ObjName)
		{
		    int Size(Elems.size());
		    Exception(Size>0, L"in Del(ObjName) list is empty");

			int Index(-1);
		    for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){
					Index = i;
					break;
				}
		    }

			Exception(Index != -1,L"in Del(ObjName) action whis name '" + ObjName + L"' not exist");

			Elems.erase(Elems.begin() + Index);
		}
		// ---------------------------------//
		void Clear()
		{
			Elems.clear();
		}
		// ---------------------------------//
		void Start()
		{
			int Size(Elems.size());
			for (int i=0;i<Size;i++){ Elems[i]->Start(); }
		}
		// ---------------------------------//
		void Stop()
		{
			int Size(Elems.size());
			for (int i=0;i<Size;i++){ Elems[i]->Stop(); }
		}
		// ---------------------------------//
		void StartOne(const int &Index)
		{
			GetElem(Index)->Start();
		}
		// ---------------------------------//
		void StartOne(const unsigned long &ObjID)
		{
			GetElem(ObjID)->Start();
		}
		// ---------------------------------//
		void StartOne(const wstring &ObjName)
		{
			GetElem(ObjName)->Start();
		}
		// ---------------------------------//
		void StopOne(const int &Index)
		{
			GetElem(Index)->Stop();
		}
		// ---------------------------------//
		void StopOne(const unsigned long &ObjID)
		{
			GetElem(ObjID)->Stop();
		}
		// ---------------------------------//
		void StopOne(const wstring &ObjName)
		{
			GetElem(ObjName)->Stop();
		}
		// ---------------------------------//
		bool IsOneActive(const int &Index)
		{

			return GetElem(Index)->GetActive();
		}
		// ---------------------------------//
		bool IsOneActive(const unsigned long &ObjID)
		{
			return GetElem(ObjID)->GetActive();
		}
		// ---------------------------------//
		bool IsOneActive(const wstring &ObjName)
		{
			return GetElem(ObjName)->GetActive();
		}
		// ---------------------------------//
		void SetActiveObject(TObject *Val)
		{
			ActiveObject = Val;
		}
		// ---------------------------------//
		void SetActiveObject(const int &Index)
		{
			ActiveObject = GetElem(Index);
		}
		// ---------------------------------//
		void SetActiveObject(const unsigned long &ObjID)
		{
			ActiveObject = GetElem(ObjID);
		}
		// ---------------------------------//
		void SetActiveObject(const wstring &ObjName)
		{
			ActiveObject = GetElem(ObjName);
		}
		// ---------------------------------//
		void UnSetActiveObject()
		{
			ActiveObject = NULL;
		}
		// ---------------------------------//
		bool IsSetActiveObject() const
		{
			return (ActiveObject != NULL);
		}
		// ---------------------------------//
		TObject* GetActiveObject()
		{
			return ActiveObject;
		}
        // ---------------------------------//
        void Update()
        {
			int Size(Elems.size());
			for (int i=0;i<Size;i++){ Elems[i]->Update(); }
        }
		// ---------------------------------//
		void Run(const TEvent &Event = TEvent())
		{
			if(!Active){return;}

			int Size(Elems.size());
			for (int i=0;i<Size;i++){ Elems[i]->Run(Event); }
		}
		// ---------------------------------//
		void RunOne(const int &Index, const TEvent &Event = TEvent())
		{
			if(!Active){return;}
			GetElem(Index)->Run(Event);
		}
		// ---------------------------------//
		void RunOne(const unsigned long &ObjID,const TEvent &Event = TEvent())
		{
			if(!Active){return;}
			GetElem(ObjID)->Run(Event);
		}
		// ---------------------------------//
		void RunOne(const wstring &ObjName, const TEvent &Event = TEvent())
		{
			if(!Active){return;}
			GetElem(ObjName)->Run(Event);
		}
		// ---------------------------------//
		void StartIsNotActive()
		{
			if(!Active){return;}

			int Size(Elems.size());
			for(int i=0;i<Size;i++){ Elems[i]->StartIsNotActive(); }
		}
		// ---------------------------------//
		void StopIsActive()
		{
			if(!Active){return;}

			int Size(Elems.size());
			for(int i=0;i<Size;i++){ Elems[i]->StopIsActive(); }
		}
		// ---------------------------------//
		TObject* Extract(const int &Index)
		{
			int Size(Elems.size());
			Exception(Size > 0                  , L"in Extract(Index) list is enpty");
			Exception(Index >= 0 && Index < Size, L"in Extract(Index) Id out of range");

			TObject *Elem = Elems[Index];

			Elems.erase(Elems.begin() + Index);
			return Elem;
		}
		// ---------------------------------//
		TObject* Extract(const unsigned long &ObjId)
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in Extract(ObjId) list is enpty");

			int ExtractId = -1;
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetID() == ObjId){
					ExtractId = i;
					break;
				}
			}
			Exception(ExtractId!=-1,L"in Extract(ObjId) object not exist");

			TObject *Elem = Elems[ExtractId];
			Elems.erase(Elems.begin() + ExtractId);

			return Elem;
		}
		// ---------------------------------//
		TObject* Extract(const wstring &ObjName)
		{
			int Size(Elems.size());
			Exception(Size > 0,L"in Extract(ObjName) list is enpty");

			int ExtractId = -1;
			for(int i=0;i<Size;i++){
				if(Elems[i]->GetName() == ObjName){
					ExtractId = i;
					break;
				}
			}

			Exception(ExtractId!=-1,
					  L"in Extract(ObjName) object whis name: " + ObjName + L" not exist");

			TObject *Elem = Elems[ExtractId];
			Elems.erase(Elems.begin()+ExtractId);

			return Elem;
		}
		// ---------------------------------//
		void Merge(TBaseActionPointerList &List)
		{
			while(List.GetSize()>0){ Add(List.Extract(0)); }
		}
		// ---------------------------------//
		void Merge(TBaseActionPointerList *List)
		{
			if(List == NULL){return;}
			Merge(*List);
		}
		// ---------------------------------//
};

template <class TObject>
unsigned long  TBaseActionPointerList<TObject>::IdCounter = 0;


#endif // BASEACTIONPOINTERLIST_H_INCLUDED
