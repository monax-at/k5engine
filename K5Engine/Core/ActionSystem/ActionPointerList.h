#ifndef ACTIONPOINTERLIST_H_INCLUDED
#define ACTIONPOINTERLIST_H_INCLUDED


#include "BaseAction.h"
#include "BaseActionPointerList.h"


class TActionPointerList:public TBaseActionPointerList<TBaseAction>
{
	public:
		TActionPointerList();
		virtual ~TActionPointerList();
};

typedef TActionPointerList* pTActionPointerList;


#endif // ACTIONPOINTERLIST_H_INCLUDED
