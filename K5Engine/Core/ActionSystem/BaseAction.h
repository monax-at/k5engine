

///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef BASEACTION_H_INCLUDED
#define BASEACTION_H_INCLUDED


#include "../EventSystem/Event.h"

#include <string>
using std::wstring;


class TBaseAction
{
	protected:
		static unsigned long IdCounter;
		unsigned long Id;

		bool    Active;
		wstring Name;
	protected:
		virtual void ToSetActive();
		virtual void ToStart    ();
		virtual void ToStop     ();
		virtual void ToRun(const TEvent &Event) = 0;
	public:
		TBaseAction();
		TBaseAction(const bool &ActiveVal);
		virtual ~TBaseAction();

		virtual TBaseAction* Clone();
		virtual void Update();

		unsigned long GetID() const;

		void    SetName(const wstring &Val);
		wstring GetName() const;

		void SetActive(const bool &Val);
		bool GetActive() const;

		void Start();
		void StartIsNotActive();

		void Stop();
		void StopIsActive();

		void Run(const TEvent &Event = TEvent());
};

typedef TBaseAction* pTBaseAction;


#endif // BASEACTION_H_INCLUDED
