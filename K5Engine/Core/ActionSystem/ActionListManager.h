#pragma once
#ifndef ACTIONLISTMANAGER_H_INCLUDED
#define ACTIONLISTMANAGER_H_INCLUDED


#include "ActionList.h"


class TActionListManager:public TBaseActionList<TActionList>
{
	public:
		TActionListManager();
		virtual ~TActionListManager();

		using TBaseActionList<TActionList>::Get;

		pTBaseAction Get(const int &Id				,const int &Action);
		pTBaseAction Get(const unsigned long &ObjID ,const int &Action);
		pTBaseAction Get(const wstring &List		,const int &Action);

		pTBaseAction Get(const int &Id				,const unsigned long &Action);
		pTBaseAction Get(const unsigned long &ObjID ,const unsigned long &Action);
		pTBaseAction Get(const wstring &List		,const unsigned long &Action);

		pTBaseAction Get(const int &Id				,const wstring &Action);
		pTBaseAction Get(const unsigned long &ObjID,const wstring &Action);
		pTBaseAction Get(const wstring &List		,const wstring &Action);

		using TBaseActionList<TActionList>::GetIfExist;

		pTBaseAction GetIfExist(const int     &List, const int           &ObjIndex);
		pTBaseAction GetIfExist(const int     &List, const unsigned long &ObjID);
		pTBaseAction GetIfExist(const int     &List, const wstring       &ObjName);

		pTBaseAction GetIfExist(const wstring &List, const int           &ObjIndex);
		pTBaseAction GetIfExist(const wstring &List, const unsigned long &ObjID);
		pTBaseAction GetIfExist(const wstring &List, const wstring       &ObjName);

		pTBaseAction GetIfExist(const unsigned long &List, const int           &ObjIndex);
		pTBaseAction GetIfExist(const unsigned long &List, const unsigned long &ObjID);
		pTBaseAction GetIfExist(const unsigned long &List, const wstring       &ObjName);

		using TBaseActionList<TActionList>::Add;

		pTActionList Add(const wstring &ListName);
		pTBaseAction Add(const int &Id				,const pTBaseAction &Action);
		pTBaseAction Add(const unsigned long &ObjID	,const pTBaseAction &Action);
		pTBaseAction Add(const wstring &ListName	,const pTBaseAction &Action);

		using  TBaseActionList<TActionList>::Del;

		void Del(const int &List,			const int &Action);
		void Del(const unsigned long &List,	const int &Action);
		void Del(const wstring &List,		const int &Action);

		void Del(const int &List,			const unsigned long &Action);
		void Del(const unsigned long &List,	const unsigned long &Action);
		void Del(const wstring &List,		const unsigned long &Action);

		void Del(const int &List,			const wstring &Action);
		void Del(const unsigned long &List,	const wstring &Action);
		void Del(const wstring &List,		const wstring &Action);
};

typedef TActionListManager* pTActionListManager;


#endif // ACTIONLISTMANAGER_H_INCLUDED
