#include "BaseAction.h"


unsigned long TBaseAction::IdCounter = 0;


TBaseAction::TBaseAction():Active(false)
{
	IdCounter++;
	Id = IdCounter;
}

TBaseAction::TBaseAction(const bool &ActiveVal):Active(ActiveVal)
{
	IdCounter++;
	Id = IdCounter;
}

TBaseAction::~TBaseAction()
{
}


void TBaseAction::ToSetActive()
{
}

void TBaseAction::ToStart()
{
}

void TBaseAction::ToStop()
{
}

TBaseAction* TBaseAction::Clone()
{
	return NULL;
}

void TBaseAction::Update()
{
}

unsigned long TBaseAction::GetID() const
{
	return Id;
}

void TBaseAction::SetName(const wstring &Val)
{
	Name = Val;
}

wstring TBaseAction::GetName() const
{
	return Name;
}

void TBaseAction::SetActive(const bool &Val)
{
	Active = Val;
	ToSetActive();
}

bool TBaseAction::GetActive() const
{
	return Active;
}

void TBaseAction::Start()
{
	Active = true;
	ToStart();
}

void TBaseAction::StartIsNotActive()
{
	if(!Active){ Start(); }
}

void TBaseAction::Stop()
{
	Active = false;
	ToStop();
}

void TBaseAction::StopIsActive()
{
	if(Active){ Stop(); }
}

void TBaseAction::Run(const TEvent &Event)
{
	if(!Active){return;}

	ToRun(Event);
}


