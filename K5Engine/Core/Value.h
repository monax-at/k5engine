///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef VALUE_H_INCLUDED
#define VALUE_H_INCLUDED


class TValue
{
	protected:
		float Value;
	public:
		TValue();
		TValue(const float &Val);
		TValue(const TValue &Val);
		~TValue();

		void operator()(const float  &Val);
		void operator()(const TValue &Val);

		void operator= (const float  &Val);
		void operator= (const TValue &Val);

		bool operator== (const float  &Val) const;
		bool operator== (const TValue &Val) const;
		bool operator!= (const float  &Val) const;
		bool operator!= (const TValue &Val) const;

		float operator+= (const float  &Val);
		float operator+= (const TValue &Val);

		float operator-= (const float  &Val);
		float operator-= (const TValue &Val);

		float  operator + (const float  &Val);
		TValue operator + (const TValue &Val);
		float  operator - (const float  &Val);
		TValue operator - (const TValue &Val);

		void  Set(const float &Val);
		void  Inc(const float &Val = 1.0f);
		void  Dec(const float &Val = 1.0f);

		void  Set(const TValue &Val);
		void  Inc(const TValue &Val);
		void  Dec(const TValue &Val);

		float Get() const;
};

typedef TValue* pTValue;


#endif // GRAPHICELEMENTSVALUE_H_INCLUDED
