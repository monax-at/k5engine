#include "BaseDevice.h"


TBaseDevice::TBaseDevice():Exception(L"TBaseDevice: "),WindowStyle(EN_DWS_Default),
	Width(800),Height(600),FullScreen(false),DrawCursor(true),EnableVSync(false),
	ColorBits(32),EventDeque(NULL),ClearColor(0.0f,0.0f,0.0f,0.0f),
	MinTextureFilter(EN_TF_Linear),MagTextureFilter(EN_TF_Linear),ActiveFlag(true),
	TimerEventDelay(0.0f),TimerEventDelayCounter(0.0f)
{
}

TBaseDevice::~TBaseDevice()
{
}


void TBaseDevice::ToSetWindowStyle()
{
}

void TBaseDevice::ToSetCaption()
{
}

void TBaseDevice::ToSetEventDeque()
{
}

void TBaseDevice::ToSetClearColor()
{
}

void TBaseDevice::ToSetTextureFilter()
{
}

int TBaseDevice::GetWidth() const
{
	return Width;
}

int TBaseDevice::GetHeight() const
{
	return Height;
}

void TBaseDevice::SetWindowStyle(const enDeviceWindowStyle &Val)
{
	WindowStyle = Val;
	ToSetWindowStyle();
}

enDeviceWindowStyle TBaseDevice::GetWindowStyle()
{
	return WindowStyle;
}

void TBaseDevice::SetCaption(const wstring &Val)
{
	Caption = Val;
	ToSetCaption();
}

void TBaseDevice::SetEventDeque(const pTEventDeque &Val)
{
	EventDeque = Val;
	ToSetEventDeque();
}

void TBaseDevice::SetClearColor(const TColor &Val)
{
	ClearColor = Val;
	ToSetClearColor();
}

void TBaseDevice::SetClearColor(	const float &Red,const float &Green,
									const float &Blue,const float &Alpha)
{
	ClearColor.Set(Red,Green,Blue,Alpha);
}

TColor TBaseDevice::GetCleerColor() const
{
	return ClearColor;
}

void TBaseDevice::SetColorBits(const int &Val)
{
	ColorBits = Val;
}

int TBaseDevice::GetColorBits()const
{
	return ColorBits;
}

void TBaseDevice::SetEnableVSync(const bool &Val)
{
	EnableVSync = Val;
}

bool TBaseDevice::GetEnableVSync() const
{
	return EnableVSync;
}

void TBaseDevice::SetMinTextureFilter(const enTextureFilter &Val)
{
	MinTextureFilter = Val;
	ToSetTextureFilter();
}

void TBaseDevice::SetMagTextureFilter(const enTextureFilter &Val)
{
	MagTextureFilter = Val;
	ToSetTextureFilter();
}

void TBaseDevice::SetTextureFilter(const enTextureFilter &Val)
{
	MinTextureFilter = Val;
	MagTextureFilter = Val;
	ToSetTextureFilter();
}

void TBaseDevice::SetTextureFilter(	const enTextureFilter &Min,
									const enTextureFilter &Mag)
{
	MinTextureFilter = Min;
	MagTextureFilter = Mag;
	ToSetTextureFilter();
}

enTextureFilter TBaseDevice::GetMinTextureFilter()
{
	return MinTextureFilter;
}

enTextureFilter TBaseDevice::GetMagTextureFilter()
{
	return MagTextureFilter;
}

void TBaseDevice::SetTimerEventDelay(const float &Val)
{
	TimerEventDelay = Val;
}

float TBaseDevice::GetTimerEventDelay() const
{
	return TimerEventDelay;
}

void TBaseDevice::ResetTimerEventDelayCounter()
{
	TimerEventDelayCounter = 0.0f;
}

void TBaseDevice::ResetDevice()
{
}

bool TBaseDevice::IsActive() const
{
	return ActiveFlag;
}

pTBaseTextureLoader TBaseDevice::CreateTextureLoader()
{
	return NULL;
}

pTBaseTextureCreater TBaseDevice::CreateTextureCreater()
{
	return NULL;
}

pTBaseScreenPixelReader TBaseDevice::CreateScreenPixelReader()
{
	return NULL;
}

pTBaseViewMatrixWorker TBaseDevice::CreateViewMatrixWorker()
{
	return NULL;
}

pTBaseVertexData TBaseDevice::CreateSpriteVertexData()
{
	return NULL;
}


