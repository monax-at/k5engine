#include "PointArray.h"


TPointArray::TPointArray():Exception(L"TPointArray: "), Changed(false)
{
}

TPointArray::TPointArray(const int &Size):Exception(L"TPointArray: "), Changed(false)
{
	if(Size <= 0){ return; }
	Points.assign(Size, TPoint());
}

TPointArray::TPointArray(const TPointArray &Val)
{
	*this = Val;
}

TPointArray::~TPointArray()
{
}


void TPointArray::operator = (const TPointArray &Val)
{
	int Size(Val.GetSize());
	SetSize(Size);

	for(int i=0;i<Size;i++){ Points[i] = Val.Get(i); }

	SetChanged();
}

void TPointArray::operator = (const TPoint &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i] = Val; }
	SetChanged();
}

void TPointArray::SetSize(const int &Val)
{
	Points.clear();
	if(Val <= 0){return;}
	Points.assign(Val, TPoint());
}

void TPointArray::Set(const float &x, const float &y)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].Set(x,y); }
	SetChanged();
}

void TPointArray::Set(const float &x, const float &y, const float &z)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].Set(x,y); }
	SetChanged();
}

void TPointArray::Set(const TPoint &Point)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i] = Point; }
	SetChanged();
}

void TPointArray::Set(const int &Index, const float &x, const float &y)
{
	try {
		Points[Index].Set(x,y);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Set(Index,x,y) Index out of range"); }
}

void TPointArray::Set(const int &Index, const float &x, const float &y, const float &z)
{
	try {
		Points[Index].Set(x,y,z);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Set(Index,x,y,z) Index out of range"); }
}

void TPointArray::Set(const int &Index, const TPoint &Point)
{
	try {
		Points[Index] = Point;
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Set(Index,Point) Index out of range"); }
}

void TPointArray::SetX(const float &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].SetX(Val); }
	SetChanged();
}

void TPointArray::SetY(const float &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].SetY(Val); }
	SetChanged();
}

void TPointArray::SetZ(const float &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].SetZ(Val); }
	SetChanged();
}

void TPointArray::SetX(const int &Index, const float &Val)
{
	try {
		Points[Index].SetX(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in SetX(Index,Val) Index out of range"); }
}

void TPointArray::SetY(const int &Index, const float &Val)
{
	try {
		Points[Index].SetY(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in SetY(Index,Val) Index out of range"); }
}

void TPointArray::SetZ(const int &Index, const float &Val)
{
	try {
		Points[Index].SetZ(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in SetZ(Index,Val) Index out of range"); }
}

void TPointArray::Inc(const int &Index, const float &x, const float &y)
{
	try {
		Points[Index].Inc(x,y);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Inc(Index,x,y) Index out of range"); }
}

void TPointArray::Inc(const int &Index, const float &x, const float &y, const float &z)
{
	try {
		Points[Index].Inc(x,y,z);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Inc(Index,x,y,z) Index out of range"); }
}

void TPointArray::Inc(const int &Index, const TPoint &Point)
{
	try {
		Points[Index].Inc(Point);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Inc(Index,Point) Index out of range"); }
}

void TPointArray::Dec(const int &Index, const float &x, const float &y)
{
	try {
		Points[Index].Dec(x,y);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Inc(Index,x,y) Index out of range"); }
}

void TPointArray::Dec(const int &Index, const float &x, const float &y, const float &z)
{
	try {
		Points[Index].Dec(x,y,z);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Inc(Index,x,y,z) Index out of range"); }
}

void TPointArray::Dec(const int &Index, const TPoint &Point)
{
	try {
		Points[Index].Dec(Point);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in Inc(Index,Point) Index out of range"); }
}

void TPointArray::IncX(const float &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].IncX(Val); }
	SetChanged();
}

void TPointArray::IncY(const float &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].IncY(Val); }
	SetChanged();
}

void TPointArray::IncZ(const float &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].IncZ(Val); }
	SetChanged();
}

void TPointArray::IncX(const int &Index, const float &Val)
{
	try {
		Points[Index].IncX(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in IncX(Index,Val) Index out of range"); }
}

void TPointArray::IncY(const int &Index, const float &Val)
{
	try {
		Points[Index].IncY(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in IncY(Index,Val) Index out of range"); }
}

void TPointArray::IncZ(const int &Index, const float &Val)
{
	try {
		Points[Index].IncZ(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in IncZ(Index,Val) Index out of range"); }
}

void TPointArray::DecX(const float &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].DecX(Val); }
	SetChanged();
}

void TPointArray::DecY(const float &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].DecY(Val); }
	SetChanged();
}

void TPointArray::DecZ(const float &Val)
{
	int Size(Points.size());
	for(int i=0;i<Size;i++){ Points[i].DecZ(Val); }
	SetChanged();
}

void TPointArray::DecX(const int &Index, const float &Val)
{
	try {
		Points[Index].DecX(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in DecX(Index,Val) Index out of range"); }
}

void TPointArray::DecY(const int &Index, const float &Val)
{
	try {
		Points[Index].DecY(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in DecY(Index,Val) Index out of range"); }
}

void TPointArray::DecZ(const int &Index, const float &Val)
{
	try {
		Points[Index].DecZ(Val);
		SetChanged();
	}
	catch(out_of_range &Ex) {Exception(L"in DecZ(Index,Val) Index out of range"); }
}

TPoint TPointArray::Get(const int &Index) const
{
	try { return Points[Index]; }
	catch(out_of_range &Ex) {Exception(L"in Get(Index) Index out of range"); }
	return TPoint();
}

float TPointArray::GetX(const int &Index) const
{
	try { return Points[Index].GetX(); }
	catch(out_of_range &Ex) {Exception(L"in GetX(Index) Index out of range"); }
	return 0.0f;
}

float TPointArray::GetY(const int &Index) const
{
	try { return Points[Index].GetY(); }
	catch(out_of_range &Ex) {Exception(L"in GetY(Index) Index out of range"); }
	return 0.0f;
}

float TPointArray::GetZ(const int &Index) const
{
	try { return Points[Index].GetZ(); }
	catch(out_of_range &Ex) {Exception(L"in GetZ(Index) Index out of range"); }
	return 0.0f;
}

int TPointArray::GetSize() const
{
	return Points.size();
}

void TPointArray::SetChanged(const bool &Val)
{
	Changed = Val;
}

bool TPointArray::GetChanged() const
{
	return Changed;
}


