#include "BaseScreenPixelReader.h"


TBaseScreenPixelReader::TBaseScreenPixelReader()
{
}

TBaseScreenPixelReader::~TBaseScreenPixelReader()
{
}


void TBaseScreenPixelReader::ToSetScreenSizes()
{
}

TColor TBaseScreenPixelReader::operator()(const float &X,const float &Y)
{
	return ToRun(X,Y);
}

TColor TBaseScreenPixelReader::operator()(const TPoint &Point)
{
	return ToRun(Point.GetX(),Point.GetY());
}

TColor TBaseScreenPixelReader::operator()(const TEvent &Event)
{
	return ToRun((float)Event.Mouse.X,(float)Event.Mouse.Y);
}

TColor TBaseScreenPixelReader::operator()(const TMouseEvent &Event)
{
	return ToRun((float)Event.X,(float)Event.Y);
}

TColor TBaseScreenPixelReader::Run(const float &X,const float &Y)
{
	return ToRun(X,Y);
}

TColor TBaseScreenPixelReader::Run(const TPoint &Point)
{
	return ToRun(Point.GetX(),Point.GetY());
}

TColor TBaseScreenPixelReader::Run(const TEvent &Event)
{
	return ToRun((float)Event.Mouse.X,(float)Event.Mouse.Y);
}

TColor TBaseScreenPixelReader::Run(const TMouseEvent &Event)
{
	return ToRun((float)Event.X,(float)Event.Y);
}


