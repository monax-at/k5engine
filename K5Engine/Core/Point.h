///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED


class TPoint
{
	protected:
		float fx;
		float fy;
		float fz;
	public:
		TPoint();
		TPoint(const float &Value);
		TPoint(const float &x,const float &y);
		TPoint(const float &x,const float &y,const float &z);
		~TPoint();

		bool operator ==(const TPoint &Val) const;
		bool operator !=(const TPoint &Val) const;

		void operator  =(const TPoint &Val);

		TPoint operator + (const TPoint &Val);
		TPoint operator - (const TPoint &Val);

		float& operator[](const int &i);

		void operator()(const int   &Val);
		void operator()(const float &Val);
		void operator()(const int &x, const int &y);
		void operator()(const int &x, const int &y, const int &z);
		void operator()(const float &x, const float &y);
		void operator()(const float &x, const float &y, const float &z);
		void operator()(const TPoint &Point);

		float& Elem(const int &i);

		void  Set (const float &x,const float &y);
		void  Set (const float &x,const float &y,const float &z);
		void  Set (const TPoint &Point);

		void  SetX(const float &Value);
		void  SetY(const float &Value);
		void  SetZ(const float &Value);
		float GetX() const;
		float GetY() const;
		float GetZ() const;
		float Get (const int &Index) const;

		void IncX(const float &Value);
		void IncY(const float &Value);
		void IncZ(const float &Value);
		void Inc (const float &x,const float &y);
		void Inc (const float &x,const float &y,const float &z);
		void Inc (const TPoint &Point);

		void DecX(const float &Value);
		void DecY(const float &Value);
		void DecZ(const float &Value);
		void Dec (const float &x,const float &y);
		void Dec (const float &x,const float &y,const float &z);
		void Dec (const TPoint &Point);

		void Mult(const float &Val);
		void Mult(const float &x,const float &y);
		void Mult(const float &x,const float &y,const float &z);
		void Mult(const TPoint &Point);

		void Div (const float &Val);
		void Div (const float &x,const float &y);
		void Div (const float &x,const float &y,const float &z);
		void Div (const TPoint &Point);
};

typedef TPoint* pTPoint;


#endif // POINT_H_INCLUDED
