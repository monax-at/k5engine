#include "Point.h"
#include "../LibMath/LibMath.h"


TPoint::TPoint() :fx(0),fy(0),fz(0)
{
}

TPoint::TPoint(const float &Value) :fx(Value),fy(Value),fz(Value)
{
}

TPoint::TPoint(const float &x,const float &y) :fx(x),fy(y),fz(0)
{
}

TPoint::TPoint(const float &x,const float &y,const float &z) :fx(x),fy(y),fz(z)
{
}

TPoint::~TPoint()
{
}


bool TPoint::operator ==(const TPoint &Val) const
{
	if( Abs(fx - Val.GetX() ) > Eps6 ){ return false; }
	if( Abs(fy - Val.GetY() ) > Eps6 ){ return false; }
	if( Abs(fz - Val.GetZ() ) > Eps6 ){ return false; }

	return true;
}

bool TPoint::operator !=(const TPoint &Val) const
{
	if( Abs(fx - Val.GetX() ) > Eps6 ){ return true; }
	if( Abs(fy - Val.GetY() ) > Eps6 ){ return true; }
	if( Abs(fz - Val.GetZ() ) > Eps6 ){ return true; }

	return false;
}

void TPoint::operator  =(const TPoint &Val)
{
	fx = Val.GetX();
	fy = Val.GetY();
	fz = Val.GetZ();
}

TPoint TPoint::operator + (const TPoint &Val)
{
	TPoint Point(*this);
	Point.Inc(Val);
	return Point;
}

TPoint TPoint::operator - (const TPoint &Val)
{
	TPoint Point(*this);
	Point.Dec(Val);
	return Point;
}

float& TPoint::operator[](const int &i)
{
	float *Val = &fx;
	if(i==1){ Val = &fy;}
	if(i==2){ Val = &fz;}

    return *Val;
}

void TPoint::operator()(const int &Val)
{
    fx=(float)Val;
    fy=(float)Val;
    fz=(float)Val;
}

void TPoint::operator()(const float &Val)
{
    fx=Val;
    fy=Val;
    fz=Val;
}

void TPoint::operator()(const int &x, const int &y)
{
    fx=(float)x;
    fy=(float)y;
}

void TPoint::operator()(const int &x, const int &y, const int &z)
{
    fx=(float)x;
    fy=(float)y;
    fz=(float)z;
}

void TPoint::operator()(const float &x, const float &y)
{
    fx=x;
    fy=y;
}

void TPoint::operator()(const float &x, const float &y, const float &z)
{
    fx=x;
    fy=y;
    fz=z;
}

void TPoint::operator()(const TPoint &Point)
{
	*this = Point;
}

float& TPoint::Elem(const int &i)
{
	float *Val = &fx;
	if(i==1){ Val = &fy;}
	if(i==2){ Val = &fz;}

    return *Val;
}

void TPoint::Set(const float &x,const float &y)
{
    fx=x;
    fy=y;
}

void TPoint::Set(const float &x,const float &y,const float &z)
{
    fx=x;
    fy=y;
    fz=z;
}

void TPoint::Set(const TPoint &Point)
{
	*this = Point;
}

void  TPoint::SetX(const float &Value){fx=Value;}
void  TPoint::SetY(const float &Value){fy=Value;}
void  TPoint::SetZ(const float &Value){fz=Value;}

float TPoint::GetX() const {return(fx);}
float TPoint::GetY() const {return(fy);}
float TPoint::GetZ() const {return(fz);}

float TPoint::Get(const int &Index) const
{
	float  Result = 0;
	switch(Index){
		case 0:{Result = fx;}break;
		case 1:{Result = fy;}break;
		case 2:{Result = fz;}break;
	}
	return Result;
}

void TPoint::IncX(const float &Value){fx += Value;}
void TPoint::IncY(const float &Value){fy += Value;}
void TPoint::IncZ(const float &Value){fz += Value;}

void TPoint::Inc(const float &x,const float &y)
{
	fx += x;
	fy += y;
}

void TPoint::Inc(const float &x,const float &y,const float &z)
{
	fx += x;
	fy += y;
	fz += z;
}

void TPoint::Inc(const TPoint &Point)
{
	fx += Point.GetX();
	fy += Point.GetY();
	fz += Point.GetZ();
}

void TPoint::DecX(const float &Value){fx -= Value;}
void TPoint::DecY(const float &Value){fy -= Value;}
void TPoint::DecZ(const float &Value){fz -= Value;}

void TPoint::Dec(const float &x,const float &y)
{
	fx -= x;
	fy -= y;
}

void TPoint::Dec(const float &x,const float &y,const float &z)
{
	fx -= x;
	fy -= y;
	fz -= z;
}

void TPoint::Dec(const TPoint &Point)
{
	fx -= Point.GetX();
	fy -= Point.GetY();
	fz -= Point.GetZ();
}

void TPoint::Mult(const float &Val)
{
	fx *= Val;
	fy *= Val;
	fz *= Val;
}

void TPoint::Mult(const float &x,const float &y)
{
	fx *= x;
	fy *= y;
}

void TPoint::Mult(const float &x,const float &y,const float &z)
{
	fx *= x;
	fy *= y;
	fz *= z;
}

void TPoint::Mult(const TPoint &Point)
{
	fx *= Point.GetX();
	fy *= Point.GetY();
	fz *= Point.GetZ();
}

void TPoint::Div(const float &Val)
{
	fx /= Val;
	fy /= Val;
	fz /= Val;
}

void TPoint::Div(const float &x,const float &y)
{
	fx /= x;
	fy /= y;
}

void TPoint::Div(const float &x,const float &y,const float &z)
{
	fx /= x;
	fy /= y;
	fz /= z;
}

void TPoint::Div(const TPoint &Point)
{
	fx /= Point.GetX();
	fy /= Point.GetY();
	fz /= Point.GetZ();
}


