///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef GRAPHICSYSTEM_H_INCLUDED
#define GRAPHICSYSTEM_H_INCLUDED


#include "GraphicSystem/Base/enum_GraphicSystem.h"

#include "GraphicSystem/Base/BaseGraphicObjectPointerList.h"
#include "GraphicSystem/Base/BaseViewMatrixWorker.h"
#include "GraphicSystem/Base/Palet.h"

#include "GraphicSystem/Sprite/Sprite.h"
#include "GraphicSystem/Sprite/SpriteList.h"
#include "GraphicSystem/Sprite/SpriteListManager.h"
#include "GraphicSystem/Sprite/SpritePointerList.h"

#include "GraphicSystem/Text/FontList.h"
#include "GraphicSystem/Text/Glyph.h"
#include "GraphicSystem/Text/GlyphList.h"
#include "GraphicSystem/Text/Text.h"
#include "GraphicSystem/Text/TextList.h"
#include "GraphicSystem/Text/TextListManager.h"
#include "GraphicSystem/Text/TextPointerList.h"

#include "GraphicSystem/TextureList.h"
#include "GraphicSystem/TextureListManager.h"
#include "GraphicSystem/TexturePointerList.h"

#include "GraphicSystem/Camera.h"
#include "GraphicSystem/ColorArray.h"


#endif // GRAPHICSYSTEM_H_INCLUDED
