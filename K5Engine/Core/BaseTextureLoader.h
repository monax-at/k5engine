/// Authors:
///	Sergey Kalenik [monax.at@gmail.com]
///	Maxim Kolesnikov [mpkmaximus@gmail.com]


#pragma once
#ifndef TBASETEXTURELOADER_H_INCLUDED
#define TBASETEXTURELOADER_H_INCLUDED


#include "GraphicSystem/Base/Texture.h"

#include <string>
using std::wstring;


class TBaseTextureLoader
{
    protected:
		TExceptionGenerator Exception;
		bool UseEngineFilePathFormat;
    public:
        TBaseTextureLoader();
        virtual ~TBaseTextureLoader();

        virtual pTTexture Run(	const wstring &FileName, const int &PosX,
								const int &PosY, const int &Width, const int &Height,
								const wstring &TexName=L"") = 0;

        virtual pTTexture Run (const wstring &FileName, const wstring &TexName=L"") = 0;
        virtual pTTexture Mask(const wstring &FileName, const wstring &TexName=L"") = 0;

		void SetUseEngineFilePathFormat(const bool &Val);
		bool GetUseEngineFilePathFormat() const;
};
typedef TBaseTextureLoader* pTBaseTextureLoader;


#endif // TBASETEXTURELOADER_H_INCLUDED
