///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef BASESCENEOBJECT_H_INCLUDED
#define BASESCENEOBJECT_H_INCLUDED


#include "PointArray.h"
#include "Value.h"

#include <string>

using std::wstring;


class TBaseSceneObject
{
	protected:
		static unsigned long IdCounter;
		unsigned long        Id;

		wstring  Name;
	public:
		TPoint Pos;
		TPoint Center;
		TValue Angle;
	public:
		TBaseSceneObject();
		~TBaseSceneObject();

		unsigned long GetID() const;

		void    SetName(const wstring &Val);
		wstring GetName() const;
};

typedef TBaseSceneObject* pTBaseSceneObject;


template <class TObject>
bool CheckObjectPosZ(TObject *One, TObject *Two)
{
	return One->Pos.GetZ() < Two->Pos.GetZ();
}


#endif // BASESCENEOBJECT_H_INCLUDED
