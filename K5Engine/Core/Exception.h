///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef EXCEPTION_H_INCLUDED
#define EXCEPTION_H_INCLUDED


#include "Debug.h"


class TException
{
    protected:
        wstring Mess;
    public:
        TException();
        TException(const wstring &NewMess);
        ~TException();

        wstring GetMess() const;
};


#endif // EXCEPTION_H_INCLUDED
