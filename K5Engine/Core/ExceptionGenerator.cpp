#include "ExceptionGenerator.h"


TExceptionGenerator::TExceptionGenerator():Ptr(NULL)
{
}

TExceptionGenerator::TExceptionGenerator(const wstring &Prifix):Ptr(NULL),MessPrefix(Prifix)
{
}

TExceptionGenerator::TExceptionGenerator(const wstring &Prifix, void *PtrVal, const wstring &Info)
{
	Ptr        = PtrVal;
	MessPrefix = Prifix;

	Debug.RegistrationObj(PtrVal,Info);
}

TExceptionGenerator::~TExceptionGenerator()
{
	if(Ptr != NULL){ Debug.UnregistrationObj(Ptr); }
}


void TExceptionGenerator::operator()(TException &Ex,const bool &SetPref) const
{
	if(SetPref == true) throw TException(MessPrefix + Ex.GetMess());
	throw TException(Ex.GetMess());
}

void TExceptionGenerator::Run(TException &Ex,const bool &SetPref) const
{
	if(SetPref == true) throw TException(MessPrefix + Ex.GetMess());
	throw TException(Ex.GetMess());
}

void TExceptionGenerator::operator()(const TException &Ex,const bool &SetPref) const
{
	if(SetPref == true) throw TException(MessPrefix + Ex.GetMess());
	throw TException(Ex.GetMess());
}

void TExceptionGenerator::Run(const TException &Ex,const bool &SetPref) const
{
	if(SetPref == true) throw TException(MessPrefix + Ex.GetMess());
	throw TException(Ex.GetMess());
}

void TExceptionGenerator::operator()(const wstring &Mess,const bool &SetPref) const
{
	if(SetPref == true){throw TException(MessPrefix + Mess);}
	throw TException(Mess);
}

void TExceptionGenerator::Run(const wstring &Mess,const bool &SetPref) const
{
	if(SetPref == true){throw TException(MessPrefix + Mess);}
	throw TException(Mess);
}

void TExceptionGenerator::operator()(	const bool &Check,const wstring &Mess,
										const bool &SetPref) const
{
	if(Check == true){return;}

	if(SetPref == true){throw TException(MessPrefix + Mess);}
	throw TException(Mess);
}

void TExceptionGenerator::Run(	const bool &Check,const wstring &Mess,
								const bool &SetPref) const
{
	if(Check == true){return;}

	if(SetPref == true){throw TException(MessPrefix + Mess);}
	throw TException(Mess);
}

void TExceptionGenerator::SetPrefix(const wstring &Val)
{
	MessPrefix = Val;
}

void TExceptionGenerator::SetInfo(const wstring &Val)
{
	if (Ptr != NULL){ Debug.SetObjInfo(Ptr, Val); }
}


