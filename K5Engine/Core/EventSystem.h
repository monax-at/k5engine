///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef EVENTSYSTEM_H_INCLUDED
#define EVENTSYSTEM_H_INCLUDED


#include "EventSystem/enum_EventTypes.h"
#include "EventSystem/Event.h"
#include "EventSystem/EventDeque.h"


#endif // EVENTSYSTEM_H_INCLUDED
