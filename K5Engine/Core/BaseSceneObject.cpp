#include "BaseSceneObject.h"


unsigned long TBaseSceneObject::IdCounter = 0;


TBaseSceneObject::TBaseSceneObject()
{
	IdCounter++;
	Id = IdCounter;
}

TBaseSceneObject::~TBaseSceneObject()
{
}


unsigned long TBaseSceneObject::GetID() const
{
	return Id;
}

void TBaseSceneObject::SetName(const wstring &Val)
{
	Name = Val;
}

wstring TBaseSceneObject::GetName() const
{
	return Name;
}



