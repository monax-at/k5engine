#include "Device.h"


TDevice::TDevice():TBaseDevice(),Device(NULL)
{
	Exception.SetPrefix(L"TDevice: ");
}

TDevice::~TDevice()
{
	delete Device;
}


void TDevice::ToSetCaption()
{
	if(Device == NULL){return;}
	Device->SetCaption(Caption);
}

void TDevice::ToSetEventDeque()
{
	if(Device == NULL){return;}
	Device->SetEventDeque(EventDeque);
}

void TDevice::ToSetClearColor()
{
	if(Device == NULL){return;}
	Device->SetClearColor(ClearColor);
}

void TDevice::ToSetTextureFilter()
{
	if(Device == NULL){return;}
	Device->SetTextureFilter(MinTextureFilter,MagTextureFilter);
}

void TDevice::Construct(const pTBaseDevice &NewDevice)
{
	delete Device;
	Device = NewDevice;
	Exception(Device!=NULL,L"in Construct() Device is NULL");
}

void TDevice::Create(	const int &NewWidth,const int &NewHeight,
						const bool &NewFullScreen)
{
	InitRandomizer();

	Exception(Device!=NULL,L"in Create() Device not construct");
	Width  = NewWidth;
	Height = NewHeight;

	Device->SetWindowStyle(WindowStyle);
	Device->SetCaption(Caption);
	Device->SetEventDeque(EventDeque);
	Device->SetClearColor(ClearColor);
	Device->SetColorBits(ColorBits);
	Device->SetTextureFilter(MinTextureFilter,MagTextureFilter);
	Device->SetEnableVSync(EnableVSync);
	Device->SetTimerEventDelay(TimerEventDelay);

	Device->Create(NewWidth,NewHeight,NewFullScreen);
}

void TDevice::ClearSceneColor()
{
	Exception(Device!=NULL, L"in ClearSceneColor() Device not construct");
	Device->ClearSceneColor();
}

void TDevice::SwapSceneBuffers()
{
	Exception(Device!=NULL, L"in SwapSceneBuffers() Device not construct");
	Device->SwapSceneBuffers();
}

void TDevice::ProcessEvent()
{
	Exception(Device!=NULL, L"in ProcessEvent() unknown Device type");
	Device->ProcessEvent();
}

void TDevice::SetDrawCursor(const bool &Val)
{
	Exception(Device!=NULL, L"in SetDrawCursor(...) unknown Device type");
	Device->SetDrawCursor(Val);
}

void TDevice::SetTimerEventDelay(const float &Val)
{
	TimerEventDelay = Val;
	if(Device != NULL){ Device->SetTimerEventDelay(TimerEventDelay); }
}

float TDevice::GetTimerEventDelay() const
{
	return TimerEventDelay;
}

void  TDevice::ResetTimerEventDelayCounter()
{
	if(Device == NULL){TimerEventDelayCounter = 0.0f;}
	Device->ResetTimerEventDelayCounter();
}

void TDevice::MinimizeWindow()
{
	Exception(Device!=NULL, L"in MinimizeWindow() unknown Device type");
	Device->MinimizeWindow();
}

void TDevice::RestoreWindow()
{
	Exception(Device!=NULL, L"in RestoreWindow() unknown Device type");
	Device->RestoreWindow();
}

TPoint TDevice::GetCursor() const
{
	Exception(Device!=NULL, L"in GetCursor() unknown Device type");
	return Device->GetCursor();
}

TPoint TDevice::GetGlobalCursor() const
{
	Exception(Device!=NULL, L"in GetGlobalCursor() unknown Device type");
	return Device->GetGlobalCursor();
}

void TDevice::ResetTimer()
{
	Exception(Device!=NULL, L"in ResetTimer() unknown Device type");
	Device->ResetTimer();
}

void TDevice::ResetDevice()
{
	Exception(Device!=NULL, L"in ResetDevice() unknown Device type");
	Device->ResetDevice();
}

bool TDevice::IsActive()
{
	Exception(Device!=NULL, L"in IsActive() unknown Device type");
	return Device->IsActive();
}

pTBaseTextureLoader TDevice::CreateTextureLoader()
{
	Exception(Device!=NULL, L"in CreateTextureLoader() Device not construct");
	return Device->CreateTextureLoader();
}

pTBaseTextureCreater TDevice::CreateTextureCreater()
{
	Exception(Device!=NULL, L"in CreateTextureCreater() Device not construct");
	return Device->CreateTextureCreater();
}

pTBaseScreenPixelReader TDevice::CreateScreenPixelReader()
{
	Exception(Device!=NULL, L"in CreateScreenPixelReader() Device not construct");
	return Device->CreateScreenPixelReader();
}

pTBaseViewMatrixWorker TDevice::CreateViewMatrixWorker()
{
	Exception(Device!=NULL, L"in CreateViewMatrixWorker() Device not construct");
	return Device->CreateViewMatrixWorker();
}

pTBaseVertexData TDevice::CreateSpriteVertexData()
{
	Exception(Device!=NULL, L"in CreateSpriteVertexData() Device not construct");
	return Device->CreateSpriteVertexData();
}



