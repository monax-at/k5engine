///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef BITMAPFONTLOADER_H_INCLUDED
#define BITMAPFONTLOADER_H_INCLUDED


#include "../Core/Core.h"

#include <fstream>
#include <sstream>

using std::ifstream;
using std::wstringstream;


class TBitmapFontLoader
{
	protected:
		TExceptionGenerator Exception;

		TTexturePointerList  TexturePointers;
		pTTextureListManager Textures;
	protected:
		inline wstring CutData(const wstring &Str);

		inline void ParceInfoStr   (const wstring &Str, const pTBitmapFont &Font);
		inline void ParceTextureStr(const wstring &Str, const pTBitmapFont &Font);
		inline void ParceCellStr   (const wstring &Str, const pTBitmapFont &Font);
		inline void ParceStr       (const wstring &Str, const pTBitmapFont &Font);
	public:
		TBitmapFontLoader();
		virtual ~TBitmapFontLoader();

		virtual pTBitmapFont Run(const wstring &FileName);

		void SetTextures(const pTTextureListManager &Val);
		pTTextureListManager GetTextures() const;

		void AddTexturePointer(const pTTexture &Val);
		void ClearTexturePointers();
};


#endif // BITMAPFONTLOADER_H_INCLUDED
