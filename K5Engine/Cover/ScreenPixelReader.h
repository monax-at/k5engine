///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef SCREENPIXELREADER_H_INCLUDED
#define SCREENPIXELREADER_H_INCLUDED

#include "../Core/BaseDevice.h"
#include "../Core/BaseScreenPixelReader.h"

class TScreenPixelReader:public TBaseScreenPixelReader
{
	protected:
		TExceptionGenerator Exception;
		pTBaseScreenPixelReader Reader;
	protected:
		TColor ToRun(const float &X,const float &Y);
	public:
		TScreenPixelReader();
		virtual ~TScreenPixelReader();

		void SetDevice(const pTBaseDevice &Device);
};

typedef TScreenPixelReader* pTScreenPixelReader;


#endif // SCREENPIXELREADER_H_INCLUDED
