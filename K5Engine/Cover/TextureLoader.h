/// Authors:
/// Sergey Kalenik [monax.at@gmail.com]
///	Maxim Kolesnikov [mpkmaximus@gmail.com]


#pragma once
#ifndef TEXTURELOADER_H_INCLUDED
#define TEXTURELOADER_H_INCLUDED


#include "../Core/Core.h"


class TTextureLoader:public TBaseTextureLoader
{
	protected:
		TExceptionGenerator Exception;
		pTBaseTextureLoader Loader;
	public:
		TTextureLoader();
		TTextureLoader(const pTBaseDevice &Device);
		virtual ~TTextureLoader();

		void SetDevice(const pTBaseDevice &Device);

		pTTexture Run(	const wstring &FileName, const int &PosX, const int &PosY,
						const int &Width,const int &Height,const wstring &TexName=L"");

		pTTexture Run(const wstring &FileName, const wstring &TexName=L"");
		pTTexture Mask(const wstring &FileName,const wstring &TexName=L"");

        /*void Run (	const pTTexture &Texture, const wstring &FileName,
					const wstring &TexName=L"");

        void Mask(	const pTTexture &Texture, const wstring &FileName,
					const wstring &TexName=L"");
		*/
};
typedef TTextureLoader* pTTextureLoader;


#endif // TEXTURELOADER_H_INCLUDED
