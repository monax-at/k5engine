#include "BitmapFontLoader.h"


TBitmapFontLoader::TBitmapFontLoader():	Exception(L"TBitmapFontLoader: "), Textures(NULL)
{
}

TBitmapFontLoader::~TBitmapFontLoader()
{
}


wstring TBitmapFontLoader::CutData(const wstring &Str)
{
	size_t StartPos(Str.find(L'"'));
	size_t StopPos(Str.rfind(L'"'));

	StartPos +=1;

	return Str.substr(StartPos,StopPos - StartPos);
}

void TBitmapFontLoader::ParceInfoStr(const wstring &Str, const pTBitmapFont &Font)
{
	size_t NamePos(Str.find(L"name"));
	size_t SizePos(Str.find(L"size"));

	Exception(NamePos != wstring::npos, L"in ParceInfoStr name attr not set" );
	Exception(SizePos != wstring::npos, L"in ParceInfoStr size attr not set" );

	Font->SetName( CutData(Str.substr(NamePos,SizePos-NamePos)) );
	Font->SetFontSize( WStrToInt(CutData(Str.substr(SizePos))) );
}

void TBitmapFontLoader::ParceTextureStr(const wstring &Str, const pTBitmapFont &Font)
{
	if(Textures == NULL){return;}

	size_t ListPos(Str.find(L"list"));
	size_t NamePos(Str.find(L"name"));

	wstring List(CutData( Str.substr(ListPos,NamePos - ListPos) ));
	wstring Name(CutData( Str.substr(NamePos) ));

	TexturePointers.Add( Textures->Get(List,Name) );
}

void TBitmapFontLoader::ParceCellStr(const wstring &Str, const pTBitmapFont &Font)
{
	size_t CharPos(Str.find(L"char="));
	size_t TextureIdPos(Str.find(L"textureid="));

	size_t XPos(Str.find(L"x="));
	size_t YPos(Str.find(L"y="));

	size_t WidthPos(Str.find(L"width="));
	size_t HeightPos(Str.find(L"height="));

	size_t ShiftXPos(Str.find(L"shiftx="));
	size_t ShiftYPos(Str.find(L"shifty="));

	size_t CursorShiftPos(Str.find(L"cursorshift="));

	TBitmapFontCell Cell;
	Cell.SetParams( CutData( Str.substr(CharPos, TextureIdPos - CharPos) )[0],
					WStrToInt(CutData( Str.substr(TextureIdPos,XPos - TextureIdPos)) ),

					WStrToInt(CutData( Str.substr(XPos, YPos - XPos)) ),
					WStrToInt(CutData( Str.substr(YPos, WidthPos - YPos)) ),

					WStrToInt(CutData( Str.substr(WidthPos, HeightPos - WidthPos)) ),
					WStrToInt(CutData( Str.substr(HeightPos, ShiftXPos - HeightPos)) ),

					WStrToInt(CutData( Str.substr(ShiftXPos, ShiftYPos - ShiftXPos)) ),
					WStrToInt(CutData( Str.substr(ShiftYPos, CursorShiftPos - ShiftYPos)) ),

					WStrToInt(CutData( Str.substr(CursorShiftPos)) )
				   );

	Font->AddGlyphInfo(Cell);
}

void TBitmapFontLoader::ParceStr(const wstring &Str, const pTBitmapFont &Font)
{
	if(Str.length() <= 0){ return; }
	if(Str[0] == L'#')	 { return; }

	wstring Module;

	for(wstring::const_iterator Itr = Str.begin(); Itr < Str.end(); Itr++ ){
		if(*Itr == L' '){ break; }
		else			{ Module += *Itr; }
	}

	if(Module == L"info")	{ ParceInfoStr   (Str, Font); }
	if(Module == L"texture"){ ParceTextureStr(Str, Font); }
	if(Module == L"cell")	{ ParceCellStr   (Str, Font); }
}

pTBitmapFont TBitmapFontLoader::Run(const wstring &FileName)
{
	ifstream File;
	File.open(WStrToStr(FileName).c_str());

	Exception(File.is_open(), L"in Run(FileName) can`t open data file: " + FileName);


	pTBitmapFont Font(new TBitmapFont);

	string Buff;
	while(getline(File,Buff)){ ParceStr( StrToWStr(Buff), Font ); }

	int TexSize(TexturePointers.GetSize());
	for(int i=0;i<TexSize;i++){
		Font->AddFontTexture(TexturePointers.Get(i));
	}

	return Font;
}

void TBitmapFontLoader::SetTextures(const pTTextureListManager &Val)
{
	Textures = Val;
}

pTTextureListManager TBitmapFontLoader::GetTextures() const
{
	return Textures;
}

void TBitmapFontLoader::AddTexturePointer(const pTTexture &Val)
{
	TexturePointers.Add(Val);
}

void TBitmapFontLoader::ClearTexturePointers()
{
	TexturePointers.Clear();
}


