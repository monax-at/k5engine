#include "GraphicScene.h"


TGraphicScene::TGraphicScene():ViewFlag(true)
{
	Camera		= new TCamera;
	RenderQueue = new TRenderQueue;

	Sprites		= new TSpriteListManager;
	Texts 		= new TTextListManager;

	Sprites		->SetRenderQueue(RenderQueue);
	Texts		->SetRenderQueue(RenderQueue);
}

TGraphicScene::~TGraphicScene()
{
	delete Texts;
	delete Sprites;

	delete RenderQueue;
	delete Camera;
}


void TGraphicScene::SetDevice(const pTBaseDevice &Val)
{
	Camera	->SetDevice(Val);
	Sprites	->SetDevice(Val);
	Texts	->SetDevice(Val);
}

pTCamera TGraphicScene::GetCamera()
{
	return Camera;
}

pTRenderQueue TGraphicScene::GetRenderQueue()
{
	return RenderQueue;
}

pTSpriteListManager TGraphicScene::GetSprites()
{
	return Sprites;
}

pTTextListManager TGraphicScene::GetTexts()
{
	return Texts;
}

pTSprite TGraphicScene::GetSprite(const int &List,const int &Sprite)
{
	return Sprites->Get(List,Sprite);
}

pTSprite TGraphicScene::GetSprite(const int &List,const wstring &Sprite)
{
	return Sprites->Get(List,Sprite);
}

pTSprite TGraphicScene::GetSprite(const wstring &List,const int &Sprite)
{
	return Sprites->Get(List,Sprite);
}

pTSprite TGraphicScene::GetSprite(const wstring &List,const wstring &Sprite)
{
	return Sprites->Get(List,Sprite);
}

pTSpriteList TGraphicScene::AddSpriteList(const wstring &ListName)
{
	return Sprites->Add(ListName);
}

pTTextList TGraphicScene::AddTextList(const wstring &ListName)
{
	return Texts->Add(ListName);
}

void TGraphicScene::Add( const pTSpriteList &List, const bool &SetDeviceFlag)
{
	Sprites->Add(List,SetDeviceFlag);
}

void TGraphicScene::Add( const pTTextList &List, const bool &SetDeviceFlag)
{
	Texts->Add(List,SetDeviceFlag);
}

void TGraphicScene::Add(const int &List,const pTSprite &Sprite)
{
	Sprites->Add(List,Sprite);
}

void TGraphicScene::Add(const int &List,const  TSprite &Sprite)
{
	Sprites->Add(List,Sprite);
}

void TGraphicScene::Add(const wstring &List,const pTSprite &Sprite)
{
	Sprites->Add(List,Sprite);
}

void TGraphicScene::Add(const wstring &List,const  TSprite &Sprite)
{
	Sprites->Add(List,Sprite);
}

void TGraphicScene::Add(const int &List,const pTText &Text)
{
	Texts->Add(List,Text);
}

void TGraphicScene::Add(const int &List,const TText &Text)
{
	Texts->Add(List,Text);
}

void TGraphicScene::Add(const wstring &List,const pTText &Text)
{
	Texts->Add(List,Text);
}

void TGraphicScene::Add(const wstring &List,const TText &Text)
{
	Texts->Add(List,Text);
}

void TGraphicScene::Sort()
{
	RenderQueue->Sort();
}

void TGraphicScene::Draw()
{
	if(ViewFlag == false){return;}

	Camera->Run();
	RenderQueue->Draw();
	Camera->Stop();
}

void TGraphicScene::SetView(const bool &Val)
{
	ViewFlag = Val;
}

bool TGraphicScene::GetView() const
{
	return ViewFlag;
}

void TGraphicScene::Clear()
{
	Sprites->Clear();
	Texts->Clear();
}


