///	authors:
///	Sergey Kalenik [monax.at@gmail.com]
///	Maxim Kolesnikov [mpkmaximus@gmail.com]


#pragma once
#ifndef DEVICE_H_INCLUDED
#define DEVICE_H_INCLUDED


#include "../Core/BaseDevice.h"


class TDevice:public TBaseDevice
{
	protected:
		pTBaseDevice Device;
	protected:
		virtual void ToSetCaption();
		virtual void ToSetEventDeque();
		virtual void ToSetClearColor();
		virtual void ToSetTextureFilter();
	public:
		TDevice();
		virtual ~TDevice();

		virtual void Construct(const pTBaseDevice &NewDevice);

		void Create(const int &NewWidth,const int &NewHeight,
					const bool &NewFullScreen = false);

		void ClearSceneColor();
		void SwapSceneBuffers();
		void ProcessEvent();

		void SetDrawCursor(const bool &Val);

		void  SetTimerEventDelay(const float &Val);
		float GetTimerEventDelay() const;
		void  ResetTimerEventDelayCounter();

		void MinimizeWindow();
		void RestoreWindow();

		TPoint GetCursor() const;
		TPoint GetGlobalCursor() const;

		void ResetTimer();
		void ResetDevice();

		bool IsActive();

		pTBaseTextureLoader 	CreateTextureLoader();
		pTBaseTextureCreater    CreateTextureCreater();

		pTBaseScreenPixelReader CreateScreenPixelReader();
		pTBaseViewMatrixWorker 	CreateViewMatrixWorker();
		pTBaseVertexData        CreateSpriteVertexData();
};

typedef TDevice* pTDevice;


#endif // DEVICE_H_INCLUDED
