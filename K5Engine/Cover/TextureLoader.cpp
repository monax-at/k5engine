#include "TextureLoader.h"


TTextureLoader::TTextureLoader():TBaseTextureLoader(),
									Exception(L"TTextureLoader: "),Loader(NULL)
{
}

TTextureLoader::TTextureLoader(const pTBaseDevice &Device):TBaseTextureLoader(),
									Exception(L"TTextureLoader: "),Loader(NULL)
{
	Exception(Device!=NULL,L"in TTextureLoader(...) Device is NULL");
	Loader = Device->CreateTextureLoader();
}

TTextureLoader::~TTextureLoader()
{
	delete Loader;
}


void TTextureLoader::SetDevice(const pTBaseDevice &Device)
{
	delete Loader;
	Loader = NULL;
	if(Device!=NULL){Loader = Device->CreateTextureLoader();}
}

pTTexture TTextureLoader::Run(	const wstring &FileName,const int &PosX,
								const int &PosY, const int &Width,
								const int &Height,const wstring &TexName)
{
	Exception(Loader!=NULL,L"in Run(... ... ...) Loader is NULL");

	Loader->SetUseEngineFilePathFormat(UseEngineFilePathFormat);
	return Loader->Run(FileName,PosX,PosY,Width,Height,TexName);
}

pTTexture TTextureLoader::Run(const wstring &FileName, const wstring &TexName)
{
	Exception(Loader!=NULL,L"in Run(...) Loader is NULL");

	Loader->SetUseEngineFilePathFormat(UseEngineFilePathFormat);
	return Loader->Run(FileName,TexName);
}

pTTexture TTextureLoader::Mask(const wstring &FileName,const wstring &TexName)
{
	Exception(Loader!=NULL,L"in Mask(...) Loader is NULL");

	Loader->SetUseEngineFilePathFormat(UseEngineFilePathFormat);
	return Loader->Mask(FileName,TexName);
}

/*
void TTextureLoader::Run (	const pTTexture &Texture, const wstring &FileName,
							const wstring &TexName)
{
	Exception(Loader!=NULL,L"in Run(...) Loader is NULL");
	Loader->Run(Texture,FileName,TexName);
}

void TTextureLoader::Mask(	const pTTexture &Texture, const wstring &FileName,
							const wstring &TexName)
{
	Exception(Loader!=NULL,L"in Mask(...) Loader is NULL");
	Loader->Mask(Texture,FileName,TexName);
}


*/
