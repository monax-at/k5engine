///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef GRAPHICSCENE_H_INCLUDED
#define GRAPHICSCENE_H_INCLUDED


#include "../Core/Core.h"


class TGraphicScene
{
	protected:
		pTCamera		Camera;
		pTRenderQueue 	RenderQueue;

		pTSpriteListManager Sprites;
		pTTextListManager 	Texts;

		bool ViewFlag;
	public:
		TGraphicScene();
		~TGraphicScene();

		void SetDevice(const pTBaseDevice &Val);

		pTCamera				GetCamera();
		pTRenderQueue			GetRenderQueue();

		pTSpriteListManager 		GetSprites();
		pTTextListManager 			GetTexts();

		pTSprite GetSprite(const int &List,const int &Sprite);
		pTSprite GetSprite(const int &List,const wstring &Sprite);

		pTSprite GetSprite(const wstring &List,const int &Sprite);
		pTSprite GetSprite(const wstring &List,const wstring &Sprite);

		pTSpriteList AddSpriteList(const wstring &ListName);
		pTTextList 	 AddTextList  (const wstring &ListName);

		void Add( const pTSpriteList		&List, const bool &SetDeviceFlag=true);
		void Add( const pTTextList			&List, const bool &SetDeviceFlag=true);

		void Add(const int &List,const pTSprite &Sprite);
		void Add(const int &List,const  TSprite &Sprite);

		void Add(const wstring &List,const pTSprite &Sprite);
		void Add(const wstring &List,const  TSprite &Sprite);

		void Add(const int &List,const pTText &Text);
		void Add(const int &List,const  TText &Text);

		void Add(const wstring &List,const pTText &Text);
		void Add(const wstring &List,const  TText &Text);

		void Sort();
		void Draw();

		void SetView(const bool &Val);
		bool GetView() const;

		void Clear();
};

typedef TGraphicScene* pTGraphicScene;


#endif // SCENEWORKER_H_INCLUDED
