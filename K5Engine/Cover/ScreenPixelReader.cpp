#include "ScreenPixelReader.h"

TScreenPixelReader::TScreenPixelReader():TBaseScreenPixelReader(),
					Exception(L"TScreenPixelReader: "),Reader(NULL)
{
}

TScreenPixelReader::~TScreenPixelReader()
{
	delete Reader;
}


TColor TScreenPixelReader::ToRun(const float &X,const float &Y)
{
	Exception(Reader!=NULL,L"in ToRun(...) Reader is NULL");
	return Reader->Run(X,Y);
}

void TScreenPixelReader::SetDevice(const pTBaseDevice &Device)
{
	delete Reader;
	Reader = Device->CreateScreenPixelReader();
}


