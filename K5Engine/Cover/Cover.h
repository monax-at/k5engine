///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef COVER_H_INCLUDED
#define COVER_H_INCLUDED


#include "BitmapFontLoader.h"
#include "Device.h"
#include "GraphicScene.h"
#include "ScreenPixelReader.h"
#include "TextureLoader.h"


#endif // COVER_H_INCLUDED
