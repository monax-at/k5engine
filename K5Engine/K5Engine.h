///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef K5ENGINE_H_INCLUDED
#define K5ENGINE_H_INCLUDED


#include "Core/Core.h"
#include "Cover/Cover.h"
#include "LibMath/LibMath.h"
#include "LibTools/LibTools.h"

#ifdef USEWINAPIGLDEVICE
	#include "LibShell/OpenGL/WinApiGLDevice.h"
#endif

#ifdef USEWINAPIDX8DEVICE
	#include "LibShell/DX8/WinApiDX8Device.h"
#endif

#ifdef USESDLGLDEVICE
	#include "LibShell/SDL/SDLGLDevice.h"
#endif


#endif // K5ENGINE_H_INCLUDED
