#include "Log.h"


TLog::TLog(): File(NULL),Enable(true)
{
}

TLog::TLog(const wstring &NewFileName): File(NULL),FileName(NewFileName),Enable(true)
{
}

TLog::~TLog()
{
}


void TLog::SetEnable(const bool &Val)
{
	Enable = Val;
}

bool TLog::GetEnable() const
{
	return Enable;
}

void TLog::Open()
{
	File = fopen(WStrToStr(FileName).c_str(),"a");
}

void TLog::Close()
{
	if(File!=NULL){ fclose(File); }
}

void TLog::WriteValue(const wstring &Mess)
{
	if(!Enable){return;}

	Open();
	if(File==NULL){return;}

	fputws(Mess.c_str(),File);
	fputws(L"\n",File);

	Close();
}

void TLog::WriteValue(const int &Val)
{
	WriteValue(IntToWStr(Val));
}

void TLog::WriteValue(const long &Val)
{
	WriteValue(LongToWStr(Val));
}

void TLog::WriteValue(const float &Val)
{
	WriteValue(FloatToWStr(Val));
}

void TLog::WriteValue(const double &Val)
{
	WriteValue(DoubleToWStr(Val));
}

void TLog::WriteValue(const TColor &Val)
{
	WriteValue( L"Color"
				L" :R = " 	+ FloatToWStr(Val.GetRed())   +
				L" ;G = " 	+ FloatToWStr(Val.GetGreen()) +
				L" ;B = " 	+ FloatToWStr(Val.GetBlue())  +
				L" ;A = " 	+ FloatToWStr(Val.GetAlpha()));
}

void TLog::WriteValue(const TPoint &Val)
{
	WriteValue( L"Point3D"
				L" :X = " 	+ FloatToWStr(Val.GetX()) +
				L" ;Y = " 	+ FloatToWStr(Val.GetY()) +
				L" ;Z = " 	+ FloatToWStr(Val.GetZ()));
}

void TLog::WriteValue(const TValue &Val)
{
	WriteValue( L"Value: " + FloatToWStr(Val.Get()) );
}

void TLog::WriteValue(const TVector2D &Val)
{
	WriteValue(	L"Vector2D"
				L" :X = " + FloatToWStr(Val.x) +
				L" ;Y = " + FloatToWStr(Val.y));
}

void TLog::WriteValue(const TVector3D &Val)
{
	WriteValue(	L"Vector2D"
				L" :X = " + FloatToWStr(Val.x) +
				L" ;Y = " + FloatToWStr(Val.y) +
				L" ;Z = " + FloatToWStr(Val.z));
}

void TLog::WriteValue(const TMatrix3D &Val)
{
	WriteValue (L"Matrix3D:\n"
				L"[0][0] = " + FloatToWStr(Val.a[0][0]) + L"; " +
				L"[0][1] = " + FloatToWStr(Val.a[0][1]) + L"; " +
				L"[0][2] = " + FloatToWStr(Val.a[0][2]) + L"; " +
				L"[0][3] = " + FloatToWStr(Val.a[0][3]) + L"; \n" +

				L"[1][0] = " + FloatToWStr(Val.a[1][0]) + L"; " +
				L"[1][1] = " + FloatToWStr(Val.a[1][1]) + L"; " +
				L"[1][2] = " + FloatToWStr(Val.a[1][2]) + L"; " +
				L"[1][3] = " + FloatToWStr(Val.a[1][3]) + L"; \n" +

				L"[2][0] = " + FloatToWStr(Val.a[2][0]) + L"; " +
				L"[2][1] = " + FloatToWStr(Val.a[2][1]) + L"; " +
				L"[2][2] = " + FloatToWStr(Val.a[2][2]) + L"; " +
				L"[2][3] = " + FloatToWStr(Val.a[2][3]) + L"; \n" +

				L"[3][0] = " + FloatToWStr(Val.a[3][0]) + L"; " +
				L"[3][1] = " + FloatToWStr(Val.a[3][1]) + L"; " +
				L"[3][2] = " + FloatToWStr(Val.a[3][2]) + L"; " +
				L"[3][3] = " + FloatToWStr(Val.a[3][3]) + L";");
}

void TLog::SetFileName(const wstring &NewFileName)
{
	FileName = NewFileName;
}


void TLog::operator ()(const string &Mess)
{
	WriteValue(StrToWStr(Mess));
}

void TLog::operator ()(const wstring &Mess)
{
	WriteValue(Mess);
}

void TLog::operator ()(const wchar_t &Char)
{
	wstring Mess;
	Mess += Char;
	WriteValue(Mess);
}

void TLog::operator ()(const int &Val)
{
	WriteValue(Val);
}

void TLog::operator ()(const long &Val)
{
	WriteValue(Val);
}

void TLog::operator ()(const float &Val)
{
	WriteValue(Val);
}

void TLog::operator ()(const double &Val)
{
	WriteValue(Val);
}

void TLog::operator ()(const TException &Val)
{
	WriteValue(Val.GetMess());
}

void TLog::operator ()(const TColor &Val)
{
	WriteValue(Val);
}

void TLog::operator ()(const TPoint &Val)
{
	WriteValue(Val);
}

void TLog::operator ()(const TValue &Val)
{
	WriteValue(Val);
}

void TLog::operator ()(const TVector2D &Val)
{
	WriteValue(Val);
}

void TLog::operator ()(const TVector3D &Val)
{
	WriteValue(Val);
}

void TLog::operator ()(const TMatrix3D &Val)
{
	WriteValue(Val);
}

void TLog::Write(const string &Mess)
{
	WriteValue(StrToWStr(Mess));
}

void TLog::Write(const wstring &Mess)
{
	WriteValue(Mess);
}

void TLog::Write(const wchar_t &Char)
{
	wstring Mess;
	Mess += Char;
	WriteValue(Mess);
}

void TLog::Write(const int &Val)
{
	WriteValue(Val);
}

void TLog::Write(const long &Val)
{
	WriteValue(Val);
}

void TLog::Write(const float &Val)
{
	WriteValue(Val);
}

void TLog::Write(const double &Val)
{
	WriteValue(Val);
}

void TLog::Write(const TException &Val)
{
	WriteValue(Val.GetMess());
}

void TLog::Write(const TColor &Val)
{
	WriteValue(Val);
}

void TLog::Write (const TPoint &Val)
{
	WriteValue(Val);
}

void TLog::Write (const TValue &Val)
{
	WriteValue(Val);
}

void TLog::Write (const TVector2D &Val)
{
	WriteValue(Val);
}

void TLog::Write (const TVector3D &Val)
{
	WriteValue(Val);
}

void TLog::Write (const TMatrix3D &Val)
{
	WriteValue(Val);
}



