#include "Tools.h"


//wstring UTF8ToUnicode(const string &Val)
//{
//#ifdef _WIN32
//	int BuffSize = MultiByteToWideChar(CP_UTF8, 0, Val.c_str(), -1, NULL, 0);
//
//	if(BuffSize == 0){
//		return wstring(L"Failed UTF8ToUnicode(...) BuffSize is 0");
//	}
//
//	wchar_t *Buff = new wchar_t[BuffSize];
//	if (Buff == NULL){
//		return wstring(L"Failed UTF8ToUnicode(...) Buff is NULL");
//	}
//	memset(Buff, 0, sizeof(Buff));
//
//	int Err = MultiByteToWideChar(CP_UTF8, 0, Val.c_str(), -1,Buff, BuffSize);
//
//	if (Err == 0){
//		delete[] Buff;
//		return wstring(L"Failed UTF8ToUnicode(...) convert operation");
//	}
//
//	wstring Res(Buff);
//	delete[] Buff;
//	return Res;
//#else
//	return StrToWStr(Val);
//#endif
//}

//string UnicodeToUTF8(const wstring &Val)
//{
//#ifdef _WIN32
//	int BuffSize = WideCharToMultiByte(CP_UTF8, 0, Val.c_str(), -1, NULL, 0,NULL,NULL);
//
//	if(BuffSize == 0){
//		return string("Failed UnicodeToUTF8(...) BuffSize is 0");
//	}
//
//	char* Buff =  new char[BuffSize];
//	if (Buff == NULL){
//		return string("Failed UnicodeToUTF8(...) Buff is NULL");
//	}
//	memset(Buff, 0, sizeof(Buff));
//
//	int Err = WideCharToMultiByte(CP_UTF8, 0, Val.c_str(), -1,Buff, BuffSize,NULL,NULL);
//
//	if (Err == 0){
//		delete[] Buff;
//		return string("Failed UTF8ToUnicode(...) convert operation");
//	}
//
//	string Res(Buff);
//	delete[] Buff;
//	return Res;
//#else
//	return WStrToStr(Val);
//#endif
//}


// Заметка: в функциях WstrToStr и StrToWstr для записи результата
//			используется цикл потому, что в cResult (и в wcResult)
//			оставался мусор. За относительно короткое время
//			лучшего решения не нашёл
// Заметка: также пришлось использовать динамическое выделение памяти
//			(char *cResult = new char[size] и wchar_t *wcResult = ...)
//			так как компилятор Микрософта не понимает создание массива
//			вида: cResult[size]; При этом MinGW воспринимает такие кон
//			струкции нормально. Насколько я понял, в MinGW возможность
//			создания массива типа cResult[size] - фича компилятора

string WStrToStr(const wstring &Target)
{
/*
#ifdef _WIN32
	int BuffSize = WideCharToMultiByte(CP_UTF8, 0, Target.c_str(), -1, NULL, 0,NULL,NULL);

	if(BuffSize == 0){
		return string("Failed UnicodeToUTF8(...) BuffSize is 0");
	}

	char* Buff =  new char[BuffSize];
	if (Buff == NULL){
		return string("Failed UnicodeToUTF8(...) Buff is NULL");
	}
	memset(Buff, 0, sizeof(Buff));

	int Err = WideCharToMultiByte(CP_UTF8, 0, Target.c_str(), -1,Buff, BuffSize,NULL,NULL);

	if (Err == 0){
		delete[] Buff;
		return string("Failed UTF8ToUnicode(...) convert operation");
	}

	string Res(Buff);
	delete[] Buff;
	return Res;
#else*/
	const wchar_t *wcTarget = Target.c_str();
    int size = Target.size();

    char *cResult = new char[size];
    memset(cResult, 0, size);

    wcstombs( cResult, wcTarget, size);

    string Result;
    for(int i=0;i<size;i++){
		Result = Result + cResult[i];
    }
    delete []cResult;
    return Result;
//#endif
}

wstring StrToWStr(const  string &Target)
{/*
#ifdef _WIN32

	int BuffSize = MultiByteToWideChar(CP_UTF8, 0, Target.c_str(), -1, NULL, 0);

	if(BuffSize == 0){
		return wstring(L"Failed UTF8ToUnicode(...) BuffSize is 0");
	}

	wchar_t *Buff = new wchar_t[BuffSize];
	if (Buff == NULL){
		return wstring(L"Failed UTF8ToUnicode(...) Buff is NULL");
	}
	memset(Buff, 0, sizeof(Buff));

	int Err = MultiByteToWideChar(CP_UTF8, 0, Target.c_str(), -1,Buff, BuffSize);

	if (Err == 0){
		delete[] Buff;
		return wstring(L"Failed UTF8ToUnicode(...) convert operation");
	}

	wstring Res(Buff);
	delete[] Buff;
	return Res;

#else*/
    const char *cTarget = Target.c_str();
    int size =  Target.size();

    wchar_t *wcResult = new wchar_t[size];
    memset(wcResult, 0, size * sizeof(wchar_t));

    mbstowcs( wcResult, cTarget, size);

    wstring Result;
    for(int i=0;i<size;i++){
		Result = Result + wcResult[i];
    }
    delete[] wcResult;
    return  Result;
//#endif
}

char WcharToChar(const wchar_t &Symbol)
{
    char Result;
    wcstombs( &Result, &Symbol, 1);

    return  (Result);
}

wchar_t CharToWChar(const char &Symbol)
{
    wchar_t Result;
    mbstowcs( &Result, &Symbol, 1);

    return  (Result);
}

// Заметка: для перевода чисел в строки используется классический способ с
//			sprintf, так как itoa не находится в стандарте С++. также можно
//			использовать строковые потоки, размер кода меньше будет.

string  IntToStr(const int &Value)
{
	char buffer[50];
	sprintf(buffer, "%d", Value);
	string result = buffer;
	return result;
}

wstring IntToWStr(const int &Value)
{
	char buffer[50];
	sprintf(buffer, "%d", Value);
	return StrToWStr(buffer);
}

wstring LongToWStr(const long &Value)
{
	char buffer[50];
	sprintf(buffer, "%ld", Value);
	return StrToWStr(buffer);
}

string FloatToStr(const float &Value)
{
	char buffer[50];
	sprintf(buffer, "%f", Value);
	string result = buffer;
	return result;
}

wstring FloatToWStr(const float &Value)
{
	char buffer[50];
	sprintf(buffer, "%f", Value);
	return StrToWStr(buffer);
}

string BoolToStr(const bool &Value)
{
	char buffer[50];
	sprintf(buffer, "%d", (int)Value);
	string result = buffer;
	return result;
}

wstring BoolToWStr(const bool &Value)
{
	char buffer[50];
	sprintf(buffer, "%d", (int)Value);
	return StrToWStr(buffer);
}

wstring DoubleToWStr(const double &Value)
{
	char buffer[50];
	sprintf(buffer, "%f", Value);
	return StrToWStr(buffer);
}

bool IsWStrBool(const wstring &Target)
{
	int Size = Target.length();
	if(Size == 0){return false;}
	if(Size>1)	 {return false;}

	if(Target == L"0" || Target == L"1"){return true;}

	return false;
}

bool IsWStrInt(const wstring &Target)
{
	int Size = Target.length();
	if(Size == 0){return false;}

	int SpartPos = 0;

	if(Target[0] == '-'){
		SpartPos = 1;
	}
	for(int i=SpartPos; i<Size; i++){
		if(isdigit(Target[i]) == false){
			return false;
		}
	}
	return true;
}

bool IsWStrFloat(const wstring &Target)
{
	int Size = Target.length();
	if(Size == 0){return false;}

	bool PoinCheck = false;
	int SpartPos = 0;

	if(Target[0] == '-'){
		SpartPos = 1;
	}

	for(int i=SpartPos; i<Size; i++){
		if(isdigit(Target[i]) == false){
			if(i>0 && PoinCheck == false){
				if(Target[i] == '.'){PoinCheck = true;}
				else				{return false;}
			}
			else{
				return false;
			}
		}
	}
	return true;
}

int WStrToInt(const wstring &Val)
{
	return atoi(WStrToStr(Val).c_str());
}

float WStrToFloat(const wstring &Val)
{
	return (float)atof(WStrToStr(Val).c_str());
}

bool WStrToBool(const wstring &Val)
{
	if(atoi(WStrToStr(Val).c_str()) == 0){return false;}
	return true;
}

int StrToInt(const string &Val)
{
	return atoi(Val.c_str());
}

float StrToFloat(const string &Val)
{
	return (float)atof(Val.c_str());
}

bool StrToBool(const string &Val)
{
	if( atoi(Val.c_str()) == 0 ){return false;}
	return true;
}

int	GenRandValue(const int &Max)
{
	if(Max == 0){return 0;}

	return rand()%Max;
}

int GenRandRangeValue(const int &Min,const int &Max)
{
	if(Min == 0 && Max == 0){return 0;}
	if(Min == Max){return Max;}

	return (rand()%(Max - Min) + Min);
}

void InitRandomizer()
{
	srand((int)time(NULL));
}

string FormatStr(const char *FrmStr, ... )
{
    string sRezult("");
    va_list args;
    va_start(args, FrmStr);
#ifdef _MSC_VER
    #ifdef _WIN32
    int len = _vscprintf(FrmStr, args);
    if (len > 0) {
        len += (1 + 2);
        char *buf = new char[len];
        if (buf) {
            len = vsprintf_s(buf, len, FrmStr, args);
            if (len > 0) {
                while (len && isspace(buf[len-1])) len--;
                buf[len++] = 0;
                buf[len++] = 0;
                buf[len] = 0;
                sRezult = buf;
            }
            delete[] buf;
        }
    }
    #endif
#else
    int len = vsnprintf(NULL, 0, FrmStr, args);
    if (len > 0) {
        len += (1 + 2);
        char *buf = new char[len];
        if (buf) {
            len = vsprintf(buf, FrmStr, args);
            if (len > 0) {
                while (len && isspace(buf[len-1])) len--;
                buf[len++] = 0;
                buf[len++] = 0;
                buf[len] = 0;
                sRezult = buf;
            }
            delete[] buf;
        }
    }
#endif
    va_end(args);
    return(sRezult);
}

wstring FormatWStr(const wchar_t *FrmStr, ... )
{
#ifdef _WIN32
    wstring sRezult(L"");
    va_list args;
    va_start(args, FrmStr);
#ifdef _MSC_VER
    #ifdef _WIN32
    int len = _vscwprintf(FrmStr, args);
    if (len > 0) {
        len += (1 + 2);
        wchar_t *buf = new wchar_t[len];
        if (buf) {
            len = vswprintf_s(buf, len, FrmStr, args);
            if (len > 0) {
                while (len && iswspace(buf[len-1])) len--;
                buf[len++] = 0;
                buf[len++] = 0;
                buf[len] = 0;
                sRezult = buf;
            }
            delete[] buf;
        }
    }
    #endif
#else
    int len = vsnwprintf(NULL, 0, FrmStr, args);
    if (len > 0) {
        len += (1 + 2);
        wchar_t *buf = new wchar_t[len];
        if (buf) {
            len = vswprintf(buf, FrmStr, args);
            if (len > 0) {
                while (len && isspace(buf[len-1])) len--;
                buf[len++] = 0;
                buf[len++] = 0;
                buf[len] = 0;
                sRezult = buf;
            }
            delete[] buf;
        }
    }
#endif
    va_end(args);
    return(sRezult);
#else
	return L"on Linux or MacOS FormatWStr() not work";
#endif
}

string BuildPathStr(const string &Str)
{
	char Separator;

#ifdef _WIN32
	Separator = '\\';
#else
	Separator = '/';
#endif

	string Result;
	string Buff;
	int BracketCounter(0);

	int StrLength(Str.length());
	for(int i=0;i<StrLength;i++){
		char Symbol(Str[i]);

		if(Symbol == ']'){BracketCounter--;}
		if(BracketCounter >=1){ Buff += Symbol; }
		if(Symbol == '['){BracketCounter++;}

		if(BracketCounter == 0){

			if(Result.length()>0){Result+=Separator;}

			Result += Buff;
			Buff = "";
		}
	}

	return Result;
}

wstring BuildPathStr(const wstring &Str)
{
	wchar_t Separator;

#ifdef _WIN32
	Separator = L'\\';
#else
	Separator = L'/';
#endif

	wstring Result;
	wstring Buff;
	int BracketCounter(0);

	int StrLength(Str.length());
	for(int i=0;i<StrLength;i++){
		wchar_t Symbol(Str[i]);

		if(Symbol == L']'){BracketCounter--;}
		if(BracketCounter >=1){ Buff += Symbol; }
		if(Symbol == L'['){BracketCounter++;}

		if(BracketCounter == 0){

			if(Result.length()>0){Result+=Separator;}

			Result += Buff;
			Buff = L"";
		}
	}

	return Result;
}


