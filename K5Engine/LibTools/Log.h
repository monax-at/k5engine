///	author: Sergey Kalenik [monax.at@gmail.com]

#pragma once
#ifndef CLASS_LOGWORKER_H_INCLUDED
#define CLASS_LOGWORKER_H_INCLUDED


#include "../Core/Core.h"
#include "../LibMath/Vector2D.h"
#include "../LibMath/Vector3D.h"
#include "Tools.h"

#include <stdio.h>
#include <assert.h>
#include <string>
#include <ctime>

using std::string;
using std::wstring;


class TLog
{
    private:
		FILE *File;
		wstring FileName;
		bool Enable;
    private:
		void Open();
		void Close();

        inline void WriteValue(const wstring &Mess);
        inline void WriteValue(const int    &Val);
        inline void WriteValue(const long   &Val);
        inline void WriteValue(const float  &Val);
        inline void WriteValue(const double &Val);
        inline void WriteValue(const TColor &Val);
        inline void WriteValue(const TPoint &Val);
        inline void WriteValue(const TValue   &Val);
        inline void WriteValue(const TVector2D &Val);
        inline void WriteValue(const TVector3D &Val);
        inline void WriteValue(const TMatrix3D &Val);
    public:
        TLog();
        TLog(const wstring &NewFileName);
        ~TLog();

		void SetEnable(const bool &Val);
		bool GetEnable() const;

        void SetFileName(const wstring &NewFileName);

		void operator ()(const  string &Mess);
        void operator ()(const wstring &Mess);
        void operator ()(const wchar_t &Char);
        void operator ()(const int &Val);
        void operator ()(const long &Val);
        void operator ()(const float  &Val);
        void operator ()(const double &Val);
        void operator ()(const TException &Val);
        void operator ()(const TColor &Val);
        void operator ()(const TPoint &Val);
        void operator ()(const TValue   &Val);
        void operator ()(const TVector2D &Val);
        void operator ()(const TVector3D &Val);
        void operator ()(const TMatrix3D &Val);

        void Write (const  string &Mess);
        void Write (const wstring &Mess);
        void Write (const wchar_t &Char);
        void Write (const int &Val);
        void Write (const long &Val);
        void Write (const float &Val);
        void Write (const double &Val);
        void Write (const TException &Val);
        void Write (const TColor &Val);
        void Write (const TPoint &Val);
        void Write (const TValue   &Val);
        void Write (const TVector2D &Val);
        void Write (const TVector3D &Val);
        void Write (const TMatrix3D &Val);
};

typedef TLog* pTLog;


#endif // CLASS_LOGWORKER_H_INCLUDED
