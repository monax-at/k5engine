///	author: Sergey Kalenik [monax.at@gmail.com]


#pragma once
#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED


#ifdef _WIN32
	#include <windows.h>
#endif

#include <stdio.h>
#include <stdarg.h>
#include <wchar.h>
#include <math.h>
#include <ctype.h>
#include <cstdlib>
#include <ctime>

#include <string.h>
#include <string>

using std::string;
using std::wstring;


string  WStrToStr(const wstring &Target);
wstring StrToWStr(const  string &Target);

char 	WcharToChar(const wchar_t &Symbol);
wchar_t CharToWChar(const char 	  &Symbol);

string  IntToStr(const int &Value);
wstring IntToWStr(const int &Value);

wstring LongToWStr(const long &Value);

string  FloatToStr(const float &Value);
wstring FloatToWStr(const float &Value);

string BoolToStr(const bool &Value);
wstring BoolToWStr(const bool &Value);

wstring DoubleToWStr(const double &Value);

bool	IsWStrBool(const wstring &Target);
bool	IsWStrInt(const wstring &Target);
bool	IsWStrFloat(const wstring &Target);

int 	WStrToInt	(const wstring &Val);
float 	WStrToFloat	(const wstring &Val);
bool 	WStrToBool	(const wstring &Val);

int 	StrToInt	(const string &Val);
float 	StrToFloat	(const string &Val);
bool 	StrToBool	(const string &Val);

// GenRandValue генерирует случайное число от 0 до Мах-1, то есть если задать Мах = 10,
// то будет сгенерировано число от 0 до 9 включительно
int		GenRandValue(const int 	 &Max);
int		GenRandRangeValue(const int &Min,const int &Max);

void 	InitRandomizer();

string  FormatStr(const char *FrmStr, ... );
wstring FormatWStr(const wchar_t *FrmStr, ... );

string 	BuildPathStr(const string &Str);
wstring BuildPathStr(const wstring &Str);


#endif // TOOLS_H_INCLUDED
