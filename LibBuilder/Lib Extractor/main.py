# -*- coding: utf-8 -*-
#coding:utf-8
'''
Created on 26.03.2010

@author: rolan
'''

import os
import shutil

def CopyDir(from_dir, to_dir):
	if os.path.isdir(to_dir):
		shutil.rmtree(to_dir)
	
	if os.path.isdir(from_dir):
		shutil.copytree(from_dir, to_dir ,ignore = shutil.ignore_patterns("*.cpp",".svn"))

	return

if __name__ == '__main__':

	CopyDir( "../../K5Engine",		"../include/K5Engine" )
	CopyDir( "../../K5EngineExtensions",	"../include/K5EngineExtensions"  )
    
	CopyDir( "../bin/MinGW",	"../lib/Windows/MinGW"  )
	CopyDir( "../bin/VisualC",	"../lib/Windows/VisualC"  )
	CopyDir( "../bin/LinuxGCC",	"../lib/Linux/GCC"  )